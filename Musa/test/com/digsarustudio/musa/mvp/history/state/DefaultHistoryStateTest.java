/**
 * 
 */
package com.digsarustudio.musa.mvp.history.state;

import static org.junit.Assert.*;

import org.junit.Test;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.musa.mvp.history.HistoryToken;
import com.digsarustudio.musa.mvp.history.HistoryTokenCollection;
import com.digsarustudio.musa.mvp.history.HistoryTokenSet;
import com.digsarustudio.musa.mvp.history.state.DefaultHistoryState;
import com.digsarustudio.musa.mvp.history.state.HistoryState;
import com.google.gwt.editor.client.Editor.Ignore;

/**
 * To test default history state class
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DefaultHistoryStateTest{
	private final String VALUE_STATE = "INIT";
	private final String STATE_TOKEN = "STATE=<INIT>";
	private final String STATE_P1 = "STATE=<INIT>;CID=<123>";
	private final String STATE_P2 = "STATE=<INIT>;CID=<123>;PID=<123>";	
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testDefaultHistoryState() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String, com.digsarustudio.musa.mvp.history.HistoryTokenCollection)}.
	 */
	@Test
	public void testDefaultHistoryStateWithHistoryTokenCollection() {
		HistoryTokenCollection collection = new HistoryTokenSet();
		collection.addToken( HistoryToken.builder(this.VALUE_STATE)
										 .setValue("INIT")
										 .build()
		);
		
		new DefaultHistoryState(this.STATE_TOKEN, collection, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String, com.digsarustudio.musa.mvp.history.HistoryTokenCollection)}.
	 */
	@Test
	public void testDefaultHistoryStateWithHistoryTokenCollectionContains2Tokens() {
		HistoryTokenCollection collection = new HistoryTokenSet();
		collection.addToken( HistoryToken.builder(this.VALUE_STATE)
										 .setValue("INIT")
										 .build()
		);
		
		collection.addToken( HistoryToken.builder(this.VALUE_STATE)
				 .setValue("1324d2")
				 .build()
		);
		
		new DefaultHistoryState(this.STATE_TOKEN, collection, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}
		
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testDefaultHistoryStateWithStringToken() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE, this.STATE_TOKEN, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test(expected=InvalidTokenFormatException.class)
	public void testDefaultHistoryStateStringWithinNoBracket() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE, "STATE=INIT", null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test(expected=InvalidTokenFormatException.class)
	public void testDefaultHistoryStateStringWithinLeftBracketOnly() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE, "STATE=<INIT", null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test(expected=InvalidTokenFormatException.class)
	public void testDefaultHistoryStateStringWithinRightBracketOnly() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE, "STATE=INIT>", null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}		
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * 
	 * No exception returned because the non-value token are supported by {@link HistoryTokenCollection}
	 * 
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testDefaultHistoryStateStringOnEqualsWithin() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE, "STATE<INIT>", null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.DefaultHistoryState#DefaultHistoryState(java.lang.String)}.
	 * 
	 * This history state can only have the key of the token.
	 * 
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testDefaultHistoryStateStringStraight() throws InvalidTokenFormatException {
		new DefaultHistoryState(this.VALUE_STATE, "STATE-INIT", null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
	}	


	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#hashCode()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testHashCodeFromSameObject() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
				
		assertEquals(state_1.hashCode(), state_1.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#hashCode()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testHashCodeFromSameValue() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		HistoryTokenCollection tokens_2 = new HistoryTokenSet(this.STATE_P1);
		
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		HistoryState state_2 = new DefaultHistoryState(this.VALUE_STATE, tokens_2, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertEquals(state_2.hashCode(), state_1.hashCode());
	}	

	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#hashCode()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testHashCodeFromDifferentValue() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		HistoryTokenCollection tokens_2 = new HistoryTokenSet(this.STATE_P2);
		
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		HistoryState state_2 = new DefaultHistoryState(this.VALUE_STATE, tokens_2, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertNotEquals(state_2.hashCode(), state_1.hashCode());
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#getStateName()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testGetStateName() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertEquals(this.VALUE_STATE, state_1.getStateName());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#fireState()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test @Ignore
	public void testFireState() throws InvalidTokenFormatException {
//		final HistoryTokenCollection tokens = new HistoryTokenSet(this.STATE_P1);
//		
//		History.addValueChangeHandler(new ValueChangeHandler<String>() {
//			
//			@Override
//			public void onValueChange(ValueChangeEvent<String> event) {
//				assertEquals(event.getValue(), tokens.toString());
//				
//			}
//		});
//		
//		DefaultHistoryState.fireState(this.VALUE_STATE, tokens);				
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#setState()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test @Ignore
	public void testSetState() throws InvalidTokenFormatException {
//		final HistoryTokenCollection tokens = new HistoryTokenSet("STATE=<MAINPAGE>");
//		
//		History.addValueChangeHandler(new ValueChangeHandler<String>() {
//			
//			@Override
//			public void onValueChange(ValueChangeEvent<String> event) {
//				String token = event.getValue();
//				
//				if( token.indexOf("MAINPAGE") < 0 ){
//					return;
//				}
//				
//				throw new UnexpectedMethodAccessedException();				
//			}
//		});
//		
//		DefaultHistoryState.setState(this.VALUE_STATE, tokens);
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#getTokens()}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testGetTokens() throws InvalidTokenFormatException {
		final HistoryTokenCollection tokens = new HistoryTokenSet(this.STATE_P1);
		
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertEquals(tokens, state_1.getTokens());		
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#equals(java.lang.Object)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testEqualsObject() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		HistoryState state_2 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertEquals(state_2.hashCode(), state_1.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#equals(java.lang.Object)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testEqualsValue() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		HistoryTokenCollection tokens_2 = new HistoryTokenSet(this.STATE_P1);
		
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		HistoryState state_2 = new DefaultHistoryState(this.VALUE_STATE, tokens_2, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertEquals(state_2.hashCode(), state_1.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.mvp.history.state.AbstractHistoryState#equals(java.lang.Object)}.
	 * @throws InvalidTokenFormatException 
	 */
	@Test
	public void testEqualsDifferentValue() throws InvalidTokenFormatException {
		HistoryTokenCollection tokens_1 = new HistoryTokenSet(this.STATE_P1);
		HistoryTokenCollection tokens_2 = new HistoryTokenSet(this.STATE_P2);
		
		HistoryState state_1 = new DefaultHistoryState(this.VALUE_STATE, tokens_1, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		HistoryState state_2 = new DefaultHistoryState(this.VALUE_STATE, tokens_2, null) {
			
			@Override
			protected void parse(HistoryTokenCollection tokens) {
				for (HistoryToken token : tokens) {
					addToken(token);
				}
			}
		};
		
		assertNotEquals(state_2.hashCode(), state_1.hashCode());
	}		
}
