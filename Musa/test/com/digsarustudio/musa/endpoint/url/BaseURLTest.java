/**
 * 
 */
package com.digsarustudio.musa.endpoint.url;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * To test {@link BaseURL}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class BaseURLTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#BaseURL(com.digsarustudio.musa.endpoint.url.ApplicationProtocolType)}.
	 */
	@Test
	public void testBaseURL() {
		new BaseURL(ApplicationProtocolType.HTTP) {
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getProtocol()}.
	 */
	@Test
	public void testGetProtocol() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#setDomainName(java.lang.String)}.
	 */
	@Test
	public void testSetDomainName() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getDomainName());
		
		url.setDomainName("digsarustudio.com.au");
		assertEquals("digsarustudio.com.au", url.getDomainName());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getDomainName()}.
	 */
	@Test
	public void testGetDomainName() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getDomainName());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getPath()}.
	 */
	@Test
	public void testGetPath() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getPath());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#setPath(java.lang.String)}.
	 */
	@Test
	public void testSetPath() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getPath());
		
		url.setPath("cart");
		
		assertEquals("cart", url.getPath());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#addParameter(com.digsarustudio.musa.endpoint.connection.RequestParameter)}.
	 */
	@Test
	public void testAddParameter() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getParameters());
		
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("12334")
												   .build());
		
		List<RequestParameter> rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru")
												   .build());
		
		rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#setParameters(java.util.List)}.
	 */
	@Test
	public void testSetParameters() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getParameters());
		
		List<RequestParameter> params = new ArrayList<>();
		params.add(RequestParameter.builder().setKey("id")
											 .setValue("12334")
											 .build());
		
		params.add(RequestParameter.builder().setKey("name")
											 .setValue("dig saru")
											 .build());
		
		assertEquals("id", params.get(0).getKey());
		assertEquals("12334", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru", params.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getParameters()}.
	 */
	@Test
	public void testGetParameters() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getParameters());
		
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("12334")
												   .build());
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru")
												   .build());
		
		List<RequestParameter> rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#clearParameters()}.
	 */
	@Test
	public void testClearParameters() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		assertNull(url.getParameters());
		
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("12334")
												   .build());
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru")
												   .build());
		
		List<RequestParameter> rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
		
		url.clearParameters();
		
		assertNull(url.getParameters());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getURL()}.
	 */
	@Test
	public void testGetURL() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		url.setDomainName("digsarustudio.com.au");
		url.setPath("cart.php");
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("12334")
												   .build());
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru")
												   .build());
		
		
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("digsarustudio.com.au", url.getDomainName());
		assertEquals("cart", url.getPath());
		
		List<RequestParameter> rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
		
		assertEquals("http://digsarustudio.com.au/cart.php?id=12334&name=dig saru", url.getURL());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getEncodedURL()}.
	 */
	@Test
	public void testGetEncodedURL() {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		url.setDomainName("digsarustudio.com.au");
		url.setPath("cart.php");
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("12334")
												   .build());
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru")
												   .build());
		
		
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("digsarustudio.com.au", url.getDomainName());
		assertEquals("cart", url.getPath());
		
		List<RequestParameter> rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
		
		assertEquals("http://digsarustudio.com.au/cart.php?id=12334&name=dig+saru", url.getEncodedURL());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testClone() throws CloneNotSupportedException {
		BaseURL url = new BaseURL(ApplicationProtocolType.HTTP) {
		};
		url.setDomainName("digsarustudio.com.au");
		url.setPath("cart.php");
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("12334")
												   .build());
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru")
												   .build());
		
		
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("digsarustudio.com.au", url.getDomainName());
		assertEquals("cart", url.getPath());
		
		List<RequestParameter> rtns = url.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
		
		assertEquals("http://digsarustudio.com.au/cart.php?id=12334&name=dig+saru", url.getEncodedURL());
		
		BaseURL copy = (BaseURL) url.clone();				
		assertEquals(ApplicationProtocolType.HTTP, copy.getProtocol());
		assertEquals("digsarustudio.com.au", copy.getDomainName());
		assertEquals("cart", copy.getPath());
		
		rtns = copy.getParameters();
		assertEquals("id", rtns.get(0).getKey());
		assertEquals("12334", rtns.get(0).getValue());
		assertEquals("name", rtns.get(1).getKey());
		assertEquals("dig saru", rtns.get(1).getValue());
		
		assertEquals("http://digsarustudio.com.au/cart.php?id=12334&name=dig+saru", copy.getEncodedURL());
	}
}
