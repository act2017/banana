/**
 * 
 */
package com.digsarustudio.musa.endpoint.url;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * To test {@link HTTPURL}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class HTTPURLTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.HTTPURL#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testClone() throws CloneNotSupportedException {
		URL url = HTTPURL.builder().setDomainName("dig-saru-studio.com.au")
									.setPath("clone")
									.addParameter(RequestParameter.builder().setKey("id")
																			.setValue("11234")
																			.build())
									.addParameter(RequestParameter.builder().setKey("name")
																			.setValue("dig saru studio")
																			.build())
									.build();
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("dig-saru-studio.com.au", url.getDomainName());
		assertEquals("clone", url.getPath());
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
									
		
		URL copy = (URL) url.clone();
		assertEquals(ApplicationProtocolType.HTTP, copy.getProtocol());
		assertEquals("dig-saru-studio.com.au", copy.getDomainName());
		assertEquals("clone", copy.getPath());
		
		params = copy.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.HTTPURL#builder()}.
	 */
	@Test
	public void testBuilder() {
		HTTPURL.builder();
	}
	
	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getProtocol()}.
	 */
	@Test
	public void testGetProtocol() {
		URL url = HTTPURL.builder().build();
		
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#setDomainName(java.lang.String)}.
	 */
	@Test
	public void testSetDomainName() {
		URL url = HTTPURL.builder().build();
		assertNull(url.getDomainName());
		
		url.setDomainName("dig-saru-studio.com.au");
		assertEquals("dig-saru-studio.com.au", url.getDomainName());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getDomainName()}.
	 */
	@Test
	public void testGetDomainName() {

		URL url = HTTPURL.builder().setDomainName("dig-saru-studio.com.au")
									.build();

		assertEquals("dig-saru-studio.com.au", url.getDomainName());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getPath()}.
	 */
	@Test
	public void testGetPath() {

		URL url = HTTPURL.builder().setPath("clone")
									.build();
		assertEquals("clone", url.getPath());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#setPath(java.lang.String)}.
	 */
	@Test
	public void testSetPath() {
		URL url = HTTPURL.builder().build();
		assertNull(url.getPath());
		
		url.setPath("clone");
		assertEquals("clone", url.getPath());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#addParameter(com.digsarustudio.musa.endpoint.connection.RequestParameter)}.
	 */
	@Test
	public void testAddParameter() {
		URL url = HTTPURL.builder().build();
		assertNull(url.getParameters());
		
		url.addParameter(RequestParameter.builder().setKey("id")
												   .setValue("11234")
												   .build());
		url.addParameter(RequestParameter.builder().setKey("name")
												   .setValue("dig saru studio")
												   .build());
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());									
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#setParameters(java.util.List)}.
	 */
	@Test
	public void testSetParameters() {

		URL url = HTTPURL.builder().build();
		assertNull(url.getParameters());
		
		List<RequestParameter> incoming = new ArrayList<>();
		incoming.add(RequestParameter.builder().setKey("id")
											   .setValue("11234")
											   .build());
		incoming.add(RequestParameter.builder().setKey("name")
											   .setValue("dig saru studio")
											   .build());
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getParameters()}.
	 */
	@Test
	public void testGetParameters() {
		URL url = HTTPURL.builder().addParameter(RequestParameter.builder().setKey("id")
																			.setValue("11234")
																			.build())
									.addParameter(RequestParameter.builder().setKey("name")
																			.setValue("dig saru studio")
																			.build())
									.build();
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getParameters()}.
	 */
	@Test
	public void testGetParametersBySet() {
		List<RequestParameter> incoming = new ArrayList<>();
		incoming.add(RequestParameter.builder().setKey("id")
											   .setValue("11234")
											   .build());
		incoming.add(RequestParameter.builder().setKey("name")
											   .setValue("dig saru studio")
											   .build());
		
		URL url = HTTPURL.builder().setParameters(incoming)				
									.build();
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#clearParameters()}.
	 */
	@Test
	public void testClearParameters() {
		URL url = HTTPURL.builder().setDomainName("dig-saru-studio.com.au")
									.setPath("clone")
									.addParameter(RequestParameter.builder().setKey("id")
																			.setValue("11234")
																			.build())
									.addParameter(RequestParameter.builder().setKey("name")
																			.setValue("dig saru studio")
																			.build())
									.build();
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("dig-saru-studio.com.au", url.getDomainName());
		assertEquals("clone", url.getPath());
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
									
		
		url.clearParameters();
		
		assertNull(url.getParameters());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getURL()}.
	 */
	@Test
	public void testGetURL() {

		URL url = HTTPURL.builder().setDomainName("dig-saru-studio.com.au")
									.setPath("clone")
									.addParameter(RequestParameter.builder().setKey("id")
																			.setValue("11234")
																			.build())
									.addParameter(RequestParameter.builder().setKey("name")
																			.setValue("dig saru studio")
																			.build())
									.build();
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("dig-saru-studio.com.au", url.getDomainName());
		assertEquals("clone", url.getPath());
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
									
		assertEquals("http://dig-saru-studio.com.au/clone?id=11234&name=dig saru studio", url.getURL());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.url.BaseURL#getEncodedURL()}.
	 */
	@Test
	public void testGetEncodedURL() {
		URL url = HTTPURL.builder().setDomainName("dig-saru-studio.com.au")
									.setPath("clone")
									.addParameter(RequestParameter.builder().setKey("id")
																			.setValue("11234")
																			.build())
									.addParameter(RequestParameter.builder().setKey("name")
																			.setValue("dig saru studio")
																			.build())
									.build();
		assertEquals(ApplicationProtocolType.HTTP, url.getProtocol());
		assertEquals("dig-saru-studio.com.au", url.getDomainName());
		assertEquals("clone", url.getPath());
		
		List<RequestParameter> params = url.getParameters();
		assertEquals("id", params.get(0).getKey());
		assertEquals("11234", params.get(0).getValue());
		assertEquals("name", params.get(1).getKey());
		assertEquals("dig saru studio", params.get(1).getValue());
									
		assertEquals("http://dig-saru-studio.com.au/clone?id=11234&name=dig+saru+studio", url.getEncodedURL());
	}

}
