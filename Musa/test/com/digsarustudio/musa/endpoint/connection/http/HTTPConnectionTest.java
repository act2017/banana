/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http;

import static org.junit.Assert.*;

import java.util.List;

import javax.xml.ws.WebServiceException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;
import com.digsarustudio.musa.endpoint.connection.http.headers.ContentType;
import com.digsarustudio.musa.endpoint.connection.http.headers.HTTPMethodOverride;
import com.digsarustudio.musa.endpoint.url.HTTPURL;
import com.digsarustudio.musa.endpoint.url.URL;

/**
 * To test {@link HTTPConnection}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class HTTPConnectionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#sendGetRequest(com.digsarustudio.musa.endpoint.connection.http.RequestCallback)}.
	 * @throws HTTPRequestException 
	 */
	@Test
	public void testSendGetRequest() throws HTTPRequestException {
		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .addParameter(RequestParameter.builder().setKey("cursor")
										   								   .setValue("10")
										   								   .build())
								   .addParameter(RequestParameter.builder().setKey("limit")
										   								   .setValue("25")
										   								   .build())
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.addHeaders(HTTPMethodOverride.POST)
															.addHeaders(ContentType.JSON)
															.setData("{}")
															.build();
		
		connection.sendGetRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				assertEquals("method: GET, Param[0]: cursor = 10, Param[1]: limit = 25, data: {}"
							, responseData);
				
			}
			
			@Override
			public void onFailure(Throwable exception) {
				throw new WebServiceException("Failed to send request", exception);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#sendGetRequest(com.digsarustudio.musa.endpoint.connection.http.RequestCallback)}.
	 * @throws HTTPRequestException 
	 */
	@Test
	public void testSendGetRequestWithoutParameters() throws HTTPRequestException {
		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.addHeaders(HTTPMethodOverride.POST)
															.addHeaders(ContentType.JSON)
															.setData("{}")
															.build();
		
		connection.sendGetRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				assertEquals("method: GET, , data: {}"
							, responseData);
				
			}
			
			@Override
			public void onFailure(Throwable exception) {
				throw new WebServiceException("Failed to send request", exception);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#sendGetRequest(com.digsarustudio.musa.endpoint.connection.http.RequestCallback)}.
	 * @throws HTTPRequestException 
	 */
	@Test
	public void testSendGetRequestWithoutData() throws HTTPRequestException {
		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.build();
		
		connection.sendGetRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				assertEquals("method: GET, , data: "
							, responseData);
				
			}
			
			@Override
			public void onFailure(Throwable exception) {
				throw new WebServiceException("Failed to send request", exception);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#sendPostRequest(com.digsarustudio.musa.endpoint.connection.http.RequestCallback)}.
	 * @throws HTTPRequestException 
	 */
	@Test
	public void testSendPostRequest() throws HTTPRequestException {
		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .addParameter(RequestParameter.builder().setKey("cursor")
										   								   .setValue("10")
										   								   .build())
								   .addParameter(RequestParameter.builder().setKey("limit")
										   								   .setValue("25")
										   								   .build())
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.addHeaders(HTTPMethodOverride.POST)
															.addHeaders(ContentType.JSON)
															.setData("{}")
															.build();
		
		connection.sendPostRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				assertEquals("method: GET, Param[0]: cursor = 10, Param[1]: limit = 25, data: {}"
							, responseData);
				
			}
			
			@Override
			public void onFailure(Throwable exception) {
				throw new WebServiceException("Failed to send request", exception);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#sendPutRequest(com.digsarustudio.musa.endpoint.connection.http.RequestCallback)}.
	 * @throws HTTPRequestException 
	 */
	@Test
	public void testSendPutRequest() throws HTTPRequestException {

		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .addParameter(RequestParameter.builder().setKey("cursor")
										   								   .setValue("10")
										   								   .build())
								   .addParameter(RequestParameter.builder().setKey("limit")
										   								   .setValue("25")
										   								   .build())
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.addHeaders(HTTPMethodOverride.POST)
															.addHeaders(ContentType.JSON)
															.setData("{}")
															.build();
		
		connection.sendPutRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				assertEquals("method: GET, Param[0]: cursor = 10, Param[1]: limit = 25, data: {}"
							, responseData);
				
			}
			
			@Override
			public void onFailure(Throwable exception) {
				throw new WebServiceException("Failed to send request", exception);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#sendDeleteRequest(com.digsarustudio.musa.endpoint.connection.http.RequestCallback)}.
	 * @throws HTTPRequestException 
	 */
	@Test
	public void testSendDeleteRequest() throws HTTPRequestException {

		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .addParameter(RequestParameter.builder().setKey("cursor")
										   								   .setValue("10")
										   								   .build())
								   .addParameter(RequestParameter.builder().setKey("limit")
										   								   .setValue("25")
										   								   .build())
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.addHeaders(HTTPMethodOverride.POST)
															.addHeaders(ContentType.JSON)
															.setData("{}")
															.build();
		
		connection.sendDeleteRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				assertEquals("method: GET, Param[0]: cursor = 10, Param[1]: limit = 25, data: {}"
							, responseData);
				
			}
			
			@Override
			public void onFailure(Throwable exception) {
				throw new WebServiceException("Failed to send request", exception);				
			}
		});
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#getURL()}.
	 */
	@Test
	public void testGetURL() {
		URL url = HTTPURL.builder().setDomainName("localhost")
								   .setPath("testing/php/http_connection_test.php")
								   .addParameter(RequestParameter.builder().setKey("cursor")
										   								   .setValue("10")
										   								   .build())
								   .addParameter(RequestParameter.builder().setKey("limit")
										   								   .setValue("25")
										   								   .build())
								   .build();
		
		HTTPConnection connection = HTTPConnection.builder().setURL(url)
															.addHeaders(HTTPMethodOverride.POST)
															.addHeaders(ContentType.JSON)
															.setData("{}")
															.build();
		
		assertEquals("localhost", connection.getURL().getDomainName());
		assertEquals("testing/php/http_connection_test.php", connection.getURL().getPath());
		
		List<RequestParameter> params = connection.getURL().getParameters();
		assertEquals("cusor", params.get(0).getKey());
		assertEquals("10", params.get(0).getValue());
		assertEquals("limit", params.get(1).getKey());
		assertEquals("25", params.get(1).getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setURL(com.digsarustudio.musa.endpoint.url.URL)}.
	 */
	@Test
	public void testSetURL() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#getHeaders()}.
	 */
	@Test
	public void testGetHeaders() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setHeaders(java.util.List)}.
	 */
	@Test
	public void testSetHeaders() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#addHeaders(com.digsarustudio.musa.endpoint.connection.http.headers.HTTPHeader)}.
	 */
	@Test
	public void testAddHeaders() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#getData()}.
	 */
	@Test
	public void testGetData() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setData(java.lang.String)}.
	 */
	@Test
	public void testSetData() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#addParameter(com.digsarustudio.musa.endpoint.connection.RequestParameter)}.
	 */
	@Test
	public void testAddParameter() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setParameters(java.util.List)}.
	 */
	@Test
	public void testSetParameters() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#getParameters()}.
	 */
	@Test
	public void testGetParameters() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#clearParameters()}.
	 */
	@Test
	public void testClearParameters() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#builder()}.
	 */
	@Test
	public void testBuilder() {
		fail("Not yet implemented");
	}

}
