/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * To test {@link RequestParameter}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class RequestParameterTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.RequestParameter#getKey()}.
	 */
	@Test
	public void testGetKey() {
		RequestParameter param = RequestParameter.builder().setKey("id").build();
		
		assertEquals("id", param.getKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.RequestParameter#setKey(java.lang.String)}.
	 */
	@Test
	public void testSetKey() {
		RequestParameter param = RequestParameter.builder().build();
		assertNull(param.getKey());
		
		param.setKey("id");
		assertEquals("id", param.getKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.RequestParameter#getValue()}.
	 */
	@Test
	public void testGetValue() {
		RequestParameter param = RequestParameter.builder().setValue("11234").build();
		
		assertEquals("11234", param.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.RequestParameter#setValue(java.lang.String)}.
	 */
	@Test
	public void testSetValue() {
		RequestParameter param = RequestParameter.builder().build();
		assertNull(param.getValue());
		
		param.setValue("11234");
		assertEquals("11234", param.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.RequestParameter#builder()}.
	 */
	@Test
	public void testBuilder() {
		RequestParameter.builder();
	}

}
