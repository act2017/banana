/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJDO;
import com.digsarustudio.musa.endpoint.connection.response.CollectionResponse;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * To test {@link CollectionResponseJDO}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class CollectionResponseJDOTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJDO#getNextPageToken()}.
	 */
	@Test
	public void testGetNextPageToken() {
		CollectionResponse<Integer> collectionResponse = CollectionResponseJDO.<Integer>builder()
																			 .setNextPageToken("3324")
																			 .build();
		assertEquals("3324", collectionResponse.getNextPageToken());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJDO#getItems()}.
	 */
	@Test
	public void testGetItems() {
		CollectionResponse<Integer> collectionResponse = CollectionResponseJDO.<Integer>builder()
																			 .addItem(12)
																			 .addItem(15)
																			 .build();
		Iterator<Integer> items = collectionResponse.getItems().iterator();
		
		assertTrue(items.hasNext());
		assertEquals(new Integer(12), items.next());
		
		assertTrue(items.hasNext());
		assertEquals(new Integer(15), items.next());
		
		assertFalse(items.hasNext());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJDO.Builder#setItems(java.util.Collection)}.
	 */
	@Test
	public void testSetItems() {
		List<Integer> source = new ArrayList<>();
		source.add(33);
		source.add(44);
		
		CollectionResponse<Integer> collectionResponse = CollectionResponseJDO.<Integer>builder()
																			 .setItems(source)
																			 .build();
		Iterator<Integer> items = collectionResponse.getItems().iterator();
		
		assertTrue(items.hasNext());
		assertEquals(new Integer(33), items.next());
		
		assertTrue(items.hasNext());
		assertEquals(new Integer(44), items.next());
		
		assertFalse(items.hasNext());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJDO#builder()}.
	 */
	@Test
	public void testBuilder() {		
		CollectionResponse<Integer> collectionResponse = CollectionResponseJDO.<Integer>builder()																			 
																			 .build();		
		assertNull(collectionResponse.getNextPageToken());
		assertNull(collectionResponse.getItems());
	}

}
