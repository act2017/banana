/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJSO;
import com.digsarustudio.musa.endpoint.connection.response.CollectionResponse;

/**
 * To test {@link CollectionResponseJSO}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class CollectionResponseDTOTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJSO#getNextPageToken()}.
	 */
	@Test
	public void testGetNextPageToken() {
		CollectionResponse<String> collection = CollectionResponseJSO.<String>builder().setNextPageToken("3451")
																					  .build();
		assertEquals("3451", collection.getNextPageToken());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJSO#getItems()}.
	 */
	@Test
	public void testGetItems() {
		CollectionResponse<String> collection = CollectionResponseJSO.<String>builder()
																	.addItem("TOYOTA")
																	.addItem("CAMRY")
																	.build();
		
		Iterator<String> iterator = collection.getItems().iterator();
		assertTrue(iterator.hasNext());
		assertEquals("TOYOTA", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("CAMRY", iterator.next());		
		assertFalse(iterator.hasNext());
	}

	/**
	 * Test method for {@link com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJSO#getItems()}.
	 */
	@Test
	public void testSetItems() {
		List<String> items = new ArrayList<>();
		items.add("TOYOTA");
		items.add("CAMERY");
		
		CollectionResponse<String> collection = CollectionResponseJSO.<String>builder()
																	.setItems(items)
																	.build();
		
		Iterator<String> iterator = collection.getItems().iterator();
		assertTrue(iterator.hasNext());
		assertEquals("TOYOTA", iterator.next());
		assertTrue(iterator.hasNext());
		assertEquals("CAMRY", iterator.next());		
		assertFalse(iterator.hasNext());
	}

}
