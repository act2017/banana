/**
 * 
 */
package com.digsarustudio.musa.event.widget.composite;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * A widget that implements this interface provides registration for
 * {@link com.digsarustudio.frontend.togautogroup.client.ui.widget.event.ClickHandler} instances.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public interface HasCompositeClickHandlers {
	/**
	   * Adds a {@link ClickEvent} handler.
	   * 
	   * @param handler the click handler
	   * @return {@link HandlerRegistration} used to remove this handler
	   */
	  HandlerRegistration addClickHandler(ClickEvent.Handler handler);
}
