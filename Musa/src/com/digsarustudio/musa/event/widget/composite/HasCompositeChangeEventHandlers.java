/**
 * 
 */
package com.digsarustudio.musa.event.widget.composite;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * A widget that implements this interface provides registration for
 * {@link com.digsarustudio.frontend.togautogroup.client.ui.widget.event.ChangeEventHandler} instances.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasCompositeChangeEventHandlers {
	/**
	   * Adds a {@link ChangeEvent} handler.
	   * 
	   * @param handler the click handler
	   * @return {@link HandlerRegistration} used to remove this handler
	   */
	  HandlerRegistration addChangeHandler(ChangeEvent.Handler handler);
}
