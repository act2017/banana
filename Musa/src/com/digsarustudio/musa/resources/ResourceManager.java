/**
 * 
 */
package com.digsarustudio.musa.resources;

import com.google.gwt.core.shared.GWT;

/**
 * The manager of the resources
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ResourceManager {
	private static ResourceManager instance;
	
	private ImageResources imageResources;
	
	/**
	 * Encapsulated for singleton 
	 */
	private ResourceManager() {
	}

	/**
	 * Returns the instance of the resource manager
	 * 
	 * @return the instance of the resource manager
	 */
	public static ResourceManager getInstance() {
		if( null == instance ){
			instance = new ResourceManager();
		}

		return instance;
	}

	/**
	 * Returns the image bundle
	 * 
	 * @return the image bundle
	 */
	public ImageResources getImagesBundle(){
		if(null == this.imageResources){
			this.imageResources = GWT.create(ImageResources.class);				
		}
		
		return this.imageResources;
	}

	/**
	 * Returns the icon resource manager
	 * 
	 * @return The icon resource manager
	 */
	public IconResourceManager getIconResourceManager(){
		return IconResourceManager.getInstance();
	}
}
