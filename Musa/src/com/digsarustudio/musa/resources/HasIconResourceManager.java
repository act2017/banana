/**
 * 
 */
package com.digsarustudio.musa.resources;

import com.google.gwt.resources.client.ImageResource;

/**
 * The sub-type of this interface is a sort of icon resource manager or it has the icon resource manager in it. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasIconResourceManager {
	/**
	 * Returns the resource of bucket deleting icon
	 * 
	 * @return the resource of bucket deleting icon
	 */
	ImageResource getBucketDeletingIcon();
	
	/**
	 * Returns the resource of pen editing icon
	 * 
	 * @return the resource of pen editing icon
	 */
	ImageResource getPenEditingIcon();
}
