/**
 * 
 */
package com.digsarustudio.musa.resources;

import com.google.gwt.core.client.GWT;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * The resources for the icons
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ListIconResources extends ClientBundle {
	public static final ListIconResources INSTANCE = GWT.create(ListIconResources.class);	
	
	@Source("images/icons/list/bucket-deleting-512.png")
	ImageResource getBucketDeletingIcon();
	
	@Source("images/icons/list/pen-editing-512.png")
	ImageResource getPenEditingIcon();
}
