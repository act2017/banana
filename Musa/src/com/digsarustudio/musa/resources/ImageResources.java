/**
 * 
 */
package com.digsarustudio.musa.resources;

import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ImageResource;

/**
 * The built-in image resources for the client code
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ImageResources extends ClientBundle {
	@Source("images/select.png")
	ImageResource getSelectPhotoIcon();
	
	@Source("images/search-512.png")
	ImageResource getSearchIcon();
	
	@Source("images/product-512.png")
	ImageResource getSampleProductIcon();
	
	@Source("images/category-981.png")
	ImageResource getSampleCateogryIcon();
	
	@Source("images/img-under-construct-512.png")
	ImageResource getImageUnderConstructionIcon();
}
