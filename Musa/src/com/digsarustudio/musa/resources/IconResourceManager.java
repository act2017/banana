/**
 * 
 */
package com.digsarustudio.musa.resources;

import com.google.gwt.resources.client.ImageResource;

/**
 * The singleton icon resource manager
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class IconResourceManager implements HasIconResourceManager{
	private static IconResourceManager instance;

	/**
	 * Seal the constructor
	 */
	private IconResourceManager() {
	}

	/**
	 * Get the Instance
	 * @return The instance of this object
	 */
	public static IconResourceManager getInstance() {
		if (null == instance) {
			instance = new IconResourceManager();
		}

		return instance;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.resources.HasIconResourceManager#getBucketDeletingIcon()
	 */
	@Override
	public ImageResource getBucketDeletingIcon() {
		return ListIconResources.INSTANCE.getBucketDeletingIcon();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.resources.HasIconResourceManager#getPenEditingIcon()
	 */
	@Override
	public ImageResource getPenEditingIcon() {
		return ListIconResources.INSTANCE.getPenEditingIcon();
	}
	
}
