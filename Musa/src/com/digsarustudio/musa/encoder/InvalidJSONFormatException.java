/**
 * 
 */
package com.digsarustudio.musa.encoder;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class InvalidJSONFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4206676890323491235L;

	/**
	 * 
	 */
	public InvalidJSONFormatException() {

	}

	/**
	 * @param message
	 */
	public InvalidJSONFormatException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public InvalidJSONFormatException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidJSONFormatException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidJSONFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
