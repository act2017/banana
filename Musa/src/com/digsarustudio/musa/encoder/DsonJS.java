/**
 * 
 */
package com.digsarustudio.musa.encoder;

import com.digsarustudio.banana.utils.Dson;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.core.client.JsonUtils;

/**
 * Represents a encoder/decoder for JSON string
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class DsonJS implements Dson {	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Dson#toJSON(java.lang.Object)
	 */
	@Override
	public <T> String toJSON(T source) {
		return JsonUtils.stringify((JavaScriptObject) source);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Dson#fromJSON(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T> T fromJSON(String jsonString) {
		return (T)JsonUtils.safeEval(jsonString);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Dson#fromJSON(java.lang.String, java.lang.Class)
	 */
	@Override
	public <T> T fromJSON(String jsonString, Class<T> type) {
		throw new UnsupportedOperationException("fromJSON(String jsonString, Class<T> type) is not supported yet.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Dson#isJSONString(java.lang.String)
	 */
	@Override
	public Boolean isJSONString(String text) {
		return JsonUtils.safeToEval(text);
	}
}
