/**
 * 
 */
package com.digsarustudio.musa.command;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 * @deprecated	1.0.15 unused
 */
public interface Update {
	<T extends JavaScriptObject> void setTarget(T target);
	<T extends JavaScriptObject> void setData(T data);
}
