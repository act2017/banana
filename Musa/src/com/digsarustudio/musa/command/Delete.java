/**
 * 
 */
package com.digsarustudio.musa.command;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * The sub-type works as a deletion command
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.4
 * @deprecated	1.0.15 unused
 */
public interface Delete {
	void setId(String id);
	
	<T extends JavaScriptObject> void setAdditionalData(T data);
}
