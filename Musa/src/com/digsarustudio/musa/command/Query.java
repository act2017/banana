/**
 * 
 */
package com.digsarustudio.musa.command;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @deprecated	1.0.15 unused
 */
public interface Query {
	void setId(String id);
	void setKeyword(String keyword);
	<T extends JavaScriptObject> void setAdditionalData(T data);
}
