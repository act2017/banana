/**
 * 
 */
package com.digsarustudio.musa.command.json;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @deprecated	1.0.15 unused
 */
public class InsertJSO extends JavaScriptObject implements com.digsarustudio.musa.command.Insert {
	/**
	 * 
	 * To build Object object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @deprecated	1.0.15 unused
	 */
	public static class Builder implements ObjectBuilder<InsertJSO> {
		private JavaScriptObject data = null;
				
		/**
		 * @param data the data to set
		 */
		public Builder setData(JavaScriptObject data) {
			this.data = data;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public InsertJSO build() {
			InsertJSO jso  = InsertJSO.createObject().cast();
			
			jso.setData(this.data);
			
			return jso;
		}
	}
	
	/**
	 * 
	 */
	protected InsertJSO() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.command.Insert#setData(com.google.gwt.core.client.JavaScriptObject)
	 */
	@Override
	public final native <T extends JavaScriptObject> void setData(T data) /*-{
		this.data = data;
	}-*/;

	
	public static Builder builder(){
		return new Builder();
	}
}
