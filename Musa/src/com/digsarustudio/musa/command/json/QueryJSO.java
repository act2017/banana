/**
 * 
 */
package com.digsarustudio.musa.command.json;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.command.Query;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @deprecated	1.0.15 unused
 */
public class QueryJSO extends JavaScriptObject implements Query {
	/**
	 * 
	 * To build Object object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @deprecated	1.0.15 unused
	 */
	public static class Builder implements ObjectBuilder<QueryJSO> {
		private String id = null;
		private String keyword = null;
		private JavaScriptObject additional = null;
				
		/**
		 * @param id the id to set
		 */
		public Builder setId(String id) {
			this.id = id;
			return this;
		}

		/**
		 * @param keyword the keyword to set
		 */
		public Builder setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		/**
		 * @param additional the additional to set
		 */
		public Builder setAdditional(JavaScriptObject additional) {
			this.additional = additional;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public QueryJSO build() {
			QueryJSO jso  = QueryJSO.createObject().cast();
			
			jso.setId(this.id);
			jso.setKeyword(this.keyword);
			jso.setAdditionalData(this.additional);
			
			return jso;
		}
	}
	
	/**
	 * 
	 */
	protected QueryJSO(){
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.command.Query#setId(java.lang.String)
	 */
	@Override
	public final native void setId(String id) /*-{
		this.id = id;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.command.Query#setKeyword(java.lang.String)
	 */
	@Override
	public final native void setKeyword(String keyword) /*-{
		this.keyword = keyword;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.command.Query#setAdditionalData(com.google.gwt.core.client.JavaScriptObject)
	 */
	@Override
	public final native <T extends JavaScriptObject> void setAdditionalData(T data) /*-{
		this.additional = data;
	}-*/;

	
	public static Builder builder(){
		return new Builder();
	}
}
