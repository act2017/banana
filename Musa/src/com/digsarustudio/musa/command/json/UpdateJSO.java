/**
 * 
 */
package com.digsarustudio.musa.command.json;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.command.Update;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @deprecated	1.0.15 unused
 */
public class UpdateJSO extends JavaScriptObject implements Update {
	/**
	 * 
	 * To build Object object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 * @deprecated	1.0.15 unused
	 */
	public static class Builder implements ObjectBuilder<UpdateJSO> {
		private JavaScriptObject data = null;
		private JavaScriptObject target = null;
				
		/**
		 * @param data the data to set
		 */
		public Builder setData(JavaScriptObject data) {
			this.data = data;
			return this;
		}

		/**
		 * @param target the target to set
		 */
		public Builder setTarget(JavaScriptObject target) {
			this.target = target;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public UpdateJSO build() {
			UpdateJSO jso  = UpdateJSO.createObject().cast();
			
			jso.setData(this.data);
			jso.setTarget(this.target);
			
			return jso;
		}
	}
	
	
	/**
	 * 
	 */
	protected UpdateJSO() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.command.Update#setTarget(com.google.gwt.core.client.JavaScriptObject)
	 */
	@Override
	public final native <T extends JavaScriptObject> void setTarget(T target) /*-{
		this.target = target;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.command.Update#setData(com.google.gwt.core.client.JavaScriptObject)
	 */
	@Override
	public final native <T extends JavaScriptObject> void setData(T data)  /*-{
		this.data = data;
	}-*/;

	public static Builder builder(){
		return new Builder();
	}	
}
