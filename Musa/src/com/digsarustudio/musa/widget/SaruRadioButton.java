/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.google.gwt.i18n.client.HasDirection.Direction;
import com.google.gwt.i18n.shared.DirectionEstimator;
import com.google.gwt.safehtml.shared.SafeHtml;
import com.google.gwt.user.client.ui.RadioButton;

/**
 * The wrapper of {@link RadioButton} to save the id of button for the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class SaruRadioButton extends RadioButton {

	private String optionValue	= null;
		
	public SaruRadioButton() {
		super("");
	}
	
	/**
	 * @param name
	 */
	public SaruRadioButton(String name) {
		super(name);
		
	}

	/**
	 * @param name
	 * @param label
	 */
	public SaruRadioButton(String name, SafeHtml label) {
		super(name, label);
		
	}

	/**
	 * @param name
	 * @param label
	 */
	public SaruRadioButton(String name, String label) {
		super(name, label);
		
	}

	/**
	 * @param name
	 * @param label
	 * @param dir
	 */
	public SaruRadioButton(String name, SafeHtml label, Direction dir) {
		super(name, label, dir);
		
	}

	/**
	 * @param name
	 * @param label
	 * @param directionEstimator
	 */
	public SaruRadioButton(String name, SafeHtml label, DirectionEstimator directionEstimator) {
		super(name, label, directionEstimator);
		
	}

	/**
	 * @param name
	 * @param label
	 * @param dir
	 */
	public SaruRadioButton(String name, String label, Direction dir) {
		super(name, label, dir);
		
	}

	/**
	 * @param name
	 * @param label
	 * @param directionEstimator
	 */
	public SaruRadioButton(String name, String label, DirectionEstimator directionEstimator) {
		super(name, label, directionEstimator);
		
	}

	/**
	 * @param name
	 * @param label
	 * @param asHTML
	 */
	public SaruRadioButton(String name, String label, boolean asHTML) {
		super(name, label, asHTML);		
	}

	public void setOptionValue(String value) {
		this.optionValue = value;
	}
	
	public String getOptionValue() {
		return this.optionValue;
	}
	
	public void setLabel(String label) {
		this.setText(label);
	}
}
