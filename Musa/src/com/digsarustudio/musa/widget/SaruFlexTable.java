/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FlexTable;

/**
 * This sort of flexible table can deal with the mouse event by row. 
 * The GWT flexible table only deal with cell event.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * The following specifications are supported in this version as following:
 * 	+ 1.0.0
 * 		- MouseOver
 * 		- MouseOut
 * 
 * 	+ 1.0.1
 * 		- A column object to handle the display or event for each cell.
 */
public class SaruFlexTable extends FlexTable {

	private String mouseOverStyle;
	
	/**
	 * 
	 */
	public SaruFlexTable() {
		//TODO Using a map or list to define what event has to be sink into handler manager.
		sinkEvents(Event.ONMOUSEOVER|Event.ONMOUSEOUT);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.Widget#onBrowserEvent(com.google.gwt.user.client.Event)
	 */
	@Override
	public void onBrowserEvent(Event event) {
		try{
			super.onBrowserEvent(event);
			
			Element td = this.getEventTargetCell(event);
			if( null == td){
				return;
			}
			
			Element tr = DOM.getParent(td);
			switch (DOM.eventGetType(event)) {
				case Event.ONMOUSEOVER:
					tr.addClassName(this.mouseOverStyle);
					break;
				case Event.ONMOUSEOUT:
					tr.removeClassName(this.mouseOverStyle);
					break;
					
				default:
					break;
			}
		}catch (Exception e) {
			//Ignore for the table click on the border of cell
		}		
	}

	/**
	 * Assign a style used when a mouse over a row
	 * 
	 * @param style The mouse over styling
	 */
	public void setMouseOverStyle(String style) {
		this.mouseOverStyle = style;
	}
}
