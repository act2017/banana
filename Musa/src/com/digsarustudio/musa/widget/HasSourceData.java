/**
 * 
 */
package com.digsarustudio.musa.widget;

/**
 * The sub-type has the ability to set or get a data object.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasSourceData {
	Object getSourceData();
	void setSourceData(Object data);
}
