/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type has the ability to handle {@link ChangeEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 *
 */
public interface HasChangeHandlers {
	HandlerRegistration addChangeHandler(ChangeHandler handler);
}
