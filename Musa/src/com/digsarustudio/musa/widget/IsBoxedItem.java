/**
 * 
 */
package com.digsarustudio.musa.widget;

/**
 * The sub-type provides caption, value for the {@link SaruListBox}.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * 
 * @param T		The data type of source data to store.
 */
public interface IsBoxedItem {
	/**
	 * Returns the caption of the item in the box
	 * 
	 * @return the caption of the item in the box
	 */
	String getCaption();
	
	/**
	 * Returns the key of this item for the box container to identify.
	 * 
	 * @return the key of this item for the box container to identify.
	 */
	String getKey();
}
