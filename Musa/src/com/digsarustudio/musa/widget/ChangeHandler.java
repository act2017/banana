/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.google.gwt.event.shared.EventHandler;

/**
 * This event handler handles {@link ChangeEvent} when the value or status has been changed in the sub widget.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musacea-1.0.3 instead 
 */
public interface ChangeHandler extends EventHandler {
	void onChange(ChangeEvent event);
}
