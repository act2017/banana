/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.DecoratedPopupPanel;
import com.google.gwt.user.client.ui.Label;

/**
 * Represents a pop-up panel which contains a notification for use and will destroy itself in a few seconds.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class BalloonMessage extends DecoratedPopupPanel {

	/**
	 * The delay time to destroy
	 */
	private Integer dealyTime = 3000;
	
	private String message = "";
	
	/**
	 * Constructs a pop-up panel
	 */
	public BalloonMessage(String message) {
		this.message = message;
	}
	
	/**
	 * Constructs a pop-up panel
	 * 
	 * @param delayTime the delay time to destroy
	 */
	public BalloonMessage(Integer delayTime, String message) {
		this(message);
		this.dealyTime = delayTime;
	}

	/**
	 * @param autoHide
	 */
	public BalloonMessage(boolean autoHide) {
		super(autoHide);
	}

	/**
	 * @param autoHide
	 * @param modal
	 */
	public BalloonMessage(boolean autoHide, boolean modal) {
		super(autoHide, modal);
	}
			
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.PopupPanel#show()
	 */
	@Override
	public void show() {
		super.show();
				
		Label msg = new Label(this.message);
		msg.setWidth("300px");
		msg.setHeight("100px");
		
		this.add(msg);
		this.triggerDestroyTimer();
	}

	/**
	 * Enable the destroy mechanism
	 */
	private void triggerDestroyTimer(){
		Timer timer = new Timer() {			
			@Override
			public void run() {
				hide();				
			}
		};
		
		timer.schedule(this.dealyTime);
	}

}
