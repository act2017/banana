/**
 * 
 */
package com.digsarustudio.musa.widget;

/**
 * The boxed item for {@link SaruListBox}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 *
 */
public abstract class ListBoxItem<T> extends BoxedItem<T, String> {

	/**
	 * Constructs a boxed item for the Box component.<br>
	 * 
	 * @param source The source data for the Box component to display
	 */
	public ListBoxItem() {
		super();
	}

}
