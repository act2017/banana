/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can be added {@link RowAppendingHandler}.<br>
 * 
 * TODO use a generic handler instead of this.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public interface HasRowAppendingHandler {
	HandlerRegistration addAppendRowHandler(final RowAppendingHandler handler);
}
