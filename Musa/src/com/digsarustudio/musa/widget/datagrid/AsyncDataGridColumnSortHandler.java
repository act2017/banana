/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

/**
 * This handler used by {@link SaruDataGrid} to return a data grid column sorting event to the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3
 *
 */
public interface AsyncDataGridColumnSortHandler {
	/**
	 * User clicks the header to sort a column.
	 * 
	 * @param columnIndex
	 * @param isAscending
	 * @param cursor
	 */
	void onColumnSort(Integer columnIndex, Boolean isAscending, Integer cursor);
}
