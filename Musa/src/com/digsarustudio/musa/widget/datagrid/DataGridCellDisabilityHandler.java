/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

/**
 * The sub-type check the disability of the target value according to the result of checking.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public interface DataGridCellDisabilityHandler<T> {
	Boolean isDisabled(T value);
}
