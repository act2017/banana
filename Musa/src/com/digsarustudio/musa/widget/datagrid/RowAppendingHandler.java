/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

/**
 * The sub-type can handles the row appending behaviour.<br>
 *
 * TODO use a generic handler instead of this.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3
 *
 */
public interface RowAppendingHandler {
	void onAppendRow();
}
