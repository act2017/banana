/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

/**
 * This command handler handles the event from the button cell, text cell, or image cell.<br>
 *  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3
 *
 */
public interface DataGridCellCommandHandler<T> {
	void execute(T source);
}
