/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

/**
 * The types of column for {@link SaruDataGrid}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3
 *
 */
public enum DataGridColumnTypes {
	 Text
	,Image
	,ActionBar
	,Button
	,Toggle
	,ActionButton
	;
}
