/**
 * 
 */
package com.digsarustudio.musa.widget.datagrid;

/**
 * This handles page range changing event.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3
 *
 */
public interface AsyncPagerChangeHandler {
	/**
	 * The page change
	 * 
	 * @param cursor The location(index) of the first data to deal with.
	 */
	void onPageChange(Integer cursor);
}
