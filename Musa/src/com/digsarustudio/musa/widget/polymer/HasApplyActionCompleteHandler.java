/**
 * 
 */
package com.digsarustudio.musa.widget.polymer;

import com.digsarustudio.musa.mvp.presenter.Presenter;
import com.digsarustudio.musa.mvp.view.SupervisingView;
import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type has the ability to receive a {@link ApplyActionCompleteHandler} and call it when a particular action has been completed 
 * to tell the parent presenter the action is done.<br>
 * Normally, this handler is applied for {@link Presenter} rather than {@link SupervisingView}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public interface HasApplyActionCompleteHandler {
	HandlerRegistration addApplyActionCompleteHandler(final ApplyActionCompleteHandler handler);
}
