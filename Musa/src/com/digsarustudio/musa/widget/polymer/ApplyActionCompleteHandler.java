/**
 * 
 */
package com.digsarustudio.musa.widget.polymer;

import com.digsarustudio.musa.widget.polymer.dialog.ApplyActionCompleteEvent;
import com.google.gwt.event.shared.EventHandler;

/**
 * This handler callback to parent when the particular action has been completed.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public interface ApplyActionCompleteHandler extends EventHandler{
	void onApplyActionComplete(ApplyActionCompleteEvent event);
}
