/**
 * 
 */
package com.digsarustudio.musa.widget.polymer;

import com.google.gwt.event.shared.EventHandler;

/**
 * The event handler of {@link DeleteEvent}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 *
 */
public interface DeleteHandler extends EventHandler {
	void onDelete(DeleteEvent event);
}
