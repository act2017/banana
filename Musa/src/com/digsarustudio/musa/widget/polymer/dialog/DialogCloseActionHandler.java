/**
 * 
 */
package com.digsarustudio.musa.widget.polymer.dialog;

/**
 * This handler handles dialog closing event.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	please use Musaceae-1.0.3 instead
 *
 */
public interface DialogCloseActionHandler {
	void onCloseDialog();
}
