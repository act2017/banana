/**
 * 
 */
package com.digsarustudio.musa.widget.polymer.dialog;

import com.google.gwt.event.shared.EventHandler;

/**
 * This handler is used for the presenter of {@link SaruDialog} to receive a confirmation action event.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3
 *
 */
public interface DialogConfirmActionHandler extends EventHandler{
	void onConfirm();
}
