/**
 * 
 */
package com.digsarustudio.musa.widget.polymer;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can be assigned an {@link DeleteHandler} to handle {@link DeleteEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 *
 */
public interface HasDeleteHandler {
	HandlerRegistration addDeleteHandler(final DeleteHandler handler);
}
