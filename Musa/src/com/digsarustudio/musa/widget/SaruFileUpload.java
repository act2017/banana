/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.digsarustudio.musa.event.widget.composite.ChangeEvent;
import com.digsarustudio.musa.event.widget.composite.HasCompositeChangeEventHandlers;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.user.client.DOM;
import com.google.gwt.user.client.Event;
import com.google.gwt.user.client.ui.FileUpload;

/**
 * A extended file upload widget to get the size and type of the selected file.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public class SaruFileUpload extends FileUpload implements HasCompositeChangeEventHandlers{
	/**
	 * The type of file. i.e. ?????
	 */
	private String fileType	= null;
	
	/**
	 * The size of file in bytes
	 * 
	 * TODO the file size might larger than 2GB, so enhance it to long as possible.<br>
	 */
	private int fileSize	= 0;	
	
	/**
	 * The files contained in this file upload (or others?!)
	 */
	private int fileCount	= 0;
	
	private String fileContent	= null;
	
	private boolean isLoaded	= false;
	
	/**
	 * Because the FileUpload didn't call the event back to the parent widget via default AddChangeHandler(),
	 * this handler is used to instead of the default handler.
	 */
	private ChangeEvent.Handler changeHandler = null;
	
	/**
	 * Constructs a file upload object 
	 */
	public SaruFileUpload() {
		sinkEvents(Event.ONCHANGE);
	}
	
	/**
	 * Reset the selected file.
	 */
	public void clear(){
		this.getElement().setPropertyString("value", "");
	}
	
	/**
	 * @return the fileType
	 */
	public String getFileType() {
		return fileType;
	}

	/**
	 * @param fileType the fileType to set
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 * @return the fileSize
	 */
	public int getFileSize() {
		return fileSize;
	}

	/**
	 * @param fileSize the fileSize to set
	 */
	public void setFileSize(int fileSize) {
		this.fileSize = fileSize;
	}

	/**
	 * @return the fileCount
	 */
	public int getFileCount() {
		return fileCount;
	}

	/**
	 * @param fileCount the fileCount to set
	 */
	public void setFileCount(int fileCount) {
		this.fileCount = fileCount;
	}
	
	public void setFileContent(String content) {
		this.fileContent = content;
	}
	
	public String getFileContent() {
		return this.fileContent;
	}
	
	public Boolean isFileLoadingComplete() {
		return this.isLoaded;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.Widget#onBrowserEvent(com.google.gwt.user.client.Event)
	 */
	@Override
	public void onBrowserEvent(Event event) {
//It doesn't matter to call the super or not, but this should be proved.
//		super.onBrowserEvent(event);

		switch (DOM.eventGetType(event)) {
			case Event.ONCHANGE:
				this.isLoaded = false;
				this.getFileInfo(this.getElement());
				this.onChangeEvent();				
				break;
			default:
				break;
		}
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.user.client.ui.FileUpload#addChangeHandler(com.google.gwt.event.dom.client.ChangeHandler)
	 */
	@Override
	public HandlerRegistration addChangeHandler(ChangeEvent.Handler handler) {
		this.changeHandler = handler;
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				changeHandler = null;
			}
		};
	}

	private void onChangeEvent(){
		if(null != this.changeHandler){
			this.changeHandler.onChanged(new ChangeEvent(this));			
		}
	}
	
	/**
	 * To retrieve the file size and type from the FileUpload element
	 * 
	 * @param element FileUpload element
	 */
	private final native void getFileInfo(Element element) /*-{
		var self = this;
		
		//Reference: https://developer.mozilla.org/en/docs/Web/API/File
		//Files: element.files
		//Number of files: element.files.length
		//File name: element.files[n].name
		//File size: element.files[n].size
		//RelativePath: element.files[n].webkitRelativePath (non-standard implementation)
		//Type: element.files[n].type		
		if( null == element.files ){
			self.@com.digsarustudio.musa.widget.SaruFileUpload::fileCount= 0;
			self.@com.digsarustudio.musa.widget.SaruFileUpload::fileSize = 0;
			self.@com.digsarustudio.musa.widget.SaruFileUpload::fileType = "";
			self.@com.digsarustudio.musa.widget.SaruFileUpload::fileContent = "";
			return;
		}

		//The file name is provided by FileUpload::getFilename().
		self.@com.digsarustudio.musa.widget.SaruFileUpload::fileCount= element.files.length;
		self.@com.digsarustudio.musa.widget.SaruFileUpload::fileSize = element.files[0].size;
		self.@com.digsarustudio.musa.widget.SaruFileUpload::fileType = element.files[0].type;
		
		var reader = new FileReader();
		reader.onload = function(e){
			self.@com.digsarustudio.musa.widget.SaruFileUpload::fileContent = e.target.result;
			self.@com.digsarustudio.musa.widget.SaruFileUpload::isLoaded	= true; 
		}
		
		reader.readAsText(element.files[0], "UTF-8");
	}-*/;
}
