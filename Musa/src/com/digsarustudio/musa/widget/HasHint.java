/**
 * 
 */
package com.digsarustudio.musa.widget;

/**
 * This sub-type of this interface can be set a text as the data source working for hint.
 * The hint style is provided by the sub-type.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasHint {
	/**
	 * Assign a hint to this class
	 * 
	 * @param hint The hint for this class
	 */
	public void setHint(String hint);
}
