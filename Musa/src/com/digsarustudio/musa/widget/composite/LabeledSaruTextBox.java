/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

import com.digsarustudio.musa.widget.SaruTextBox;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class LabeledSaruTextBox extends Composite {

	private static LabeledSaruTextBoxUiBinder uiBinder = GWT.create(LabeledSaruTextBoxUiBinder.class);

	interface LabeledSaruTextBoxUiBinder extends UiBinder<Widget, LabeledSaruTextBox> {
	}
	
	
	@UiField Label			caption;
	@UiField SaruTextBox	textBox;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public LabeledSaruTextBox() {		
		initWidget(uiBinder.createAndBindUi(this));
	}
		
	public HandlerRegistration addChangeHandler(ChangeHandler handler){
		return this.textBox.addChangeHandler(handler);
	}
	
	public void setCaption(String caption){
		this.caption.setText(caption);
	}
	
	public void setText(String text){
		this.textBox.setText(text);
	}
	
	public String getText(){
		return this.textBox.getText();
	}
	
	public void setCaptionWidth(String width){
		this.caption.setWidth(width);
	}
	
	public void setTextBoxWidth(String width){
		this.textBox.setWidth(width);
	}
	
	public void setPlaceHolder(String text){
		this.textBox.setPlaceHolder(text);
	}
	
	public void setRequired(Boolean isRequired){
		this.textBox.setRequired(isRequired);
	}
	
	public void setReadOnly(Boolean isReadOnly){
		this.textBox.setReadOnly(isReadOnly);
	}	
	
	public void clear(){
		this.textBox.clear();
	}	
	
}
