/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import com.digsarustudio.musa.mvp.view.event.ClickEvent;
import com.digsarustudio.musa.mvp.view.event.ClickHandler;
import com.digsarustudio.musa.mvp.view.event.HasClickHandlers;
import com.digsarustudio.musa.widget.SaruRadioButton;
import com.digsarustudio.musa.widget.WidgetAlignment;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.logical.shared.HasValueChangeHandlers;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiChild;
import com.google.gwt.uibinder.client.UiConstructor;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.CellPanel;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.HasVerticalAlignment;
import com.google.gwt.user.client.ui.HorizontalPanel;
import com.google.gwt.user.client.ui.VerticalPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This composite handles multiple radio buttons in a group.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class SaruRadioButtonPanel extends Composite implements HasValueChangeHandlers<Boolean>
														 	,HasClickHandlers<String>,
														 	HasVerticalAlignment {
	Logger logger = Logger.getLogger(this.getClass().getName());

	private static SaruRadioButtonsUiBinder uiBinder = GWT.create(SaruRadioButtonsUiBinder.class);

	interface SaruRadioButtonsUiBinder extends UiBinder<Widget, SaruRadioButtonPanel> {
	}

	@UiField HTMLPanel								mainFrame;
	
	private CellPanel								panel;
	private String									groupName;
	private Map<String, SaruRadioButton>			buttons 			= null;
	private List<ValueChangeHandler<Boolean>>		valueChangeHandlers	= null;
	private List<ClickHandler<String>>				clickHandlers		= null;
	private Boolean									isReadOnly			= false;
	private VerticalAlignmentConstant 				mAlign;
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	@UiConstructor
	public SaruRadioButtonPanel(String groupName, WidgetAlignment alignment) {	
		initWidget(uiBinder.createAndBindUi(this));
		
		this.setGroupName(groupName);
		
		if(WidgetAlignment.HorizontalAlignment == alignment) {
			this.panel = new HorizontalPanel();
		}else{
			this.panel = new VerticalPanel();
		}
		
		this.panel.setWidth("100%");
		this.panel.setHeight("72%");
		
		this.mainFrame.add(this.panel);
	}

	@UiChild(tagname="option")
	public void addOption(final SaruRadioButton button) {		
		button.setName(this.groupName);
		button.setEnabled(!this.isReadOnly);
		button.addValueChangeHandler(new ValueChangeHandler<Boolean>() {

			@Override
			public void onValueChange(ValueChangeEvent<Boolean> event) {
				if( !event.getValue() ) {
					return;
				}
				
				onRadioButonValueChange(button.getOptionValue(), event);
			}
		});		
		button.addClickHandler(new com.google.gwt.event.dom.client.ClickHandler() {
			
			@Override
			public void onClick(com.google.gwt.event.dom.client.ClickEvent event) {
				onRadioButtonClick(button.getOptionValue());
				
			}
		});
		
		if(null == this.buttons) {
			this.buttons = new HashMap<>();
		}
		
		this.buttons.put(button.getOptionValue(), button);
		this.panel.add(button);	
		this.panel.setCellHeight(button, "24px");
		this.panel.setCellVerticalAlignment(button, this.getVerticalAlignment());
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
		
		if(null != this.buttons) {
			for (Map.Entry<String, SaruRadioButton> keyValueSet : this.buttons.entrySet()) {
				if(null == keyValueSet || null == keyValueSet.getValue()) {
					continue;
				}
				
				keyValueSet.getValue().setName(groupName);				
			}
		}
	}

	public String getValue() {
		if(null == this.buttons) {
			return null;
		}
		
		String value = null;
		for (Map.Entry<String, SaruRadioButton> keyValueSet : this.buttons.entrySet()) {
			if( !keyValueSet.getValue().getValue() ) {
				continue;
			}
			
			value = keyValueSet.getKey();
		}
		
		return value;
	}
	
	public void setValue(String value) {
		if(null == this.buttons) {
			return;
		}
		
		for (Map.Entry<String, SaruRadioButton> keyValueSet : this.buttons.entrySet()) {
			if( !value.equals(keyValueSet.getKey()) ) {
				continue;
			}
			
			keyValueSet.getValue().setValue(true);
		}
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.view.event.HasClickHandlers#addClickHandler(com.digsarustudio.musa.mvp.view.event.ClickHandler)
	 */
	@Override
	public HandlerRegistration addClickHandler(final ClickHandler<String> handler) {
		if(null == this.clickHandlers) {
			this.clickHandlers = new ArrayList<>();
		}
		this.clickHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				if( null == clickHandlers ) {
					return;
				}
				
				clickHandlers.remove(handler);
			}
		};
	}

	@Override
	public HandlerRegistration addValueChangeHandler(final ValueChangeHandler<Boolean> handler) {
		if(null == this.valueChangeHandlers) {
			this.valueChangeHandlers = new ArrayList<>();
		}
		this.valueChangeHandlers.add(handler);
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				if(null == valueChangeHandlers) {
					return;
				}
				
				valueChangeHandlers.remove(handler);
			}
		};
	}

	public void clear() {
		if(null == this.panel) {
			return;
		}
		
		this.panel.clear();
	}
	
	public void reset() {
		if(null == this.buttons) {
			return;
		}
		
		Iterator<SaruRadioButton> iterator = this.buttons.values().iterator();
		if(null == iterator || !iterator.hasNext()) {
			return;
		}
		
		SaruRadioButton button = iterator.next();
		if(null == button) {
			return;
		}
		
		button.setValue(true);
	}
	
	public void onRadioButonValueChange(String id, ValueChangeEvent<Boolean> event) {
		if(null == this.valueChangeHandlers) {
			return;
		}
		
		for (ValueChangeHandler<Boolean> handler : this.valueChangeHandlers) {
			handler.onValueChange(event);
		}
	}
	
	public void setReadOnly(Boolean isReadOnly) {
		this.isReadOnly = isReadOnly;
		
		if(null == this.buttons) {
			return;
		}
		
		for (Map.Entry<String, SaruRadioButton> keyValueSet : this.buttons.entrySet()) {
			if( null == keyValueSet.getValue() ) {
				continue;
			}
			
			keyValueSet.getValue().setEnabled(!isReadOnly);
		}
	}
	
	public void onRadioButtonClick(String id) {
		if(null == this.clickHandlers) {
			return;
		}
		
		for (ClickHandler<String> handler : this.clickHandlers) {
			handler.onClicked(new ClickEvent<String>(id));
		}
	}

	@Override
	public VerticalAlignmentConstant getVerticalAlignment() {
		return mAlign==null? ALIGN_TOP:mAlign;
	}

	@Override
	public void setVerticalAlignment(VerticalAlignmentConstant align) {
	
		this.mAlign = align;
	}
}
