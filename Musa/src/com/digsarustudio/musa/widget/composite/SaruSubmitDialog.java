/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

import com.digsarustudio.musa.widget.polymer.dialog.DialogCancelActionHandler;
import com.digsarustudio.musa.widget.polymer.dialog.DialogConfirmActionHandler;
import com.digsarustudio.musa.widget.polymer.dialog.DialogResetActionHandler;
import com.digsarustudio.musa.widget.polymer.dialog.HasDialogCancelActionHandler;
import com.digsarustudio.musa.widget.polymer.dialog.HasDialogConfirmActionHandler;
import com.digsarustudio.musa.widget.polymer.dialog.HasDialogResetActionHandler;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.shared.HandlerRegistration;
import com.google.gwt.resources.client.CssResource;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The dialog contains Confirm, Cancel, and Reset button.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruSubmitDialog extends DialogBox implements HasDialogConfirmActionHandler
														, HasDialogCancelActionHandler
														, HasDialogResetActionHandler{

	private static SaruSubmitDialogUiBinder uiBinder = GWT.create(SaruSubmitDialogUiBinder.class);

	interface SaruSubmitDialogUiBinder extends UiBinder<Widget, SaruSubmitDialog> {
	}
	
	interface Styling extends CssResource{
		
	}	
	
	@UiField		HTMLPanel 	canvas;
	
	private DialogConfirmActionHandler	confirmActionHandler	= null;
	private DialogCancelActionHandler	cancelActionHandler	= null;
	private DialogResetActionHandler		resetActionHandler	= null;
	
	/**
	 * 
	 */
	public SaruSubmitDialog() {
		setWidget(uiBinder.createAndBindUi(this));
		
		this.init();
	}

	/**
	 * @param autoHide
	 */
	public SaruSubmitDialog(boolean autoHide) {
		super(autoHide);

	}

	/**
	 * @param captionWidget
	 */
	public SaruSubmitDialog(Caption captionWidget) {
		super(captionWidget);

	}

	/**
	 * @param autoHide
	 * @param modal
	 */
	public SaruSubmitDialog(boolean autoHide, boolean modal) {
		super(autoHide, modal);

	}

	@UiHandler("submitBtn")
	protected void onSubmitButtonClickEvent(ClickEvent event) {
		if(null == this.confirmActionHandler) {
			return;
		}
		
		this.confirmActionHandler.onConfirm();
	}

	@UiHandler("cancelBtn")
	protected void onCancelButtonClickEvent(ClickEvent event) {
		if(null == this.cancelActionHandler) {
			return;
		}
		
		this.cancelActionHandler.onCancel();
		
		this.hide();
	}

	@UiHandler("resetBtn")
	protected void onResetButtonClickEvent(ClickEvent event) {
		if(null == this.resetActionHandler) {
			return;
		}
		
		this.resetActionHandler.onReset();
	}
	
	/**
	 * @param autoHide
	 * @param modal
	 * @param captionWidget
	 */
	public SaruSubmitDialog(boolean autoHide, boolean modal, Caption captionWidget) {
		super(autoHide, modal, captionWidget);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.dialog.HasDialogResetActionHandler#addDialogResetActionHandler(com.digsarustudio.musa.widget.polymer.dialog.DialogResetActionHandler)
	 */
	@Override
	public HandlerRegistration addDialogResetActionHandler(DialogResetActionHandler handler) {
		this.resetActionHandler = handler;
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				resetActionHandler = null;
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.dialog.HasDialogCancelActionHandler#addDialogCancelActionHandler(com.digsarustudio.musa.widget.polymer.dialog.DialogCancelActionHandler)
	 */
	@Override
	public HandlerRegistration addDialogCancelActionHandler(DialogCancelActionHandler handler) {
		this.cancelActionHandler = handler;
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				cancelActionHandler = null;
			}
		};
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.polymer.dialog.HasDialogConfirmActionHandler#addDialogConfirmActionHandler(com.digsarustudio.musa.widget.polymer.dialog.DialogConfirmActionHandler)
	 */
	@Override
	public HandlerRegistration addDialogConfirmActionHandler(DialogConfirmActionHandler handler) {
		this.confirmActionHandler = handler;
		
		return new HandlerRegistration() {
			
			@Override
			public void removeHandler() {
				confirmActionHandler = null;
			}
		};
	}

	private void init() {
		this.setGlassEnabled(true);
		this.setAnimationEnabled(true);
		
		this.setModal(true);
	}
}
