/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * The copy right widget
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Copyright extends Composite {
	private final String COPYRIGHT_SYMBO = "\u00A9";

	private static CopyrightUiBinder uiBinder = GWT.create(CopyrightUiBinder.class);

	interface CopyrightUiBinder extends UiBinder<Widget, Copyright> {
	}

	
	@UiField Label symbol;
	@UiField Label year;
	@UiField Label company;
	@UiField Label version;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public Copyright() {
		initWidget(uiBinder.createAndBindUi(this));
		
		this.init();
	}

	/**
	 * @param symbol the symbol to set
	 */
	public void setSymbol(String symbol) {
		this.symbol.setText(symbol);
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year.setText(year);
	}

	/**
	 * @param company the company to set
	 */
	public void setCompany(String company) {
		this.company.setText(company);
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version.setText(version);
	}

	private void init(){
		this.setSymbol("Copyright" + this.COPYRIGHT_SYMBO);
	}
}
