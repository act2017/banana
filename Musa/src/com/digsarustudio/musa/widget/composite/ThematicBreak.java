/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

import com.google.gwt.core.client.GWT;

import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This class wraps HR tag for GWT widget
 * 
 * Bug: The styling from parent widget doesn't not affect this composite.
 *      Using java code to solve it so far.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ThematicBreak extends Composite {
	private static ThematicBreakUiBinder uiBinder = GWT.create(ThematicBreakUiBinder.class);

	interface ThematicBreakUiBinder extends UiBinder<Widget, ThematicBreak> {
	}	
	
	@UiField HTMLPanel breakLine;
	
	/**
	 * Constructs a thematic break 
	 */
	public ThematicBreak() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setWidth(String width){
		this.breakLine.setWidth(width);
	}
	
	public void setColour(String colour){
		//The way to set a property of the styling
		this.breakLine.getElement().getStyle().setProperty("backgroundColor", colour);
	}
}
