/**
 * 
 */
package com.digsarustudio.musa.widget.composite.login;

import com.digsarustudio.musa.mvp.presenter.Presenter;

/**
 * The presenter who deals with 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LoginDialogPresenter extends Presenter {
	public enum LoginStatus{
		 None
		,WrongPassword
		,PasswordExpired
	}
	
	/**
	 * The interface for this presenter to mutate and access the target view. 
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Display {
		/**
		 * To clear the contents of this display
		 */
		void clear();

		/**
		 * To reset the contents of this display to default value
		 */
		void reset();
		
		/**
		 * The event handler method to tell user the login was failed.
		 * 
		 * @param message The message for the user to indicate what's going on the login.
		 */
		void onFailToLogin(String message);
		
		/**
		 * To show the dialog
		 */
		void showDialog();
		
		/**
		 * To hide the dialog
		 */
		void hideDialog();
		
		void showResetPasswordBtn();
		void hideResetPasswordBtn();
		
		void showForgotPasswordBtn();
		void hideForgotPasswordBtn();
		
		String getUserName();
		String getPassword();
		
		void setIsPasswordExpired();
		void setIsWrongPassword();
	}
	
	
	/**
	 * The user tries to log into the system.
	 * 
	 * @param userName The user name for the login
	 * @param password The password for the login
	 * 
	 */
	void onLogin(String userName, String password);
	
	void onCancel();
	
	void onResetPassword(String userName);
	void onForgotPassword(String userName);
	
	void setIsPasswordExpired();
	void setIsWrongPassword();
}
