/**
 * 
 */
package com.digsarustudio.musa.widget.composite.login;

import com.digsarustudio.musa.mvp.model.ViewModel;
import com.digsarustudio.musa.mvp.presenter.Presenter;
import com.digsarustudio.musa.mvp.view.SupervisingView;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyDownEvent;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.uibinder.client.UiHandler;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.Widget;

/**
 * A small widget for the user to trigger the login dialog
 * 
 * The call back interface might instead of the presenter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 *
 * @deprecated 1.0.1 Until the UI refactored as {@link LoginDialogView}
 */
public class LoginWidgetView extends Composite implements SupervisingView{

	private static LoginWidgetViewUiBinder uiBinder = GWT.create(LoginWidgetViewUiBinder.class);

	interface LoginWidgetViewUiBinder extends UiBinder<Widget, LoginWidgetView> {
	}
	
	@UiField Label button;
	
	private LoginWidgetPresenter presenter = null;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public LoginWidgetView() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	@UiHandler("button")
	protected void onButtonClickEvent(ClickEvent event) {
		this.presenter.onLogin();
	}
	
	@UiHandler("button")
	protected void onButtonKeyDownClickEvent(KeyDownEvent event) {
		if( event.getNativeKeyCode() != KeyCodes.KEY_ENTER ){
			return;
		}
		
		this.presenter.onLogin();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.view.SupervisingView#setPresenter(com.digsarustudio.musa.mvp.presenter.Presenter)
	 */
	@Override
	public void setPresenter(Presenter presenter) {
		this.presenter = (LoginWidgetPresenter) presenter;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.view.SupervisingView#setViewModel(com.digsarustudio.musa.mvp.model.ViewModel)
	 */
	@Override
	public void setViewModel(ViewModel model) {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.view.SupervisingView#clear()
	 */
	@Override
	public void clear() {

		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.view.SupervisingView#reset()
	 */
	@Override
	public void reset() {

		
	}
	
}
