/**
 * 
 */
package com.digsarustudio.musa.widget.composite.login;

import com.digsarustudio.musa.mvp.event.ShowLoginDialogEvent;
import com.digsarustudio.musa.mvp.model.DataModel;
import com.digsarustudio.musa.mvp.presenter.AbstractPresenter;
import com.digsarustudio.musa.mvp.presenter.Presenter;
import com.digsarustudio.musa.mvp.view.PassiveView;
import com.google.gwt.event.shared.EventBus;

/**
 * The implementation of login widget
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated 1.0.1 Until this refactored as {@link LoginDialogPresenter}
 */
public class DefaultLoginWidgetPresenter extends AbstractPresenter implements Presenter, LoginWidgetPresenter {

	/**
	 * @param eventBus
	 * @param model
	 * @param view
	 */
	public DefaultLoginWidgetPresenter(EventBus eventBus, DataModel model, PassiveView view) {
		super(eventBus, model, view);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginWidgetPresenter#onLogin()
	 */
	@Override
	public void onLogin() {
		this.triggerEvent(new ShowLoginDialogEvent());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.AbstractPresenter#init()
	 */
	@Override
	protected void init() {

	}

}
