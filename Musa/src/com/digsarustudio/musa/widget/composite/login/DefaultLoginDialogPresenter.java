/**
 * 
 */
package com.digsarustudio.musa.widget.composite.login;

import com.digsarustudio.musa.mvp.presenter.AbstractPresenter;
import com.digsarustudio.musa.mvp.presenter.Presenter;
import com.digsarustudio.musa.mvp.view.PassiveView;
import com.google.gwt.event.shared.EventBus;

/**
 * The implementation of the login presenter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DefaultLoginDialogPresenter extends AbstractPresenter implements Presenter, LoginDialogPresenter {

	private LoginDialogPresenter.Display display = null;
	
	private LoginCallback				loginCallback = null;
	
	public DefaultLoginDialogPresenter(PassiveView view) {
		super(null, view);
		
		this.display = (LoginDialogPresenter.Display)view;
	}
	
	/**
	 * @param eventBus
	 * @param model
	 * @param view
	 * 
	 * @deprecated This presenter use callback object to notify client code what's going on.
	 */
	public DefaultLoginDialogPresenter(EventBus eventBus, PassiveView view) {
		super(eventBus, view);
		
		this.display = (LoginDialogPresenter.Display)view;
	}

	/**
	 * @param loginCallback the loginCallback to set
	 */
	public void setLoginCallback(LoginCallback loginCallback) {
		this.loginCallback = loginCallback;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.LoginPresenter#onLogin(java.lang.String, java.lang.String)
	 */
	@Override
	public void onLogin(String userName, String password) {
		if(null == this.loginCallback) {
			return;
		}
		
		this.loginCallback.onLogin(userName, password);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#onCancel()
	 */
	@Override
	public void onCancel() {
		if(null == this.loginCallback) {
			return;
		}
		
		this.loginCallback.onCancel();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#onResetPassword(java.lang.String)
	 */
	@Override
	public void onResetPassword(String userName) {
		if(null == this.loginCallback) {
			return;
		}
	
		this.loginCallback.onResetPassword(userName);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#onForgotPassword(java.lang.String)
	 */
	@Override
	public void onForgotPassword(String userName) {
		if(null == this.loginCallback) {
			return;
		}
		
		this.loginCallback.onForgetPassword(userName);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#setIsPasswordExpired()
	 */
	@Override
	public void setIsPasswordExpired() {
		this.display.setIsPasswordExpired();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.widget.composite.login.LoginDialogPresenter#setIsWrongPassword()
	 */
	@Override
	public void setIsWrongPassword() {
		this.display.setIsWrongPassword();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.AbstractPresenter#init()
	 */
	@Override
	protected void init() {
		this.display.reset();
		this.display.showDialog();
	}
}
