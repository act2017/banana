/**
 * 
 */
package com.digsarustudio.musa.widget.composite.login;

import com.digsarustudio.musa.mvp.presenter.Presenter;

/**
 * The presenter who deals with the login event
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated 1.0.1 Until this refactored as {@link LoginDialogPresenter}
 */
public interface LoginWidgetPresenter extends Presenter {
	/**
	 * The user tries to log into the system.
	 * 
	 * 
	 */
	void onLogin();
}
