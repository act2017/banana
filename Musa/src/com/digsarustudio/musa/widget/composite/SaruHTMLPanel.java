/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

import com.google.gwt.core.client.GWT;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * This composite wraps the HTML panel for the plain container to use.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SaruHTMLPanel extends Composite {

	private static SaruHTMLPanelUiBinder uiBinder = GWT.create(SaruHTMLPanelUiBinder.class);

	interface SaruHTMLPanelUiBinder extends UiBinder<Widget, SaruHTMLPanel> {
	}

	@UiField	HTMLPanel container;
	
	/**
	 * Because this class has a default constructor, it can
	 * be used as a binder template. In other words, it can be used in other
	 * *.ui.xml files as follows:
	 * <ui:UiBinder xmlns:ui="urn:ui:com.google.gwt.uibinder"
	  *   xmlns:g="urn:import:**user's package**">
	 *  <g:**UserClassName**>Hello!</g:**UserClassName>
	 * </ui:UiBinder>
	 * Note that depending on the widget that is used, it may be necessary to
	 * implement HasHTML instead of HasText.
	 */
	public SaruHTMLPanel() {
		initWidget(uiBinder.createAndBindUi(this));
	}

	public void setWidth(String width){
		this.container.setWidth(width);
	}
	
	public void setHeight(String height){
		this.container.setHeight(height);
	}
	
	public void addWidget(Widget widget){
		this.container.add(widget);
	}
}
