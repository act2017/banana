/**
 * 
 */
package com.digsarustudio.musa.widget.composite.list;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.widget.HasChangeHandlers;
import com.digsarustudio.musa.widget.polymer.HasDeleteHandler;
import com.google.gwt.user.client.ui.IsWidget;

/**
 * The sub-type can be added into {@link ListPanel} and cooperating with it.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public interface ListItemPanel<T> extends HasDeleteHandler
									   , HasChangeHandlers
									   , IsWidget {
	/**
	 * 
	 * The builder for {@link ListItemPanel}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder<S> extends ObjectBuilder<ListItemPanel<S>>{
	
	}
	
	void setSourceData(T source);
	T getSourceData();
	
	/**
	 * Returns T if the value has been set or changed, otherwise null returned to indicate that 
	 * there is nothing different.<br>
	 * 
	 * @return T if the value has been set or changed, otherwise null returned to indicate that 
	 * 			 there is nothing different.
	 */
	T getValue();
}
