/**
 * 
 */
package com.digsarustudio.musa.widget.composite;

/**
 * This exception represents that the target queue is full
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class QueueFullException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5578617987051882347L;

	/**
	 * 
	 */
	public QueueFullException() {

	}

	/**
	 * @param arg0
	 */
	public QueueFullException(String arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 */
	public QueueFullException(Throwable arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public QueueFullException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public QueueFullException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

}
