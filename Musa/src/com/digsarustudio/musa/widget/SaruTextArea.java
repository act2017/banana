/**
 * 
 */
package com.digsarustudio.musa.widget;

import com.google.gwt.dom.client.Element;
import com.google.gwt.user.client.ui.TextArea;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 * @deprecated	1.0.3	Please use Musaceae-1.0.4 instead
 */
public class SaruTextArea extends TextArea {

	/**
	 * 
	 */
	public SaruTextArea() {

	}

	/**
	 * @param element
	 */
	public SaruTextArea(Element element) {
		super(element);

	}

	/**
	 * Set the place holder to the custom text box.
	 * 
	 * @param text The text for place holder as a hint
	 */
	public void setPlaceholder(String text){
		String placeholder = (null != text) ? text : "";
		
		this.getElement().setPropertyString("placeholder", placeholder);
	}
}
