/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event indicates that the app has to show an error message widget to notify the users.<br>
 * Due to this event calls main layout view to show toaster and it must be embedded in a view, 
 * please call this event in the module executed by {@link EntryPoint}. Do not use this event with shared {@link EventBus}.<br>
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * 
 *
 */
public class ShowErrorMessageEvent extends GwtEvent<ShowErrorMessageEvent.Handler> {
	/**
	 * 
	 * Event handler to handle {@link ShowErrorMessageEvent}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler {
		/*
		 * To handle {@link ShowInfoMessageEvent}
		 */
		void onShowInfoMessage(ShowErrorMessageEvent event);
	}
	
	public static Type<Handler> TYPE = new Type<>();
	
	private String msg = null;

	/**
	 * Constructs a {@link ShowErrorMessageEvent} with a message
	 * 
	 * @param msg The message to carry on
	 */
	public ShowErrorMessageEvent(String msg){
		this.setMsg(msg);
	}
	
	/**
	 * Returns the message of the event
	 * 
	 * @return the message of the event
	 */
	public String getMessage() {
		return msg;
	}

	/**
	 * Assigns a message for this event
	 * 
	 * @param msg the message to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}

	/**
	 * To register this event into an event bus
	 * 
	 * @param eventbus The event bus to register
	 * 
	 * @param handler The event handler of this event
	 */
	public static void register(EventBus eventbus, ShowErrorMessageEvent.Handler handler) {
		eventbus.addHandler(TYPE, handler);
	}

	/**
	 * To fire event with event bus
	 * 
	 * @param eventBus The event handler between event sender and receiver
	 * @param message The message to carry
	 */
	public static void fireEvent(EventBus eventBus, String message) {
		ShowErrorMessageEvent event = new ShowErrorMessageEvent(message);
		//The parameters can be set when the event has been initiated.

		eventBus.fireEvent(event);
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onShowInfoMessage(this);
	}
}
