/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * Defining the operations of event handler for log out event 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface LogOutEventHandler extends EventHandler{
	void onLogOutEvent(LogOutEvent event);
}
