/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class OnAttachEvent extends GwtEvent<OnAttachEventHandler> {
	public static Type<OnAttachEventHandler> TYPE = new Type<>();
	
	private Object data;
	
	public OnAttachEvent(){
		
	}
	
	public OnAttachEvent(Object data){
		this.data = data;
	}
	
	public Object getData(){
		return this.data;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<OnAttachEventHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(OnAttachEventHandler handler) {
		handler.onAttach(this);
	}

}
