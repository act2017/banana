/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Calling this event to notify controller to switch the view for the end-user or administrator
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LoginEvent extends GwtEvent<LoginEvent.Handler> {
	/**
	 * 
	 * Event handler of listing products
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler{
		/*
		 * Login event triggered
		 */
		void onLogin(LoginEvent event);
	}
	
	@Deprecated
	private Object certification;
	
	private String account	= null;
	private String password	= null;
	
	public static Type<Handler> TYPE = new Type<>();
	
	public LoginEvent() {
		super();
	}
	
	public LoginEvent(String account, String password) {
		this.account = account;
		this.password = password;
	}
	
	/**
	 * @return the account
	 */
	protected String getAccount() {
		return account;
	}

	/**
	 * @param account the account to set
	 */
	protected void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the password
	 */
	protected String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	protected void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Constructs a product listing event
	 * 
	 * @param certification The certification for the search
	 * @deprecated
	 */
	public LoginEvent(Object certification) {
		super();
		this.certification = certification;
	}
	
	/**
	 * Returns the certification for the search
	 * 
	 * @return the certification for the search
	 * @deprecated
	 */
	public Object getCertification() {
		return certification;
	}

	/**
	 * Assign a certification for the app controller to switch view
	 * 
	 * @param certification the certification to set
	 * @deprecated
	 */
	public void setCertification(Object certification) {
		this.certification = certification;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onLogin(this);
	}
}
