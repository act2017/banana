/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * This hadler deals with the deleting event
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface HasDeleteHandler {
	/**
	 * To add a deleting event handler
	 * 
	 * @param handler The deleting event handler
	 * @return The registration of handler 
	 */
	HandlerRegistration addDeleteEventHandler(DeleteEventHandler handler);
}
