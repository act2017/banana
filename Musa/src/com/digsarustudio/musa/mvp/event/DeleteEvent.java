/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.GwtEvent;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class DeleteEvent extends GwtEvent<DeleteEventHandler> {
	public static Type<DeleteEventHandler> TYPE = new Type<>();
	
	private Object data;
	
	public DeleteEvent(){
		
	}
	
	public DeleteEvent(Object data){
		this.data = data;
	}
	
	/**
	 * Returns the data object sent by event holder.<br>
	 * Please call this method to retrieve the source data from the event holder.<br>
	 * 
	 * @return The data object sent by event holder.
	 */
	public Object getData(){
		return this.data;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<DeleteEventHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(DeleteEventHandler handler) {
		handler.onDelete(this);
	}

}
