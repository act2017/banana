/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.UIObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class UpdateEvent extends GwtEvent<UpdateEventHandler> {
	public static Type<UpdateEventHandler> TYPE = new Type<>();
	
	public UpdateEvent(){
		
	}
	
	public UpdateEvent(UIObject source){
		this.setSource(source);
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<UpdateEventHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(UpdateEventHandler handler) {
		handler.onUpdate(this);
	}

}
