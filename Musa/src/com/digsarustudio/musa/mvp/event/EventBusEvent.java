/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.digsarustudio.musa.mvp.presenter.Presenter;
import com.digsarustudio.musa.mvp.controller.AppController;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * The subclass of this type is working as the event transited between {@link AppController} and {@link Presenter} 
 * via {@link EventBus}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public abstract class EventBusEvent<H extends EventHandler> extends GwtEvent<H> {

}
