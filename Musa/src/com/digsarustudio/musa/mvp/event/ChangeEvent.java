/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.UIObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class ChangeEvent extends GwtEvent<ChangeEventHandler> {
	public static Type<ChangeEventHandler> TYPE = new Type<>();

	private Integer id;
	private UIObject data;
	
	public ChangeEvent(){
		
	}
	
	public ChangeEvent(Integer id){
		this.id = id;
	}
	
	public ChangeEvent(UIObject data){
		this.data = data;
	}
	
	
	public Integer getId(){
		return this.id;
	}
	
	public UIObject getData(){
		return this.data;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<ChangeEventHandler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(ChangeEventHandler handler) {
		handler.onChange(this);
	}

}
