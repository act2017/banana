/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This event indicates that to show the home page of the main web app.<br>
 * It is used to go back the web app home page from back-end console or other module to use.<br>
 * If you are going to go back to the main page of back-end console, please use the event the back-end console provides.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ShowWebAppHomePageEvent extends GwtEvent<ShowWebAppHomePageEvent.Handler> {
	/**
	 * 
	 * Event handler to handle ShowWebAppHomePageEvent
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler {
		/*
		 * To handle ShowWebAppHomePageEvent
		 */
		void onShowWebAppHomePage(ShowWebAppHomePageEvent event);
	}

	public static Type<ShowWebAppHomePageEvent.Handler> TYPE = new Type<>();

	/**
	 * To register this event into an event bus
	 * 
	 * @param eventbus The event bus to register
	 * 
	 * @param handler The event handler of this event
	 */
	public static void register(EventBus eventbus, ShowWebAppHomePageEvent.Handler handler) {
		eventbus.addHandler(TYPE, handler);
	}

	/**
	 * To fire event with event bus
	 * 
	 * @param eventBus The event handler between event sender and receiver
	 * @param others The other parameters if needs.
	 */
	public static void fireEvent(EventBus eventBus) {
		ShowWebAppHomePageEvent event = new ShowWebAppHomePageEvent();
		//The parameters can be set when the event has been initiated.

		eventBus.fireEvent(event);
	}
	
	/**
	 * 
	 */
	public ShowWebAppHomePageEvent() {
		
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public Type<Handler> getAssociatedType() {
		return TYPE;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onShowWebAppHomePage(this);
		
	}

}
