/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * This exception event wraps {@link GwtEvent} for the client code to send an event to notify controller there is an exception happened and 
 * the controller has to handle it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 * @deprecated	1.0.2	Please Musaceae-1.0.3 instead
 */
public abstract class ExceptionEvent<H extends EventHandler> extends GwtEvent<H> {

}
