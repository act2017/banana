/**
 * 
 */
package com.digsarustudio.musa.mvp.event;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;

/**
 * Calling this event to notify controller to show login dialog
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class ShowLoginDialogEvent extends GwtEvent<ShowLoginDialogEvent.Handler> {
	/**
	 * 
	 * Event handler of listing products
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public interface Handler extends EventHandler{
		/*
		 * To show login dialog
		 */
		void onShowLoginDialog(ShowLoginDialogEvent event);
	}
	
	private Object data;
	
	public static Type<Handler> TYPE = new Type<>();
	
	public ShowLoginDialogEvent() {
		super();
	}
	
	/**
	 * Constructs a product listing event
	 * 
	 * @param data The data for the search
	 */
	public ShowLoginDialogEvent(Object data) {
		super();
		this.data = data;
	}
	
	/**
	 * Returns the data
	 * 
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * Assign a data for the app controller to switch view
	 * 
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#getAssociatedType()
	 */
	@Override
	public com.google.gwt.event.shared.GwtEvent.Type<Handler> getAssociatedType() {
		return TYPE;
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.shared.GwtEvent#dispatch(com.google.gwt.event.shared.EventHandler)
	 */
	@Override
	protected void dispatch(Handler handler) {
		handler.onShowLoginDialog(this);
	}
}
