/**
 * 
 */
package com.digsarustudio.musa.mvp.model;

/**
 * The data model for the view
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	Please use Musaceae instead.
 *
 */
public interface ViewModel {
	
}
