/**
 * 
 */
package com.digsarustudio.musa.mvp;

import com.digsarustudio.musa.mvp.controller.AppController;
import com.digsarustudio.musa.mvp.event.ShowErrorMessageEvent;
import com.digsarustudio.musa.mvp.event.ShowInfoMessageEvent;
import com.digsarustudio.musa.mvp.event.ShowWarningMessageEvent;
import com.google.gwt.event.shared.EventBus;
import com.vaadin.polymer.paper.widget.PaperToast;

/**
 * A singleton message handler passes message to the {@link AppController} via {@link EventBus}.
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * 
 * @deprecated	1.0.15	The controller should handle its own message event because {@link PaperToast} only can be 
 * 						embedded into its own main view.<br>
 *
 */
public class Messenger {
	private static Messenger INSTANCE = null;
	
	private EventBus eventBus = null;
	
	/**
	 * 
	 */
	private Messenger(EventBus eventBus) {
		this.eventBus = eventBus;
	}

	/**
	 * Please call this method to initiate the event bus for this messenger to use
	 * @param eventBus
	 */
	public static void init(EventBus eventBus){
		if( null != INSTANCE){
			return;
		}
		
		INSTANCE = new Messenger(eventBus);
	}
	
	public static void showErrorMessage(String message){
		ShowErrorMessageEvent.fireEvent(INSTANCE.eventBus, message);
	}
	
	public static void showInfoMessage(String message){
		ShowInfoMessageEvent.fireEvent(INSTANCE.eventBus, message);
	}
	
	public static void showWarningMessage(String message){
		ShowWarningMessageEvent.fireEvent(INSTANCE.eventBus, message);
	}
	
	public static void showExceptionMessage(Throwable exception){
		String msg = Messenger.getExceptionMessage(exception);
		Messenger.showErrorMessage(msg);
	}
	
	public static String getExceptionMessage(Throwable exception){
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(exception.getMessage());
		
		Throwable cause = exception.getCause();
		while(null != cause){
			buffer.append("\nCause:\n");
			buffer.append(cause.getMessage());
			
			cause = cause.getCause();
		}		
		
		return buffer.toString();
	}
	
	protected static void printErrorMessage(String msg){
		System.err.println(msg);
	}
	
	protected static void printInfoMessage(String msg){
		System.out.println(msg);
	}
}
