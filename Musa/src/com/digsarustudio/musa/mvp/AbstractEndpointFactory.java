/**
 * 
 */
package com.digsarustudio.musa.mvp;

import com.digsarustudio.banana.designpattern.abstractfactory.GenericAbstractFactory;
import com.digsarustudio.musa.endpoint.Endpoint;

/**
 * The sub-type of factory generates the particular {@link Endpoint} for the system.<br>
 * The methods must be defined in the sub-type depending on its purpose.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface AbstractEndpointFactory extends GenericAbstractFactory{

}
