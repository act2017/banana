/**
 * 
 */
package com.digsarustudio.musa.mvp.view;

import com.digsarustudio.musa.mvp.model.ViewModel;
import com.digsarustudio.musa.mvp.presenter.Presenter;

/**
 * In supervising view, it can fetch data from the model via a binding interface.
 * If you are considering to use UI Binder to implement the user interface, you might choose this interface.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.2
 * @since		1.0.0
 *
 * @deprecated	Please use Musacea instead
 */
public interface SupervisingView extends PassiveView{
	/**
	 * Assign a presenter for the view to handle the user events
	 * 
	 * @param presenter handling user events
	 */
	void setPresenter(Presenter presenter);
	
	/**
	 * The data model for the view to update itself
	 * 
	 * @param model the data model for the view to update itself
	 * 
	 * @deprecated 1.0.15 No longer to use.
	 */
	void setViewModel(ViewModel model);
	
	/**
	 * To clear the data in this view
	 * 
	 * @since 1.0.1
	 */
	void clear();
	
	/**
	 * To rest the data to the default in this view
	 * 
	 * @since 1.0.1
	 */
	void reset();
}
