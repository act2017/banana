/**
 * 
 */
package com.digsarustudio.musa.mvp.view;

import com.digsarustudio.musa.mvp.AbstractDataModelBuilderSetFactory;

/**
 * The sub-type of this {@link FactoryView} can be set a {@link AbstractDataModelBuilderSetFactory} for the data model object generation.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public interface FactoryView extends SupervisingView {
	void setDataModelBuilderSetFactory(AbstractDataModelBuilderSetFactory factory);
}
