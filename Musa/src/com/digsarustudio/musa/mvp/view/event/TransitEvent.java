/**
 * 
 */
package com.digsarustudio.musa.mvp.view.event;

import com.digsarustudio.musa.mvp.controller.AppController;
import com.digsarustudio.musa.mvp.event.EventBusEvent;
import com.digsarustudio.musa.mvp.presenter.Presenter;
import com.digsarustudio.musa.mvp.view.PassiveView;

/**
 * The subclass of this {@link TransitEvent} has the ability to carry a source data for the client code to 
 * handle an event passed from a {@link PassiveView} widget.<br>
 * Normally, this type of event only used between {@link Presenter} and {@link PassiveView}.<br>
 * If you wanna pass the event to the {@link AppController}, please use the subclass of {@link EventBusEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public abstract class TransitEvent<T>{
	private T sourceData = null;
	
	/**
	 * 
	 */
	public TransitEvent() {
	}

	/**
	 * @return the source data
	 */
	public T getSourceData() {
		return sourceData;
	}

	/**
	 * @param source the source to set
	 */
	public void setSourceData(T source) {
		this.sourceData = source;
	}

}
