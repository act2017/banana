/**
 * 
 */
package com.digsarustudio.musa.mvp.view.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * This sub-type has the ability to handle {@link ClickEvent} and notify the client call.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public interface HasClickHandlers<T> {
	HandlerRegistration addClickHandler(final ClickHandler<T> handler);
}
