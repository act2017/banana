/**
 * 
 */
package com.digsarustudio.musa.mvp.view.event;

import com.google.gwt.event.shared.EventHandler;

/**
 * This handler handles the event from {@link ValueChangeEvent}.<br>
 *
 * @param T The data type carried by {@link ValueChangeEvent}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public interface ValueChangeEventHandler<T> extends EventHandler{
	void onValueChanged(ValueChangeEvent<T> event);
}
