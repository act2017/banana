/**
 * 
 */
package com.digsarustudio.musa.mvp.view.event;

import com.google.gwt.event.shared.HandlerRegistration;

/**
 * The sub-type can be add an event handler which handles {@link ValueChangeEvent}.<br>
 *
 * @param T The data type carried by {@link ValueChangeEvent}.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public interface HasValueChangeEventHandlers<T> {
	HandlerRegistration addValueChangeEventHandler(ValueChangeEventHandler<T> handler);
}
