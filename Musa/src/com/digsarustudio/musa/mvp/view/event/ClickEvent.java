/**
 * 
 */
package com.digsarustudio.musa.mvp.view.event;

/**
 * This event triggered while a user tries to click a button.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public class ClickEvent<T> extends TransitEvent<T> {

	/**
	 * 
	 */
	public ClickEvent() {
	}
	
	public ClickEvent(T source) {
		this.setSourceData(source);
	}
}
