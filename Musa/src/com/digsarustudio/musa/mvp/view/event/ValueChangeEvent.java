/**
 * 
 */
package com.digsarustudio.musa.mvp.view.event;

/**
 * This event indicates the value of a widget has been changed and handles by {@link ValueChangeEventHandler}.<br>
 *
 * @param T The data type to carry.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public class ValueChangeEvent<T> extends TransitEvent<T>{
	
	public ValueChangeEvent(T source) {
		this.setSourceData(source);
	}	
}
