/**
 * 
 */
package com.digsarustudio.musa.mvp.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.musa.mvp.AbstractFactoryCollection;
import com.digsarustudio.musa.mvp.history.HistoryToken;
import com.digsarustudio.musa.mvp.history.HistoryTokenCollection;
import com.digsarustudio.musa.mvp.history.HistoryTokenSet;
import com.digsarustudio.musa.mvp.history.state.HistoryState;
import com.digsarustudio.musa.mvp.history.state.HistoryStateBus;
import com.digsarustudio.musa.mvp.history.state.HistoryStateHandler;
import com.digsarustudio.musa.mvp.view.PassiveView;
import com.digsarustudio.musa.utility.NoSuchValueException;
import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent.Type;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The base class of app controller
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead.
 */
public abstract class AbstractAppController implements AppController
														, ValueChangeHandler<String>/* @deprecated 1.0.8*/ {
	private Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 * The event bus used to control the events between the presenter
	 */
	protected EventBus eventBus;
	
	protected AbstractFactoryCollection factoryCollection = null;
	
	/**
	 * The main container used to be a root panel.
	 */
	protected HasWidgets mainContainer;
	
	/**
	 * The table of history states
	 * 
	 * @deprecated 1.0.8
	 */
	protected Map<String, HistoryStateHandler> historyStateHandlerMap = new HashMap<String, HistoryStateHandler>();
	
	/**
	 * The other persistent view 
	 */
	@Deprecated
	private Map<Class<? extends HistoryState>, PassiveView> persistentViewsMusa = new HashMap<>();
	@Deprecated
	private Map<Class<? extends PassiveView>, PassiveView>	viewsMusa	= new HashMap<>();
		
	/**
	 * Constructs the app controller
	 * 
	 * @param eventBus The event handler
	 */
	public AbstractAppController(EventBus eventBus) {
		this.eventBus = eventBus;
		
		this.init();
		this.bind();
	}
	
	/**
	 * 
	 */
	public AbstractAppController(EventBus eventBus, AbstractFactoryCollection factory) {
		this.eventBus = eventBus;
		this.factoryCollection = factory;
		
		this.init();
		this.bind();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.tutorial.google.web.tooltkits.twoeight.client.AppController#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		this.mainContainer = container;
		this.mainContainer.clear();
		
		this.buildMainLayout();
	}
	
	/* (non-Javadoc)
	 * @see com.google.gwt.event.logical.shared.ValueChangeHandler#onValueChange(com.google.gwt.event.logical.shared.ValueChangeEvent)
	 */
	@Override
	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();
		
		HistoryTokenCollection collection = null;
		
		try {
			collection = new HistoryTokenSet(token);
			this.executeHistoryState(collection);
			
		} catch (InvalidTokenFormatException | NoSuchValueException e) {
			logger.severe(e.getMessage() + "\nGo back to home page");
		}
		
	}

	/**
	 * To append a token into the history and trigger the onValueChange event after it.
	 * 
	 * @param tokens The token for state of the history
	 * 
	 * @deprecated 1.0.4. Please call {@link HistoryState.fireState()} instead.
	 */
	protected void triggerNewHistory(HistoryTokenCollection tokens){
		History.newItem(tokens.toString());
	}
	
	/**
	 * To append a token into the history without triggering the onValueChange event.
	 * 
	 * @param tokens The token for state of the history
	 * 
	 * @deprecated 1.0.4. Please call {@link HistoryState.setState()} instead.
	 */
	protected void appendNewHistory(HistoryTokenCollection tokens){
		History.newItem(tokens.toString(), false);
	}
	
	/**
	 * Register this controller as the on value change event handler to History
	 * 
	 * Please implement this function in the sub-class in order to register the event handler to the event bus.
	 */
	protected void bind(){
		/**
		 * @deprecated 1.0.7
		 */
//		History.addValueChangeHandler(this);
		
		this.bindEvents();
		this.bindHistoryState();
	}
	
	/**
	 * To register an event and its handler to the event bus
	 * 
	 * @param type The type of GWT Event
	 * @param handler The event handler of the target event
	 */
	protected <H extends EventHandler> void registerEventHandler(Type<H> type, H handler) {
		this.eventBus.addHandler(type, handler);
	}	
	
	/**
	 * 
	 * @param historyToken
	 * @param handler
	 * 
	 * @deprecated 1.0.4. Please call {@link HistoryStateBus.addHistoryStateHandler()} instead.
	 * 					  Or call {@link this.registerHistoryStateHandler()} instead.
	 */
	protected void addHistoryStateHandler(String historyToken, HistoryStateHandler handler) {
//		this.historyStateHandlerMap.put(historyToken, handler);
		this.registerHistoryStateHandler(historyToken, handler);
	}
	
	/**
	 * To register a history state handler for the state changing notification
	 * 
	 * @param state The target state
	 * @param handler The handler which deals with the operation when this state occurred.
	 * 
	 * @since 1.0.5
	 */
	protected void registerHistoryStateHandler(String state, HistoryStateHandler handler){
		HistoryStateBus.addHistoryStateHandler(state, handler);
	}
	
	/**
	 * To execute a history state
	 * 
	 * @param tokens The parameters to be executed in this state
	 * 
	 * @throws NoSuchValueException When the STATE token cannot be found.
	 * @throws NoSuchElementException when the history token stack doesn't contain TOKEN_STATE.
	 * @throws NullPointerException when this history state refers to a null pointer.
	 * 
	 * @deprecated 1.0.5
	 */
	protected void executeHistoryState(HistoryTokenCollection tokens) throws NoSuchValueException{
		HistoryToken tokenState = tokens.getToken(HistoryState.TOKEN_STATE);
		if( null == tokenState ){
			throw new NoSuchValueException("The STATE token is not existed");
		}
		
		String tokenValue = tokens.getToken(HistoryState.TOKEN_STATE).getValue();
		
		if( !this.historyStateHandlerMap.containsKey(tokenValue) ){
			throw new NoSuchElementException("The token[" + tokenValue + "] is not valid.");
		}
		
		HistoryStateHandler state= this.historyStateHandlerMap.get(tokenValue);
		if( null == state ){
			throw new NullPointerException("The token[" + tokenValue +"] refers to a null pointer.");
		}
				
		state.execute(tokens);
	}
	
	/**
	 * Returns the token stack of current state
	 * 
	 * @return The token stack of current state
	 * @throws InvalidTokenFormatException Throws if the current token is not in a valid token format
	 * 
	 * @deprecated 1.0.4 Please use {@link HistoryStateBus.getCurrentState()} instead.
	 * 						 Or use {@link this.getCurrentHistoryStateTokens()} instead. 
	 */
	protected HistoryTokenCollection getCurrentState() throws InvalidTokenFormatException {
//		String currentToken = History.getToken();
//		
//		return new HistoryTokenSet(currentToken);		
		
		return this.getCurrentHistoryStateTokens();
	}
	
	/**
	 * Returns the tokens of current history state
	 * 
	 * @return the tokens of current history state
	 * @throws InvalidTokenFormatException When the current token is not in a valid format.
	 * 
	 * @since 1.0.5
	 */
	protected HistoryTokenCollection getCurrentHistoryStateTokens() throws InvalidTokenFormatException{
		return HistoryStateBus.getCurrentState();
	}
	
	/**
	 * To add a persistent view called by a particular state
	 * 
	 * @param state The state operating the view
	 * @param view The view to be handled by the specific event
	 * 
	 * TODO going to be deprecated
	 */
	protected void addPersistentView(Class<? extends HistoryState> state, PassiveView view){
		if( this.persistentViewsMusa.containsKey(state) ){
			throw new IllegalArgumentException("Duplicated regsitering for view[" + view.getClass().getName() + "].");
		}
		
		this.persistentViewsMusa.put(state, view);
	}
	
	/**
	 * Returns a persistent view called by a particular state
	 * 
	 * @param state The state operating the view
	 * 
	 * @throws NoSuchValueException When the expecting view was not binded with the target event
	 * 
	 * @return The binded view for the target event
	 */
	@SuppressWarnings("unchecked")
	protected <T> T getPersistentView(Class<? extends HistoryState> state) throws NoSuchValueException{
		if( !this.persistentViewsMusa.containsKey(state) ){
			throw new NoSuchValueException("No match view for event " + state.toString() + ".");
		}
		
		return (T) this.persistentViewsMusa.get(state);
	}	
	
	/**
	 * Returns a view stored in the app controller
	 * 
	 * @param viewClass The class of the view
	 * 
	 * @return a view stored in the app controller
	 * 
	 * @throws NoSuchValueException The specific view cannot be found.
	 */
	@SuppressWarnings("unchecked")
	protected <T> T getView(Class<? extends PassiveView> viewClass) throws NoSuchValueException {
		if( !this.viewsMusa.containsKey(viewClass) ){
			throw new NoSuchValueException("No match view for view " + viewClass.toString() + ".");
		}
		
		return (T) this.viewsMusa.get(viewClass);
	}
	
	/**
	 * To add a view called in the app controller
	 * 
	 * @param state The state operating the view
	 * @param view The view to be handled by the specific event
	 */
	protected void addView(Class<? extends PassiveView> viewClass, PassiveView view){
		if( this.viewsMusa.containsKey(viewClass) ){
			throw new IllegalArgumentException("Duplicated regsitering for view[" + view.getClass().getName() + "].");
		}
		
		this.viewsMusa.put(viewClass, view);
	}
	
	@Deprecated
	protected AbstractFactoryCollection getEndpointSetFactory() {
		return this.factoryCollection;
	}
	
	protected AbstractFactoryCollection getFactoryCollection() {
		return this.factoryCollection;
	}

	/**
	 * To bind the events used in event bus
	 */
	protected abstract void bindEvents();
	
	/**
	 * To bind the history state used when the state of history changed
	 * 
	 * Please bind the history state with {@link HistoryStateBus.addHistoryStateHandler()}
	 */
	protected abstract void bindHistoryState();
	
	/**
	 * To initialize this app controller
	 */
	protected abstract void init();
	
	/**
	 * To build main layout. Please create the main layout object in this function 
	 * if the main layout has to be created dynamically.
	 */
	protected abstract void buildMainLayout();
	
	/**
	 * Returns the history states this controller handles for the client code to pass history state and 
	 * handles the unknown states.<br>
	 * 
	 * TODO do this when the user panel/console has been accomplished.<br>
	 * 
	 * @return Returns the history states this controller handles for the client code to pass history state and 
	 * 			handles the unknown states.
	 */
//	protected abstract List<HistoryState> getHistoryStates();
	
	/**
	 * To check the current token has been set or not.
	 * If the token is not set, showing the main page. Otherwise showing page executed in current state.
	 * 
	 * @deprecated 1.0.4 Please use {@link HistoryStateBus.checkCurrentState()} instead.  
	 */
	@SuppressWarnings("unused")
	private void checkCurrentState(){
		if( History.getToken().equals("") ){
			//Do nothing, let client code decides what has to be done. 
		}else{
			History.fireCurrentHistoryState();
		}				
	}
}
