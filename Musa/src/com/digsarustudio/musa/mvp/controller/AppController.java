package com.digsarustudio.musa.mvp.controller;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The definition of app controller
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead.
 *
 */
public interface AppController {
	void go(HasWidgets container);
}
