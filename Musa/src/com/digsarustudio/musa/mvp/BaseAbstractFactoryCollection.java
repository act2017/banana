/**
 * 
 */
package com.digsarustudio.musa.mvp;

/**
 * The base class of {@link AbstractFactoryCollection}.<br>
 * The {@link #endpointSetFactory} and {@link #dataModelBuilderFactory} must be initiated in the 
 * overridden {@link #getEndpointSetFactory()} and {@link #getDataModelBuilderFactory()} in the sub-class. 
 * After the {@link #endpointSetFactory} and {@link #dataModelBuilderFactory} have been initiated, 
 * call super{@link #getEndpointSetFactory()} to return the stored factory for the client code.<br>  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 */
public abstract class BaseAbstractFactoryCollection implements AbstractFactoryCollection {
	protected AbstractEndpointSetFactory			endpointSetFactory		= null;
	protected AbstractDataModelBuilderSetFactory	dataModelBuilderFactory	= null;
	
	/**
	 * 
	 */
	public BaseAbstractFactoryCollection() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.AbstractFactoryCollection#getEndpointSetFactory()
	 */
	@Override
	public AbstractEndpointSetFactory getEndpointSetFactory() {

		return this.endpointSetFactory;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.AbstractFactoryCollection#getDataModelBuilderFactory()
	 */
	@Override
	public AbstractDataModelBuilderSetFactory getDataModelBuilderSetFactory() {

		return this.dataModelBuilderFactory;
	}

	/**
	 * @param endpointSetFactory the endpointSetFactory to set
	 */
	protected void setEndpointSetFactory(AbstractEndpointSetFactory endpointSetFactory) {
		this.endpointSetFactory = endpointSetFactory;
	}

	/**
	 * @param dataModelBuilderFactory the dataModelBuilderFactory to set
	 */
	protected void setDataModelBuilderFactory(AbstractDataModelBuilderSetFactory dataModelBuilderFactory) {
		this.dataModelBuilderFactory = dataModelBuilderFactory;
	}

}
