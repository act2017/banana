/**
 * 
 */
package com.digsarustudio.musa.mvp;

import com.digsarustudio.musa.mvp.controller.AppController;
import com.google.gwt.core.client.EntryPoint;

/**
 * The sub-type provides {@link AbstractDataModelBuilderSetFactory} and {@link AbstractEndpointSetFactory} 
 * for the client code to create the particular data module used by the specific endpoint.<br>
 * <br>
 * Please initiating this in the {@link EntryPoint#onModuleLoad()} for the {@link AppController}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.saru.erp.musaceae.client.factory.AbstractFactoryCollection} instead
 */
public interface AbstractFactoryCollection {
	AbstractEndpointSetFactory getEndpointSetFactory();
	
	AbstractDataModelBuilderSetFactory getDataModelBuilderSetFactory();
}
