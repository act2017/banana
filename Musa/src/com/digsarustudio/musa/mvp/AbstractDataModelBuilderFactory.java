/**
 * 
 */
package com.digsarustudio.musa.mvp;

import com.digsarustudio.banana.designpattern.abstractfactory.GenericAbstractFactory;

/**
 * The sub-type has the ability to generate data model builder for the client code.<br>
 * But the method must be defined in the sub-class depending on its purpose.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 *
 */
public interface AbstractDataModelBuilderFactory extends GenericAbstractFactory {

}
