/**
 * 
 */
package com.digsarustudio.musa.mvp.history;

import com.digsarustudio.musa.mvp.history.HistoryTokenCollection;

/**
 * A handler deals with the onValuChangedEvent
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated 1.0.4 Please use {@link com.digsarustudio.musa.mvp.history.state.HistoryStateHandler} instead.
 */
public interface HistoryStateHandler {
	/**
	 * To execute what the state goes to do
	 * 
	 * @param collection The collection of history token.
	 */
	void execute(HistoryTokenCollection collection);
}
