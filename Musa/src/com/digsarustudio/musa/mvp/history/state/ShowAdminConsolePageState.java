/**
 * 
 */
package com.digsarustudio.musa.mvp.history.state;

import com.digsarustudio.banana.utils.InvalidTokenFormatException;
import com.digsarustudio.musa.mvp.AbstractDataModelBuilderSetFactory;
import com.digsarustudio.musa.mvp.history.HistoryTokenCollection;

/**
 * The controller who binds with this state will go to show the administrator console page
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	Please use the state from your admin console project
 *
 */
public class ShowAdminConsolePageState extends AbstractHistoryState implements HistoryState {
	public static final String STATE = "CCP-ADMIN";

	/**
	 * @param name
	 */
	public ShowAdminConsolePageState() {
		super(STATE);
	}

	/**
	 * @param name
	 * @param tokens
	 * @param factory
	 */
	public ShowAdminConsolePageState(HistoryTokenCollection tokens, AbstractDataModelBuilderSetFactory factory) {
		super(STATE, tokens, factory);
	}

	/**
	 * @param name
	 * @param tokenString
	 * @param factory
	 * @throws InvalidTokenFormatException
	 */
	public ShowAdminConsolePageState(String tokenString, AbstractDataModelBuilderSetFactory factory) throws InvalidTokenFormatException {
		super(STATE, tokenString, factory);
	}

	/**
	 * To fire the {@link ShowAdminConsolePageState } to notify the controller to show brands page
	 */
	public static void fireHistoryState() {
		ShowAdminConsolePageState state = new ShowAdminConsolePageState();
		//setup parameters if necessary

		HistoryStateBus.fireState(state);
	}
}
