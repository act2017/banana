/**
 * 
 */
package com.digsarustudio.musa.mvp;

import com.google.gwt.user.client.Cookies;

/**
 * The config of app used to handle {@link Cookies}.<br>
 * The sub-type can inherit this class for the fundamental implementations or delegate it.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.4 Please use {@link com.digsarustudio.saru.erp.banana.ship.client.AppConfig} isntead.
 */
public class AppConfig {
	
	/**
	 * 
	 */
	public AppConfig() {
		
	}

	public void setConfig(String key, String value) {
		Cookies.setCookie(key, value);
	}
	
	public String getConfig(String key) {
		return Cookies.getCookie(key);
	}
}
