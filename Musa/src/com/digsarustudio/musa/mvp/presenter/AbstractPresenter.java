/**
 * 
 */
package com.digsarustudio.musa.mvp.presenter;

import com.digsarustudio.musa.mvp.model.DataModel;
import com.digsarustudio.musa.mvp.view.PassiveView;
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The boilerplate of presenter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		1.0.0
 *
 * @deprecated	Please use Musaceae-1.0.3 instead.
 */
public abstract class AbstractPresenter implements Presenter {
	private PassiveView view;
	protected EventBus eventBus;
	
	/**
	 * @deprecated	1.0.15 No longer to use.
	 */
	protected DataModel dataModel;

	/**
	 * Construct the presenter object
	 * 
	 * @param eventBus The event handler manager
	 * @param model The data model
	 * @param view The view for display
	 * 
	 * @deprecated	1.0.15 {@link DataModel} is no longer to used. Please use {@link #AbstractPresenter(EventBus, PassiveView)} instead.
	 */
	public AbstractPresenter(EventBus eventBus, DataModel model, PassiveView view) {
		this.eventBus = eventBus;
		this.dataModel = model;
		this.view = view;
	}
	
	/**
	 * Construct the presenter object
	 * 
	 * @param eventBus The event handler manager
	 * @param model The data model
	 * @param view The view for display
	 * 
	 * @since 1.0.1
	 */ 
	public AbstractPresenter(EventBus eventBus, PassiveView view) {
		this.eventBus = eventBus;
		this.view = view;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.tutorial.gwt.twoeight.shared.mvp.presenter.Presenter#go(com.google.gwt.user.client.ui.HasWidgets)
	 */
	@Override
	public void go(HasWidgets container) {
		container.clear();
		container.add(this.view.asWidget());
		
		this.init();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.Presenter#go()
	 */
	@Override
	public void go() {
		this.init();
	}

	/**
	 * To send an event to the app controller
	 * 
	 * @param event The event that this present wants the controller to do. It's like a notification.
	 * 
	 * @deprecated	1.0.15 
	 */
	protected <E extends EventHandler> void triggerEvent(GwtEvent<E> event) {
		if(null == this.eventBus){
			return;
		}
		
		this.eventBus.fireEvent(event);
	}
	
	protected EventBus getEventBus() {
		return this.eventBus;
	}

	/**
	 * Initialize this presenter
	 */
	protected abstract void init();
}
