/**
 * 
 */
package com.digsarustudio.musa.mvp.presenter;

import com.digsarustudio.musa.mvp.AbstractFactoryCollection;

/**
 * The sub-type can access an abstract factory and use it to generate a particular data model for the 
 * view, presenter, and endpoint.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use Musaceae-1.0.3 instead
 *
 */
public interface FactoryPresenter extends Presenter {
	void setFactoryCollection(AbstractFactoryCollection factoryCollection);
}
