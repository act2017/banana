/**
 * 
 */
package com.digsarustudio.musa.mvp.presenter;

import com.digsarustudio.musa.widget.polymer.dialog.SaruDialog;

/**
 * This presenter receive {@link #onConfirmed()}, {@link #onCancelled()}, and {@link #onReset()} from the client code to 
 * deal with the event triggered by {@link SaruDialog}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @deprecated	1.0.2	please use Musaceae-1.0.3 instead
 */
public interface DialogPresenter extends Presenter {
	void onConfirmed();
	void onCancelled();
	void onReset();
	void onClose();
}
