/**
 * 
 */
package com.digsarustudio.musa.mvp.presenter;

import com.google.gwt.user.client.ui.HasWidgets;

/**
 * The definitions of presenter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	Please use Musaceae instead.
 */
public interface Presenter {
	/**
	 * Representing the target view on the incoming layout panel
	 * 
	 * @param container The incoming layout panel
	 */
	void go(HasWidgets container);
	
	/**
	 * Representing the target view as an independent view.
	 */
	void go();
}
