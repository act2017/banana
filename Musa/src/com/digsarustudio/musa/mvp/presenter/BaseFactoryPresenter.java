/**
 * 
 */
package com.digsarustudio.musa.mvp.presenter;

import com.digsarustudio.musa.mvp.AbstractDataModelBuilderSetFactory;
import com.digsarustudio.musa.mvp.AbstractEndpointSetFactory;
import com.digsarustudio.musa.mvp.AbstractFactoryCollection;
import com.digsarustudio.musa.mvp.view.PassiveView;
import com.google.gwt.event.shared.EventBus;

/**
 * The base class of {@link FactoryPresenter}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2 Please use Musaceae-1.0.3 instead
 */
public abstract class BaseFactoryPresenter extends AbstractPresenter implements FactoryPresenter {
	
	private AbstractFactoryCollection	factoryCollection = null;
	
	/**
	 * Creates a {@link BaseFactoryPresenter}
	 * 
	 * @param eventBus The event handler
	 * @param view The view to control
	 */
	public BaseFactoryPresenter(EventBus eventBus, PassiveView view) {
		super(eventBus, view);
	}
	
	public BaseFactoryPresenter(EventBus eventBus, PassiveView view, AbstractFactoryCollection factory) {
		super(eventBus, view);
		this.setFactoryCollection(factory);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.mvp.presenter.FactoryPresenter#setFactoryCollection(com.digsarustudio.musa.mvp.AbstractFactoryCollection)
	 */
	@Override
	public void setFactoryCollection(AbstractFactoryCollection factoryCollection) {
		this.factoryCollection = factoryCollection;
		
	}

	protected AbstractFactoryCollection getFactoryCollection() {
		return this.factoryCollection;
	}
	
	protected AbstractEndpointSetFactory getEndpointSetFactory() {
		return this.factoryCollection.getEndpointSetFactory();
	}
	
	protected AbstractDataModelBuilderSetFactory getDataModelBuilderSetFactory() {
		return this.factoryCollection.getDataModelBuilderSetFactory();
	}
}
