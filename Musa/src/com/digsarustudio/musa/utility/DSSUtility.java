/**
 * 
 */
package com.digsarustudio.musa.utility;

/**
 * Represents the utility for some purpose.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class DSSUtility {

	/**
	 * Encapsulate constructor
	 */
	private DSSUtility() {

	}

	
	/**
	 * Returns the name of the function caller
	 * 
	 * @return The name of the function caller
	 */
	public static native String getCallerName()/*-{
		return arguments.callee.caller.toString();		
	}-*/;
}
