/**
 * 
 */
package com.digsarustudio.musa.utility;

/**
 * Represents the configuration of connection
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class ConnectionConfig {
	/**
	 * The API key
	 */
	private String apiKey = "1qsxdr5thnji9o;/";	
	
	private static ConnectionConfig instance;
	
	/**
	 * Encapsulate the constructor 
	 */
	private ConnectionConfig() {				
	}

	/**
	 * Returns the instance of ConnectionConfig object
	 * 
	 * @return The instance of ConnectionConfig object
	 */
	public static ConnectionConfig getInstance(){
		if(null == instance){
			instance = new ConnectionConfig();
		}
		
		return instance;
	}

	/**
	 * Returns the API key
	 * 
	 * @return the apiKey
	 */
	public String getApiKey() {
		return apiKey;
	}

	/**
	 * Assign an API key
	 * 
	 * @param apiKey the api key to set
	 */
	public void setAPIKey(String apiKey) {
		this.apiKey = apiKey;
	}
	
	
}
