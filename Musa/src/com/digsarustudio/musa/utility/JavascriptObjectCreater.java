/**
 * 
 */
package com.digsarustudio.musa.utility;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * The subclass of {@link JavascriptObjectCreater} has the ability of creating
 * a javascript object casted as a known data type.
 * 
 * Please use this to instead of {@link JavaScriptObjectBuilder} and {@link AbstractJavaScriptObjectBuilder} 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class JavascriptObjectCreater<T extends JavaScriptObject> {
	protected T createObject(){		
		return JavaScriptObject.createObject().cast();
	}

	protected T createArray(){
		return JavaScriptObject.createArray().cast();
	}
	
	protected T createArray(Integer size){
		return JavaScriptObject.createArray(size).cast();
	}
	
	protected T createFunction(){
		return JavaScriptObject.createFunction().cast();
	}
}
