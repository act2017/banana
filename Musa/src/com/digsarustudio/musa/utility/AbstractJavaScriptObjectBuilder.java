/**
 * 
 */
package com.digsarustudio.musa.utility;

import com.google.gwt.core.client.JavaScriptObject;

/**
 * The base class of javascript object builder
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.12
 *
 * @deprecated	1.0.14 In order to support factory pattern, please use {@link JavascriptObjectCreater} instead.
 */
public abstract class AbstractJavaScriptObjectBuilder<T extends JavaScriptObject> implements JavaScriptObjectBuilder<T> {
	protected T createObject() {
		return JavaScriptObject.createObject().cast();
	}
}
