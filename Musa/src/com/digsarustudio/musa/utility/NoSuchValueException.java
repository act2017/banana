/**
 * 
 */
package com.digsarustudio.musa.utility;

/**
 * This exception indicates the results of target operation which cannot find the value according to 
 * a particular key or index from a collection.
 * 
 * Please using this exception while the collection might contain null as its value. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2 Please use Musaceae-1.0.3 instead
 */
public class NoSuchValueException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4347146204819890321L;

	/**
	 * 
	 */
	public NoSuchValueException() {

	}

	/**
	 * @param message
	 */
	public NoSuchValueException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public NoSuchValueException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public NoSuchValueException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NoSuchValueException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
