/**
 * 
 */
package com.digsarustudio.musa.utility;

/**
 * The singleton app configuration object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * @deprecated	1.0.4 Please use {@link AppConfig} instead
 */
public class AppConfigs {
	private static AppConfigs instance = null;
	
	private String priceFormat = "##,##0.00";
	
	/**
	 * Seal the constructor
	 */
	private AppConfigs() {
	}
	
	/**
	 * Get the Instance
	 * @return The instance of this object
	 */
	public static AppConfigs getInstance() {
		if (null == instance) {
			instance = new AppConfigs();
		}
	
		return instance;
	}

	/**
	 * @return the priceFormat
	 */
	public String getPriceFormat() {
		return priceFormat;
	}

	/**
	 * @param priceFormat the priceFormat to set
	 */
	public void setPriceFormat(String priceFormat) {
		this.priceFormat = priceFormat;
	}

}
