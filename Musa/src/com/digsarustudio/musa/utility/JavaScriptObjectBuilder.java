
/**
 * 
 */
package com.digsarustudio.musa.utility;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * The builder for creating javascript object.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.12
 *
 * @deprecated	1.0.14 In order to support factory pattern, please use {@link JavascriptObjectCreater} instead.
 */
public interface JavaScriptObjectBuilder<T extends JavaScriptObject> extends ObjectBuilder<T>{

}
