/**
 * 
 */
package com.digsarustudio.musa.endpoint;

import com.digsarustudio.musa.endpoint.connection.response.CollectionResponse;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Defining the operations of Endpoint
 *
 * @param T The type of incoming data from client
 * @param E The type of outgoing data to client
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 * @deprecated	please use {@link com.digsarustudio.banana.endpoint.Endpoint} instead.
 */
public interface Endpoint<T, E> {	
	/**
	 * Assigns an API token for the remote service to check the credential of user
	 * 
	 * @param token An API token for the remote service to check the credential of user
	 * 
	 * @since 1.0.0
	 */
	void setAPIToken(String token);

	
	/**
	 * To execute an insertion command on a particular data.<br>
	 *   
	 * @param data The data to be updated
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error.
	 * 
	 * @since 1.0.0
	 */
	void insert(T data, final AsyncCallback<E> callback) throws EndpointOperationException;
	
	/**
	 * To execute a updating command on a particular data.<br>
	 *   
	 * @param data The data to be updated
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void update(T data, final AsyncCallback<E> callback) throws EndpointOperationException;
	
	/**
	 * To execute a updating command on a particular data.<br>
	 *   
	 * @param data The data to be updated
	 * @param callback The callback indicating the result of request.
	 * @param maxRowCount The maximum row count to be delete
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void update(T data, final AsyncCallback<E> callback, Integer maxRowCount) throws EndpointOperationException;
	
	/**
	 * To execute a listing command on a particular data.<br>
	 * 
	 * @param data The data to be fetched
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void list(T data, final AsyncCallback<CollectionResponse<E>> callback) throws EndpointOperationException;

	/**
	 * To execute a listing command on a particular data.<br>
	 *   
	 * @param data The data to be fetched
	 * @param callback The callback indicating the result of request.
	 * @param cursor The beginning row for the search.
	 * @param maxRowCount The maximum row count to be delete
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void list(T data, final AsyncCallback<CollectionResponse<E>> callback, Integer cursor, Integer maxRowCount) throws EndpointOperationException;
	
	/**
	 * To execute a deletion command on a particular data.<br>
	 * 
	 * @param data The data to be deleted
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void delete(T data, final AsyncCallback<E> callback) throws EndpointOperationException;
	
	/**
	 * To execute a deletion command on a particular data.<br>
	 *   
	 * @param data The data to be deleted
	 * @param callback The callback indicating the result of request.
	 * @param maxRowCount The maximum row count to be delete
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void delete(T data, final AsyncCallback<E> callback, Integer maxRowCount) throws EndpointOperationException;
	
	/**
	 * To execute a fetching command on a particular data.<br>
	 *   
	 * @param data The data to be deleted
	 * @param callback The callback indicating the result of request.
	 * 
	 * @throws EndpointOperationException when the operation is failed due to the connection issue or 
	 * 			the restriction of the remote service. Please use {@link EndpointOperationException#getCause()} 
	 * 			to retrieve the reason of the error. 
	 * 
	 * @since 1.0.0
	 */
	void get(T data, final AsyncCallback<E> callback) throws EndpointOperationException;
	
}
