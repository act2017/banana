/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

/**
 * The type of criteria for query filter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public enum QueryCriteriaType {
	  Greater(">")
	, GreaterThan(">=")
	, Equals("=")
	, LessThan("<=")
	, Less("<")
	;
	
	/**
	 * The symbol the criteria type represents
	 */
	private String symbol;
	
	/**
	 * Encapsulate the constructor
	 * 
	 * @param symbol The symbol the criteria type represents
	 */
	private QueryCriteriaType(String symbol){
		this.symbol = symbol;
	}
	
	/**
	 * Returns the symbol the criteria type represents
	 * 
	 * @return the symbol the criteria type represents
	 */
	public String getSymbol(){
		return this.symbol;
	}
}
