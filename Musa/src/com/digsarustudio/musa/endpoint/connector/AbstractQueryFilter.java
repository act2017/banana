/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;
import com.digsarustudio.musa.endpoint.connector.QueryCriteriaType;
import com.digsarustudio.musa.endpoint.connector.QueryFilter;

/**
 * The boilerplate of query filter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.15 Please use {@link RequestParameter} to instead.
 */
public abstract class AbstractQueryFilter implements QueryFilter {	
	/**
	 * The name of target field
	 */
	protected String fieldName;
	
	/**
	 * The criteria type of query
	 */
	protected QueryCriteriaType type;
	
	/**
	 * the value of criteria
	 */
	protected String value;
	
	/**
	 * Constructor
	 * 
	 * @param fieldName The name of filter
	 * @param type The criteria type
	 * @param value The value of criteria
	 */
	public AbstractQueryFilter(String fieldName, QueryCriteriaType type, String value){
		this.fieldName = fieldName;
		this.type = type;
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IQueryFilter#getValue(java.lang.String)
	 */
	@Override
	public String getValue(String fieldName) {
		return this.value;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		if( null != this.fieldName )
			builder.append(this.fieldName);
		
		if( null != this.type )
			builder.append(this.type.getSymbol());
		
		if( null != this.value )
			builder.append(this.value);
		
		return builder.toString();
	}	
}
