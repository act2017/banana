/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import com.digsarustudio.musa.endpoint.connection.http.HTTPConnection;

/**
 * The poilerplate of connector
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.15 Please use {@link HTTPConnection} instead
 */
public abstract class AbstractConnector implements Connector {
	/**
	 * The URL of target service
	 */
	protected String url;

	/**
	 * Constructs a connector 
	 * 
	 * @param url The URL of target service
	 */
	public AbstractConnector(String url) {
		this.url = url;
	}
	
	/**
	 * Returns the URL of target service
	 * 
	 * @return the URL of target service
	 */
	public String getURL(){
		return this.url;
	}
}
