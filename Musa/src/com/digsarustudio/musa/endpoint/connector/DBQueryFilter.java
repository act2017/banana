/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.connector.QueryCriteriaType;
import com.digsarustudio.musa.endpoint.connector.QueryFilter;

/**
 * The filter for database query
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 * @deprecated	1.0.15 unused
 */
public class DBQueryFilter extends AbstractQueryFilter implements QueryFilter {
	
	/**
	 * 
	 * The query filter builder
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<DBQueryFilter> {
		/**
		 * The name of target field
		 */
		private String fieldName;
		
		/**
		 * The criteria type of query
		 */
		private QueryCriteriaType type;
		
		/**
		 * the value of criteria
		 */
		private String value;
		
		/**
		 * Constructs a builder  object 
		 */
		private Builder(String fieldName) {
			this.fieldName = fieldName;
		}		

		/**
		 * Returns the name of filter
		 * 
		 * @return the name of filter
		 */
		private String getFieldName() {
			return fieldName;
		}

		/**
		 * Assign a name to this filter
		 * 
		 * @param fieldName the name to set
		 */
		public Builder setFieldName(String fieldName) {
			this.fieldName = fieldName;
			return this;
		}


		/**
		 * Returns the type of criteria
		 * 
		 * @return the type of criteria
		 */
		private QueryCriteriaType getType() {
			return type;
		}

		/**
		 * Assign a type of criteria for this filter
		 * 
		 * @param type the type to set
		 */
		public Builder setType(QueryCriteriaType type) {
			this.type = type;
			return this;
		}

		/**
		 * Returns the value of criteria
		 * 
		 * @return the value of criteria
		 */
		private String getValue() {
			return value;
		}

		/**
		 * Assign a value of criteria
		 * 
		 * @param value the value to set
		 */
		public Builder setValue(String value) {
			this.value = value;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public DBQueryFilter build() {
			return new DBQueryFilter(this);
		}
	}

	/**
	 * Encapsulate constructor
	 * 
	 * @param builder The builder to build a query filter object
	 */
	private DBQueryFilter(Builder builder){
		super(builder.getFieldName(), builder.getType(), builder.getValue());
	}
	
	/**
	 * Creates a query filter builder
	 * 
	 * @param fieldName The name of filter
	 * 
	 * @return The query filter builder
	 */
	public static Builder builder(String fieldName){
		return new Builder(fieldName);
	}
}
