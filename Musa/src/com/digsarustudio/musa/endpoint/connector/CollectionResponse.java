/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import com.digsarustudio.banana.utils.ObjectBuilder;


/**
 * Represents a client site CollectionResponse for GWT because of the com.google.api.server.spi.response.CollectionResponse cannot be found while compiling.
 * This is used to instead of com.google.api.server.spi.response.CollectionResponse in client site. 
 * @see @{link: com.google.api.server.spi.response.CollectionResponse}
 *
 * TODO replace this class by the implementation of com.digsarustudio.musa.endpoint.connector.response.CollectionResponse
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.1
 * @since		0.0.1
 * @deprecated	1.0.15
 */
public class CollectionResponse<T> implements Serializable{
		
	/**
	 * 
	 */
	private static final long serialVersionUID = -275777333795684224L;

	public static class Builder<S> implements ObjectBuilder<CollectionResponse<S>> {
		private String nextPageToken;
		private Integer totalCount;
		private Collection<S> items;
		
		/**
		 * Constructs a Builder  object 
		 */
		public Builder() {
		}			

		/**
		 * @return the nextPageToken
		 */
		private String getNextPageToken() {
			return nextPageToken;
		}

		/**
		 * @param nextPageToken the nextPageToken to set
		 */
		public Builder<S> setNextPageToken(String nextPageToken) {
			this.nextPageToken = nextPageToken;
			return this;
		}

		/**
		 * @return the items
		 */
		private Collection<S> getItems() {
			return items;
		}

		/**
		 * @param items the items to set
		 */
		public Builder<S> setItems(Collection<S> items) {
			this.items = items;
			return this;
		}

		
		/**
		 * Returns the total count of target data on the server side
		 * 
		 * @return the total count of target data on the server side
		 */
		private Integer getTotalCount() {
			return totalCount;
		}

		/**
		 * Assign a total count of target data on the server side
		 * @param totalCount the total count of target data on the server side to set
		 */
		public Builder<S> setTotalCount(Integer totalCount) {
			this.totalCount = totalCount;
			
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public CollectionResponse<S> build() {
	
			return new CollectionResponse<S>(this);
		}
	}
	
	
	private String nextPageToken;	
	private Integer totalCount;
	private Collection<T> items;
	
	
	@SuppressWarnings("unused")
	private CollectionResponse(){
		super();
	}

	/**
	 * Constructs a CollectionResponseClient
	 * 
	 * hided for Builder 
	 */
	private CollectionResponse(Builder<T> builder) {
		this.setItems(builder.getItems());
		this.setNextPageToken(builder.getNextPageToken());	
		this.setTotalCount(builder.getTotalCount());
	}
	
	/**
	 * Copy constructor
	 * 
	 * @param source The source object
	 */
	public CollectionResponse(CollectionResponse<T> source){
		Collection<T> items = new ArrayList<>(source.getItems());
		this.setItems(items);
		this.setNextPageToken(source.getNextPageToken());
		this.setTotalCount(source.getTotalCount());
	}
	
	public static <T> Builder<T> builder(){
		return new CollectionResponse.Builder<T>();
	}

	/**
	 * @return the nextPageToken
	 */
	public String getNextPageToken() {
		return nextPageToken;
	}

	/**
	 * @param nextPageToken the nextPageToken to set
	 */
	public void setNextPageToken(String nextPageToken) {
		this.nextPageToken = nextPageToken;
	}

	/**
	 * @return the items
	 */
	public Collection<T> getItems() {
		return items;
	}

	/**
	 * @param items the items to set
	 */
	public void setItems(Collection<T> items) {
		this.items = items;
	}
	
	/**
	 * Returns the total count of target data on the server side
	 * 
	 * @return the total count of target data on the server side
	 */
	public Integer getTotalCount() {
		return totalCount;
	}

	/**
	 * Assign a total count of target data on the server side
	 * @param totalCount the total count of target data on the server side to set
	 */
	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((nextPageToken == null) ? 0 : nextPageToken.hashCode());
		result = prime * result + ((totalCount == null) ? 0 : totalCount.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollectionResponse<T> other = (CollectionResponse<T>) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (nextPageToken == null) {
			if (other.nextPageToken != null)
				return false;
		} else if (!nextPageToken.equals(other.nextPageToken))
			return false;
		if (totalCount == null) {
			if (other.totalCount != null)
				return false;
		} else if (!totalCount.equals(other.totalCount))
			return false;
		return true;
	}
}
