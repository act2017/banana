/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import java.util.List;

import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.musa.endpoint.connector.http.FormData;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Defining the operations of a connector 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15 Please use {@link DatabaseConnection} instead
 */
public interface Connector {
	/**
	 * To retrieve the data from web service according to the filters
	 * 
	 * @param filters The criteria of query
	 * @param callback The callback function from client
	 */
	void list(List<QueryFilter> filters, AsyncCallback<String> callback);
	
	/**
	 * To list the data from the web service
	 * 
	 * @param parameters The parameters to insert
	 * @param jsonString The json string
	 */
	void list(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback);	
	
	/**
	 * To retrieve a specific data from web service by a key
	 * 
	 * @param filters The criteria of query
	 * @param callback The callback function from client
	 */
	void get(List<QueryFilter> filters, AsyncCallback<String> callback);
	
	/**
	 * To insert a new data to the web service
	 * 
	 * @param parameters The parameters to insert
	 */
	void insert(List<QueryFilter> parameters, AsyncCallback<String> callback);
		
	/**
	 * To insert a new data to the web service
	 * 
	 * @param parameters The parameters to insert
	 * @param formData The form data to insert
	 */
	void insert(List<QueryFilter> parameters, List<FormData> formData, AsyncCallback<String> callback);
	
	/**
	 * To insert a new data to the web service
	 * 
	 * @param parameters The parameters to insert
	 * @param jsonString The json string
	 */
	void insert(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback);
		
	/**
	 * To update a existed data to the web service
	 * 
	 * @param parameters The parameters to update
	 */
	void update(List<QueryFilter> parameters, AsyncCallback<String> callback);
	
	/**
	 * To update a existed data to the web service
	 * 
	 * @param parameters The parameters to insert
	 * @param formData The form data to insert
	 */
	void update(List<QueryFilter> parameters, List<FormData> formData, AsyncCallback<String> callback);
	
	/**
	 * To update a new data to the web service
	 * 
	 * @param parameters The parameters to insert
	 * @param jsonString The json string
	 */
	void update(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback);
	
	/**
	 * To delete the existed data to the web service
	 * 
	 * @param parameters The parameters for the data deletion
	 */
	void delete(List<QueryFilter> parameters, AsyncCallback<String> callback);	
	
	/**
	 * To delete a new data to the web service
	 * 
	 * @param parameters The parameters to insert
	 * @param jsonString The json string
	 */
	void delete(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback);	
}
