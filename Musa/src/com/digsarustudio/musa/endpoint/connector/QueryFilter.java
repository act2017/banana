/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * Defining the operations of query filter
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.15 Please use {@link RequestParameter} to instead.
 */
public interface QueryFilter {
	/**
	 * Returns the value of criteria
	 * 
	 * @param fieldName The name of field to filter
	 * 
	 * @return The value of criteria
	 */
	String getValue(String fieldName);
	
	/**
	 * Returns a formatted string represents the filter.
	 * For example:
	 * 		fieldName		= price
	 * 		criteriaType	= GreaterThan
	 * 		value			= 20
	 * 
	 * The result will be "price >= 20" depends on the sub-type.
	 * 
	 * @return a formatted string represents the filter
	 */
	String toString();
}
