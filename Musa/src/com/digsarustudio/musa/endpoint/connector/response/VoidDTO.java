/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public interface VoidDTO {

}
