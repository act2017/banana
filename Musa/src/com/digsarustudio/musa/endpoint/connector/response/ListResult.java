/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

import com.digsarustudio.musa.endpoint.connector.CollectionResponse;

/**
 * Defines the interface of list reslut object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.01
 * 
 * @deprecated	1.0.15	Please use {@link CollectionResponse} instead.
 */
public interface ListResult<T> {
	/**
	 * Returns the total number of matched data oon the server side
	 * 
	 * @return the total number of matched data oon the server side
	 */
	public int getTotalCount();
	
	/**
	 * Returns the next cursor
	 * 
	 * @return the next cursor
	 */
	public int getNextCursor();
	
	/**
	 * Returns the result by a specific index
	 * 
	 * @param index The index of result
	 * 
	 * @return  the result by a specific index
	 */
	public T getResult(int index);
	
	/**
	 * Returns the count of returned results
	 * 
	 * @return the count of returned results
	 */
	public int getResultCount();
}
