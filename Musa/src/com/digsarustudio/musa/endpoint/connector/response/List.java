/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

import com.digsarustudio.musa.endpoint.connector.CollectionResponse;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 * @deprecated	1.0.15	Please use {@link CollectionResponse} instead.
 */
public interface List<T> {
	/**
	 * Returns the result by a specific index
	 * 
	 * @param index The index of result
	 * 
	 * @return  the result by a specific index
	 */
	public T get(int index);
	
	/**
	 * Returns the count of returned results
	 * 
	 * @return the count of returned results
	 */
	public int getCount();
	
	/**
	 * Append a new object
	 * 
	 * @param obj The object to append
	 */
	public void add(T obj);
}
