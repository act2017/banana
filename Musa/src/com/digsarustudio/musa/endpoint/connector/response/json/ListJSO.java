/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response.json;

import com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJSO;
import com.digsarustudio.musa.endpoint.connector.response.List;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 * @deprecated	1.0.15
 * 
 * @deprecated	1.0.15 Please use {@link CollectionResponseJSO} instead
 */
public class ListJSO<T> extends JavaScriptObject implements List<T> {

	/**
	 * Do not remove
	 */
	protected ListJSO() {}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.List#get(int)
	 */
	@Override
	public final native T get(int index) /*-{
		if( null == this.list ){
			return null;
		}else if( index >= this.list.length ){
			return null;
		}
		
		return this.list[index];
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.List#getCount()
	 */
	@Override
	public final native int getCount() /*-{
		if( null == this.list ){
			return 0;
		}
		
		return this.list.length;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.List#add(java.lang.Object)
	 */
	@Override
	public final native void add(T obj) /*-{
		if( null == this.list ){
			this.list = [];
		}
		
		this.list.push(obj);
	}-*/;

}
