/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response.json;

import com.digsarustudio.musa.endpoint.connection.response.CommandResponseJSO;
import com.digsarustudio.musa.endpoint.connector.response.DebugInfo;
import com.digsarustudio.musa.endpoint.connector.response.QueryResponse;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * Represents a javascript object working as QueryResponse object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15 Please use {@link CommandResponseJSO} instead
 */
public final class QueryResponseJSO<T> extends JavaScriptObject implements QueryResponse<T> {

	/**
	 * Do not modify this.
	 */
	protected QueryResponseJSO(){

	}


	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.response.QueryResponse#getStatusCode()
	 */
	@Override
	public final native int getStatusCode() /*-{		
		return this.statusCode;
	}-*/;
	

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.response.QueryResponse#getErrorMessage()
	 */
	@Override
	public final native String getErrorMessage() /*-{		
		return this.errorMsg;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.QueryResponse#getDebugInfo()
	 */
	@Override
	public final native DebugInfo getDebugInfo() /*-{
		return this.debug;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.QueryResponse#getResult()
	 */
	@Override
	public final native T getResult() /*-{
		return this.result;
	}-*/;
}
