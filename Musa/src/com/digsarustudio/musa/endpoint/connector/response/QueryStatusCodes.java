/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

import com.digsarustudio.musa.endpoint.connection.response.CommandResponseStatus;

/**
 * Enumerates the status code for query
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15	Please use {@link CommandResponseStatus}
 */
public enum QueryStatusCodes {
	NoError(0000)
	
//System
	,ProductCategoryNotExist(0001)
	,PermissionDenied(0002)
	
//General	
	,ConnectionFailed(1001)
	,InvalidArgument(1002)
	,InvalidToken(1003)
	,NotSupportFunction(1004)
	
//Database	
	,NonExistedDatabase(1101)
	,DuplicatedDatabase(1102)
	
//Table	
	,NonExistedTable(1201)
	,DuplicatedTable(1202)
	
//SQL	
	,InsertionFailed(1301)
	,UpdatingFailed(1302)
    ,DeletionFailed(1303)
    ,SelectionFailed(1304)	
	
//Data	
	,DuplicatedData(2001)
	,NonExistedData(2002)
	
//Cutomised - from 4	
	
	,unKnown(9999)
	;
	
	
	private Integer code = 0;
	
	private static QueryStatusCodes[] values = values();	
	
	private QueryStatusCodes(Integer code){
		this.code = code;
	}
	
	/**
	 * Returns the code of Query Status Code
	 * @return the code of Query Status Code
	 */
	public Integer getValue(){
		return this.code;
	}
	
	/**
	 * Get Query Status Code enumerations by integer value
	 * 
	 * @param value The value of target status code
	 * 
	 * @return Query status code if found, otherwise unKnown returned
	 */
	public static QueryStatusCodes fromValue(Integer value){
		for (QueryStatusCodes code : values) {
			if( !code.getValue().equals(value) ){
				continue;
			}
			
			return code;
		}
		
		return unKnown; 
	}
}
