/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

import com.digsarustudio.musa.endpoint.connection.response.CommandResponse;

/**
 * Defines a query response interface 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15	Please use {@link CommandResponse} instead.
 */
public interface QueryResponse<T> {
	
	/**
	 * Returns the status code of query
	 * 
	 * @return the status code of query
	 */
	public int getStatusCode();
	
	/**
	 * Returns the error message
	 * 
	 * @return the error message
	 */
	public String getErrorMessage();
	
	public DebugInfo getDebugInfo();
	
	/**
	 * Returns the query result
	 * 
	 * @return the query result
	 */
	public T getResult();
}
