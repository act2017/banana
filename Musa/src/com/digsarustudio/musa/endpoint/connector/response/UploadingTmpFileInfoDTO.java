/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public interface UploadingTmpFileInfoDTO {
	public final String FIELD_NAME		= "name";
	public final String FIELD_TYPE		= "type";
	public final String FIELD_PATH		= "path";
	public final String FIELD_ERROR		= "error";
	public final String FIELD_SIZE		= "size";
	
	String getName();
	String getType();
	String getPath();
	int getError();
	int getSize();
}
