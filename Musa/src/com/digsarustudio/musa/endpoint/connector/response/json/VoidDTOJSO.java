/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response.json;

import com.digsarustudio.musa.endpoint.connector.response.VoidDTO;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class VoidDTOJSO extends JavaScriptObject implements VoidDTO {

	/**
	 * Do not remove.
	 */
	protected VoidDTOJSO() {
	}

}
