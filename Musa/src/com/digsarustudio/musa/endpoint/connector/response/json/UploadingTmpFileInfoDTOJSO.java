/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response.json;

import com.digsarustudio.musa.endpoint.connector.response.UploadingTmpFileInfoDTO;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class UploadingTmpFileInfoDTOJSO extends JavaScriptObject implements UploadingTmpFileInfoDTO {

	/**
	 * Don not remove.
	 */
	protected UploadingTmpFileInfoDTOJSO() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.file.UploadingTmpFileInfoDTO#getName()
	 */
	@Override
	public final native String getName() /*-{
		return this.name;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.file.UploadingTmpFileInfoDTO#getType()
	 */
	@Override
	public final native String getType() /*-{
		return this.type;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.file.UploadingTmpFileInfoDTO#getPath()
	 */
	@Override
	public final native String getPath() /*-{
		return this.path;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.file.UploadingTmpFileInfoDTO#getError()
	 */
	@Override
	public final native int getError() /*-{
		return this.error;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.file.UploadingTmpFileInfoDTO#getSize()
	 */
	@Override
	public final native int getSize() /*-{
		return this.size;
	}-*/;

}
