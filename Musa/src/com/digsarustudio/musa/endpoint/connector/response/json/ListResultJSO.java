/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response.json;

import com.digsarustudio.musa.endpoint.connection.response.CollectionResponseJSO;
import com.digsarustudio.musa.endpoint.connector.response.ListResult;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * Represents a javascript object which works as ListResult object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15 Please use {@link CollectionResponseJSO} instead
 */
public final class ListResultJSO<T> extends JavaScriptObject implements ListResult<T> {

	/**
	 * Do not modify it.
	 */
	protected ListResultJSO() {		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.ListResult#getTotalCount()
	 */
	@Override
	public final native int getTotalCount() /*-{
		
		return this.totalCount;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.ListResult#getNextCursor()
	 */
	@Override
	public final native int getNextCursor() /*-{
		
		return this.nextCursor;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.ListResult#getResult(int)
	 */
	@Override
	public final native T getResult(int index) /*-{
		if( null == this.results ){
			return null;
		}
		
		if( index >= this.results.length ){
			return null;
		}
		
		return this.results[index];
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.ListResult#getResultCount()
	 */
	@Override
	public final native int getResultCount() /*-{
		if( null == this.results ){
			return 0;
		}
		
		return this.resluts.length;
	}-*/;

}
