/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public enum FileUploadErrorCode {
	Ok(0),
	OverMaxSizeInINI(1),
	OverMaxSizeInForm(2),
	UploadedPartially(3),
	NoFileUploaded(4),
	NoTmpFolderFound(5),
	FailedTOWrite(6),
	InvalidExtension(7);
	
	private Integer value;
	
	
	private FileUploadErrorCode(Integer value){
		this.value = value;
	}
	
	public Integer getValue(){
		return this.value;
	}
	
	public static FileUploadErrorCode fromValue(Integer value){
		FileUploadErrorCode[] values = values();
		
		for (FileUploadErrorCode code : values) {
			if( code.getValue() != value ){
				continue;
			}
			
			return code;
		}
		
		return null;
	}
}
