/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class DataRetrieveFailedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1341749282063579858L;

	/**
	 * 
	 */
	public DataRetrieveFailedException() {

	}

	/**
	 * @param arg0
	 */
	public DataRetrieveFailedException(String arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 */
	public DataRetrieveFailedException(Throwable arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DataRetrieveFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public DataRetrieveFailedException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

}
