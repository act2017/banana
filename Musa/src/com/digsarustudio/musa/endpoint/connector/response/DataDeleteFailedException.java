/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response;

/**
 * Represents an exception thrown when the data cannot be deleted from database
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DataDeleteFailedException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5418676533845198660L;

	/**
	 * 
	 */
	public DataDeleteFailedException() {

	}
	
	/**
	 * @param arg0
	 */
	public DataDeleteFailedException(String arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 */
	public DataDeleteFailedException(Throwable arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DataDeleteFailedException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public DataDeleteFailedException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}	
}
