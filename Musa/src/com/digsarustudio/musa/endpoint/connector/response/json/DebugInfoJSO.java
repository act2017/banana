/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.response.json;

import com.digsarustudio.musa.endpoint.connector.response.DebugInfo;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DebugInfoJSO extends JavaScriptObject implements DebugInfo {

	/**
	 * 
	 */
	protected DebugInfoJSO() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.DebugInfo#getQueryString()
	 */
	@Override
	public final native String getQueryString() /*-{
		return this.queryString;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.DebugInfo#getExceptionMessage()
	 */
	@Override
	public final native String getExceptionMessage() /*-{
		return this.exceptionMsg;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.DebugInfo#getExceptionCode()
	 */
	@Override
	public final native int getExceptionCode() /*-{
		return this.exceptionCode;
	}-*/;

}
