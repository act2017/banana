/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector;

import java.util.List;

/**
 * Defining the operations of the object which needs to be serialized.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15 no longer to use.
 */
public interface ParameterSerializable {
	/**
	 * Serializes the sub-type in the HTTP parameter form
	 * 
	 * @return A list of parameters
	 */
	List<QueryFilter> toHTTPParameter();
}
