/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.http;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents a HTTP form data object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class FormData {
	private final String HYPHEN_TO_STRING= ":";
	private final String ENCLOSURER= "\n";
	
	/**
	 * 
	 * Represents a builder which can build a FormData object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<FormData> {
		private String key;
		private String value;
		
		/**
		 * Constructs a builder  object
		 * 
		 *  @param key The key of form data
		 */
		public Builder(String key) {
			this.setKey(key);
		}
		
		/**
		 * Returns the key of form data
		 * 
		 * @return the key of form data
		 */
		private String getKey() {
			return key;
		}

		/**
		 * Assign a key to form data
		 * 
		 * @param key the key for form data to set
		 */
		public Builder setKey(String key) {
			this.key = key;
			
			return this;
		}

		/**
		 * Returns the value of form data
		 * 
		 * @return the value of form data
		 */
		private String getValue() {
			return value;
		}

		/**
		 * Assign a value to the form data
		 * 
		 * @param value the value for the form data to set
		 */
		public Builder setValue(String value) {
			this.value = value;
			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public FormData build() {
			return new FormData(this);
		}
	}
	
	
		
	private String key;
	private String value;	
	
	/**
	 * Constructs a form data object
	 */
	private FormData(Builder builder) {
		this.setKey(builder.getKey());
		this.setValue(builder.getValue());
	}

	/**
	 * Returns the key of form data
	 * 
	 * @return the key of form data
	 */
	public String getKey() {
		return key;
	}


	/**
	 * Assign a key to form data
	 * 
	 * @param key the key for form data to set
	 */
	public void setKey(String key) {
		this.key = key;
	}


	/**
	 * Returns the value of form data
	 * 
	 * @return the value of form data
	 */
	public String getValue() {
		return value;
	}


	/**
	 * Assign a value to the form data
	 * 
	 * @param value the value for the form data to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	
	/**
	 * To build a form data builder
	 * 
	 * @param key The key of form data
	 * 
	 * @return The builder of form data
	 */
	public static final Builder build(String key){
		return new Builder(key);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		
		builder.append(this.getKey());
		builder.append(this.HYPHEN_TO_STRING);
		builder.append(this.getValue());
		builder.append(this.ENCLOSURER);
		
		return builder.toString();
	}
	
	
	
}
