/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.http;

import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.musa.endpoint.connection.http.HTTPConnection;
import com.digsarustudio.musa.endpoint.connector.AbstractConnector;
import com.digsarustudio.musa.endpoint.connector.Connector;
import com.digsarustudio.musa.endpoint.connector.QueryFilter;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.URL;
import com.google.gwt.http.client.RequestBuilder.Method;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Represents a connector for HTTP
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15 Please use {@link HTTPConnection} instead
 */
@SuppressWarnings("deprecation")
public class HTTPConnector extends AbstractConnector implements Connector {
	Logger logger = Logger.getLogger(HTTPConnector.class.getName());
	
	
	private final String HYPHEN_REQUEST_DATA = "&";
	private final String HYPHEN_REQUEST_PARAMETERS = "?";

	/**
	 * Constructs a HTTP connector
	 * 
	 * @param url The URL of target service
	 */
	public HTTPConnector(String url) {
		super(url);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IConnector#list(java.util.List)
	 */
	@Override
	public void list(List<QueryFilter> filters, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.GET, this.formatQueryString(filters), callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on listing: " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.Connector#list(java.util.List, java.lang.String, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void list(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.GET, parameters, jsonString, callback);
		} catch (RequestException e) {			
			e.printStackTrace();
			
			logger.severe("Exception thrown on fetching: " + e.getMessage());
		}		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IConnector#get(java.util.String)
	 */
	@Override
	public void get(List<QueryFilter> filters, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.GET, this.formatQueryString(filters), callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on listing: " + e.getMessage());
		}		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IConnector#insert(java.util.List)
	 */	
	@Override
	public void insert(List<QueryFilter> parameters, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.GET, parameters, callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on listing: " + e.getMessage());
		}
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IConnector#insert(java.util.List)
	 */	
	@Override
	public void insert(List<QueryFilter> parameters, List<FormData> formData, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.POST, parameters, formData, callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on inserting: " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Connector#insert(java.util.List, java.lang.String, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void insert(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.POST, parameters, jsonString, callback);
		} catch (RequestException e) {			
			e.printStackTrace();
			
			logger.severe("Exception thrown on inserting: " + e.getMessage());
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IConnector#update(java.util.List)
	 */
	@Override
	public void update(List<QueryFilter> parameters, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.PUT, parameters, callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on updating: " + e.getMessage());
		}		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Connector#update(java.util.List, java.util.List, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void update(List<QueryFilter> parameters, List<FormData> formData, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.PUT, parameters, formData, callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on updating with form data: " + e.getMessage());
		}	
		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Connector#update(java.util.List, java.lang.String, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void update(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.PUT, parameters, jsonString, callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on updating with json string: " + e.getMessage());
		}
		
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoints.IConnector#delete(java.util.List)
	 */
	@Override
	public void delete(List<QueryFilter> parameters, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.DELETE, this.formatQueryString(parameters), callback);
		} catch (RequestException e) {
			e.printStackTrace();
			
			logger.severe("Exception thrown on deleting: " + e.getMessage());
		}
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Connector#delete(java.util.List, java.lang.String, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void delete(List<QueryFilter> parameters, String jsonString, AsyncCallback<String> callback) {
		try {
			this.sendRequest(RequestBuilder.DELETE, parameters, jsonString, callback);
		} catch (RequestException e) {
			logger.severe("Exception thrown on deleting with json string: " + e.getMessage());
			e.printStackTrace();
		}
		
	}

	protected void sendRequest(Method method, List<QueryFilter> queryParameters, final AsyncCallback<String> callback) throws RequestException{
		RequestBuilder requestBuilder = this.buildRequestBuilder(queryParameters);
		
		this.dispatchRequest(requestBuilder, method, this.formatQueryString(queryParameters), callback);
	}
	
	protected void sendRequest(Method method, List<QueryFilter> queryParameters, List<FormData> formData, final AsyncCallback<String> callback) throws RequestException{
		RequestBuilder requestBuilder = this.buildRequestBuilder(queryParameters);
		
		//Make the request be able to take form data
		requestBuilder.setHeader(HTTPHeaders.ContentType.getKey(), "application/x-www-form-urlencoded");
		
		this.dispatchRequest(requestBuilder, method, this.formatFormDataString(formData), callback);
	}
	
	protected void sendRequest(Method method, List<QueryFilter> queryParameters, String jsonString, final AsyncCallback<String> callback) throws RequestException{
		RequestBuilder requestBuilder = this.buildRequestBuilder(queryParameters);
		
		//Make the request be able to take form data
		requestBuilder.setHeader(HTTPHeaders.ContentType.getKey(), "application/json");
	
		this.dispatchRequest(requestBuilder, method, jsonString, callback);
	}	
	
	/**
	 * Send request to target service
	 * 
	 * @param method The GET, POST, UPDATE, or DELETE method
	 * @param queryParameter The query parameter string
	 * @param callback
	 * @throws RequestException
	 * 
	 * @deprecated 1.0.0
	 */
	protected void sendRequest(Method method, String queryParameter, final AsyncCallback<String> callback) throws RequestException {
		this.sendRequest(method, queryParameter, null, callback);
	}
	
	/**
	 * Send request to target service
	 * 
	 * @param method The GET, POST, UPDATE, or DELETE method
	 * @param queryParameter The query parameter string
	 * @param requestData The form data string
	 * @param callback
	 * @throws RequestException
	 * 
	 * @deprecated 1.0.0
	 */
	protected void sendRequest(Method method, String queryParameter, String requestData, final AsyncCallback<String> callback) throws RequestException {
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder.append(this.getURL());	
		if( null != queryParameter ){
			stringbuilder.append(queryParameter);
		}
		
		RequestBuilder builder = new RequestBuilder(RequestBuilder.POST, URL.encode(stringbuilder.toString()));

		//Use HTTP override method to skip the barrier of DELETE and PUT from some servers. 
		builder.setHeader(HTTPHeaders.HttpMethodOverride.getKey(), method.toString());
		
		//Make the request be able to take form data
		builder.setHeader(HTTPHeaders.ContentType.getKey(), "application/x-www-form-urlencoded");
		

		builder.sendRequest(requestData, new RequestCallback() {			
			@Override
			public void onResponseReceived(Request request, Response response) {
				if( null == callback )
					return;
				
				if( 200 == response.getStatusCode() ){
					callback.onSuccess(response.getText());
				}else{
					callback.onFailure(new Exception(response.getStatusCode() + " : " + response.getStatusText()));
				}
			}
			
			@Override
			public void onError(Request request, Throwable exception) {
				if( null == callback )
					return;
				
				callback.onFailure(exception);
			}
		});
	}
	
	/**
	 * Converts query filters to a query string
	 * 
	 * @param filters The query filters
	 * 
	 * @return non-empty string if the filters is not null or empty, otherwise null
	 */
	private String formatQueryString(List<QueryFilter> filters){
		if( null == filters || filters.isEmpty() ){
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append(HYPHEN_REQUEST_PARAMETERS);		
		
		Integer counter = 0;
		for (QueryFilter filter : filters) {
			if( 0 != counter++ ){
				builder.append(HYPHEN_REQUEST_DATA);
			}
			
			builder.append(filter.toString());
		}
		
		return builder.toString();
	}

	/**
	 * Converts form data to a request data string
	 * 
	 * @param formData The list of form data
	 * 
	 * @return The request data string
	 */
	private String formatFormDataString(List<FormData> formData){
		if( null == formData || formData.isEmpty() ){
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		for (FormData data : formData) {
			builder.append(data.toString());
		}
		
		return builder.toString();
	}
		
	private RequestBuilder buildRequestBuilder(List<QueryFilter> queryParameters){
		StringBuilder stringbuilder = new StringBuilder();
		stringbuilder.append(this.getURL());	
		if( null != queryParameters && !queryParameters.isEmpty() ){
			stringbuilder.append(this.formatQueryString(queryParameters));
		}
		
		return new RequestBuilder(RequestBuilder.POST, URL.encode(stringbuilder.toString()));
	}
	
	private void dispatchRequest(RequestBuilder builder, Method method, String requestData, final AsyncCallback<String> callback) throws RequestException{
		//Use HTTP override method to skip the barrier of DELETE and PUT from some servers. 
		builder.setHeader(HTTPHeaders.HttpMethodOverride.getKey(), method.toString());
				
		builder.sendRequest(requestData, new RequestCallback() {			
			@Override
			public void onResponseReceived(Request request, Response response) {
				if( null == callback )
					return;
				
				if( 200 == response.getStatusCode() ){
					callback.onSuccess(response.getText());
				}else{
					callback.onFailure(new Exception(response.getStatusCode() + " : " + response.getStatusText()));
				}
			}
			
			@Override
			public void onError(Request request, Throwable exception) {
				if( null == callback )
					return;
				
				callback.onFailure(exception);
			}
		});
	}
}
