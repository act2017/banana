/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.http;

/**
 * Represents a enumeration of the fields of HTTP header
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.15 Please use {@link com.digsarustudio.musa.endpoint.connection.http.HTTPHeaders} instead.
 */
public enum HTTPHeaders {
	HttpMethodOverride("X-HTTP-METHOD-OVERRIDE"),	//Supported by PHP 5.6.* later	
	ContentType("Content-type")
	;
	
	
	private String key;
	
	
	private HTTPHeaders(String key){
		this.key = key;
	}
	
	/**
	 * Returns the key of the specific HTTP header
	 * 
	 * @return the key of the specific HTTP header
	 */
	public String getKey(){
		return this.key;
	}
}
