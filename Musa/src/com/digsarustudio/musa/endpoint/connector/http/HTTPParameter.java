/**
 * 
 */
package com.digsarustudio.musa.endpoint.connector.http;


import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.connector.AbstractQueryFilter;
import com.digsarustudio.musa.endpoint.connector.QueryCriteriaType;
import com.digsarustudio.musa.endpoint.connector.QueryFilter;

/**
 * Represents the parameter for HTTP URL
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class HTTPParameter extends AbstractQueryFilter implements QueryFilter {
	
	/**
	 * 
	 * The query filter builder
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<HTTPParameter> {
		/**
		 * The name of target field
		 */
		private String fieldName;
		
		/**
		 * the value of criteria
		 */
		private String value;
		
		/**
		 * Constructs a builder  object 
		 */
		private Builder(String fieldName) {
			this.fieldName = fieldName;
		}		

		/**
		 * Returns the name of filter
		 * 
		 * @return the name of filter
		 */
		private String getFieldName() {
			return fieldName;
		}

		/**
		 * Assign a name to this filter
		 * 
		 * @param fieldName the name to set
		 */
		public Builder setFieldName(String fieldName) {
			this.fieldName = fieldName;
			return this;
		}

		/**
		 * Returns the value of criteria
		 * 
		 * @return the value of criteria
		 */
		private String getValue() {
			return value;
		}

		/**
		 * Assign a value of criteria
		 * 
		 * @param value the value to set
		 */
		public Builder setValue(String value) {
			this.value = value;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public HTTPParameter build() {
			return new HTTPParameter(this);
		}
	}

	/**
	 * Encapsulate constructor
	 * 
	 * @param builder The builder to build a HTTP parameter object
	 */
	private HTTPParameter(Builder builder){
		super(builder.getFieldName(), QueryCriteriaType.Equals, builder.getValue());
	}
	
	/**
	 * Creates a HTTP parameter builder
	 * 
	 * @param fieldName The name of filter
	 * 
	 * @return The HTTP parameter builder
	 */
	public static Builder build(String fieldName){
		return new Builder(fieldName);
	}
}
