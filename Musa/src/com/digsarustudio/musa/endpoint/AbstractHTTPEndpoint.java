/**
 * 
 */
package com.digsarustudio.musa.endpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.Dson;
import com.digsarustudio.musa.encoder.DsonJS;
import com.digsarustudio.musa.encoder.InvalidJSONFormatException;
import com.digsarustudio.musa.endpoint.connector.CollectionResponse;
import com.digsarustudio.musa.endpoint.connector.Connector;
import com.digsarustudio.musa.endpoint.connector.ParameterSerializable;
import com.digsarustudio.musa.endpoint.connector.QueryFilter;
import com.digsarustudio.musa.endpoint.connector.http.HTTPConnector;
import com.digsarustudio.musa.endpoint.connector.http.HTTPParameter;
import com.digsarustudio.musa.endpoint.connector.response.DataDeleteFailedException;
import com.digsarustudio.musa.endpoint.connector.response.DataInsertFailedException;
import com.digsarustudio.musa.endpoint.connector.response.DataRetrieveFailedException;
import com.digsarustudio.musa.endpoint.connector.response.DataUpdateFailedException;
import com.digsarustudio.musa.endpoint.connector.response.QueryResponse;
import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The boilerplate of Endpoint
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
@SuppressWarnings("deprecation")
public abstract class AbstractHTTPEndpoint implements EndpointLegacy {
	Logger logger = Logger.getLogger(AbstractHTTPEndpoint.class.getName());	
		
	/**
	 * The URL of target service
	 */
	protected String url;
	
	protected String json;
	
	/**
	 * The key for API
	 */
	protected String apiKey;
	
	
	Dson dsonJs = new DsonJS();
	
	/**
	 * Constructs an endpoint object
	 * 
	 * @param url The URL of target service. 
	 * 			  It could be a relative URL for the same domain service or an absolute URL for the module service.
	 * 
	 * @param apiKey The API key for access of web service
	 */
	public AbstractHTTPEndpoint(String url, String apiKey) {
		if( url.contains("http://") || url.contains("https://") ){
			this.url = url;
		}else{
			this.url = GWT.getHostPageBaseURL() + url;
		}
		
		this.apiKey = apiKey;
	}
		
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#list(java.lang.String, java.lang.Integer, com.google.gwt.user.client.rpc.AsyncCallback)
	 */

	@Override
	public <T> void list(String cursor, Integer limit, final AsyncCallback<CollectionResponse<T>> callback) {
		List<QueryFilter> filters = new ArrayList<>();
		this.list(cursor, limit, filters, callback);		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#list(java.lang.String, java.lang.Integer, java.util.List, com.google.gwt.user.client.rpc.AsyncCallback)
	 */

	@Override
	public <T> void list(String cursor, Integer limit, List<QueryFilter> filters, final AsyncCallback<CollectionResponse<T>> callback) {
		if( null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);
		
		if(null == filters){
			filters = new ArrayList<>();
		}
		
		filters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		
		if(null != cursor)
			filters.add( HTTPParameter.build(HTTPParameters.getInstance().getCursor()).setValue(cursor).build() );
		
		if(null != limit)
			filters.add( HTTPParameter.build(HTTPParameters.getInstance().getLimit()).setValue(limit.toString()).build() );			
		
		
		connector.list(filters, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);				
			}

			@Override
			public void onSuccess(String result) {
				CollectionResponse<T> response = null;
				
				try {
					response = parseListResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataRetrieveFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);			
			}
		});		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#list(java.lang.String, java.lang.Integer, com.google.gwt.core.client.JavaScriptObject, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T extends JavaScriptObject, E> void list(String cursor, Integer limit, T object, final AsyncCallback<CollectionResponse<E>> callback) {
		if( null == object || null == this.apiKey ){
			return;
		}
		
		Connector connector = new HTTPConnector(this.url);
		
		List<QueryFilter> parameters = new ArrayList<>();		
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );

		this.json = this.dsonJs.toJSON(object);
	
		connector.list(parameters, this.json, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {							
				CollectionResponse<E> response = null;
				
				try {
					response = parseListResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataRetrieveFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});
		
	}


	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#insert(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T extends ParameterSerializable, E> void insert(T object, final AsyncCallback<E> callback) {
		if( null == object || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);		
		List<QueryFilter> parameters = object.toHTTPParameter();	
		parameters.add(0, HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		
		connector.insert(parameters, new AsyncCallback<String>() {			
			@Override
			public void onSuccess(String result) {
				E response = null;
				
				try {
					response = parseInsertResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataInsertFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.Endpoint#insert(com.google.gwt.core.client.JavaScriptObject, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T extends JavaScriptObject, E> void insert(T object, final AsyncCallback<E> callback) {
		if( null == object || null == this.apiKey ){
			return;
		}
		
		Connector connector = new HTTPConnector(this.url);
		
		List<QueryFilter> parameters = new ArrayList<>();		
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );

		this.json = this.dsonJs.toJSON(object);
		
		connector.insert(parameters, this.json, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				E response = null;
				
				try {
					response = parseInsertResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataInsertFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});
	}


	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#get(java.lang.String, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T> void get(String key, final AsyncCallback<T> callback) {
		if( null == key || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);
		List<QueryFilter> parameters = new ArrayList<>();
		
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getKey()).setValue(key).build() );
		
		connector.get(parameters, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess(String result) {
				T response = null;
				
				try {
					response = parseGetResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataRetrieveFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);							
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#get(java.lang.Integer, com.google.gwt.user.client.rpc.AsyncCallback)
	 */

	@Override
	public <T> void get(Integer id, final AsyncCallback<T> callback) {
		if( null == id || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);
		List<QueryFilter> parameters = new ArrayList<>();
		
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getId()).setValue(id.toString()).build() );
		
		connector.get(parameters, new AsyncCallback<String>() {
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}

			@Override
			public void onSuccess(String result) {
				T response = null;
				
				try {
					response = parseGetResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataRetrieveFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);							
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#update(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */

	@Override
	public <T extends ParameterSerializable, E> void update(T source, final AsyncCallback<E> callback) {
		if( null == source || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);		
		List<QueryFilter> parameters = source.toHTTPParameter();	
		parameters.add(0, HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		
		connector.update(parameters, new AsyncCallback<String>() {			
			@Override
			public void onSuccess(String result) {
				E response = null;
				
				try {
					response = parseUpdateResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataUpdateFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.Endpoint#update(com.google.gwt.core.client.JavaScriptObject, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T extends JavaScriptObject, E> void update(T source, final AsyncCallback<E> callback) {
		if( null == source || null == this.apiKey ){
			return;
		}
		
		Connector connector = new HTTPConnector(this.url);
		
		List<QueryFilter> parameters = new ArrayList<>();		
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );

		this.json = this.dsonJs.toJSON(source);
		
		connector.update(parameters, this.json, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				E response = null;
				
				try {
					response = parseUpdateResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataUpdateFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#remove(java.lang.Object)
	 */
	@Override
	public <T> void remove(List<QueryFilter> filters, final AsyncCallback<T> callback) {
		if( null == filters || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);			
		filters.add(0, HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		
		connector.delete(filters, new AsyncCallback<String>() {			
			@Override
			public void onSuccess(String result) {
				T response = null;
				
				try {
					response = parseRemoveResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataDeleteFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#remove(java.lang.String)
	 */
	@Override
	public <T> void remove(String key, final AsyncCallback<T> callback) {
		if( null == key || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);		
		List<QueryFilter> parameters = new ArrayList<>();	
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getKey()).setValue(key).build() );
		
		connector.delete(parameters, new AsyncCallback<String>() {			
			@Override
			public void onSuccess(String result) {
				T response = null;
				
				try {
					response = parseRemoveResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataDeleteFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.connector.Endpoint#remove(java.lang.Integer)
	 */
	@Override
	public <T> void remove(Integer id, final AsyncCallback<T> callback) {
		if( null == id || null == this.apiKey )
			return;
		
		Connector connector = new HTTPConnector(this.url);		
		List<QueryFilter> parameters = new ArrayList<>();	
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getId()).setValue(id.toString()).build() );
		
		connector.delete(parameters, new AsyncCallback<String>() {			
			@Override
			public void onSuccess(String result) {			
				T response = null;
				
				try {
					response = parseRemoveResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataDeleteFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);				
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.Endpoint#remove(com.google.gwt.core.client.JavaScriptObject, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T extends JavaScriptObject, E> void remove(T source, final AsyncCallback<E> callback) {
		if( null == source || null == this.apiKey ){
			return;
		}
		
		Connector connector = new HTTPConnector(this.url);
		
		List<QueryFilter> parameters = new ArrayList<>();		
		parameters.add( HTTPParameter.build(HTTPParameters.getInstance().getAPIKey()).setValue(this.apiKey).build() );

		this.json = this.dsonJs.toJSON(source);
		
		connector.delete(parameters, this.json, new AsyncCallback<String>() {
			@Override
			public void onSuccess(String result) {
				E response = null;
				
				try {
					response = parseRemoveResponse(result);
					
				//If the input is not valid JSON
				} catch (IllegalArgumentException e) {
					callback.onFailure(new InvalidJSONFormatException("The returned string is not a valid JSON string. (" + result + ")\nPlease snapshot this message and email to administrator."));
					return;
				} catch (DataDeleteFailedException e) {
					callback.onFailure(e);
					return;
				}finally{
					
				}

				callback.onSuccess(response);
			}
			
			@Override
			public void onFailure(Throwable caught) {
				if( null == callback )
					return;
				
				callback.onFailure(caught);
			}
		});		
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#getCurrentServiceURL()
	 */
	@Override
	public String getCurrentServiceURL() {
		return this.url;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#getCurrentJSONString()
	 */
	@Override
	public String getCurrentJSONString() {
		return this.json;
	}

	/**
	 * Parse the response text into an query response object
	 * 
	 * @param responseText The response text from the service
	 * 
	 * @return An collection of response object from server
	 * 
	 * @throws DataRetrieveFailedException  When the target data cannot be retrieved from database
	 */
	abstract protected <T> CollectionResponse<T> parseListResponse(String responseText) throws DataRetrieveFailedException;
		
	/**
	 * Parse the response text from inserting into a specific response object
	 * 
	 * @param responseText The response text from the service
	 * 
	 * @return A specific response object
	 * 
	 * @throws DataInsertFailedException When the target data cannot be inserted into database.
	 */
	abstract protected <T> T parseInsertResponse(String responseText) throws DataInsertFailedException;
	
	/**
	 * Parse the response text from GET into a specific response object
	 * 
	 * @param responseText The response text from the service
	 * 
	 * @return A specific response object
	 * 
	 * @throws DataRetrieveFailedException When the target data cannot be retrieved from database 
	 */
	abstract protected <T> T parseGetResponse(String responseText) throws DataRetrieveFailedException;
	
	/**
	 * Parse the response text form updating into a specific response object
	 * 
	 * @param responseText The response text from the service
	 * 
	 * @return A specific response object
	 * 
	 * @throws DataUpdateFailedException When the data cannot be updated to database
	 */
	abstract protected <T> T parseUpdateResponse(String responseText) throws DataUpdateFailedException;
	
	/**
	 * Parse the response text from removing into a specific response object
	 * 
	 * @param responseText The response text from the service
	 * 
	 * @return A specific response object
	 * 
	 * @throws DataDeleteFailedException When the data cannot be deleted from database.
	 */
	abstract protected <T> T parseRemoveResponse(String responseText) throws DataDeleteFailedException;
	
	/**
	 * Returns true if there is no error returned, otherwise false returned.
	 * 
	 * @param response The response object returned from server
	 * 
	 * @return true if there is no error returned, otherwise false returned.
	 */
	abstract protected <T> Boolean isNoError(QueryResponse<T> response);
}
