/**
 * 
 */
package com.digsarustudio.musa.endpoint;

import java.util.List;

import com.digsarustudio.musa.endpoint.connector.CollectionResponse;
import com.digsarustudio.musa.endpoint.connector.ParameterSerializable;
import com.digsarustudio.musa.endpoint.connector.QueryFilter;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * Defining the operations of Endpoint
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 * @deprecated	Please use {@link Endpoint} instead.<br>
 *
 */
public interface EndpointLegacy {	
	/**
	 * Returns the query result according to the pages and items per pages.
	 * 
	 * @param cursor The cursor indicates the first item in this page
	 * @param limit The number of items per page
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return The collection of target object
	 * 
	 */
	<T> void list(String cursor, Integer limit, AsyncCallback<CollectionResponse<T>> callback);
	
	/**
	 * Returns the query result according to the pages and items per pages with the filters.
	 * 
	 * @param cursor The cursor indicates the first item in this page
	 * @param limit The number of items per page
	 * @param filters The filters indicate what sort of data could be returned.
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return The filtered data
	 * 
	 */
	<T> void list(String cursor, Integer limit, List<QueryFilter> filters, AsyncCallback< CollectionResponse<T>> callback);

	/**
	 * Returns the query results according to the criteria setup in the object field.
	 * 
	 * @param cursor
	 * @param limit
	 * @param object
	 * @param callback
	 * 
	 */
	<T extends JavaScriptObject, E> void list(String cursor, Integer limit, T object, final AsyncCallback<CollectionResponse<E>> callback);	
	
	/**
	 * To insert a target object
	 * 
	 * @param object the target object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return the object inserted exactly
	 * 
	 */
	<T extends ParameterSerializable, E> void insert(T object, AsyncCallback<E> callback);

	/**
	 * To insert a target object
	 * 
	 * @param object the target object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return the object inserted exactly
	 * 
	 */
	<T extends JavaScriptObject, E> void insert(T object, AsyncCallback<E> callback);
	
	/**
	 * Returns a object according to its key
	 * 
	 * @param key The key of target object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return a object identified the assigned key
	 * 
	 */
	<T> void get(String key, AsyncCallback<T> callback);
	
	/**
	 * Returns a object according to its id
	 * 
	 * @param id The id of target object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return a object identified by the assigned id
	 * 
	 */
	<T> void get(Integer id, AsyncCallback<T> callback);
	
	/**
	 * To update the target object by reference object
	 * 
	 * @param source The reference object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return the object updated exactly
	 * 
	 */
	<T extends ParameterSerializable, E> void update(T source, AsyncCallback<E> callback);
	
	/**
	 * To update the target object by reference object
	 * 
	 * @param source The reference object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return the object updated exactly
	 * 
	 */
	<T extends JavaScriptObject, E>  void update(T source, AsyncCallback<E> callback);
	
	/**
	 * To remove target object according to its entity
	 * 
	 * @param target The target object to remove
	 * 
	 */
	<T> void remove(List<QueryFilter> filters, AsyncCallback<T> callback);
	
	/**
	 * To remove target object according to its key
	 * 
	 * @param key The key of target object
	 * 
	 */
	<T> void remove(String key, AsyncCallback<T> callback);
	
	/**
	 * To remove target object according to its id
	 * 
	 * @param id The id of target object
	 * 
	 */
	<T> void remove(Integer id, AsyncCallback<T> callback);	
	
	/**
	 * To remove the target object by reference object
	 * 
	 * @param source The reference object
	 * @param callback The callback function to receive the returning object
	 * 
	 * @return the object updated exactly
	 * 
	 */
	<T extends JavaScriptObject, E>  void remove(T source, AsyncCallback<E> callback);
	
	/**
	 * 
	 * @return
	 * 
	 */
	String getCurrentServiceURL();
	
	/**
	 * 
	 * @return
	 * 
	 */
	String getCurrentJSONString();
}
