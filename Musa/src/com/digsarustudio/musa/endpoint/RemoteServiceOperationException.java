/**
 * 
 */
package com.digsarustudio.musa.endpoint;

/**
 * This exception represents that an error occurred when the user tried to use a remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * 
 * @deprecated	1.0.2 Please use Musaceae-1.0.3 instead.<br>
 *
 */
@SuppressWarnings("serial")
public class RemoteServiceOperationException extends Exception {

	/**
	 * 
	 */
	public RemoteServiceOperationException() {

	}

	/**
	 * @param arg0
	 */
	public RemoteServiceOperationException(String arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 */
	public RemoteServiceOperationException(Throwable arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public RemoteServiceOperationException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public RemoteServiceOperationException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

}
