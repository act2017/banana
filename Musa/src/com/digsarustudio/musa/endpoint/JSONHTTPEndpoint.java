/**
 * 
 */
package com.digsarustudio.musa.endpoint;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.digsarustudio.banana.utils.Dson;
import com.digsarustudio.musa.encoder.DsonJS;
import com.digsarustudio.musa.endpoint.connector.CollectionResponse;
import com.digsarustudio.musa.endpoint.connector.response.DataDeleteFailedException;
import com.digsarustudio.musa.endpoint.connector.response.DataInsertFailedException;
import com.digsarustudio.musa.endpoint.connector.response.DataRetrieveFailedException;
import com.digsarustudio.musa.endpoint.connector.response.DataUpdateFailedException;
import com.digsarustudio.musa.endpoint.connector.response.ListResult;
import com.digsarustudio.musa.endpoint.connector.response.QueryResponse;
import com.digsarustudio.musa.endpoint.connector.response.QueryStatusCodes;
import com.digsarustudio.musa.endpoint.connector.response.json.ListResultJSO;
import com.digsarustudio.musa.endpoint.connector.response.json.QueryResponseJSO;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * Represents a HTTP endpoint returning JSON format result.
 * 
 * The type E is a javascript object used to retrieve data from JSON string.
 * And It must implement the interface which the endpoint uses to get the retrieved data from CollectionResponse. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	The new HTTP endpoint for {@link JavaScriptObject} under development now.<br>
 */
public class JSONHTTPEndpoint<E extends JavaScriptObject> extends AbstractHTTPEndpoint implements EndpointLegacy {
	Logger logger = Logger.getLogger(JSONHTTPEndpoint.class.getName());
	Dson	dsonJs = new DsonJS();
			
	/**
	 * Constructs a HTTP endpoint returning JSON format result
	 * 
	 * @param url The URL of target web service
	 * @param apiKey The API key to access the web service
	 */
	public JSONHTTPEndpoint(String url, String apiKey) {
		super(url, apiKey);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.AbstractHTTPEndpoint#parseListResponse(java.lang.String)
	 */
	@Override
	protected <T> CollectionResponse<T> parseListResponse(String responseText) throws DataRetrieveFailedException {
		if(!this.dsonJs.isJSONString(responseText)){
			throw new IllegalArgumentException();
		}
		
		
		QueryResponseJSO< ListResultJSO<T> > responseJSO = this.dsonJs.fromJSON(responseText);
		
		if( !this.isNoError(responseJSO) ){
			throw new DataRetrieveFailedException(this.formatExceptionMessage(responseJSO.getStatusCode(), responseJSO.getErrorMessage()));
		}
		
		
		ListResult<T> result = responseJSO.getResult();
		List<T> retrievedItems = new ArrayList<>();
		Integer counter = 0;
		T retrievedItem = null;		
		
		while( null != (retrievedItem = (T)result.getResult(counter++)) ){
			retrievedItems.add(retrievedItem);
		}		
		
		return CollectionResponse.<T>builder()
								 .setNextPageToken(new Integer(result.getNextCursor()).toString())
								 .setItems(retrievedItems)
								 .setTotalCount(result.getTotalCount())
								 .build()
		;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.AbstractHTTPEndpoint#parseInsertResponse(java.lang.String)
	 */
	@Override
	protected <T> T parseInsertResponse(String responseText) throws DataInsertFailedException {
		if(!this.dsonJs.isJSONString(responseText)){
			throw new IllegalArgumentException();
		}
		
		QueryResponseJSO<T> responseJSO = this.dsonJs.fromJSON(responseText);
		
		if( !this.isNoError(responseJSO) ){		
			throw new DataInsertFailedException(this.formatExceptionMessage(responseJSO.getStatusCode(), responseJSO.getErrorMessage()));
		}
		
		return responseJSO.getResult();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.AbstractHTTPEndpoint#parseGetResponse(java.lang.String)
	 */
	@Override
	protected <T> T parseGetResponse(String responseText) throws DataRetrieveFailedException {
		if(!this.dsonJs.isJSONString(responseText)){
			throw new IllegalArgumentException();
		}
		
		QueryResponseJSO<T> responseJSO = this.dsonJs.fromJSON(responseText);
		
		if( !this.isNoError(responseJSO) ){		
			throw new DataRetrieveFailedException(this.formatExceptionMessage(responseJSO.getStatusCode(), responseJSO.getErrorMessage()));
		}
		
		return responseJSO.getResult();
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.AbstractHTTPEndpoint#parseUpdateResponse(java.lang.String)
	 */
	@Override
	protected <T> T parseUpdateResponse(String responseText) throws DataUpdateFailedException {
		if(!this.dsonJs.isJSONString(responseText)){
			throw new IllegalArgumentException();
		}
		
		QueryResponseJSO<T> responseJSO = this.dsonJs.fromJSON(responseText);		
		
		if( !this.isNoError(responseJSO) ){
			throw new DataUpdateFailedException(this.formatExceptionMessage(responseJSO.getStatusCode(), responseJSO.getErrorMessage()));
		}
		
		return responseJSO.getResult();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.togauto.shared.query.AbstractHTTPEndpoint#parseRemoveResponse(java.lang.String)
	 */
	@Override
	protected <T> T parseRemoveResponse(String responseText) throws DataDeleteFailedException {
		if(!this.dsonJs.isJSONString(responseText)){
			throw new IllegalArgumentException();
		}

		QueryResponseJSO<T> responseJSO = this.dsonJs.fromJSON(responseText);		
		
		if( !this.isNoError(responseJSO) ){
			throw new DataDeleteFailedException(this.formatExceptionMessage(responseJSO.getStatusCode(), responseJSO.getErrorMessage()));
		}
		
		return responseJSO.getResult();
	}	
		
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.AbstractHTTPEndpoint#isNoError(com.digsarustudio.musa.connector.response.QueryResponse)
	 */
	@Override
	protected <T> Boolean isNoError(QueryResponse<T> response) {
		return (QueryStatusCodes.NoError == QueryStatusCodes.fromValue(response.getStatusCode()));
	}

	/**
	 * To format the exception message
	 * 
	 * @param errorCode The error code
	 * @param errorMessage The error message
	 * 
	 * @return A well-formated error message
	 */
	private String formatExceptionMessage(Integer errorCode, String errorMessage){
		return "\n" + this.getClass().getName() + "[" + errorCode + "]: " + errorMessage + ".";
	}
}
