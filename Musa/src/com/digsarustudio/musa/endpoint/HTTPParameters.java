/**
 * 
 */
package com.digsarustudio.musa.endpoint;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;
import com.digsarustudio.musa.endpoint.connection.parameters.APIToken;
import com.digsarustudio.musa.endpoint.connection.parameters.Cursor;
import com.digsarustudio.musa.endpoint.connection.parameters.MaxRowCount;

/**
 * Represents the name of HTTP parameter.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	1.0.15 Please use {@link APIToken}, {@link Cursor}, {@link MaxRowCount}, or the subclass of 
 * 						{@link RequestParameter} instead
 */
public class HTTPParameters {
	/**
	 * @deprecated 1.0.15 unused
	 */
	private String token 	= "token";
	private String cursor 	= "cursor";
	private String limit	= "limit";
	private String apiKey	= "apiToken";
	/**
	 * @deprecated 1.0.15 unused
	 */
	private String key		= "key";
	/**
	 * @deprecated 1.0.15 unused
	 */
	private String id		= "id";
	/**
	 * @deprecated 1.0.15 unused
	 */
	private String orderBy	= "orderBy";
	/**
	 * @deprecated 1.0.15 unused
	 */
	private String ordering = "ordering";

	/**
	 * The instance	
	 */
	private static HTTPParameters instance;

	
	/**
	 * Returns the instance of HTTP parameters object
	 * 
	 * @return the instance of HTTP parameters object
	 */
	public static HTTPParameters getInstance(){
		if( null == instance )
			instance = new HTTPParameters();
		
		return instance;
	}
	
	/**
	 * Encapsulate constructor
	 */
	private HTTPParameters() {
	}

	/**
	 * Returns the name of token
	 * 
	 * @return the name of token
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Returns the name of cursor
	 * 
	 * @return the name of cursor
	 */
	public String getCursor() {
		return cursor;
	}

	/**
	 * Returns the name of limit
	 * 
	 * @return the name of limit
	 */
	public String getLimit() {
		return limit;
	}

	/**
	 * Returns the name of API key
	 * 
	 * @return The name of API key
	 */
	public String getAPIKey(){
		return this.apiKey;
	}


	/**
	 * Returns the name of key
	 * 
	 * @return the name of key
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public String getKey() {
		return key;
	}
	


	/**
	 * Returns the name of id
	 * 
	 * @return the name of id
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public String getId() {
		return id;
	}


	/**
	 * Returns the name of orderBy 
	 * @return the name of orderBy
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public String getOrderBy() {
		return orderBy;
	}
	

	/**
	 * Assign a name to orderBy 
	 * @param orderBy the name to orderBy to set
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	

	/**
	 * Returns the name of ordering
	 * @return the name of ordering
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public String getOrdering() {
		return ordering;
	}
	

	/**
	 * Assign a name to ordering
	 * @param ordering the name to ordering to set
	 * 
	 * @deprecated 1.0.15 unused
	 */
	public void setOrdering(String ordering) {
		this.ordering = ordering;
	}	
}
