/**
 * 
 */
package com.digsarustudio.musa.endpoint;

import com.digsarustudio.musa.endpoint.connection.RESTConnection;
import com.digsarustudio.musa.endpoint.connection.http.HTTPRequestException;
import com.digsarustudio.musa.endpoint.connection.parameters.APIToken;
import com.digsarustudio.musa.endpoint.connection.parameters.Cursor;
import com.digsarustudio.musa.endpoint.connection.parameters.MaxRowCount;
import com.digsarustudio.musa.endpoint.connection.response.CollectionResponse;
import com.digsarustudio.musa.endpoint.url.URL;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The subclass represents a endpoint which handles a RESTful connection with remote service<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * @deprecated	The advance version will be implemented in Banana.<br>
 */
public abstract class RESTEndpoint<T extends JavaScriptObject, E extends JavaScriptObject> implements Endpoint<T, E> {
	protected RESTConnection connection = null;
	protected URL			 url		= null;
	
	/**
	 * 
	 */
	public RESTEndpoint() {
		this.init();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#setAPIToken(java.lang.String)
	 */
	@Override
	public void setAPIToken(String token) {
		this.url.addParameter(APIToken.builder().setValue(token)
												.build());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#insert(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void insert(T data, AsyncCallback<E> callback) throws EndpointOperationException {
		try {
			this.connection.sendPostRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#update(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void update(T data, AsyncCallback<E> callback) throws EndpointOperationException {
		try {
			this.connection.sendPutRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#update(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback, java.lang.Integer)
	 */
	@Override
	public void update(T data, AsyncCallback<E> callback, Integer maxRowCount) throws EndpointOperationException {
		try {
			this.connection.sendPutRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#list(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void list(T data, AsyncCallback<CollectionResponse<E>> callback) throws EndpointOperationException {
		try {
			this.connection.sendGetRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#list(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback, java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public void list(T data, AsyncCallback<CollectionResponse<E>> callback, Integer cursor, Integer maxRowCount) throws EndpointOperationException {
		try {
			this.connection.sendGetRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#delete(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void delete(T data, AsyncCallback<E> callback) throws EndpointOperationException {
		try {
			this.connection.sendDeleteRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#delete(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback, java.lang.Integer)
	 */
	@Override
	public void delete(T data, AsyncCallback<E> callback, Integer maxRowCount) throws EndpointOperationException {
		try {
			this.connection.sendDeleteRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#get(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public void get(T data, AsyncCallback<E> callback) throws EndpointOperationException {
		try {
			this.connection.sendGetRequest(data, callback);
		} catch (HTTPRequestException e) {
			throw new EndpointOperationException(e);
		}

	}

	/**
	 * Assigns a URL for connection 
	 * 
	 * @param url a URL for connection
	 */
	protected void setURL(URL url) {
		this.url = url;
		
		this.connection = new RESTConnection();
		this.connection.setURL(url);
	}
	
	/**
	 * Assigns the maximum row count to affect the query
	 * 
	 * @param count the maximum row count to affect the query
	 */
	protected void setMaxRowCount(Integer count){
		this.url.addParameter(MaxRowCount.builder().setValue(count.toString())
												   .build());
	}
	
	/**
	 * Assigns the 1st row of the search
	 * 
	 * @param cursor the 1st row of the search
	 */
	protected void setCursor(Integer cursor){
		this.url.addParameter(Cursor.builder().setValue(cursor.toString())
											  .build());
	}
	
	/**
	 * To clear all of parameters
	 */
	protected void clearParameters(){
		this.url.clearParameters();
	}
	
	protected void init(){
		this.setupURL();
	}
	
	/**
	 * To setup the URL
	 */
	abstract protected void setupURL();
}
