/**
 * 
 */
package com.digsarustudio.musa.endpoint.url;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * The base class of {@link URL} which handles protocol, domain name, service path, and parameters.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public abstract class BaseURL implements URL {	
	protected ApplicationProtocolType	protocol	= null;
	protected String					domain		= null;
	protected String					path		= null;
	protected List<RequestParameter>	params		= null;
	
	/**
	 * 
	 */
	public BaseURL(ApplicationProtocolType protocol) {
		this.protocol = protocol;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getProtocol()
	 */
	@Override
	public ApplicationProtocolType getProtocol() {
		return this.protocol;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#setDomainName(java.lang.String)
	 */
	@Override
	public void setDomainName(String name) {
		this.domain = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getDomainName()
	 */
	@Override
	public String getDomainName() {

		return this.domain;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getPath()
	 */
	@Override
	public String getPath() {

		return this.path;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#setPath(java.lang.String)
	 */
	@Override
	public void setPath(String path) {
		this.path = path;		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#addParameter(com.digsarustudio.musa.endpoint.RequestParameter)
	 */
	@Override
	public void addParameter(RequestParameter param) {
		if(null == this.params){
			this.params = new ArrayList<>();
		}
		
		this.params.add(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#setParameters(java.util.List)
	 */
	@Override
	public void setParameters(List<RequestParameter> params) {
		this.params = params;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getParameters()
	 */
	@Override
	public List<RequestParameter> getParameters() {

		return this.params;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#clearParameters()
	 */
	@Override
	public void clearParameters() {
		this.params.clear();
		this.params = null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getURL()
	 */
	@Override
	public String getURL() {
		return formatPathPart(this.protocol.getValue(), this.domain, this.path);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#getEncodedURL()
	 */
	@Override
	public String getEncodedURL() {
		String queryString = this.formatRequestParameters(this.params);
		return formatURL(this.getURL(), com.google.gwt.http.client.URL.encodeQueryString(queryString));
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.URL#clone()
	 */
	@Override
	public URL clone() throws CloneNotSupportedException{
		throw new CloneNotSupportedException();
	}

	/**
	 * To decode the input URL and set those into the fields.
	 * 
	 * @param url The URL to be encoded
	 * 
	 * @throws URISyntaxException 
	 * 
	 */
	protected void decodeURL(String url) throws URISyntaxException {
		String[] l1 = url.split(URL.QUERY_LINKER);
		
		String[] l2 = l1[0].split("://");
		if( l2.length < 2 ){
			throw new URISyntaxException("protocol is not contained in " + url);
		}
		this.protocol = ApplicationProtocolType.fromValue(l2[0]);
		String[] l3 = l2[1].split("/");
		if( l3.length < 2 ){
			this.domain = l2[1];
		}else{
			this.domain = l3[0];
			this.path	= l3[1];
		}
		
		if(l1.length > 1){
			String decoded = com.google.gwt.http.client.URL.decode(l1[1]);
			String[] params = decoded.split(URL.PARAMETER_LINKER);
			
			for (String paramString : params) {
				String[] param = paramString.split("=");
				
				RequestParameter.Builder builder = RequestParameter.builder();
				builder.setKey(param[0]);
				
				if(param.length > 1){
					builder.setValue(param[1]);
				}
				
				this.addParameter(builder.build());
			}
		}
	}

	/**
	 * Returns the parameters in the format as "?key1=value1&key2=value2" in plain text
	 * 
	 * @param params The parameters to format
	 * 
	 * @return the parameters in the format as "?key1=value1&key2=value2" in plain text
	 */
	protected String formatRequestParameters(List<RequestParameter> params){
		if(null == params){
			return "";
		}
		
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0;
		for (RequestParameter requestParameter : params) {
			if(0 != count++){
				builder.append(URL.PARAMETER_LINKER);
			}
			
			builder.append(StringFormatter.format("%s=%s", requestParameter.getKey(), requestParameter.getValue()));
		}
		
		return builder.toString();
	}
	
	protected String getQueryStringPart(String url){
		String[] tmp = url.split(URL.QUERY_LINKER);
		
		return (null != tmp && tmp.length > 1) ? tmp[1] : null;
	}
	
	protected String getPathPart(String url){
		String[] tmp = url.split(URL.QUERY_LINKER);
		
		return (null != tmp && tmp.length > 0) ? tmp[0] : null;
	}
	
	protected String formatURL(String urlPath, String queryString){
		return StringFormatter.format("%s%s%s%s", urlPath, URL.QUERY_LINKER, queryString);
	}
	
	protected String formatPathPart(String protocol, String domain, String path){
		return StringFormatter.format("%s://%s/%s", protocol, domain, path);
	}
	
}
