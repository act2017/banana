/**
 * 
 */
package com.digsarustudio.musa.endpoint.url;

import java.util.List;

import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * This class represents a url using HTTP protocol to communicate with remote server.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class HTTPURL extends BaseURL implements URL {
	private static final ApplicationProtocolType PROTOCOL = ApplicationProtocolType.HTTP; 

	/**
	 * 
	 * The object builder for {@link HTTPURL}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder implements URL.Builder {
		private HTTPURL result = null;

		public Builder() {
			this.result = new HTTPURL();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setDomainName(java.lang.String)
		 */
		@Override
		public Builder setDomainName(String name) {
			this.result.setDomainName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setPath(java.lang.String)
		 */
		@Override
		public Builder setPath(String path) {
			this.result.setPath(path);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#addParameter(com.digsarustudio.musa.endpoint.RequestParameter)
		 */
		@Override
		public Builder addParameter(RequestParameter param) {
			this.result.addParameter(param);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setParameters(java.util.List)
		 */
		@Override
		public Builder setParameters(List<RequestParameter> params) {
			this.result.setParameters(params);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setEncodedURL(java.lang.String)
		 */
		@Override
		public Builder setEncodedURL(String url) throws URISyntaxException {
			this.result.decodeURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.url.URL.Builder#setPlainURL(java.lang.String)
		 */
		@Override
		public Builder setPlainURL(String url) throws URISyntaxException {
			this.result.decodeURL(url);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public URL build() {

			return this.result;
		}

	}
	
	/**
	 * 
	 */
	private HTTPURL() {
		super(PROTOCOL);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.url.BaseURL#clone()
	 */
	@Override
	public URL clone() throws CloneNotSupportedException {

		return HTTPURL.builder().setDomainName(this.getDomainName())
								.setPath(this.getPath())
								.setParameters(this.getParameters())
								.build();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
