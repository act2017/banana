/**
 * 
 */
package com.digsarustudio.musa.endpoint;

/**
 * The subclass represents a endpoint which handles remote procedure call to communicate with remote 
 * service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * @deprecated	The advance version will be implemented in Banana.<br>
 *
 */
public abstract class RPCEndpoint<T, E> implements Endpoint<T, E> {
	private String apiToken = null;

	public RPCEndpoint(){
		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.Endpoint#setAPIToken(java.lang.String)
	 */
	@Override
	public void setAPIToken(String token) {
		this.apiToken = token;
		
	}

	/**
	 * Returns the API token which represents the identity of the user
	 * 
	 * @return the API token which represents the identity of the user
	 */
	protected String getAPIToken(){
		return this.apiToken;
	}
}
