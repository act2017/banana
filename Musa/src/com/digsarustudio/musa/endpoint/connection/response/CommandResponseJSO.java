/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

import com.digsarustudio.musa.endpoint.connector.response.DebugInfo;
import com.digsarustudio.musa.utility.JavascriptObjectCreater;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * The data transfer object of {@link CommandResponse}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class CommandResponseJSO<T> extends JavaScriptObject implements CommandResponse<T> {

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder
	 */
	public static class Builder<S> extends JavascriptObjectCreater<CommandResponseJSO<S>> implements CommandResponse.Builder<S> {
		private CommandResponseStatus			status			= CommandResponseStatus.unKnown;
		private String							message			= null;
		private DebugInfo						debugInfo		= null;
		private S								commandResult = null;
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setStatus(com.digsarustudio.musa.endpoint.connector.response.CommandResponseStatus)
		 */
		@Override
		public Builder<S> setStatus(CommandResponseStatus status) {
			this.status = status;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setErrorMessage(java.lang.String)
		 */
		@Override
		public Builder<S> setErrorMessage(String message) {
			this.message = message;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setDebugInfo(com.digsarustudio.musa.endpoint.connector.response.DebugInfo)
		 */
		@Override
		public Builder<S> setDebugInfo(DebugInfo info) {
			this.debugInfo = info;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setCommandResult(java.lang.Object)
		 */
		@Override
		public Builder<S> setCommandResult(S result) {
			this.commandResult = result;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CommandResponse<S> build() {
			CommandResponseJSO<S> dto = this.createObject();
			
			dto.setStatus(this.status);
			dto.setErrorMessage(this.message);
			dto.setDebugInfo(this.debugInfo);
			dto.setCommandResult(this.commandResult);
			
			return dto;
		}
	}	
	
	/**
	 * For JavascriptObject to use 
	 */
	protected CommandResponseJSO() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getStatus()
	 */
	@Override
	public final CommandResponseStatus getStatus() {
		return CommandResponseStatus.fromValue(this.getNativeStatus());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getErrorMessage()
	 */
	@Override
	public final native  String getErrorMessage() /*-{

		return this.message;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getDebugInfo()
	 */
	@Override
	public final native  DebugInfo getDebugInfo() /*-{

		return this.debugInfo;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getCommandResult()
	 */
	@Override
	public final native T getCommandResult() /*-{

		return this.result;
	}-*/;

	public final void setStatus(CommandResponseStatus status) {
		this.setNativeStatus(status.getValue().toString());
	}

	public final native void setErrorMessage(String message) /*-{

		this.message = message;
	}-*/;

	public final native void setDebugInfo(DebugInfo info) /*-{
		this.debugInfo = info;
	}-*/;

	public final native void setCommandResult(T result) /*-{
		this.result = result;
	}-*/;	
	
	public final native String getNativeStatus() /*-{
		return this.status;
	}-*/;
	
	public final native void setNativeStatus(String status) /*-{
		this.status = status;
	}-*/;
}
