/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

import java.util.ArrayList;
import java.util.Collection;

import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The java data object of {@link CollectionResponse}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * @deprecated	Please use {@link com.digsarustudio.banana.endpoint.javabean.CollectionResponse} instead.
 */
public class CollectionResponseJDO<T> implements CollectionResponse<T>, IsSerializable {
	/**
	 * 
	 * The object builder for {@link CollectionResponseJDO}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder<S> implements CollectionResponse.Builder<S> {
		private String nextPageToken = null;
		private Collection<S> items = null;

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose.Builder#setNextPageToken(java.lang.String)
		 */
		@Override
		public Builder<S> setNextPageToken(String token) {
			this.nextPageToken = token;
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose.Builder#setItems(java.util.Collection)
		 */
		@Override
		public Builder<S> setItems(Collection<S> items) {
			this.items = items;
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose.Builder#addItem(java.lang.Object)
		 */
		@Override
		public Builder<S> addItem(S item) {
			if(null == this.items){
				this.items = new ArrayList<>();
			}
			
			this.items.add(item);
			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CollectionResponse<S> build() {
			return new CollectionResponseJDO<>(this);
		}

		/**
		 * @return the nextPageToken
		 */
		private String getNextPageToken() {
			return nextPageToken;
		}

		/**
		 * @return the items
		 */
		private Collection<S> getItems() {
			return items;
		}
		
	}
	
	private String nextPageToken = null;
	private Collection<T> items = null;
	
	/**
	 * For GWT to use.
	 */
	private CollectionResponseJDO() {

	}
	
	private CollectionResponseJDO(Builder<T> builder) {
		this.items			= builder.getItems();
		this.nextPageToken	= builder.getNextPageToken();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose#getNextPageToken()
	 */
	@Override
	public String getNextPageToken() {

		return this.nextPageToken;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose#getItems()
	 */
	@Override
	public Collection<T> getItems() {

		return this.items;
	}
	
	/*
	 * To create a new builder
	 */
	public static <T> Builder<T> builder() {
		return new Builder<T>();
	}
}
