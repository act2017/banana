/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.digsarustudio.musa.utility.JavascriptObjectCreater;
import com.google.gwt.core.client.JavaScriptObject;

/**
 * The javascript object of {@link CollectionResponseJSO}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class CollectionResponseJSO<T> extends JavaScriptObject implements CollectionResponse<T> {
	/**
	 * 
	 * The object builder for {@link CollectionResponseJSO}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder<S> extends JavascriptObjectCreater<CollectionResponseJSO<S>> implements CollectionResponse.Builder<S> {
		private String nextPageToken = null;
		private Collection<S> items = null;

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose.Builder#setNextPageToken(java.lang.String)
		 */
		@Override
		public Builder<S> setNextPageToken(String token) {
			this.nextPageToken = token;
			
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose.Builder#setItems(java.util.Collection)
		 */
		@Override
		public Builder<S> setItems(Collection<S> items) {
			this.items = items;
			
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose.Builder#addItem(java.lang.Object)
		 */
		@Override
		public Builder<S> addItem(S item) {
			if(null == this.items){
				this.items = new ArrayList<>();
			}
			
			this.items.add(item);
			
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CollectionResponse<S> build() {
			CollectionResponseJSO<S> dto = this.createObject();
			
			dto.setNextPageToken(this.nextPageToken);
			dto.setItems(this.items);
			
			return dto;
		}		
	}
	
	/**
	 * Do not remove. For JSNI to use
	 */
	protected CollectionResponseJSO() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose#getNextPageToken()
	 */
	@Override
	public final native String getNextPageToken() /*-{

		return this.nextPageToken;
	}-*/;

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CollectionRespose#getItems()
	 */
	@Override
	public final Collection<T> getItems() {
		List<T> rtns = new ArrayList<>();
		
		int counter = 0;		
		T data = null;
		
		while( null != (data = this.getItem(counter++)) ){
			rtns.add(data);
		}
		
		return rtns;
	};

	public final native T getItem(int index) /*-{
		if(null == this.items || index >= this.items.length){
			return null;
		}
		
		return this.items[index];		
	}-*/;
	
	public final native void setNextPageToken(String token) /*-{
		this.nextPageToken = token;
	}-*/;
	
	public final void setItems(Collection<T> items){
		for (T item : items) {
			this.addItem(item);
		}
	}
	
	public final native void addItem(T item) /*-{
		if(null == this.items){
			this.items = [];
		}
		
		this.items.push(item);
	}-*/;
	
	/*
	 * To create a new builder
	 */
	public static <T> Builder<T> builder() {
		return new Builder<T>();
	}
}
