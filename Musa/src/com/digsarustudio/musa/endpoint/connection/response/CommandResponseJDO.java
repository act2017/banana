/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

import com.digsarustudio.musa.endpoint.connector.response.DebugInfo;
import com.google.gwt.user.client.rpc.IsSerializable;

/**
 * The java data object form for {@link CommandResponse}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class CommandResponseJDO<T> implements CommandResponse<T>, IsSerializable {
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder
	 */
	public static class Builder<S> implements CommandResponse.Builder<S> {
		private CommandResponseStatus			status			= CommandResponseStatus.unKnown;
		private String							message			= null;
		private DebugInfo						debugInfo		= null;
		private S								commandResult = null;
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setStatus(com.digsarustudio.musa.endpoint.connector.response.CommandResponseStatus)
		 */
		@Override
		public Builder<S> setStatus(CommandResponseStatus status) {
			this.status = status;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setErrorMessage(java.lang.String)
		 */
		@Override
		public Builder<S> setErrorMessage(String message) {
			this.message = message;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setDebugInfo(com.digsarustudio.musa.endpoint.connector.response.DebugInfo)
		 */
		@Override
		public Builder<S> setDebugInfo(DebugInfo info) {
			this.debugInfo = info;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse.Builder#setCommandResult(java.lang.Object)
		 */
		@Override
		public Builder<S> setCommandResult(S result) {
			this.commandResult = result;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public CommandResponse<S> build() {
			return new CommandResponseJDO<S>(this);
		}

		/**
		 * @return the message
		 */
		private String getMessage() {
			return message;
		}

		/**
		 * @return the status
		 */
		private CommandResponseStatus getStatus() {
			return status;
		}

		/**
		 * @return the debugInfo
		 */
		private DebugInfo getDebugInfo() {
			return debugInfo;
		}

		/**
		 * @return the commandResult
		 */
		private S getCommandResult() {
			return commandResult;
		}

	}
	
	private CommandResponseStatus			status			= CommandResponseStatus.unKnown;
	private String							message			= null;
	private DebugInfo						debugInfo		= null;
	private T								commandResult = null;

	/**
	 * For GWT to use
	 */
	private CommandResponseJDO() {

	}
	
	private CommandResponseJDO(Builder<T> builder) {
		this.status			= builder.getStatus();
		this.message		= builder.getMessage();
		this.debugInfo		= builder.getDebugInfo();
		this.commandResult= builder.getCommandResult();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getStatus()
	 */
	@Override
	public CommandResponseStatus getStatus() {

		return this.status;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getErrorMessage()
	 */
	@Override
	public String getErrorMessage() {

		return this.message;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getDebugInfo()
	 */
	@Override
	public DebugInfo getDebugInfo() {

		return this.debugInfo;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connector.response.CommandResponse#getCommandResult()
	 */
	@Override
	public T getCommandResult() {

		return this.commandResult;
	}

	/*
	 * To create a new builder
	 */
	public static <T> Builder<T> builder() {
		return new Builder<T>();
	}
}
