/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.EndpointLegacy;
import com.digsarustudio.musa.endpoint.connector.response.DebugInfo;

/**
 * The response of the command command from server service via HTTP.<br>
 * The {@link CommandResponseStatus} indicates if there was an error occurred while the command executed, the client side app should parse this result 
 * under {@link EndpointLegacy} and throws exception when this status is not {@link CommandResponseStatus#NoError} instead of return the status to the client side app.
 *
 * @param	T	The data object returned from server after an API call.<br>
 * 				If the command is INSERT, the object which has been inserted will be returned. The status code would be not NO_ERROR if the data cannot be written into database.<br>
 * 				If the command is UDPATE, the object which has been updated will be returned. The status code would be not NO_ERROR if the data cannot be written into database.<br>
 * 				If the command is DELETE, the object which has been deleted will be returned. The status code would be not NO_ERROR if the data cannot be deleted from database.<br>
 * 				If the command is LIST, there is a list of object returned carried by {@link CollectionResponse}. 
 * 				The status code would be not NO_ERROR if the data cannot be fetched from database.<br>
 *  
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public interface CommandResponse<T> {	
	/**
	 * 
	 * The builder for {@link CommandResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static interface Builder<T> extends ObjectBuilder<CommandResponse<T>> {
		/**
		 * Assign command status
		 * 
		 * @param status The command status
		 * 
		 * @return The instance of this builder
		 */
		Builder<T> setStatus(CommandResponseStatus status);
		
		/**
		 * Assigns the error message when an error occurred.
		 * 
		 * @param message The error message
		 * 
		 * @return The instance of this builder
		 */
		Builder<T> setErrorMessage(String message);
		
		/**
		 * Assigns a debug information
		 * 
		 * @param info The information for debugging.
		 * 
		 * @return The instance of this builder
		 */
		Builder<T> setDebugInfo(DebugInfo info);
		
		/**
		 * Assigns the result of command
		 * 
		 * @param result The result of command
		 * 
		 * @return The instance of this builder
		 */
		Builder<T> setCommandResult(T result);
	}
	
	/**
	 * Returns the status of the command
	 * 
	 * @return the status of the command
	 */
	public CommandResponseStatus getStatus();
	
	/**
	 * Returns the error message
	 * 
	 * @return the error message
	 */
	public String getErrorMessage();
	
	/**
	 * Returns the debug information
	 * 
	 * @return the debug information
	 */
	public DebugInfo getDebugInfo();
	
	/**
	 * Returns the result of command, it might be an object or a container which contains a list of objects.
	 * 
	 * @return the result of command, it might be an object or a container which contains a list of objects.
	 */
	public T getCommandResult();
}
