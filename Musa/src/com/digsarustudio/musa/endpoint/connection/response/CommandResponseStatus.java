/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

/**
 * The status returned by {@link CommandResponse} when a command complete
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public enum CommandResponseStatus {
	NoError(0000)
	
	//System
		,ProductCategoryNotExist(0001)
		,PermissionDenied(0002)
		
	//General	
		,ConnectionFailed(1001)
		,InvalidArgument(1002)
		,InvalidToken(1003)
		,NotSupportFunction(1004)
		
	//Database	
		,NonExistedDatabase(1101)
		,DuplicatedDatabase(1102)
		
	//Table	
		,NonExistedTable(1201)
		,DuplicatedTable(1202)
		
	//SQL	
		,InsertionFailed(1301)
		,UpdatingFailed(1302)
	    ,DeletionFailed(1303)
	    ,SelectionFailed(1304)	
		
	//Data	
		,DuplicatedData(2001)
		,NonExistedData(2002)
		
	//Customized - from 4	
		
		,unKnown(9999)
		;
		
		
		private Integer code = 0;
		
		private static CommandResponseStatus[] values = values();	
		
		private CommandResponseStatus(Integer code){
			this.code = code;
		}
		
		/**
		 * Returns the code of {@link CommandResponse}
		 * 
		 * @return the code of {@link CommandResponse}
		 */
		public Integer getValue(){
			return this.code;
		}
		
		/**
		 * Returns {@link CommandResponse} by code
		 * 
		 * @param code The code used to find the status
		 * 
		 * @return any {@link CommandResponse} excepting {@link #unKnown} if the code is valid, otherwise {@link #unKnown} returned
		 */
		public static CommandResponseStatus fromValue(Integer code){
			for (CommandResponseStatus status : values) {
				if( !status.getValue().equals(code) ){
					continue;
				}
				
				return status;
			}
			
			return unKnown; 
		}
		
		/**
		 * Returns {@link CommandResponse} by code
		 * 
		 * @param code The code used to find the status
		 * 
		 * @return any {@link CommandResponse} excepting {@link #unKnown} if the code is valid, otherwise {@link #unKnown} returned
		 */
		public static CommandResponseStatus fromValue(String code){					
			return CommandResponseStatus.fromValue(new Integer(code)); 
		}		
}
