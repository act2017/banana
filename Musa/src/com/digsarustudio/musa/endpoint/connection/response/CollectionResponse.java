/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.response;

import java.util.Collection;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.EndpointLegacy;

/**
 * The response object which contains a list of data transferring between client app and server service.<br>
 * If the returning data are more than one, please use this interface with the call of {@link EndpointLegacy}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 * @deprecated	Please use {@link com.digsarustudio.banana.endpoint.CollectionResponse} instead.<br>
 */
public interface CollectionResponse<T> {
	
	/**
	 * 
	 * The builder for {@link ConnectionResponse}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder<S> extends ObjectBuilder<CollectionResponse<S>> {
		/**
		 * Assigns the next page token for the next search
		 * 
		 * @param token The next page token for the next search
		 * 
		 * @return The instance of this builder
		 */
		Builder<S> setNextPageToken(String token);
		
		/**
		 * Assigns the list of items to carry
		 * 
		 * @param items The list of items to carry
		 * 
		 * @return The instance of this builder
		 */
		Builder<S> setItems(Collection<S> items);
		
		/**
		 * Assigns an item to carry
		 * 
		 * @param item The item to carry
		 * 
		 * @return The instance of this builder
		 */
		Builder<S> addItem(S item);
	}
	
	/**
	 * Returns the token of next page.
	 * 
	 * @return the token of next page.
	 */
	public String getNextPageToken();

	/**
	 * Returns the list of item transferred from server service or client app.
	 * 
	 * @return the list of item transferred from server service or client app.
	 */
	public Collection<T> getItems();
}
