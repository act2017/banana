/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.rpc;

import com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator;
import com.digsarustudio.musa.endpoint.connection.ServiceManipulator;
import com.digsarustudio.musa.endpoint.connection.http.HTTPRequestException;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This is a sample class showing how to implement a RPC service manipulator.<br>
 * Do not use this as a formal service manipulator.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public final class SampleRPCServiceManipulator extends ServiceManipulator implements RemoteServiceManipulator {
	//1. Declare the Remote Service Async interface here and instantiate it.
	//protected SampleRPCServiceAsync service = GWT.create(SampleRPCService.class);
	
	/**
	 * 
	 */
	public SampleRPCServiceManipulator() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#create(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void create(T data, AsyncCallback<E> callback) throws HTTPRequestException {
		//2. Calling the correct method of remote service and cast the input data as the correct data.
		//this.service.insert((Item)data, callback);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#read(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void read(T data, AsyncCallback<E> callback) {
		//2. Calling the correct method of remote service and cast the input data as the correct data.
		//this.service.list(cursor, limit, (Item)data, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#update(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void update(T data, AsyncCallback<E> callback) {
		//2. Calling the correct method of remote service and cast the input data as the correct data.
		//this.service.update((Item)data, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#delete(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void delete(T data, AsyncCallback<E> callback) {
		//2. Calling the correct method of remote service and cast the input data as the correct data.
		//this.service.delete((Item)data, callback);

	}

}
