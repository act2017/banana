/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class contains a value with the key to represent as a parameter for the request action to the 
 * remote server.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class RequestParameter {
	/**
	 * 
	 * The object builder for {@link RequestParameter}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder implements ObjectBuilder<RequestParameter> {
		private RequestParameter result = null;

		public Builder() {
			this.result = new RequestParameter();
		}

		/**
		 * @param key
		 * @see com.digsarustudio.musa.endpoint.connection.RequestParameter#setKey(java.lang.String)
		 */
		public Builder setKey(String key) {
			this.result.setKey(key);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.musa.endpoint.connection.RequestParameter#setValue(java.lang.String)
		 */
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public RequestParameter build() {
			return this.result;
		}

	}
	
	private String key		= null;
	private String value	= null;
	
	/**
	 * 
	 */
	protected RequestParameter() {
		
	}

	/**
	 * Returns the key of this parameter
	 * 
	 * @return the key of this parameter
	 */
	public String getKey() {
		return key;
	}

	/**
	 * Assigns a key to this parameter
	 * 
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Returns the value of this parameter
	 * 
	 * @return the value of this parameter
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Assigns a value to this parameter
	 * 
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
