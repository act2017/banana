/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection;

import java.util.List;

import com.digsarustudio.musa.endpoint.connection.parameters.APIToken;
import com.digsarustudio.musa.endpoint.connection.parameters.Cursor;
import com.digsarustudio.musa.endpoint.connection.parameters.MaxRowCount;
import com.digsarustudio.musa.endpoint.url.URL;

/**
 * The base class of {@link RemoteServiceManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public abstract class ServiceManipulator implements RemoteServiceManipulator {
	protected URL	url		= null;

	/**
	 * 
	 */
	public ServiceManipulator() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#setURL(com.digsarustudio.musa.endpoint.url.URL)
	 */
	@Override
	public void setURL(URL url) {
		this.url = url;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#setParameters(java.util.List)
	 */
	@Override
	public void setParameters(List<RequestParameter> params) {
		this.url.setParameters(params);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#addParameter(com.digsarustudio.musa.endpoint.connection.RequestParameter)
	 */
	@Override
	public void addParameter(RequestParameter param) {
		this.url.addParameter(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#clearParameters()
	 */
	@Override
	public void clearParameters() {
		this.url.clearParameters();

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#setAPIToken(java.lang.String)
	 */
	@Override
	public void setAPIToken(String token) {
		RequestParameter param = APIToken.builder().setValue(token).build();

		this.url.addParameter(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#setMaxDataCount(java.lang.Integer)
	 */
	@Override
	public void setMaxDataCount(Integer count) {
		RequestParameter param = MaxRowCount.builder().setValue(count.toString()).build();

		this.url.addParameter(param);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#setCursor(java.lang.Integer)
	 */
	@Override
	public void setCursor(Integer cursor) {
		RequestParameter param = Cursor.builder().setValue(cursor.toString()).build();

		this.url.addParameter(param);
	}	

	protected URL getURL(){
		return this.url;
	}
}
