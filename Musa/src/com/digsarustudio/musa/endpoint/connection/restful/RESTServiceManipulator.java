/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.restful;

import com.digsarustudio.musa.endpoint.connection.RESTConnection;
import com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator;
import com.digsarustudio.musa.endpoint.connection.ServiceManipulator;
import com.digsarustudio.musa.endpoint.connection.http.HTTPRequestException;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This class handles create, read, update, delete operations for the {@link RESTConnection}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class RESTServiceManipulator extends ServiceManipulator implements RemoteServiceManipulator {
	protected RESTConnection	connection		= null;

	/**
	 * 
	 */
	public RESTServiceManipulator() {
		this.connection = new RESTConnection();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#create(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void create(T data, AsyncCallback<E> callback) throws HTTPRequestException {
		this.connection.setURL(this.getURL());
		
		this.connection.sendPostRequest(data, callback);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#read(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void read(T data, AsyncCallback<E> callback) {
		this.connection.setURL(this.getURL());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#update(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void update(T data, AsyncCallback<E> callback) {
		this.connection.setURL(this.getURL());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.RemoteServiceManipulator#delete(java.lang.Object, com.google.gwt.user.client.rpc.AsyncCallback)
	 */
	@Override
	public <T, E> void delete(T data, AsyncCallback<E> callback) {
		this.connection.setURL(this.getURL());

	}

}
