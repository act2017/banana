/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.parameters;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * This class represents the begin row of the search.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public final class Cursor extends RequestParameter {
	/**
	 * 
	 * The object builder for {@link Cursor}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder implements ObjectBuilder<Cursor> {
		private Cursor result = null;

		public Builder() {
			this.result = new Cursor();
		}

		/**
		 * Assigns the begin row of the search
		 * 
		 * @param cursor The begin row of the search
		 * 
		 * @return The instance of the builder
		 */
		public Builder setValue(Integer cursor){
			this.result.setValue(cursor.toString());
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Cursor build() {
			return this.result;
		}

	}
	
	private static final String KEY	= "cursor";
	
	/**
	 * 
	 */
	private Cursor() {
		this.setKey(KEY);
	}

}
