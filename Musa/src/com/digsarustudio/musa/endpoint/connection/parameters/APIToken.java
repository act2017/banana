/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.parameters;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.connection.RequestParameter;
import com.digsarustudio.musa.endpoint.connection.http.HTTPConnection;

/**
 * This class represents as an API token passed to {@link HTTPConnection} as a {@link RequestParameter}<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public final class APIToken extends RequestParameter {
	
	/**
	 * 
	 * The object builder for {@link APIToken}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder implements ObjectBuilder<APIToken> {
		private APIToken result = null;

		public Builder() {
			this.result = new APIToken();
		}
		
		/**
		 * Assigns a value for this API token
		 * 
		 * @param value A value for this API token
		 * 
		 * @return The instance of this builder
		 */
		public Builder setValue(String value){
			this.result.setValue(value);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public APIToken build() {
			return this.result;
		}

	}
	
	private static final String	KEY	= "apiToken";
	
	/**
	 * 
	 */
	private APIToken() {
		this.setKey(KEY);
	}

}
