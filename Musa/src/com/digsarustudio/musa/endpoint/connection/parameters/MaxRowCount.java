/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.parameters;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.connection.RequestParameter;

/**
 * This class represents the maximum row count for the search.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public final class MaxRowCount extends RequestParameter {
	/**
	 * 
	 * The object builder for {@link MaxRowCount}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder implements ObjectBuilder<MaxRowCount> {
		private MaxRowCount result = null;

		public Builder() {
			this.result = new MaxRowCount();
		}

		/**
		 * Assigns a maximum row count for the search
		 * 
		 * @param maxRowCount The maximum row count for the search
		 * 
		 * @return The instance of the builder
		 */
		public Builder setValue(Integer maxRowCount){
			this.result.setValue(maxRowCount.toString());
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MaxRowCount build() {
			return this.result;
		}

	}
	
	private static final String KEY	= "maxRowCount";
	
	/**
	 * 
	 */
	private MaxRowCount() {
		this.setKey(KEY);
	}

}
