/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection;

import java.util.List;

import com.digsarustudio.musa.endpoint.connection.http.HTTPRequestException;
import com.digsarustudio.musa.endpoint.url.URL;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The sub-type handles {@link RESTConnection} and RPC connection to communicate with remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public interface RemoteServiceManipulator {
	/**
	 * Assigns a URL of the remote service to communicate.<br>
	 * 
	 * @param url A URL of the remote service to communicate.
	 */
	void setURL(URL url);
	
	/**
	 * Assigns a list of request parameters for the operations.<br>
	 * 
	 * @param params A list of request parameters for the operations.
	 */
	void setParameters(List<RequestParameter> params);
	
	/**
	 * Appends a request parameter for the operations.<br>
	 * 
	 * @param param A request parameter for the operations
	 */
	void addParameter(RequestParameter param);
	
	/**
	 * To clear all of the request parameters
	 */
	void clearParameters();
	
	/**
	 * Assigns an API token to show the identify of the user to do the operations.
	 * 
	 * @param token An API token showing the identify of the user to do the operations.
	 */
	void setAPIToken(String token);
	
	/**
	 * Assigns the maximum data count of the search for the {@link #read(Object, AsyncCallback)} operation.
	 * 
	 * @param count The maximum data count of the search for the {@link #read(Object, AsyncCallback)} operation.
	 */
	void setMaxDataCount(Integer count);
	
	/**
	 * Assigns a cursor for the {@link #read(Object, AsyncCallback)} operation.
	 * 
	 * @param cursor The cursor for the {@link #read(Object, AsyncCallback)} operation.
	 */
	void setCursor(Integer cursor);
	
	/**
	 * To create a particular data on the remote service
	 * 
	 * @param data The particular data to create
	 * 
	 * @param callback The callback to notify what happened on the request
	 * 
	 * @throws HTTPRequestException if there was an error occurred between the client side and remote service side  
	 */
	<T, E> void create(T data, AsyncCallback<E> callback) throws HTTPRequestException;
	
	/**
	 * To read a particular data from the remote service
	 * 
	 * @param data The particular data to read
	 * 
	 * @param callback The callback to notify what happened on the reqeust
	 */
	<T, E> void read(T data, AsyncCallback<E> callback);
	
	/**
	 * To update a particular data on the remote service
	 * 
	 * @param data The particular data to update
	 * 
	 * @param callback The callback to notify what happened on the reqeust
	 */
	<T, E> void update(T data, AsyncCallback<E> callback);
	
	/**
	 * To delete a particular data on the remote service
	 * 
	 * @param data The particular data to delete
	 * 
	 * @param callback The callback to notify what happened on the reqeust
	 */
	<T, E> void delete(T data, AsyncCallback<E> callback);
}
