/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection;

import com.digsarustudio.banana.utils.Dson;
import com.digsarustudio.banana.utils.StringFormatter;
import com.digsarustudio.musa.encoder.DsonJS;
import com.digsarustudio.musa.endpoint.connection.http.HTTPConnection;
import com.digsarustudio.musa.endpoint.connection.http.HTTPRequestException;
import com.digsarustudio.musa.endpoint.connection.http.RequestCallback;
import com.digsarustudio.musa.endpoint.connection.http.headers.ContentType;
import com.digsarustudio.musa.endpoint.connection.http.headers.HTTPMethodOverride;
import com.digsarustudio.musa.endpoint.connection.response.CommandResponse;
import com.digsarustudio.musa.endpoint.connection.response.CommandResponseStatus;
import com.digsarustudio.musa.endpoint.connector.response.DataRetrieveFailedException;
import com.digsarustudio.musa.endpoint.url.URL;
import com.google.gwt.core.client.JavaScriptObject;
import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * This class provides some RESTful methods for the user to send data to the remote service.<br>
 * 
 * Note:<br>
 * 	Must ensure that the incoming data type and outgoing data type should extend {@link JavaScriptObject}, otherwise an {@link Exception} will 
 *  be thrown.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class RESTConnection {
	private HTTPConnection	connection = null;

	/**
	 * 
	 */
	public RESTConnection() {
	}

	/**
	 * Assigns a URL to operate
	 * 
	 * @param url A URL to operate
	 */
	public void setURL(URL url){
		this.connection = HTTPConnection.builder().setURL(url)
												  .build();
	}
	
	/**
	 * To send a REST Get request
	 * 
	 * @param callback The callback function to receive response.
	 * 
	 * @throws HTTPRequestException if the request cannot reach remote service or an occurred between the connection.
	 */
	public <T, E> void sendGetRequest(T data, final AsyncCallback<E> callback) throws HTTPRequestException{
		this.connection.addHeaders(HTTPMethodOverride.GET);
		this.connection.addHeaders(ContentType.JSON);
		
		final Dson dson = new DsonJS();
		this.sendRequest(dson.toJSON(data), callback);		
	}
	
	/**
	 * To send a REST Post request
	 * 
	 * @param callback The callback function to receive response.
	 * 
	 * @throws HTTPRequestException if the request cannot reach remote service or an occurred between the connection.
	 */
	public <T, E> void sendPostRequest(T data, final AsyncCallback<E> callback) throws HTTPRequestException{
		this.connection.addHeaders(HTTPMethodOverride.POST);
		this.connection.addHeaders(ContentType.JSON);
		
		final Dson dson = new DsonJS();
		this.sendRequest(dson.toJSON(data), callback);		
	}
	
	/**
	 * To send a REST Put request
	 * 
	 * @param callback The callback function to receive response.
	 * 
	 * @throws HTTPRequestException if the request cannot reach remote service or an occurred between the connection.
	 */
	public <T, E> void sendPutRequest(T data, final AsyncCallback<E> callback) throws HTTPRequestException{
		this.connection.addHeaders(HTTPMethodOverride.PUT);
		this.connection.addHeaders(ContentType.JSON);
		
		final Dson dson = new DsonJS();
		this.sendRequest(dson.toJSON(data), callback);		
	}
	
	/**
	 * To send a REST Delete request
	 * 
	 * @param callback The callback function to receive response.
	 * 
	 * @throws HTTPRequestException if the request cannot reach remote service or an occurred between the connection.
	 */
	public <T, E> void sendDeleteRequest(T data, final AsyncCallback<E> callback) throws HTTPRequestException{
		this.connection.addHeaders(HTTPMethodOverride.DELETE);
		this.connection.addHeaders(ContentType.JSON);
		
		final Dson dson = new DsonJS();
		this.sendRequest(dson.toJSON(data), callback);		
	}
	
	
	/**
	 * To send request by POST method for the RESTful connection
	 * 
	 * @param data The data carried to the remote service 
	 * @param callback The response callback function
	 * 
	 * @throws HTTPRequestException if the request cannot reach remote service or an occurred between the connection.
	 */
	private <T> void sendRequest(String data, final AsyncCallback<T> callback) throws HTTPRequestException{
		this.connection.setData(data);
		
		this.connection.sendPostRequest(new RequestCallback() {
			
			@Override
			public void onSuccess(String responseData) {
				Dson dson = new DsonJS();
				CommandResponse<T> response = dson.fromJSON(responseData);
				
				if( CommandResponseStatus.NoError == response.getStatus() ){
					callback.onSuccess(response.getCommandResult());
				}else{
					callback.onFailure(new DataRetrieveFailedException(StringFormatter.format("Error[%s]: %s", response.getDebugInfo().getExceptionCode()
																										  	 , response.getDebugInfo().getExceptionMessage())));
				}
			}
			
			@Override
			public void onFailure(Throwable exception) {
				callback.onFailure(exception);
			}
		});
	}
}
