/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http.headers;

/**
 * The sub-type of this interface represents a HTTP header object which contains the name of header and 
 * its own value.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public interface HTTPHeader {
		
	/**
	 * Returns the name of this header
	 * 
	 * @return the name of this header
	 */
	String getHeader();
	
	/**
	 * Returns the value of this header
	 * 
	 * @return the value of this header
	 */
	String getValue();
}
