/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http;

/**
 * This interface handles the return data from a {@link HTTPConnection}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public interface RequestCallback {
	/**
	 * To indicate the request was executed well and the result coming back.
	 * 
	 * @param responseData The response data from the remote service.
	 */
	void onSuccess(String responseData);
	
	/**
	 * To indicate the request couldn't be executed well and nothing coming back.
	 * 
	 * @param exception If the request is failed.
	 */
	void onFailure(Throwable exception);
}
