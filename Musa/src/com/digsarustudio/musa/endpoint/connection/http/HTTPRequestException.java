/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http;

/**
 * This exception indicates an error occurred when the user sent a request to remote service.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class HTTPRequestException extends Exception {
	private Integer statusCode	= null;
	private String	statusMsg	= null; 	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1694342972752385603L;

	/**
	 * 
	 */
	public HTTPRequestException() {

	}
	
	public HTTPRequestException(Integer statusCode, String statusMsg){
		super(statusCode.toString() + " : " + statusMsg);
		this.statusCode = statusCode;
		this.statusMsg = statusMsg;
	}

	/**
	 * @param message
	 */
	public HTTPRequestException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public HTTPRequestException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public HTTPRequestException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public HTTPRequestException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

	/**
	 * Returns the status code
	 * 
	 * @return the statusCode
	 */
	public Integer getStatusCode() {
		return statusCode;
	}

	/**
	 * Returns the status message
	 * 
	 * @return the statusMsg
	 */
	public String getStatusMsg() {
		return statusMsg;
	}

	
}
