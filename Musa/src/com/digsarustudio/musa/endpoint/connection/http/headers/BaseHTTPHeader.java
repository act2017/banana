/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http.headers;

/**
 * The base class of {@link HTTPHeader}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public abstract class BaseHTTPHeader implements HTTPHeader {

	private String header = null;
	
	/**
	 * 
	 */
	public BaseHTTPHeader(String header) {
		this.header = header;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPHeader#getHeader()
	 */
	@Override
	public String getHeader() {
		return this.header;
	}
}
