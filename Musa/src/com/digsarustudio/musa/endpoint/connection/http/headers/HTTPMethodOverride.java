/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http.headers;

/**
 * This class represents the overriding of HTTP method used in HTTP header
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class HTTPMethodOverride extends BaseHTTPHeader implements HTTPHeader {
	/**
	 * To ask remote service to do a fetching by the request
	 */
	public final static HTTPMethodOverride	GET		= new HTTPMethodOverride("GET");
	
	/**
	 * To ask remote service to do a insertion by the request
	 */
	public final static HTTPMethodOverride	POST		= new HTTPMethodOverride("POST");
	
	/**
	 * To ask remote service to do a updating by the request
	 */
	public final static HTTPMethodOverride	PUT		= new HTTPMethodOverride("PUT");
	
	/**
	 * To ask remote service to do a deletion by the request 
	 */
	public final static HTTPMethodOverride	DELETE	= new HTTPMethodOverride("DELETE");
	
	
	private final static String HEADER = "X-HTTP-METHOD-OVERRIDE";
	
	private String method = null;
	
	/**
	 * @param header
	 */
	private HTTPMethodOverride(String method) {
		super(HEADER);

		this.method = method;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.http.headers.HTTPHeader#getValue()
	 */
	@Override
	public String getValue() {
		return this.method;
	}

}
