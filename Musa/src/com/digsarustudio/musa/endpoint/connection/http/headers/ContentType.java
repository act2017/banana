/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http.headers;

import com.digsarustudio.banana.utils.StringFormatter;

/**
 * This class represents the content type used in HTTP header field
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public final class ContentType extends BaseHTTPHeader implements HTTPHeader{
	/**
	 * To indicates using form data as the content to send to the remote service
	 */
	public final static ContentType	FORM_DATA	= new ContentType("application", "x-www-form-urlencoded");
	
	/**
	 * To indicates using a json string as the content to send to the remote service.
	 */
	public final static ContentType	JSON		= new ContentType("application", "json");
	
	
	
	private static final String HEADER = "Content-type";
	
	private String type				= null;
	private String mediaTypeName	= null;
	
	/**
	 * 
	 */
	private ContentType(String type, String mediaTypeName) {
		super(HEADER);
		
		this.type = type;
		this.mediaTypeName = mediaTypeName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPHeader#getValue()
	 */
	@Override
	public String getValue() {
		return StringFormatter.format("%s/%s", this.type, this.mediaTypeName);
	}

}
