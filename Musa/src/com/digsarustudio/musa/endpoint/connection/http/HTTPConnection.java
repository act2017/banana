/**
 * 
 */
package com.digsarustudio.musa.endpoint.connection.http;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.musa.endpoint.connection.RequestParameter;
import com.digsarustudio.musa.endpoint.connection.http.headers.HTTPHeader;
import com.digsarustudio.musa.endpoint.url.URL;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.http.client.RequestBuilder.Method;

/**
 * This class handles the data model and how to send a HTTP request to the remote service via a URL, and 
 * retrieve the result by a request callback interface to the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.15
 *
 */
public class HTTPConnection {
	/**
	 * 
	 * The object builder for {@link HTTPConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.15
	 *
	 */
	public static class Builder implements ObjectBuilder<HTTPConnection> {
		private HTTPConnection result = null;

		public Builder() {
			this.result = new HTTPConnection();
		}

		/**
		 * @param url
		 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setURL(com.digsarustudio.musa.endpoint.url.URL)
		 */
		public Builder setURL(URL url) {
			result.setURL(url);
			return this;
		}

		/**
		 * @param headers
		 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setHeaders(java.util.List)
		 */
		public Builder setHeaders(List<HTTPHeader> headers) {
			result.setHeaders(headers);
			return this;
		}

		/**
		 * @param header
		 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#addHeaders(com.digsarustudio.musa.endpoint.connection.http.HTTPHeader)
		 */
		public Builder addHeaders(HTTPHeader header) {
			result.addHeaders(header);
			return this;
		}

		/**
		 * @param data
		 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setData(java.lang.String)
		 */
		public Builder setData(String data) {
			result.setData(data);
			return this;
		}

		/**
		 * @param param
		 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#addParameter(com.digsarustudio.musa.endpoint.connection.RequestParameter)
		 */
		public Builder addParameter(RequestParameter param) {
			result.addParameter(param);
			return this;
		}

		/**
		 * @param params
		 * @see com.digsarustudio.musa.endpoint.connection.http.HTTPConnection#setParameters(java.util.List)
		 */
		public Builder setParameters(List<RequestParameter> params) {
			result.setParameters(params);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public HTTPConnection build() {
			return this.result;
		}

	}
	
	public static final Method	POST	= RequestBuilder.POST;
	public static final Method	GET		= RequestBuilder.GET;
	public static final Method	PUT		= RequestBuilder.PUT;
	public static final Method	DELETE	= RequestBuilder.DELETE;	
	
	protected URL				url			= null;
	
	/**
	 * The HTTP header can be reused for the next request.<br> 
	 */
	protected List<HTTPHeader>	headers			= null;
	protected String			data			= null;	
	
	/**
	 * 
	 */
	private HTTPConnection() {
		
	}

	/**
	 * To send a GET request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HTTPRequestException if the request cannot be sent or there is no response from remote service.<br>
	 */
	public void sendGetRequest(RequestCallback callback) throws HTTPRequestException{
		this.sendRequest(this.url.getEncodedURL(), GET, this.headers, this.data, callback);		
	}
	
	/**
	 * To send a POST request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HTTPRequestException if the request cannot be sent or there is no response from remote service.<br>
	 */
	public void sendPostRequest(RequestCallback callback) throws HTTPRequestException{
		this.sendRequest(this.url.getEncodedURL(), POST, this.headers, this.data, callback);
	}
	
	/**
	 * To send a PUT request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HTTPRequestException if the request cannot be sent or there is no response from remote service.<br>
	 */
	public void sendPutRequest(RequestCallback callback) throws HTTPRequestException{
		this.sendRequest(this.url.getEncodedURL(), PUT, this.headers, this.data, callback);
	}
	
	/**
	 * To send a DELETE request to the remote service
	 * 
	 * @param callback The callback for this request
	 * 
	 * @throws HTTPRequestException if the request cannot be sent or there is no response from remote service.<br>
	 */
	public void sendDeleteRequest(RequestCallback callback) throws HTTPRequestException{
		this.sendRequest(this.url.getEncodedURL(), DELETE, this.headers, this.data,callback);
	}
	
	/**
	 * Returns the details of URL of a remote service
	 * 
	 * @return the details of URL of a remote service
	 */
	public URL getURL() {
		return url;
	}

	/**
	 * Assigns a details of URL of a remote service
	 * 
	 * @param url the URL of a remote service to set
	 */
	public void setURL(URL url) {
		this.url = url;
	}

	/**
	 * Returns the {@link HTTPHeader} for the request
	 * 
	 * @return the {@link HTTPHeader} for the request
	 */
	public List<HTTPHeader> getHeaders() {
		return headers;
	}

	/**
	 * Assigns a list of {@link HTTPHeader} for the request
	 * 
	 * @param headers the headers to set
	 */
	public void setHeaders(List<HTTPHeader> headers) {
		this.headers = headers;
	}

	/**
	 * Assigns a {@link HTTPHeader} for the request
	 * 
	 * @param header The header to set
	 */
	public void addHeaders(HTTPHeader header){
		if(null == this.headers){
			this.headers = new ArrayList<>();
		}
		
		this.headers.add(header);
	}
	
	/**
	 * Returns the data to carry to the remote service
	 * 
	 * @return the data to carry to the remote service
	 */
	public String getData() {
		return data;
	}

	/**
	 * Assigns the data to carry to the remote service
	 * 
	 * @param data the data to carry to the remote service
	 */
	public void setData(String data) {
		this.data = data;
	}

	/**
	 * @param param
	 * @see com.digsarustudio.musa.endpoint.url.URL#addParameter(com.digsarustudio.musa.endpoint.connection.RequestParameter)
	 */
	public void addParameter(RequestParameter param) {
		this.url.addParameter(param);
	}

	/**
	 * @param params
	 * @see com.digsarustudio.musa.endpoint.this.url.URL#setParameters(java.util.List)
	 */
	public void setParameters(List<RequestParameter> params) {
		this.url.setParameters(params);
	}

	/**
	 * @return
	 * @see com.digsarustudio.musa.endpoint.this.url.URL#getParameters()
	 */
	public List<RequestParameter> getParameters() {
		return this.url.getParameters();
	}

	/**
	 * 
	 * @see com.digsarustudio.musa.endpoint.this.url.URL#clearParameters()
	 */
	public void clearParameters() {
		this.url.clearParameters();
	}	

	protected void sendRequest(String url, Method method, List<HTTPHeader> headers, String data, final RequestCallback callback) throws HTTPRequestException{
		RequestBuilder builder = new RequestBuilder(method, url);
		
		for (HTTPHeader header : headers) {
			builder.setHeader(header.getHeader(), header.getValue());
		}
		
		try {
			builder.sendRequest(data, new com.google.gwt.http.client.RequestCallback() {
				
				@Override
				public void onResponseReceived(Request request, Response response) {				
					if( 200 == response.getStatusCode() ){
						callback.onSuccess(response.getText());
					}else{
						callback.onFailure(new HTTPRequestException(response.getStatusCode(), response.getStatusText()));
					}
				}
				
				@Override
				public void onError(Request request, Throwable exception) {
					callback.onFailure(exception);
				}
			});
		} catch (RequestException e) {
			throw new HTTPRequestException(e.getMessage());
		}
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
