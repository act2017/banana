/**
 * 
 */
package com.digsarustudio.musa.endpoint;

/**
 * Defining the operations of callback object for endpoint communication
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * 
 * @deprecated	1.0.15 unused.
 */
public interface EndpointCallback<T> {
	void onSuccess(T result);	
	void onFailure(Throwable caught);
}
