Version: 1.0.3
	1. Appending list method of endpoint and connector for the client code to send out JSON string to the server.

Reference: 
	1.gwt-test-utils: https://github.com/gwt-test-utils/gwt-test-utils

I.Before using GWT-Test-Utils, please add the jars shown as follows into the build path:
	1.gwt-test-utils-?.jar
	2.javasist-3.20.0-GA+.jar
	3.junit-4.5+.jar
	4.slf4j-api-1.6+.jar
	5.assertj-core-3.4.1+.jar
	6.slf4j-nop.jar
	
II.Just open a new JUnit Run configuration, and specify in the JVM Arguments text area the amount of memory you'll need for your tests. 
	For example :
		-Xmx512M -XX:MaxPermSize=128M
		
		
III. The gson-x.x.jar has to be placed into the /war/lib and added into the build path before to use this library.