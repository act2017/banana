/**
 * 
 */
package com.digsarustudio.banana.database.io;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseConfig;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.mysql.MySQLConnection;

/**
 * To test {@link BaseDatabaseManipulatorTest}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		1.0.2
 *
 */
public class BaseDatabaseManipulatorTest {
	static DatabaseConfig config = DatabaseConfig.builder().build();
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		config.setHost("localhost");
		config.setUserName("db_boss");
		config.setPassword("1234");
		config.setDefaultDB("junit_test");
		config.setEngineType(DatabaseEngineType.InnoDB);
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#BaseDatabaseManipulator()}.
	 */
	@Test
	public void testBaseDatabaseManipulator() {
		new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
			}
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#setConnection(com.digsarustudio.banana.database.io.DatabaseConnection)}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testSetConnection() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		DatabaseConnection connection = MySQLConnection.builder().setHostName(config.getHost())
																 .setUserName(config.getUserName())
																 .setPassword(config.getPassword())
																 .build();
		
		manipulator.setConnection(connection);
		
		manipulator.open();
		
		manipulator.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		assertNull(manipulator.getName());
		
		manipulator.setName("junit_test");
		
		assertEquals("junit_test", manipulator.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#setCharacterSets(com.digsarustudio.banana.database.CharacterSets)}.
	 */
	@Test
	public void testSetCharacterSets() {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		assertNull(manipulator.getCharacterSets());
		
		manipulator.setCharacterSets(CharacterSets.UTF16Unicode);
		
		assertEquals(CharacterSets.UTF16Unicode, manipulator.getCharacterSets());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#setCollations(com.digsarustudio.banana.database.Collations)}.
	 */
	@Test
	public void testSetCollations() {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		assertNull(manipulator.getCollations());
		
		manipulator.setCollations(Collations.UTF16Unicode);
		
		assertEquals(Collations.UTF16Unicode, manipulator.getCollations());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#getName()}.
	 */
	@Test
	public void testGetName() {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		assertNull(manipulator.getName());
		
		manipulator.setName("junit_test");
		
		assertEquals("junit_test", manipulator.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#getCharacterSets()}.
	 */
	@Test
	public void testGetCharacterSets() {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		assertNull(manipulator.getCharacterSets());
		
		manipulator.setCharacterSets(CharacterSets.UTF16Unicode);
		
		assertEquals(CharacterSets.UTF16Unicode, manipulator.getCharacterSets());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#getCollations()}.
	 */
	@Test
	public void testGetCollations() {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		assertNull(manipulator.getCollations());
		
		manipulator.setCollations(Collations.UTF16Unicode);
		
		assertEquals(Collations.UTF16Unicode, manipulator.getCollations());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#open()}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testOpen() throws SQLManipulationException, DriverNotLoadedException {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		DatabaseConnection connection = MySQLConnection.builder().setHostName(config.getHost())
																 .setUserName(config.getUserName())
																 .setPassword(config.getPassword())
																 .build();
		
		manipulator.setConnection(connection);
		
		manipulator.open();
		
		manipulator.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#IsConnected()()}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testIsConnected() throws SQLManipulationException, DriverNotLoadedException {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		DatabaseConnection connection = MySQLConnection.builder().setHostName(config.getHost())
																 .setUserName(config.getUserName())
																 .setPassword(config.getPassword())
																 .build();
		
		manipulator.setConnection(connection);
		
		assertFalse(manipulator.isConnected());
		
		manipulator.open();
		
		assertTrue(manipulator.isConnected());
		
		manipulator.close();
		
		assertFalse(manipulator.isConnected());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#close()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testClose() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		DatabaseConnection connection = MySQLConnection.builder().setHostName(config.getHost())
																 .setUserName(config.getUserName())
																 .setPassword(config.getPassword())
																 .build();
		
		manipulator.setConnection(connection);
		
		manipulator.open();
		
		manipulator.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#executeUpdate(java.lang.String)}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testExecuteUpdate() throws SQLManipulationException, DriverNotLoadedException {

		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		DatabaseConnection connection = MySQLConnection.builder().setHostName(config.getHost())
																 .setUserName(config.getUserName())
																 .setPassword(config.getPassword())
																 .build();
		
		manipulator.setConnection(connection);
		
		manipulator.open();
		
		String query = "USE `junit_test`;";
		manipulator.executeUpdate(query);		
		
		manipulator.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulatorTest#executeQuery(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testExecuteQuery() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseManipulator manipulator = new BaseDatabaseManipulator() {
			
			@Override
			public void selectDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void selectDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public Boolean isDatabaseExisting() throws SQLManipulationException {
				
				return null;
			}
			
			@Override
			public void deleteDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void deleteDatabase(String name) throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase() throws SQLManipulationException {
				
				
			}
			
			@Override
			public void createDatabase(String name, CharacterSets charset, Collations collations)
					throws SQLManipulationException {
				
				
			}
		};
		
		DatabaseConnection connection = MySQLConnection.builder().setHostName(config.getHost())
																 .setUserName(config.getUserName())
																 .setPassword(config.getPassword())
																 .build();
		
		manipulator.setConnection(connection);
		
		manipulator.open();
		
		String query = "USE `mysql`;";
		manipulator.executeUpdate(query);
		
		query = "SELECT * FROM `user`;";
		
		ResultBundle result = manipulator.executeQuery(query);
		
		assertTrue(result.next());
		assertEquals("localhost", result.getString("Host"));
		assertEquals("root", result.getString("User"));		
		
		manipulator.close();
	}

}
