/**
 * 
 */
package com.digsarustudio.banana.database.io;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.io.mysql.MySQLConnection;
import com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator;
import com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link ResultBundle}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class ResultBundleTest {
	private static final String TB_NAME = "result_bundle_test";
	static DatabaseManipulator dbManipulator = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DatabaseConnection connection = MySQLConnection.builder().setHostName("localhost")
																 .setUserName("db_boss")
																 .setPassword("1234")
																 .build();
		
		dbManipulator = MySQLDatabaseManipulator.builder().setName("junit_test")
														  .setConnection(connection)
														  .setCharacterSets(CharacterSets.UTF8Unicode)
														  .setCollations(Collations.UTF8Unicode)
														  .build();
		
		dbManipulator.open();
		if( !dbManipulator.isDatabaseExisting() ){
			dbManipulator.createDatabase();
		}else{
			dbManipulator.selectDatabase();
		}
		
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator tbManipulator = MySQLTableManipulator.builder().setTable(table)
																		.setDatabaseEngineType(DatabaseEngineType.InnoDB)
																		.setPreventErrorIfTableExists()
																		.build();
		if( !tbManipulator.isTableExisting() ){
			tbManipulator.createTable();
		}
		
		if( tbManipulator.isEmpty() ){
			insertTestingData();
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dbManipulator.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.ResultBundle#next()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testNext() throws SQLManipulationException {
		String query = "SELECT * FROM `%s`;";
		
		ResultBundle result = dbManipulator.executeQuery(String.format(query, TB_NAME));
		
		assertTrue(result.next());
		
		result.release();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.ResultBundle#next()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testNextWithEmptyResult() throws SQLManipulationException {
		String query = "SELECT * FROM `%s` WHERE `id`='100';";
		
		ResultBundle result = dbManipulator.executeQuery(String.format(query, TB_NAME));
		
		assertFalse(result.next());
		
		result.release();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.ResultBundle#getString(java.lang.Integer)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testGetStringByIndex() throws SQLManipulationException {
		String query = "SELECT * FROM `%s`;";
		
		ResultBundle result = dbManipulator.executeQuery(String.format(query, TB_NAME));
		
		assertTrue(result.next());
		assertEquals("1", result.getString(0));
		assertEquals("1st", result.getString(1));
		
		assertTrue(result.next());
		assertEquals("2", result.getString(0));
		assertEquals("2nd", result.getString(1));
		
		assertTrue(result.next());
		assertEquals("3", result.getString(0));
		assertEquals("3rd", result.getString(1));
		
		assertTrue(result.next());
		assertEquals("4", result.getString(0));
		assertEquals("4th", result.getString(1));
		
		assertTrue(result.next());
		assertEquals("5", result.getString(0));
		assertEquals("5th", result.getString(1));
		
		assertFalse(result.next());
		
		result.release();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.ResultBundle#getString(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testGetStringByColumnName() throws SQLManipulationException {
		String query = "SELECT * FROM `%s`;";
		
		ResultBundle result = dbManipulator.executeQuery(String.format(query, TB_NAME));
		
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("1st", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("2", result.getString("id"));
		assertEquals("2nd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("3", result.getString("id"));
		assertEquals("3rd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("4", result.getString("id"));
		assertEquals("4th", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("5", result.getString("id"));
		assertEquals("5th", result.getString("comment"));
		
		assertFalse(result.next());
		
		result.release();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.ResultBundle#release()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRelease() throws SQLManipulationException {
		String query = "SELECT * FROM `%s`;";
		
		ResultBundle result = dbManipulator.executeQuery(String.format(query, TB_NAME));
		
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("1st", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("2", result.getString("id"));
		assertEquals("2nd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("3", result.getString("id"));
		assertEquals("3rd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("4", result.getString("id"));
		assertEquals("4th", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("5", result.getString("id"));
		assertEquals("5th", result.getString("comment"));
		
		assertFalse(result.next());
		
		result.release();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.ResultBundle#builder()}.
	 */
	@Test
	public void testBuilder() {
		ResultBundle.builder();
	}

	private static void insertTestingData() throws SQLManipulationException{
		String query = "INSERT INTO `%s` (`id`, `comment`) VALUES ('%d', '%s');";
		
		dbManipulator.executeUpdate(String.format(query, TB_NAME, 1, "1st"));
		dbManipulator.executeUpdate(String.format(query, TB_NAME, 2, "2nd"));
		dbManipulator.executeUpdate(String.format(query, TB_NAME, 3, "3rd"));
		dbManipulator.executeUpdate(String.format(query, TB_NAME, 4, "4th"));
		dbManipulator.executeUpdate(String.format(query, TB_NAME, 5, "5th"));
	}
}
