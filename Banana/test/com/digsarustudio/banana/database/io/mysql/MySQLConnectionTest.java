/**
 * 
 */
package com.digsarustudio.banana.database.io.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.ResultBundle;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.io.TableManipulator;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link MySQLConnection}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLConnectionTest {
	static final String HOST	= "localhost";
	static final String USER	= "db_boss";
	static final String PWD		= "1234";
	static final String DB		= "junit_test";
	
	static final String TB	= "mysql_connection_test";
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DatabaseConnection connection = MySQLConnection.builder().setHostName(HOST)
																 .setUserName(USER)
																 .setPassword(PWD)
																 .build();
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																			.setName(DB)
																			.build();
		
		manipulator.open();
		
		if( !manipulator.isDatabaseExisting() ){
			manipulator.createDatabase();
		}
		
		manipulator.selectDatabase();
		
		Table table = new AbstractTable(TB) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator tbManipulator = MySQLTableManipulator.builder().setTable(table)
																	    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	    .setPreventErrorIfTableExists()
																	    .build();
		if( !tbManipulator.isTableExisting() ){
			tbManipulator.createTable();
		}
		
		if( tbManipulator.isEmpty() ){
			insertTestingData(connection);
		}
		
		connection.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLConnection#builder()}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testBuilder() throws DriverNotLoadedException {
		MySQLConnection.builder();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#open()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */	
	@Test
	public void testOpen() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName(HOST)
																 .setUserName(USER)
																 .setPassword(PWD)
																 .build();
		
		connection.open();
		
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#close()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testClose() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName(HOST)
																 .setUserName(USER)
																 .setPassword(PWD)
																 .build();
		
		connection.open();
		
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#executeUpdate(java.lang.String)}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testExecuteUpdate() throws SQLManipulationException, DriverNotLoadedException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName(HOST)
																 .setUserName(USER)
																 .setPassword(PWD)
																 .build();
		
		connection.open();
		String query = String.format("USE %s;", TB);
		connection.executeUpdate(query);
		
		query = String.format("SELECT COUNT(*) AS `total` FROM `%s`;");
		
		
		ResultBundle result = connection.executeQuery(query);
		Integer count = Integer.parseInt(result.getString("total")) +1;
		result.release();
		
		query = "INSERT INTO `%s` (`id`, `comment`) VALUES ('%d', '%s');";
		connection.executeUpdate(String.format(query, TB, count, count.toString() + "th"));
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#executeQuery(java.lang.String)}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testExecuteQuery() throws SQLManipulationException, DriverNotLoadedException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName(HOST)
																 .setUserName(USER)
																 .setPassword(PWD)
																 .build();
		
		String query = "SELECT * FROM `%s`;";
		
		ResultBundle result = connection.executeQuery(String.format(query, TB));
		
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("1st", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("2", result.getString("id"));
		assertEquals("2nd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("3", result.getString("id"));
		assertEquals("3rd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("4", result.getString("id"));
		assertEquals("4th", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("5", result.getString("id"));
		assertEquals("5th", result.getString("comment"));
		
		assertFalse(result.next());
		
		result.release();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setHostName(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetHostName() throws DriverNotLoadedException {
		MySQLConnection connection = MySQLConnection.builder().setHostName(HOST)
																 .build();
		assertEquals(HOST, connection.getHostName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setPort(java.lang.Integer)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetPort() throws DriverNotLoadedException {
		MySQLConnection connection = MySQLConnection.builder().setPort(3388)
																 .build();
		
		assertEquals(3388, connection.getPort().intValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setUserName(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetUserName() throws DriverNotLoadedException {
		MySQLConnection connection = MySQLConnection.builder().setUserName(USER)
																 .build();
		assertEquals(USER, connection.getUserName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setPassword(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetPassword() throws DriverNotLoadedException {
		MySQLConnection connection = MySQLConnection.builder().setPassword(PWD)
																 .build();
		
		assertEquals(PWD, connection.getPassword());
	}

	private static void insertTestingData(DatabaseConnection connection) throws SQLManipulationException{
		String query = "INSERT INTO `%s` (`id`, `comment`) VALUES ('%d', '%s');";
		
		connection.executeUpdate(String.format(query, TB, 1, "1st"));
		connection.executeUpdate(String.format(query, TB, 2, "2nd"));
		connection.executeUpdate(String.format(query, TB, 3, "3rd"));
		connection.executeUpdate(String.format(query, TB, 4, "4th"));
		connection.executeUpdate(String.format(query, TB, 5, "5th"));
	}

}
