/**
 * 
 */
package com.digsarustudio.banana.database.io.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.ResultBundle;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.io.TableManipulator;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link MySQLDatabaseManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLDatabaseManipulatorTest {
	static DatabaseConnection connection = null;
	
	
	static final String TB	= "mysql_manipulator_test";
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		connection = MySQLConnection.builder().setHostName("localhost")
											  .setUserName("db_boss")
											  .setPassword("1234")
											  .build();
		
		connection.open();
		
		Table table = new AbstractTable(TB) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator tbManipulator = MySQLTableManipulator.builder().setTable(table)
																	    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	    .setPreventErrorIfTableExists()
																	    .build();
		if( !tbManipulator.isTableExisting() ){
			tbManipulator.createTable();
		}
		
		if( tbManipulator.isEmpty() ){
			insertTestingData(connection);
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		connection.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#createDatabase(java.lang.String, com.digsarustudio.banana.database.CharacterSets, com.digsarustudio.banana.database.Collations)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testCreateDatabaseByNameAndCharacterSetsAndCollations() throws SQLManipulationException {
		final String DB = "junit_test_create_db_by_name_char_collaton";
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																		    .build();
		
		manipulator.createDatabase(DB, CharacterSets.UTF8Unicode, Collations.UTF8Unicode);
		
		String query = String.format("USE `%s`;", DB);
		manipulator.executeUpdate(query);
		
		manipulator.deleteDatabase(DB);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#selectDatabase(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testSelectDatabaseByName() throws SQLManipulationException {
		final String DB = "junit_test_select_database_by_name";
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																		    .build();
		
		manipulator.createDatabase(DB, CharacterSets.UTF8Unicode, Collations.UTF8Unicode);
		
		manipulator.selectDatabase(DB);
		
		manipulator.deleteDatabase(DB);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#deleteDatabase(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testDeleteDatabaseByName() throws SQLManipulationException {
		final String DB = "junit_test_delete_database_by_name";
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																		    .build();
		
		manipulator.createDatabase(DB, CharacterSets.UTF8Unicode, Collations.UTF8Unicode);
		
		manipulator.selectDatabase(DB);
		
		manipulator.deleteDatabase(DB);
		
		assertFalse(manipulator.isDatabaseExisting(DB));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#createDatabase()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testCreateDatabase() throws SQLManipulationException {
		final String DB = "junit_test_create_database";
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																		    .build();
		
		manipulator.createDatabase();
		
		manipulator.selectDatabase();
		
		manipulator.deleteDatabase();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#selectDatabase()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testSelectDatabase() throws SQLManipulationException {
		final String DB = "junit_test_select_database";
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																		    .build();
		
		manipulator.createDatabase();
		
		manipulator.selectDatabase();
		
		manipulator.deleteDatabase();		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#deleteDatabase()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testDeleteDatabase() throws SQLManipulationException {
		final String DB = "junit_test_delete_database";
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																		    .build();
		
		manipulator.createDatabase();
		
		manipulator.selectDatabase();
		
		manipulator.deleteDatabase();
		
		assertFalse(manipulator.isDatabaseExisting());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#isDatabaseExisting()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsDatabaseExisting() throws SQLManipulationException {
		final String DB = "junit_test_is_database_exist";
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																		    .build();
		
		manipulator.createDatabase();
		
		assertFalse(manipulator.isDatabaseExisting());
		
		manipulator.deleteDatabase();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#isDatabaseExisting(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsDatabaseExistingByName() throws SQLManipulationException {
		final String DB = "junit_test_is_database_exist";
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																		    .build();
		
		manipulator.createDatabase();
		
		assertFalse(manipulator.isDatabaseExisting(DB));
		
		manipulator.deleteDatabase();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator#builder()}.
	 */
	@Test
	public void testBuilder() {
		MySQLDatabaseManipulator.builder();
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#setConnection(com.digsarustudio.banana.database.io.DatabaseConnection)}.
	 */
	@Test
	public void testSetConnection() {
		
		@SuppressWarnings("unused")
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																		    .build();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().build();
		assertNull(manipulator.getName());
		
		manipulator.setName("set_name");
		assertEquals("set_name", manipulator.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#setCharacterSets(com.digsarustudio.banana.database.CharacterSets)}.
	 */
	@Test
	public void testSetCharacterSets() {		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().build();
		assertNull(manipulator.getCharacterSets());
		
		manipulator.setCharacterSets(CharacterSets.UTF8Unicode);
		assertEquals(CharacterSets.UTF8Unicode, manipulator.getCharacterSets());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#setCollations(com.digsarustudio.banana.database.Collations)}.
	 */
	@Test
	public void testSetCollations() {		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().build();
		assertNull(manipulator.getCollations());
		
		manipulator.setCollations(Collations.UTF8Unicode);	    
		assertEquals(Collations.UTF8Unicode, manipulator.getCollations());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#getName()}.
	 */
	@Test
	public void testGetName() {		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setName("get_name")
																		    .build();
		
		assertEquals("get_name", manipulator.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#getCharacterSets()}.
	 */
	@Test
	public void testGetCharacterSets() {		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setCharacterSets(CharacterSets.UTF8Unicode)
																			.build();

		assertEquals(CharacterSets.UTF8Unicode, manipulator.getCharacterSets());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#getCollations()}.
	 */
	@Test
	public void testGetCollations() {		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setCollations(Collations.UTF8Unicode)
			    															.build();

		assertEquals(Collations.UTF8Unicode, manipulator.getCollations());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#open()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testOpen() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName("localhost")
																 .setUserName("db_boss")
																 .setPassword("1234")
																 .build();
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName("junit_test_open")
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																			.build();
		
		manipulator.open();
		
		manipulator.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#isConnected()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsConnected() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName("localhost")
																 .setUserName("db_boss")
																 .setPassword("1234")
																 .build();
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
									.setName("junit_test_isConnected")
									.setCharacterSets(CharacterSets.UTF8Unicode)
									.setCollations(Collations.UTF8Unicode)
									.build();
		
		assertFalse(manipulator.isConnected());
		
		manipulator.open();
		assertTrue(manipulator.isConnected());
		manipulator.close();
		
		assertFalse(manipulator.isConnected());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#close()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testClose() throws DriverNotLoadedException, SQLManipulationException {
		DatabaseConnection connection = MySQLConnection.builder().setHostName("localhost")
																 .setUserName("db_boss")
																 .setPassword("1234")
																 .build();
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
									.setName("junit_test_close")
									.setCharacterSets(CharacterSets.UTF8Unicode)
									.setCollations(Collations.UTF8Unicode)
									.build();
		
		manipulator.open();
		
		manipulator.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#executeUpdate(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testExecuteUpdate() throws SQLManipulationException {
		final String DB = "junit_test_execute_update";
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																			.build();
		
		String query = String.format("USE `%s`;", DB);
		
		manipulator.executeUpdate(query);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseManipulator#executeQuery(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testExecuteQuery() throws SQLManipulationException {
		final String DB = "junit_test_execute_query";
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																			.build();
		
		manipulator.selectDatabase();
		
		String query = "SELECT * FROM `%s`;";
		
		ResultBundle result = connection.executeQuery(String.format(query, TB));
		
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("1st", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("2", result.getString("id"));
		assertEquals("2nd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("3", result.getString("id"));
		assertEquals("3rd", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("4", result.getString("id"));
		assertEquals("4th", result.getString("comment"));
		
		assertTrue(result.next());
		assertEquals("5", result.getString("id"));
		assertEquals("5th", result.getString("comment"));
		
		assertFalse(result.next());
		
		result.release();
	}

	private static void insertTestingData(DatabaseConnection connection) throws SQLManipulationException{
		String query = "INSERT INTO `%s` (`id`, `comment`) VALUES ('%d', '%s');";
		
		connection.executeUpdate(String.format(query, TB, 1, "1st"));
		connection.executeUpdate(String.format(query, TB, 2, "2nd"));
		connection.executeUpdate(String.format(query, TB, 3, "3rd"));
		connection.executeUpdate(String.format(query, TB, 4, "4th"));
		connection.executeUpdate(String.format(query, TB, 5, "5th"));
	}
}
