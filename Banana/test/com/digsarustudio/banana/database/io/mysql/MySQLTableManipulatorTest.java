/**
 * 
 */
package com.digsarustudio.banana.database.io.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.ResultBundle;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.io.TableManipulator;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.ForeignKeyConstraint;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link MySQLTableManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MySQLTableManipulatorTest {
	static final String DB = "junit_test";
	
	static DatabaseConnection connection	= null;
	static DatabaseManipulator dbManipulator	= null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		connection = MySQLConnection.builder().setHostName("localhost")
											  .setUserName("db_boss")
											  .setPassword("1234")
											  .build();
		
		dbManipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
														  .setName(DB)
														  .setCharacterSets(CharacterSets.UTF8Unicode)
														  .setCollations(Collations.UTF8Unicode)
														  .build();
		
		dbManipulator.open();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		dbManipulator.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#createTable()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testCreateTable() throws SQLManipulationException {
		Table table = new AbstractTable("create_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#renameTable(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRenameTableByName() throws SQLManipulationException {
		Table table = new AbstractTable("rename_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		table.setName("renamed_table");		
		manipulator.renameTable("rename_table");
		
		assertTrue(manipulator.isTableExisting("renamed_table"));
		assertFalse(manipulator.isTableExisting("rename_table"));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#addColumn(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testAddColumn() throws SQLManipulationException {
		Table table = new AbstractTable("add_column") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		
		manipulator.addColumn(TableColumn.builder().setName("additional")
												   .setDataType(TableColumnDataTypes.VarChar)
												   .setLength(16)
												   .build());
		
		assertTrue(manipulator.isColumnExisting("additional"));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#modifyColumn(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testModifyColumn() throws SQLManipulationException {
		Table table = new AbstractTable("modify_column") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		
		manipulator.modifyColumn(TableColumn.builder().setName("comment")
													  .setDataType(TableColumnDataTypes.VarChar)
													  .setLength(64)
													  .build());
				
		assertTrue(manipulator.isColumnExisting("comment"));
		assertEquals(TableColumnDataTypes.VarChar, manipulator.getColumn("comment").getDataType());
		assertEquals(64, manipulator.getColumn("comment").getLength().intValue());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#changeColumnName(java.lang.String, com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testChangeColumnNameByTableColumn() throws SQLManipulationException {
		Table table = new AbstractTable("change_column_name_by_table_column") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.changeColumnName("comment", TableColumn.builder().setName("renamed_comment")
																	 .setDataType(TableColumnDataTypes.VarChar)
																	 .setLength(16)
																	 .build());
		
		assertFalse(manipulator.isColumnExisting("comment"));
		assertTrue(manipulator.isColumnExisting("renamed_comment"));
		assertEquals(TableColumnDataTypes.VarChar, manipulator.getColumn("renamed_comment").getDataType());
		assertEquals(64, manipulator.getColumn("renamed_comment").getLength().intValue());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#getColumn(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testGetColumn() throws SQLManipulationException {
		Table table = new AbstractTable("get_column") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		TableColumn idColumn = manipulator.getColumn("id");
		assertEquals("id", idColumn.getName());
		assertEquals(TableColumnDataTypes.SmallInteger, idColumn.getDataType());

		TableColumn commentColumn = manipulator.getColumn("comment");
		assertEquals("comment", commentColumn.getName());
		assertEquals(TableColumnDataTypes.VarChar, commentColumn.getDataType());
		assertEquals(16, commentColumn.getLength().intValue());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#registerColumnAsPrimaryKey(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRegisterColumnAsPrimaryKey() throws SQLManipulationException {
		Table table = new AbstractTable("register_pk") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());

		manipulator.registerColumnAsPrimaryKey(TableColumn.builder().setName("id")
																	.build());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#unregisterPrimaryKeys()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testUnregisterPrimaryKeys() throws SQLManipulationException {
		Table table = new AbstractTable("unregister_pks") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAsPrimaryKey()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());

		manipulator.unregisterPrimaryKeys();
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#registerColumnAsForeignKey(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRegisterColumnAsForeignKey() throws SQLManipulationException {
		Table table = new AbstractTable("register_fk") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("parent")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());

		manipulator.registerColumnAsForeignKey(TableColumn.builder().setName("parent")
																	.setDataType(TableColumnDataTypes.SmallInteger)
																	.setForeignKeyConstraint(ForeignKeyConstraint.builder().setName("parent_fk")
																														   .setReferentialTable("register_fk")
																														   .setReferentialPrimaryKey("id")
																														   .build())
																	.build());		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#unregisterColumnFromForeignKey(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testUnregisterColumnFromForeignKey() throws SQLManipulationException {
		Table table = new AbstractTable("unregister_fk") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("parent")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setForeignKeyConstraint(ForeignKeyConstraint.builder().setName("parent_fk")
																								    .setReferentialTable("register_fk")
																								    .setReferentialPrimaryKey("id")
																								    .build())
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());

		manipulator.unregisterColumnFromForeignKey(TableColumn.builder().setName("parent")
																		.setForeignKeyConstraint(ForeignKeyConstraint.builder().setName("parent_fk")
																			    .setReferentialTable("register_fk")
																			    .setReferentialPrimaryKey("id")
																			    .build())
																		.build());		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#registerColumnAsUniqueKey(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRegisterColumnAsUniqueKey() throws SQLManipulationException {
		Table table = new AbstractTable("register_uk") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());

		manipulator.registerColumnAsUniqueKey(TableColumn.builder().setName("comment")
																	.build());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#unregisterColumnFromUniquekKey(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testUnregisterColumnFromUniquekKey() throws SQLManipulationException {
		Table table = new AbstractTable("unregister_uk") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .setAsUniqueKey("comment_uk")
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());

		manipulator.unregisterColumnFromUniquekKey(TableColumn.builder().setName("comment")
																		.setAsUniqueKey("comment_uk")
																		.build());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#removeColumn(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRemoveColumnByTableColumn() throws SQLManipulationException {
		Table table = new AbstractTable("remove_column_by_table_column") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		TableColumn idColumn = manipulator.getColumn("id");
		assertEquals("id", idColumn.getName());
		assertEquals(TableColumnDataTypes.SmallInteger, idColumn.getDataType());

		TableColumn commentColumn = manipulator.getColumn("comment");
		assertEquals("comment", commentColumn.getName());
		assertEquals(TableColumnDataTypes.VarChar, commentColumn.getDataType());
		assertEquals(16, commentColumn.getLength().intValue());
		
		manipulator.removeColumn(TableColumn.builder().setName("comment")
													  .build());
		
		commentColumn = manipulator.getColumn("comment");
		assertNull(commentColumn);
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#removeColumn(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRemoveColumnByName() throws SQLManipulationException {
		Table table = new AbstractTable("remove_column_by_name") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		TableColumn idColumn = manipulator.getColumn("id");
		assertEquals("id", idColumn.getName());
		assertEquals(TableColumnDataTypes.SmallInteger, idColumn.getDataType());

		TableColumn commentColumn = manipulator.getColumn("comment");
		assertEquals("comment", commentColumn.getName());
		assertEquals(TableColumnDataTypes.VarChar, commentColumn.getDataType());
		assertEquals(16, commentColumn.getLength().intValue());
		
		manipulator.removeColumn("comment");
		
		commentColumn = manipulator.getColumn("comment");
		assertNull(commentColumn);
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#deleteTable(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testDeleteTableByName() throws SQLManipulationException {
		Table table = new AbstractTable("delete_table_by_name") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#clearTable(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testClearTableByName() throws SQLManipulationException {
		final String TB_NAME = "clear_table_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		
		assertEquals(5, manipulator.getTotalRowCount().intValue());
		
		manipulator.clearTable();		
		assertEquals(0, manipulator.getTotalRowCount().intValue());
		
		dbManipulator.executeUpdate(query);
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
				
		ResultBundle result = dbManipulator.executeQuery(query);
		assertTrue(result.next());
		assertEquals("6", result.getString("id"));
		assertTrue(result.next());
		assertEquals("7", result.getString("id"));
		assertTrue(result.next());
		assertEquals("8", result.getString("id"));
		assertTrue(result.next());
		assertEquals("9", result.getString("id"));
		assertTrue(result.next());
		assertEquals("10", result.getString("id"));
		assertFalse(result.next());		
		result.release();
		
		manipulator.clearTable("clear_table_by_name");
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		
		result = dbManipulator.executeQuery(query);
		assertFalse(result.next());
		result.release();
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#resetTable(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testResetTableString() throws SQLManipulationException {

		final String TB_NAME = "reset_table_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		
		assertEquals(5, manipulator.getTotalRowCount().intValue());
		
		manipulator.clearTable();		
		assertEquals(0, manipulator.getTotalRowCount().intValue());
		
		dbManipulator.executeUpdate(query);
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
				
		ResultBundle result = dbManipulator.executeQuery(query);
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertTrue(result.next());
		assertEquals("2", result.getString("id"));
		assertTrue(result.next());
		assertEquals("3", result.getString("id"));
		assertTrue(result.next());
		assertEquals("4", result.getString("id"));
		assertTrue(result.next());
		assertEquals("5", result.getString("id"));
		assertFalse(result.next());
		result.release();
		
		manipulator.resetTable("reset_table_by_name");
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		
		result = dbManipulator.executeQuery(query);
		assertFalse(result.next());
		result.release();
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#isTableExisting(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsTableExistingByName() throws SQLManipulationException {
		final String TB_NAME = "is_table_existing_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting(TB_NAME));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#getTotalRowCount()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testGetTotalRowCount() throws SQLManipulationException {
		final String TB_NAME = "get_total_row_count";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		
		assertEquals(5, manipulator.getTotalRowCount().intValue());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#getTotalRowCount(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testGetTotalRowCountByName() throws SQLManipulationException {
		final String TB_NAME = "get_total_row_count_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		
		assertEquals(5, manipulator.getTotalRowCount(TB_NAME).intValue());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#isEmpty()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsEmpty() throws SQLManipulationException {
		Table table = new AbstractTable("is_empty_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#isEmpty(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsEmptyByName() throws SQLManipulationException {
		final String TB_NAME = "is_empty_table_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isEmpty());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#isEmpty()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsNotEmpty() throws SQLManipulationException {
		final String TB_NAME = "is_not_empty_table";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isEmpty());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		assertFalse(manipulator.isEmpty());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#isEmpty(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsNotEmptyByName() throws SQLManipulationException {
		final String TB_NAME = "is_not_empty_table_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isEmpty());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		assertFalse(manipulator.isEmpty(TB_NAME));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator#builder()}.
	 */
	@Test
	public void testBuilder() {
		MySQLTableManipulator.builder();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetTable() {
		Table table = new AbstractTable("set_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		TableManipulator manipulator = MySQLTableManipulator.builder().build();
		assertNull(manipulator.getTable());
		
		manipulator.setTable(table);
		assertEquals("set_table", manipulator.getTable().getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#getTable()}.
	 */
	@Test
	public void testGetTable() {
		Table table = new AbstractTable("get_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		TableManipulator manipulator = MySQLTableManipulator.builder().setTable(table).build();
		
		assertEquals("get_table", manipulator.getTable().getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setDatabaseEngineType(com.digsarustudio.banana.database.DatabaseEngineType)}.
	 */
	@Test
	public void testSetDatabaseEngineType() {
		TableManipulator manipulator = MySQLTableManipulator.builder().build();
		assertNull(manipulator.getDatabaseEngineType());
		
		manipulator.setDatabaseEngineType(DatabaseEngineType.InnoDB);
		assertEquals(DatabaseEngineType.InnoDB, manipulator.getDatabaseEngineType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#getDatabaseEngineType()}.
	 */
	@Test
	public void testGetDatabaseEngineType() {
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		assertEquals(DatabaseEngineType.InnoDB, manipulator.getDatabaseEngineType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setPreventErrorIfTableExists()}.
	 */
	@Test
	public void testSetPreventErrorIfTableExists() {
		TableManipulator manipulator = MySQLTableManipulator.builder().build();		
		assertFalse(manipulator.isPreventErrorIfTableExists());
		
		manipulator.setPreventErrorIfTableExists();
		assertTrue(manipulator.isPreventErrorIfTableExists());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setNotPreventErrorIfTableExists()}.
	 */
	@Test
	public void testSetNotPreventErrorIfTableExists() {
		TableManipulator manipulator = MySQLTableManipulator.builder().build();		
		assertFalse(manipulator.isPreventErrorIfTableExists());
		
		manipulator.setNotPreventErrorIfTableExists();
		assertFalse(manipulator.isPreventErrorIfTableExists());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#isPreventErrorIfTableExists()}.
	 */
	@Test
	public void testIsPreventErrorIfTableExists() {
		TableManipulator manipulator = MySQLTableManipulator.builder().setPreventErrorIfTableExists()
																	  .build();		
		assertTrue(manipulator.isPreventErrorIfTableExists());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#renameTable(com.digsarustudio.banana.database.table.Table)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testRenameTableByTable() throws SQLManipulationException {
		Table renamedTable = new AbstractTable("renamed_table_by_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		Table table = new AbstractTable("rename_table_by_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.setTable(renamedTable);		
		manipulator.renameTable(table);
		
		assertTrue(manipulator.isTableExisting("renamed_table_by_table"));
		assertFalse(manipulator.isTableExisting("rename_table_by_table"));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#changeColumnName(com.digsarustudio.banana.database.table.TableColumn, com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testChangeColumnNameByOriginalTableColumnAndNewTableColumn() throws SQLManipulationException {
		Table table = new AbstractTable("change_column_name_by_table_column_2") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		TableColumn comment = TableColumn.builder().setName("comment")
												   .setDataType(TableColumnDataTypes.VarChar)
												   .setLength(16)
												   .build();
		table.addColumn(comment);
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.changeColumnName(comment, TableColumn.builder().setName("renamed_comment")
																   .setDataType(TableColumnDataTypes.VarChar)
																   .setLength(16)
																   .build());
		
		assertFalse(manipulator.isColumnExisting("comment"));
		assertTrue(manipulator.isColumnExisting("renamed_comment"));
		assertEquals(TableColumnDataTypes.VarChar, manipulator.getColumn("renamed_comment").getDataType());
		assertEquals(64, manipulator.getColumn("renamed_comment").getLength().intValue());
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#isColumnExisting(java.lang.String)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsColumnExistingByName() throws SQLManipulationException {
		Table table = new AbstractTable("is_column_existing_by_name") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isColumnExisting("id"));
		assertTrue(manipulator.isColumnExisting("comment"));
		assertFalse(manipulator.isColumnExisting("parent"));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#isColumnExisting(com.digsarustudio.banana.database.table.TableColumn)}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsColumnExistingByTableColumn() throws SQLManipulationException {
		Table table = new AbstractTable("is_column_existing_by_table_column") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		TableColumn idColumn = TableColumn.builder().setName("id")
													.setDataType(TableColumnDataTypes.SmallInteger)
													.build(); 
		table.addColumn(idColumn);
		
		TableColumn commentColumn = TableColumn.builder().setName("comment")
														 .setDataType(TableColumnDataTypes.VarChar)
														 .setLength(16)
														 .build();
		table.addColumn(commentColumn);
		
		TableColumn parentColumn = TableColumn.builder().setName("parent")
														 .setDataType(TableColumnDataTypes.VarChar)
														 .setLength(16)
														 .build();
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isColumnExisting(idColumn));
		assertTrue(manipulator.isColumnExisting(commentColumn));
		assertFalse(manipulator.isColumnExisting(parentColumn));
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#deleteTable()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testDeleteTable() throws SQLManipulationException {
		Table table = new AbstractTable("delete_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		TableColumn idColumn = TableColumn.builder().setName("id")
													.setDataType(TableColumnDataTypes.SmallInteger)
													.build(); 
		table.addColumn(idColumn);
		
		TableColumn commentColumn = TableColumn.builder().setName("comment")
														 .setDataType(TableColumnDataTypes.VarChar)
														 .setLength(16)
														 .build();
		table.addColumn(commentColumn);
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.deleteTable();
		
		assertFalse(manipulator.isTableExisting());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#clearTable()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testClearTable() throws SQLManipulationException {
		final String TB_NAME = "clear_table";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		
		assertEquals(5, manipulator.getTotalRowCount().intValue());
		
		manipulator.clearTable();		
		assertEquals(0, manipulator.getTotalRowCount().intValue());
		
		dbManipulator.executeUpdate(query);
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
				
		ResultBundle result = dbManipulator.executeQuery(query);
		assertTrue(result.next());
		assertEquals("6", result.getString("id"));
		assertTrue(result.next());
		assertEquals("7", result.getString("id"));
		assertTrue(result.next());
		assertEquals("8", result.getString("id"));
		assertTrue(result.next());
		assertEquals("9", result.getString("id"));
		assertTrue(result.next());
		assertEquals("10", result.getString("id"));
		assertFalse(result.next());		
		result.release();
		
		manipulator.clearTable();
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		
		result = dbManipulator.executeQuery(query);
		assertFalse(result.next());
		result.release();
		
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#resetTable()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testResetTable() throws SQLManipulationException {

		final String TB_NAME = "reset_table";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();		
		assertTrue(manipulator.isTableExisting());
		
		String query = String.format("INSERT INTO `%s` (`id`, `comment`) VALUES (('1st'), ('2nd'), ('3rd'), ('4th'), ('5th'));"
									, TB_NAME);		
		dbManipulator.executeUpdate(query);
		
		assertEquals(5, manipulator.getTotalRowCount().intValue());
		
		manipulator.clearTable();		
		assertEquals(0, manipulator.getTotalRowCount().intValue());
		
		dbManipulator.executeUpdate(query);
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
				
		ResultBundle result = dbManipulator.executeQuery(query);
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertTrue(result.next());
		assertEquals("2", result.getString("id"));
		assertTrue(result.next());
		assertEquals("3", result.getString("id"));
		assertTrue(result.next());
		assertEquals("4", result.getString("id"));
		assertTrue(result.next());
		assertEquals("5", result.getString("id"));
		assertFalse(result.next());
		result.release();
		
		manipulator.resetTable();
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		
		result = dbManipulator.executeQuery(query);
		assertFalse(result.next());
		result.release();
		manipulator.deleteTable();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#isTableExisting()}.
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsTableExisting() throws SQLManipulationException {
		final String TB_NAME = "is_table_existing_by_name";
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .build());
		
		table.addColumn(TableColumn.builder().setName("comment")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(16)
											 .build());
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setDatabaseManipulator(dbManipulator)
																	  .setTable(table)
																	  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	  .build();
		
		manipulator.createTable();
		
		assertTrue(manipulator.isTableExisting());
		
		manipulator.deleteTable();
	}

}
