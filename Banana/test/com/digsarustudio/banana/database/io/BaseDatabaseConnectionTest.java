/**
 * 
 */
package com.digsarustudio.banana.database.io;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.activity.InvalidActivityException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.mysql.MySQLConnection;
import com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator;
import com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link BaseDatabaseConnection}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		1.0.2
 *
 */
public class BaseDatabaseConnectionTest {
	private static final String DB_NAME	= "junit_test_banana";
	private static final String TB_NAME = "base_database_connection";
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DatabaseConnection connection = MySQLConnection.builder().setHostName("localhost")
																 .setUserName("db_boss")
																 .setPassword("1234")
																 .build();
		
		DatabaseManipulator manipulator = MySQLDatabaseManipulator.builder().setConnection(connection)
																			.setName(DB_NAME)
																			.setCharacterSets(CharacterSets.UTF8Unicode)
																			.setCollations(Collations.UTF8Unicode)
																			.build();
																			
		manipulator.open();
		manipulator.createDatabase();
		
		Table table = new AbstractTable(TB_NAME) {
			
			@Override
			protected void initColumns() {
			}
		};
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAsUnsignedColumn()
											 .setAutoIncrement()
											 .setNonNullable()
											 .setAsPrimaryKey()
											 .build());
		table.addColumn(TableColumn.builder().setName("caption")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNonNullable()
											 .build());
		
		TableManipulator tbManipulator = MySQLTableManipulator.builder().setDatabaseManipulator(manipulator)
																		.setTable(table)
																		.setPreventErrorIfTableExists()
																		.setDatabaseEngineType(DatabaseEngineType.InnoDB)
																		.build();
		tbManipulator.createTable();
		
		manipulator.close();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#BaseDatabaseConnection(java.lang.String, java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testBaseDatabaseConnection() throws DriverNotLoadedException {
		new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#BaseDatabaseConnection(java.lang.String, java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test(expected = DriverNotLoadedException.class)
	public void testUseUnexistDriver() throws DriverNotLoadedException {
		new BaseDatabaseConnection("yoursql", "com.yoursql.jdbc.Driver") {
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#open()}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testOpen() throws SQLManipulationException, DriverNotLoadedException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		connection.setHostName("localhost");
		connection.setUserName("db_boss");
		connection.setPassword("1234");
		connection.open();
		
		String query = String.format("USE `%s`;", DB_NAME);
		connection.executeUpdate(query);
		
		query = String.format("INSERT INTO `%s` (`%s`) VALUES('%s');", TB_NAME, "caption", "oepn");
		connection.executeUpdate(query);
		
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		ResultBundle result = connection.executeQuery(query);
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("oepn", result.getString("caption"));
		
		query = String.format("TRUNCATE TABLE `%s`;", TB_NAME);
		connection.executeUpdate(query);
		
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#close()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 * @throws InvalidActivityException 
	 */
	@Test(expected = InvalidActivityException.class)
	public void testClose() throws DriverNotLoadedException, SQLManipulationException, InvalidActivityException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		connection.setHostName("localhost");
		connection.setUserName("db_boss");
		connection.setPassword("1234");
		connection.open();
		
		String query = String.format("USE `%s`;", DB_NAME);
		connection.executeUpdate(query);
		
		query = String.format("INSERT INTO `%s` (`%s`) VALUES('%s');", TB_NAME, "caption", "close");
		connection.executeUpdate(query);
		
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		ResultBundle result = connection.executeQuery(query);
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("close", result.getString("caption"));
		
		query = String.format("TRUNCATE TABLE `%s`;", TB_NAME);
		connection.executeUpdate(query);
		
		connection.close();
		
		try {
			query = String.format("USE `%s`;", DB_NAME);
			connection.executeUpdate(query);
		}catch(SQLManipulationException e) {
			throw new InvalidActivityException();
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#executeUpdate(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testExecuteUpdate() throws DriverNotLoadedException, SQLManipulationException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		connection.setHostName("localhost");
		connection.setUserName("db_boss");
		connection.setPassword("1234");
		connection.open();
		
		String query = String.format("USE `%s`;", DB_NAME);
		connection.executeUpdate(query);
		
		query = String.format("INSERT INTO `%s` (`%s`) VALUES('%s');", TB_NAME, "caption", "execute Update");
		connection.executeUpdate(query);
		
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		ResultBundle result = connection.executeQuery(query);
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("execute Update", result.getString("caption"));
		
		query = String.format("TRUNCATE TABLE `%s`;", TB_NAME);
		connection.executeUpdate(query);
		
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#executeQuery(java.lang.String)}.
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 * @throws SQLException 
	 */
	@Test
	public void testExecuteQuery() throws SQLManipulationException, DriverNotLoadedException, SQLException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		connection.setHostName("localhost");
		connection.setUserName("db_boss");
		connection.setPassword("1234");
		connection.open();
		
		String query = String.format("USE `%s`;", DB_NAME);
		connection.executeUpdate(query);
		
		query = String.format("INSERT INTO `%s` (`%s`) VALUES('%s');", TB_NAME, "caption", "execute Query");
		connection.executeUpdate(query);
		
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		ResultBundle result = connection.executeQuery(query);
		assertTrue(result.next());
		assertEquals("1", result.getString("id"));
		assertEquals("execute Query", result.getString("caption"));
		
		query = String.format("TRUNCATE TABLE `%s`;", TB_NAME);
		connection.executeUpdate(query);
		
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#executeStatementQuery(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 * @throws SQLException 
	 */
	@Test
	public void testExecuteStatementQuery() throws DriverNotLoadedException, SQLManipulationException, SQLException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		connection.setHostName("localhost");
		connection.setUserName("db_boss");
		connection.setPassword("1234");
		connection.open();
		
		String query = String.format("USE `%s`;", DB_NAME);
		connection.executeUpdate(query);
		
		query = String.format("INSERT INTO `%s` (`%s`) VALUES('%s');", TB_NAME, "caption", "execute statement query");
		connection.executeUpdate(query);
		
		query = String.format("SELECT * FROM `%s`;", TB_NAME);
		ResultSet result = connection.executeStatementQuery(query);
		assertTrue(result.next());
		assertEquals(1, result.getInt("id"));
		assertEquals("execute statement query", result.getString("caption"));
		
		query = String.format("TRUNCATE TABLE `%s`;", TB_NAME);
		connection.executeUpdate(query);
		
		connection.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setHostName(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetHostName() throws DriverNotLoadedException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		assertNull(connection.getHostName());
		
		connection.setHostName("localhost");
		
		assertEquals("hostname", connection.getHostName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setPort(java.lang.Integer)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetPort() throws DriverNotLoadedException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		assertNull(connection.getPort());
		
		connection.setPort(3312);
		
		assertEquals(3312, connection.getPort().intValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setUserName(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetUserName() throws DriverNotLoadedException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		assertNull(connection.getUserName());
		
		connection.setUserName("boos");
		
		assertEquals("boos", connection.getUserName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#setPassword(java.lang.String)}.
	 * @throws DriverNotLoadedException 
	 */
	@Test
	public void testSetPassword() throws DriverNotLoadedException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		assertNull(connection.getPassword());
		
		connection.setPassword("123444");
		
		assertEquals("123444", connection.getPassword());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseDatabaseConnection#isConnected()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 */
	@Test
	public void testIsConnected() throws DriverNotLoadedException, SQLManipulationException {
		BaseDatabaseConnection connection = new BaseDatabaseConnection("mysql", "com.mysql.jdbc.Driver") {
		};
		
		assertFalse(connection.isConnected());
		
		connection.setHostName("localhost");
		connection.setUserName("db_boss");
		connection.setPassword("1234");
		connection.open();
		
		assertTrue(connection.isConnected());
		
		connection.close();
		
		assertFalse(connection.isConnected());
	}
}
