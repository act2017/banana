/**
 * 
 */
package com.digsarustudio.banana.database.io;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;

/**
 * To test {@link BaseTableManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class BaseTableManipulatorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#BaseTableManipulator()}.
	 */
	@Test
	public void testBaseTableManipulator() {
		new BaseTableManipulator() {
			
			@Override
			public void unregisterPrimaryKeys() throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void resetTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void renameTable(String originalName) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void modifyColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public Boolean isTableExisting(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public TableColumn getColumn(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public void deleteTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void createTable() throws SQLManipulationException {

				
			}
			
			@Override
			public void clearTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException {

				
			}
			
			@Override
			public void addColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			protected String getUniqueKeyString(Table table) {

				return null;
			}
			
			@Override
			protected String getPrimaryKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getFullTextColumnString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeyDefinition(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getEnumString(List<String> enums) {

				return null;
			}
			
			@Override
			protected String getDefinitions(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getColumnNamesString(Table table) {

				return null;
			}
			
			@Override
			protected String getColumnDefinition(TableColumn column) {

				return null;
			}

			@Override
			public Integer getTotalRowCount() throws SQLManipulationException {

				return null;
			}

			@Override
			public Integer getTotalRowCount(String name) throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty() throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty(String name) throws SQLManipulationException {

				return null;
			}
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetTable() {
		BaseTableManipulator manipulator = new BaseTableManipulator() {
			
			@Override
			public void unregisterPrimaryKeys() throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void resetTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void renameTable(String originalName) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void modifyColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public Boolean isTableExisting(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public TableColumn getColumn(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public void deleteTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void createTable() throws SQLManipulationException {

				
			}
			
			@Override
			public void clearTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException {

				
			}
			
			@Override
			public void addColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			protected String getUniqueKeyString(Table table) {

				return null;
			}
			
			@Override
			protected String getPrimaryKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getFullTextColumnString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeyDefinition(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getEnumString(List<String> enums) {

				return null;
			}
			
			@Override
			protected String getDefinitions(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getColumnNamesString(Table table) {

				return null;
			}
			
			@Override
			protected String getColumnDefinition(TableColumn column) {

				return null;
			}

			@Override
			public Integer getTotalRowCount() throws SQLManipulationException {

				return null;
			}

			@Override
			public Integer getTotalRowCount(String name) throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty() throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty(String name) throws SQLManipulationException {

				return null;
			}
		};
		
		Table table = new AbstractTable("base_table_manipulator_set_table") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		manipulator.setTable(table);
		
		assertEquals("base_table_manipulator_set_table", table.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setDatabaseEngineType(com.digsarustudio.banana.database.DatabaseEngineType)}.
	 */
	@Test
	public void testSetDatabaseEngineType() {
		BaseTableManipulator manipulator = new BaseTableManipulator() {
			
			@Override
			public void unregisterPrimaryKeys() throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void resetTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void renameTable(String originalName) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void modifyColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public Boolean isTableExisting(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public TableColumn getColumn(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public void deleteTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void createTable() throws SQLManipulationException {

				
			}
			
			@Override
			public void clearTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException {

				
			}
			
			@Override
			public void addColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			protected String getUniqueKeyString(Table table) {

				return null;
			}
			
			@Override
			protected String getPrimaryKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getFullTextColumnString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeyDefinition(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getEnumString(List<String> enums) {

				return null;
			}
			
			@Override
			protected String getDefinitions(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getColumnNamesString(Table table) {

				return null;
			}
			
			@Override
			protected String getColumnDefinition(TableColumn column) {

				return null;
			}

			@Override
			public Integer getTotalRowCount() throws SQLManipulationException {

				return null;
			}

			@Override
			public Integer getTotalRowCount(String name) throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty() throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty(String name) throws SQLManipulationException {

				return null;
			}
		};
		
		manipulator.setDatabaseEngineType(DatabaseEngineType.InnoDB);
		
		assertEquals(DatabaseEngineType.InnoDB, manipulator.getDatabaseEngineType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setPreventErrorIfTableExists()}.
	 */
	@Test
	public void testSetPreventErrorIfTableExists() {
		BaseTableManipulator manipulator = new BaseTableManipulator() {
			
			@Override
			public void unregisterPrimaryKeys() throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void resetTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void renameTable(String originalName) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void modifyColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public Boolean isTableExisting(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public TableColumn getColumn(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public void deleteTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void createTable() throws SQLManipulationException {

				
			}
			
			@Override
			public void clearTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException {

				
			}
			
			@Override
			public void addColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			protected String getUniqueKeyString(Table table) {

				return null;
			}
			
			@Override
			protected String getPrimaryKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getFullTextColumnString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeyDefinition(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getEnumString(List<String> enums) {

				return null;
			}
			
			@Override
			protected String getDefinitions(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getColumnNamesString(Table table) {

				return null;
			}
			
			@Override
			protected String getColumnDefinition(TableColumn column) {

				return null;
			}

			@Override
			public Integer getTotalRowCount() throws SQLManipulationException {

				return null;
			}

			@Override
			public Integer getTotalRowCount(String name) throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty() throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty(String name) throws SQLManipulationException {

				return null;
			}
		};
		
		manipulator.setPreventErrorIfTableExists();
		assertTrue(manipulator.isPreventingErrorIfTableExists);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.io.BaseTableManipulator#setNotPreventErrorIfTableExists()}.
	 */
	@Test
	public void testSetNotPreventErrorIfTableExists() {
		BaseTableManipulator manipulator = new BaseTableManipulator() {
			
			@Override
			public void unregisterPrimaryKeys() throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void resetTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void renameTable(String originalName) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void removeColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public void modifyColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			public Boolean isTableExisting(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public TableColumn getColumn(String name) throws SQLManipulationException {

				return null;
			}
			
			@Override
			public void deleteTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void createTable() throws SQLManipulationException {

				
			}
			
			@Override
			public void clearTable(String name) throws SQLManipulationException {

				
			}
			
			@Override
			public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException {

				
			}
			
			@Override
			public void addColumn(TableColumn column) throws SQLManipulationException {

				
			}
			
			@Override
			protected String getUniqueKeyString(Table table) {

				return null;
			}
			
			@Override
			protected String getPrimaryKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getFullTextColumnString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeysString(Table table) {

				return null;
			}
			
			@Override
			protected String getForeignKeyDefinition(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getEnumString(List<String> enums) {

				return null;
			}
			
			@Override
			protected String getDefinitions(TableColumn column) {

				return null;
			}
			
			@Override
			protected String getColumnNamesString(Table table) {

				return null;
			}
			
			@Override
			protected String getColumnDefinition(TableColumn column) {

				return null;
			}

			@Override
			public Integer getTotalRowCount() throws SQLManipulationException {

				return null;
			}

			@Override
			public Integer getTotalRowCount(String name) throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty() throws SQLManipulationException {

				return null;
			}

			@Override
			public Boolean isEmpty(String name) throws SQLManipulationException {

				return null;
			}
		};
		
		manipulator.setNotPreventErrorIfTableExists();
		assertFalse(manipulator.isPreventingErrorIfTableExists);
	}
}
