/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.director;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.manipulation.director.ManipulationDirector;
import com.digsarustudio.banana.database.manipulation.manipulator.InvalidDataException;
import com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy;
import com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy;
import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.SimpleQueryTable;
import com.digsarustudio.banana.database.query.SortingType;
import com.digsarustudio.banana.database.query.Table;

/**
 * To test {@link ManipulationDirector}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class ManipulationDirectorTest {
	class Item{
		private String id = null;
		private String name = null;
		
		public Item(){
			
		}

		/**
		 * @return the id
		 */
		public String getId() {
			return id;
		}

		/**
		 * @param id the id to set
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * @return the name
		 */
		public String getName() {
			return name;
		}

		/**
		 * @param name the name to set
		 */
		public void setName(String name) {
			this.name = name;
		}
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.director.ManipulationDirector#ManipulationDirector()}.
	 */
	@Test
	public void testManipulationDirector() {
		new ManipulationDirector<String, String>() {
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.director.ManipulationDirector#setDataManipulationStrategy(com.digsarustudio.banana.database.manipulation.DataManipulationStrategy)}.
	 */
	@Test
	public void testSetDataManipulationStrategy() {
		DataManipulationDirector<String, String> director = new ManipulationDirector<String, String>() {
		};
		
		DataManipulationStrategy<String, String> strategy = new ManipulationStrategy<String, String>() {

			@Override
			public List<String> execute(String data) throws SQLManipulationException, DriverNotLoadedException {

				return null;
			}

			@Override
			protected void validateData(String data) throws IllegalArgumentException {

				
			}

			@Override
			protected Table generateTable() {

				return null;
			}

			@Override
			protected void setupTables() {

				
			}

			@Override
			protected void setupColumns() {

				
			}

			@Override
			protected List<Dataset> getDatasets(String source) {

				return null;
			}

			@Override
			protected List<SelectColumn> getSelectColumns() {

				return null;
			}

			@Override
			protected List<JoinTable> getJoinTables() {

				return null;
			}

			@Override
			protected List<Condition> getConditions(String source) {

				return null;
			}

			@Override
			protected List<Aggregation> getAggregations() {

				return null;
			}

			@Override
			protected List<Ordering> getOrderings() {

				return null;
			}
		};
		
		director.setDataManipulationStrategy(strategy);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.director.ManipulationDirector#setDataModel(java.lang.Object)}.
	 */
	@Test
	public void testSetDataModel() {
		DataManipulationDirector<Item, Item> director = new ManipulationDirector<Item, Item>() {
		};
		
		director.setDataModel(new Item());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.director.ManipulationDirector#execute()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 * @throws InvalidDataException 
	 */
	@Test
	public void testExecute() throws DriverNotLoadedException, SQLManipulationException, InvalidDataException {
		DataManipulationDirector<Item, Item> director = new ManipulationDirector<Item, Item>() {
		};
		
		DataManipulationStrategy<Item, Item> strategy = new ManipulationStrategy<Item, Item>() {

			@Override
			public List<Item> execute(Item data) throws SQLManipulationException, DriverNotLoadedException {
				Boolean isCaught = false;
				try {
					validateData(data);
				}catch (UnsupportedOperationException e) {
					isCaught = true;
				} finally {
					assertTrue(isCaught);
				}
				
				assertEquals("generated_table", generateTable().getName());
				
				isCaught = false;
				try {
					setupTables();
				}catch (UnsupportedOperationException e) {
					isCaught = true;
				} finally {
					assertTrue(isCaught);
				}
				
				isCaught = false;
				try {
					setupColumns();
				}catch (UnsupportedOperationException e) {
					isCaught = true;
				} finally {
					assertTrue(isCaught);
				}
				
				assertEquals("execute", getDatasets(data).get(0).getValue());				
				assertEquals("select_columns", getSelectColumns().get(0).getAlias());
				assertEquals("join_tables", getJoinTables().get(0).getAlias());
				assertEquals("execute", getConditions(data).get(0).getValue());
				assertEquals(SortingType.Descending, getAggregations().get(0).getSortingType());
				assertEquals(SortingType.Descending, getOrderings().get(0).getType());				
				
				List<Item> rtns = new ArrayList<>();
				
				Item item = new Item();
				item.setName("executed");
				
				rtns.add(item);
				return rtns;
			}

			@Override
			protected void validateData(Item data) throws IllegalArgumentException {
				throw new UnsupportedOperationException();
				
			}

			@Override
			protected Table generateTable() {				
				return SimpleQueryTable.builder().setName("generated_table").build();
			}

			@Override
			protected void setupTables() {
				throw new UnsupportedOperationException();
				
			}

			@Override
			protected void setupColumns() {
				throw new UnsupportedOperationException();
				
			}

			@Override
			protected List<Dataset> getDatasets(Item source) {
				List<Dataset> rtns = new ArrayList<>();
				rtns.add(Dataset.builder().setValue(source.getName()).build());
				return rtns;
			}

			@Override
			protected List<SelectColumn> getSelectColumns() {
				List<SelectColumn> rtns = new ArrayList<>();
				rtns.add(SelectColumn.builder().setAlias("select_columns").build());
				return rtns;
			}

			@Override
			protected List<JoinTable> getJoinTables() {
				List<JoinTable> rtns = new ArrayList<>();
				rtns.add(JoinTable.builder().setAlias("join_tables").build());
				return rtns;
			}

			@Override
			protected List<Condition> getConditions(Item source) {
				List<Condition> rtns = new ArrayList<>();
				rtns.add(Condition.builder().setValue(source.getName()).build());
				return rtns;
			}

			@Override
			protected List<Aggregation> getAggregations() {
				List<Aggregation> rtns = new ArrayList<>();
				rtns.add(Aggregation.builder().setSortingType(SortingType.Descending).build());
				return rtns;
			}

			@Override
			protected List<Ordering> getOrderings() {
				List<Ordering> rtns = new ArrayList<>();
				rtns.add(Ordering.builder().setType(SortingType.Descending).build());
				return rtns;
			}

		};
		
		director.setDataManipulationStrategy(strategy);
		
		Item item = new Item();
		item.setName("execute");
		director.setDataModel(item);
		List<Item> rtns = director.execute();
		
		assertEquals(1, rtns.size());
		assertEquals("executed", item.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.director.ManipulationDirector#getSQLQuery()}.
	 * @throws DriverNotLoadedException 
	 * @throws SQLManipulationException 
	 * @throws InvalidDataException 
	 */
	@Test
	public void testGetSQLString() throws SQLManipulationException, DriverNotLoadedException, InvalidDataException {
		DataManipulationDirector<Item, Item> director = new ManipulationDirector<Item, Item>() {
		};
		
		DataManipulationStrategy<Item, Item> strategy = new ManipulationStrategy<Item, Item>() {

			@Override
			public List<Item> execute(Item data) throws SQLManipulationException, DriverNotLoadedException {
				validateData(data);
				String sql = getSQLQuery() + ", " + generateTable().getName();
				setSQLQuery(sql);
				setupTables();
				setupColumns();
				
				sql = getSQLQuery() + ", " + getDatasets(data).get(0).getValue();				
				sql += ", " + getSelectColumns().get(0).getAlias();
				sql += ", " + getJoinTables().get(0).getAlias();
				sql += ", " + getConditions(data).get(0).getValue();
				sql += ", " + getAggregations().get(0).getSortingType();
				sql += ", " + getOrderings().get(0).getType();
				
				setSQLQuery(sql);
				
				List<Item> rtns = new ArrayList<>();
				
				Item item = new Item();
				item.setName("executed");
				
				rtns.add(item);
				return rtns;
			}

			@Override
			protected void validateData(Item data) throws IllegalArgumentException {
				String sql = getSQLQuery();
				sql += data.getName();
				setSQLQuery(sql);				
			}

			@Override
			protected Table generateTable() {				
				return SimpleQueryTable.builder().setName("generated_table").build();
			}

			@Override
			protected void setupTables() {
				String sql = getSQLQuery();
				sql += ", set_up_table";				
				setSQLQuery(sql);
				
			}

			@Override
			protected void setupColumns() {
				String sql = getSQLQuery();
				sql += ", set_up_columns";				
				setSQLQuery(sql);
			}

			@Override
			protected List<Dataset> getDatasets(Item source) {
				List<Dataset> rtns = new ArrayList<>();
				rtns.add(Dataset.builder().setValue(source.getName()).build());
				return rtns;
			}

			@Override
			protected List<SelectColumn> getSelectColumns() {
				List<SelectColumn> rtns = new ArrayList<>();
				rtns.add(SelectColumn.builder().setAlias("select_columns").build());
				return rtns;
			}

			@Override
			protected List<JoinTable> getJoinTables() {
				List<JoinTable> rtns = new ArrayList<>();
				rtns.add(JoinTable.builder().setAlias("join_tables").build());				
				return rtns;
			}

			@Override
			protected List<Condition> getConditions(Item source) {
				List<Condition> rtns = new ArrayList<>();
				rtns.add(Condition.builder().setValue(source.getName()).build());
				return rtns;
			}

			@Override
			protected List<Aggregation> getAggregations() {
				List<Aggregation> rtns = new ArrayList<>();
				rtns.add(Aggregation.builder().setSortingType(SortingType.Descending).build());
				return rtns;
			}

			@Override
			protected List<Ordering> getOrderings() {
				List<Ordering> rtns = new ArrayList<>();
				rtns.add(Ordering.builder().setType(SortingType.Descending).build());
				return rtns;
			}

		};
		
		director.setDataManipulationStrategy(strategy);
		
		Item item = new Item();
		item.setName("execute");
		director.setDataModel(item);
		director.execute();
		
		assertEquals("execute, generated_table, set_up_table, set_up_columns, execute, select_columns, join_tables, execute, DESC, DESC"
					, director.getSQLQuery());
	}

}
