/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory;

/**
 * To tset {@link MySQLDataAccessObjectFactory}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLDataAccessObjectFacotryTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#MySQLDataAccessObjectFactory()}.
	 */
	@Test
	public void testMySQLDataAccessObjectFactory() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createDatabaseConnection()}.
	 */
	@Test
	public void testCreateDatabaseConnection() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createDatabaseManipulator()}.
	 */
	@Test
	public void testCreateDatabaseManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseConnection)}.
	 */
	@Test
	public void testCreateDatabaseManipulatorDatabaseConnection() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createTableManipulator()}.
	 */
	@Test
	public void testCreateTableManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createTableManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)}.
	 */
	@Test
	public void testCreateTableManipulatorDatabaseManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createStatementCompiler()}.
	 */
	@Test
	public void testCreateStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.mysql.MySQLDataAccessObjectFactory#createClauseCompiler()}.
	 */
	@Test
	public void testCreateClauseCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObjectFactory#DataAccessObjectFactory()}.
	 */
	@Test
	public void testDataAccessObjectFactory() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObjectFactory#setDatabaseConfig(com.digsarustudio.banana.database.DatabaseConfig)}.
	 */
	@Test
	public void testSetDatabaseConfig() {
		fail("Not yet implemented");
	}

}
