/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject;

/**
 * To test {@link DataAccessObject}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DataAccessObjectFactoryTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObjectFactory#DataAccessObjectFactory()}.
	 */
	@Test
	public void testDataAccessObjectFactory() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObjectFactory#setDatabaseConfig(com.digsarustudio.banana.database.DatabaseConfig)}.
	 */
	@Test
	public void testSetDatabaseConfig() {
		fail("Not yet implemented");
	}

}
