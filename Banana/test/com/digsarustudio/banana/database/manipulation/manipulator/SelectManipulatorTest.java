/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.manipulation.manipulator.SelectManipulator;

/**
 * To test {@link SelectManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SelectManipulatorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.SelectManipulator#init()}.
	 */
	@Test
	public void testInit() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.SelectManipulator#execute()}.
	 */
	@Test
	public void testExecute() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#DataManipulator()}.
	 */
	@Test
	public void testDataManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)}.
	 */
	@Test
	public void testSetDatabaseManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setPriority(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)}.
	 */
	@Test
	public void testSetPriority() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setDistinctType(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)}.
	 */
	@Test
	public void testSetDistinctType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setInsertInto(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetInsertInto() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setSelectFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetSelectFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setDeleteFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetDeleteFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setUpdateTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetUpdateTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setDatasets(java.util.List)}.
	 */
	@Test
	public void testSetDatasets() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addDataset(com.digsarustudio.banana.database.query.Dataset)}.
	 */
	@Test
	public void testAddDataset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setOnDuplicateKeyUpdateAssignments(java.util.List)}.
	 */
	@Test
	public void testSetOnDuplicateKeyUpdateAssignments() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)}.
	 */
	@Test
	public void testAddOnDuplicateKeyUpdateAssignment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setSelectColumns(java.util.List)}.
	 */
	@Test
	public void testSetSelectColumns() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)}.
	 */
	@Test
	public void testAddSelectColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)}.
	 */
	@Test
	public void testSetJoinType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setJoinTableRefereces(java.util.List)}.
	 */
	@Test
	public void testSetJoinTableRefereces() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addJoinTableReference(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testAddJoinTableReference() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setJoinTableReference(java.util.List)}.
	 */
	@Test
	public void testSetJoinTableReference() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addJoinTableReferences(com.digsarustudio.banana.database.query.JoinTable)}.
	 */
	@Test
	public void testAddJoinTableReferences() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setJoinConditions(java.util.List)}.
	 */
	@Test
	public void testSetJoinConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)}.
	 */
	@Test
	public void testAddJoinCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setConditions(java.util.List)}.
	 */
	@Test
	public void testSetConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addCondition(com.digsarustudio.banana.database.query.Condition)}.
	 */
	@Test
	public void testAddCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setAggregations(java.util.List)}.
	 */
	@Test
	public void testSetAggregations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addAggregation(com.digsarustudio.banana.database.query.Aggregation)}.
	 */
	@Test
	public void testAddAggregation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setOrderings(java.util.List)}.
	 */
	@Test
	public void testSetOrderings() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addOrdering(com.digsarustudio.banana.database.query.Ordering)}.
	 */
	@Test
	public void testAddOrdering() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setOffset(java.lang.Integer)}.
	 */
	@Test
	public void testSetOffset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setMaxRowCount(java.lang.Integer)}.
	 */
	@Test
	public void testSetMaxRowCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setCompiler(com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler)}.
	 */
	@Test
	public void testSetCompilerClauseCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setCompiler(com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler)}.
	 */
	@Test
	public void testSetCompilerStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#getResult()}.
	 */
	@Test
	public void testGetResult() {
		fail("Not yet implemented");
	}

}
