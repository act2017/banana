/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.strategy;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link ManipulationStrategy}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ManipulationStrategyTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#ManipulationStrategy()}.
	 */
	@Test
	public void testManipulationStrategy() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#setDataAccessObjectFactory(com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory)}.
	 */
	@Test
	public void testSetDataAccessObjectFactory() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#setOffset(java.lang.Integer)}.
	 */
	@Test
	public void testSetOffset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#setMaxRowCount(java.lang.Integer)}.
	 */
	@Test
	public void testSetMaxRowCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getConnection()}.
	 */
	@Test
	public void testGetConnection() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getDatabaseManipulator()}.
	 */
	@Test
	public void testGetDatabaseManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#setDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)}.
	 */
	@Test
	public void testSetDatabaseManipulator() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#connect()}.
	 */
	@Test
	public void testConnect() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#isConnected()}.
	 */
	@Test
	public void testIsConnected() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#disconnect()}.
	 */
	@Test
	public void testDisconnect() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getOffset()}.
	 */
	@Test
	public void testGetOffset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getMaxRowCount()}.
	 */
	@Test
	public void testGetMaxRowCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getStatementCompiler()}.
	 */
	@Test
	public void testGetStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getClauseCompiler()}.
	 */
	@Test
	public void testGetClauseCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getFacotry()}.
	 */
	@Test
	public void testGetFacotry() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#setupColumns()}.
	 */
	@Test
	public void testSetupColumns() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getDatasets(java.lang.Object)}.
	 */
	@Test
	public void testGetDatasets() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getConditions(java.lang.Object)}.
	 */
	@Test
	public void testGetConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getSelectColumns()}.
	 */
	@Test
	public void testGetSelectColumns() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getOrderings()}.
	 */
	@Test
	public void testGetOderings() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getTable()}.
	 */
	@Test
	public void testGetTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getAggregations()}.
	 */
	@Test
	public void testGetAggregations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.manipulation.strategy.ManipulationStrategy#getJoinTables()}.
	 */
	@Test
	public void testGetJoinTables() {
		fail("Not yet implemented");
	}

}
