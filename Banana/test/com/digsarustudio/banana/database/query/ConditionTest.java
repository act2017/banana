/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To test {@link Condition}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("deprecation")
public class ConditionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getTarget()}.
	 */
	@Test
	public void testGetTarget() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("name")
															  .build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .build();
		
		assertEquals("name", condition.getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setTarget(java.lang.String)}.
	 */
	@Test
	public void testSetTarget() {
		Condition condition = Condition.builder().build();
		assertNull(condition.getTableColumn());
		
		
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("name").build();
		condition.setTableColumn(column);;
		assertEquals("name", condition.getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getValue()}.
	 */
	@Test
	public void testGetValue() {
		Condition condition = Condition.builder().setValue("Omama").build();
		
		assertEquals("Omama", condition.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setValue(java.lang.String)}.
	 */
	@Test
	public void testSetValue() {
		Condition condition = Condition.builder().build();
		assertNull(condition.getValue());
		
		condition.setValue("Omama");
		assertEquals("Omama", condition.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getComparator()}.
	 */
	@Test
	public void testGetComparator() {
		Condition condition = Condition.builder().setComparator(Comparators.LessEquals).build();
				
		assertEquals(Comparators.LessEquals, condition.getComparator());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setComparator(com.digsarustudio.banana.database.query.Comparators)}.
	 */
	@Test
	public void testSetComparator() {
		Condition condition = Condition.builder().build();
		assertEquals(Comparators.getDefault(), condition.getComparator());
		
		condition.setComparator(Comparators.LessEquals);
		assertEquals(Comparators.LessEquals, condition.getComparator());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getOperation()}.
	 */
	@Test
	public void testGetOperation() {
		Condition condition = Condition.builder().setOperation(Operations.Not).build();
		
		assertEquals(Operations.Not, condition.getOperation());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setOperation(com.digsarustudio.banana.database.query.Operations)}.
	 */
	@Test
	public void testSetOperation() {
		Condition condition = Condition.builder().build();
		
		assertNull(condition.getOperation());
		
		condition.setOperation(Operations.Not);
		assertEquals(Operations.Not, condition.getOperation());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setSubQuery(com.digsarustudio.banana.database.query.Statement)}.
	 */
	@Test
	public void testSetSubQuery() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getSubQuery()}.
	 */
	@Test
	public void testGetSubQuery() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getQueryFunction()}.
	 */
	@Test
	public void testGetQueryFunction() {
		Condition condition = Condition.builder().setQueryFunction(SQLStringFunctions.Now).build();
		
		assertEquals(SQLStringFunctions.Now, condition.getQueryFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setQueryFunction(com.digsarustudio.banana.database.query.SQLStringFunctions)}.
	 */
	@Test
	public void testSetQueryFunction() {
		Condition condition = Condition.builder().build();
		assertNull(condition.getQueryFunction());
		
		condition.setQueryFunction(SQLStringFunctions.Now);
		assertEquals(SQLStringFunctions.Now, condition.getQueryFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getAggregateFunction()}.
	 */
	@Test
	public void testGetAggregateFunction() {
		Condition condition = Condition.builder().setAggregateFunction(SQLAggregateFunctions.Summary).build();
		
		assertEquals(SQLAggregateFunctions.Summary, condition.getAggregateFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setAggregateFunction(com.digsarustudio.banana.database.query.SQLAggregateFunctions)}.
	 */
	@Test
	public void testSetAggregateFunction() {
		Condition condition = Condition.builder().build();
		assertNull(condition.getAggregateFunction());
		
		condition.setAggregateFunction(SQLAggregateFunctions.Summary);
		assertEquals(SQLAggregateFunctions.Summary, condition.getAggregateFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getQueryCondition()}.
	 */
	@Test
	public void testGetQueryCondition() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .setValue("123445")
												 .setComparator(Comparators.Equals)
												 .build();
		
		assertEquals("id", condition.getTableColumn());
		assertEquals("123445", condition.getValue());
		
		assertEquals("`id`='123445'", condition.getQueryCondition());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getQueryCondition()}.
	 */
	@Test
	public void testGetQueryConditionWithFunction() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		Condition condition = Condition.builder().setTableColumn(column)
												 .setValue("123445bc")
												 .setComparator(Comparators.Equals)
												 .setQueryFunction(SQLStringFunctions.UnHex)
												 .build();
		
		assertEquals("id", condition.getTableColumn().getColumn());
		assertEquals("123445bc", condition.getValue());
		
		assertEquals("`id`=UNHEX('123445bc')", condition.getQueryCondition());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getQueryCondition()}.
	 */
	@Test
	public void testGetQueryConditionWithFunctionOnly() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("createDate").build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.Equals)
												 .setQueryFunction(SQLStringFunctions.Now)
												 .build();
		
		assertEquals("createdDate", condition.getTableColumn().getColumn());
		assertEquals(SQLStringFunctions.Now, condition.getQueryFunction());
		
		assertEquals("`createdDate`=NOW()", condition.getQueryCondition());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getQueryCondition()}.
	 */
	@Test
	public void testGetQueryConditionWithEncryptionFunction(){
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.Equals)
												 .setValue("12334")
												 .setEncryptFunction(SQLEncryptionFunctions.MD5)
												 .build();
		
		assertEquals("id", condition.getTableColumn());
		assertEquals("12334", condition.getValue());
		assertEquals(Comparators.Equals, condition.getComparator());
		assertEquals(SQLEncryptionFunctions.MD5, condition.getEncryptionFunction());
		
		assertEquals("`id`=MD5('12334')", condition.getQueryCondition());
		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getQueryCondition()}.
	 */
	@Test
	public void testGetQueryConditionWithAggregationFunction() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("qty").build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.GreaterThan)
												 .setValue("12")
												 .setAggregateFunction(SQLAggregateFunctions.Summary)
												 .build();
		
		
		assertEquals("qty", condition.getTableColumn());
		assertEquals("12", condition.getValue());
		assertEquals(SQLAggregateFunctions.Summary, condition.getAggregateFunction());
		
		assertEquals("SUM(`qty`)>'12'", condition.getQueryCondition());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#isFunctionOperated()}.
	 */
	@Test
	public void testIsFunctionOperated() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.Equals)
												 .setValue("123abe")
												 .setQueryFunction(SQLStringFunctions.UnHex)
												 .build();
												 
		assertEquals(SQLStringFunctions.UnHex, condition.getQueryFunction());
		assertTrue(condition.isFunctionOperated());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#isFunctionOperated()}.
	 */
	@Test
	public void testNotIsFunctionOperated() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.Equals)
												 .setValue("123abe")
												 .build();
		
		assertNull(condition.getQueryFunction());
		assertFalse(condition.isFunctionOperated());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#isFunctionOperated()}.
	 */
	@Test
	public void testNotIsFunctionOperatedWhenAggregateFunctionSet() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.Equals)												 
												 .setValue("123abe")
												 .setAggregateFunction(SQLAggregateFunctions.Average)
												 .build();
		assertEquals(SQLAggregateFunctions.Average, condition.getAggregateFunction());
												 
		assertFalse(condition.isFunctionOperated());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#isFunctionOperated()}.
	 */
	@Test
	public void testNotIsFunctionOperatedWhenEncryptionFunctionSet() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		Condition condition = Condition.builder().setTableColumn(column)
												 .setComparator(Comparators.Equals)												 
												 .setValue("123abe")
												 .setEncryptFunction(SQLEncryptionFunctions.MD5)
												 .build();
		assertEquals(SQLEncryptionFunctions.MD5, condition.getEncryptionFunction());
												 
		assertFalse(condition.isFunctionOperated());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#hasAggregateFunction()}.
	 */
	@Test
	public void testHasAggregateFunction() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("qty").build();
		
		Condition condition = Condition.builder().setTableColumn(column)
												 .setAggregateFunction(SQLAggregateFunctions.Average)
												 .build();
		
		assertTrue(condition.hasAggregateFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#hasAggregateFunction()}.
	 */
	@Test
	public void testNotHasAggregateFunction() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("qty").build();
		Condition condition = Condition.builder().setTableColumn(column)
												 .build();
		
		assertFalse(condition.hasAggregateFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setSubqueryOperator(com.digsarustudio.banana.database.query.Operations)}.
	 */
	@Test
	public void testSetSubqueryOperator() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		Condition condition = Condition.builder().setTableColumn(column)												 
												 .build();
												 
		assertNull(condition.getSubqueryOperator());
		
		condition.setSubqueryOperator(Operations.Including);
		assertEquals(Operations.Including, condition.getSubqueryOperator());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getSubqueryOperator()}.
	 */
	@Test
	public void testGetSubqueryOperator() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		Condition condition = Condition.builder().setTableColumn(column)
												 .setSubQueryOperator(Operations.Including)
				 								 .build();
		
		assertEquals(Operations.Including, condition.getSubqueryOperator());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setIsNullValue()}.
	 */
	@Test
	public void testSetIsNullValue() {
		Condition condition = Condition.builder().build();
		
		assertNull(condition.isValueNullable());
		
		condition.setIsNullValue();
		assertTrue(condition.isValueNullable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setIsNotNullValue()}.
	 */
	@Test
	public void testSetIsNotNullValue() {
		Condition condition = Condition.builder().build();
		
		assertNull(condition.isValueNullable());
		
		condition.setIsNotNullValue();;
		assertFalse(condition.isValueNullable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#isNotNullValue()}.
	 */
	@Test
	public void testIsNotNullValue() {
		Condition condition = Condition.builder().setIsNotNullValue().build();
		
		assertFalse(condition.isValueNullable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#isNotNullValue()}.
	 */
	@Test
	public void testIsNullValue() {
		Condition condition = Condition.builder().setIsNullValue().build();
		
		assertTrue(condition.isValueNullable());		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#getEncryptionFunction()}.
	 */
	@Test
	public void testGetEncryptionFunction() {
		Condition condition = Condition.builder().setEncryptFunction(SQLEncryptionFunctions.MD5).build();
		
		assertEquals(SQLEncryptionFunctions.MD5, condition.getEncryptionFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Condition#setEncriptionFunction()}.
	 */
	@Test
	public void testSetEncryptionFunction() {
		Condition condition = Condition.builder().build();
		
		assertNull(condition.getEncryptionFunction());
		
		condition.setEncryptionFunction(SQLEncryptionFunctions.MD5);
		assertEquals(SQLEncryptionFunctions.MD5, condition.getEncryptionFunction());
	}

}
