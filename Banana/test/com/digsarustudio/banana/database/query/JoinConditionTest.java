/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To test {@link JoinCondition}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class JoinConditionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#getColumn1()}.
	 */
	@Test
	public void testGetRefereeColumn() {
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();		
		JoinCondition condition = JoinCondition.builder().setRefereeColumn(referee).build();
		
		assertEquals("inventory.id", condition.getRefereeColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#setColumn1(java.lang.String)}.
	 */
	@Test
	public void testSetRefereeColumn() {
		JoinCondition condition = JoinCondition.builder().build();
		assertNull(condition.getRefereeColumn());
		
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();
		condition.setRefereeColumn(referee);
		assertEquals("inventory.id", condition.getRefereeColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#getColumn2()}.
	 */
	@Test
	public void testGetColumn2() {
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		JoinCondition condition = JoinCondition.builder().setReferentialColumn(reference).build();
		
		assertEquals("category.id", condition.getReferentialColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#setColumn2(java.lang.String)}.
	 */
	@Test
	public void testSetColumn2() {
		JoinCondition condition = JoinCondition.builder().build();
		assertNull(condition.getReferentialColumn());
		
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		condition.setReferentialColumn(reference);;
		assertEquals("category.id", condition.getReferentialColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#getOperation()}.
	 */
	@Test
	public void testGetOperation() {
		JoinCondition condition = JoinCondition.builder().setOperation(Operations.And).build();
		
		assertEquals(Operations.And, condition.getOperation());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#setOperation(com.digsarustudio.banana.database.query.Operations)}.
	 */
	@Test
	public void testSetOperation() {
		JoinCondition condition = JoinCondition.builder().build();
		assertNull(condition.getOperation());
		
		condition.setOperation(Operations.Or);
		assertEquals(Operations.Or, condition.getOperation());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#getQueryJoinCondition()}.
	 */
	@Test
	public void testGetQueryJoinCondition() {
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		JoinCondition condition = JoinCondition.builder().setRefereeColumn(referee)
														 .setReferentialColumn(reference)
														 .build();
		
		assertEquals("inventory.id", condition.getRefereeColumn().getQuerySimpleTableColumn());
		assertEquals("category.id", condition.getReferentialColumn().getQuerySimpleTableColumn());
		
		assertEquals("inventory.id=category.id", condition.getQueryJoinCondition());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinCondition#getQueryJoinCondition()}.
	 */
	@Test
	public void testGetQueryJoinConditionWithOperation() {
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		JoinCondition condition = JoinCondition.builder().setRefereeColumn(referee)
														 .setReferentialColumn(reference)
														 .setOperation(Operations.And)
														 .build();
		
		assertEquals("inventory.id", condition.getRefereeColumn().getQuerySimpleTableColumn());
		assertEquals("category.id", condition.getReferentialColumn().getQuerySimpleTableColumn());
		assertEquals(Operations.And, condition.getOperation());
		
		assertEquals("AND inventory.id=category.id", condition.getQueryJoinCondition());
	}

}
