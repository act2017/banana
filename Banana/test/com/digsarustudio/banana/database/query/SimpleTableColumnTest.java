/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To test {@link SimpleTableColumn}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SimpleTableColumnTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SimpleTableColumn#getColumn()}.
	 */
	@Test
	public void testGetColumn() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		
		assertEquals("id", column.getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SimpleTableColumn#setColumn(java.lang.String)}.
	 */
	@Test
	public void testSetColumn() {
		SimpleTableColumn column = SimpleTableColumn.builder().build();
		assertNull(column.getColumn());
		
		column.setColumn("qty");
		assertEquals("qty", column.getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SimpleTableColumn#setTable(java.lang.String)}.
	 */
	@Test
	public void testSetTable() {
		SimpleTableColumn column = SimpleTableColumn.builder().build();
		assertNull(column.getTable());
		
		column.setTable("inventory");
		assertEquals("inventory", column.getTable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SimpleTableColumn#getTable()}.
	 */
	@Test
	public void testGetTable() {
		SimpleTableColumn column = SimpleTableColumn.builder().setTable("inventory").build();
		
		assertEquals("inventory", column.getTable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SimpleTableColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectColumnWithoutTable() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("quantity").build();
		
		assertEquals("quantity", column.getColumn());
		
		assertEquals("`quantity`", column.getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SimpleTableColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectColumnWithTable() {
		SimpleTableColumn column = SimpleTableColumn.builder().setTable("inventory")
													.setColumn("quantity")
													.build();
		
		
		assertEquals("inventory", column.getTable());
		assertEquals("quantity", column.getColumn());
		
		assertEquals("inventory.quantity", column.getQuerySimpleTableColumn());
	}

}
