/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To test {@link Dataset}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
@SuppressWarnings("deprecation")
public class DatasetTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getName()}.
	 */
	@Test
	public void testGetName() {
		SimpleTableColumn column = SimpleTableColumn.builder().setColumn("id").build();
		Dataset set = Dataset.builder().setTableColumn(column).build();
		
		assertEquals("id", set.getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		Dataset set = Dataset.builder().build();
		assertNull(set.getTableColumn());
		
		set.setTableColumn(SimpleTableColumn.builder().setColumn("id").build());

		assertEquals("id", set.getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getValue()}.
	 */
	@Test
	public void testGetValue() {
		Dataset set = Dataset.builder().setValue("1234557").build();
		
		assertEquals("1234557", set.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#setValue(java.lang.String)}.
	 */
	@Test
	public void testSetValue() {
		Dataset set = Dataset.builder().build();
		assertNull(set.getValue());
		
		set.setValue("1234477");
		assertEquals("1234477", set.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryFunction()}.
	 */
	@Test
	public void testGetQueryFunction() {
		Dataset set = Dataset.builder().setQueryFunction(SQLStringFunctions.Hex).build();
		
		assertEquals(SQLStringFunctions.Hex, set.getQueryFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#setQueryFunction(com.digsarustudio.banana.database.query.SQLStringFunctions)}.
	 */
	@Test
	public void testSetQueryFunction() {
		Dataset set = Dataset.builder().build();
		assertNull(set.getQueryFunction());
		
		set.setQueryFunction(SQLStringFunctions.UnHex);
		assertEquals(SQLStringFunctions.UnHex, set.getQueryFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}.
	 */
	@Test
	public void testGetQueryValue() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setValue("123447ab").build();
		
		assertEquals("123447ab", set.getValue());
		
		assertEquals("'123447ab'", set.getQueryValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}.
	 */
	@Test
	public void testGetQueryValueWithFunction() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setValue("123447ab").setQueryFunction(SQLStringFunctions.Hex).build();
		
		assertEquals("123447ab", set.getValue());
		
		assertEquals("HEX('123447ab')", set.getQueryValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}.
	 */
	@Test
	public void testGetQueryValueWithFunctionOnly() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setQueryFunction(SQLStringFunctions.Now).build();
		
		assertNull(set.getValue());
		
		assertEquals("NOW()", set.getQueryValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}.
	 */
	@Test
	public void testGetQueryValueWithEncryptionFunction() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setValue("132")
									   .setEncryptionFunction(SQLEncryptionFunctions.MD5)
									   .build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("132", set.getValue());
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
		
		assertEquals("MD5('132')", set.getQueryValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}.
	 */
	@Test
	public void testGetQueryDataSet() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setValue("123447ba").build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("123447ba", set.getValue());
		
		assertEquals("`id`='123447ba'", set.getQueryDataSet());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}.
	 */
	@Test
	public void testGetQueryDataSetWithFunction() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setValue("123447ba").setQueryFunction(SQLStringFunctions.UnHex).build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("123447ba", set.getValue());
		assertEquals(SQLStringFunctions.UnHex, set.getQueryFunction());
		
		assertEquals("`id`=UNHEX('123447ba')", set.getQueryDataSet());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}.
	 */
	@Test
	public void testGetQueryDataSetWithFunctionOnly() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setQueryFunction(SQLStringFunctions.Now).build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertNull(set.getValue());
		assertEquals(SQLStringFunctions.Now, set.getQueryFunction());
		
		assertEquals("`id`=NOW()", set.getQueryDataSet());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}.
	 */
	@Test
	public void testGetQueryDataSetWithEncryptionFunction() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setValue("132").setEncryptionFunction(SQLEncryptionFunctions.MD5).build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("132", set.getValue());
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
		
		assertEquals("`id`=MD5('132')", set.getQueryDataSet());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#isFunctionOperated()}.
	 */
	@Test
	public void testIsFunctionOperated() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setQueryFunction(SQLStringFunctions.Now).build();
		
		assertTrue(set.isFunctionOperated());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#isFunctionOperated()}.
	 */
	@Test
	public void testIsNotFunctionOperated() {
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).build();
		
		assertFalse(set.isFunctionOperated());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getEncryptionFunction()}
	 */
	@Test
	public void testGetEncryptionFunction(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).setEncryptionFunction(SQLEncryptionFunctions.MD5).build();
		
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#setEncryptionFunction()}
	 */
	@Test
	public void testSetEncryptionFunction(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).build();
		assertNull(set.getEncryptionFunction());
		
		set.setEncryptionFunction(SQLEncryptionFunctions.MD5);
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}
	 */
	@Test
	public void testGetQueryValueWithQueryFunction(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder()
																		.setColumn("id")
																		.build())
										.setQueryFunction(SQLStringFunctions.UnHex)
										.setValue("45E324A")
										.build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("45E324A", set.getValue());
		assertEquals(SQLStringFunctions.UnHex, set.getQueryFunction());
		assertEquals("UNHEX('45E324A')", set.getQueryValue());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}
	 */
	@Test
	public void testGetQueryValueWithEncryption(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder()
																		.setColumn("password")
																		.build())
										.setEncryptionFunction(SQLEncryptionFunctions.MD5)
										.setValue("132@tog.auto.com.au")
										.build();
		
		assertEquals("password", set.getTableColumn().getColumn());
		assertEquals("132@tog.auto.com.au", set.getValue());
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
		assertEquals("MD5('132@tog.auto.com.au')", set.getQueryValue());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryValue()}
	 */
	@Test
	public void testGetQueryValueWithQueryFunctionAndEncryption(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder()
																		.setColumn("id")
																		.build())
										.setQueryFunction(SQLStringFunctions.UnHex)
										.setEncryptionFunction(SQLEncryptionFunctions.MD5)
										.setValue("tog@mrauto.com.au")
										.build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("tog@mrauto.com.au", set.getValue());
		assertEquals(SQLStringFunctions.UnHex, set.getQueryFunction());
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
		assertEquals("UNHEX(MD5('tog@mrauto.com.au'))", set.getQueryValue());
		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}
	 */
	@Test
	public void testGetQueryDatasetWithQueryFunction(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder()
																		.setColumn("id")
																		.build())
										.setQueryFunction(SQLStringFunctions.UnHex)
										.setValue("45E324A")
										.build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("45E324A", set.getValue());
		assertEquals(SQLStringFunctions.UnHex, set.getQueryFunction());
		assertEquals("`id`=UNHEX('45E324A')", set.getQueryDataSet());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}
	 */
	@Test
	public void testGetQueryDatasetWithEncryption(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder()
																		.setColumn("password")
																		.build())
										.setEncryptionFunction(SQLEncryptionFunctions.MD5)
										.setValue("132@tog.auto.com.au")
										.build();
		
		assertEquals("password", set.getTableColumn().getColumn());
		assertEquals("132@tog.auto.com.au", set.getValue());
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
		assertEquals("`password`=MD5('132@tog.auto.com.au')", set.getQueryDataSet());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Dataset#getQueryDataSet()}
	 */
	@Test
	public void testGetQueryDatasetWithQueryFunctionAndEncryption(){
		Dataset set = Dataset.builder().setTableColumn(SimpleTableColumn.builder()
																		.setColumn("id")
																		.build())
										.setQueryFunction(SQLStringFunctions.UnHex)
										.setEncryptionFunction(SQLEncryptionFunctions.MD5)
										.setValue("tog@mrauto.com.au")
										.build();
		
		assertEquals("id", set.getTableColumn().getColumn());
		assertEquals("tog@mrauto.com.au", set.getValue());
		assertEquals(SQLStringFunctions.UnHex, set.getQueryFunction());
		assertEquals(SQLEncryptionFunctions.MD5, set.getEncryptionFunction());
		assertEquals("`id`=UNHEX(MD5('tog@mrauto.com.au'))", set.getQueryDataSet());
		
	}
}
