/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link DuplicateKeyUpdateAssignment}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DuplicateKeyUpdateAssignmentTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#getColumn()}.
	 */
	@Test
	public void testGetColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#getRightOperand()}.
	 */
	@Test
	public void testGetRightOperand() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#setColumn(com.digsarustudio.banana.database.table.SimpleTableColumn)}.
	 */
	@Test
	public void testSetColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#setValue(java.lang.String)}.
	 */
	@Test
	public void testSetValue() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#setValueFrom(com.digsarustudio.banana.database.table.SimpleTableColumn)}.
	 */
	@Test
	public void testSetValueFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#setValueSummaryFrom(com.digsarustudio.banana.database.table.SimpleTableColumn[])}.
	 */
	@Test
	public void testSetValueSummaryFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment#builder()}.
	 */
	@Test
	public void testBuilder() {
		fail("Not yet implemented");
	}

}
