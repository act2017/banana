/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To test {@link SelectColumn}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SelectColumnTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getColumn()}.
	 */
	@Test
	public void testGetColumn() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id").build()).build();
		
		assertEquals("id", column.getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#setColumn(java.lang.String)}.
	 */
	@Test
	public void testSetColumn() {
		SelectColumn column = SelectColumn.builder().build();
		assertNull(column.getTableColumn());
		
		column.setTableColumn(SimpleTableColumn.builder().setColumn("qty").build());
		assertEquals("qty", column.getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getAlias()}.
	 */
	@Test
	public void testGetAlias() {
		SelectColumn column = SelectColumn.builder().setAlias("identifier").build();
		
		assertEquals("identifier", column.getAlias());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#setTable(java.lang.String)}.
	 */
	@Test
	public void testSetTable() {
		SelectColumn column = SelectColumn.builder().build();
		assertNull(column.getTableColumn());
		
		column.setTableColumn(SimpleTableColumn.builder().setTable("inventory").build());
		assertEquals("inventory", column.getTableColumn().getTable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getTable()}.
	 */
	@Test
	public void testGetTable() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setTable("inventory").build()).build();
		
		assertEquals("inventory", column.getTableColumn().getTable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#setAlias(java.lang.String)}.
	 */
	@Test
	public void testSetAlias() {
		SelectColumn column = SelectColumn.builder().build();
		assertNull(column.getAlias());
		
		column.setAlias("quantity");
		assertEquals("quantity", column.getAlias());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#setQueryFunction(SQLStringFunctions)}.
	 */
	@Test
	public void testSetQueryFunction(){
		SelectColumn column = SelectColumn.builder().build();
		assertNull(column.getQueryFunction());
		
		column.setQueryFunction(SQLStringFunctions.UnHex);
		assertEquals(SQLStringFunctions.UnHex, column.getQueryFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQueryFunction()}.
	 */
	@Test
	public void testGetQueryFunction() {
		SelectColumn column = SelectColumn.builder().setQueryFunction(SQLStringFunctions.Hex).build();
		
		assertEquals(SQLStringFunctions.Hex, column.getQueryFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#setAggregateFunction(SQLAggregateFunctions)}.
	 */
	@Test
	public void testSetAggregateFunction(){
		SelectColumn column = SelectColumn.builder().build();
		assertNull(column.getAggregateFunction());
		
		column.setAggregateFunction(SQLAggregateFunctions.Summary);;
		assertEquals(SQLAggregateFunctions.Summary, column.getAggregateFunction());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getAggregateFunction()}.
	 */
	@Test
	public void testGetAggregateFunction() {
		SelectColumn column = SelectColumn.builder().setAggregateFunction(SQLAggregateFunctions.Average).build();
		
		assertEquals(SQLAggregateFunctions.Average, column.getAggregateFunction());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectColumnWithQueryFunction() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id")
																							   .build())
													.setAlias("productId")
													.setQueryFunction(SQLStringFunctions.Hex)
													.build();
		
		assertEquals("id", column.getTableColumn().getColumn());
		assertEquals("productId", column.getAlias());
		assertEquals(SQLStringFunctions.Hex, column.getQueryFunction());
		
		assertEquals("HEX(`id`) AS 'productId'", column.getQuerySelectColumn());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectTableColumnWithQueryFunction() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setTable("products")
																							   .setColumn("id")
																							   .build())
													.setAlias("productId")
													.setQueryFunction(SQLStringFunctions.Hex)
													.build();
		
		assertEquals("products", column.getTableColumn().getTable());
		assertEquals("id", column.getTableColumn().getColumn());
		assertEquals("productId", column.getAlias());
		assertEquals(SQLStringFunctions.Hex, column.getQueryFunction());
		
		assertEquals("HEX(products.id) AS 'productId'", column.getQuerySelectColumn());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectColumnWithAggregateFunction() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setColumn("quantity")
																							   .build())
													.setAlias("qty")
													.setAggregateFunction(SQLAggregateFunctions.Summary)
													.build();
		
		assertEquals("quantity", column.getTableColumn().getColumn());
		assertEquals("qty", column.getAlias());
		assertEquals(SQLAggregateFunctions.Summary, column.getAggregateFunction());
		
		assertEquals("SUM(`quantity`) AS 'qty'", column.getQuerySelectColumn());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectTableColumnWithAggregateFunction() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setTable("products")
																							   .setColumn("quantity")
																							   .build())
													.setAlias("total")
													.setAggregateFunction(SQLAggregateFunctions.Summary)
													.build();
		
		assertEquals("products", column.getTableColumn().getTable());
		assertEquals("quantity", column.getTableColumn().getColumn());
		assertEquals("total", column.getAlias());
		assertEquals(SQLAggregateFunctions.Summary, column.getAggregateFunction());
		
		assertEquals("SUM(products.quantity) AS 'total'", column.getQuerySelectColumn());
	}
	
	//Get query string with query function and aggregate function
	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectColumnWithoutTable() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setColumn("quantity").build()).setAlias("qty").build();
		
		assertEquals("quantity", column.getTableColumn().getColumn());
		assertEquals("qty", column.getAlias());
		
		assertEquals("`quantity` AS 'qty'", column.getQuerySelectColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 */
	@Test
	public void testGetQuerySelectColumnWithTable() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setTable("inventory").setColumn("quantity").build())
													.setAlias("qty")
													.build();
		
		
		assertEquals("inventory", column.getTableColumn().getTable());
		assertEquals("quantity", column.getTableColumn().getColumn());
		assertEquals("qty", column.getAlias());
		
		assertEquals("inventory.quantity AS 'qty'", column.getQuerySelectColumn());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 * 
	 * The {@link SQLStringFunctions} won't appear with {@link SQLAggregateFunctions} at the same time, and 
	 * the priority of {@link SQLStringFunctions} is higher than {@link SQLAggregateFunctions}.<br>
	 */
	@Test
	public void testGetQuerySelectColumnWithQueryFunctionAndAggregateFunction() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setColumn("id")
																							   .build())
													.setAlias("productId")
													.setQueryFunction(SQLStringFunctions.Hex)
													.setAggregateFunction(SQLAggregateFunctions.Average)
													.build();
		
		assertEquals("id", column.getTableColumn().getColumn());
		assertEquals("productId", column.getAlias());
		assertEquals(SQLStringFunctions.Hex, column.getQueryFunction());
		
		assertEquals("HEX(`id`) AS 'productId'", column.getQuerySelectColumn());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SelectColumn#getQuerySelectColumn()}.
	 * 
	 * The {@link SQLStringFunctions} won't appear with {@link SQLAggregateFunctions} at the same time, and 
	 * the priority of {@link SQLStringFunctions} is higher than {@link SQLAggregateFunctions}.<br>
	 */
	@Test
	public void testGetQuerySelectTableColumnWithQueryFunctionAndAggregateFunction() {
		SelectColumn column = SelectColumn.builder().setTableColumn(SimpleTableColumn.builder().setTable("products")
																							   .setColumn("id")
																							   .build())
													.setAlias("productId")
													.setQueryFunction(SQLStringFunctions.Hex)
													.setAggregateFunction(SQLAggregateFunctions.Summary)
													.build();
		
		assertEquals("products", column.getTableColumn().getTable());
		assertEquals("id", column.getTableColumn().getColumn());
		assertEquals("productId", column.getAlias());
		assertEquals(SQLStringFunctions.Hex, column.getQueryFunction());
		
		assertEquals("HEX(products.id) AS 'productId'", column.getQuerySelectColumn());
	}

}
