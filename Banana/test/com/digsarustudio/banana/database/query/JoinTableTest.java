/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.database.query.Table;

/**
 * To test {@link JoinTable}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class JoinTableTest {
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#getTable()}.
	 */
	@Test
	public void testGetTable() {		
		JoinTable table = JoinTable.builder().setTable(SimpleQueryTable.builder().setName("table2Join")
																				 .build())
											 .build();
		
		assertEquals("table2Join", table.getTable().getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#setTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetTable() {
		
		JoinTable table = JoinTable.builder().build();
		assertNull(table.getTable());
		
		table.setTable(SimpleQueryTable.builder().setName("table2Join").build());		
		assertEquals("table2Join", table.getTable().getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#getType()}.
	 */
	@Test
	public void testGetType() {		
		Table joinTable = SimpleQueryTable.builder().setName("table2Join").build();
		JoinTable table = JoinTable.builder().setType(TableJoinType.LeftJoin).setTable(joinTable).build();
		
		assertEquals(TableJoinType.LeftJoin, table.getType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#setType(com.digsarustudio.banana.database.query.TableJoinType)}.
	 */
	@Test
	public void testSetType() {
		JoinTable table = JoinTable.builder().build();
		assertNull(table.getType());
		
		table.setType(TableJoinType.RightJoin);
		assertEquals(TableJoinType.RightJoin, table.getType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#getConditions()}.
	 */
	@Test
	public void testGetConditions() {
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		JoinTable table = JoinTable.builder().addCondition(JoinCondition.builder().setRefereeColumn(referee)
																				  .setReferentialColumn(reference)
																				  .build())
											 .build();
		
		assertEquals("inventory.id", table.getConditions().get(0).getRefereeColumn().getQuerySimpleTableColumn());
		assertEquals("category.id", table.getConditions().get(0).getReferentialColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#setConditions(java.util.List)}.
	 */
	@Test
	public void testSetConditions() {
		JoinTable table = JoinTable.builder().build();
		assertNull(table.getConditions());
		
		List<JoinCondition> conditions = new ArrayList<>();
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		conditions.add(JoinCondition.builder().setRefereeColumn(referee)
											  .setReferentialColumn(reference)
											  .build());
		
		table.setConditions(conditions);
		
		assertEquals("inventory.id", table.getConditions().get(0).getRefereeColumn().getQuerySimpleTableColumn());
		assertEquals("category.id", table.getConditions().get(0).getReferentialColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#addCondition(com.digsarustudio.banana.database.query.Condition)}.
	 */
	@Test
	public void testAddCondition() {
		
		JoinTable table = JoinTable.builder().build();
		assertNull(table.getConditions());
		
		SimpleTableColumn referee = SimpleTableColumn.builder().setTable("inventory").setColumn("id").build();
		SimpleTableColumn reference = SimpleTableColumn.builder().setTable("category").setColumn("id").build();
		table.addCondition(JoinCondition.builder().setRefereeColumn(referee)
												  .setReferentialColumn(reference)
												  .build());
				
		assertEquals("inventory.id", table.getConditions().get(0).getRefereeColumn().getQuerySimpleTableColumn());
		assertEquals("category.id", table.getConditions().get(0).getReferentialColumn().getQuerySimpleTableColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#setAlias(java.lang.String)}.
	 */
	@Test
	public void testSetAlias(){
		fail("not implemented yet");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.JoinTable#setAlias(java.lang.String)}.
	 */
	@Test
	public void testGetAlias(){
		fail("not implemented yet");
	}
}
