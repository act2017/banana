/**
 * 
 */
package com.digsarustudio.banana.database.query.clause;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.SortingType;
import com.digsarustudio.banana.database.query.claue.AggregationClause;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To test {@link AggregationClause}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class AggregationClauseTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.AggregationClause#setAggregations(java.util.List)}.
	 */
	@Test
	public void testSetAggregations() {
		SimpleTableColumn column = SimpleTableColumn.builder().setTable("items")
															  .setColumn("id")
															  .build();
		
		Aggregation aggregation = Aggregation.builder().setColumn(column)
													   .setSortingType(SortingType.Ascending)
													   .build();
		
		column = SimpleTableColumn.builder().setTable("items")
											.setColumn("title")
											.build();
		Aggregation aggregation2 = Aggregation.builder().setColumn(column)
														.setSortingType(SortingType.Descending)
														.build();
		
		List<Aggregation> aggregations = new ArrayList<>();
		aggregations.add(aggregation);
		aggregations.add(aggregation2);
		
		AggregationClause clause = new AggregationClause();
		clause.setAggregations(aggregations);
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("id", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Ascending, clause.getAggregations().get(0).getSortingType());
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("title", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Descending, clause.getAggregations().get(0).getSortingType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.AggregationClause#addAggregation(com.digsarustudio.banana.database.query.Aggregation)}.
	 */
	@Test
	public void testAddAggregation() {
		SimpleTableColumn column = SimpleTableColumn.builder().setTable("items")
															  .setColumn("id")
															  .build();
		
		Aggregation aggregation = Aggregation.builder().setColumn(column)
													   .setSortingType(SortingType.Ascending)
													   .build();
		
		column = SimpleTableColumn.builder().setTable("items")
											.setColumn("title")
											.build();
		
		Aggregation aggregation2 = Aggregation.builder().setColumn(column)
					.setSortingType(SortingType.Descending)
					.build();
		
		AggregationClause clause = new AggregationClause();
		clause.addAggregation(aggregation);
		clause.addAggregation(aggregation2);
		
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("id", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Ascending, clause.getAggregations().get(0).getSortingType());
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("title", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Descending, clause.getAggregations().get(0).getSortingType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.AggregationClause#AggregationClause()}.
	 */
	@Test
	public void testAggregationClause() {
		new AggregationClause();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.AggregationClause#getAggregations()}.
	 */
	@Test
	public void testGetAggregations() {
		SimpleTableColumn column = SimpleTableColumn.builder().setTable("items")
															  .setColumn("id")
															  .build();
		
		Aggregation aggregation = Aggregation.builder().setColumn(column)
													   .setSortingType(SortingType.Ascending)
													   .build();
		
		column = SimpleTableColumn.builder().setTable("items")
											.setColumn("title")
											.build();
		
		Aggregation aggregation2 = Aggregation.builder().setColumn(column)
														.setSortingType(SortingType.Descending)
														.build();
		
		AggregationClause clause = new AggregationClause();
		clause.addAggregation(aggregation);
		clause.addAggregation(aggregation2);
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("id", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Ascending, clause.getAggregations().get(0).getSortingType());
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("title", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Descending, clause.getAggregations().get(0).getSortingType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.AggregationClause#build()}.
	 */
	@Test
	public void testBuild() {
		SimpleTableColumn column = SimpleTableColumn.builder().setTable("items")
															  .setColumn("id")
															  .build();
		
		Aggregation aggregation = Aggregation.builder().setColumn(column)
													   .setSortingType(SortingType.Ascending)
													   .build();
		
		column = SimpleTableColumn.builder().setColumn("title")
											.build();
		
		Aggregation aggregation2 = Aggregation.builder().setColumn(column)
														.setSortingType(SortingType.Descending)
														.build();
		
		AggregationClause clause = new AggregationClause();
		clause.addAggregation(aggregation);
		clause.addAggregation(aggregation2);
		
		assertEquals("items", clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("id", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Ascending, clause.getAggregations().get(0).getSortingType());
		
		assertNull(clause.getAggregations().get(0).getColumn().getTable());
		assertEquals("title", clause.getAggregations().get(0).getColumn().getColumn());
		assertEquals(SortingType.Descending, clause.getAggregations().get(0).getSortingType());
		
		clause.build();
		assertEquals("items.id ASC, `title` DESC" , clause.getResult());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#SyntaxClause()}.
	 */
	@Test
	public void testSyntaxClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#getClause()}.
	 */
	@Test
	public void testGetClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setClause(java.lang.String)}.
	 */
	@Test
	public void testSetClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)}.
	 */
	@Test
	public void testSetModifier() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setInsertInto(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetInsertInto() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setSelectFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetSelectFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setDeleteFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetDeleteFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setDatasets(java.util.List)}.
	 */
	@Test
	public void testSetDatasets() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addDataset(com.digsarustudio.banana.database.query.Dataset)}.
	 */
	@Test
	public void testAddDataset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setOnDuplicateKeyUpdateAssignments(java.util.List)}.
	 */
	@Test
	public void testSetOnDuplicateKeyUpdateAssignments() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)}.
	 */
	@Test
	public void testAddOnDuplicateKeyUpdateAssignment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setSelectColumns(java.util.List)}.
	 */
	@Test
	public void testSetSelectColumns() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)}.
	 */
	@Test
	public void testAddSelectColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setConditions(java.util.List)}.
	 */
	@Test
	public void testSetConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addCondition(com.digsarustudio.banana.database.query.Condition)}.
	 */
	@Test
	public void testAddCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setOrderings(java.util.List)}.
	 */
	@Test
	public void testSetOrderings() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addOrdering(com.digsarustudio.banana.database.query.Ordering)}.
	 */
	@Test
	public void testAddOrdering() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setOffset(java.lang.Integer)}.
	 */
	@Test
	public void testSetOffset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setMaxRowCount(java.lang.Integer)}.
	 */
	@Test
	public void testSetMaxRowCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)}.
	 */
	@Test
	public void testSetJoinType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinTableRefereces(java.util.List)}.
	 */
	@Test
	public void testSetJoinTableRefereces() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addJoinTableReference(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testAddJoinTableReference() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinConditions(java.util.List)}.
	 */
	@Test
	public void testSetJoinConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)}.
	 */
	@Test
	public void testAddJoinCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#setCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)}.
	 */
	@Test
	public void testSetCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.SyntaxClause#getResult()}.
	 */
	@Test
	public void testGetResult() {
		fail("Not yet implemented");
	}

}
