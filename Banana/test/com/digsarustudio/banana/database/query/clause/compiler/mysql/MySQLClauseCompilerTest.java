/**
 * 
 */
package com.digsarustudio.banana.database.query.clause.compiler.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link MySQLClauseCompilerTest}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLClauseCompilerTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#MySQLClauseCompiler()}.
	 */
	@Test
	public void testMySQLClauseCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.ModifierClause)}.
	 */
	@Test
	public void testCompileModifierClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.TableReferenceClause)}.
	 */
	@Test
	public void testCompileTableReferenceClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.VirtualTableReferenceClause)}.
	 */
	@Test
	public void testCompileVirtualTableReferenceClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.InsertColumnClause)}.
	 */
	@Test
	public void testCompileInsertColumnClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.InsertValueClause)}.
	 */
	@Test
	public void testCompileInsertValueClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.DuplicateKeyUpdateClause)}.
	 */
	@Test
	public void testCompileDuplicateKeyUpdateClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.SelectColumnClause)}.
	 */
	@Test
	public void testCompileSelectColumnClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.JoinTableReferenceClause)}.
	 */
	@Test
	public void testCompileJoinTableReferenceClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.JoinConditionClause)}.
	 */
	@Test
	public void testCompileJoinConditionClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.ConditionClause)}.
	 */
	@Test
	public void testCompileConditionClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.AggregationClause)}.
	 */
	@Test
	public void testCompileAggregationClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.UpdateSetClause)}.
	 */
	@Test
	public void testCompileUpdateSetClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.LimitClause)}.
	 */
	@Test
	public void testCompileLimitClause() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.OrderingClause)}.
	 */
	@Test
	public void testCompileOrderingClause() {
		fail("Not yet implemented");
	}

}
