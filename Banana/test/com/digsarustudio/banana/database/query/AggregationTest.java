/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link Aggregation}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class AggregationTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Aggregation#getColumn()}.
	 */
	@Test
	public void testGetColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Aggregation#setColumn(com.digsarustudio.banana.database.table.SimpleTableColumn)}.
	 */
	@Test
	public void testSetColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Aggregation#getSortingType()}.
	 */
	@Test
	public void testGetSortingType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Aggregation#setSortingType(com.digsarustudio.banana.database.query.SortingType)}.
	 */
	@Test
	public void testSetSortingType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.Aggregation#builder()}.
	 */
	@Test
	public void testBuilder() {
		fail("Not yet implemented");
	}

}
