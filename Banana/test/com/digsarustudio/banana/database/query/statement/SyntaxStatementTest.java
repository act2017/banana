/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link SyntaxStatement}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SyntaxStatementTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#SyntaxStatement()}.
	 */
	@Test
	public void testSyntaxStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)}.
	 */
	@Test
	public void testSetModifier() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setInsertInto(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetInsertInto() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetSelectFromTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectFrom(com.digsarustudio.banana.database.table.VirtualTable)}.
	 */
	@Test
	public void testSetSelectFromVirtualTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setDeleteFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetDeleteFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setUpdateTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetUpdateTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setDatasets(java.util.List)}.
	 */
	@Test
	public void testSetDatasets() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addDataset(com.digsarustudio.banana.database.query.Dataset)}.
	 */
	@Test
	public void testAddDataset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOnDuplicateKeyUpdateAssignments(java.util.List)}.
	 */
	@Test
	public void testSetOnDuplicateKeyUpdateAssignments() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)}.
	 */
	@Test
	public void testAddOnDuplicateKeyUpdateAssignment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectColumns(java.util.List)}.
	 */
	@Test
	public void testSetSelectColumns() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)}.
	 */
	@Test
	public void testAddSelectColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTable(java.util.List)}.
	 */
	@Test
	public void testSetJoinTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)}.
	 */
	@Test
	public void testAddJoinTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setConditions(java.util.List)}.
	 */
	@Test
	public void testSetConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addCondition(com.digsarustudio.banana.database.query.Condition)}.
	 */
	@Test
	public void testAddCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setAggregations(java.util.List)}.
	 */
	@Test
	public void testSetAggregations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addAggregation(com.digsarustudio.banana.database.query.Aggregation)}.
	 */
	@Test
	public void testAddAggregation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOrderings(java.util.List)}.
	 */
	@Test
	public void testSetOrderings() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#addOrdering(com.digsarustudio.banana.database.query.Ordering)}.
	 */
	@Test
	public void testAddOrdering() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOffset(java.lang.Integer)}.
	 */
	@Test
	public void testSetOffset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setMaxRowCount(java.lang.Integer)}.
	 */
	@Test
	public void testSetMaxRowCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setCompiler(com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler)}.
	 */
	@Test
	public void testSetCompilerStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setCompiler(com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler)}.
	 */
	@Test
	public void testSetCompilerClauseCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler)}.
	 */
	@Test
	public void testUpdateCompilerStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler)}.
	 */
	@Test
	public void testUpdateCompilerClauseCompiler() {
		fail("Not yet implemented");
	}

}
