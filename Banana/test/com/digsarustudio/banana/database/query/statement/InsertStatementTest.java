/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link InsertStatement}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class InsertStatementTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)}.
	 */
	@Test
	public void testSetModifier() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setInsertInto(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetInsertInto() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setSelectFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetSelectFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setDatasets(java.util.List)}.
	 */
	@Test
	public void testSetDatasets() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addDataset(com.digsarustudio.banana.database.query.Dataset)}.
	 */
	@Test
	public void testAddDataset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setOnDuplicateKeyUpdateAssignments(java.util.List)}.
	 */
	@Test
	public void testSetOnDuplicateKeyUpdateAssignments() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)}.
	 */
	@Test
	public void testAddOnDuplicateKeyUpdateAssignment() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setSelectColumns(java.util.List)}.
	 */
	@Test
	public void testSetSelectColumns() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)}.
	 */
	@Test
	public void testAddSelectColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)}.
	 */
	@Test
	public void testSetJoinType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setJoinTableRefereces(java.util.List)}.
	 */
	@Test
	public void testSetJoinTableRefereces() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addJoinTableReference(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testAddJoinTableReference() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setJoinConditions(java.util.List)}.
	 */
	@Test
	public void testSetJoinConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)}.
	 */
	@Test
	public void testAddJoinCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setConditions(java.util.List)}.
	 */
	@Test
	public void testSetConditions() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addCondition(com.digsarustudio.banana.database.query.Condition)}.
	 */
	@Test
	public void testAddCondition() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setAggregations(java.util.List)}.
	 */
	@Test
	public void testSetAggregations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addAggregation(com.digsarustudio.banana.database.query.Aggregation)}.
	 */
	@Test
	public void testAddAggregation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setOrderings(java.util.List)}.
	 */
	@Test
	public void testSetOrderings() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#addOrdering(com.digsarustudio.banana.database.query.Ordering)}.
	 */
	@Test
	public void testAddOrdering() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setOffset(java.lang.Integer)}.
	 */
	@Test
	public void testSetOffset() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#setMaxRowCount(java.lang.Integer)}.
	 */
	@Test
	public void testSetMaxRowCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#updateCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)}.
	 */
	@Test
	public void testUpdateCompilerStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#updateCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)}.
	 */
	@Test
	public void testUpdateCompilerClauseCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#InsertStatement()}.
	 */
	@Test
	public void testInsertStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#getModifier()}.
	 */
	@Test
	public void testGetModifier() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#getTableReference()}.
	 */
	@Test
	public void testGetTableReference() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#getInsertColumn()}.
	 */
	@Test
	public void testGetInsertColumn() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#getInsertValue()}.
	 */
	@Test
	public void testGetInsertValue() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#getDuplicateKeyUpdate()}.
	 */
	@Test
	public void testGetDuplicateKeyUpdate() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#getSelectStatement()}.
	 */
	@Test
	public void testGetSelectStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.InsertStatement#construct()}.
	 */
	@Test
	public void testConstruct() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#SyntaxStatement()}.
	 */
	@Test
	public void testSyntaxStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setDeleteFrom(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetDeleteFrom() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setUpdateTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetUpdateTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)}.
	 */
	@Test
	public void testSetCompilerStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.SyntaxStatement#setCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)}.
	 */
	@Test
	public void testSetCompilerClauseCompiler() {
		fail("Not yet implemented");
	}

}
