/**
 * 
 */
package com.digsarustudio.banana.database.query.statement.compiler.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link MySQLStatementCompiler}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLStatementCompilerTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler#MySQLStatementCompiler()}.
	 */
	@Test
	public void testMySQLStatementCompiler() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler#compile(com.digsarustudio.banana.database.query.statement.InsertStatement)}.
	 */
	@Test
	public void testCompileInsertStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler#compile(com.digsarustudio.banana.database.query.statement.SelectStatement)}.
	 */
	@Test
	public void testCompileSelectStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler#compile(com.digsarustudio.banana.database.query.statement.JoinStatement)}.
	 */
	@Test
	public void testCompileJoinStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler#compile(com.digsarustudio.banana.database.query.statement.UpdateStatement)}.
	 */
	@Test
	public void testCompileUpdateStatement() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler#compile(com.digsarustudio.banana.database.query.statement.DeleteStatement)}.
	 */
	@Test
	public void testCompileDeleteStatement() {
		fail("Not yet implemented");
	}

}
