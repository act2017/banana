/**
 * 
 */
package com.digsarustudio.banana.database.query;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * To test {@link SortingCriteria}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2
 */
@Ignore
public class SortingCriteriaTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SortingCriteria#getColumn()}.
	 */
	@Test
	public void testGetColumn() {
		SortingCriteria criteria = SortingCriteria.builder().setColumn("id").build();
		
		assertEquals("id", criteria.getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SortingCriteria#setColumn(java.lang.String)}.
	 */
	@Test
	public void testSetColumn() {
		SortingCriteria criteria = SortingCriteria.builder().build();		
		assertNull(criteria.getColumn());
		
		criteria.setColumn("qty");
		assertEquals("qty", criteria.getColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SortingCriteria#getType()}.
	 */
	@Test
	public void testGetType() {
		SortingCriteria criteria = SortingCriteria.builder().setType(SortingType.Ascending).build();
		
		assertEquals(SortingType.Ascending, criteria.getType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SortingCriteria#setType(com.digsarustudio.banana.database.query.SortingType)}.
	 */
	@Test
	public void testSetType() {
		SortingCriteria criteria = SortingCriteria.builder().build();		
		assertNull(criteria.getType());
		
		criteria.setType(SortingType.Descending);
		assertEquals(SortingType.Descending, criteria.getType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.query.SortingCriteria#getQuerySortingCriteria()}.
	 */
	@Test
	public void testGetQuerySortingCriteria() {
		SortingCriteria criteria = SortingCriteria.builder().setColumn("id").setType(SortingType.Descending).build();
		
		assertEquals("id", criteria.getColumn());
		assertEquals(SortingType.Descending, criteria.getType());
		
		assertEquals("id DESC", criteria.getQuerySortingCriteria());
	}

}
