/**
 * 
 */
package com.digsarustudio.banana.database.table;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link ColumnDataType}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ColumnDataTypeTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#ColumnDataType(String)}.
	 */
	@Test
	public void testColumnDataTypeConstructor() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#getDataType()}.
	 */
	@Test
	public void testGetDataType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#setDataType(com.digsarustudio.banana.database.table.TableColumnDataTypes)}.
	 */
	@Test
	public void testSetDataType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#getLength()}.
	 */
	@Test
	public void testGetLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#setLength(java.lang.Integer)}.
	 */
	@Test
	public void testSetLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#getSupplementaryLength()}.
	 */
	@Test
	public void testGetSupplementaryLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#setSupplementaryLength(java.lang.Integer)}.
	 */
	@Test
	public void testSetSupplementaryLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#getEnumerations()}.
	 */
	@Test
	public void testGetEnumerations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#setEnumerations(java.util.List)}.
	 */
	@Test
	public void testSetEnumerations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ColumnDataType#addEnumerations(java.lang.String)}.
	 */
	@Test
	public void testAddEnumerations() {
		fail("Not yet implemented");
	}

}
