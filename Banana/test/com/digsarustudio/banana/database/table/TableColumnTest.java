/**
 * 
 */
package com.digsarustudio.banana.database.table;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.ForeignKeyConstraint;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link TableColumnTest}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TableColumnTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getName()}.
	 */
	@Test
	public void testGetName() {
		TableColumn column = TableColumn.builder().setName("col_1").build();
		
		assertEquals("col_1", column.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		TableColumn column = TableColumn.builder().setName("col_1").build();		
		assertEquals("col_1", column.getName());
		
		column.setName("col_11");
		assertEquals("col_11", column.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getDataType()}.
	 */
	@Test
	public void testGetDataType() {
		TableColumn column = TableColumn.builder().setDataType(TableColumnDataTypes.Integer).build();		
		assertEquals(TableColumnDataTypes.Integer, column.getDataType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setDataType(com.digsarustudio.banana.database.table.TableColumnDataTypes)}.
	 */
	@Test
	public void testSetDataType() {
		TableColumn column = TableColumn.builder().setDataType(TableColumnDataTypes.Integer).build();		
		assertEquals(TableColumnDataTypes.Integer, column.getDataType());
		
		column.setDataType(TableColumnDataTypes.Binary);
		assertEquals(TableColumnDataTypes.Binary, column.getDataType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getLength()}.
	 */
	@Test
	public void testGetLength() {
		TableColumn column = TableColumn.builder().setLength(32).build();		
		assertEquals(new Integer(32), column.getLength());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setLength(java.lang.Integer)}.
	 */
	@Test
	public void testSetLength() {
		TableColumn column = TableColumn.builder().setLength(32).build();		
		assertEquals(new Integer(32), column.getLength());
		
		column.setLength(64);
		assertEquals(new Integer(64), column.getLength());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isNullable()}.
	 */
	@Test
	public void testIsNullable() {
		TableColumn column = TableColumn.builder().setNullable().build();		
		assertTrue(column.isNullable());
		
		column.setUnNullable();
		assertFalse(column.isNullable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setNullable()}.
	 */
	@Test
	public void testSetNullable() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isNullable());
		
		column.setNullable();		
		assertTrue(column.isNullable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setUnNullable()}.
	 */
	@Test
	public void testSetUnNullable() {
		TableColumn column = TableColumn.builder().setNonNullable().build();		
		assertFalse(column.isNullable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isAutoIncrement()}.
	 */
	@Test
	public void testIsAutoIncrement() {
		TableColumn column = TableColumn.builder().setAutoIncrement().build();		
		assertTrue(column.isAutoIncrement());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#enableAutoIncrement()}.
	 */
	@Test
	public void testEnableAutoIncrement() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isAutoIncrement());
		
		column.enableAutoIncrement();
		assertTrue(column.isAutoIncrement());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#disableAutoIncrement()}.
	 */
	@Test
	public void testDisableAutoIncrement() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isAutoIncrement());
		
		column.disableAutoIncrement();
		assertFalse(column.isAutoIncrement());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isUnsignedColumn()}.
	 */
	@Test
	public void testIsUnsignedColumn() {
		TableColumn column = TableColumn.builder().setAsUnsignedColumn().build();		
		assertTrue(column.isUnsignedColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setAsUnsignedColumn()}.
	 */
	@Test
	public void testSetAsUnsignedColumn() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isUnsignedColumn());
		
		column.setAsUnsignedColumn();
		assertTrue(column.isUnsignedColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setAsSignedColumn()}.
	 */
	@Test
	public void testSetAsSignedColumn() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isUnsignedColumn());
		
		column.setAsSignedColumn();
		assertFalse(column.isUnsignedColumn());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isPrimaryKey()}.
	 */
	@Test
	public void testIsPrimaryKey() {
		TableColumn column = TableColumn.builder().setAsPrimaryKey().build();		
		assertTrue(column.isPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setAsPrimaryKey()}.
	 */
	@Test
	public void testSetAsPrimaryKey() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isPrimaryKey());
		
		column.setAsPrimaryKey();
		assertTrue(column.isPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setNotAsPrimaryKey()}.
	 */
	@Test
	public void testSetNotAsPrimaryKey() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isPrimaryKey());
		
		column.setNotAsPrimaryKey();
		assertFalse(column.isPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isUniqueKey()}.
	 */
	@Test
	public void testIsUniqueKey() {
		TableColumn column = TableColumn.builder().setAsUniqueKey().build();		
		assertTrue(column.isUniqueKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setAsUniqueKey()}.
	 */
	@Test
	public void testSetAsUniqueKey() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isUniqueKey());
		
		column.setAsUniqueKey();
		assertTrue(column.isUniqueKey());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setAsUniqueKey(String)}.
	 */
	@Test
	public void testSetAsUniqueKeyWithIndexName(){
		TableColumn column = TableColumn.builder().build();
		assertFalse(column.isUniqueKey());
		
		column.setAsUniqueKey("testUnique");
		assertTrue(column.isUniqueKey());
		assertEquals("testUnique", column.getIndexName());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getIndexName()}.
	 */
	@Test
	public void testGetNameOfUniqueKey(){
		TableColumn column = TableColumn.builder().setAsUniqueKey("testUnique").build();
		assertTrue(column.isUniqueKey());
		assertEquals("testUnique", column.getIndexName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setNotAsUniqueKey()}.
	 */
	@Test
	public void testSetNotAsUniqueKey() {
		TableColumn column = TableColumn.builder().build();		
		assertFalse(column.isUniqueKey());
		
		column.setNotAsUniqueKey();
		assertFalse(column.isUniqueKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getForeignKeyConstraint()}.
	 */
	@Test
	public void testGetForeignKeyConstraint() {
		TableColumn column = TableColumn.builder().setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("tb_1")
																								   .setReferentialPrimaryKey("tb_1_col_1")
																								   .build()
																		)
												  .build();		
		
		assertEquals("tb_1", column.getForeignKeyConstraint().getReferentialTable());
		assertEquals("tb_1_col_1", column.getForeignKeyConstraint().getReferentialPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setForeignKeyConstraint(com.digsarustudio.banana.database.table.ForeignKeyConstraint)}.
	 */
	@Test
	public void testSetForeignKeyConstraint() {
		TableColumn column = TableColumn.builder().build();		
		assertNull(column.getForeignKeyConstraint());
		
		
		ForeignKeyConstraint foreignKeyConstraint = ForeignKeyConstraint.builder().setReferentialTable("tb_1")
																				  .setReferentialPrimaryKey("tb_1_col_1")
																				  .build();
		
		column.setForeignKeyConstraint(foreignKeyConstraint);
		assertEquals("tb_1", column.getForeignKeyConstraint().getReferentialTable());
		assertEquals("tb_1_col_1", column.getForeignKeyConstraint().getReferentialPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isForeignKey()}.
	 */
	@Test
	public void testIsForeignKey() {
		TableColumn column = TableColumn.builder().setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("tb_1")
																									     .setReferentialPrimaryKey("tb_1_col_1")
																									     .build()
																			)
																			.build();		
		
		assertTrue(column.isForeignKey());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isForeignKey()}.
	 */
	@Test
	public void testIsNotForeignKey() {
		TableColumn column = TableColumn.builder().build();		
		
		assertFalse(column.isForeignKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setSupportFullTextSearch()}.
	 */
	@Test
	public void testSetSupportFullTextSearch() {
		TableColumn column = TableColumn.builder().build();
		assertFalse(column.isSupportFullTextSearch());
		
		column.setSupportFullTextSearch();
		assertTrue(column.isSupportFullTextSearch());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setNotSupportFullTextSearch()}.
	 */
	@Test
	public void testSetNotSupportFullTextSearch() {
		TableColumn column = TableColumn.builder().build();
		assertFalse(column.isSupportFullTextSearch());
		
		column.setNotSupportFullTextSearch();
		assertFalse(column.isSupportFullTextSearch());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isSupportFullTextSearch()}.
	 */
	@Test
	public void testIsSupportFullTextSearch() {
		TableColumn column = TableColumn.builder().setSupportFullTextSearch().build();		
		assertTrue(column.isSupportFullTextSearch());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setPrecisionLength(java.lang.Integer)}.
	 */
	@Test
	public void testSetPrecisionLength() {
		TableColumn column = TableColumn.builder().build();		
		assertNull(column.getPrecisionLength());
		
		column.setPrecisionLength(128);
		assertEquals(new Integer(128), column.getPrecisionLength());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getPrecisionLength()}.
	 */
	@Test
	public void testGetPrecisionLength() {
		TableColumn column = TableColumn.builder().setPrecisionLength(128).build();		
		assertEquals(new Integer(128), column.getPrecisionLength());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#addEnum(java.lang.String)}.
	 */
	@Test
	public void testAddEnum() {
		TableColumn column = TableColumn.builder().build();		
		assertNull(column.getEnums());
		
		column.addEnum("KIKI");
		assertNotNull(column.getEnums());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#getEnums()}.
	 */
	@Test
	public void testGetEnums() {
		TableColumn column = TableColumn.builder().addEnum("Hello")
												  .addEnum("KIKI")
												  .build();		
		
		List<String> enums = column.getEnums();
		assertTrue(enums.contains("Hello"));
		assertTrue(enums.contains("KIKI"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setOnUpdateCurrentTimestamp()}
	 */
	@Test
	public void testSetOnUpdateCurrentTimestampe(){
		TableColumn column = TableColumn.builder().build();
		
		assertFalse(column.isOnUpdateCurrentTimestamp());
		
		column.setOnUpdateCurrentTimestamp();
		assertTrue(column.isOnUpdateCurrentTimestamp());
		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#setNotOnUpdateCurrentTimestamp()}
	 */
	@Test
	public void testSetNotOnUpdateCurrentTimestampe(){
		TableColumn column = TableColumn.builder().build();
		
		assertFalse(column.isOnUpdateCurrentTimestamp());
		
		column.setNotOnUpdateCurrentTimestamp();
		assertFalse(column.isOnUpdateCurrentTimestamp());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#isOnUpdateCurrentTimestampe()}
	 */
	@Test
	public void testIsOnUpdateCurrentTimestampe(){
		TableColumn column = TableColumn.builder().setOnUpdateCurrentTimestamp().build();
		
		assertTrue(column.isOnUpdateCurrentTimestamp());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.TableColumn#builder()}.
	 */
	@Test
	public void testBuilder() {
		TableColumn column = TableColumn.builder().setName("tb_test")
												  .setDataType(TableColumnDataTypes.Integer)
												  .setLength(128)
												  .setAsPrimaryKey()
												  .setNullable()
												  .build();		
		
		assertEquals("tb_test", column.getName());
		assertEquals(TableColumnDataTypes.Integer, column.getDataType());
		assertEquals(new Integer(128), column.getLength());
		assertTrue(column.isPrimaryKey());
		assertTrue(column.isNullable());
	}

}
