/**
 * 
 */
package com.digsarustudio.banana.database.table;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.mysql.MySQLConnector;
import com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator;
import com.digsarustudio.banana.database.mysql.MySQLTableManipulator;
import com.digsarustudio.banana.database.table.TableManipulator;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link TableManipulator}
 * 
 * TODO Refactoring
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MySQLTableManipulatorTest {
	private DatabaseConnector	connector	= null;
	private Query				query		= null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.connector = new MySQLConnector();
		this.connector.setURL("localhost");
		this.connector.setUserName("db_boss");
		this.connector.setPassword("1234");
		
		this.connector.connect();
		
		this.query = com.digsarustudio.banana.database.mysql.MySQLQuery.builder().setConnection(this.connector.getConnection())
																			.build();
		
		MySQLDatabaseManipulator dbManipulator = new MySQLDatabaseManipulator(this.query);
		dbManipulator.selectDatabase("junit_test_db");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.query.close();
		this.connector.disconnect();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#createTable()}.
	 * @throws SQLException 
	 */
	@Test
	public void testCreateTable() throws SQLException {
		Table table = new AbstractTable("test_create_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .build();
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#alterTable()}.
	 * 
	 * @deprecated 1.0.0
	 */
	@Ignore
	@Test
	public void testAlterTable() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#deleteTable()}.
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteTable() throws SQLException {
		Table table = new AbstractTable("test_delete_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .build();
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		manipulator.deleteTable();
		assertFalse(manipulator.isTableExisting());
		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#deleteTable(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteTableWithName() throws SQLException {
		Table table = new AbstractTable("test_delete_table_by_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .build();
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());

		manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .build();		
		
		manipulator.deleteTable("test_delete_table_by_name");
		assertFalse(manipulator.isTableExisting());
		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#clearTable()}.
	 * @throws SQLException 
	 */
	@Test
	public void testClearTable() throws SQLException {
		Table table = new AbstractTable("test_clear_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		//write a data
		String queryString = "INSERT INTO `" + table.getName() + "` (`make`) VALUES ('TOYOTA');";
		this.query.executeUpdate(queryString);
		
		queryString = "INSERT INTO `" + table.getName() + "` (`make`) VALUES ('HONDA');";
		this.query.executeUpdate(queryString);
		
		queryString = "SELECT * FROM `" + table.getName() + "`;";
		QueryResult result = this.query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertTrue(result.next());
		assertEquals("HONDA", result.getString("make"));
		assertFalse(result.next());
		
		manipulator.clearTable();
		
		queryString = "SELECT * FROM `" + table.getName() + "`;";
		result = this.query.executeQuery(queryString);
		assertFalse(result.next());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#clearTable(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testClearTableByName() throws SQLException {
		Table table = new AbstractTable("test_clear_table_by_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		//write a data
		String queryString = "INSERT INTO `" + table.getName() + "` (`make`) VALUES ('TOYOTA');";
		this.query.executeUpdate(queryString);
		
		queryString = "INSERT INTO `" + table.getName() + "` (`make`) VALUES ('HONDA');";
		this.query.executeUpdate(queryString);
		
		queryString = "SELECT * FROM `" + table.getName() + "`;";
		QueryResult result = this.query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertTrue(result.next());
		assertEquals("HONDA", result.getString("make"));
		assertFalse(result.next());
		
		manipulator.clearTable("test_clear_table_by_name");
		
		queryString = "SELECT * FROM `test_clear_table_by_name`;";
		result = this.query.executeQuery(queryString);
		assertFalse(result.next());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#isTableExisting()}.
	 * @throws SQLException 
	 */
	@Test
	public void testIsTableExisting() throws SQLException {
		Table table = new AbstractTable("test_exist_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#isTableExisting()}.
	 * @throws SQLException 
	 */
	@Test
	public void testIsTableExistingWithName() throws SQLException {
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setPreventErrorIfTableExists()
													    .build();
		
		assertFalse(manipulator.isTableExisting("test_exist_table"));
		
		Table table = new AbstractTable("test_exist_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
												.setQuery(this.query)
											    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
											    .setTable(table)
											    .setPreventErrorIfTableExists()
											    .build();
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting("test_exist_table"));
	}

	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#renameTable(String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testRenameTableWithName() throws SQLException {
		Table table = new AbstractTable("test_rename_table_by_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();

		Table newTable = new AbstractTable("test_new_rename_table_by_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		TableManipulator tmpManipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
																				 .setQuery(this.query)
																				 .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																				 .setTable(newTable)
																				 .setPreventErrorIfTableExists()
																				 .build();
		
		if( tmpManipulator.isTableExisting() ){
			tmpManipulator.deleteTable();
		}
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		manipulator.renameTable("test_rename_table_by_name");
		
		assertTrue(manipulator.isTableExisting("test_new_rename_table_by_name"));
		assertFalse(manipulator.isTableExisting("test_rename_table_by_name"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#renameTable(Table)}.
	 * @throws SQLException 
	 */
	@Test
	public void renameTable() throws SQLException {
		Table table = new AbstractTable("test_rename_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();

		Table newTable = new AbstractTable("test_new_rename_table") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		TableManipulator tmpManipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
																				 .setQuery(this.query)
																				 .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																				 .setTable(newTable)
																				 .setPreventErrorIfTableExists()
																				 .build();
		
		if( tmpManipulator.isTableExisting() ){
			tmpManipulator.deleteTable();
		}
		
		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		manipulator.renameTable(table);
		
		assertTrue(manipulator.isTableExisting("test_new_rename_table"));
		assertFalse(manipulator.isTableExisting("test_rename_table"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#addColumn(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void addColumn() throws SQLException {
		Table table = new AbstractTable("test_add_column") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		manipulator.addColumn(TableColumn.builder().setName("model")
												   .setDataType(TableColumnDataTypes.VarChar)
												   .setLength(32)
												   .build());
		
		assertTrue(manipulator.isColumnExisting("model"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#modifyColumn(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void modifyColumn() throws SQLException {
		Table table = new AbstractTable("test_add_column") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("model")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		TableColumn newColumn = TableColumn.builder().setName("model")
													 .setDataType(TableColumnDataTypes.Integer)
													 .setLength(10)
													 .setNonNullable()
													 .build();
		
		manipulator.modifyColumn(newColumn);
		
		TableColumn retrieved = manipulator.getColumn("model");
		assertEquals(TableColumnDataTypes.Integer, retrieved.getDataType());
		assertEquals(new Integer(10), retrieved.getLength());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#changeColumnName(String)}.
	 * @throws SQLException 
	 */
	@Test
	public void changeColumnNameByName() throws SQLException {
		Table table = new AbstractTable("test_change_name_by_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("model")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		assertTrue(manipulator.isColumnExisting("model"));
	
		TableColumn newColumn = TableColumn.builder().setName("year")
													 .setDataType(TableColumnDataTypes.Integer)
													 .setLength(10)
													 .setNonNullable()
													 .build();
		
		manipulator.changeColumnName("model", newColumn);
		
		TableColumn org = manipulator.getColumn("model");
		assertNull(org);		
		
		TableColumn retrieved = manipulator.getColumn("year");
		assertEquals("year", retrieved.getName());
		assertEquals(TableColumnDataTypes.Integer, retrieved.getDataType());
		assertEquals(new Integer(10), retrieved.getLength());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#changeColumnName(Table)}.
	 * @throws SQLException 
	 */
	@Test
	public void changeColumnName() throws SQLException {
		Table table = new AbstractTable("test_change_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableColumn col2Modify = TableColumn.builder().setName("model")
													 .setDataType(TableColumnDataTypes.VarChar)
													 .setLength(32)
													 .setNullable()
													 .build();
		table.addColumn(col2Modify);
		TableManipulator manipulator = MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		assertTrue(manipulator.isColumnExisting("model"));
	
		TableColumn newColumn = TableColumn.builder().setName("year")
													 .setDataType(TableColumnDataTypes.Integer)
													 .setLength(10)
													 .setNonNullable()
													 .build();
		
		manipulator.changeColumnName(col2Modify, newColumn);
		
		TableColumn org = manipulator.getColumn(col2Modify.getName());
		assertNull(org);		
		
		TableColumn retrieved = manipulator.getColumn("year");
		assertEquals("year", retrieved.getName());
		assertEquals(TableColumnDataTypes.Integer, retrieved.getDataType());
		assertEquals(new Integer(10), retrieved.getLength());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#registerAsPrimaryKey(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void registerAsPrimaryKey() throws SQLException {
		Table table = new AbstractTable("test_register_pk") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		TableColumn pkCol = TableColumn.builder().setName("id")
												 .setDataType(TableColumnDataTypes.Integer)
												 .setLength(10)
												 .build();
		
		table.addColumn(pkCol);
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		manipulator.registerColumnAsPrimaryKey(pkCol);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#unregisterPrimaryKeys()}.
	 * @throws SQLException 
	 */
	@Test
	public void unregisterPrimaryKeys() throws SQLException {
		Table table = new AbstractTable("test_unregister_pk") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAsPrimaryKey()
											 .build()
											 );
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build()
											 );
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		
		manipulator.unregisterPrimaryKeys();
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#registerAsForeignKey(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void registerAsForeignKey() {
		fail("Not yet implemented because results of show columns cannot be valided");
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#unregisterFromForeignKey(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void unregisterFromForeignKey() {
		fail("Not yet implemented because results of show columns cannot be valided");
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#registerAsUniqueKey(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void registerAsUniqueKey() {
		fail("Not yet implemented because result cannot be valided");
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#unregisterFromUniquekKey(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void unregisterFromUniquekKey() {
		fail("Not yet implemented because result cannot be valided");
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#removeColumn(String)}.
	 * @throws SQLException 
	 */
	@Test
	public void removeColumnByName() throws SQLException {
		Table table = new AbstractTable("test_remove_column") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		TableColumn col2Remove = TableColumn.builder().setName("make")
													 .setDataType(TableColumnDataTypes.VarChar)
													 .setLength(32)
													 .setNullable()
													 .build();
		table.addColumn(col2Remove);
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		assertTrue(manipulator.isColumnExisting("make"));
		
		manipulator.removeColumn("make");
		
		assertFalse(manipulator.isColumnExisting("make"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#removeColumn(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void removeColumn() throws SQLException {
		Table table = new AbstractTable("test_remove_column") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .build()
											 );
		
		TableColumn col2Remove = TableColumn.builder().setName("make")
													 .setDataType(TableColumnDataTypes.VarChar)
													 .setLength(32)
													 .setNullable()
													 .build();
		table.addColumn(col2Remove);
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isTableExisting());
		assertTrue(manipulator.isColumnExisting(col2Remove));
		
		manipulator.removeColumn(col2Remove);
		
		assertFalse(manipulator.isColumnExisting(col2Remove));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#isColumnExisting(String)}.
	 * @throws SQLException 
	 */
	@Test
	public void isColumnExistingByName() throws SQLException{
		Table table = new AbstractTable("test_col_exist_by_name") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAsPrimaryKey()
											 .build()
											 );
		
		TableColumn col2Check = TableColumn.builder().setName("make")
													 .setDataType(TableColumnDataTypes.VarChar)
													 .setLength(32)
													 .setNullable()
													 .build();
		table.addColumn(col2Check);
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isColumnExisting("make"));
		assertFalse(manipulator.isColumnExisting("model"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLTableManipulator#isColumnExisting(TableColumn)}.
	 * @throws SQLException 
	 */
	@Test
	public void isColumnExistingByColumnObject() throws SQLException{
		Table table = new AbstractTable("test_col_exist") {
			
			@Override
			protected void initColumns() {				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.Integer)
											 .setLength(10)
											 .setAsPrimaryKey()
											 .build()
											 );
		
		TableColumn col2Check = TableColumn.builder().setName("make")
													 .setDataType(TableColumnDataTypes.VarChar)
													 .setLength(32)
													 .setNullable()
													 .build();
		table.addColumn(col2Check);
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder()
														.setQuery(this.query)
													    .setDatabaseEngineType(DatabaseEngineType.InnoDB)
													    .setTable(table)
													    .setPreventErrorIfTableExists()
													    .build();
		
		TableColumn nonExistCol = TableColumn.builder().setName("model")
													 .setDataType(TableColumnDataTypes.VarChar)
													 .setLength(32)
													 .setNullable()
													 .build();
		
		if( manipulator.isTableExisting() ){
			manipulator.deleteTable();
		}

		manipulator.createTable();
		assertTrue(manipulator.isColumnExisting(col2Check));
		assertFalse(manipulator.isColumnExisting(nonExistCol));
	}
}
