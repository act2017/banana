/**
 * 
 */
package com.digsarustudio.banana.database.table;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.ForeignKeyConstraint;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;

/**
 * To test {@link AbstractTable}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AbstractTableTest {	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#AbstractTable(java.lang.String)}.
	 */
	@Test
	public void testAbstractTable() {
		new AbstractTable("test") {
			
			@Override
			protected void initColumns() {
				
			}
		};
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		assertEquals("test", table.getName());
		
		table.setName("rename");
		assertEquals("rename", table.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getName()}.
	 */
	@Test
	public void testGetName() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		assertEquals("test", table.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getColumn(java.lang.Integer)}.
	 */
	@Test
	public void testGetColumn() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setDataType(TableColumnDataTypes.Integer)
											.build());
		
		assertEquals("col_1", table.getColumn("col_1").getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getPrimaryKey(java.lang.Integer)}.
	 */
	@Test
	public void testGetPrimaryKey() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
												.setAsPrimaryKey()
												.build());
		
		assertTrue(table.getColumn("col_1").isPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getPrimaryKey(java.lang.Integer)}.
	 */
	@Test
	public void testGetMultiplePrimaryKeys() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setAsPrimaryKey()
											.build());
		table.addColumn(TableColumn.builder().setName("col_2")
											.build());
		
		table.addColumn(TableColumn.builder().setName("col_3")
											.setAsPrimaryKey()
											.build());
		
		assertTrue(table.getColumn("col_1").isPrimaryKey());
		assertFalse(table.getColumn("col_2").isPrimaryKey());
		assertTrue(table.getColumn("col_3").isPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getForeignKey(java.lang.Integer)}.
	 */
	@Test
	public void testGetForeignKey() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("ref")
																							 .setReferentialPrimaryKey("ref_pk")
																							 .build())
											.build());
		
		assertEquals("ref", table.getColumn("col_1").getForeignKeyConstraint().getReferentialTable());
		assertEquals("ref_pk", table.getColumn("col_1").getForeignKeyConstraint().getReferentialPrimaryKey());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getForeignKey(java.lang.Integer)}.
	 */
	@Test
	public void testGetMultipleForeignKeys() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("ref")
																							 .setReferentialPrimaryKey("ref_pk")
																							 .build())
											.build());

		table.addColumn(TableColumn.builder().setName("col_2")
											.setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("ref_1")
																							 .setReferentialPrimaryKey("ref1_pk")
																							 .build())
											.build());		
		
		assertEquals("ref", table.getColumn("col_1").getForeignKeyConstraint().getReferentialTable());
		assertEquals("ref_pk", table.getColumn("col_1").getForeignKeyConstraint().getReferentialPrimaryKey());
		
		assertEquals("ref_1", table.getColumn("col_2").getForeignKeyConstraint().getReferentialTable());
		assertEquals("ref1_pk", table.getColumn("col_2").getForeignKeyConstraint().getReferentialPrimaryKey());
	}	

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getUniqueKey(java.lang.Integer)}.
	 */
	@Test
	public void testGetUniqueKey() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setAsUniqueKey()
											.build());
		
		assertTrue(table.getColumn("col_1").isUniqueKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#getUniqueKey(java.lang.Integer)}.
	 */
	@Test
	public void testGetMultipleUniqueKeys() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setAsUniqueKey()
											.build());
		table.addColumn(TableColumn.builder().setName("col_2")
											.build());
		
		table.addColumn(TableColumn.builder().setName("col_3")
											.setAsUniqueKey()
											.build());
		
		assertTrue(table.getColumn("col_1").isUniqueKey());
		assertFalse(table.getColumn("col_2").isUniqueKey());
		assertTrue(table.getColumn("col_3").isUniqueKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#addColumn(com.digsarustudio.banana.database.table.TableColumn)}.
	 */
	@Test
	public void testAddColumn() {
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1").build());
		
		assertNotNull(table.getColumn("col_1"));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#HasPrimaryKey()}.
	 */
	@Test
	public void testHasPrimayKey(){
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setAsPrimaryKey()
											.build());
		table.addColumn(TableColumn.builder().setName("col_2")
											.build());
		
		table.addColumn(TableColumn.builder().setName("col_3")
											.setAsPrimaryKey()
											.build());
		
		assertTrue(table.getColumn("col_1").isPrimaryKey());
		assertFalse(table.getColumn("col_2").isPrimaryKey());
		assertTrue(table.getColumn("col_3").isPrimaryKey());	
		
		assertTrue(table.hasPrimaryKey());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#HasForeignKey()}.
	 */
	@Test
	public void testHasForeignKey(){
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("ref")
																							 .setReferentialPrimaryKey("ref_pk")
																							 .build())
											.build());

		table.addColumn(TableColumn.builder().setName("col_2")
											.setForeignKeyConstraint(ForeignKeyConstraint.builder().setReferentialTable("ref_1")
																							 .setReferentialPrimaryKey("ref1_pk")
																							 .build())
											.build());		
		
		assertEquals("ref", table.getColumn("col_1").getForeignKeyConstraint().getReferentialTable());
		assertEquals("ref_pk", table.getColumn("col_1").getForeignKeyConstraint().getReferentialPrimaryKey());
		
		assertEquals("ref_1", table.getColumn("col_2").getForeignKeyConstraint().getReferentialTable());
		assertEquals("ref1_pk", table.getColumn("col_2").getForeignKeyConstraint().getReferentialPrimaryKey());
		
		assertTrue(table.hasForeignKey());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.AbstractTable#HasUniqueKey()}.
	 */
	@Test
	public void testHasUniqueKey(){
		AbstractTable table = new AbstractTable("test") {
			
			@Override
			protected void initColumns() {

			}
		};
		
		table.addColumn(TableColumn.builder().setName("col_1")
											.setAsUniqueKey()
											.build());
		table.addColumn(TableColumn.builder().setName("col_2")
											.build());
		
		table.addColumn(TableColumn.builder().setName("col_3")
											.setAsUniqueKey()
											.build());
		
		assertTrue(table.getColumn("col_1").isUniqueKey());
		assertFalse(table.getColumn("col_2").isUniqueKey());
		assertTrue(table.getColumn("col_3").isUniqueKey());
		
		assertTrue(table.hasUniqueKey());
	}
}
