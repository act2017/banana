/**
 * 
 */
package com.digsarustudio.banana.database;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link DatabaseConfig}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DatabaseConfigTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#getHost()}.
	 */
	@Test
	public void testGetHost() {
		DatabaseConfig config = DatabaseConfig.builder().setHost("localhost").build();
		
		assertEquals("localhost", config.getHost());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#setHost(java.lang.String)}.
	 */
	@Test
	public void testSetHost() {
		DatabaseConfig config = DatabaseConfig.builder().build();
		assertNull(config.getHost());
		
		config.setHost("localhost");
		assertEquals("localhost", config.getHost());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#getUserName()}.
	 */
	@Test
	public void testGetUserName() {
		DatabaseConfig config = DatabaseConfig.builder().setUserName("booss").build();
		
		assertEquals("booss", config.getUserName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#setUserName(java.lang.String)}.
	 */
	@Test
	public void testSetUserName() {
		DatabaseConfig config = DatabaseConfig.builder().build();
		assertNull(config.getUserName());
		
		config.setUserName("booss");
		assertEquals("booss", config.getUserName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#getPassword()}.
	 */
	@Test
	public void testGetPassword() {
		DatabaseConfig config = DatabaseConfig.builder().setPassword("1234").build();
		
		assertEquals("1234", config.getPassword());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#setPassword(java.lang.String)}.
	 */
	@Test
	public void testSetPassword() {
		DatabaseConfig config = DatabaseConfig.builder().build();
		assertNull(config.getPassword());
		
		config.setPassword("1234");
		assertEquals("1234", config.getPassword());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#getDefaultDB()}.
	 */
	@Test
	public void testGetDefaultDB() {
		DatabaseConfig config = DatabaseConfig.builder().setDefaultDB("junit_test").build();
		
		assertEquals("junit_test", config.getDefaultDB());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#setDefaultDB(java.lang.String)}.
	 */
	@Test
	public void testSetDefaultDB() {
		DatabaseConfig config = DatabaseConfig.builder().build();
		assertNull(config.getDefaultDB());
		
		config.setDefaultDB("junit_test");
		assertEquals("junit_test", config.getDefaultDB());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#getEngineType()}.
	 */
	@Test
	public void testGetEngineType() {
		DatabaseConfig config = DatabaseConfig.builder().setEngineType(DatabaseEngineType.InnoDB).build();
		
		assertEquals(DatabaseEngineType.InnoDB, config.getEngineType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#setEngineType(com.digsarustudio.banana.database.DatabaseEngineType)}.
	 */
	@Test
	public void testSetEngineType() {
		DatabaseConfig config = DatabaseConfig.builder().build();
		assertNull(config.getEngineType());
		
		config.setEngineType(DatabaseEngineType.InnoDB);
		assertEquals(DatabaseEngineType.InnoDB, config.getEngineType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#getPort()}.
	 */
	@Test
	public void testGetPort() {
		DatabaseConfig config = DatabaseConfig.builder().setPort(3389).build();
		
		assertEquals(3389, config.getPort().intValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#setPort(java.lang.Integer)}.
	 */
	@Test
	public void testSetPort() {
		DatabaseConfig config = DatabaseConfig.builder().build();
		assertEquals(DatabaseConfig.DEFAULT_PORT, config.getPort());
		
		config.setPort(3389);
		assertEquals(3389, config.getPort().intValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.DatabaseConfig#builder()}.
	 */
	@Test
	public void testBuilder() {
		DatabaseConfig.builder();
	}

}
