/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.ForeignKeyConstraint;
import com.digsarustudio.banana.database.table.ReferentialActions;

/**
 * To test {@link ForeignKeyConstraint}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ForeignKeyConstraintTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setReferencedTabe(java.lang.String)}.
	 */
	@Test
	public void testSetReferencedTabe() {
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();		
		
		details.setReferentialTabe("ref");
		assertEquals("ref", details.getReferentialTable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setReferencedPrimaryKey(java.lang.String)}.
	 */
	@Test
	public void testSetReferencedPrimaryKey() {
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();		
		
		details.setReferentialPrimaryKey("pk");
		assertEquals("pk", details.getReferentialPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#getReferencedTable()}.
	 */
	@Test
	public void testGetReferencedTable() {
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setReferentialTable("ref")
															   .build();
		
		assertEquals("ref", details.getReferentialTable());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#getReferencedPrimaryKey()}.
	 */
	@Test
	public void testGetReferencedPrimaryKey() {
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setReferentialPrimaryKey("pk")
				   											   .build();
		
		assertEquals("pk", details.getReferentialPrimaryKey());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnDeleteCascade()}
	 */
	@Test
	public void testSetOnDeleteCascadeWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnDeleteCascade()
				   											   .build();

		assertEquals(ReferentialActions.Cascade, details.getDeleteReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnDeleteCascade()}
	 */
	@Test
	public void testSetOnDeleteCascade(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getDeleteReferentialActions());
		
		details.setOnDeleteCascade();
		assertEquals(ReferentialActions.Cascade, details.getDeleteReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnDeleteRestrict()}
	 */
	@Test
	public void testSetOnDeleteRestrictWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnDeleteRestrict()
				   											   .build();

		assertEquals(ReferentialActions.Restrict, details.getDeleteReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnDeleteRestrict()}
	 */
	@Test
	public void testSetOnDeleteRestrict(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getDeleteReferentialActions());
		
		details.setOnDeleteRestrict();
		assertEquals(ReferentialActions.Restrict, details.getDeleteReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnDeleteNoAction()}
	 */
	@Test
	public void testSetOnDeleteNoActionWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnDeleteNoAction()
				   											   .build();

		assertEquals(ReferentialActions.NoAction, details.getDeleteReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnDeleteNoAction()}
	 */
	@Test
	public void testSetOnDeleteNoAction(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getDeleteReferentialActions());
		
		details.setOnDeleteNoAction();
		assertEquals(ReferentialActions.NoAction, details.getDeleteReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnDeleteSetNull()}
	 */
	@Test
	public void testSetOnDeleteSetNullWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnDeleteSetNull()
				   											   .build();

		assertEquals(ReferentialActions.SetNull, details.getDeleteReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnDeleteSetNull()}
	 */
	@Test
	public void testSetOnDeleteSetNull(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getDeleteReferentialActions());
		
		details.setOnDeleteSetNull();
		assertEquals(ReferentialActions.SetNull, details.getDeleteReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnDeleteSetDefault()}
	 */
	@Test
	public void testSetOnDeleteSetDefaultWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnDeleteSetDefault()
				   											   .build();

		assertEquals(ReferentialActions.SetDefault, details.getDeleteReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnDeleteSetDefault()}
	 */
	@Test
	public void testSetOnDeleteSetDefault(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getDeleteReferentialActions());
		
		details.setOnDeleteSetDefault();
		assertEquals(ReferentialActions.SetDefault, details.getDeleteReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnUpdateCascade()}
	 */
	@Test
	public void testSetOnUpdateCascadeWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnUpdateCascade()
				   											   .build();

		assertEquals(ReferentialActions.Cascade, details.getUpdateReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnUpdateCascade()}
	 */
	@Test
	public void testSetOnUpdateCascade(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getUpdateReferentialActions());
		
		details.setOnUpdateCascade();
		assertEquals(ReferentialActions.Cascade, details.getUpdateReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnUpdateRestrict()}
	 */
	@Test
	public void testSetOnUpdateRestrictWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnUpdateRestrict()
				   											   .build();

		assertEquals(ReferentialActions.Restrict, details.getUpdateReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnUpdateRestrict()}
	 */
	@Test
	public void testSetOnUpdateRestrict(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getUpdateReferentialActions());
		
		details.setOnUpdateRestrict();
		assertEquals(ReferentialActions.Restrict, details.getUpdateReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnUpdateNoAction()}
	 */
	@Test
	public void testSetOnUpdateNoActionWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnUpdateNoAction()
				   											   .build();

		assertEquals(ReferentialActions.NoAction, details.getUpdateReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnUpdateNoAction()}
	 */
	@Test
	public void testSetOnUpdateNoAction(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getUpdateReferentialActions());
		
		details.setOnUpdateNoAction();
		assertEquals(ReferentialActions.NoAction, details.getUpdateReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnUpdateSetNull()}
	 */
	@Test
	public void testSetOnUpdateSetNullWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnUpdateSetNull()
				   											   .build();

		assertEquals(ReferentialActions.SetNull, details.getUpdateReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnUpdateSetNull()}
	 */
	@Test
	public void testSetOnUpdateSetNull(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getUpdateReferentialActions());
		
		details.setOnUpdateSetNull();
		assertEquals(ReferentialActions.SetNull, details.getUpdateReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint.Builder#setOnUpdateSetDefault()}
	 */
	@Test
	public void testSetOnUpdateSetDefaultWhenBuilt(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().setOnUpdateSetDefault()
				   											   .build();

		assertEquals(ReferentialActions.SetDefault, details.getUpdateReferentialActions());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setOnUpdateSetDefault()}
	 */
	@Test
	public void testSetOnUpdateSetDefault(){
		ForeignKeyConstraint details = ForeignKeyConstraint.builder().build();
		assertNull(details.getUpdateReferentialActions());
		
		details.setOnUpdateSetDefault();
		assertEquals(ReferentialActions.SetDefault, details.getUpdateReferentialActions());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#getName()}
	 */
	@Test
	public void testGetName() {
		ForeignKeyConstraint constraint = ForeignKeyConstraint.builder().setName("orderNo").build();
		
		assertEquals("orderNo", constraint.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ForeignKeyConstraint#setName()}
	 */
	@Test
	public void testSetName() {
		ForeignKeyConstraint constraint = ForeignKeyConstraint.builder().build();
		assertNull(constraint.getName());
		
		constraint.setName("orderNo");
		assertEquals("orderNo", constraint.getName());
	}
}
