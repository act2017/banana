/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseConfig;
import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseManipulator;
import com.digsarustudio.banana.database.query.Query;

/**
 * To test {@link MySQLDatabaseManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2
 */
@Ignore
public class MySQLDatabaseManipulatorTest {
	static final String DB_NAME						= "junit_test_db";
	static final String DB_NAME_UTF16				= "junit_test_db_utf16";
	static final String DB_NAME_SELECT				= "junit_test_db_select";
	static final String DB_NAME_SELECT_STRING		= "junit_test_db_select_string";
	static final String DB_NAME_DELETE				= "junit_test_db_delete";
	static final String DB_NAME_DELETE_STRING		= "junit_test_db_delete_string";
	static final String DB_NAME_EXISTS				= "junit_test_db_exists";
	static final String DB_NAME_NON_EXISTS			= "junit_test_db_non_exists";
	static final String DB_NAME_EXISTS_STRING		= "junit_test_db_exists_string";
	static final String DB_NAME_NON_EXISTS_STRING	= "junit_test_db_non_exists_string";
	
	static DatabaseConnector connector	= null;
	static Query			 query		= null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		DatabaseConfig config = DatabaseConfig.builder().build();
		
		connector = new MySQLConnector();
		connector.setURL(config.getHost());
		connector.setUserName(config.getUserName());
		connector.setPassword(config.getPassword());
		
		connector.connect();		
		
		query = MySQLQuery.builder().setConnection(connector.getConnection())
									.build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		query.close();
		connector.disconnect();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {

	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#DatabaseManipulator(com.digsarustudio.banana.database.DatabaseConnector)}.
	 */
	@Test
	public void testDatabaseManipulator() {
		new MySQLDatabaseManipulator(query);
		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#createDatabase()}.
	 * @throws SQLException 
	 */
	@Test
	public void testCreateDatabase() throws SQLException {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(DB_NAME);
		manipulator.setCharacterSets(CharacterSets.UTF8Unicode);
		manipulator.setCollations(Collations.UTF8Unicode);
		manipulator.createDatabase();
		
		assertTrue(DB_NAME, manipulator.isDatabaseExisting());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#createDatabase(java.lang.String, com.digsarustudio.banana.database.CharacterSets, com.digsarustudio.banana.database.Collations)}.
	 * @throws SQLException 
	 */
	@Test
	public void testCreateDatabaseStringCharacterSetsCollations() throws SQLException {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.createDatabase(DB_NAME_UTF16, CharacterSets.UTF16Unicode, Collations.UTF16Unicode);
		assertTrue(DB_NAME_UTF16, manipulator.isDatabaseExisting());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#selectDatabase()}.
	 * @throws SQLException 
	 */
	@Test
	public void testSelectDatabase() throws SQLException {
		this.createDatabase(DB_NAME_SELECT);
				
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		
		manipulator.setName(DB_NAME_SELECT);
		manipulator.selectDatabase();
		
		this.deleteDatabase(DB_NAME_SELECT);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#selectDatabase(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testSelectDatabaseString() throws SQLException {
		this.createDatabase(DB_NAME_SELECT_STRING);
		
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.selectDatabase(DB_NAME_SELECT_STRING);
		
		this.deleteDatabase(DB_NAME_SELECT_STRING);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#deleteDatabase()}.
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteDatabase() throws SQLException {
		this.createDatabase(DB_NAME_DELETE);
		
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(DB_NAME_DELETE);
		manipulator.deleteDatabase();
		
		assertFalse(manipulator.isDatabaseExisting(DB_NAME_DELETE));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#deleteDatabase(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteDatabaseString() throws SQLException {
		this.createDatabase(DB_NAME_DELETE_STRING);
		
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);		
		manipulator.deleteDatabase(DB_NAME_DELETE_STRING);
		
		assertFalse(manipulator.isDatabaseExisting(DB_NAME_DELETE_STRING));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#setName(java.lang.String)}.
	 */
	@Test
	public void testSetName() {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(DB_NAME);
		
		assertEquals(DB_NAME, manipulator.getName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#setCharacterSets(com.digsarustudio.banana.database.CharacterSets)}.
	 */
	@Test
	public void testSetCharacterSets() {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setCharacterSets(CharacterSets.UTF16Unicode);
		
		assertEquals(CharacterSets.UTF16Unicode, manipulator.getCharacterSets());
	
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#setCollations(com.digsarustudio.banana.database.Collations)}.
	 */
	@Test
	public void testSetCollations() {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setCollations(Collations.UTF16Unicode);
		
		assertEquals(Collations.UTF16Unicode, manipulator.getCollations());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#isDatabaseExisting()}.
	 * @throws SQLException 
	 */
	@Test
	public void testIsDatabaseExistingForExistedDB() throws SQLException {
		this.createDatabase(DB_NAME_EXISTS);
		
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(DB_NAME_EXISTS);
		
		assertTrue(manipulator.isDatabaseExisting());
		
		this.deleteDatabase(DB_NAME_EXISTS);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#isDatabaseExisting()}.
	 * @throws SQLException 
	 */
	@Test
	public void testIsDatabaseExistingForNonExistedDB() throws SQLException {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(DB_NAME_NON_EXISTS);
		
		assertFalse(manipulator.isDatabaseExisting());		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#isDatabaseExisting(String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testIsParticularDatabaseExisting() throws SQLException {
		this.createDatabase(DB_NAME_EXISTS_STRING);
		
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		
		assertTrue(manipulator.isDatabaseExisting(DB_NAME_EXISTS_STRING));
		
		this.deleteDatabase(DB_NAME_EXISTS_STRING);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLDatabaseManipulator#isDatabaseExisting(String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testIsParticularDatabaseExistingForNonExistedDB() throws SQLException {
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		
		assertFalse(manipulator.isDatabaseExisting(DB_NAME_NON_EXISTS_STRING));
	}

	private void createDatabase(String name) throws SQLException{
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(name);
		manipulator.setCharacterSets(CharacterSets.UTF8Unicode);
		manipulator.setCollations(Collations.UTF8Unicode);
		manipulator.createDatabase();		
	}
	
	private void deleteDatabase(String name) throws SQLException{
		DatabaseManipulator manipulator = new MySQLDatabaseManipulator(query);
		manipulator.setName(name);
		manipulator.deleteDatabase();		
	}
}
