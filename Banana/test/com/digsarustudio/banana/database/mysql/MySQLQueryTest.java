/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;
import com.digsarustudio.banana.database.table.TableManipulator;

/**
 * To test {@link MySQLQuery}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2
 */
public class MySQLQueryTest {
	static DatabaseConnector connector = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Table table = new AbstractTable("test_query") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .setNonNullable()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("model")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("year")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build());
		
		connector = new MySQLConnector();
		connector.setUserName("db_boss");
		connector.setPassword("1234");
		
		connector.connect();
						
		Query query = MySQLQuery.builder().setConnection(connector.getConnection()).setDatabase("test_db").build();
				
		
		TableManipulator manipulator = MySQLTableManipulator.builder().setPreventErrorIfTableExists()
																	 .setTable(table)
																	 .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																	 .setQuery(query)
																	 .build();
		
		manipulator.createTable();		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		connector.disconnect();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.Query#executeUpdate(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testExecuteUpdate() throws SQLException {
		Query query = MySQLQuery.builder().setConnection(connector.getConnection()).setDatabase("test_db").build();
		
		String queryString = "INSERT INTO `test_query` (`make`,`model`,`year`) VALUES ('HONDA', 'CIVIC', '03/2005-ON');";
		
		Integer rows = query.executeUpdate(queryString);
		
		assertEquals(new Integer(1), rows);
		
		query.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.Query#executeQuery(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testExecuteQuery() throws SQLException {
		Query query = MySQLQuery.builder().setConnection(connector.getConnection()).setDatabase("test_db").build();
		
		String queryString = "SELECT * FROM `test_query`;";
		
		QueryResult result = query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString(1));		
		assertEquals("HONDA", result.getString("make"));
		assertEquals("CIVIC", result.getString(2));
		assertEquals("CIVIC", result.getString("model"));
		assertEquals("03/2005-ON", result.getString(3));
		assertEquals("03/2005-ON", result.getString("year"));
		
		result.close();
		query.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.Query#close()}.
	 * @throws SQLException 
	 */
	@Test
	public void testClose() throws SQLException {
		Query query = MySQLQuery.builder().setConnection(connector.getConnection()).setDatabase("test_db").build();
		
		String queryString = "SELECT * FROM `test_query`;";
		
		QueryResult result = query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString(1));
		assertEquals("HONDA", result.getString("make"));
		assertEquals("CIVIC", result.getString(2));
		assertEquals("CIVIC", result.getString("model"));
		assertEquals("03/2005-ON", result.getString(3));
		assertEquals("03/2005-ON", result.getString("year"));
		
		result.close();
		query.close();
	}



	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.Query#getLastQuery()}.
	 *  
	 */
	@Test
	public void testGetLastQuery() throws SQLException {
		Query query = MySQLQuery.builder().setConnection(connector.getConnection()).setDatabase("test_db").build();
		
		String queryString = "SHOW COLUMNS FROM `test_query`;";
		
		query.executeUpdate(queryString);		
		assertEquals("SHOW COLUMNS FROM `test_query`;", query.getLastQuery());
		
		query.close();
	}
}
