/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.mysql.MySQLConnector;

/**
 * To test {@link MySQLConnector}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2
 */
@Ignore
public class MySQLConnectorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#MySQLConnector()}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * 
	 * @since 1.0.0
	 */
	@Test
	public void testMySQLConnector() throws InstantiationException, IllegalAccessException
											, ClassNotFoundException, SQLException, DriverNotLoadedException {
		new MySQLConnector();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#setURL(java.lang.String)}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @since 1.0.0
	 */
	@Test
	public void testSetURL() throws InstantiationException, IllegalAccessException
															, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("localhost");

	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#setUserName(java.lang.String)}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @since 1.0.0
	 */
	@Test
	public void testSetUserName() throws InstantiationException, IllegalAccessException
										, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setUserName("db_boss");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#setPassword(java.lang.String)}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @since 1.0.0
	 */
	@Test
	public void testSetPassword() throws InstantiationException, IllegalAccessException
										, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setPassword("1234");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#connect()}.
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SQLException 
	 * @throws DriverNotLoadedException 
	 * 
	 * @since 1.0.0
	 */
	@Test
	public void testConnect() throws InstantiationException, IllegalAccessException
									, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("localhost");
		connector.setUserName("db_boss");
		connector.setPassword("1234");
		
		assertEquals("localhost", connector.getURL());
		assertEquals("db_boss", connector.getUserName());
		assertEquals("1234", connector.getPassword());
		
		connector.connect();
		
		//To release the resource
		connector.disconnect();
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#connect()}
	 * to test the URL which does not exist.
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SQLException 
	 * @throws DriverNotLoadedException 
	 * @since 1.0.0
	 */
	@Test(expected = SQLException.class)
	public void testConnectInexistentURL() throws InstantiationException, IllegalAccessException
												, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("192.168.254.1");
		connector.setUserName("db_boss");
		connector.setPassword("1234");
		
		assertEquals("192.168.254.1", connector.getURL());
		assertEquals("db_boss", connector.getUserName());
		assertEquals("1234", connector.getPassword());
		
		connector.connect();
		
		//To release the resource
		connector.disconnect();
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#connect()}
	 * to test invalid user name
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SQLException
	 *  
	 * @since 1.0.0
	 */
	@Test(expected = SQLException.class)
	public void testConnectWithInvalidUserName() throws InstantiationException, IllegalAccessException
														, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("localhost");
		connector.setUserName("wahaha");
		connector.setPassword("1234");
		
		assertEquals("localhost", connector.getURL());
		assertEquals("wahaha", connector.getUserName());
		assertEquals("1234", connector.getPassword());
		
		connector.connect();
		
		//To release the resource
		connector.disconnect();
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#connect()}
	 * to test invalid password
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SQLException 
	 * @since 1.0.0
	 */
	@Test(expected = SQLException.class)
	public void testConnectWithInvalidPassword() throws InstantiationException, IllegalAccessException
														, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("localhost");
		connector.setUserName("db_boss");
		connector.setPassword("5678");
		
		assertEquals("localhost", connector.getURL());
		assertEquals("db_boss", connector.getUserName());
		assertEquals("5678", connector.getPassword());
		
		connector.connect();
		
		//To release the resource
		connector.disconnect();
	}	

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#disconnect()}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws SQLException 
	 * @since 1.0.0
	 */
	@Test
	public void testDisconnect() throws InstantiationException, IllegalAccessException
										, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("localhost");
		connector.setUserName("db_boss");
		connector.setPassword("1234");
		
		assertEquals("localhost", connector.getURL());
		assertEquals("db_boss", connector.getUserName());
		assertEquals("1234", connector.getPassword());
		
		connector.connect();
		
		connector.disconnect();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#getURL()}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @since 1.0.0
	 */
	@Test
	public void testGetURL() throws InstantiationException, IllegalAccessException
									, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setURL("localhost");
		
		assertEquals("localhost", connector.getURL());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#getUserName()}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @since 1.0.0
	 */
	@Test
	public void testGetUserName() throws InstantiationException, IllegalAccessException
										, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setUserName("db_boss");
		
		assertEquals("db_boss", connector.getUserName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLConnector#getPassword()}.
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @since 1.0.0
	 */
	@Test
	public void testGetPassword() throws InstantiationException, IllegalAccessException
										, ClassNotFoundException, SQLException, DriverNotLoadedException {
		DatabaseConnector connector = new MySQLConnector();
		connector.setPassword("1234");
		
		assertEquals("1234", connector.getPassword());
	}

	
}
