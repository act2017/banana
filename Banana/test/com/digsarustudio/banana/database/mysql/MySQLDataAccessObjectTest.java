/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.dao.DataAccessObject;
import com.digsarustudio.banana.database.query.Comparators;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Operations;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.query.SQLAggregateFunctions;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.SortingCriteria;
import com.digsarustudio.banana.database.query.SortingType;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;
import com.digsarustudio.banana.database.table.TableManipulator;

/**
 * To test {@link DataAccessObject}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2
 * 
 */
@Ignore
public class MySQLDataAccessObjectTest {
	static DatabaseConnector connector = null;
	static Table table = null;
	static Query query = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
		table = new AbstractTable("data_access_object") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 	  .setDataType(TableColumnDataTypes.SmallInteger)
											 	  .setAutoIncrement()
											 	  .setAsPrimaryKey()
											 	  .setNonNullable()
											 	  .build());
		
		table.addColumn(TableColumn.builder().setName("make")
											 	  .setDataType(TableColumnDataTypes.VarChar)
											 	  .setLength(32)
											 	  .setNullable()
											 	  .build());
		
		table.addColumn(TableColumn.builder().setName("model")
											 	  .setDataType(TableColumnDataTypes.VarChar)
											 	  .setLength(32)
											 	  .setNullable()
											 	  .build());
		
		table.addColumn(TableColumn.builder().setName("year")
											 	  .setDataType(TableColumnDataTypes.VarChar)
											 	  .setLength(32)
											 	  .setNullable()
											 	  .build());
		
		connector = new MySQLConnector();
		connector.setURL("localhost");
		connector.setUserName("db_boss");
		connector.setPassword("1234");
		
		connector.connect();
		
		query = MySQLQuery.builder().setConnection(connector.getConnection()).setDatabase("junit_test").build();
		
		TableManipulator dao = MySQLTableManipulator.builder().setPreventErrorIfTableExists()
															 .setTable(table)
															 .setDatabaseEngineType(DatabaseEngineType.InnoDB)
															 .setQuery(query)
															 .build();
		if( !dao.isTableExisting() ){
			dao.createTable();
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		if(null != query){
			query.close();
		}
		
		if(null != connector){
			connector.disconnect();
		}
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		TableManipulator dao = MySQLTableManipulator.builder().setPreventErrorIfTableExists()
															 .setTable(table)
															 .setDatabaseEngineType(DatabaseEngineType.InnoDB)
															 .setQuery(query)
															 .build();		
		//To clear rows if the table exists already.
		dao.clearTable();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#insert()}.
	 * @throws SQLException 
	 */
	@Test
	public void testInsert() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());				
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());			
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2006-12/2016").build());
						
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
															  .setDataset(dataset)
															  .setTable(table)
															  .build();
		
		dao.insert();
		
		//check result
		List<Condition>  conditions = new ArrayList<>();		
		conditions.add(Condition.builder().setTableColumn(makeColumn)
										  .setValue("TOYOTA")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(modelColumn)
										  .setValue("HiAce")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(yearColumn)
										  .setValue("04/2006-12/2016")
										  .setComparator(Comparators.Equals)
										  .build());
		
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setTable(table)
													  .setConditions(conditions)
													  .build();

		QueryResult result = dao.fetch();
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("HiAce", result.getString("model"));
		assertEquals("04/2006-12/2016", result.getString("year"));		
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#update()}.
	 * @throws SQLException 
	 */
	@Test
	public void testUpdate() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();		
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2006-01/2010").build());
		
		
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();
		
		dao.insert();
		
		dataset = new ArrayList<>();		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2006-ON").build());
		
		
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn)
										  .setValue("TOYOTA")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(modelColumn)
										  .setValue("HiAce")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(yearColumn)
										  .setValue("04/2006-01/2010")
										  .setComparator(Comparators.Equals).build());
			
				
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .setConditions(conditions)
													  .build();
		
		dao.update();
		
		//check result
		conditions.clear();
		conditions.add(Condition.builder().setTableColumn(makeColumn)
										  .setValue("TOYOTA")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(modelColumn)
										  .setValue("HiAce")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(yearColumn)
										  .setValue("04/2006-ON")
										  .setComparator(Comparators.Equals)
										  .build());
		
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setTable(table)
													  .setConditions(conditions)
													  .build();

		QueryResult result = dao.fetch();
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("HiAce", result.getString("model"));
		assertEquals("04/2006-ON", result.getString("year"));
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#update(java.lang.Integer)}.
	 * @throws SQLException 
	 */
	@Test
	public void testUpdateWithLimit() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();		
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2006-01/2010").build());
		
				
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();
		
		dao.insert();
		dao.insert();
		dao.insert();
		
		dataset = new ArrayList<>();		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2006-ON").build());
		
		
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn)
										  .setValue("TOYOTA")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(modelColumn)
										  .setValue("HiAce")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(yearColumn)
										  .setValue("04/2006-01/2010")
										  .setComparator(Comparators.Equals)
										  .build());
			
				
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .setConditions(conditions)
													  .build();
		
		dao.update(2);
		
		//check result
		conditions.clear();
		conditions.add(Condition.builder().setTableColumn(makeColumn)
										  .setValue("TOYOTA")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(modelColumn)
										  .setValue("HiAce")
										  .setComparator(Comparators.Equals)
										  .build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(yearColumn)
										  .setValue("04/2006-01/2010")
										  .setComparator(Comparators.Equals)
										  .build());
		
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setTable(table)
													  .setConditions(conditions)
													  .build();

		QueryResult result = dao.fetch();
		Integer count = 0;
		
		while(result.next()) {
			count++;
		}
		
		assertEquals(new Integer(1), count);
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#fetch(java.lang.Integer, java.lang.Integer)}.
	 * @throws SQLException 
	 */
	@Test
	public void testFetchWithCursorAndLimit() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2006-01/2010").build());
		
			
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();	
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("Camry").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/1995-01/2008").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("RAV4").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("04/2000-01/2014").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setComparator(Comparators.Equals).setValue("TOYOTA").build());
		dao = MySQLDataAccessObject.builder().setConditions(conditions)
													  .setQuery(query)
													  .setTable(table)
													  .build();
		
		QueryResult result = dao.fetch(1, 2);
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("Camry", result.getString("model"));
		assertEquals("04/1995-01/2008", result.getString("year"));

		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("RAV4", result.getString("model"));
		assertEquals("04/2000-01/2014", result.getString("year"));
		
		assertFalse(result.next());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#fetch()}.
	 * @throws SQLException 
	 */
	@Test
	public void testFetch() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("01/2006-10/2010").build());
		
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();	
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("Camry").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("06/1995-09/2008").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("RAV4").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("11/2000-12/2014").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setComparator(Comparators.Equals).setValue("TOYOTA").build());
		dao = MySQLDataAccessObject.builder().setConditions(conditions)
													  .setQuery(query)
													  .setTable(table)
													  .build();
		
		QueryResult result = dao.fetch();
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("HiAce", result.getString("model"));
		assertEquals("01/2006-10/2010", result.getString("year"));
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("Camry", result.getString("model"));
		assertEquals("06/1995-09/2008", result.getString("year"));

		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("RAV4", result.getString("model"));
		assertEquals("11/2000-12/2014", result.getString("year"));
		
		assertFalse(result.next());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#fetch()}.
	 * @throws SQLException 
	 */
	@Test
	public void testFetchSortedlyAndGroupBy() throws SQLException{
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("01/2006-10/2010").build());
		
			
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();	
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("Camry").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("06/1995-09/2008").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("RAV4").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("11/2000-12/2014").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("HONDA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("CIVIC").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("11/1999-12/2014").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();
		dao.insert();		
		
		dao = MySQLDataAccessObject.builder().addColumn(SelectColumn.builder().setTableColumn(makeColumn)
													  						   .setAggregateFunction(SQLAggregateFunctions.Count)
													  						   .setAlias("total")
													  						   .build())
											 .addColumn(SelectColumn.builder().setTableColumn(makeColumn)
													 						  .build())
													  .addGroupBy(table.getColumn("make"))
													  .addSortingCriteria(SortingCriteria.builder().setColumn("total").setType(SortingType.Descending).build())
													  .setQuery(query)
													  .setTable(table)
													  .build();

		QueryResult result = dao.fetch();

		assertEquals("SELECT COUNT(`make`) AS 'total', `make` FROM `data_access_object` GROUP BY `make` ORDER BY `total` DESC;", dao.getLastQuery());
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("3", result.getString("total"));
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString("make"));
		assertEquals("1", result.getString("total"));

		assertFalse(result.next());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#delete()}.
	 * @throws SQLException 
	 */
	@Test
	public void testDelete() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("11/2006-10/2010").build());
		
		
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();	
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("Camry").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("10/1995-09/2008").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("RAV4").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("09/2000-12/2014").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		//Delete
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn)
										  .setComparator(Comparators.Equals)
										  .setValue("TOYOTA").build());
		conditions.add(Condition.builder().setOperation(Operations.And)
										  .setTableColumn(modelColumn)
										  .setComparator(Comparators.Equals)
										  .setValue("RAV4").build());
		dao = MySQLDataAccessObject.builder().setConditions(conditions)
													  .setQuery(query)
													  .setTable(table)
													  .build();

		dao.delete();

		
		//Verify
		conditions.clear();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setComparator(Comparators.Equals).setValue("TOYOTA").build());
		dao = MySQLDataAccessObject.builder().setConditions(conditions)
													  .setQuery(query)
													  .setTable(table)
													  .build();
		
		QueryResult result = dao.fetch();
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("HiAce", result.getString("model"));
		assertEquals("11/2006-10/2010", result.getString("year"));
		
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("Camry", result.getString("model"));
		assertEquals("10/1995-09/2008", result.getString("year"));

		assertFalse(result.next());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#delete(java.lang.Integer)}.
	 * @throws SQLException 
	 */
	@Test
	public void testDeleteWithLimit() throws SQLException {
		List<Dataset> dataset = new ArrayList<>();
		
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("HiAce").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("11/2006-10/2010").build());
		
		
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																	  .setDataset(dataset)
																	  .setTable(table)
																	  .build();	
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("Camry").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("10/1995-09/2008").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		dataset.clear();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("RAV4").build());
		dataset.add(Dataset.builder().setTableColumn(yearColumn).setValue("09/2000-12/2014").build());
		dao = MySQLDataAccessObject.builder().setQuery(query)
													  .setDataset(dataset)
													  .setTable(table)
													  .build();		
		dao.insert();
		
		//Delete
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setComparator(Comparators.Equals).setValue("TOYOTA").build());
		dao = MySQLDataAccessObject.builder().setConditions(conditions)
													  .setQuery(query)
													  .setTable(table)
													  .build();
		
		dao.delete(2);
		
		//Verify
		conditions.clear();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setComparator(Comparators.Equals).setValue("TOYOTA").build());
		dao = MySQLDataAccessObject.builder().setConditions(conditions)
													  .setQuery(query)
													  .setTable(table)
													  .build();
		
		QueryResult result = dao.fetch();
		assertTrue(result.next());
		assertEquals("TOYOTA", result.getString("make"));
		assertEquals("RAV4", result.getString("model"));
		assertEquals("09/2000-12/2014", result.getString("year"));
		
		assertFalse(result.next());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setQuery(com.digsarustudio.banana.database.query.Query)}.
	 */
	@Test
	public void testSetQuery() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertNull(dao.getQuery());
		
		dao.setQuery(query);
		assertEquals(query, dao.getQuery());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setTable(com.digsarustudio.banana.database.table.Table)}.
	 */
	@Test
	public void testSetTable() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertNull(dao.getTable());
		
		dao.setTable(table);
		assertEquals("data_access_object", dao.getTable().getName());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setIgnoreDuplicateData()}.
	 */
	@Test
	public void testSetIgnoreDuplicateData() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertFalse(dao.isIgnoreDuplicateData());
		
		dao.setIgnoreDuplicateData();;
		assertTrue(dao.isIgnoreDuplicateData());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setIncludeDuplicateData()}.
	 */
	@Test
	public void testSetIncludeDuplicateData() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertFalse(dao.isIgnoreDuplicateData());
		
		dao.setIncludeDuplicateData();
		assertFalse(dao.isIgnoreDuplicateData());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#addColumn(com.digsarustudio.banana.database.query.SelectColumn)}.
	 */
	@Test
	public void testAddColumn() {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertNull(dao.getColumns());
		
		dao.addColumn(SelectColumn.builder().setTableColumn(makeColumn).build());
		dao.addColumn(SelectColumn.builder().setTableColumn(modelColumn).build());
		
		assertEquals("make", dao.getColumns().get(0).getTableColumn().getColumn());
		assertEquals("model", dao.getColumns().get(1).getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setColumns(java.util.List)}.
	 */
	@Test
	public void testSetColumns() {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertNull(dao.getColumns());
		
		List<SelectColumn> columns = new ArrayList<>();
		
		columns.add(SelectColumn.builder().setTableColumn(makeColumn).build());
		columns.add(SelectColumn.builder().setTableColumn(modelColumn).build());
		dao.setColumns(columns);		
		
		assertEquals("make", dao.getColumns().get(0).getTableColumn().getColumn());
		assertEquals("model", dao.getColumns().get(1).getTableColumn().getColumn());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#getColumns()}
	 */
	@Test
	public void testGetColumns(){
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().addColumn(SelectColumn.builder().setTableColumn(makeColumn)
																											  .build())
																			 .addColumn(SelectColumn.builder().setTableColumn(modelColumn)
																					 						  .build())
																			 .build();
		
		assertNotNull(dao.getColumns());
		
		assertEquals("make", dao.getColumns().get(0).getTableColumn().getColumn());
		assertEquals("model", dao.getColumns().get(1).getTableColumn().getColumn());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#addDataset(com.digsarustudio.banana.database.query.Dataset)}.
	 */
	@Test
	public void testAddDataset() {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();	
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertNull(dao.getDataset());
		
		
		dao.addDataset(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dao.addDataset(Dataset.builder().setTableColumn(modelColumn).setValue("Hilux").build());
		
		
		assertEquals("make", dao.getDataset().get(0).getTableColumn().getColumn());
		assertEquals("TOYOTA", dao.getDataset().get(0).getValue());
		assertEquals("model", dao.getDataset().get(1).getTableColumn().getColumn());
		assertEquals("Hilux", dao.getDataset().get(1).getValue());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setDataset(java.util.List)}.
	 */
	@Test
	public void testSetDataset() {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		assertNull(dao.getDataset());
				
		List<Dataset> dataset = new ArrayList<>();
		dataset.add(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dataset.add(Dataset.builder().setTableColumn(modelColumn).setValue("Hilux").build());
		dao.setDataset(dataset);
		
		assertEquals("make", dao.getDataset().get(0).getTableColumn().getColumn());
		assertEquals("TOYOTA", dao.getDataset().get(0).getValue());
		assertEquals("model", dao.getDataset().get(1).getTableColumn().getColumn());
		assertEquals("Hilux", dao.getDataset().get(1).getValue());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#getDataset()}
	 */
	@Test
	public void testGetDataset(){
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().addDataset(Dataset.builder().setTableColumn(makeColumn).setValue("TOYOTA").build())
																			 .addDataset(Dataset.builder().setTableColumn(modelColumn).setValue("Hilux").build())
																			 .build();
		assertNotNull(dao.getDataset());		
		assertEquals("make", dao.getDataset().get(0).getTableColumn().getColumn());
		assertEquals("TOYOTA", dao.getDataset().get(0).getValue());
		assertEquals("model", dao.getDataset().get(1).getTableColumn().getColumn());
		assertEquals("Hilux", dao.getDataset().get(1).getValue());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#addCondition(com.digsarustudio.banana.database.query.Condition)}.
	 */
	@Test
	public void testAddCondition() {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getConditions());
		
		dao.addCondition(Condition.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		dao.addCondition(Condition.builder().setTableColumn(modelColumn).setValue("Yaries").build());
		dao.addCondition(Condition.builder().setTableColumn(yearColumn).setValue("01/2011-ON").build());
		
		assertEquals("make", dao.getConditions().get(0).getTableColumn().getColumn());
		assertEquals("TOYOTA", dao.getConditions().get(0).getValue());
		assertEquals("model", dao.getConditions().get(1).getTableColumn().getColumn());
		assertEquals("Yaries", dao.getConditions().get(1).getValue());
		assertEquals("year", dao.getConditions().get(2).getTableColumn().getColumn());
		assertEquals("01/2011-ON", dao.getConditions().get(2).getValue());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setConditions(java.util.List)}.
	 */
	@Test
	public void testSetConditions() {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getConditions());
		
		List<Condition> conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setValue("TOYOTA").build());
		conditions.add(Condition.builder().setTableColumn(modelColumn).setValue("Yaries").build());
		conditions.add(Condition.builder().setTableColumn(yearColumn).setValue("01/2011-ON").build());
		
		dao.setConditions(conditions);
		
		assertEquals("make", dao.getConditions().get(0).getTableColumn().getColumn());
		assertEquals("TOYOTA", dao.getConditions().get(0).getValue());
		assertEquals("model", dao.getConditions().get(1).getTableColumn().getColumn());
		assertEquals("Yaries", dao.getConditions().get(1).getValue());
		assertEquals("year", dao.getConditions().get(2).getTableColumn().getColumn());
		assertEquals("01/2011-ON", dao.getConditions().get(2).getValue());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#getConditions()}
	 */
	@Test
	public void testGetConditions(){
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder()
													   .addCondition(Condition.builder().setTableColumn(makeColumn).setValue("TOYOTA").build())
													   .addCondition(Condition.builder().setTableColumn(modelColumn).setValue("Yaries").build())
													   .addCondition(Condition.builder().setTableColumn(yearColumn).setValue("01/2011-ON").build())			
													   .build();
		
		assertNotNull(dao.getConditions());
		
		assertEquals("make", dao.getConditions().get(0).getTableColumn().getColumn());
		assertEquals("TOYOTA", dao.getConditions().get(0).getValue());
		assertEquals("model", dao.getConditions().get(1).getTableColumn().getColumn());
		assertEquals("Yaries", dao.getConditions().get(1).getValue());
		assertEquals("year", dao.getConditions().get(2).getTableColumn().getColumn());
		assertEquals("01/2011-ON", dao.getConditions().get(2).getValue());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#addSortingCriteria(com.digsarustudio.banana.database.query.SortingCriteria)}.
	 */
	@Test
	public void testAddSortingCriteria() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getSortingCriteria());
		
		dao.addSortingCriteria(SortingCriteria.builder().setColumn("make").setType(SortingType.Ascending).build());
		dao.addSortingCriteria(SortingCriteria.builder().setColumn("model").setType(SortingType.Descending).build());
		
		assertEquals("make", dao.getSortingCriteria().get(0).getColumn());
		assertEquals(SortingType.Ascending, dao.getSortingCriteria().get(0).getType());
		assertEquals("model", dao.getSortingCriteria().get(1).getColumn());
		assertEquals(SortingType.Descending, dao.getSortingCriteria().get(1).getType());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setSortingCriteria(java.util.List)}.
	 */
	@Test
	public void testSetSortingCriteria() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getSortingCriteria());
		
		List<SortingCriteria> criteria = new ArrayList<>();
		criteria.add(SortingCriteria.builder().setColumn("make").setType(SortingType.Ascending).build());
		criteria.add(SortingCriteria.builder().setColumn("model").setType(SortingType.Descending).build());
		
		dao.setSortingCriteria(criteria);		
		assertEquals("make", dao.getSortingCriteria().get(0).getColumn());
		assertEquals(SortingType.Ascending, dao.getSortingCriteria().get(0).getType());
		assertEquals("model", dao.getSortingCriteria().get(1).getColumn());
		assertEquals(SortingType.Descending, dao.getSortingCriteria().get(1).getType());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#getSortingCriteria()}.
	 */
	@Test
	public void testGetSortingCriteria(){
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder()
													   .addSortingCriteria(SortingCriteria.builder().setColumn("make").setType(SortingType.Ascending).build())
													   .addSortingCriteria(SortingCriteria.builder().setColumn("model").setType(SortingType.Descending).build())		
													   .build();
		
		assertNotNull(dao.getSortingCriteria());
		
		assertEquals("make", dao.getSortingCriteria().get(0).getColumn());
		assertEquals(SortingType.Ascending, dao.getSortingCriteria().get(0).getType());
		assertEquals("model", dao.getSortingCriteria().get(1).getColumn());
		assertEquals(SortingType.Descending, dao.getSortingCriteria().get(1).getType());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)}.
	 */
	@Test
	public void testAddJoinTable() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getJoinTables());
		
		Table table1 = new AbstractTable("inventory") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		Table table2 = new AbstractTable("products") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		
//		dao.addJoinTable(JoinTable.builder().setTable(table1).setType(TableJoinType.CrossJoin).build());
//		dao.addJoinTable(JoinTable.builder().setTable(table2).setType(TableJoinType.CrossJoin).build());
//		
		assertEquals("inventory", dao.getJoinTables().get(0).getTable().getName());
		assertEquals("products", dao.getJoinTables().get(1).getTable().getName());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setJoinTables(java.util.List)}.
	 */
	@Test
	public void testSetJoinTables() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getJoinTables());
		
		Table table1 = new AbstractTable("inventory") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		Table table2 = new AbstractTable("products") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		List<JoinTable> tables = new ArrayList<>();
//		tables.add(JoinTable.builder().setTable(table1).setType(TableJoinType.CrossJoin).build());
//		tables.add(JoinTable.builder().setTable(table2).setType(TableJoinType.CrossJoin).build());
		
		dao.setJoinTables(tables);
		
		assertEquals("inventory", dao.getJoinTables().get(0).getTable().getName());
		assertEquals("products", dao.getJoinTables().get(1).getTable().getName());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#getJoinTables()}.
	 */
	@Test
	public void testGetJoinTables(){		
		Table table1 = new AbstractTable("inventory") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		Table table2 = new AbstractTable("products") {
			
			@Override
			protected void initColumns() {
				
			}
		};		
		
//		MySQLDataAccessObject dao = MySQLDataAccessObject.builder()
//																   .addJoinTable(JoinTable.builder().setTable(table1).setType(TableJoinType.CrossJoin).build())
//																   .addJoinTable(JoinTable.builder().setTable(table2).setType(TableJoinType.CrossJoin).build())				
//																   .build();
//		
//		assertNotNull(dao.getJoinTables());		
//		assertEquals("inventory", dao.getJoinTables().get(0).getTable().getName());
//		assertEquals("products", dao.getJoinTables().get(1).getTable().getName());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#addGroupBy(com.digsarustudio.banana.database.table.TableColumn)}.
	 */
	@Test
	public void testAddGroupBy() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getGroupBys());
		
		dao.addGroupBy(TableColumn.builder().setName("make").build());
		dao.addGroupBy(TableColumn.builder().setName("model").build());
		
		assertEquals("make", dao.getGroupBys().get(0).getName());
		assertEquals("model", dao.getGroupBys().get(1).getName());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#setGroupBy(java.util.List)}.
	 */
	@Test
	public void testSetGroupBy() {
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().build();
		
		assertNull(dao.getGroupBys());
		
		List<TableColumn> columns = new ArrayList<>();		
		columns.add(TableColumn.builder().setName("make").build());
		columns.add(TableColumn.builder().setName("model").build());
		dao.setGroupBy(columns);
		assertEquals("make", dao.getGroupBys().get(0).getName());
		assertEquals("model", dao.getGroupBys().get(1).getName());
	}
	
	/**
	 * Test method for {@link MySQLDataAccessObject#getGroupBys()}.
	 */
	@Test
	public void testGetGroupBy(){
		MySQLDataAccessObject dao = MySQLDataAccessObject.builder()
																	.addGroupBy(TableColumn.builder().setName("make").build())
																	.addGroupBy(TableColumn.builder().setName("model").build())		
																	.build();
		
		assertNotNull(dao.getGroupBys());		
		assertEquals("make", dao.getGroupBys().get(0).getName());
		assertEquals("model", dao.getGroupBys().get(1).getName());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#clear()}
	 */
	@Test
	public void testClear(){
//		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
//		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
//		
//		Table table1 = new AbstractTable("vehicle_details") {
//			
//			@Override
//			protected void initColumns() {
//				
//			}
//		};
//		
//		table1.addColumn(TableColumn.builder().setName("make").build());
//		
//		Table table2 = new AbstractTable("products") {
//			
//			@Override
//			protected void initColumns() {
//				
//			}
//		};
//
//		SimpleTableColumn modelCodeColumn = SimpleTableColumn.builder().setColumn("modelCode").build();
//		SimpleTableColumn wheelDriveColumn = SimpleTableColumn.builder().setColumn("wheelDrive").build();
//		
//		MySQLDataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
//																	.addColumn(SelectColumn.builder().setTableColumn(makeColumn).build())
//																	.addColumn(SelectColumn.builder().setTableColumn(modelColumn).build())
//															  		 .setTable(table1)
//															  		 .addJoinTable(JoinTable.builder().setTable(table2).setType(TableJoinType.LeftJoin).build())
//															  		 .addCondition(Condition.builder().setTableColumn(modelCodeColumn).setComparator(Comparators.Equals).setValue("1Z32-CDG").build())
//															  		 .addCondition(Condition.builder().setTableColumn(wheelDriveColumn).setComparator(Comparators.Equals).setValue("RWD").build())
//															  		 .addSortingCriteria(SortingCriteria.builder().setColumn("make").setType(SortingType.Descending).build())
//															  		 .addGroupBy(table1.getColumn("make"))
//															  		 .build();
//																	  
//		assertEquals("make", dao.getColumns().get(0).getTableColumn().getColumn());
//		assertEquals("model", dao.getColumns().get(1).getTableColumn().getColumn());
//		
//		assertEquals("vehicle_details", dao.getTable().getName());
//		
//		assertEquals("products", dao.getJoinTables().get(0).getTable().getName());
//		assertEquals(TableJoinType.LeftJoin, dao.getJoinTables().get(0).getType());
//		
//		assertEquals("modelCode", dao.getConditions().get(0).getTableColumn().getColumn());
//		assertEquals(Comparators.Equals, dao.getConditions().get(0).getComparator());
//		assertEquals("1Z32-CDG", dao.getConditions().get(0).getValue());
//		
//		assertEquals("wheelDrive", dao.getConditions().get(1).getTableColumn().getColumn());
//		assertEquals(Comparators.Equals, dao.getConditions().get(1).getComparator());
//		assertEquals("RWD", dao.getConditions().get(1).getValue());
//		
//		assertEquals("make", dao.getGroupBys().get(0).getName());
//		
//		assertEquals("make", dao.getSortingCriteria().get(0).getColumn());
//		assertEquals(SortingType.Descending, dao.getSortingCriteria().get(0).getType());
//		
//		dao.clear();
//		
//		assertNull(dao.getColumns());
//		assertEquals("vehicle_details", dao.getTable().getName());
//		
//		assertNull(dao.getJoinTables());
//		assertNull(dao.getConditions());
//		
//		assertNull(dao.getGroupBys());
//		assertNull(dao.getSortingCriteria());
	}

	/**
	 * Test method for {@link MySQLDataAccessObject#getLastQuery()}.
	 * @throws SQLException 
	 */
	@Test
	public void testGetLastQuery() throws SQLException {
		SimpleTableColumn makeColumn = SimpleTableColumn.builder().setColumn("make").build();
		SimpleTableColumn modelColumn = SimpleTableColumn.builder().setColumn("model").build();
		SimpleTableColumn yearColumn = SimpleTableColumn.builder().setColumn("year").build();
		
		//check result
		List<Condition>  conditions = new ArrayList<>();
		conditions.add(Condition.builder().setTableColumn(makeColumn).setValue("TOYOTA").setComparator(Comparators.Equals).build());
		conditions.add(Condition.builder().setTableColumn(modelColumn).setValue("HiAce").setComparator(Comparators.Equals).setOperation(Operations.And).build());
		conditions.add(Condition.builder().setTableColumn(yearColumn).setValue("04/2006-ON").setComparator(Comparators.Equals).setOperation(Operations.And).build());
		
		DataAccessObject dao = MySQLDataAccessObject.builder().setQuery(query)
																  .setTable(table)
																  .setConditions(conditions)
																  .build();
		dao.addColumn(SelectColumn.builder().setTableColumn(makeColumn).build());
		dao.addColumn(SelectColumn.builder().setTableColumn(modelColumn).build());
		dao.addColumn(SelectColumn.builder().setTableColumn(yearColumn).build());
		dao.setConditions(conditions);
		dao.fetch();
		
		assertEquals("SELECT `make`, `model`, `year` FROM `data_access_object` WHERE `make`='TOYOTA' AND `model`='HiAce' AND `year`='04/2006-ON';", dao.getLastQuery());
	}
}
