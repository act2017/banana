/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.database.table.ReferentialActions;

/**
 * To test {@link com.digsarustudio.banana.database.table.ReferentialActions}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ReferentialActionsTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ReferentialActions#getValue()}.
	 */
	@Test
	public void testGetValueOfCascade() {
		assertEquals("CASCADE", ReferentialActions.Cascade.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ReferentialActions#getValue()}.
	 */
	@Test
	public void testGetValueOfNoAction() {
		assertEquals("NO ACTION", ReferentialActions.NoAction.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ReferentialActions#getValue()}.
	 */
	@Test
	public void testGetValueOfRestrict() {
		assertEquals("RESTRICT", ReferentialActions.Restrict.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.table.ReferentialActions#getValue()}.
	 */
	@Test
	public void testGetValueOfSetNull() {
		assertEquals("SET NULL", ReferentialActions.SetNull.getValue());
	}
}
