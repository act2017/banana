/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.table.AbstractTable;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;
import com.digsarustudio.banana.database.table.TableManipulator;

/**
 * To test {@link QueryResult}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2
 */
@Ignore
public class MySQLQueryResultTest {
	DatabaseConnector connector = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		Table table = new AbstractTable("test_query") {
			
			@Override
			protected void initColumns() {
				
			}
		};
		
		table.addColumn(TableColumn.builder().setName("id")
											 .setDataType(TableColumnDataTypes.SmallInteger)
											 .setAutoIncrement()
											 .setAsPrimaryKey()
											 .setNonNullable()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("make")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("model")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build());
		
		table.addColumn(TableColumn.builder().setName("year")
											 .setDataType(TableColumnDataTypes.VarChar)
											 .setLength(32)
											 .setNullable()
											 .build());
		
		this.connector = new MySQLConnector();
		this.connector.setUserName("db_boss");
		this.connector.setPassword("1234");
		
		this.connector.connect();
		
		com.digsarustudio.banana.database.query.Query query = MySQLQuery.builder().setConnection(this.connector.getConnection()).build();
		
		TableManipulator manipulator = com.digsarustudio.banana.database.mysql.MySQLTableManipulator.builder().setPreventErrorIfTableExists()
																										 .setTable(table)
																										 .setDatabaseEngineType(DatabaseEngineType.InnoDB)
																										 .setQuery(query)
																										 .build();
		
		manipulator.createTable();		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
		this.connector.disconnect();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLQueryResult#next()}.
	 * @throws SQLException 
	 */
	@Test
	public void testNext() throws SQLException {
		com.digsarustudio.banana.database.query.Query query = MySQLQuery.builder().setConnection(this.connector.getConnection()).build();
		
		String queryString = "INSERT INTO `test_query` (`make`,`model`,`year`) VALUES ('HONDA', 'CIVIC', '03/2005-ON');";
		
		Integer rows = query.executeUpdate(queryString);
		
		assertEquals(new Integer(1), rows);
		
		queryString = "SELECT * FROM `test_query`;";
		
		QueryResult result = query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString(1));		
		assertEquals("HONDA", result.getString("make"));
		assertEquals("CIVIC", result.getString(2));
		assertEquals("CIVIC", result.getString("model"));
		assertEquals("03/2005-ON", result.getString(3));
		assertEquals("03/2005-ON", result.getString("year"));
		
		result.close();
		query.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLQueryResult#getString(java.lang.Integer)}.
	 * @throws SQLException 
	 */
	@Test
	public void testGetStringByIndex() throws SQLException {
		com.digsarustudio.banana.database.query.Query query = MySQLQuery.builder().setConnection(this.connector.getConnection()).build();
		
		String queryString = "INSERT INTO `test_query` (`make`,`model`,`year`) VALUES ('HONDA', 'CIVIC', '03/2005-ON');";
		
		Integer rows = query.executeUpdate(queryString);
		
		assertEquals(new Integer(1), rows);
		
		queryString = "SELECT * FROM `test_query`;";
		
		QueryResult result = query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString(1));		
		assertEquals("CIVIC", result.getString(2));
		assertEquals("03/2005-ON", result.getString(3));
		
		result.close();
		query.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLQueryResult#getString(java.lang.String)}.
	 * @throws SQLException 
	 */
	@Test
	public void testGetStringByColumnName() throws SQLException {
		com.digsarustudio.banana.database.query.Query query = MySQLQuery.builder().setConnection(this.connector.getConnection()).build();
		
		String queryString = "INSERT INTO `test_query` (`make`,`model`,`year`) VALUES ('HONDA', 'CIVIC', '03/2005-ON');";
		
		Integer rows = query.executeUpdate(queryString);
		
		assertEquals(new Integer(1), rows);
		
		queryString = "SELECT * FROM `test_query`;";
		
		QueryResult result = query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString("make"));
		assertEquals("CIVIC", result.getString("model"));
		assertEquals("03/2005-ON", result.getString("year"));
		
		result.close();
		query.close();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.database.mysql.MySQLQueryResult#close()}.
	 * @throws SQLException 
	 */
	@Test
	public void testClose() throws SQLException {
		com.digsarustudio.banana.database.query.Query query = MySQLQuery.builder().setConnection(this.connector.getConnection()).build();
		
		String queryString = "INSERT INTO `test_query` (`make`,`model`,`year`) VALUES ('HONDA', 'CIVIC', '03/2005-ON');";
		
		Integer rows = query.executeUpdate(queryString);
		
		assertEquals(new Integer(1), rows);
		
		queryString = "SELECT * FROM `test_query`;";
		
		QueryResult result = query.executeQuery(queryString);
		
		assertTrue(result.next());
		assertEquals("HONDA", result.getString(1));		
		assertEquals("CIVIC", result.getString(2));
		assertEquals("03/2005-ON", result.getString(3));
		
		result.close();
		query.close();
	}

}
