/**
 * 
 */
package com.digsarustudio.banana.encryption;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.encryption.EncryptionException;
import com.digsarustudio.banana.encryption.EncryptionTool;

/**
 * To test {@link EncryptionTool}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class EncryptionToolTest {
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#EncryptionTool()}.
	 */
	@Test
	public void testEncryptionTool() {
		new EncryptionTool();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#encryptBase64Password(java.lang.String)}.
	 * @throws EncryptionException 
	 * 
	 */
	@Test
	public void testEncryptBase64Password() throws EncryptionException {
		EncryptionTool tool = new EncryptionTool();
		String encrypted = tool.encryptBase64Password("123456");
		
		//The returned value is always changing.		
		assertNotNull(encrypted);
		assertFalse(encrypted.isEmpty());
		assertNotEquals(0, encrypted.length());
	}
	
	@Test(expected = EncryptionException.class)
	public void testDecryptNonBase64Password() throws EncryptionException {
		EncryptionTool tool = new EncryptionTool();
		tool.decryptBase64Password("13.34522-235");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#encryptBase64Password(java.lang.String, java.lang.String)}.
	 * @throws EncryptionException 
	 */
	@Test
	public void testEncryptBase64PasswordWithSalt() throws EncryptionException{
		EncryptionTool tool = new EncryptionTool();		
		String encrypted = tool.encryptBase64Password("123456", "dig-saru");		

		//The returned value is always changing.		
		assertNotNull(encrypted);
		assertFalse(encrypted.isEmpty());
		assertNotEquals(0, encrypted.length());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#decryptBase64Password(java.lang.String)}.
	 * @throws EncryptionException 
	 *  
	 */
	@Test
	public void testDecryptBase64Password() throws EncryptionException {
		EncryptionTool tool = new EncryptionTool();
		String decrypted = tool.decryptBase64Password("ioNtsmA2/PBDhqsmOdNr2w==:RHQgpePkcAGrl/v3VfAiDg");
		
		assertEquals("123456", decrypted);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#decryptBase64Password(java.lang.String, java.lang.String)}.
	 * @throws EncryptionException
	 */
	@Test
	public void testDecryptBase64PasswordWithSalt() throws EncryptionException {
		EncryptionTool tool = new EncryptionTool();
		String decrypted = tool.decryptBase64Password("NOm0T0CoJGEgChZ2792t1A==:0NxIioZN/zk6Uq6ab3xzww==", "dig-saru");
		
		assertEquals("123456", decrypted);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#encryptPassword(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testEncryptPasswordStringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#decryptPassword(java.lang.String, java.lang.String)}.
	 * @throws EncryptionException 
	 */
	@Test
	public void testDecryptPasswordWithSalt() throws EncryptionException {
		EncryptionTool tool = new EncryptionTool();
		
//		String decrypted = tool.decryptHexadecimalPassword("827498BEA59E1716A10211759E94C87C");
		String key = EncryptionTool.generateAESKey();
		String encrypted = tool.encryptHexadecimalPassword("1234", key);
		String decrypted = tool.decryptHexadecimalPassword(encrypted, key);
		assertEquals("1234", decrypted);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#generateSalt(java.lang.String)}.
	 */
	@Test
	public void testGenerateSalt() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#fromBase64StringToByte(java.lang.String)}.
	 */
	@Test
	public void testFromBase64StringToByte() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#fromBase64StringToString(java.lang.String)}.
	 */
	@Test
	public void testFromBase64StringToString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#toBase64String(byte[])}.
	 */
	@Test
	public void testToBase64StringByteArray() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#toBase64String(java.lang.String)}.
	 */
	@Test
	public void testToBase64StringString() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getAlgorithm()}.
	 */
	@Test
	public void testGetAlgorithm() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setAlgorithm(java.lang.String)}.
	 */
	@Test
	public void testSetAlgorithm() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getKeyFactory()}.
	 */
	@Test
	public void testGetKeyFactory() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setKeyFactory(java.lang.String)}.
	 */
	@Test
	public void testSetKeyFactory() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getIterationCount()}.
	 */
	@Test
	public void testGetIterationCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setIterationCount(java.lang.Integer)}.
	 */
	@Test
	public void testSetIterationCount() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getKeyLength()}.
	 */
	@Test
	public void testGetKeyLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setKeyLength(java.lang.Integer)}.
	 */
	@Test
	public void testSetKeyLength() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getKey()}.
	 */
	@Test
	public void testGetKey() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setKey(java.lang.String)}.
	 */
	@Test
	public void testSetKey() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getTransformation()}.
	 */
	@Test
	public void testGetTransformation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setTransformation(java.lang.String)}.
	 */
	@Test
	public void testSetTransformation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#getCharSet()}.
	 */
	@Test
	public void testGetCharSet() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#setCharSet(java.lang.String)}.
	 */
	@Test
	public void testSetCharSet() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.encryption.EncryptionTool#hashPassword(String, String)}.
	 * @throws EncryptionException 
	 */
	@Test
	public void testHashPassword() throws EncryptionException {
		String salt = "1A34F29B1A";		
		String password = "1234567";
		
		EncryptionTool tool = new EncryptionTool();
		String hashed = tool.hashPassword(salt, password);
		
		assertEquals("4000:1A34F29B1A:516FB9E731806D617F4DFCFCA0667F3F", hashed);
	}
}
