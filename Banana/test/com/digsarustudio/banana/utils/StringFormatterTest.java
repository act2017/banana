/**
 * 
 */
package com.digsarustudio.banana.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test {@link StringFormatter}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class StringFormatterTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormat1String() {
		String result = StringFormatter.format("This is %s", "one");
		
		assertEquals("This is one", result);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormat2String() {
		String result = StringFormatter.format("There are %s and %s", "1st string", "2nd string");
		
		assertEquals("There are 1st string and 2nd string", result);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormat3String() {
		String result = StringFormatter.format("There are %s, %s, and %s", "1st string", "2nd string", "3rd string");
		
		assertEquals("There are 1st string, 2nd string, and 3rd string", result);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormat1Integer() {
		String result = StringFormatter.format("This is %d", new Integer(155));
		
		assertEquals("This is 155", result);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormat2Integer() {
		String result = StringFormatter.format("Those are %d and %d", new Integer(155), new Integer(255));
		
		assertEquals("Those are 155 and 255", result);
	}



	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormatSQLCondition() {
		String result = StringFormatter.format("`%s`='%s'", "make", "WAHAHA");
		
		assertEquals("`make`='WAHAHA'", result);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#bytes2Hex(byte[])}.
	 */
	@Test
	public void testBytes2Hex() {
		byte[] bytes = new byte[6];
		bytes[0] = 1;
		bytes[1] = 9;
		bytes[2] = 15;
		bytes[3] = 64;
		bytes[4] = 12;
		bytes[5] = 127;
		
		assertEquals("01090F400C7F", StringFormatter.bytes2Hex(bytes));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#hex2Bytes(String)}.
	 */
	@Test
	public void testHex2Bytes() {
		byte[] bytes = StringFormatter.hex2Bytes("01090F400C7F");
		
		assertEquals(1, bytes[0]);
		assertEquals(9, bytes[1]);
		assertEquals(15, bytes[2]);
		assertEquals(64, bytes[3]);
		assertEquals(12, bytes[4]);
		assertEquals(127, bytes[5]);
	}
	
	@Test
	public void testBigBytes2Hex() {
		byte[] bytes = new byte[6];
		bytes[0] = -1;
		bytes[1] = 9;
		bytes[2] = 15;
		bytes[3] = 64;
		bytes[4] = 12;
		bytes[5] = 127;
		
		assertEquals("FF090F400C7F", StringFormatter.bytes2Hex(bytes));
	}
	
	@Test
	public void testBigHex2Bytes() {
		byte[] bytes = StringFormatter.hex2Bytes("01090F400CFF");
		
		assertEquals(1, bytes[0]);
		assertEquals(9, bytes[1]);
		assertEquals(15, bytes[2]);
		assertEquals(64, bytes[3]);
		assertEquals(12, bytes[4]);
		assertEquals(-1, bytes[5]);
	}
}
