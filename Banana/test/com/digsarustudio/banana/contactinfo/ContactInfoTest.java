/**
 * 
 */
package com.digsarustudio.banana.contactinfo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.PostalAddress;
import com.digsarustudio.banana.contactinfo.address.ResidentialAddress;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.AustralianStates;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.BrisbaneSuburbs;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;
import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * To test Contact info object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class ContactInfoTest {
	ContactInfo target;
	ResidentialAddress residentialAddress;
	PostalAddress postalAddress;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.target = new ContactInfo();
		
		this.residentialAddress = new ResidentialAddress.Builder(Countries.Australia)
														.setState(AustralianStates.Queensland)
														.setCity(QueenslandLGAs.Brisbane)
														.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														.setStreetName("Mary")
														.setStreetType(StreetType.Street)
														.setPropertyNumber("223-225")
														.setPropertyType(BuildingType.Flat)
														.setLevel(10)
														.setLevelType(LevelType.Floor)
														.build();
		
		this.postalAddress = new PostalAddress.Builder(Countries.Australia)
											  .setState(AustralianStates.Queensland)
											  .setCity(QueenslandLGAs.Brisbane)
											  .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
											  .setPoBoxNumber("1223344")
											  .setDeliveryType(PostalDeliveryType.PostOfficeBox)
											  .build();
		
		this.target.setResidentialAddress(this.residentialAddress);
		this.target.setPostalAddress(this.postalAddress);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		assertEquals(Countries.Australia, this.target.getResidentialAddress().getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#ContactInfo()}.
	 */
	@Test @Ignore
	public void testContactInfo() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#getResidentialAddress()}.
	 */
	@Test @Ignore
	public void testGetResidentialAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#setResidentialAddress(com.digsarustudio.banana.contactinfo.address.ResidentialAddress)}.
	 */
	@Test @Ignore
	public void testSetResidentialAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#getPostalAddress()}.
	 */
	@Test @Ignore
	public void testGetPostalAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#setPostalAddress(com.digsarustudio.banana.contactinfo.address.PostalAddress)}.
	 */
	@Test @Ignore
	public void testSetPostalAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#getTelephoneNumber()}.
	 */
	@Test @Ignore
	public void testGetTelephoneNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#setTelephoneNumber(com.digsarustudio.banana.contactinfo.phone.PhoneNumber)}.
	 */
	@Test @Ignore
	public void testSetTelephoneNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#getMobileNumber()}.
	 */
	@Test @Ignore
	public void testGetMobileNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#setMobileNumber(com.digsarustudio.banana.contactinfo.phone.PhoneNumber)}.
	 */
	@Test @Ignore
	public void testSetMobileNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#getFaxNumber()}.
	 */
	@Test @Ignore
	public void testGetFaxNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#setFaxNumber(com.digsarustudio.banana.contactinfo.phone.PhoneNumber)}.
	 */
	@Test @Ignore
	public void testSetFaxNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#geteMailAddress()}.
	 */
	@Test @Ignore
	public void testGeteMailAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#seteMailAddress(com.digsarustudio.banana.contactinfo.EMailAddress)}.
	 */
	@Test @Ignore
	public void testSeteMailAddress() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.ContactInfo#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

}
