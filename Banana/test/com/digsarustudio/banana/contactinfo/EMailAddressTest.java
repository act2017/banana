/**
 * 
 */
package com.digsarustudio.banana.contactinfo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test the e-mail address object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class EMailAddressTest {
	
	EmailAddress target;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
//		this.target = new EmailAddress("omama@digsarustudio.com");
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeSameObject() {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target;
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeSameValue() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com");
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertEquals(this.target.hashCode(), address.hashCode());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeSameAccount() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com.au");
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeSameDomain() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama1@digsarustudio.com");
//		assertEquals("omama1", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama1@digsarustudio.com", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeDiff() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama1@digsarustudio.com.au");
//		assertEquals("omama1", address.getAccount());
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama1@digsarustudio.com.au", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeAccountModifiedAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setAccount("ulala");
//		assertEquals("ulala", address.getAccount());
//		assertEquals("ulala@digsarustudio.com", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testHashCodeDomainModifiedAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testHashCodeAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testHashCodeAccountModifiedAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setAccount("ulala");
//		assertEquals("ulala", address.getAccount());
//		assertEquals("ulala@digsarustudio.com", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#hashCode()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testHashCodeDomainModifiedAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertNotEquals(this.target.hashCode(), address.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#EMailAddress()}.
//	 */
//	@SuppressWarnings("unused")
//	@Test
//	public void testEMailAddress() {		
//		EmailAddress address = new EmailAddress();
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#EMailAddress(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@SuppressWarnings("unused")
//	@Test
//	public void testEMailAddressString() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com.au");
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#EMailAddress()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertEquals(this.target, address);
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#EMailAddress()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testAccountModifiedAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setAccount("ulala");
//		assertEquals("ulala", address.getAccount());
//		assertEquals("ulala@digsarustudio.com", address.getAddress());
//		
//		assertNotEquals(this.target, address);
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#EMailAddress()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testDomainModifiedAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertNotEquals(this.target, address);
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#setAddress(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testSetAddress() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAddress());
//		
//		address.setAddress("omama@digsarustudio.com.au");
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#getAddress()}.
//	 */
//	@Test
//	public void testGetAddress() {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAddress());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#getAddress()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testGetAddressAfterConstructedWithAddress() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com.au");
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#getAccount()}.
//	 */
//	@Test
//	public void testGetAccount() {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAccount());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#getAccount()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testGetAccountAfterConstructedWithAddress() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com.au");
//		assertEquals("omama", address.getAccount());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#setAccount(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testSetAccount() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAccount());
//		
//		address.setAccount("omama");
//		assertEquals("omama", address.getAccount());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#setAccount(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testSetAccountContainsDot() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAccount());
//		
//		address.setAccount("omama.cc");
//		assertEquals("omama.cc", address.getAccount());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#setAccount(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test(expected = EMailAddressFormatException.class)
//	public void testSetAccountContainsDollarSign() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAccount());
//		
//		address.setAccount("omama$cc");
//		assertEquals("omama$cc", address.getAccount());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#setAccount(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test(expected = EMailAddressFormatException.class)
//	public void testSetAccountContainsBraket() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getAccount());
//		
//		address.setAccount("omama(cc)");
//		assertEquals("omama$cc", address.getAccount());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#getDomain()}.
//	 */
//	@Test
//	public void testGetDomain() {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getDomain());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#getDomain()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testGetDomainWhenConstructedWithAddress() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#setDomain(java.lang.String)}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testSetDomain() throws EMailAddressFormatException {
//		EmailAddress address = new EmailAddress();
//		assertNull(address.getDomain());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 */
//	@Test
//	public void testEqualsSameObject() {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target;
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertTrue(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsSameValue() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com");
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertTrue(this.target.equals(address));
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsSameAccount() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama@digsarustudio.com.au");
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsSameDomain() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama1@digsarustudio.com");
//		assertEquals("omama1", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama1@digsarustudio.com", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsDiff() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress("omama1@digsarustudio.com.au");
//		assertEquals("omama1", address.getAccount());
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama1@digsarustudio.com.au", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertTrue(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsAccountModifiedAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setAccount("ulala");
//		assertEquals("ulala", address.getAccount());
//		assertEquals("ulala@digsarustudio.com", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 */
//	@Test
//	public void testEqualsDomainModifiedAfterCopy() throws EMailAddressFormatException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = new EmailAddress(this.target);
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testEqualsAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertTrue(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testEqualsAccountModifiedAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setAccount("ulala");
//		assertEquals("ulala", address.getAccount());
//		assertEquals("ulala@digsarustudio.com", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#equals()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testEqualsDomainModifiedAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertFalse(this.target.equals(address));
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#clone()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		assertEquals(this.target, address);
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#clone()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testAccountModifiedAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setAccount("ulala");
//		assertEquals("ulala", address.getAccount());
//		assertEquals("ulala@digsarustudio.com", address.getAddress());
//		
//		assertNotEquals(this.target, address);
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.EmailAddress#clone()}.
//	 * @throws EMailAddressFormatException 
//	 * @throws CloneNotSupportedException 
//	 */
//	@Test
//	public void testDomainModifiedAfterClone() throws EMailAddressFormatException, CloneNotSupportedException {
//		assertEquals("omama", this.target.getAccount());
//		assertEquals("digsarustudio.com", this.target.getDomain());
//		assertEquals("omama@digsarustudio.com", this.target.getAddress());
//		
//		EmailAddress address = this.target.clone();
//		assertEquals("omama", address.getAccount());
//		assertEquals("digsarustudio.com", address.getDomain());
//		assertEquals("omama@digsarustudio.com", address.getAddress());
//		
//		address.setDomain("digsarustudio.com.au");
//		assertEquals("digsarustudio.com.au", address.getDomain());
//		assertEquals("omama@digsarustudio.com.au", address.getAddress());
//		
//		assertNotEquals(this.target, address);
//	}

}
