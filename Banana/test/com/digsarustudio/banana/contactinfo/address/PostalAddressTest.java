/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.PostalAddress;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.CityNotSetException;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.StateNotSetException;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.AustralianStates;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.BrisbaneSuburbs;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;
import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.ParcelOfLandType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * To test the postal address object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PostalAddressTest {
	PostalAddress postal;
	PostalAddress residential;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.postal = new PostalAddress.Builder(Countries.Australia)
									   .setState(AustralianStates.Queensland)
									   .setCity(QueenslandLGAs.Brisbane)
									   .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
									   .setDeliveryType(PostalDeliveryType.PostOfficeBox)
									   .setPoBoxNumber("3341")								   
									   .build();
		
		this.residential = new PostalAddress.Builder(Countries.Australia)
											.setState(AustralianStates.Queensland)
											.setCity(QueenslandLGAs.Brisbane)
											.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
											.setStreetName("Mary")
											.setStreetType(StreetType.Street)
											.setStreetSuffix("W")
											.setPropertyNumber("33-44")
											.setPropertyType(BuildingType.Flat)
											.setBuildingNumber("3")
											.setLevel(10)
											.setLevelType(LevelType.Floor)
											.build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCodeSameObject() {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address = this.postal;
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameValue() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Australia)
												  .setState(AustralianStates.Queensland)
												  .setCity(QueenslandLGAs.Brisbane)
												  .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												  .setDeliveryType(PostalDeliveryType.PostOfficeBox)
									   .setPoBoxNumber("3341")
												  .build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameCountry() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Australia)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameState() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Queensland)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameCity() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Brisbane)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameSuburb() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSamePOBoxNumber() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("3341")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSamePostalDeliveryType() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.PostOfficeBox)
									   .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeDiff() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterCopy() throws CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setSuburb(BrisbaneSuburbs.AcaciaRidge);;
		assertEquals(BrisbaneSuburbs.AcaciaRidge, address.getSuburb());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePOBoxNumberModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPOBoxNumber("123");
		assertEquals("123", address.getPOBoxNumber());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePostalDeliveryTypeModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterClone() throws StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterClone() throws CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setSuburb(BrisbaneSuburbs.AcaciaRidge);;
		assertEquals(BrisbaneSuburbs.AcaciaRidge, address.getSuburb());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodePOBoxNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPOBoxNumber("123");
		assertEquals("123", address.getPOBoxNumber());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodePostalDeliveryTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getCountry()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCountry() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)											
												 .build();
		
		assertEquals(Countries.Australia, address.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCountry() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)											
												 .build();

		assertEquals(Countries.Australia, address.getCountry());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getState()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetState() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();
		
		assertNull(address.getState());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getState()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testGetStateWhenBuilt() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .build();
		
		assertEquals(AustralianStates.Queensland, address.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetState() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				 								 .build();

		assertNull(address.getState());
		
		address.setState(AustralianStates.Queensland);
		assertEquals(AustralianStates.Queensland, address.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getCity()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCity() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();

		assertNull(address.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getCity()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCityWhenBuilt() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setCity(QueenslandLGAs.Brisbane)
												 .build();

		assertNull(address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getCity()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetCityWhenBuiltWithState() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .setCity(QueenslandLGAs.Brisbane)
												 .build();

		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCity() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();
		
		address.setCity(QueenslandLGAs.Brisbane);
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetCityWhenStateSetAtBuilt() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .build();
		
		address.setCity(QueenslandLGAs.Brisbane);
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCityWhenStateSetAfterBuilt() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();
		
		address.setState(AustralianStates.Queensland);
		address.setCity(QueenslandLGAs.Brisbane);		
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getSuburb()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetSuburb() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();
		
		assertNull(address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getSuburb()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetSuburbWhenBuilt() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												 .build();
		
		assertNull(address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getSuburb()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetSuburbWhenBuiltWithCity() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .setCity(QueenslandLGAs.Brisbane)
												 .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												 .build();
		
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetSuburb() throws CityNotSetException, StateNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();
								
		address.setSuburb(BrisbaneSuburbs.BrisbaneCBD);
		assertNull(address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburbWhenCitySetAtBuilt() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .setCity(QueenslandLGAs.Brisbane)
												 .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												 .build();
								
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburbWhenCitySetAfterBuilt() throws CityNotSetException, StateNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .setCity(QueenslandLGAs.Brisbane)
												 .build();
								
		address.setSuburb(BrisbaneSuburbs.BrisbaneCBD);
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPostcode() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .build();
								
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetPostcodeAfterTheLocalDivisionSet() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setState(AustralianStates.Queensland)
												 .setCity(QueenslandLGAs.Cairns)
												 .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												 .build();
								
		assertEquals("4000", address.getPostcode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPOBoxNumber()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPOBoxNumber() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setDeliveryType(PostalDeliveryType.PostOfficeBox)
									   .setPoBoxNumber("4311")
												 .build();
		
		assertEquals("4311", address.getPOBoxNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setPOBoxNumber(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPOBoxNumber() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setDeliveryType(PostalDeliveryType.PostOfficeBox)
									   .setPoBoxNumber("4311")
												 .build();
		
		assertEquals("4311", address.getPOBoxNumber());
		
		address.setPOBoxNumber("12345");
		assertEquals("12345", address.getPOBoxNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostalDeliveryType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPostalDeliveryType() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setDeliveryType(PostalDeliveryType.PostOfficeBox)
									   .setPoBoxNumber("4311")
												 .build();
		
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPostalDeliveryType() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
												 .setDeliveryType(PostalDeliveryType.PostOfficeBox)
												 .setPoBoxNumber("4311")
												 .build();
		
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 */
	@Test @Ignore
	public void testEqualsSameObject() {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address = this.postal;
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertTrue(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameValue() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Australia)
												  .setState(AustralianStates.Queensland)
												  .setCity(QueenslandLGAs.Brisbane)
												  .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												  .setDeliveryType(PostalDeliveryType.PostOfficeBox)
												  .setPoBoxNumber("3341")
												  .build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertTrue(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameCountry() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Australia)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameState() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Queensland)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameCity() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Brisbane)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameSuburb() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSamePOBoxNumber() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("3341")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSamePostalDeliveryType() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.PostOfficeBox)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsDiff() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress.Builder(Countries.Taiwan)
												  .setState(AustralianStates.Victoria)
												  .setCity(QueenslandLGAs.Aurukun)
												  .setSuburb(BrisbaneSuburbs.Indooroopilly)
												  .setDeliveryType(PostalDeliveryType.CareOfPostOffice)
												  .setPoBoxNumber("123")
												  .build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, address.getSuburb());
		assertEquals("123", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertTrue(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStateModifedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterCopy() throws CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setSuburb(BrisbaneSuburbs.AcaciaRidge);;
		assertEquals(BrisbaneSuburbs.AcaciaRidge, address.getSuburb());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPOBoxNumberModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPOBoxNumber("123");
		assertEquals("123", address.getPOBoxNumber());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPostalDeliveryTypeModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertTrue(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsStateModifedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterClone() throws StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterClone() throws CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setSuburb(BrisbaneSuburbs.AcaciaRidge);;
		assertEquals(BrisbaneSuburbs.AcaciaRidge, address.getSuburb());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsPOBoxNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPOBoxNumber("123");
		assertEquals("123", address.getPOBoxNumber());
		
		assertFalse(this.postal.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsPostalDeliveryTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertFalse(this.postal.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCountryModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testStateModifedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCityModifiedAfterClone() throws StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testSuburbModifiedAfterClone() throws CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setSuburb(BrisbaneSuburbs.AcaciaRidge);;
		assertEquals(BrisbaneSuburbs.AcaciaRidge, address.getSuburb());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testPOBoxNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPOBoxNumber("123");
		assertEquals("123", address.getPOBoxNumber());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testPostalDeliveryTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  this.postal.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		assertEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testCountryModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testStateModifedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testCityModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSuburbModifiedAfterCopy() throws CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setSuburb(BrisbaneSuburbs.AcaciaRidge);;
		assertEquals(BrisbaneSuburbs.AcaciaRidge, address.getSuburb());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testPOBoxNumberModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPOBoxNumber("123");
		assertEquals("123", address.getPOBoxNumber());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testPostalDeliveryTypeModifiedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.postal.getCountry());
		assertEquals(AustralianStates.Queensland, this.postal.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.postal.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.postal.getSuburb());
		assertEquals("3341", this.postal.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.postal.getPostalDeliveryType());
		
		PostalAddress address =  new PostalAddress(this.postal);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		assertEquals("3341", address.getPOBoxNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, address.getPostalDeliveryType());
		
		address.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
		assertEquals(PostalDeliveryType.CareOfPostOffice, address.getPostalDeliveryType());
		
		assertNotEquals(this.postal, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCodeSameObjectResidentail() {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential;
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameValueResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("Mary")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("W")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("3")
															.setLevel(10)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameCountryResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStateResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameCityResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameSuburbResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStreetNameResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("Mary")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStreetTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStreetSuffixResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("W")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSamePropertyNumberResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSamePropertyTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameBuildingNumberResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("3")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameLevelResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(10)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameLevelTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.residential.getCountry(), address.getCountry());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.residential.getState(), address.getState());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.residential.getCity(), address.getCity());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.residential.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetNameModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.residential.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.residential.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetSuffixModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.residential.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyNumberModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.residential.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.residential.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeBuildingNumberModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.residential.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.residential.getLevel(), address.getLevel());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.residential.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.residential.getCountry(), address.getCountry());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.residential.getState(), address.getState());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.residential.getCity(), address.getCity());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.residential.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetNameModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.residential.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.residential.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetSuffixModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.residential.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyNumberModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.residential.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.residential.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeBuildingNumberModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.residential.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.residential.getLevel(), address.getLevel());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.residential.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.residential.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testLotAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress lot = new PostalAddress.Builder(Countries.Australia)
														.setState(AustralianStates.Queensland)
														.setCity(QueenslandLGAs.Brisbane)
														.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														.setStreetName("Mary")
														.setStreetType(StreetType.Street)
														.setStreetSuffix("W")
														.setPropertyNumber("33-44")
														.setPropertyType(ParcelOfLandType.Lot)														
														.build();
		assertEquals(Countries.Australia, lot.getCountry());
		assertEquals(AustralianStates.Queensland, lot.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot.getSuburb());
		
		assertEquals("Mary", lot.getStreetName());
		assertEquals(StreetType.Street, lot.getStreetType());
		assertEquals("W", lot.getStreetSuffix());
		assertEquals("33-44", lot.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot.getPropertyType());


		PostalAddress lot2 = new PostalAddress(lot);
		
		assertEquals(Countries.Australia, lot2.getCountry());
		assertEquals(AustralianStates.Queensland, lot2.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot2.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot2.getSuburb());
		
		assertEquals("Mary", lot2.getStreetName());
		assertEquals(StreetType.Street, lot2.getStreetType());
		assertEquals("W", lot2.getStreetSuffix());
		assertEquals("33-44", lot2.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot2.getPropertyType());
		
		assertEquals(lot, lot2);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testCountryModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.residential.getCountry(), address.getCountry());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testStateModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.residential.getState(), address.getState());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testCityModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.residential.getCity(), address.getCity());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testSuburbModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.residential.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetNameModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.residential.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.residential.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetSuffixModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.residential.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyNumberModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.residential.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.residential.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testBuildingNumberModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.residential.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.residential.getLevel(), address.getLevel());
		
		assertNotEquals(this.residential, address);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.residential.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.residential, address);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getCountry()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCountryResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia).build();
		assertEquals(Countries.Australia, address.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCountryResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia).build();
		assertEquals(Countries.Australia, address.getCountry());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getState()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStateResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStateResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getState());
		
		address.setState(AustralianStates.Queensland);
		assertEquals(AustralianStates.Queensland, address.getState());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetStateWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .build();
		
		assertEquals(AustralianStates.Queensland, address.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getCity()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCityResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetCityResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .build();
		
		assertNull(address.getCity());
		
		address.setCity(QueenslandLGAs.Brisbane);
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCityWithoutStateResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		
		assertNull(address.getCity());
		
		address.setCity(QueenslandLGAs.Brisbane);
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testSetCityWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .setCity(QueenslandLGAs.Brisbane)
														   .build();
		
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getSuburb()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetSuburbResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburbResidentail() throws CityNotSetException, StateNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .setCity(QueenslandLGAs.Brisbane)
														   .build();
		
		assertNull(address.getSuburb());
		
		address.setSuburb(BrisbaneSuburbs.BrisbaneCBD);
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetSuburbWithoutCityResidentail() throws CityNotSetException, StateNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .build();
		
		assertNull(address.getSuburb());
		
		address.setSuburb(BrisbaneSuburbs.BrisbaneCBD);
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburbWhenBuiltResidentail() throws CityNotSetException, StateNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .setCity(QueenslandLGAs.Brisbane)
														   .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														   .build();
		
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPostcodeResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testGetPostcodeWhenStateSetResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .setState(AustralianStates.Queensland)
				   										   .build();
		
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetPostcodeWhenCitySetResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .setState(AustralianStates.Queensland)
				   										   .setCity(QueenslandLGAs.Brisbane)
				   										   .build();
		
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetPostcodeWhenSuburbSetResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .setState(AustralianStates.Queensland)
				   										   .setCity(QueenslandLGAs.Brisbane)
				   										   .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				   										   .build();
		
		assertEquals("4000", address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getStreetName()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStreetNameResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setStreetName(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetNameResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
						   								   .build();
		assertNull(address.getStreetName());
		
		address.setStreetName("Mary");
		assertEquals("Mary", address.getStreetName());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setStreetName(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetNameWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
						   								   .setStreetName("Mary")
						   								   .build();
		assertEquals("Mary", address.getStreetName());
	}	

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getStreetType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStreetTypeResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetTypeResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetType());
		
		address.setStreetType(StreetType.Alley);
		assertEquals(StreetType.Alley, address.getStreetType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetTypeWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .setStreetType(StreetType.Alley)
				   										   .build();
		assertEquals(StreetType.Alley, address.getStreetType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getStreetSuffix()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStreetSuffixResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setStreetSuffix(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetSuffixResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetSuffix());
		
		address.setStreetSuffix("W");
		assertEquals("W", address.getStreetSuffix());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setStreetSuffix(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetSuffixWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .setStreetSuffix("W")
				   										   .build();
		assertEquals("W", address.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPropertyNumber()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPropertyNumberResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getPropertyNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setPropertyNumber(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyNumberResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getPropertyNumber());
		
		address.setPropertyNumber("22-33");
		assertEquals("22-33", address.getPropertyNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setPropertyNumber(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyNumberWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setPropertyNumber("22-33")
														   .build();
		assertEquals("22-33", address.getPropertyNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getPropertyType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPropertyTypeResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getPropertyType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyTypeResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getPropertyType());
		
		address.setPropertyType(BuildingType.Apartment);
		assertEquals(BuildingType.Apartment, address.getPropertyType());
		
		address.setPropertyType(ParcelOfLandType.Section);
		assertEquals(ParcelOfLandType.Section, address.getPropertyType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyTypeWhenBuiltResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setPropertyType(BuildingType.Apartment)
														   .build();
		assertEquals(BuildingType.Apartment, address.getPropertyType());
		
		PostalAddress address2 = new PostalAddress.Builder(Countries.Australia)
				   										    .setPropertyType(ParcelOfLandType.Lot)
				   										    .build();
		assertEquals(ParcelOfLandType.Lot, address2.getPropertyType());
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getBuildingNumber()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetBuildingNumberResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getBuildingNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetBuildingNumberResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getBuildingNumber());
		
		address.setBuildingNumber("33");
		assertEquals("33", address.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetBuildingNumberWhenBuiltResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				   										   .setBuildingNumber("33")
				   										   .build();
		assertEquals("33", address.getBuildingNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getLevel()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetLevelResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														  .build();
		assertNull(address.getLevel());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getLevel());
		
		address.setLevel(10);
		assertEquals(new Integer(10), address.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelWhenBuiltResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
														   .setLevel(10)
														   .build();

		assertEquals(new Integer(10), address.getLevel());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#getLevelType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetLevelTypeResidentail() throws StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				  										   .build();
		assertNull(address.getLevelType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				  										   .build();
		assertNull(address.getLevel());
		
		address.setLevelType(LevelType.Basement);
		assertEquals(LevelType.Basement, address.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelTypeWhenBuiltResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
				  										   .setLevelType(LevelType.Basement)
				  										   .build();
		assertEquals(LevelType.Basement, address.getLevelType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 */
	@Test @Ignore
	public void testEqualsSameObjectResidentail() {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential;
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameValueResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("Mary")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("W")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("3")
															.setLevel(10)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameCountryResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStateResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameCityResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameSuburbResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStreetNameResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("Mary")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStreetTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStreetSuffixResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("W")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSamePropertyNumberResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSamePropertyTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameBuildingNumberResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("3")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameLevelResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(10)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameLevelTypeResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.residential.getCountry(), address.getCountry());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsStateModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.residential.getState(), address.getState());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.residential.getCity(), address.getCity());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.residential.getSuburb(), address.getSuburb());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetNameModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.residential.getStreetName(), address.getStreetName());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.residential.getStreetType(), address.getStreetType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetSuffixModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.residential.getStreetSuffix(), address.getStreetSuffix());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyNumberModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.residential.getPropertyNumber(), address.getPropertyNumber());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.residential.getPropertyType(), address.getPropertyType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsBuildingNumberModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.residential.getBuildingNumber(), address.getBuildingNumber());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.residential.getLevel(), address.getLevel());
		
		assertFalse(this.residential.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelTypeModifiedAfterCopyResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = new PostalAddress(this.residential);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.residential.getLevelType(), address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.residential.getCountry(), address.getCountry());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsStateModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.residential.getState(), address.getState());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.residential.getCity(), address.getCity());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.residential.getSuburb(), address.getSuburb());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetNameModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.residential.getStreetName(), address.getStreetName());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.residential.getStreetType(), address.getStreetType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetSuffixModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.residential.getStreetSuffix(), address.getStreetSuffix());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyNumberModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.residential.getPropertyNumber(), address.getPropertyNumber());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.residential.getPropertyType(), address.getPropertyType());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsBuildingNumberModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.residential.getBuildingNumber(), address.getBuildingNumber());
		
		assertFalse(this.residential.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.residential.getLevel(), address.getLevel());
		
		assertFalse(this.residential.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.residential.getLevelType(), address.getLevelType());
		
		assertFalse(this.residential.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#PostalAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testLotAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException, StateNotSetException, CityNotSetException {
		PostalAddress lot = new PostalAddress.Builder(Countries.Australia)
														.setState(AustralianStates.Queensland)
														.setCity(QueenslandLGAs.Brisbane)
														.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														.setStreetName("Mary")
														.setStreetType(StreetType.Street)
														.setStreetSuffix("W")
														.setPropertyNumber("33-44")
														.setPropertyType(ParcelOfLandType.Lot)														
														.build();
		assertEquals(Countries.Australia, lot.getCountry());
		assertEquals(AustralianStates.Queensland, lot.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot.getSuburb());
		
		assertEquals("Mary", lot.getStreetName());
		assertEquals(StreetType.Street, lot.getStreetType());
		assertEquals("W", lot.getStreetSuffix());
		assertEquals("33-44", lot.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot.getPropertyType());


		PostalAddress lot2 = lot.clone();
		
		assertEquals(Countries.Australia, lot2.getCountry());
		assertEquals(AustralianStates.Queensland, lot2.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot2.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot2.getSuburb());
		
		assertEquals("Mary", lot2.getStreetName());
		assertEquals(StreetType.Street, lot2.getStreetType());
		assertEquals("W", lot2.getStreetSuffix());
		assertEquals("33-44", lot2.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot2.getPropertyType());
		
		assertEquals(lot, lot2);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCountryModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.residential.getCountry(), address.getCountry());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testStateModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.residential.getState(), address.getState());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCityModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.residential.getCity(), address.getCity());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testSuburbModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.residential.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetNameModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.residential.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.residential.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetSuffixModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.residential.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyNumberModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.residential.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.residential.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testBuildingNumberModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.residential.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.residential, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.residential.getLevel(), address.getLevel());
		
		assertNotEquals(this.residential, address);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.PostalAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelTypeModifiedAfterCloneResidentail() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.residential.getCountry());
		assertEquals(AustralianStates.Queensland, this.residential.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.residential.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.residential.getSuburb());
		
		assertEquals("Mary", this.residential.getStreetName());
		assertEquals(StreetType.Street, this.residential.getStreetType());
		assertEquals("W", this.residential.getStreetSuffix());
		assertEquals("33-44", this.residential.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.residential.getPropertyType());
		assertEquals("3", this.residential.getBuildingNumber());
		assertEquals(new Integer(10), this.residential.getLevel());
		assertEquals(LevelType.Floor, this.residential.getLevelType());

		PostalAddress address = this.residential.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.residential.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.residential, address);
	}

}
