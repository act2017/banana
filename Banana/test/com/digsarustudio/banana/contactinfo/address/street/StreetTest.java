/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.street;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * To test the functionalities of Street
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class StreetTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#Street(com.digsarustudio.banana.contactinfo.address.street.Street)}.
	 */
	@Test @Ignore
	public void testStreet() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#getName()}.
	 */
	@Test @Ignore
	public void testGetName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#setName(java.lang.String)}.
	 */
	@Test @Ignore
	public void testSetName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#getType()}.
	 */
	@Test @Ignore
	public void testGetType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#setType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 */
	@Test @Ignore
	public void testSetType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#getSuffix()}.
	 */
	@Test @Ignore
	public void testGetSuffix() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#setSuffix(java.lang.String)}.
	 */
	@Test @Ignore
	public void testSetSuffix() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.street.Street#clone()}.
	 */
	@Test @Ignore
	public void testClone() {
		fail("Not yet implemented");
	}

}
