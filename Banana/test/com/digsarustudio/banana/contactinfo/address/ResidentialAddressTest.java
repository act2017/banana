/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.ResidentialAddress;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.CityNotSetException;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.StateNotSetException;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.AustralianStates;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.BrisbaneSuburbs;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;
import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.ParcelOfLandType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * To test residential address object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class ResidentialAddressTest {
	private ResidentialAddress target = null;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.target = new ResidentialAddress.Builder(Countries.Australia)
											.setState(AustralianStates.Queensland)
											.setCity(QueenslandLGAs.Brisbane)
											.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
											.setStreetName("Mary")
											.setStreetType(StreetType.Street)
											.setStreetSuffix("W")
											.setPropertyNumber("33-44")
											.setPropertyType(BuildingType.Flat)
											.setBuildingNumber("3")
											.setLevel(10)
											.setLevelType(LevelType.Floor)
											.build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCodeSameObject() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target;
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameValue() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("Mary")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("W")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("3")
															.setLevel(10)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameCountry() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameState() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameCity() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameSuburb() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStreetName() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("Mary")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStreetType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameStreetSuffix() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("W")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSamePropertyNumber() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSamePropertyType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameBuildingNumber() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("3")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameLevel() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(10)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeSameLevelType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.target.getCountry(), address.getCountry());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.target.getState(), address.getState());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.target.getCity(), address.getCity());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterCopy() throws PropertyTypeNotMatchException, CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.target.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetNameModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.target.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.target.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetSuffixModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.target.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyNumberModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.target.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.target.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeBuildingNumberModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.target.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.target.getLevel(), address.getLevel());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.target.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.target.getCountry(), address.getCountry());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.target.getState(), address.getState());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterClone() throws PropertyTypeNotMatchException, StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.target.getCity(), address.getCity());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterClone() throws PropertyTypeNotMatchException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.target.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetNameModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.target.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.target.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeStreetSuffixModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.target.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyNumberModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.target.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodePropertyTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.target.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeBuildingNumberModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.target.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.target.getLevel(), address.getLevel());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeLevelTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.target.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.target.hashCode(), address.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testLotAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress lot = new ResidentialAddress.Builder(Countries.Australia)
														.setState(AustralianStates.Queensland)
														.setCity(QueenslandLGAs.Brisbane)
														.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														.setStreetName("Mary")
														.setStreetType(StreetType.Street)
														.setStreetSuffix("W")
														.setPropertyNumber("33-44")
														.setPropertyType(ParcelOfLandType.Lot)														
														.build();
		assertEquals(Countries.Australia, lot.getCountry());
		assertEquals(AustralianStates.Queensland, lot.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot.getSuburb());
		
		assertEquals("Mary", lot.getStreetName());
		assertEquals(StreetType.Street, lot.getStreetType());
		assertEquals("W", lot.getStreetSuffix());
		assertEquals("33-44", lot.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot.getPropertyType());


		ResidentialAddress lot2 = new ResidentialAddress(lot);
		
		assertEquals(Countries.Australia, lot2.getCountry());
		assertEquals(AustralianStates.Queensland, lot2.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot2.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot2.getSuburb());
		
		assertEquals("Mary", lot2.getStreetName());
		assertEquals(StreetType.Street, lot2.getStreetType());
		assertEquals("W", lot2.getStreetSuffix());
		assertEquals("33-44", lot2.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot2.getPropertyType());
		
		assertEquals(lot, lot2);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testCountryModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.target.getCountry(), address.getCountry());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testStateModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.target.getState(), address.getState());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testCityModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.target.getCity(), address.getCity());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testSuburbModifiedAfterCopy() throws PropertyTypeNotMatchException, CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.target.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetNameModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.target.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.target.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetSuffixModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.target.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyNumberModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.target.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.target.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testBuildingNumberModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.target.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.target.getLevel(), address.getLevel());
		
		assertNotEquals(this.target, address);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.target.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.target, address);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getCountry()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCountry() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia).build();
		assertEquals(Countries.Australia, address.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCountry() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia).build();
		assertEquals(Countries.Australia, address.getCountry());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getState()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetState() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetState() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getState());
		
		address.setState(AustralianStates.Queensland);
		assertEquals(AustralianStates.Queensland, address.getState());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetStateWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .build();
		
		assertEquals(AustralianStates.Queensland, address.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getCity()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetCity() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetCity() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .build();
		
		assertNull(address.getCity());
		
		address.setCity(QueenslandLGAs.Brisbane);
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetCityWithoutState() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		
		assertNull(address.getCity());
		
		address.setCity(QueenslandLGAs.Brisbane);
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testSetCityWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .setCity(QueenslandLGAs.Brisbane)
														   .build();
		
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getSuburb()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetSuburb() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia).build();
		
		assertNull(address.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburb() throws CityNotSetException, StateNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .setCity(QueenslandLGAs.Brisbane)
														   .build();
		
		assertNull(address.getSuburb());
		
		address.setSuburb(BrisbaneSuburbs.BrisbaneCBD);
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testSetSuburbWithoutCity() throws CityNotSetException, StateNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .build();
		
		assertNull(address.getSuburb());
		
		address.setSuburb(BrisbaneSuburbs.BrisbaneCBD);
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburbWhenBuilt() throws CityNotSetException, StateNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setState(AustralianStates.Queensland)
														   .setCity(QueenslandLGAs.Brisbane)
														   .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														   .build();
		
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPostcode() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = CityNotSetException.class) @Ignore
	public void testGetPostcodeWhenStateSet() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .setState(AustralianStates.Queensland)
				   										   .build();
		
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetPostcodeWhenCitySet() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .setState(AustralianStates.Queensland)
				   										   .setCity(QueenslandLGAs.Brisbane)
				   										   .build();
		
		assertNull(address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getPostcode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetPostcodeWhenSuburbSet() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .setState(AustralianStates.Queensland)
				   										   .setCity(QueenslandLGAs.Brisbane)
				   										   .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				   										   .build();
		
		assertEquals("4000", address.getPostcode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getStreetName()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStreetName() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setStreetName(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetName() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
						   								   .build();
		assertNull(address.getStreetName());
		
		address.setStreetName("Mary");
		assertEquals("Mary", address.getStreetName());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setStreetName(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetNameWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
						   								   .setStreetName("Mary")
						   								   .build();
		assertEquals("Mary", address.getStreetName());
	}	

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getStreetType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStreetType() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetType() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetType());
		
		address.setStreetType(StreetType.Alley);
		assertEquals(StreetType.Alley, address.getStreetType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetTypeWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .setStreetType(StreetType.Alley)
				   										   .build();
		assertEquals(StreetType.Alley, address.getStreetType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getStreetSuffix()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetStreetSuffix() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setStreetSuffix(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetSuffix() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getStreetSuffix());
		
		address.setStreetSuffix("W");
		assertEquals("W", address.getStreetSuffix());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setStreetSuffix(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetStreetSuffixWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .setStreetSuffix("W")
				   										   .build();
		assertEquals("W", address.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getPropertyNumber()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPropertyNumber() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getPropertyNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setPropertyNumber(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyNumber() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getPropertyNumber());
		
		address.setPropertyNumber("22-33");
		assertEquals("22-33", address.getPropertyNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setPropertyNumber(java.lang.String)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyNumberWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setPropertyNumber("22-33")
														   .build();
		assertEquals("22-33", address.getPropertyNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getPropertyType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetPropertyType() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getPropertyType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyType() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getPropertyType());
		
		address.setPropertyType(BuildingType.Apartment);
		assertEquals(BuildingType.Apartment, address.getPropertyType());
		
		address.setPropertyType(ParcelOfLandType.Section);
		assertEquals(ParcelOfLandType.Section, address.getPropertyType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetPropertyTypeWhenBuilt() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setPropertyType(BuildingType.Apartment)
														   .build();
		assertEquals(BuildingType.Apartment, address.getPropertyType());
		
		ResidentialAddress address2 = new ResidentialAddress.Builder(Countries.Australia)
				   										    .setPropertyType(ParcelOfLandType.Lot)
				   										    .build();
		assertEquals(ParcelOfLandType.Lot, address2.getPropertyType());
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getBuildingNumber()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetBuildingNumber() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getBuildingNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetBuildingNumber() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .build();
		assertNull(address.getBuildingNumber());
		
		address.setBuildingNumber("33");
		assertEquals("33", address.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetBuildingNumberWhenBuilt() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				   										   .setBuildingNumber("33")
				   										   .build();
		assertEquals("33", address.getBuildingNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getLevel()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetLevel() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														  .build();
		assertNull(address.getLevel());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevel() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .build();
		assertNull(address.getLevel());
		
		address.setLevel(10);
		assertEquals(new Integer(10), address.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelWhenBuilt() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
														   .setLevel(10)
														   .build();

		assertEquals(new Integer(10), address.getLevel());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#getLevelType()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testGetLevelType() throws StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				  										   .build();
		assertNull(address.getLevelType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				  										   .build();
		assertNull(address.getLevel());
		
		address.setLevelType(LevelType.Basement);
		assertEquals(LevelType.Basement, address.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = StateNotSetException.class) @Ignore
	public void testSetLevelTypeWhenBuilt() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
				  										   .setLevelType(LevelType.Basement)
				  										   .build();
		assertEquals(LevelType.Basement, address.getLevelType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 */
	@Test @Ignore
	public void testEqualsSameObject() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target;
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameValue() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("Mary")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("W")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("3")
															.setLevel(10)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameCountry() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Australia)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameState() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Queensland)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameCity() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Brisbane)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameSuburb() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStreetName() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("Mary")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStreetType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Street)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameStreetSuffix() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("W")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSamePropertyNumber() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("33-44")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSamePropertyType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Flat)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameBuildingNumber() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("3")
															.setLevel(1)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameLevel() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(10)
															.setLevelType(LevelType.Basement)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Basement, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsSameLevelType() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress.Builder(Countries.Taiwan)
															.setState(AustralianStates.Victoria)
															.setCity(QueenslandLGAs.Aurukun)
															.setSuburb(BrisbaneSuburbs.Calamvale)
															.setStreetName("John")
															.setStreetType(StreetType.Road)
															.setStreetSuffix("E")
															.setPropertyNumber("123")
															.setPropertyType(BuildingType.Unit)
															.setBuildingNumber("10")
															.setLevel(1)
															.setLevelType(LevelType.Floor)
															.build();
		
		assertEquals(Countries.Taiwan, address.getCountry());
		assertEquals(AustralianStates.Victoria, address.getState());
		assertEquals(QueenslandLGAs.Aurukun, address.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		
		assertEquals("John", address.getStreetName());
		assertEquals(StreetType.Road, address.getStreetType());
		assertEquals("E", address.getStreetSuffix());
		assertEquals("123", address.getPropertyNumber());
		assertEquals(BuildingType.Unit, address.getPropertyType());
		assertEquals("10", address.getBuildingNumber());
		assertEquals(new Integer(1), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.target.getCountry(), address.getCountry());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test @Ignore
	public void testEqualsStateModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.target.getState(), address.getState());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.target.getCity(), address.getCity());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterCopy() throws PropertyTypeNotMatchException, CityNotSetException, StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.target.getSuburb(), address.getSuburb());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetNameModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.target.getStreetName(), address.getStreetName());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.target.getStreetType(), address.getStreetType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetSuffixModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.target.getStreetSuffix(), address.getStreetSuffix());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyNumberModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.target.getPropertyNumber(), address.getPropertyNumber());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.target.getPropertyType(), address.getPropertyType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsBuildingNumberModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.target.getBuildingNumber(), address.getBuildingNumber());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.target.getLevel(), address.getLevel());
		
		assertFalse(this.target.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelTypeModifiedAfterCopy() throws PropertyTypeNotMatchException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = new ResidentialAddress(this.target);
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.target.getLevelType(), address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertTrue(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.target.getCountry(), address.getCountry());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsStateModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.target.getState(), address.getState());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterClone() throws PropertyTypeNotMatchException, StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.target.getCity(), address.getCity());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterClone() throws PropertyTypeNotMatchException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.target.getSuburb(), address.getSuburb());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetNameModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.target.getStreetName(), address.getStreetName());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.target.getStreetType(), address.getStreetType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsStreetSuffixModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.target.getStreetSuffix(), address.getStreetSuffix());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyNumberModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.target.getPropertyNumber(), address.getPropertyNumber());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsPropertyTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.target.getPropertyType(), address.getPropertyType());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsBuildingNumberModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.target.getBuildingNumber(), address.getBuildingNumber());
		
		assertFalse(this.target.equals(address));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.target.getLevel(), address.getLevel());
		
		assertFalse(this.target.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsLevelTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.target.getLevelType(), address.getLevelType());
		
		assertFalse(this.target.equals(address));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		assertEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#ResidentialAddress()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testLotAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException, StateNotSetException, CityNotSetException {
		ResidentialAddress lot = new ResidentialAddress.Builder(Countries.Australia)
														.setState(AustralianStates.Queensland)
														.setCity(QueenslandLGAs.Brisbane)
														.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
														.setStreetName("Mary")
														.setStreetType(StreetType.Street)
														.setStreetSuffix("W")
														.setPropertyNumber("33-44")
														.setPropertyType(ParcelOfLandType.Lot)														
														.build();
		assertEquals(Countries.Australia, lot.getCountry());
		assertEquals(AustralianStates.Queensland, lot.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot.getSuburb());
		
		assertEquals("Mary", lot.getStreetName());
		assertEquals(StreetType.Street, lot.getStreetType());
		assertEquals("W", lot.getStreetSuffix());
		assertEquals("33-44", lot.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot.getPropertyType());


		ResidentialAddress lot2 = lot.clone();
		
		assertEquals(Countries.Australia, lot2.getCountry());
		assertEquals(AustralianStates.Queensland, lot2.getState());
		assertEquals(QueenslandLGAs.Brisbane, lot2.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lot2.getSuburb());
		
		assertEquals("Mary", lot2.getStreetName());
		assertEquals(StreetType.Street, lot2.getStreetType());
		assertEquals("W", lot2.getStreetSuffix());
		assertEquals("33-44", lot2.getPropertyNumber());
		assertEquals(ParcelOfLandType.Lot, lot2.getPropertyType());
		
		assertEquals(lot, lot2);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCountryModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, address.getCountry());
		assertNotEquals(this.target.getCountry(), address.getCountry());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testStateModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, address.getState());
		assertNotEquals(this.target.getState(), address.getState());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCityModifiedAfterClone() throws PropertyTypeNotMatchException, StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setCity(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, address.getCity());
		assertNotEquals(this.target.getCity(), address.getCity());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testSuburbModifiedAfterClone() throws PropertyTypeNotMatchException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setSuburb(BrisbaneSuburbs.Calamvale);
		assertEquals(BrisbaneSuburbs.Calamvale, address.getSuburb());
		assertNotEquals(this.target.getSuburb(), address.getSuburb());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetNameModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetName("John");
		assertEquals("John", address.getStreetName());
		assertNotEquals(this.target.getStreetName(), address.getStreetName());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetType(StreetType.Avenue);
		assertEquals(StreetType.Avenue, address.getStreetType());
		assertNotEquals(this.target.getStreetType(), address.getStreetType());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testStreetSuffixModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setStreetSuffix("E");
		assertEquals("E", address.getStreetSuffix());
		assertNotEquals(this.target.getStreetSuffix(), address.getStreetSuffix());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyNumberModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyNumber("123");
		assertEquals("123", address.getPropertyNumber());
		assertNotEquals(this.target.getPropertyNumber(), address.getPropertyNumber());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testPropertyTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setPropertyType(BuildingType.Factory);
		assertEquals(BuildingType.Factory, address.getPropertyType());
		assertNotEquals(this.target.getPropertyType(), address.getPropertyType());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testBuildingNumberModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setBuildingNumber("122");
		assertEquals("122", address.getBuildingNumber());
		assertNotEquals(this.target.getBuildingNumber(), address.getBuildingNumber());
		
		assertNotEquals(this.target, address);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevel(13);
		assertEquals(new Integer(13), address.getLevel());
		assertNotEquals(this.target.getLevel(), address.getLevel());
		
		assertNotEquals(this.target, address);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.ResidentialAddress#clone()}.
	 * @throws PropertyTypeNotMatchException 
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testLevelTypeModifiedAfterClone() throws PropertyTypeNotMatchException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("Mary", this.target.getStreetName());
		assertEquals(StreetType.Street, this.target.getStreetType());
		assertEquals("W", this.target.getStreetSuffix());
		assertEquals("33-44", this.target.getPropertyNumber());
		assertEquals(BuildingType.Flat, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(10), this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());

		ResidentialAddress address = this.target.clone();
		
		assertEquals(Countries.Australia, address.getCountry());
		assertEquals(AustralianStates.Queensland, address.getState());
		assertEquals(QueenslandLGAs.Brisbane, address.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, address.getSuburb());
		
		assertEquals("Mary", address.getStreetName());
		assertEquals(StreetType.Street, address.getStreetType());
		assertEquals("W", address.getStreetSuffix());
		assertEquals("33-44", address.getPropertyNumber());
		assertEquals(BuildingType.Flat, address.getPropertyType());
		assertEquals("3", address.getBuildingNumber());
		assertEquals(new Integer(10), address.getLevel());
		assertEquals(LevelType.Floor, address.getLevelType());
		
		address.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, address.getLevelType());
		assertNotEquals(this.target.getLevelType(), address.getLevelType());
		
		assertNotEquals(this.target, address);
	}

}
