/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *To test the functionalities of Property
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PropertyTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#Property(com.digsarustudio.banana.contactinfo.address.property.Property)}.
	 */
	@Test @Ignore
	public void testPropertyProperty() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#getType()}.
	 */
	@Test @Ignore
	public void testGetType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#setType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 */
	@Test @Ignore
	public void testSetType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#getPropertyNumber()}.
	 */
	@Test @Ignore
	public void testGetPropertyNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#setPropertyNumber(java.lang.String)}.
	 */
	@Test @Ignore
	public void testSetPropertyNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#clone()}.
	 */
	@Test @Ignore
	public void testClone() {
		fail("Not yet implemented");
	}

}
