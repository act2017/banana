/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * To test the functionalities of Building
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class BuildingTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#Building(com.digsarustudio.banana.contactinfo.address.property.Building)}.
	 */
	@Test @Ignore
	public void testBuilding() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#getLevel()}.
	 */
	@Test @Ignore
	public void testGetLevel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#setLevel(com.digsarustudio.banana.contactinfo.address.property.Level)}.
	 */
	@Test @Ignore
	public void testSetLevel() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#getBuildingNumber()}.
	 */
	@Test @Ignore
	public void testGetBuildingNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#setBuildingNumber(java.lang.String)}.
	 */
	@Test @Ignore
	public void testSetBuildingNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Building#clone()}.
	 */
	@Test @Ignore
	public void testClone() {
		fail("Not yet implemented");
	}

	/** @Ignore
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#Property(com.digsarustudio.banana.contactinfo.address.property.Property)}.
	 */
	@Test @Ignore
	public void testPropertyProperty() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#getType()}.
	 */
	@Test @Ignore
	public void testGetType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#setType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 */
	@Test @Ignore
	public void testSetType() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#getPropertyNumber()}.
	 */
	@Test @Ignore
	public void testGetPropertyNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#setPropertyNumber(java.lang.String)}.
	 */
	@Test @Ignore
	public void testSetPropertyNumber() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.property.Property#clone()}.
	 */
	@Test @Ignore
	public void testClone1() {
		fail("Not yet implemented");
	}

}
