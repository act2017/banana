/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.ParcelOfLandType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * To test PhysicalDeliveryLocation object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PhysicalDeliveryLocationTest {
	PhysicalDeliveryLocation target;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.target = new PhysicalDeliveryLocation.Builder("23-24")
												  .setStreetName("Marry")
												  .setStreetType(StreetType.Road)
												  .setStreetSuffix("West")
												  .setPropertyType(BuildingType.Unit)
												  .setBuildingNumber("3")
												  .setLevel(2)
												  .setLevelType(LevelType.Floor)
												  .build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeSameObject() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target;
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameValue() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("23-24")
																		.setStreetName("Marry")
																		.setStreetType(StreetType.Road)
																		.setStreetSuffix("West")
																		.setPropertyType(BuildingType.Unit)
																		.setBuildingNumber("3")
																		.setLevel(2)
																		.setLevelType(LevelType.Floor)
																		.build();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameLocationNumber() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("23-24")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameStreetName() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("Marry")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameStreetType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Road)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameStreetSuffix() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("West")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSamePropertyType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Unit)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameBuildingNumber() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("3")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameLevel() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(2)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameLevelType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Floor)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testHashCodeDiffPropertyType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("23-24")
																		.setStreetName("Marry")
																		.setStreetType(StreetType.Road)
																		.setStreetSuffix("West")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Floor)
																		.build();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		assertNull(location.getBuildingNumber());
		assertNull(location.getLevel());
		assertNull(location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeAfter() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeLocationNumberModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLocationNumber("123");
		assertEquals("123", location.getLocationNumber());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeStreetNameModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeStreetTypeModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetType(StreetType.Street);
		assertEquals(StreetType.Street, location.getStreetType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeStreetSuffixModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetSuffix("E");
		assertEquals("E", location.getStreetSuffix());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodePropertyTypeModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(BuildingType.Room);
		assertEquals(BuildingType.Room, location.getPropertyType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeBuildingNumberModifiedAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setBuildingNumber("32");
		assertEquals("32", location.getBuildingNumber());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeLevelModifiedAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevel(10);
		assertEquals(new Integer(10), location.getLevel());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameLevelTypeAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffPropertyTypeAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeLocationNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLocationNumber("123");
		assertEquals("123", location.getLocationNumber());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeStreetNameModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeStreetTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetType(StreetType.Street);
		assertEquals(StreetType.Street, location.getStreetType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeStreetSuffixModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetSuffix("E");
		assertEquals("E", location.getStreetSuffix());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodePropertyTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(BuildingType.Room);
		assertEquals(BuildingType.Room, location.getPropertyType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeBuildingNumberModifiedAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setBuildingNumber("32");
		assertEquals("32", location.getBuildingNumber());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeLevelModifiedAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevel(10);
		assertEquals(new Integer(10), location.getLevel());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testHashCodeSameLevelTypeAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#hashCode()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeDiffPropertyTypeAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		
		assertNotEquals(this.target.hashCode(), location.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsSameObject() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target;
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertTrue(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameValue() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("23-24")
																		.setStreetName("Marry")
																		.setStreetType(StreetType.Road)
																		.setStreetSuffix("West")
																		.setPropertyType(BuildingType.Unit)
																		.setBuildingNumber("3")
																		.setLevel(2)
																		.setLevelType(LevelType.Floor)
																		.build();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertTrue(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameLocationNumber() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("23-24")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameStreetName() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("Marry")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameStreetType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Road)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameStreetSuffix() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("West")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSamePropertyType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Unit)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameBuildingNumber() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("3")
																		.setLevel(5)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameLevel() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(2)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Basement, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameLevelType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("122")
																		.setStreetName("John")
																		.setStreetType(StreetType.Street)
																		.setStreetSuffix("E")
																		.setPropertyType(BuildingType.Room)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Floor)
																		.build();
		assertEquals("122", location.getLocationNumber());
		assertEquals("John", location.getStreetName());
		assertEquals(StreetType.Street, location.getStreetType());
		assertEquals("E", location.getStreetSuffix());
		assertEquals(BuildingType.Room, location.getPropertyType());
		assertEquals("1", location.getBuildingNumber());
		assertEquals(new Integer(5), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected=PropertyTypeNotMatchException.class)
	public void testEqualsDiffPropertyType() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("23-24")
																		.setStreetName("Marry")
																		.setStreetType(StreetType.Road)
																		.setStreetSuffix("West")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.setBuildingNumber("1")
																		.setLevel(5)
																		.setLevelType(LevelType.Floor)
																		.build();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		assertNull(location.getBuildingNumber());
		assertNull(location.getLevel());
		assertNull(location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsAfter() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertTrue(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsLocationNumberModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLocationNumber("123");
		assertEquals("123", location.getLocationNumber());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsStreetNameModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsStreetTypeModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetType(StreetType.Street);
		assertEquals(StreetType.Street, location.getStreetType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsStreetSuffixModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetSuffix("E");
		assertEquals("E", location.getStreetSuffix());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsPropertyTypeModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(BuildingType.Room);
		assertEquals(BuildingType.Room, location.getPropertyType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsBuildingNumberModifiedAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setBuildingNumber("32");
		assertEquals("32", location.getBuildingNumber());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsLevelModifiedAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevel(10);
		assertEquals(new Integer(10), location.getLevel());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameLevelTypeAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 */
	@Test
	public void testEqualsDiffPropertyTypeAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertTrue(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsLocationNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLocationNumber("123");
		assertEquals("123", location.getLocationNumber());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsStreetNameModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsStreetTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetType(StreetType.Street);
		assertEquals(StreetType.Street, location.getStreetType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsStreetSuffixModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetSuffix("E");
		assertEquals("E", location.getStreetSuffix());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsPropertyTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(BuildingType.Room);
		assertEquals(BuildingType.Room, location.getPropertyType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsBuildingNumberModifiedAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setBuildingNumber("32");
		assertEquals("32", location.getBuildingNumber());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsLevelModifiedAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevel(10);
		assertEquals(new Integer(10), location.getLevel());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testEqualsSameLevelTypeAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
		
		assertFalse(this.target.equals(location));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#equals()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsDiffPropertyTypeAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		
		assertFalse(this.target.equals(location));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testLocationNumberModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLocationNumber("123");
		assertEquals("123", location.getLocationNumber());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testStreetNameModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testStreetTypeModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetType(StreetType.Street);
		assertEquals(StreetType.Street, location.getStreetType());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testStreetSuffixModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetSuffix("E");
		assertEquals("E", location.getStreetSuffix());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testPropertyTypeModifiedAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(BuildingType.Room);
		assertEquals(BuildingType.Room, location.getPropertyType());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testBuildingNumberModifiedAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setBuildingNumber("32");
		assertEquals("32", location.getBuildingNumber());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testLevelModifiedAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevel(10);
		assertEquals(new Integer(10), location.getLevel());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSameLevelTypeAfterCopy() throws PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 */
	@Test
	public void testDiffPropertyTypeAfterCopy() {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation(this.target);
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		
		assertNotEquals(this.target, location);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getStreetName()}.
	 */
	@Test
	public void testGetStreetNameWithoutSet() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setStreetName(java.lang.String)}.
	 */
	@Test
	public void testSetStreetNameString() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetName());
		
		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setStreetName(java.lang.String)}.
	 */
	@Test
	public void testSetStreetNameWhenBuilt() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").setStreetName("John").build();
		assertEquals("John", location.getStreetName());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setStreetName(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSetStreetNameWithSuffix() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetName());
		assertNull(location.getStreetSuffix());
		
		location.setStreetName("John");
		location.setStreetSuffix("W");
		assertEquals("John", location.getStreetName());
		assertEquals("W", location.getStreetSuffix());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setStreetName(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSetStreetNameWithSuffixWhenBuilt() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setStreetName("John")
																		.setStreetSuffix("W")
																		.build();
		assertEquals("John", location.getStreetName());
		assertEquals("W", location.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getStreetType()}.
	 */
	@Test
	public void testGetStreetType() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetType());		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 */
	@Test
	public void testSetStreetType() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetType());
		
		location.setStreetType(StreetType.Road);
		assertEquals(StreetType.Road, location.getStreetType());
	}

	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getStreetType()}.
	 */
	@Test
	public void testGetStreetTypeWhenBuilt() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setStreetType(StreetType.Road)
																		.build();
		
		assertEquals(StreetType.Road, location.getStreetType());
	}

	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getStreetSuffix()}.
	 */
	@Test
	public void testGetStreetSuffix() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setStreetSuffix(java.lang.String)}.
	 */
	@Test
	public void testSetStreetSuffix() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getStreetSuffix());
		
		location.setStreetSuffix("W");
		assertEquals("W", location.getStreetSuffix());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getStreetType()}.
	 */
	@Test
	public void testGetStreetSuffixWhenBuilt() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setStreetSuffix("W")
																		.build();
		
		assertEquals("W", location.getStreetSuffix());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getPropertyType()}.
	 */
	@Test
	public void testGetPropertyType() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getPropertyType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getPropertyType()}.
	 */
	@Test
	public void testGetPropertyTypeAsBuildingWhenBuild() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(BuildingType.Unit)
																		.build();
		assertEquals(BuildingType.Unit, location.getPropertyType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getPropertyType()}.
	 */
	@Test
	public void testGetPropertyTypeAsLotWhenBuild() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.build();
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 */
	@Test
	public void testSetPropertyTypeAsBuidling() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getPropertyType());
		
		location.setPropertyType(BuildingType.Apartment);
		assertEquals(BuildingType.Apartment, location.getPropertyType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 */
	@Test
	public void testSetPropertyTypeAsLot() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getPropertyType());
		
		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getBuildingNumber()}.
	 */
	@Test
	public void testGetBuildingNumber() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getBuildingNumber()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testGetBuildingNumberWhenBuilt() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setBuildingNumber("12")
																		.build();
		assertEquals("12", location.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getBuildingNumber()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testGetBuildingNumberWhenBuiltWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(BuildingType.Apartment)
																		.setBuildingNumber("12")
																		.build();
		assertEquals("12", location.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getBuildingNumber()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testGetBuildingNumberWhenBuiltWithParcelOfLandType() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.setBuildingNumber("12")
																		.build();
		assertEquals("12", location.getBuildingNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetBuildingNumber() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getBuildingNumber());
		
		location.setBuildingNumber("21");
		assertEquals("21", location.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetBuildingNumberWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getBuildingNumber());
		
		location.setPropertyType(BuildingType.Apartment);
		location.setBuildingNumber("21");		
		assertEquals("21", location.getBuildingNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setBuildingNumber(java.lang.String)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testSetBuildingNumberWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getBuildingNumber());
		
		location.setPropertyType(ParcelOfLandType.Lot);
		location.setBuildingNumber("21");		
		assertEquals("12", location.getBuildingNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevel()}.
	 */
	@Test
	public void testGetLevel() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevel()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testGetLevelWhenBuilt() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setLevel(10)
																		.build();
		assertEquals(new Integer(10), location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevel()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testGetLevelWhenBuiltWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(BuildingType.Flat)
																		.setLevel(10)
																		.build();
		assertEquals(new Integer(10), location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevel()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testGetLevelWhenBuiltWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.setLevel(10)
																		.build();
		assertEquals(new Integer(10), location.getLevel());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetLevel() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getLevel());
		
		location.setLevel(20);
		assertEquals(new Integer(20), location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetLevelWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getLevel());
		
		location.setPropertyType(BuildingType.Flat);
		location.setLevel(20);
		assertEquals(new Integer(20), location.getLevel());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetLevelBuiltWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(BuildingType.Flat)
																		.build();
		assertNull(location.getLevel());
		
		location.setLevel(20);
		assertEquals(new Integer(20), location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testSetLevelWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getLevel());
		
		location.setPropertyType(ParcelOfLandType.Lot);
		location.setLevel(20);
		assertEquals(new Integer(20), location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevel(java.lang.Integer)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testSetLevelBuiltWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.build();
		assertNull(location.getLevel());
		
		location.setLevel(20);
		assertEquals(new Integer(20), location.getLevel());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevelType()}.
	 */
	@Test
	public void testGetLevelType() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		assertNull(location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevelType()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testGetLevelTypeWhenBuilt() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals(LevelType.Basement, location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevelType()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testGetLevelTypeWhenBuiltWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(BuildingType.Flat)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals(LevelType.Basement, location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getLevelType()}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testGetLevelTypeWhenBuiltWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.setLevelType(LevelType.Basement)
																		.build();
		assertEquals(LevelType.Basement, location.getLevelType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetLevelType() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.build();
		assertNull(location.getLevel());
		
		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetLevelTypeWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.build();
		assertNull(location.getLevel());
		
		location.setPropertyType(BuildingType.Flat);
		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testSetLevelTypeWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.build();
		assertNull(location.getLevel());
		
		location.setPropertyType(ParcelOfLandType.Lot);
		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSetLevelTypeBuiltWithBuilding() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(BuildingType.Flat)
																		.build();
		assertNull(location.getLevel());
		
		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test(expected = PropertyTypeNotMatchException.class)
	public void testSetLevelTypeBuiltWithLot() throws PropertyTypeNotMatchException {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.setPropertyType(ParcelOfLandType.Lot)
																		.build();
		assertNull(location.getLevel());
		
		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#getPostalDeliveryType()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetPostalDeliveryType() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.build();
		assertNull(location.getPostalDeliveryType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetPostalDeliveryType() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33")
																		.build();
		location.setPostalDeliveryType(PostalDeliveryType.CareOfPostOffice);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());
		
		assertEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testLocationNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLocationNumber("123");
		assertEquals("123", location.getLocationNumber());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testStreetNameModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetName("John");
		assertEquals("John", location.getStreetName());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testStreetTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetType(StreetType.Street);
		assertEquals(StreetType.Street, location.getStreetType());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testStreetSuffixModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setStreetSuffix("E");
		assertEquals("E", location.getStreetSuffix());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testPropertyTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(BuildingType.Room);
		assertEquals(BuildingType.Room, location.getPropertyType());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testBuildingNumberModifiedAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setBuildingNumber("32");
		assertEquals("32", location.getBuildingNumber());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testLevelModifiedAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevel(10);
		assertEquals(new Integer(10), location.getLevel());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 * @throws PropertyTypeNotMatchException 
	 */
	@Test
	public void testSameLevelTypeAfterClone() throws CloneNotSupportedException, PropertyTypeNotMatchException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setLevelType(LevelType.GroundFloor);
		assertEquals(LevelType.GroundFloor, location.getLevelType());
		
		assertNotEquals(this.target, location);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation#PhysicalDeliveryLocation()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testDiffPropertyTypeAfterClone() throws CloneNotSupportedException {
		assertEquals("23-24", this.target.getLocationNumber());
		assertEquals("Marry", this.target.getStreetName());
		assertEquals(StreetType.Road, this.target.getStreetType());
		assertEquals("West", this.target.getStreetSuffix());
		assertEquals(BuildingType.Unit, this.target.getPropertyType());
		assertEquals("3", this.target.getBuildingNumber());
		assertEquals(new Integer(2),  this.target.getLevel());
		assertEquals(LevelType.Floor, this.target.getLevelType());
		
		PhysicalDeliveryLocation location = this.target.clone();
		assertEquals("23-24", location.getLocationNumber());
		assertEquals("Marry", location.getStreetName());
		assertEquals(StreetType.Road, location.getStreetType());
		assertEquals("West", location.getStreetSuffix());
		assertEquals(BuildingType.Unit, location.getPropertyType());
		assertEquals("3", location.getBuildingNumber());
		assertEquals(new Integer(2), location.getLevel());
		assertEquals(LevelType.Floor, location.getLevelType());

		location.setPropertyType(ParcelOfLandType.Lot);
		assertEquals(ParcelOfLandType.Lot, location.getPropertyType());
		
		assertNotEquals(this.target, location);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractDeliveryLocation#getLocationNumber()}.
	 */
	@Test
	public void testGetLocationNumber() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		
		assertEquals("33", location.getLocationNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractDeliveryLocation#setLocationNumber(java.lang.String)}.
	 */
	@Test
	public void testSetLocationNumber() {
		PhysicalDeliveryLocation location = new PhysicalDeliveryLocation.Builder("33").build();
		
		assertEquals("33", location.getLocationNumber());
		
		location.setLocationNumber("22-33");
		assertEquals("22-33", location.getLocationNumber());
	}
}
