/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * To test all of the classes under DeliveryLocation package
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ POBoxTest.class, PhysicalDeliveryLocationTest.class})
public class DeliveryLocationTests {

}
