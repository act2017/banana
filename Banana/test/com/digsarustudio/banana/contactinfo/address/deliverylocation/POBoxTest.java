/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * To test Australian P.O.Box
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class POBoxTest {
	private POBox target = new POBox.Builder("1233", PostalDeliveryType.PostOfficeBox).build();
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.POBox#AustralianPOBox(java.lang.String, com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)}.
	 */
	@Test
	public void testConstructsAustralianPOBox() {
		POBox poBox = new POBox.Builder("1233", PostalDeliveryType.PostOfficeBox).build();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#AustralianPOBox()}.
	 */
	@Test
	public void testAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertEquals(this.target, poBox);
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#AustralianPOBox()}.
	 */
	@Test
	public void testLocationNumberModifiedAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		assertEquals("3", poBox.getLocationNumber());
		
		assertNotEquals(this.target, poBox);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#AustralianPOBox()}.
	 */
	@Test
	public void testTypeModifiedAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target, poBox);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#AustralianPOBox()}.
	 */
	@Test
	public void testDiffValAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target, poBox);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#clone();}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertEquals(this.target, poBox);
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#clone();}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testLocationNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		assertEquals("3", poBox.getLocationNumber());
		
		assertNotEquals(this.target, poBox);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#clone();}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target, poBox);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#clone();}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testDiffValAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target, poBox);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeSameObject() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target;
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertEquals(this.target.hashCode(), poBox.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeSameObjectModified() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target;
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("12");
		assertEquals("12", this.target.getLocationNumber());
		assertEquals("12", poBox.getLocationNumber());
		
		assertEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeSameValue() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("1233", PostalDeliveryType.PostOfficeBox).build();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeSameLocationNumber() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("1233", PostalDeliveryType.PrivateBag).build();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PrivateBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeSameType() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("3", PostalDeliveryType.PostOfficeBox).build();
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeDiffVal() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("3", PostalDeliveryType.LockedBag).build();
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertEquals(this.target.hashCode(), poBox.hashCode());
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeLocationNumberModifiedAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		assertEquals("3", poBox.getLocationNumber());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeTypeModifiedAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 */
	@Test
	public void testHashCodeDiffValAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertEquals(this.target.hashCode(), poBox.hashCode());
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeLocationNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		assertEquals("3", poBox.getLocationNumber());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#hashCode(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testHashCodeDiffValAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertNotEquals(this.target.hashCode(), poBox.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSameObject() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target;
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertTrue(this.target.equals(poBox));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSameObjectModified() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target;
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("12");
		assertEquals("12", this.target.getLocationNumber());
		assertEquals("12", poBox.getLocationNumber());
		
		assertTrue(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSameValue() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("1233", PostalDeliveryType.PostOfficeBox).build();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertTrue(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSameLocationNumber() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("1233", PostalDeliveryType.PrivateBag).build();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PrivateBag, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSameType() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("3", PostalDeliveryType.PostOfficeBox).build();
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsDiffVal() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox.Builder("3", PostalDeliveryType.LockedBag).build();
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertTrue(this.target.equals(poBox));
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsLocationNumberModifiedAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		assertEquals("3", poBox.getLocationNumber());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsTypeModifiedAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsDiffValAfterCopy() {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = new POBox(this.target);
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		assertTrue(this.target.equals(poBox));
	}	
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsLocationNumberModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		assertEquals("3", poBox.getLocationNumber());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsTypeModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#equals(java.lang.Object)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testEqualsDiffValAfterClone() throws CloneNotSupportedException {
		assertEquals("1233", this.target.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, this.target.getPostalDeliveryType());
		
		POBox poBox = this.target.clone();
		assertEquals("1233", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setLocationNumber("3");
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals("3", poBox.getLocationNumber());
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
		
		assertFalse(this.target.equals(poBox));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getType()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getStreetType();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setStreetType(StreetType.Alley);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getStreetName()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetStreetName() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getStreetName();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setStreetName(java.lang.String)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetStreetNameString() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setStreetName("Marry");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setStreetName(java.lang.String, java.lang.String)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetStreetNameStringString() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setStreetName("Marry", "W");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getStreetType()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetStreetType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getStreetType();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetStreetType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setStreetType(StreetType.Alley);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getStreetSuffix()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetStreetSuffix() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getStreetSuffix();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setStreetSuffix(java.lang.String)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetStreetSuffix() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setStreetSuffix("W");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getPropertyType()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetPropertyType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getPropertyType();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetPropertyType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setPropertyType(BuildingType.Apartment);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getBuildingNumber()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetBuildingNumber() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getBuildingNumber();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setBuildingNumber(java.lang.String)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetBuildingNumber() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setBuildingNumber("123");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getLevel()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetLevel() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getLevel();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setLevel(java.lang.Integer)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetLevel() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setLevel(12);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getLevelType()}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testGetLevelType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.getLevelType();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)}.
	 */
	@SuppressWarnings("deprecation")
	@Test(expected = UnsupportedOperationException.class)
	public void testSetLevelType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();
		
		poBox.setLevelType(LevelType.Floor);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#getPostalDeliveryType()}.
	 */
	@Test
	public void testGetPostalDeliveryType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();		
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)}.
	 */
	@Test
	public void testSetPostalDeliveryType() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();		
		assertEquals(PostalDeliveryType.PostOfficeBox, poBox.getPostalDeliveryType());
		
		poBox.setPostalDeliveryType(PostalDeliveryType.LockedBag);
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractPOBox#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)}.
	 */
	@Test
	public void testSetPostalDeliveryTypeWhenBuilt() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox)
												   .setType(PostalDeliveryType.LockedBag)
												   .build();		
		assertEquals(PostalDeliveryType.LockedBag, poBox.getPostalDeliveryType());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractDeliveryLocation#getLocationNumber()}.
	 */
	@Test
	public void testGetLocationNumber() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();		
		assertEquals("33", poBox.getLocationNumber());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractDeliveryLocation#setLocationNumber(java.lang.String)}.
	 */
	@Test
	public void testSetLocationNumber() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox).build();		
		assertEquals("33", poBox.getLocationNumber());
		
		poBox.setLocationNumber("33444");
		assertEquals("33444", poBox.getLocationNumber());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.deliverylocation.AbstractDeliveryLocation#setLocationNumber(java.lang.String)}.
	 */
	@Test
	public void testSetLocationNumberWhenBuilt() {
		POBox poBox = new POBox.Builder("33", PostalDeliveryType.PostOfficeBox)
												   .setLocationNumber("1234")
												   .build();
		
		assertEquals("1234", poBox.getLocationNumber());
	}

}
