/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * To test the functionalities of Suburb
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class SuburbTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Suburb#Suburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Suburb)}.
	 */
	@Test @Ignore
	public void testSuburb() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Suburb#clone()}.
	 */
	@Test @Ignore
	public void testClone() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Suburb#getPostcode()}.
	 */
	@Test @Ignore
	public void testGetPostcode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#AdministrativeDivision(com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision)}.
	 */
	@Test @Ignore
	public void testAdministrativeDivisionAdministrativeDivision() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#getSubdivision()}.
	 */
	@Test @Ignore
	public void testGetSubdivision() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#setSubdivision(com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision)}.
	 */
	@Test @Ignore
	public void testSetSubdivision() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#getDivision()}.
	 */
	@Test @Ignore
	public void testGetDivision() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#setDivision(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 */
	@Test @Ignore
	public void testSetDivision() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.AdministrativeDivision#clone()}.
	 */
	@Test @Ignore
	public void testClone1() {
		fail("Not yet implemented");
	}

}
