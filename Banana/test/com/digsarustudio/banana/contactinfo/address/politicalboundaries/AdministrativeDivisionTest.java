/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.AustralianStates;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;

/**
 * To test administrative division object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class AdministrativeDivisionTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state;
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCodeWithSameValue() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCodeDifferentValue() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Victoria).build();
		Assert.assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		Assert.assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCodeSameSubdivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertEquals(lgas, lgas1);
		
		Assert.assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffSubdivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Ipswich).build();
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertNotEquals(lgas, lgas1);
		
		Assert.assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCodeSameSubdivisionButDiffDivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Victoria).build();
		Assert.assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertEquals(lgas, lgas1);
		
		Assert.assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffSubdivisionButDiffDivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Victoria).build();
		Assert.assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Ipswich).build();
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertNotEquals(lgas, lgas1);
		
		Assert.assertFalse(state.equals(state1));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#getSubdivision()}.
	 */
	@Test
	public void testGetSubdivision() {
		AdministrativeDivision states = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertNull(states.getSubdivision());
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertNull(lgas.getSubdivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#setSubdivision(com.digsarustudio.banana.contactinfo.address.territory.IAdministrativeDivisionConstituent)}.
	 */
	@Test
	public void testSetSubdivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertNull(state.getSubdivision());
		
		AdministrativeDivision city = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		state.setSubdivision(city);
		
		Assert.assertEquals(city, state.getSubdivision());
		Assert.assertEquals(QueenslandLGAs.Brisbane, state.getSubdivision().getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#getDivision()}.
	 */
	@Test
	public void testGetName() {
		AdministrativeDivision states = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, states.getDivision());
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#setDivision(com.digsarustudio.banana.contactinfo.address.IEnumAbbreviatable)}.
	 */
	@Test
	public void testSetName() {
		AdministrativeDivision states = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, states.getDivision());
		
		states.setDivision(AustralianStates.NewSouthWales);
		Assert.assertEquals(AustralianStates.NewSouthWales, states.getDivision());
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		lgas.setDivision(QueenslandLGAs.Ipswich);
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas.getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state;
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObjectInSameValue() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsObject() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.NewSouthWales).build();
		Assert.assertEquals(AustralianStates.NewSouthWales, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSubdivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertEquals(lgas, lgas1);
		
		Assert.assertEquals(state, state1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsSubdivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Ipswich).build();
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);		
		Assert.assertNotEquals(lgas, lgas1);
		
		Assert.assertNotEquals(state, state1);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsDivision() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.NewSouthWales).build();
		Assert.assertEquals(AustralianStates.NewSouthWales, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Ipswich).build();
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertNotEquals(lgas, lgas1);
		
		Assert.assertNotEquals(state, state1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSubdivisionButNotEqualsDivisions() {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = new AdministrativeDivision.Builder(AustralianStates.NewSouthWales).build();
		Assert.assertEquals(AustralianStates.NewSouthWales, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertEquals(lgas, lgas1);
		
		Assert.assertNotEquals(state, state1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testClone() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testCloneWithSameValue() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testCloneDifferentValue() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		state1.setDivision(AustralianStates.Victoria);
		Assert.assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		Assert.assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testCloneSameSubdivision() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertEquals(lgas, lgas1);
		
		Assert.assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testCloneDiffSubdivision() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		Assert.assertEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Ipswich).build();
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertNotEquals(lgas, lgas1);
		
		Assert.assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testCloneSameSubdivisionButDiffDivision() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		state1.setDivision(AustralianStates.Victoria);
		Assert.assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertEquals(lgas, lgas1);
		
		Assert.assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#Clone()}.
	 * @throws CloneNotSupportedException 
	 */
	@Test
	public void testCloneDiffSubdivisionButDiffDivision() throws CloneNotSupportedException {
		AdministrativeDivision state = new AdministrativeDivision.Builder(AustralianStates.Queensland).build();
		Assert.assertEquals(AustralianStates.Queensland, state.getDivision());
		
		AdministrativeDivision state1 = state.clone();
		Assert.assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		state1.setDivision(AustralianStates.Victoria);
		Assert.assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		Assert.assertNotEquals(state, state1);
		
		AdministrativeDivision lgas = new AdministrativeDivision.Builder(QueenslandLGAs.Brisbane).build();
		Assert.assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		AdministrativeDivision lgas1 = new AdministrativeDivision.Builder(QueenslandLGAs.Ipswich).build();
		Assert.assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		Assert.assertNotEquals(lgas, lgas1);
		
		Assert.assertFalse(state.equals(state1));
	}
	
	
}
