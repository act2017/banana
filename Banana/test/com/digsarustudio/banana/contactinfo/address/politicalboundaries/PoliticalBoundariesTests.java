/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * To test all of the classes under PoliticalBoundaries package
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ AdministrativeDivisionTest.class, CityTest.class, PoliticalBoundariesTest.class, StateTest.class })
public class PoliticalBoundariesTests {

}
