/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test the states in Australia
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class AustralianStatesTest {


	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.AustralianStates#getStateName(com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.AustralianStates)}.
	 */
	@Test
	public void testGetStateName() {
		Assert.assertEquals("Queensland", AustralianStates.Queensland.getValue());
		Assert.assertEquals("New South Wales", AustralianStates.NewSouthWales.getValue());
		Assert.assertEquals("Australian Capital Territory", AustralianStates.AustralianCapitalTerritory.getValue());
		Assert.assertEquals("Victoria", AustralianStates.Victoria.getValue());
		Assert.assertEquals("Tasmania", AustralianStates.Tasmania.getValue());
		Assert.assertEquals("South Australia", AustralianStates.SouthAustralia.getValue());
		Assert.assertEquals("Western Australia", AustralianStates.WesternAustralia.getValue());
		Assert.assertEquals("Northen Territory", AustralianStates.NorthenTerritory.getValue());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.AustralianStates#getStateAbbreviation(com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.AustralianStates)}.
	 */
	@Test
	public void testGetStateAbbreviation() {
		Assert.assertEquals("QLD", AustralianStates.Queensland.getAbbreviation());
		Assert.assertEquals("NSW", AustralianStates.NewSouthWales.getAbbreviation());
		Assert.assertEquals("ACT", AustralianStates.AustralianCapitalTerritory.getAbbreviation());
		Assert.assertEquals("VIC", AustralianStates.Victoria.getAbbreviation());
		Assert.assertEquals("TAS", AustralianStates.Tasmania.getAbbreviation());
		Assert.assertEquals("SA", AustralianStates.SouthAustralia.getAbbreviation());
		Assert.assertEquals("WA", AustralianStates.WesternAustralia.getAbbreviation());
		Assert.assertEquals("NT", AustralianStates.NorthenTerritory.getAbbreviation());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.AustralianStates#getTypeFromName(java.lang.String)}.
	 */
	@Test
	public void testFromName() {
		Assert.assertEquals(AustralianStates.Queensland, AustralianStates.getTypeFromName("Queensland"));
		Assert.assertEquals(AustralianStates.Queensland, AustralianStates.getTypeFromName("queensland"));
		
		Assert.assertEquals(AustralianStates.NewSouthWales, AustralianStates.getTypeFromName("New South Wales"));
		Assert.assertEquals(AustralianStates.NewSouthWales, AustralianStates.getTypeFromName("new south wales"));
		
		Assert.assertEquals(AustralianStates.AustralianCapitalTerritory, AustralianStates.getTypeFromName("Australian Capital Territory"));
		Assert.assertEquals(AustralianStates.AustralianCapitalTerritory, AustralianStates.getTypeFromName("australian capital territory"));
		
		Assert.assertEquals(AustralianStates.Victoria, AustralianStates.getTypeFromName("Victoria"));
		Assert.assertEquals(AustralianStates.Victoria, AustralianStates.getTypeFromName("victoria"));
		
		Assert.assertEquals(AustralianStates.Tasmania, AustralianStates.getTypeFromName("Tasmania"));
		Assert.assertEquals(AustralianStates.Tasmania, AustralianStates.getTypeFromName("tasmania"));
		
		Assert.assertEquals(AustralianStates.SouthAustralia, AustralianStates.getTypeFromName("South Australia"));
		Assert.assertEquals(AustralianStates.SouthAustralia, AustralianStates.getTypeFromName("south australia"));
		
		Assert.assertEquals(AustralianStates.WesternAustralia, AustralianStates.getTypeFromName("Western Australia"));
		Assert.assertEquals(AustralianStates.WesternAustralia, AustralianStates.getTypeFromName("western australia"));
		
		Assert.assertEquals(AustralianStates.NorthenTerritory, AustralianStates.getTypeFromName("Northen Territory"));
		Assert.assertEquals(AustralianStates.NorthenTerritory, AustralianStates.getTypeFromName("northen territory"));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.AustralianStates#fromOrdinal(int)}.
	 */
	@Test
	public void testFromAbbreviation() {
		Assert.assertEquals(AustralianStates.Queensland, AustralianStates.getTypeFromAbbreviation("QLD"));
		Assert.assertEquals(AustralianStates.Queensland, AustralianStates.getTypeFromAbbreviation("qld"));
		
		Assert.assertEquals(AustralianStates.NewSouthWales, AustralianStates.getTypeFromAbbreviation("NSW"));
		Assert.assertEquals(AustralianStates.NewSouthWales, AustralianStates.getTypeFromAbbreviation("nsw"));
		
		Assert.assertEquals(AustralianStates.AustralianCapitalTerritory, AustralianStates.getTypeFromAbbreviation("ACT"));
		Assert.assertEquals(AustralianStates.AustralianCapitalTerritory, AustralianStates.getTypeFromAbbreviation("act"));
		
		Assert.assertEquals(AustralianStates.Victoria, AustralianStates.getTypeFromAbbreviation("VIC"));
		Assert.assertEquals(AustralianStates.Victoria, AustralianStates.getTypeFromAbbreviation("vic"));
		
		Assert.assertEquals(AustralianStates.Tasmania, AustralianStates.getTypeFromAbbreviation("TAS"));
		Assert.assertEquals(AustralianStates.Tasmania, AustralianStates.getTypeFromAbbreviation("tas"));
		
		Assert.assertEquals(AustralianStates.SouthAustralia, AustralianStates.getTypeFromAbbreviation("SA"));
		Assert.assertEquals(AustralianStates.SouthAustralia, AustralianStates.getTypeFromAbbreviation("sa"));
		
		Assert.assertEquals(AustralianStates.WesternAustralia, AustralianStates.getTypeFromAbbreviation("WA"));
		Assert.assertEquals(AustralianStates.WesternAustralia, AustralianStates.getTypeFromAbbreviation("wa"));
		
		Assert.assertEquals(AustralianStates.NorthenTerritory, AustralianStates.getTypeFromAbbreviation("NT"));
		Assert.assertEquals(AustralianStates.NorthenTerritory, AustralianStates.getTypeFromAbbreviation("nt"));
	}

}
