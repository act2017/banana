/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test QueenslandLGAs
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class QueenslandLGAsTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.QueenslandLGAs#getTypeFromName(java.lang.String)}.
	 */
	@Test
	public void testGetTypeFromName() {
		Assert.assertEquals(QueenslandLGAs.Brisbane, QueenslandLGAs.getTypeFromValue("Brisbane"));		
		Assert.assertEquals(QueenslandLGAs.GoldCoast, QueenslandLGAs.getTypeFromValue("Gold Coast"));
		Assert.assertEquals(QueenslandLGAs.Ipswich, QueenslandLGAs.getTypeFromValue("Ipswich"));
		Assert.assertEquals(QueenslandLGAs.LockyerValley, QueenslandLGAs.getTypeFromValue("Lockyer Valley"));
		Assert.assertEquals(QueenslandLGAs.Logan, QueenslandLGAs.getTypeFromValue("Logan"));
		Assert.assertEquals(QueenslandLGAs.MoretonBay, QueenslandLGAs.getTypeFromValue("Moreton Bay"));
		Assert.assertEquals(QueenslandLGAs.Noosa, QueenslandLGAs.getTypeFromValue("Noosa"));
		Assert.assertEquals(QueenslandLGAs.Redland, QueenslandLGAs.getTypeFromValue("Redland"));
		Assert.assertEquals(QueenslandLGAs.ScenicRim, QueenslandLGAs.getTypeFromValue("Scenic Rim"));
		Assert.assertEquals(QueenslandLGAs.Somerset, QueenslandLGAs.getTypeFromValue("Somerset"));
		Assert.assertEquals(QueenslandLGAs.SunshineCoast, QueenslandLGAs.getTypeFromValue("Sunshine Coast"));	
		Assert.assertEquals(QueenslandLGAs.Bundaberg, QueenslandLGAs.getTypeFromValue("Bundaberg"));
		Assert.assertEquals(QueenslandLGAs.Cherbourg, QueenslandLGAs.getTypeFromValue("Cherbourg"));
		Assert.assertEquals(QueenslandLGAs.FraserCoast, QueenslandLGAs.getTypeFromValue("Fraser Coast"));
		Assert.assertEquals(QueenslandLGAs.Gympie, QueenslandLGAs.getTypeFromValue("Gympie"));
		Assert.assertEquals(QueenslandLGAs.NorthBurnett, QueenslandLGAs.getTypeFromValue("North Burnett"));
		Assert.assertEquals(QueenslandLGAs.SouthBurnett, QueenslandLGAs.getTypeFromValue("South Burnett"));
		Assert.assertEquals(QueenslandLGAs.Goondiwindi, QueenslandLGAs.getTypeFromValue("Goondiwindi"));
		Assert.assertEquals(QueenslandLGAs.SouthernDowns, QueenslandLGAs.getTypeFromValue("Southern Downs"));
		Assert.assertEquals(QueenslandLGAs.Toowoomba, QueenslandLGAs.getTypeFromValue("Toowoomba"));
		Assert.assertEquals(QueenslandLGAs.WesternDowns, QueenslandLGAs.getTypeFromValue("Western Downs"));
	
		Assert.assertEquals(QueenslandLGAs.Banana, QueenslandLGAs.getTypeFromValue("Banana"));
		Assert.assertEquals(QueenslandLGAs.CentralHighlands, QueenslandLGAs.getTypeFromValue("Central Highlands"));
		Assert.assertEquals(QueenslandLGAs.Gladstone, QueenslandLGAs.getTypeFromValue("Gladstone"));
		Assert.assertEquals(QueenslandLGAs.Isaac, QueenslandLGAs.getTypeFromValue("Isaac"));
		Assert.assertEquals(QueenslandLGAs.Livingstone, QueenslandLGAs.getTypeFromValue("Livingstone"));
		Assert.assertEquals(QueenslandLGAs.Rockhampton, QueenslandLGAs.getTypeFromValue("Rockhampton"));
		Assert.assertEquals(QueenslandLGAs.Whitsunday, QueenslandLGAs.getTypeFromValue("Whitsunday"));
		Assert.assertEquals(QueenslandLGAs.Woorabinda, QueenslandLGAs.getTypeFromValue("Woorabinda"));
	
		Assert.assertEquals(QueenslandLGAs.Burdekin, QueenslandLGAs.getTypeFromValue("Burdekin"));
		Assert.assertEquals(QueenslandLGAs.ChartersTowers, QueenslandLGAs.getTypeFromValue("Charters Towers"));
		Assert.assertEquals(QueenslandLGAs.Hinchinbrook, QueenslandLGAs.getTypeFromValue("Hinchinbrook"));
		Assert.assertEquals(QueenslandLGAs.Mackay, QueenslandLGAs.getTypeFromValue("Mackay"));
		Assert.assertEquals(QueenslandLGAs.Mareeba, QueenslandLGAs.getTypeFromValue("Mareeba"));
		Assert.assertEquals(QueenslandLGAs.PalmIsland, QueenslandLGAs.getTypeFromValue("Palm Island"));
		Assert.assertEquals(QueenslandLGAs.Townsville, QueenslandLGAs.getTypeFromValue("Townsville"));
	
		Assert.assertEquals(QueenslandLGAs.Aurukun, QueenslandLGAs.getTypeFromValue("Aurukun"));
		Assert.assertEquals(QueenslandLGAs.Cairns, QueenslandLGAs.getTypeFromValue("Cairns"));
		Assert.assertEquals(QueenslandLGAs.CassowaryCoast, QueenslandLGAs.getTypeFromValue("Cassowary Coast"));
		Assert.assertEquals(QueenslandLGAs.Cook, QueenslandLGAs.getTypeFromValue("Cook"));
		Assert.assertEquals(QueenslandLGAs.Douglas, QueenslandLGAs.getTypeFromValue("Douglas"));
		Assert.assertEquals(QueenslandLGAs.Hopevale, QueenslandLGAs.getTypeFromValue("Hopevale"));
		Assert.assertEquals(QueenslandLGAs.Kowanyama, QueenslandLGAs.getTypeFromValue("Kowanyama"));
		Assert.assertEquals(QueenslandLGAs.LockhartRiver, QueenslandLGAs.getTypeFromValue("Lockhart River"));
		Assert.assertEquals(QueenslandLGAs.Mapoon, QueenslandLGAs.getTypeFromValue("Mapoon"));
		Assert.assertEquals(QueenslandLGAs.Napranum, QueenslandLGAs.getTypeFromValue("Napranum"));
		Assert.assertEquals(QueenslandLGAs.NorthernPeninsulaArea, QueenslandLGAs.getTypeFromValue("Northern Peninsula Area"));
		Assert.assertEquals(QueenslandLGAs.Pormpuraaw, QueenslandLGAs.getTypeFromValue("Pormpuraaw"));
		Assert.assertEquals(QueenslandLGAs.Tablelands, QueenslandLGAs.getTypeFromValue("Tablelands"));
		Assert.assertEquals(QueenslandLGAs.Torres, QueenslandLGAs.getTypeFromValue("Torres"));
		Assert.assertEquals(QueenslandLGAs.TorresStraitIsland, QueenslandLGAs.getTypeFromValue("Torres Strait Island"));
		Assert.assertEquals(QueenslandLGAs.Weipa, QueenslandLGAs.getTypeFromValue("Weipa"));
		Assert.assertEquals(QueenslandLGAs.WujalWujal, QueenslandLGAs.getTypeFromValue("Wujal Wujal"));
		Assert.assertEquals(QueenslandLGAs.Yarrabah, QueenslandLGAs.getTypeFromValue("Yarrabah"));
			
		Assert.assertEquals(QueenslandLGAs.Burke, QueenslandLGAs.getTypeFromValue("Burke"));
		Assert.assertEquals(QueenslandLGAs.Carpentaria, QueenslandLGAs.getTypeFromValue("Carpentaria"));
		Assert.assertEquals(QueenslandLGAs.Cloncurry, QueenslandLGAs.getTypeFromValue("Cloncurry"));
		Assert.assertEquals(QueenslandLGAs.Croydon, QueenslandLGAs.getTypeFromValue("Croydon"));
		Assert.assertEquals(QueenslandLGAs.Doomadgee, QueenslandLGAs.getTypeFromValue("Doomadgee"));
		Assert.assertEquals(QueenslandLGAs.Etheridge, QueenslandLGAs.getTypeFromValue("Etheridge"));
		Assert.assertEquals(QueenslandLGAs.Flinders, QueenslandLGAs.getTypeFromValue("Flinders"));
		Assert.assertEquals(QueenslandLGAs.McKinlay, QueenslandLGAs.getTypeFromValue("McKinlay"));
		Assert.assertEquals(QueenslandLGAs.Mornington, QueenslandLGAs.getTypeFromValue("Mornington"));
		Assert.assertEquals(QueenslandLGAs.MountIsa, QueenslandLGAs.getTypeFromValue("Mount Isa"));
		Assert.assertEquals(QueenslandLGAs.Richmond, QueenslandLGAs.getTypeFromValue("Richmond"));
			
		Assert.assertEquals(QueenslandLGAs.Barcaldine, QueenslandLGAs.getTypeFromValue("Barcaldine"));
		Assert.assertEquals(QueenslandLGAs.Barcoo, QueenslandLGAs.getTypeFromValue("Barcoo"));
		Assert.assertEquals(QueenslandLGAs.BlackallTambo, QueenslandLGAs.getTypeFromValue("Blackall-Tambo"));
		Assert.assertEquals(QueenslandLGAs.Boulia, QueenslandLGAs.getTypeFromValue("Boulia"));
		Assert.assertEquals(QueenslandLGAs.Diamantina, QueenslandLGAs.getTypeFromValue("Diamantina"));
		Assert.assertEquals(QueenslandLGAs.Longreach, QueenslandLGAs.getTypeFromValue("Longreach"));
		Assert.assertEquals(QueenslandLGAs.Winton, QueenslandLGAs.getTypeFromValue("Winton"));
	
		Assert.assertEquals(QueenslandLGAs.Balonne, QueenslandLGAs.getTypeFromValue("Balonne"));
		Assert.assertEquals(QueenslandLGAs.Bulloo, QueenslandLGAs.getTypeFromValue("Bulloo"));
		Assert.assertEquals(QueenslandLGAs.Maranoa, QueenslandLGAs.getTypeFromValue("Maranoa"));
		Assert.assertEquals(QueenslandLGAs.Murweh, QueenslandLGAs.getTypeFromValue("Murweh"));
		Assert.assertEquals(QueenslandLGAs.Paroo, QueenslandLGAs.getTypeFromValue("Paroo"));
		Assert.assertEquals(QueenslandLGAs.Quilpie, QueenslandLGAs.getTypeFromValue("Quilpie"));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.QueenslandLGAs#getTypeFromAbbreviation(java.lang.String)}.
	 */
	@Test
	public void testGetTypeFromAbbreviation() {
		Assert.assertEquals(QueenslandLGAs.Brisbane, QueenslandLGAs.getTypeFromAbbreviation("BNE"));
		Assert.assertEquals(QueenslandLGAs.Brisbane, QueenslandLGAs.getTypeFromAbbreviation("bne"));		
		Assert.assertEquals(QueenslandLGAs.GoldCoast, QueenslandLGAs.getTypeFromAbbreviation("Gold Coast"));
		Assert.assertEquals(QueenslandLGAs.Ipswich, QueenslandLGAs.getTypeFromAbbreviation("Ipswich"));
		Assert.assertEquals(QueenslandLGAs.LockyerValley, QueenslandLGAs.getTypeFromAbbreviation("Lockyer Valley"));
		Assert.assertEquals(QueenslandLGAs.Logan, QueenslandLGAs.getTypeFromAbbreviation("Logan"));
		Assert.assertEquals(QueenslandLGAs.MoretonBay, QueenslandLGAs.getTypeFromAbbreviation("Moreton Bay"));
		Assert.assertEquals(QueenslandLGAs.Noosa, QueenslandLGAs.getTypeFromAbbreviation("Noosa"));
		Assert.assertEquals(QueenslandLGAs.Redland, QueenslandLGAs.getTypeFromAbbreviation("Redland"));
		Assert.assertEquals(QueenslandLGAs.ScenicRim, QueenslandLGAs.getTypeFromAbbreviation("Scenic Rim"));
		
		Assert.assertEquals(QueenslandLGAs.Somerset, QueenslandLGAs.getTypeFromAbbreviation("Somerset"));
		Assert.assertEquals(QueenslandLGAs.SunshineCoast, QueenslandLGAs.getTypeFromAbbreviation("Sunshine Coast"));	
		Assert.assertEquals(QueenslandLGAs.Bundaberg, QueenslandLGAs.getTypeFromAbbreviation("Bundaberg"));
		Assert.assertEquals(QueenslandLGAs.Cherbourg, QueenslandLGAs.getTypeFromAbbreviation("Cherbourg"));
		Assert.assertEquals(QueenslandLGAs.FraserCoast, QueenslandLGAs.getTypeFromAbbreviation("Fraser Coast"));
		Assert.assertEquals(QueenslandLGAs.Gympie, QueenslandLGAs.getTypeFromAbbreviation("Gympie"));
		Assert.assertEquals(QueenslandLGAs.NorthBurnett, QueenslandLGAs.getTypeFromAbbreviation("North Burnett"));
		Assert.assertEquals(QueenslandLGAs.SouthBurnett, QueenslandLGAs.getTypeFromAbbreviation("South Burnett"));
		Assert.assertEquals(QueenslandLGAs.Goondiwindi, QueenslandLGAs.getTypeFromAbbreviation("Goondiwindi"));
		Assert.assertEquals(QueenslandLGAs.SouthernDowns, QueenslandLGAs.getTypeFromAbbreviation("Southern Downs"));
		Assert.assertEquals(QueenslandLGAs.Toowoomba, QueenslandLGAs.getTypeFromAbbreviation("Toowoomba"));
		Assert.assertEquals(QueenslandLGAs.WesternDowns, QueenslandLGAs.getTypeFromAbbreviation("Western Downs"));
	
		Assert.assertEquals(QueenslandLGAs.Banana, QueenslandLGAs.getTypeFromAbbreviation("Banana"));
		Assert.assertEquals(QueenslandLGAs.CentralHighlands, QueenslandLGAs.getTypeFromAbbreviation("Central Highlands"));
		Assert.assertEquals(QueenslandLGAs.Gladstone, QueenslandLGAs.getTypeFromAbbreviation("Gladstone"));
		Assert.assertEquals(QueenslandLGAs.Isaac, QueenslandLGAs.getTypeFromAbbreviation("Isaac"));
		Assert.assertEquals(QueenslandLGAs.Livingstone, QueenslandLGAs.getTypeFromAbbreviation("Livingstone"));
		Assert.assertEquals(QueenslandLGAs.Rockhampton, QueenslandLGAs.getTypeFromAbbreviation("Rockhampton"));
		Assert.assertEquals(QueenslandLGAs.Whitsunday, QueenslandLGAs.getTypeFromAbbreviation("Whitsunday"));
		Assert.assertEquals(QueenslandLGAs.Woorabinda, QueenslandLGAs.getTypeFromAbbreviation("Woorabinda"));
	
		Assert.assertEquals(QueenslandLGAs.Burdekin, QueenslandLGAs.getTypeFromAbbreviation("Burdekin"));
		Assert.assertEquals(QueenslandLGAs.ChartersTowers, QueenslandLGAs.getTypeFromAbbreviation("Charters Towers"));
		Assert.assertEquals(QueenslandLGAs.Hinchinbrook, QueenslandLGAs.getTypeFromAbbreviation("Hinchinbrook"));
		Assert.assertEquals(QueenslandLGAs.Mackay, QueenslandLGAs.getTypeFromAbbreviation("Mackay"));
		Assert.assertEquals(QueenslandLGAs.Mareeba, QueenslandLGAs.getTypeFromAbbreviation("Mareeba"));
		Assert.assertEquals(QueenslandLGAs.PalmIsland, QueenslandLGAs.getTypeFromAbbreviation("Palm Island"));
		Assert.assertEquals(QueenslandLGAs.Townsville, QueenslandLGAs.getTypeFromAbbreviation("Townsville"));
	
		Assert.assertEquals(QueenslandLGAs.Aurukun, QueenslandLGAs.getTypeFromAbbreviation("Aurukun"));
		Assert.assertEquals(QueenslandLGAs.Cairns, QueenslandLGAs.getTypeFromAbbreviation("Cairns"));
		Assert.assertEquals(QueenslandLGAs.CassowaryCoast, QueenslandLGAs.getTypeFromAbbreviation("Cassowary Coast"));
		Assert.assertEquals(QueenslandLGAs.Cook, QueenslandLGAs.getTypeFromAbbreviation("Cook"));
		Assert.assertEquals(QueenslandLGAs.Douglas, QueenslandLGAs.getTypeFromAbbreviation("Douglas"));
		Assert.assertEquals(QueenslandLGAs.Hopevale, QueenslandLGAs.getTypeFromAbbreviation("Hopevale"));
		Assert.assertEquals(QueenslandLGAs.Kowanyama, QueenslandLGAs.getTypeFromAbbreviation("Kowanyama"));
		Assert.assertEquals(QueenslandLGAs.LockhartRiver, QueenslandLGAs.getTypeFromAbbreviation("Lockhart River"));
		Assert.assertEquals(QueenslandLGAs.Mapoon, QueenslandLGAs.getTypeFromAbbreviation("Mapoon"));
		Assert.assertEquals(QueenslandLGAs.Napranum, QueenslandLGAs.getTypeFromAbbreviation("Napranum"));
		Assert.assertEquals(QueenslandLGAs.NorthernPeninsulaArea, QueenslandLGAs.getTypeFromAbbreviation("Northern Peninsula Area"));
		Assert.assertEquals(QueenslandLGAs.Pormpuraaw, QueenslandLGAs.getTypeFromAbbreviation("Pormpuraaw"));
		Assert.assertEquals(QueenslandLGAs.Tablelands, QueenslandLGAs.getTypeFromAbbreviation("Tablelands"));
		Assert.assertEquals(QueenslandLGAs.Torres, QueenslandLGAs.getTypeFromAbbreviation("Torres"));
		Assert.assertEquals(QueenslandLGAs.TorresStraitIsland, QueenslandLGAs.getTypeFromAbbreviation("Torres Strait Island"));
		Assert.assertEquals(QueenslandLGAs.Weipa, QueenslandLGAs.getTypeFromAbbreviation("Weipa"));
		Assert.assertEquals(QueenslandLGAs.WujalWujal, QueenslandLGAs.getTypeFromAbbreviation("Wujal Wujal"));
		Assert.assertEquals(QueenslandLGAs.Yarrabah, QueenslandLGAs.getTypeFromAbbreviation("Yarrabah"));
			
		Assert.assertEquals(QueenslandLGAs.Burke, QueenslandLGAs.getTypeFromAbbreviation("Burke"));
		Assert.assertEquals(QueenslandLGAs.Carpentaria, QueenslandLGAs.getTypeFromAbbreviation("Carpentaria"));
		Assert.assertEquals(QueenslandLGAs.Cloncurry, QueenslandLGAs.getTypeFromAbbreviation("Cloncurry"));
		Assert.assertEquals(QueenslandLGAs.Croydon, QueenslandLGAs.getTypeFromAbbreviation("Croydon"));
		Assert.assertEquals(QueenslandLGAs.Doomadgee, QueenslandLGAs.getTypeFromAbbreviation("Doomadgee"));
		Assert.assertEquals(QueenslandLGAs.Etheridge, QueenslandLGAs.getTypeFromAbbreviation("Etheridge"));
		Assert.assertEquals(QueenslandLGAs.Flinders, QueenslandLGAs.getTypeFromAbbreviation("Flinders"));
		Assert.assertEquals(QueenslandLGAs.McKinlay, QueenslandLGAs.getTypeFromAbbreviation("McKinlay"));
		Assert.assertEquals(QueenslandLGAs.Mornington, QueenslandLGAs.getTypeFromAbbreviation("Mornington"));
		Assert.assertEquals(QueenslandLGAs.MountIsa, QueenslandLGAs.getTypeFromAbbreviation("Mount Isa"));
		Assert.assertEquals(QueenslandLGAs.Richmond, QueenslandLGAs.getTypeFromAbbreviation("Richmond"));
			
		Assert.assertEquals(QueenslandLGAs.Barcaldine, QueenslandLGAs.getTypeFromAbbreviation("Barcaldine"));
		Assert.assertEquals(QueenslandLGAs.Barcoo, QueenslandLGAs.getTypeFromAbbreviation("Barcoo"));
		Assert.assertEquals(QueenslandLGAs.BlackallTambo, QueenslandLGAs.getTypeFromAbbreviation("Blackall-Tambo"));
		Assert.assertEquals(QueenslandLGAs.Boulia, QueenslandLGAs.getTypeFromAbbreviation("Boulia"));
		Assert.assertEquals(QueenslandLGAs.Diamantina, QueenslandLGAs.getTypeFromAbbreviation("Diamantina"));
		Assert.assertEquals(QueenslandLGAs.Longreach, QueenslandLGAs.getTypeFromAbbreviation("Longreach"));
		Assert.assertEquals(QueenslandLGAs.Winton, QueenslandLGAs.getTypeFromAbbreviation("Winton"));
	
		Assert.assertEquals(QueenslandLGAs.Balonne, QueenslandLGAs.getTypeFromAbbreviation("Balonne"));
		Assert.assertEquals(QueenslandLGAs.Bulloo, QueenslandLGAs.getTypeFromAbbreviation("Bulloo"));
		Assert.assertEquals(QueenslandLGAs.Maranoa, QueenslandLGAs.getTypeFromAbbreviation("Maranoa"));
		Assert.assertEquals(QueenslandLGAs.Murweh, QueenslandLGAs.getTypeFromAbbreviation("Murweh"));
		Assert.assertEquals(QueenslandLGAs.Paroo, QueenslandLGAs.getTypeFromAbbreviation("Paroo"));
		Assert.assertEquals(QueenslandLGAs.Quilpie, QueenslandLGAs.getTypeFromAbbreviation("Quilpie"));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.QueenslandLGAs#values()}.
	 */
	@Test
	public void testValues() {
		List<QueenslandLGAs> values = new ArrayList<QueenslandLGAs>(Arrays.asList(QueenslandLGAs.values()));
		
		Assert.assertTrue(values.contains(QueenslandLGAs.Brisbane));
		Assert.assertTrue(values.contains(QueenslandLGAs.GoldCoast));
		Assert.assertTrue(values.contains(QueenslandLGAs.Ipswich));
		Assert.assertTrue(values.contains(QueenslandLGAs.LockyerValley));
		Assert.assertTrue(values.contains(QueenslandLGAs.Logan));
		Assert.assertTrue(values.contains(QueenslandLGAs.MoretonBay));
		Assert.assertTrue(values.contains(QueenslandLGAs.Noosa));
		Assert.assertTrue(values.contains(QueenslandLGAs.Redland));
		Assert.assertTrue(values.contains(QueenslandLGAs.ScenicRim));
		Assert.assertTrue(values.contains(QueenslandLGAs.Somerset));
		Assert.assertTrue(values.contains(QueenslandLGAs.SunshineCoast));
		
		Assert.assertTrue(values.contains(QueenslandLGAs.Bundaberg));
		Assert.assertTrue(values.contains(QueenslandLGAs.Cherbourg));
		Assert.assertTrue(values.contains(QueenslandLGAs.FraserCoast));
		Assert.assertTrue(values.contains(QueenslandLGAs.Gympie));
		Assert.assertTrue(values.contains(QueenslandLGAs.NorthBurnett));
		Assert.assertTrue(values.contains(QueenslandLGAs.SouthBurnett));
		Assert.assertTrue(values.contains(QueenslandLGAs.Goondiwindi));
		Assert.assertTrue(values.contains(QueenslandLGAs.SouthernDowns));
		Assert.assertTrue(values.contains(QueenslandLGAs.Toowoomba));
		Assert.assertTrue(values.contains(QueenslandLGAs.WesternDowns));
	
		Assert.assertTrue(values.contains(QueenslandLGAs.Banana));
		Assert.assertTrue(values.contains(QueenslandLGAs.CentralHighlands));
		Assert.assertTrue(values.contains(QueenslandLGAs.Gladstone));
		Assert.assertTrue(values.contains(QueenslandLGAs.Isaac));
		Assert.assertTrue(values.contains(QueenslandLGAs.Livingstone));
		Assert.assertTrue(values.contains(QueenslandLGAs.Rockhampton));
		Assert.assertTrue(values.contains(QueenslandLGAs.Whitsunday));
		Assert.assertTrue(values.contains(QueenslandLGAs.Woorabinda));
	
		Assert.assertTrue(values.contains(QueenslandLGAs.Burdekin));
		Assert.assertTrue(values.contains(QueenslandLGAs.ChartersTowers));
		Assert.assertTrue(values.contains(QueenslandLGAs.Hinchinbrook));
		Assert.assertTrue(values.contains(QueenslandLGAs.Mackay));
		Assert.assertTrue(values.contains(QueenslandLGAs.Mareeba));
		Assert.assertTrue(values.contains(QueenslandLGAs.PalmIsland));
		Assert.assertTrue(values.contains(QueenslandLGAs.Townsville));
	
		Assert.assertTrue(values.contains(QueenslandLGAs.Aurukun));
		Assert.assertTrue(values.contains(QueenslandLGAs.Cairns));
		Assert.assertTrue(values.contains(QueenslandLGAs.CassowaryCoast));
		Assert.assertTrue(values.contains(QueenslandLGAs.Cook));
		Assert.assertTrue(values.contains(QueenslandLGAs.Douglas));
		Assert.assertTrue(values.contains(QueenslandLGAs.Hopevale));
		Assert.assertTrue(values.contains(QueenslandLGAs.Kowanyama));
		Assert.assertTrue(values.contains(QueenslandLGAs.LockhartRiver));
		Assert.assertTrue(values.contains(QueenslandLGAs.Mapoon));
		Assert.assertTrue(values.contains(QueenslandLGAs.Napranum));
		Assert.assertTrue(values.contains(QueenslandLGAs.NorthernPeninsulaArea));
		Assert.assertTrue(values.contains(QueenslandLGAs.Pormpuraaw));
		Assert.assertTrue(values.contains(QueenslandLGAs.Tablelands));
		Assert.assertTrue(values.contains(QueenslandLGAs.Torres));
		Assert.assertTrue(values.contains(QueenslandLGAs.TorresStraitIsland));
		Assert.assertTrue(values.contains(QueenslandLGAs.Weipa));
		Assert.assertTrue(values.contains(QueenslandLGAs.WujalWujal));
		Assert.assertTrue(values.contains(QueenslandLGAs.Yarrabah));
			
		Assert.assertTrue(values.contains(QueenslandLGAs.Burke));
		Assert.assertTrue(values.contains(QueenslandLGAs.Carpentaria));
		Assert.assertTrue(values.contains(QueenslandLGAs.Cloncurry));
		Assert.assertTrue(values.contains(QueenslandLGAs.Croydon));
		Assert.assertTrue(values.contains(QueenslandLGAs.Doomadgee));
		Assert.assertTrue(values.contains(QueenslandLGAs.Etheridge));
		Assert.assertTrue(values.contains(QueenslandLGAs.Flinders));
		Assert.assertTrue(values.contains(QueenslandLGAs.McKinlay));
		Assert.assertTrue(values.contains(QueenslandLGAs.Mornington));
		Assert.assertTrue(values.contains(QueenslandLGAs.MountIsa));
		Assert.assertTrue(values.contains(QueenslandLGAs.Richmond));
			
		Assert.assertTrue(values.contains(QueenslandLGAs.Barcaldine));
		Assert.assertTrue(values.contains(QueenslandLGAs.Barcoo));
		Assert.assertTrue(values.contains(QueenslandLGAs.BlackallTambo));
		Assert.assertTrue(values.contains(QueenslandLGAs.Boulia));
		Assert.assertTrue(values.contains(QueenslandLGAs.Diamantina));
		Assert.assertTrue(values.contains(QueenslandLGAs.Longreach));
		Assert.assertTrue(values.contains(QueenslandLGAs.Winton));
	
		Assert.assertTrue(values.contains(QueenslandLGAs.Balonne));
		Assert.assertTrue(values.contains(QueenslandLGAs.Bulloo));
		Assert.assertTrue(values.contains(QueenslandLGAs.Maranoa));
		Assert.assertTrue(values.contains(QueenslandLGAs.Murweh));
		Assert.assertTrue(values.contains(QueenslandLGAs.Paroo));
		Assert.assertTrue(values.contains(QueenslandLGAs.Quilpie));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.australia.QueenslandLGAs#getRegion()}.
	 */
	@Test
	public void testGetRegion() {
		Assert.assertEquals("South East", QueenslandLGAs.Brisbane.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.Brisbane.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.GoldCoast.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.Ipswich.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.LockyerValley.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.Logan.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.MoretonBay.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.Noosa.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.Redland.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.ScenicRim.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.Somerset.getRegion());
		Assert.assertEquals("South East", QueenslandLGAs.SunshineCoast.getRegion());
		
		Assert.assertEquals("Wide Bay-Burnett", QueenslandLGAs.Bundaberg.getRegion());
		Assert.assertEquals("Wide Bay-Burnett", QueenslandLGAs.Cherbourg.getRegion());
		Assert.assertEquals("Wide Bay-Burnett", QueenslandLGAs.FraserCoast.getRegion());
		Assert.assertEquals("Wide Bay-Burnett", QueenslandLGAs.Gympie.getRegion());
		Assert.assertEquals("Wide Bay-Burnett", QueenslandLGAs.NorthBurnett.getRegion());
		Assert.assertEquals("Wide Bay-Burnett", QueenslandLGAs.SouthBurnett.getRegion());
		
		Assert.assertEquals("Darling Downs", QueenslandLGAs.Goondiwindi.getRegion());
		Assert.assertEquals("Darling Downs", QueenslandLGAs.SouthernDowns.getRegion());
		Assert.assertEquals("Darling Downs", QueenslandLGAs.Toowoomba.getRegion());
		Assert.assertEquals("Darling Downs", QueenslandLGAs.WesternDowns.getRegion());
	
		Assert.assertEquals("Central", QueenslandLGAs.Banana.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.CentralHighlands.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.Gladstone.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.Isaac.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.Livingstone.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.Rockhampton.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.Whitsunday.getRegion());
		Assert.assertEquals("Central", QueenslandLGAs.Woorabinda.getRegion());
	
		Assert.assertEquals("North", QueenslandLGAs.Burdekin.getRegion());
		Assert.assertEquals("North", QueenslandLGAs.ChartersTowers.getRegion());
		Assert.assertEquals("North", QueenslandLGAs.Hinchinbrook.getRegion());
		Assert.assertEquals("North", QueenslandLGAs.Mackay.getRegion());
		Assert.assertEquals("North", QueenslandLGAs.Mareeba.getRegion());
		Assert.assertEquals("North", QueenslandLGAs.PalmIsland.getRegion());
		Assert.assertEquals("North", QueenslandLGAs.Townsville.getRegion());
	
		Assert.assertEquals("Far North", QueenslandLGAs.Aurukun.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Cairns.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.CassowaryCoast.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Cook.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Douglas.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Hopevale.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Kowanyama.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.LockhartRiver.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Mapoon.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Napranum.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.NorthernPeninsulaArea.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Pormpuraaw.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Tablelands.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Torres.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.TorresStraitIsland.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Weipa.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.WujalWujal.getRegion());
		Assert.assertEquals("Far North", QueenslandLGAs.Yarrabah.getRegion());
			
		Assert.assertEquals("North West", QueenslandLGAs.Burke.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Carpentaria.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Cloncurry.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Croydon.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Doomadgee.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Etheridge.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Flinders.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.McKinlay.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Mornington.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.MountIsa.getRegion());
		Assert.assertEquals("North West", QueenslandLGAs.Richmond.getRegion());
			
		Assert.assertEquals("Central West", QueenslandLGAs.Barcaldine.getRegion());
		Assert.assertEquals("Central West", QueenslandLGAs.Barcoo.getRegion());
		Assert.assertEquals("Central West", QueenslandLGAs.BlackallTambo.getRegion());
		Assert.assertEquals("Central West", QueenslandLGAs.Boulia.getRegion());
		Assert.assertEquals("Central West", QueenslandLGAs.Diamantina.getRegion());
		Assert.assertEquals("Central West", QueenslandLGAs.Longreach.getRegion());
		Assert.assertEquals("Central West", QueenslandLGAs.Winton.getRegion());
	
		Assert.assertEquals("South West", QueenslandLGAs.Balonne.getRegion());
		Assert.assertEquals("South West", QueenslandLGAs.Bulloo.getRegion());
		Assert.assertEquals("South West", QueenslandLGAs.Maranoa.getRegion());
		Assert.assertEquals("South West", QueenslandLGAs.Murweh.getRegion());
		Assert.assertEquals("South West", QueenslandLGAs.Paroo.getRegion());
		Assert.assertEquals("South West", QueenslandLGAs.Quilpie.getRegion());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.AbstractAbbrevitableEnum#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		List<QueenslandLGAs> cities = new ArrayList<>();
		List<QueenslandLGAs> cities2 = new ArrayList<>();
		
		cities.add(QueenslandLGAs.Brisbane);
		cities.add(QueenslandLGAs.GoldCoast);
		cities.add(QueenslandLGAs.Ipswich);
		cities.add(QueenslandLGAs.LockyerValley);
		cities.add(QueenslandLGAs.Logan);
		cities.add(QueenslandLGAs.MoretonBay);
		cities.add(QueenslandLGAs.Noosa);
		cities.add(QueenslandLGAs.Redland);
		cities.add(QueenslandLGAs.ScenicRim);
		cities.add(QueenslandLGAs.Somerset);
		cities.add(QueenslandLGAs.SunshineCoast);
		
		cities.add(QueenslandLGAs.Bundaberg);
		cities.add(QueenslandLGAs.Cherbourg);
		cities.add(QueenslandLGAs.FraserCoast);
		cities.add(QueenslandLGAs.Gympie);
		cities.add(QueenslandLGAs.NorthBurnett);
		cities.add(QueenslandLGAs.SouthBurnett);
		cities.add(QueenslandLGAs.Goondiwindi);
		cities.add(QueenslandLGAs.SouthernDowns);
		cities.add(QueenslandLGAs.Toowoomba);
		cities.add(QueenslandLGAs.WesternDowns);
	
		cities.add(QueenslandLGAs.Banana);
		cities.add(QueenslandLGAs.CentralHighlands);
		cities.add(QueenslandLGAs.Gladstone);
		cities.add(QueenslandLGAs.Isaac);
		cities.add(QueenslandLGAs.Livingstone);
		cities.add(QueenslandLGAs.Rockhampton);
		cities.add(QueenslandLGAs.Whitsunday);
		cities.add(QueenslandLGAs.Woorabinda);
	
		cities.add(QueenslandLGAs.Burdekin);
		cities.add(QueenslandLGAs.ChartersTowers);
		cities.add(QueenslandLGAs.Hinchinbrook);
		cities.add(QueenslandLGAs.Mackay);
		cities.add(QueenslandLGAs.Mareeba);
		cities.add(QueenslandLGAs.PalmIsland);
		cities.add(QueenslandLGAs.Townsville);
	
		cities.add(QueenslandLGAs.Aurukun);
		cities.add(QueenslandLGAs.Cairns);
		cities.add(QueenslandLGAs.CassowaryCoast);
		cities.add(QueenslandLGAs.Cook);
		cities.add(QueenslandLGAs.Douglas);
		cities.add(QueenslandLGAs.Hopevale);
		cities.add(QueenslandLGAs.Kowanyama);
		cities.add(QueenslandLGAs.LockhartRiver);
		cities.add(QueenslandLGAs.Mapoon);
		cities.add(QueenslandLGAs.Napranum);
		cities.add(QueenslandLGAs.NorthernPeninsulaArea);
		cities.add(QueenslandLGAs.Pormpuraaw);
		cities.add(QueenslandLGAs.Tablelands);
		cities.add(QueenslandLGAs.Torres);
		cities.add(QueenslandLGAs.TorresStraitIsland);
		cities.add(QueenslandLGAs.Weipa);
		cities.add(QueenslandLGAs.WujalWujal);
		cities.add(QueenslandLGAs.Yarrabah);
			
		cities.add(QueenslandLGAs.Burke);
		cities.add(QueenslandLGAs.Carpentaria);
		cities.add(QueenslandLGAs.Cloncurry);
		cities.add(QueenslandLGAs.Croydon);
		cities.add(QueenslandLGAs.Doomadgee);
		cities.add(QueenslandLGAs.Etheridge);
		cities.add(QueenslandLGAs.Flinders);
		cities.add(QueenslandLGAs.McKinlay);
		cities.add(QueenslandLGAs.Mornington);
		cities.add(QueenslandLGAs.MountIsa);
		cities.add(QueenslandLGAs.Richmond);
			
		cities.add(QueenslandLGAs.Barcaldine);
		cities.add(QueenslandLGAs.Barcoo);
		cities.add(QueenslandLGAs.BlackallTambo);
		cities.add(QueenslandLGAs.Boulia);
		cities.add(QueenslandLGAs.Diamantina);
		cities.add(QueenslandLGAs.Longreach);
		cities.add(QueenslandLGAs.Winton);
	
		cities.add(QueenslandLGAs.Balonne);
		cities.add(QueenslandLGAs.Bulloo);
		cities.add(QueenslandLGAs.Maranoa);
		cities.add(QueenslandLGAs.Murweh);
		cities.add(QueenslandLGAs.Paroo);
		cities.add(QueenslandLGAs.Quilpie);
		
		cities2.add(QueenslandLGAs.Brisbane);
		cities2.add(QueenslandLGAs.GoldCoast);
		cities2.add(QueenslandLGAs.Ipswich);
		cities2.add(QueenslandLGAs.LockyerValley);
		cities2.add(QueenslandLGAs.Logan);
		cities2.add(QueenslandLGAs.MoretonBay);
		cities2.add(QueenslandLGAs.Noosa);
		cities2.add(QueenslandLGAs.Redland);
		cities2.add(QueenslandLGAs.ScenicRim);
		cities2.add(QueenslandLGAs.Somerset);
		cities2.add(QueenslandLGAs.SunshineCoast);
		
		cities2.add(QueenslandLGAs.Bundaberg);
		cities2.add(QueenslandLGAs.Cherbourg);
		cities2.add(QueenslandLGAs.FraserCoast);
		cities2.add(QueenslandLGAs.Gympie);
		cities2.add(QueenslandLGAs.NorthBurnett);
		cities2.add(QueenslandLGAs.SouthBurnett);
		cities2.add(QueenslandLGAs.Goondiwindi);
		cities2.add(QueenslandLGAs.SouthernDowns);
		cities2.add(QueenslandLGAs.Toowoomba);
		cities2.add(QueenslandLGAs.WesternDowns);
	
		cities2.add(QueenslandLGAs.Banana);
		cities2.add(QueenslandLGAs.CentralHighlands);
		cities2.add(QueenslandLGAs.Gladstone);
		cities2.add(QueenslandLGAs.Isaac);
		cities2.add(QueenslandLGAs.Livingstone);
		cities2.add(QueenslandLGAs.Rockhampton);
		cities2.add(QueenslandLGAs.Whitsunday);
		cities2.add(QueenslandLGAs.Woorabinda);
	
		cities2.add(QueenslandLGAs.Burdekin);
		cities2.add(QueenslandLGAs.ChartersTowers);
		cities2.add(QueenslandLGAs.Hinchinbrook);
		cities2.add(QueenslandLGAs.Mackay);
		cities2.add(QueenslandLGAs.Mareeba);
		cities2.add(QueenslandLGAs.PalmIsland);
		cities2.add(QueenslandLGAs.Townsville);
	
		cities2.add(QueenslandLGAs.Aurukun);
		cities2.add(QueenslandLGAs.Cairns);
		cities2.add(QueenslandLGAs.CassowaryCoast);
		cities2.add(QueenslandLGAs.Cook);
		cities2.add(QueenslandLGAs.Douglas);
		cities2.add(QueenslandLGAs.Hopevale);
		cities2.add(QueenslandLGAs.Kowanyama);
		cities2.add(QueenslandLGAs.LockhartRiver);
		cities2.add(QueenslandLGAs.Mapoon);
		cities2.add(QueenslandLGAs.Napranum);
		cities2.add(QueenslandLGAs.NorthernPeninsulaArea);
		cities2.add(QueenslandLGAs.Pormpuraaw);
		cities2.add(QueenslandLGAs.Tablelands);
		cities2.add(QueenslandLGAs.Torres);
		cities2.add(QueenslandLGAs.TorresStraitIsland);
		cities2.add(QueenslandLGAs.Weipa);
		cities2.add(QueenslandLGAs.WujalWujal);
		cities2.add(QueenslandLGAs.Yarrabah);
			
		cities2.add(QueenslandLGAs.Burke);
		cities2.add(QueenslandLGAs.Carpentaria);
		cities2.add(QueenslandLGAs.Cloncurry);
		cities2.add(QueenslandLGAs.Croydon);
		cities2.add(QueenslandLGAs.Doomadgee);
		cities2.add(QueenslandLGAs.Etheridge);
		cities2.add(QueenslandLGAs.Flinders);
		cities2.add(QueenslandLGAs.McKinlay);
		cities2.add(QueenslandLGAs.Mornington);
		cities2.add(QueenslandLGAs.MountIsa);
		cities2.add(QueenslandLGAs.Richmond);
			
		cities2.add(QueenslandLGAs.Barcaldine);
		cities2.add(QueenslandLGAs.Barcoo);
		cities2.add(QueenslandLGAs.BlackallTambo);
		cities2.add(QueenslandLGAs.Boulia);
		cities2.add(QueenslandLGAs.Diamantina);
		cities2.add(QueenslandLGAs.Longreach);
		cities2.add(QueenslandLGAs.Winton);
	
		cities2.add(QueenslandLGAs.Balonne);
		cities2.add(QueenslandLGAs.Bulloo);
		cities2.add(QueenslandLGAs.Maranoa);
		cities2.add(QueenslandLGAs.Murweh);
		cities2.add(QueenslandLGAs.Paroo);
		cities2.add(QueenslandLGAs.Quilpie);
		
		for (int i = 0; i < cities.size(); i++) {
			Assert.assertEquals(cities.get(i).hashCode(), cities2.get(i).hashCode());			
		}
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.AbstractAbbrevitableEnum#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		List<QueenslandLGAs> cities = new ArrayList<>();
		List<QueenslandLGAs> cities2 = new ArrayList<>();
		
		cities.add(QueenslandLGAs.Brisbane);
		cities.add(QueenslandLGAs.GoldCoast);
		cities.add(QueenslandLGAs.Ipswich);
		cities.add(QueenslandLGAs.LockyerValley);
		cities.add(QueenslandLGAs.Logan);
		cities.add(QueenslandLGAs.MoretonBay);
		cities.add(QueenslandLGAs.Noosa);
		cities.add(QueenslandLGAs.Redland);
		cities.add(QueenslandLGAs.ScenicRim);
		cities.add(QueenslandLGAs.Somerset);
		cities.add(QueenslandLGAs.SunshineCoast);
		
		cities.add(QueenslandLGAs.Bundaberg);
		cities.add(QueenslandLGAs.Cherbourg);
		cities.add(QueenslandLGAs.FraserCoast);
		cities.add(QueenslandLGAs.Gympie);
		cities.add(QueenslandLGAs.NorthBurnett);
		cities.add(QueenslandLGAs.SouthBurnett);
		cities.add(QueenslandLGAs.Goondiwindi);
		cities.add(QueenslandLGAs.SouthernDowns);
		cities.add(QueenslandLGAs.Toowoomba);
		cities.add(QueenslandLGAs.WesternDowns);
	
		cities.add(QueenslandLGAs.Banana);
		cities.add(QueenslandLGAs.CentralHighlands);
		cities.add(QueenslandLGAs.Gladstone);
		cities.add(QueenslandLGAs.Isaac);
		cities.add(QueenslandLGAs.Livingstone);
		cities.add(QueenslandLGAs.Rockhampton);
		cities.add(QueenslandLGAs.Whitsunday);
		cities.add(QueenslandLGAs.Woorabinda);
	
		cities.add(QueenslandLGAs.Burdekin);
		cities.add(QueenslandLGAs.ChartersTowers);
		cities.add(QueenslandLGAs.Hinchinbrook);
		cities.add(QueenslandLGAs.Mackay);
		cities.add(QueenslandLGAs.Mareeba);
		cities.add(QueenslandLGAs.PalmIsland);
		cities.add(QueenslandLGAs.Townsville);
	
		cities.add(QueenslandLGAs.Aurukun);
		cities.add(QueenslandLGAs.Cairns);
		cities.add(QueenslandLGAs.CassowaryCoast);
		cities.add(QueenslandLGAs.Cook);
		cities.add(QueenslandLGAs.Douglas);
		cities.add(QueenslandLGAs.Hopevale);
		cities.add(QueenslandLGAs.Kowanyama);
		cities.add(QueenslandLGAs.LockhartRiver);
		cities.add(QueenslandLGAs.Mapoon);
		cities.add(QueenslandLGAs.Napranum);
		cities.add(QueenslandLGAs.NorthernPeninsulaArea);
		cities.add(QueenslandLGAs.Pormpuraaw);
		cities.add(QueenslandLGAs.Tablelands);
		cities.add(QueenslandLGAs.Torres);
		cities.add(QueenslandLGAs.TorresStraitIsland);
		cities.add(QueenslandLGAs.Weipa);
		cities.add(QueenslandLGAs.WujalWujal);
		cities.add(QueenslandLGAs.Yarrabah);
			
		cities.add(QueenslandLGAs.Burke);
		cities.add(QueenslandLGAs.Carpentaria);
		cities.add(QueenslandLGAs.Cloncurry);
		cities.add(QueenslandLGAs.Croydon);
		cities.add(QueenslandLGAs.Doomadgee);
		cities.add(QueenslandLGAs.Etheridge);
		cities.add(QueenslandLGAs.Flinders);
		cities.add(QueenslandLGAs.McKinlay);
		cities.add(QueenslandLGAs.Mornington);
		cities.add(QueenslandLGAs.MountIsa);
		cities.add(QueenslandLGAs.Richmond);
			
		cities.add(QueenslandLGAs.Barcaldine);
		cities.add(QueenslandLGAs.Barcoo);
		cities.add(QueenslandLGAs.BlackallTambo);
		cities.add(QueenslandLGAs.Boulia);
		cities.add(QueenslandLGAs.Diamantina);
		cities.add(QueenslandLGAs.Longreach);
		cities.add(QueenslandLGAs.Winton);
	
		cities.add(QueenslandLGAs.Balonne);
		cities.add(QueenslandLGAs.Bulloo);
		cities.add(QueenslandLGAs.Maranoa);
		cities.add(QueenslandLGAs.Murweh);
		cities.add(QueenslandLGAs.Paroo);
		cities.add(QueenslandLGAs.Quilpie);
		
		cities2.add(QueenslandLGAs.Brisbane);
		cities2.add(QueenslandLGAs.GoldCoast);
		cities2.add(QueenslandLGAs.Ipswich);
		cities2.add(QueenslandLGAs.LockyerValley);
		cities2.add(QueenslandLGAs.Logan);
		cities2.add(QueenslandLGAs.MoretonBay);
		cities2.add(QueenslandLGAs.Noosa);
		cities2.add(QueenslandLGAs.Redland);
		cities2.add(QueenslandLGAs.ScenicRim);
		cities2.add(QueenslandLGAs.Somerset);
		cities2.add(QueenslandLGAs.SunshineCoast);
		
		cities2.add(QueenslandLGAs.Bundaberg);
		cities2.add(QueenslandLGAs.Cherbourg);
		cities2.add(QueenslandLGAs.FraserCoast);
		cities2.add(QueenslandLGAs.Gympie);
		cities2.add(QueenslandLGAs.NorthBurnett);
		cities2.add(QueenslandLGAs.SouthBurnett);
		cities2.add(QueenslandLGAs.Goondiwindi);
		cities2.add(QueenslandLGAs.SouthernDowns);
		cities2.add(QueenslandLGAs.Toowoomba);
		cities2.add(QueenslandLGAs.WesternDowns);
	
		cities2.add(QueenslandLGAs.Banana);
		cities2.add(QueenslandLGAs.CentralHighlands);
		cities2.add(QueenslandLGAs.Gladstone);
		cities2.add(QueenslandLGAs.Isaac);
		cities2.add(QueenslandLGAs.Livingstone);
		cities2.add(QueenslandLGAs.Rockhampton);
		cities2.add(QueenslandLGAs.Whitsunday);
		cities2.add(QueenslandLGAs.Woorabinda);
	
		cities2.add(QueenslandLGAs.Burdekin);
		cities2.add(QueenslandLGAs.ChartersTowers);
		cities2.add(QueenslandLGAs.Hinchinbrook);
		cities2.add(QueenslandLGAs.Mackay);
		cities2.add(QueenslandLGAs.Mareeba);
		cities2.add(QueenslandLGAs.PalmIsland);
		cities2.add(QueenslandLGAs.Townsville);
	
		cities2.add(QueenslandLGAs.Aurukun);
		cities2.add(QueenslandLGAs.Cairns);
		cities2.add(QueenslandLGAs.CassowaryCoast);
		cities2.add(QueenslandLGAs.Cook);
		cities2.add(QueenslandLGAs.Douglas);
		cities2.add(QueenslandLGAs.Hopevale);
		cities2.add(QueenslandLGAs.Kowanyama);
		cities2.add(QueenslandLGAs.LockhartRiver);
		cities2.add(QueenslandLGAs.Mapoon);
		cities2.add(QueenslandLGAs.Napranum);
		cities2.add(QueenslandLGAs.NorthernPeninsulaArea);
		cities2.add(QueenslandLGAs.Pormpuraaw);
		cities2.add(QueenslandLGAs.Tablelands);
		cities2.add(QueenslandLGAs.Torres);
		cities2.add(QueenslandLGAs.TorresStraitIsland);
		cities2.add(QueenslandLGAs.Weipa);
		cities2.add(QueenslandLGAs.WujalWujal);
		cities2.add(QueenslandLGAs.Yarrabah);
			
		cities2.add(QueenslandLGAs.Burke);
		cities2.add(QueenslandLGAs.Carpentaria);
		cities2.add(QueenslandLGAs.Cloncurry);
		cities2.add(QueenslandLGAs.Croydon);
		cities2.add(QueenslandLGAs.Doomadgee);
		cities2.add(QueenslandLGAs.Etheridge);
		cities2.add(QueenslandLGAs.Flinders);
		cities2.add(QueenslandLGAs.McKinlay);
		cities2.add(QueenslandLGAs.Mornington);
		cities2.add(QueenslandLGAs.MountIsa);
		cities2.add(QueenslandLGAs.Richmond);
			
		cities2.add(QueenslandLGAs.Barcaldine);
		cities2.add(QueenslandLGAs.Barcoo);
		cities2.add(QueenslandLGAs.BlackallTambo);
		cities2.add(QueenslandLGAs.Boulia);
		cities2.add(QueenslandLGAs.Diamantina);
		cities2.add(QueenslandLGAs.Longreach);
		cities2.add(QueenslandLGAs.Winton);
	
		cities2.add(QueenslandLGAs.Balonne);
		cities2.add(QueenslandLGAs.Bulloo);
		cities2.add(QueenslandLGAs.Maranoa);
		cities2.add(QueenslandLGAs.Murweh);
		cities2.add(QueenslandLGAs.Paroo);
		cities2.add(QueenslandLGAs.Quilpie);
		
		for (int i = 0; i < cities.size(); i++) {
			Assert.assertEquals(cities.get(i), cities2.get(i));			
		}
	}
}
