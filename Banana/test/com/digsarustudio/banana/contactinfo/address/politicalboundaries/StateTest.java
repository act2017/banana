/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.AustralianStates;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;

/**
 * To test State object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class StateTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = state;
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCodeWithSameValue() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCodeDifferentValue() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Victoria).build();
		assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCodeSameSubdivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertTrue(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffSubdivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertNotEquals(lgas, lgas1);
		
		assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCodeSameSubdivisionButDiffDivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Victoria).build();
		assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		assertNotEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertFalse(state.equals(state1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffSubdivisionButDiffDivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Victoria).build();
		assertEquals(AustralianStates.Victoria, state1.getDivision());
		
		assertNotEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertNotEquals(lgas, lgas1);
		
		assertFalse(state.equals(state1));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#getSubdivision()}.
	 */
	@Test
	public void testGetSubdivision() {
		State states = new State.Builder(AustralianStates.Queensland).build();
		assertNull(states.getSubdivision());
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertNull(lgas.getSubdivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#setSubdivision(com.digsarustudio.banana.contactinfo.address.territory.IStateConstituent)}.
	 */
	@Test
	public void testSetSubdivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertNull(state.getSubdivision());
		
		City city = new City.Builder(QueenslandLGAs.Brisbane).build();
		state.setSubdivision(city);
		
		assertEquals(city, state.getSubdivision());
		assertEquals(QueenslandLGAs.Brisbane, state.getSubdivision().getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#getDivision()}.
	 */
	@Test
	public void testGetName() {
		State states = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, states.getDivision());
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#setDivision(com.digsarustudio.banana.contactinfo.address.IEnumAbbreviatable)}.
	 */
	@Test
	public void testSetName() {
		State states = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, states.getDivision());
		
		states.setDivision(AustralianStates.NewSouthWales);
		assertEquals(AustralianStates.NewSouthWales, states.getDivision());
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		lgas.setDivision(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, lgas.getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = state;
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertEquals(state, state1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObjectInSameValue() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertEquals(state, state1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsObject() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.NewSouthWales).build();
		assertEquals(AustralianStates.NewSouthWales, state1.getDivision());
		
		assertNotEquals(state, state1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSubdivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertEquals(state, state1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsSubdivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state1.getDivision());
		
		assertEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);		
		assertNotEquals(lgas, lgas1);
		
		assertNotEquals(state, state1);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsDivision() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.NewSouthWales).build();
		assertEquals(AustralianStates.NewSouthWales, state1.getDivision());
		
		assertNotEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertNotEquals(lgas, lgas1);
		
		assertNotEquals(state, state1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.State#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSubdivisionButNotEqualsDivisions() {
		State state = new State.Builder(AustralianStates.Queensland).build();
		assertEquals(AustralianStates.Queensland, state.getDivision());
		
		State state1 = new State.Builder(AustralianStates.NewSouthWales).build();
		assertEquals(AustralianStates.NewSouthWales, state1.getDivision());
		
		assertNotEquals(state, state1);
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		City lgas1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas1.getDivision());
		
		state.setSubdivision(lgas);
		state1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertNotEquals(state, state1);
	}

}
