/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.BrisbaneSuburbs;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;

/**
 * To test City object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class CityTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCode() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = City;
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertTrue(City.equals(City1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCodeWithSameValue() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertTrue(City.equals(City1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCodeDifferentValue() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Gladstone).build();
		assertEquals(QueenslandLGAs.Gladstone, City1.getDivision());
		
		assertFalse(City.equals(City1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCodeSameSubdivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.Graceville).build();
		assertEquals(BrisbaneSuburbs.Graceville, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.Graceville).build();
		assertEquals(BrisbaneSuburbs.Graceville, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertTrue(City.equals(City1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffSubdivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.Graceville).build();
		assertEquals(BrisbaneSuburbs.Graceville, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.Zillmere).build();
		assertEquals(BrisbaneSuburbs.Zillmere, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertNotEquals(lgas, lgas1);
		
		assertFalse(City.equals(City1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCodeSameSubdivisionButDiffDivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Diamantina).build();
		assertEquals(QueenslandLGAs.Diamantina, City1.getDivision());
		
		assertNotEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.Graceville).build();
		assertEquals(BrisbaneSuburbs.Graceville, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.Graceville).build();
		assertEquals(BrisbaneSuburbs.Graceville, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertFalse(City.equals(City1));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#hashCode()}.
	 */
	@Test
	public void testHashCodeDiffSubdivisionButDiffDivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.LockyerValley).build();
		assertEquals(QueenslandLGAs.LockyerValley, City1.getDivision());
		
		assertNotEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.Graceville).build();
		assertEquals(BrisbaneSuburbs.Graceville, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.Zillmere).build();
		assertEquals(BrisbaneSuburbs.Zillmere, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertNotEquals(lgas, lgas1);
		
		assertFalse(City.equals(City1));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#getSubdivision()}.
	 */
	@Test
	public void testGetSubdivision() {
		City Citys = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertNull(Citys.getSubdivision());
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.CowanCowan).build();
		assertNull(lgas.getSubdivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#setSubdivision(com.digsarustudio.banana.contactinfo.address.territory.ICityConstituent)}.
	 */
	@Test
	public void testSetSubdivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertNull(City.getSubdivision());
		
		Suburb suburb = new Suburb.Builder(BrisbaneSuburbs.GordonPark).build();
		City.setSubdivision(suburb);
		
		assertEquals(suburb, City.getSubdivision());
		assertEquals(BrisbaneSuburbs.GordonPark, City.getSubdivision().getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#getDivision()}.
	 */
	@Test
	public void testGetName() {
		City Citys = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, Citys.getDivision());
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#setDivision(com.digsarustudio.banana.contactinfo.address.IEnumAbbreviatable)}.
	 */
	@Test
	public void testSetName() {
		City Citys = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, Citys.getDivision());
		
		Citys.setDivision(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, Citys.getDivision());
		
		City lgas = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, lgas.getDivision());
		
		lgas.setDivision(QueenslandLGAs.Ipswich);
		assertEquals(QueenslandLGAs.Ipswich, lgas.getDivision());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = City;
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertEquals(City, City1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObjectInSameValue() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertEquals(City, City1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsObject() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, City1.getDivision());
		
		assertNotEquals(City, City1);		
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSubdivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.Archerfield).build();
		assertEquals(BrisbaneSuburbs.Archerfield, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.Archerfield).build();
		assertEquals(BrisbaneSuburbs.Archerfield, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertEquals(City, City1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsSubdivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City1.getDivision());
		
		assertEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.Darra).build();
		assertEquals(BrisbaneSuburbs.Darra, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.EightMilePlains).build();
		assertEquals(BrisbaneSuburbs.EightMilePlains, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);		
		assertNotEquals(lgas, lgas1);
		
		assertNotEquals(City, City1);
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testNotEqualsDivision() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, City1.getDivision());
		
		assertNotEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.ForestLake).build();
		assertEquals(BrisbaneSuburbs.ForestLake, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.CannonHill).build();
		assertEquals(BrisbaneSuburbs.CannonHill, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertNotEquals(lgas, lgas1);
		
		assertNotEquals(City, City1);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.administrativedivision.City#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsSubdivisionButNotEqualsDivisions() {
		City City = new City.Builder(QueenslandLGAs.Brisbane).build();
		assertEquals(QueenslandLGAs.Brisbane, City.getDivision());
		
		City City1 = new City.Builder(QueenslandLGAs.Ipswich).build();
		assertEquals(QueenslandLGAs.Ipswich, City1.getDivision());
		
		assertNotEquals(City, City1);
		
		Suburb lgas = new Suburb.Builder(BrisbaneSuburbs.BrisbaneCBD).build();
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lgas.getDivision());
		
		Suburb lgas1 = new Suburb.Builder(BrisbaneSuburbs.BrisbaneCBD).build();
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, lgas1.getDivision());
		
		City.setSubdivision(lgas);
		City1.setSubdivision(lgas1);
		assertEquals(lgas, lgas1);
		
		assertNotEquals(City, City1);
	}
}
