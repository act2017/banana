/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 * To test the functionalities of Countries
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class CountriesTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries#getValues()}.
	 */
	@Test @Ignore
	public void testGetValues() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries#self()}.
	 */
	@Test @Ignore
	public void testSelf() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries#getTypeFromName(java.lang.String)}.
	 */
	@Test @Ignore
	public void testGetTypeFromName() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries#getTypeFromAbbreviation(java.lang.String)}.
	 */
	@Test @Ignore
	public void testGetTypeFromAbbreviation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCode() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsObject() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum#AbstractAbbreviatableEnum(java.lang.String, java.lang.String)}.
	 */
	@Test @Ignore
	public void testAbstractAbbreviatableEnum() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum#fromAbbreviation(java.lang.String)}.
	 */
	@Test @Ignore
	public void testFromAbbreviation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum#getAbbreviation()}.
	 */
	@Test @Ignore
	public void testGetAbbreviation() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum#getOtherAbbreviations()}.
	 */
	@Test @Ignore
	public void testGetOtherAbbreviations() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractEnum#AbstractEnum(java.lang.String)}.
	 */
	@Test @Ignore
	public void testAbstractEnum() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractEnum#getValue()}.
	 */
	@Test @Ignore
	public void testGetValue() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.enumeration.AbstractEnum#fromValue(java.lang.String)}.
	 */
	@Test @Ignore
	public void testFromValue() {
		fail("Not yet implemented");
	}

}
