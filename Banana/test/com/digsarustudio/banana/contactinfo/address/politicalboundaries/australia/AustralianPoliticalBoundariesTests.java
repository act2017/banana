/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * To test all of the australian boundaries under PoliticalBoundaries.Australia package
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ AustralianStatesTest.class, BrisbaneSuburbsTest.class, QueenslandLGAsTest.class })
public class AustralianPoliticalBoundariesTests {

}
