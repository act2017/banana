/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.AustralianStates;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.BrisbaneSuburbs;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia.QueenslandLGAs;

/**
 * To test political boundaries object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PoliticalBoundariesTest {
	private PoliticalBoundaries target;
	
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		this.target = new PoliticalBoundaries.Builder(Countries.Australia)
											 .setState(AustralianStates.Queensland)
											 .setCity(QueenslandLGAs.Brisbane)
											 .setSuburb(BrisbaneSuburbs.BrisbaneCBD)
											 .build();
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 */
	@Test @Ignore
	public void testHashCodeSameObject() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target;
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameValue() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
																.setState(AustralianStates.Queensland)
																.setCity(QueenslandLGAs.Brisbane)
																.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
																.build();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameCountry() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Banana)
																.setSuburb(BrisbaneSuburbs.Indooroopilly)
																.build();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Banana, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, boundaries.getSuburb());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameState() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Queensland)
																.setCity(QueenslandLGAs.Banana)
																.setSuburb(BrisbaneSuburbs.Indooroopilly)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Banana, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, boundaries.getSuburb());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameCity() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Brisbane)
																.setSuburb(BrisbaneSuburbs.Indooroopilly)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, boundaries.getSuburb());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSameSuburb() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Balonne)
																.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Balonne, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#hashCode()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeAllDiff() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Balonne)
																.setSuburb(BrisbaneSuburbs.Calamvale)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Balonne, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, boundaries.getSuburb());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testHashCodeAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testHashCodeCountryChangedAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testHashCodeStateChangedAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeCityChangedAfterCopy() throws StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbChangedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setSuburb(BrisbaneSuburbs.AcaciaRidge);
		assertEquals(BrisbaneSuburbs.AcaciaRidge, boundaries.getSuburb());
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeCountryModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testHashCodeStateModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeCityModifiedAfterClone() throws CloneNotSupportedException, StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testHashCodeSuburbModifiedAfterClone() throws CloneNotSupportedException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setSuburb(BrisbaneSuburbs.AcaciaRidge);
		assertEquals(BrisbaneSuburbs.AcaciaRidge, boundaries.getSuburb());
		
		assertNotEquals(this.target.hashCode(), boundaries.hashCode());
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#getCountry()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test
	public void testGetCountry() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();

		assertEquals(Countries.Australia, boundaries.getCountry());		
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test
	public void testSetCountry() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();

		assertEquals(Countries.Australia, boundaries.getCountry());
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#getState()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test
	public void testGetState() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();

		assertEquals(AustralianStates.Queensland, boundaries.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test
	public void testSetState() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();


		assertEquals(AustralianStates.Queensland, boundaries.getState());

		boundaries.setState(AustralianStates.NorthenTerritory);
		assertEquals(AustralianStates.NorthenTerritory, boundaries.getState());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#getCity()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetCity() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();


		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testSetCity() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();

		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());

		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#getSuburb()}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testGetSuburb() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();

		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testSetSuburb() throws StateNotSetException, CityNotSetException {
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
				.setState(AustralianStates.Queensland)
				.setCity(QueenslandLGAs.Brisbane)
				.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
				.build();

		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 */
	@Test @Ignore
	public void testEqualsSameObject() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target;
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertTrue(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameValue() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
																.setState(AustralianStates.Queensland)
																.setCity(QueenslandLGAs.Brisbane)
																.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
																.build();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertTrue(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameCountry() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Australia)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Banana)
																.setSuburb(BrisbaneSuburbs.Indooroopilly)
																.build();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Banana, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, boundaries.getSuburb());
		
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameState() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Queensland)
																.setCity(QueenslandLGAs.Banana)
																.setSuburb(BrisbaneSuburbs.Indooroopilly)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Banana, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, boundaries.getSuburb());
		
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameCity() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Brisbane)
																.setSuburb(BrisbaneSuburbs.Indooroopilly)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Indooroopilly, boundaries.getSuburb());
		
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSameSuburb() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Balonne)
																.setSuburb(BrisbaneSuburbs.BrisbaneCBD)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Balonne, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#equals(java.lang.Object)}.
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsAllDiff() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries.Builder(Countries.Taiwan)
																.setState(AustralianStates.Victoria)
																.setCity(QueenslandLGAs.Balonne)
																.setSuburb(BrisbaneSuburbs.Calamvale)
																.build();
		
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertEquals(QueenslandLGAs.Balonne, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.Calamvale, boundaries.getSuburb());
		
		assertFalse(this.target.equals(boundaries));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testEqualsAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertTrue(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testEqualsCountryChangedAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testEqualsStateChangedAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsCityChangedAfterCopy() throws StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSuburbChangedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setSuburb(BrisbaneSuburbs.AcaciaRidge);
		assertEquals(BrisbaneSuburbs.AcaciaRidge, boundaries.getSuburb());
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertTrue(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsCountryModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testEqualsStateModifiedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		
		assertFalse(this.target.equals(boundaries));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testEqualsCityModifiedAfterClone() throws CloneNotSupportedException, StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
		
		assertFalse(this.target.equals(boundaries));
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testEqualsSuburbModifiedAfterClone() throws CloneNotSupportedException, StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		boundaries.setSuburb(BrisbaneSuburbs.AcaciaRidge);
		assertEquals(BrisbaneSuburbs.AcaciaRidge, boundaries.getSuburb());
		
		assertFalse(this.target.equals(boundaries));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCountryChangedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testStateChangedAfterClone() throws CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testCityChangedAfterClone() throws StateNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 * @throws CloneNotSupportedException 
	 */
	@Test @Ignore
	public void testSuburbChangedAfterClone() throws StateNotSetException, CityNotSetException, CloneNotSupportedException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = this.target.clone();
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setSuburb(BrisbaneSuburbs.AcaciaRidge);
		assertEquals(BrisbaneSuburbs.AcaciaRidge, boundaries.getSuburb());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		assertEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testCountryChangedAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCountry(Countries.Taiwan);
		assertEquals(Countries.Taiwan, boundaries.getCountry());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 */
	@Test @Ignore
	public void testStateChangedAfterCopy() {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setState(AustralianStates.Victoria);
		assertEquals(AustralianStates.Victoria, boundaries.getState());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 */
	@Test @Ignore
	public void testCityChangedAfterCopy() throws StateNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setCity(QueenslandLGAs.Aurukun);
		assertEquals(QueenslandLGAs.Aurukun, boundaries.getCity());
		assertNotEquals(this.target, boundaries);
	}
	
	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#PoliticalBoundaries(com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries)}.
	 * @throws StateNotSetException 
	 * @throws CityNotSetException 
	 */
	@Test @Ignore
	public void testSuburbChangedAfterCopy() throws StateNotSetException, CityNotSetException {
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		PoliticalBoundaries boundaries = new PoliticalBoundaries(this.target);
		
		assertEquals(Countries.Australia, boundaries.getCountry());
		assertEquals(AustralianStates.Queensland, boundaries.getState());
		assertEquals(QueenslandLGAs.Brisbane, boundaries.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, boundaries.getSuburb());
		
		
		boundaries.setSuburb(BrisbaneSuburbs.AcaciaRidge);
		assertEquals(BrisbaneSuburbs.AcaciaRidge, boundaries.getSuburb());
		assertNotEquals(this.target, boundaries);
	}	

	/**
	 * Test method for {@link com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries#getPostcode}.
	 */
	@Test @Ignore
	public void testGetPostcode(){
		assertEquals(Countries.Australia, this.target.getCountry());
		assertEquals(AustralianStates.Queensland, this.target.getState());
		assertEquals(QueenslandLGAs.Brisbane, this.target.getCity());
		assertEquals(BrisbaneSuburbs.BrisbaneCBD, this.target.getSuburb());
		
		assertEquals("4000", this.target.getPostcode());
	}	
}
