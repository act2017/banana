/**
 * 
 */
package com.digsarustudio.banana.contactinfo.phone;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test phone number object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PhoneNumberTest {	
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.PhoneNumber#PhoneNumber(java.lang.String, java.lang.String)}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@SuppressWarnings("unused")
//	@Test
//	public void testPhoneNumber() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#hashCode()}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testHashCodeSameObject() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = number;
//		assertEquals("8989889", number2.getValue());
//		
//		assertEquals(number.hashCode(), number2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#hashCode()}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testHashCodeSameValue() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number2.getValue());
//		
//		assertEquals(number.hashCode(), number2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#hashCode()}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testHashCodeSameNumber() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = new PhoneNumber("8989889", "^[a-z0-9]+$");
//		assertEquals("8989889", number2.getValue());
//		
//		assertNotEquals(number.hashCode(), number2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#hashCode()}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testHashCodeDiffNumber() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = new PhoneNumber("8825252", "^[a-z0-9]+$");
//		assertEquals("8825252", number2.getValue());
//		
//		assertNotEquals(number.hashCode(), number2.hashCode());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#getValue()}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testGetValue() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[0-9]+$");
//		assertEquals("8989889", number.getValue());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#setValue(java.lang.String)}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testSetValue() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		number.setValue("8825252");
//		assertEquals("8825252", number.getValue());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#equals(java.lang.Object)}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testEqualsSameObject() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = number;
//		assertEquals("8989889", number2.getValue());
//		
//		assertTrue(number.equals(number2));
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#equals(java.lang.Object)}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testEqualsSameValue() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number2.getValue());
//		
//		assertTrue(number.equals(number2));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#equals(java.lang.Object)}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testEqualsSameNumber() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = new PhoneNumber("8989889", "^[a-z0-9]+$");
//		assertEquals("8989889", number2.getValue());
//		
//		assertFalse(number.equals(number2));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.banana.contactinfo.phone.AbstractPhoneNumber#equals(java.lang.Object)}.
//	 * @throws PhoneNumberFormatException 
//	 */
//	@Test
//	public void testEqualsDiffNumber() throws PhoneNumberFormatException {
//		PhoneNumber number = new PhoneNumber("8989889", "^[a-zA-Z0-9]+$");
//		assertEquals("8989889", number.getValue());
//		
//		PhoneNumber number2 = new PhoneNumber("8825252", "^[a-zA-Z0-9]+$");
//		assertEquals("8825252", number2.getValue());
//		
//		assertFalse(number.equals(number2));
//	}
}
