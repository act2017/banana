/**
 * 
 */
package com.digsarustudio.banana.contactinfo.utils;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.digsarustudio.banana.utils.StringFormatter;

/**
 * To test {@link StringFormatter}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class StringFormatterTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormatWithString() {
		String format = "Hello, %s!!";
		String name = "Kitty";
		
		assertEquals("Hello, Kitty!!", StringFormatter.format(format, name));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormatWith2Strings() {
		String format = "Hello, %s, %s!!";
		String name = "Kitty";
		String addtional = "how's going?";
		
		assertEquals("Hello, Kitty, how's going?!!", StringFormatter.format(format, name, addtional));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormatWithInteger() {
		String format = "Hello, %s %d!!";
		String name = "Kitty";
		Integer year = 2017;
		
		assertEquals("Hello, Kitty 2017!!", StringFormatter.format(format, name, year));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormatWithInsufficientArguements() {
		String format = "Hello, %s!!";
		String name = "Kitty";
		Integer year = 2017;
		
		assertEquals("Hello, Kitty!!", StringFormatter.format(format, name, year));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.utils.StringFormatter#format(java.lang.String, java.lang.Object[])}.
	 */
	@Test
	public void testFormatWithOverArguments() {
		String format = "Hello, %s %d!!";
		String name = "Kitty";
		
		assertEquals("Hello, Kitty %d!!", StringFormatter.format(format, name));
	}
}
