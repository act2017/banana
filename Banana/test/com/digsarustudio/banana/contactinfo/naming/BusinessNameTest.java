/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test business name object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class BusinessNameTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#BusinessName(com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName)}.
//	 */
//	@Test
//	public void testBusinessName() {
//		@SuppressWarnings("unused")
//		BusinessName name = new BusinessName();
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#BusinessName(com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName)}.
//	 */
//	@Test
//	public void testBusinessNameConstructsWithName() {
//		@SuppressWarnings("unused")
//		BusinessName name = new BusinessName("Dig Saru Studio");
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#BusinessName(com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName)}.
//	 */
//	@Test
//	public void testBusinessNameCopyConstructor() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		
//		@SuppressWarnings("unused")
//		BusinessName name2 = new BusinessName(name);
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#getAbbreviation()}.
//	 */
//	@Test
//	public void testGetAbbreviation() {
//		BusinessName name = new BusinessName();
//		Assert.assertNull(name.getAbbreviation());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#getAbbreviation()}.
//	 */
//	@Test
//	public void testGetAbbreviationWithConstructor() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertNull(name.getAbbreviation());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#getAbbreviation()}.
//	 */
//	@Test
//	public void testGetAbbreviationAfterCopy() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertNull(name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		Assert.assertNull(name2.getAbbreviation());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#setAbbreviation()}.
//	 */
//	@Test
//	public void testSetAbbreviation() {
//		BusinessName name = new BusinessName();
//		Assert.assertNull(name.getAbbreviation());
//		
//		name.setAbbreviation("DDS");
//		Assert.assertEquals("DDS", name.getAbbreviation());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#setAbbreviation()}.
//	 */
//	@Test
//	public void testSetAbbreviationWhenConstructed() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertNull(name.getAbbreviation());
//		
//		name.setAbbreviation("DDS");
//		Assert.assertEquals("DDS", name.getAbbreviation());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.BusinessName#setAbbreviation()}.
//	 */
//	@Test
//	public void testSetAbbreviationAfterCopy() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertNull(name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setAbbreviation("DDS");
//		Assert.assertEquals("DDS", name2.getAbbreviation());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetName() {
//		BusinessName name = new BusinessName();
//		Assert.assertNull(name.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetNameWithConstruct() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetNameAfterCopied() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		
//		BusinessName name2 = new BusinessName(name);
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetName() {
//		BusinessName name = new BusinessName();
//		Assert.assertNull(name.getName());
//		
//		name.setName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//	}	
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetNameWithConstruct() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		
//		name.setName("Dig Saru Workshop");
//		Assert.assertEquals("Dig Saru Workshop", name.getName());
//	}	
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetNameAfterCopied() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setName("Dig Saru Workshop");
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//	}	
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeSameObject() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertNull(name.getAbbreviation());
//		
//		BusinessName name2 = name;
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertNull(name2.getAbbreviation());
//		
//		Assert.assertEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeSameValue() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertNull(name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Studio");
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertNull(name2.getAbbreviation());
//		
//		Assert.assertEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeSameName() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Studio");		
//		name2.setAbbreviation("SSD");
//		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeSameAbbreviation() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Workshop");		
//		name2.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeDifferentValue() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Workshop");		
//		name2.setAbbreviation("SSD");
//		
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeAfterCopied() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeAfterCopiedSameName() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setAbbreviation("SSD");
//		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeAfterCopiedSameAbbreviation() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setName("Dig Saru Workshop");
//		
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testHashCodeAfterCopiedModified() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setName("Dig Saru Workshop");
//		name2.setAbbreviation("SSD");
//		
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameObject() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = name;
//		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertTrue(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameValue() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Studio");
//		name2.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertTrue(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameName() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Studio");
//		name2.setAbbreviation("SSD");
//		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameAbbreviation() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Workshop");
//		name2.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsDifferentValue() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName("Dig Saru Workshop");
//		name2.setAbbreviation("SSD");
//		
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopied() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertTrue(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopiedSameName() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setAbbreviation("SSD");
//		Assert.assertEquals("Dig Saru Studio", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopiedSameAbbreviation() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setName("Dig Saru Workshop");
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("DSS", name2.getAbbreviation());
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopiedModified() {
//		BusinessName name = new BusinessName("Dig Saru Studio");
//		name.setAbbreviation("DSS");
//		
//		Assert.assertEquals("Dig Saru Studio", name.getName());
//		Assert.assertEquals("DSS", name.getAbbreviation());
//		
//		BusinessName name2 = new BusinessName(name);
//		name2.setName("Dig Saru Workshop");
//		name2.setAbbreviation("SSD");
//		Assert.assertEquals("Dig Saru Workshop", name2.getName());
//		Assert.assertEquals("SSD", name2.getAbbreviation());
//		
//		Assert.assertFalse(name2.equals(name));
//	}

}
