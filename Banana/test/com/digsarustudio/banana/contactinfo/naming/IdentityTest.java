/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test Identity object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class IdentityTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Identification()}.
//	 */
//	@Test
//	public void testIdentification() {
//		@SuppressWarnings("unused")
//		Identity id = new Identity();
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Identification(java.lang.String)}.
//	 */
//	@Test
//	public void testIdentificationString() {
//		Identity id = new Identity("Digging Saru");
//		
//		Assert.assertEquals("Digging Saru", id.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Identification(com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity)}.
//	 */
//	@Test
//	public void testCpoyIdentification() {
//		Identity id = new Identity("Digging Saru");		
//		Assert.assertEquals("Digging Saru", id.getName());
//		
//		Identity id2 = new Identity(id);
//		Assert.assertEquals("Digging Saru", id2.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetName() {
//		Identity id = new Identity();
//		id.setName("Digging Saru");
//		Assert.assertEquals("Digging Saru", id.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetName() {
//		Identity id = new Identity();
//		Assert.assertNull(id.getName());
//		
//		id.setName("Digging Saru");
//		Assert.assertEquals("Digging Saru", id.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetNameByConstructor() {
//		Identity id = new Identity("Digging Monkey");
//		Assert.assertEquals("Digging Monkey", id.getName());
//		
//		id.setName("Digging Saru");
//		Assert.assertEquals("Digging Saru", id.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetNameWithCopiedObject() {
//		Identity id = new Identity("Digging Monkey");
//		Assert.assertEquals("Digging Monkey", id.getName());
//		
//		Identity id2 = new Identity(id);
//		Assert.assertEquals("Digging Monkey", id2.getName());
//		
//		id2.setName("Digging Saru");
//		Assert.assertEquals("Digging Saru", id2.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeWithSameValue(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity("Digging Saru");
//		
//		Assert.assertEquals(id.hashCode(), id2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeWithDifferentValue(){
//		Identity id = new Identity("Digging Monkey");
//		Identity id2 = new Identity("Digging Saru");
//		
//		Assert.assertNotEquals(id.hashCode(), id2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeWithSameObject(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = id;
//		
//		Assert.assertEquals(id.hashCode(), id2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeWithCopiedObject(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity(id);
//		
//		Assert.assertEquals(id.hashCode(), id2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeWithModifiedCopiedObject(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity(id);
//		id2.setName("Digging Monkey");
//		
//		Assert.assertNotEquals(id.hashCode(), id2.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lnag.Object)}.
//	 */
//	@Test
//	public void testEqualsSameObject(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = id;
//		Assert.assertTrue(id.equals(id2));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lnag.Object)}.
//	 */
//	@Test
//	public void testEqualsSameValue(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity("Digging Saru");
//		Assert.assertTrue(id.equals(id2));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lnag.Object)}.
//	 */
//	@Test
//	public void testEqualsCopiedObject(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity(id);
//		
//		Assert.assertTrue(id.equals(id2));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lnag.Object)}.
//	 */
//	@Test
//	public void testEqualsModifiedCopiedObject(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity(id);
//		
//		Assert.assertTrue(id.equals(id2));
//		
//		id2.setName("Digging Monkey");
//		Assert.assertFalse(id.equals(id2));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lnag.Object)}.
//	 */
//	@Test
//	public void testEqualsDifferentValue(){
//		Identity id = new Identity("Digging Saru");
//		Identity id2 = new Identity("digging monkey");
//		
//		Assert.assertFalse(id.equals(id2));
//	}
}
