/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * To test all of the tests under Naming package
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ BusinessNameTest.class, IdentityTest.class, PersonalNameTest.class })
public class NamingTests {

}
