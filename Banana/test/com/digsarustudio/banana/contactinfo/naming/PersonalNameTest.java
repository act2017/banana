/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;


import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test personal name object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PersonalNameTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#PersonalName()}.
//	 */
//	@Test
//	public void testPersonalName() {
//		@SuppressWarnings("unused")
//		PersonalName name = new PersonalName();	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#PersonalName(java.lang.String, java.lang.String)}.
//	 */
//	@Test
//	public void testPersonalNameConstructsByName() {
//		@SuppressWarnings("unused")
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#PersonalName(com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName)}.
//	 */
//	@Test
//	public void testCopyPersonalName() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		@SuppressWarnings("unused")
//		PersonalName name2 = new PersonalName(name);
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#getSurname()}.
//	 */
//	@Test
//	public void testGetSurname() {
//		PersonalName name = new PersonalName();		
//		Assert.assertNull(name.getSurname());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#getSurname()}.
//	 */
//	@Test
//	public void testGetSurnameWhenConstructedWithName() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");		
//		Assert.assertEquals("Nitohei", name.getSurname());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#getSurname()}.
//	 */
//	@Test
//	public void testGetSurnameAfterCopy() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");		
//		Assert.assertEquals("Nitohei", name.getSurname());
//		
//		PersonalName name2 = new PersonalName(name);
//		Assert.assertEquals("Nitohei", name2.getSurname());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#setSurname(java.lang.String)}.
//	 */
//	@Test
//	public void testSetSurname() {
//		PersonalName name = new PersonalName();
//		name.setSurname("Nitohei");
//		
//		Assert.assertEquals("Nitohei", name.getSurname());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#setSurname(java.lang.String)}.
//	 */
//	@Test
//	public void testSetSurnameWhenConstructedWithName() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");		
//		Assert.assertEquals("Nitohei", name.getSurname());
//		
//		name.setSurname("Kunso");
//		Assert.assertEquals("Kunso", name.getSurname());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.PersonalName#setSurname(java.lang.String)}.
//	 */
//	@Test
//	public void testSetSurnameAfterCopy() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");		
//		Assert.assertEquals("Nitohei", name.getSurname());
//		
//		PersonalName name2 = new PersonalName(name);
//		Assert.assertEquals("Nitohei", name2.getSurname());
//		
//		name2.setSurname("Kunso");
//		Assert.assertEquals("Kunso", name2.getSurname());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetName() {
//		PersonalName name = new PersonalName();
//		
//		Assert.assertNull(name.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetNameWhenConstructedWithName() {
//		PersonalName name = new PersonalName("Tamama", "Netohei");
//		
//		Assert.assertEquals("Tamama", name.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#getName()}.
//	 */
//	@Test
//	public void testGetNameAfterCopy() {
//		PersonalName name = new PersonalName("Tamama", "Netohei");		
//		Assert.assertEquals("Tamama", name.getName());
//		
//		PersonalName name2 = new PersonalName(name);
//		
//		Assert.assertEquals("Tamama", name2.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetName() {
//		PersonalName name = new PersonalName();
//		Assert.assertNull(name.getName());
//		
//		name.setName("Tamama");
//		Assert.assertEquals("Tamama", name.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetNameAfterConstructed() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		Assert.assertEquals("Tamama", name.getName());
//		
//		name.setName("Kururu");
//		Assert.assertEquals("Kururu", name.getName());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#setName(java.lang.String)}.
//	 */
//	@Test
//	public void testSetNameAfterCopy() {
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		Assert.assertEquals("Tamama", name.getName());
//		
//		PersonalName name2 = new PersonalName(name);
//		name2.setName("Kururu");
//		Assert.assertEquals("Kururu", name2.getName());
//	}
//
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeSameObject(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = name;
//		
//		Assert.assertEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeSameValue(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Tamama", "Nitohei");
//		
//		Assert.assertEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeSameName(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Tamama", "KunSou");
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeSameSurName(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Kururu", "Nitohei");
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeCopiedObject(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName(name);
//		
//		Assert.assertEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeModifiedCopiedObject(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName(name);
//		name2.setName("Kururu");
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeModifiedSurnameCopiedObject(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName(name);
//		name2.setSurname("KunSou");
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#hashCode()}.
//	 */
//	@Test
//	public void testhashCodeDifferent(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Kururu", "Soujo");
//		
//		Assert.assertNotEquals(name2.hashCode(), name.hashCode());
//	}	
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameObject(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = name;
//		
//		Assert.assertTrue(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameName(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Tamama", "Soujo");
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsSameSurname(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Kururu", "Nitohei");
//		
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopied(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName(name);
//		
//		Assert.assertTrue(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopiedChangeName(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName(name);
//		name2.setName("Kururu");
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsCopiedChangeSurname(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName(name);
//		name2.setSurname("SouJo");
//		Assert.assertFalse(name2.equals(name));
//	}
//	
//	/**
//	 * Test method for {@link com.digsarustudio.sarucouriers.sukhavati.shared.banana.naming.Identity#Equals(java.lang.Object)}.
//	 */
//	@Test
//	public void testEqualsDifferent(){
//		PersonalName name = new PersonalName("Tamama", "Nitohei");
//		PersonalName name2 = new PersonalName("Kururu", "Soujo");
//		
//		Assert.assertFalse(name2.equals(name));
//	}

}
