/**
 * 
 */
package com.digsarustudio.banana.converter;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test date converter object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
@SuppressWarnings("deprecation")
public class DateConverterTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#DateConverter(java.lang.String)}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testDateConverter() {
		DateConverter converter = new DateConverter("dd/MM/yy");
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#getDate()}.
	 */
	@Test
	public void testGetDate() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		assertNull(converter.getDate());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#setDate(java.lang.String)}.
	 */
	@Test
	public void testSetDateString() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		converter.setDate("19/10/2016");
		
		assertEquals("19/10/2016", converter.getDate());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#setDate(java.util.Date)}.
	 */
	@Test
	public void testSetDateDate() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		converter.setDate(new Date());
		
		assertEquals(new Date(), converter.getResult());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#getFormat()}.
	 */
	@Test
	public void testGetFormat() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		assertEquals("dd/MM/yy", converter.getFormat());
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#getResult()}.
	 */
	@Test
	public void testGetResult() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		assertEquals("dd/MM/yy", converter.getFormat());
		
		converter.setDate("19/10/2016");
		
		converter.accept(new SimpleDateFormatVisitor());
		Date date = converter.getResult();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertEquals(19, calendar.get(Calendar.DAY_OF_MONTH));
		assertEquals(Calendar.OCTOBER, calendar.get(Calendar.MONTH));
		assertEquals(2016, calendar.get(Calendar.YEAR));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#convertString(java.lang.String, java.lang.Object)}.
	 */
	@Test
	public void testConvertString() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		assertEquals("dd/MM/yy", converter.getFormat());
		
		converter.convertString("19/10/2016", new SimpleDateFormatVisitor());
		Date date = converter.getResult();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertEquals(19, calendar.get(Calendar.DAY_OF_MONTH));
		assertEquals(Calendar.OCTOBER, calendar.get(Calendar.MONTH));
		assertEquals(2016, calendar.get(Calendar.YEAR));
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.DateConverter#accept(java.lang.Object)}.
	 */
	@Test
	public void testAccept() {
		DateConverter converter = new DateConverter("dd/MM/yy");
		assertEquals("dd/MM/yy", converter.getFormat());
		
		converter.setDate("19/10/2016");
		
		converter.accept(new SimpleDateFormatVisitor());
		Date date = converter.getResult();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertEquals(19, calendar.get(Calendar.DAY_OF_MONTH));
		assertEquals(Calendar.OCTOBER, calendar.get(Calendar.MONTH));
		assertEquals(2016, calendar.get(Calendar.YEAR));
	}

}
