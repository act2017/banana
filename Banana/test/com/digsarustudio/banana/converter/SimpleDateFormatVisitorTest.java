/**
 * 
 */
package com.digsarustudio.banana.converter;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * To test the simple date format visitor
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class SimpleDateFormatVisitorTest {

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.SimpleDateFormatVisitor#SimpleDateFormatVisitor()}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testSimpleDateFormatVisitor() {
		SimpleDateFormatVisitor visitor = new SimpleDateFormatVisitor();
	}

	/**
	 * Test method for {@link com.digsarustudio.banana.converter.SimpleDateFormatVisitor#visit(com.digsarustudio.banana.converter.DateConverter)}.
	 */
	@Test
	public void testVisit() {
		SimpleDateFormatVisitor visitor = new SimpleDateFormatVisitor();
		DateConverter converter = new DateConverter("dd/MM/yy");
		converter.setDate("19/10/2016");
		
		visitor.visit(converter);
		
		Date date = converter.getResult();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertEquals(19, calendar.get(Calendar.DAY_OF_MONTH));
		assertEquals(Calendar.OCTOBER, calendar.get(Calendar.MONTH));
		assertEquals(2016, calendar.get(Calendar.YEAR));
	}
	/**
	 * Test method for {@link com.digsarustudio.banana.converter.SimpleDateFormatVisitor#visit(com.digsarustudio.banana.converter.DateConverter)}.
	 */
	@Test
	public void testVisitValueInMDY() {
		SimpleDateFormatVisitor visitor = new SimpleDateFormatVisitor();
		DateConverter converter = new DateConverter("dd/MM/yy");
		converter.setDate("190/10/2016");
		
		visitor.visit(converter);
		
		Date date = converter.getResult();
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		
		assertNotEquals(19, calendar.get(Calendar.DAY_OF_MONTH));
		assertNotEquals(Calendar.OCTOBER, calendar.get(Calendar.MONTH));
		assertNotEquals(2016, calendar.get(Calendar.YEAR));
	}
}
