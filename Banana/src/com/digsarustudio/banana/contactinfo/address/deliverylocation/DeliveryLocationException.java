/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

/**
 * Represents the exception when it is failed to set delivery location
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class DeliveryLocationException extends Exception {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 8453848843437434817L;

	/**
	 * 
	 */
	public DeliveryLocationException() {
	}

	/**
	 * @param message
	 */
	public DeliveryLocationException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public DeliveryLocationException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DeliveryLocationException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DeliveryLocationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
