/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * The definition of P.O.Box
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractPOBox extends AbstractDeliveryLocation{
	/**
	 * The postal delivery type
	 */
	private PostalDeliveryType type;

	/**
	 * Constructs a P.O.Box object
	 * 
	 * @param locationNumber The number of location
	 * @param type The postal delivery type
	 */
	public AbstractPOBox(String locationNumber, PostalDeliveryType type) {
		super(locationNumber);
		this.setPostalDeliveryType(type);
	}
	
	/**
	 * Returns the postal delivery type
	 * 
	 * @return the postal delivery type
	 */
	public PostalDeliveryType getType() {
		return type;
	}

	/**
	 * Assign a postal delivery type
	 * 
	 * @param type the postal delivery type to set
	 */
	public void setType(PostalDeliveryType type) {
		this.type = type;
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getStreetName()
	 */
	@Override
	@Deprecated
	public String getStreetName() {

		throw new UnsupportedOperationException("P.O.Box doesn't support getStreetName()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetName(java.lang.String)
	 */
	@Override
	@Deprecated
	public void setStreetName(String name) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setStreetName()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetName(java.lang.String, java.lang.String)
	 */
	@Override
	@Deprecated
	public void setStreetName(String name, String suffix) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setStreetName()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getStreetType()
	 */
	@Override
	@Deprecated
	public StreetType getStreetType() {

		throw new UnsupportedOperationException("P.O.Box doesn't support getStreetType()");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)
	 */
	@Override
	@Deprecated
	public void setStreetType(StreetType type) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setStreetType()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getStreetSuffix()
	 */
	@Override
	@Deprecated
	public String getStreetSuffix() {

		throw new UnsupportedOperationException("P.O.Box doesn't support getStreeSuffix()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetSuffix(java.lang.String)
	 */
	@Override
	@Deprecated
	public void setStreetSuffix(String suffix) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setStreeSuffix()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getPropertyType()
	 */
	@Override
	@Deprecated
	public <T extends AbstractPropertyType> T getPropertyType() {

		throw new UnsupportedOperationException("P.O.Box doesn't support getPropertyType()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)
	 */
	@Override
	@Deprecated
	public <T extends AbstractPropertyType> void setPropertyType(T type) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setPropertyType()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getBuildingNumber()
	 */
	@Override
	@Deprecated
	public String getBuildingNumber() {

		throw new UnsupportedOperationException("P.O.Box doesn't support getBuildingNumber()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setBuildingNumber(java.lang.String)
	 */
	@Override
	@Deprecated
	public void setBuildingNumber(String number) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setBuildingNumber()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getLevel()
	 */
	@Override
	@Deprecated
	public Integer getLevel() {		

		throw new UnsupportedOperationException("P.O.Box doesn't support getLevel()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setLevel(java.lang.Integer)
	 */
	@Override
	@Deprecated
	public void setLevel(Integer level) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setLevel()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getLevelType()
	 */
	@Override
	@Deprecated
	public LevelType getLevelType() {

		throw new UnsupportedOperationException("P.O.Box doesn't support getLevelType()");
	}

	/**
	 * This method is not supported by AbstractPOBox
	 * 
	 *  (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)
	 */
	@Override
	@Deprecated
	public void setLevelType(LevelType type) {

		throw new UnsupportedOperationException("P.O.Box doesn't support setLevelType()");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getPostalDeliveryType()
	 */
	@Override
	public PostalDeliveryType getPostalDeliveryType() {

		return this.type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)
	 */
	@Override
	public void setPostalDeliveryType(PostalDeliveryType type) {

		this.type = type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof AbstractPOBox)) {
			return false;
		}
		AbstractPOBox other = (AbstractPOBox) obj;
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}
}
