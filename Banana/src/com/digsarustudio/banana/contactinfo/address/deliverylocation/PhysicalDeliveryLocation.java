/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import java.io.Serializable;

import com.digsarustudio.banana.contactinfo.address.IDeliveryLocation;
import com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType;
import com.digsarustudio.banana.contactinfo.address.property.Building;
import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.Level;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.ParcelOfLand;
import com.digsarustudio.banana.contactinfo.address.property.ParcelOfLandType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.property.Property;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.Street;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the physical delivery location of the address for the delivery
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class PhysicalDeliveryLocation extends AbstractDeliveryLocation implements Cloneable, Serializable {
	/**
	 * The street object
	 */
	private Street street;
	
	/**
	 * The property object
	 */
	private Property property;
	

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -523616530155120128L;
	
	/**
	 * To build PhysicalDeliveryLocation object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<PhysicalDeliveryLocation>{
		private String					locationNumber	= null;
		private String					streetName		= null;
		private StreetType				streetType		= null;
		private String					streetSuffix	= null;
		private AbstractPropertyType	propertyType	= null;
		private String					buildingNumber	= null;
		private Integer					level			= null;
		private LevelType				levelType		= null;
		
		/**
		 * Constructs a builder
		 * 
		 * @param locationNumber The number of location
		 */
		public Builder(String locationNumber) {
			this.locationNumber = locationNumber;
		}

		/**
		 * Assign a name to the street
		 * 
		 * @param name the name for the street to set
		 * 
		 * @return builder
		 */
		public Builder setStreetName(String name) {
			this.streetName = name;
			return this;
		}
		
		/**
		 * Assign a type to the street
		 * 
		 * @param type the type for the street to set
		 * 
		 * @return builder
		 */
		public Builder setStreetType(StreetType type) {
			this.streetType = type;
			return this;
		}
		
		/**
		 * Assign a suffix to the street
		 * 
		 * @param suffix The suffix for the street
		 * 
		 * @return Builder
		 */
		public Builder setStreetSuffix(String suffix) {
			this.streetSuffix = suffix;
			return this;
		}
		
		/**
		 * Assign a type to the property
		 * 
		 * @param type the type of property to set
		 * @return Builder
		 */
		public Builder setPropertyType(AbstractPropertyType type) {
			this.propertyType = type;
			return this;
		}
		
		/**
		 * Assign a number to the building
		 * 
		 * @param number The number for the building
		 * @return Builder
		 * @throws PropertyTypeNotMatchException When the property type is not a building
		 */
		public Builder setBuildingNumber(String buildingNumber) throws PropertyTypeNotMatchException {
			if(null != this.propertyType && !(this.propertyType instanceof BuildingType))
				throw new PropertyTypeNotMatchException("Please set the property type as building type before call this function");
			
			this.buildingNumber = buildingNumber;
			return this;
		}
		
		/**
		 * Assign a level to the building
		 * 
		 * @param level the level for a building to set
		 * @return Builder
		 * @throws PropertyTypeNotMatchException When the property type is not a building
		 */
		public Builder setLevel(Integer level) throws PropertyTypeNotMatchException {
			if(null != this.propertyType && !(this.propertyType instanceof BuildingType))
				throw new PropertyTypeNotMatchException("Please set the property type as building type before call this function");
			
			this.level = level;
			return this;
		}
		
		/**
		 * Assign the type of level
		 * 
		 * @param type The type for the level to set
		 * @return Builder
		 * @throws PropertyTypeNotMatchException When the property type is not a building
		 */
		public Builder setLevelType(LevelType type) throws PropertyTypeNotMatchException {
			if(null != this.propertyType && !(this.propertyType instanceof BuildingType))
				throw new PropertyTypeNotMatchException("Please set the property type as building type before call this function");
			
			
			this.levelType = type;
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public PhysicalDeliveryLocation build() {
	
			return new PhysicalDeliveryLocation(this);
		}

		/**
		 * Returns the number of location
		 * 
		 * @return the number of location
		 */
		protected String getLocationNumber() {
			return this.locationNumber;
		}
		
		/**
		 * Returns the name of street
		 * 
		 * @return the name of street
		 */
		protected String getStreetName() {
			return streetName;
		}
		
		/**
		 * Returns the type of street
		 * 
		 * @return the type of street
		 */
		protected StreetType getStreetType() {
			return this.streetType;
		}
		
		
		/**
		 * Returns the suffix of street
		 * 
		 * @return the suffix of street
		 */
		protected String getStreetSuffix() {
			return streetSuffix;
		}

		/**
		 * Returns the type of property
		 * 
		 * @return the type of property
		 */
		protected AbstractPropertyType getPropertyType() {
			return propertyType;
		}

		/**
		 * Returns the number of building
		 * 
		 * @return the number of building
		 */
		protected String getBuildingNumber() {
			return buildingNumber;
		}

		/**
		 * Returns the level of building
		 * 
		 * @return the level of building
		 */
		protected Integer getLevel() {
			return level;
		}

		/**
		 * Returns the type of level
		 * 
		 * @return the type of level
		 */
		protected LevelType getLevelType() {
			return levelType;
		}
	}
	

	/**
	 * Constructs a physical delivery location type
	 * 
	 * @param builder The PhysicalDeliveryLocation builder
	 */
	private PhysicalDeliveryLocation(Builder builder) {
		super(builder.getLocationNumber());

		this.setStreetName(builder.getStreetName(), builder.getStreetSuffix());
		this.setStreetType(builder.getStreetType());
		this.setPropertyType(builder.getPropertyType());
		try {
			this.setBuildingNumber(builder.getBuildingNumber());
			this.setLevel(builder.getLevel());			
			this.setLevelType(builder.getLevelType());
		} catch (PropertyTypeNotMatchException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Copy the physical delivery location
	 * 
	 * @param source The target to copy
	 */
	public PhysicalDeliveryLocation(PhysicalDeliveryLocation source) {

		super(source.getLocationNumber());
		
		this.setStreetName(source.getStreetName(), source.getStreetSuffix());
		this.setStreetType(source.getStreetType());
		this.setPropertyType(source.getPropertyType());
		try {
			this.setBuildingNumber(source.getBuildingNumber());
			this.setLevel(source.getLevel());
			this.setLevelType(source.getLevelType());
		} catch (PropertyTypeNotMatchException e) {
			e.printStackTrace();
		}
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getStreetName()
	 */
	@Override
	public String getStreetName() {

		if(null == this.street)
			return null;
		
		return this.street.getName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetName(java.lang.String)
	 */
	@Override
	public void setStreetName(String name) {

		if(null == this.street){
			this.street = new Street.Builder(name, null).build();
		}else {
			this.street.setName(name);			
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetName(java.lang.String, java.lang.String)
	 */
	@Override
	public void setStreetName(String name, String suffix) {

		if(null == this.street){
			this.street = new Street.Builder(name, null).setSuffix(suffix).build();
		}else {
			this.street.setName(name);			
			this.street.setSuffix(suffix);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getStreetType()
	 */
	@Override
	public StreetType getStreetType() {

		if(null == this.street)
			return null;
		
		return this.street.getType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)
	 */
	@Override
	public void setStreetType(StreetType type) {

		if(null != this.street){
			this.street.setType(type);
		}else{
			this.street = new Street.Builder(null, type).build();
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getStreetSuffix()
	 */
	@Override
	public String getStreetSuffix() {

		if(null == this.street)
			return null;
		
		return this.street.getSuffix();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setStreetSuffix(java.lang.String)
	 */
	@Override
	public void setStreetSuffix(String suffix) {

		if(null != this.street){
			this.street.setSuffix(suffix);
		}else{
			this.street = new Street.Builder(null, null).setSuffix(suffix).build();
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getPropertyType()
	 */
	@Override
	public <T extends AbstractPropertyType> T getPropertyType() {

		if(null == this.property)
			return null;
		
		return this.property.getType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)
	 */
	@Override
	public <T extends AbstractPropertyType> void setPropertyType(T type) {

		Boolean isTypeOfBuilding	= (type instanceof BuildingType);
		Boolean isTypeOfParcelOfLand= (type instanceof ParcelOfLandType);
		Boolean isBuilding			= (this.property instanceof Building);
		Boolean isParcelOfLand		= (this.property instanceof ParcelOfLand);
		
		Boolean isSame				= ((isBuilding && isTypeOfBuilding) || (isParcelOfLand && isTypeOfParcelOfLand));
				
		if(null != this.property && isSame){
			this.property.setType(type);
		}else{
			this.property = this.createProperty(type);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getBuildingNumber()
	 */
	@Override
	public String getBuildingNumber() {

		if(null == this.property)
			return null;
		
		String rtn = null;
		
		try{
			Building building = (Building)this.property;
			
			rtn = building.getBuildingNumber();
		}catch(ClassCastException exception){
			//Do nothing
		}
		
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setBuildingNumber(java.lang.String)
	 */
	@Override
	public void setBuildingNumber(String number) throws PropertyTypeNotMatchException {

		if(null == number)
			return;
		
		this.propertyChecking(Building.class);
		
		if( null == this.property ){
			this.property = new Building.Builder(null, null).build();			
		}
		
		Building building = (Building)this.property;
		building.setBuildingNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getLevel()
	 */
	@Override
	public Integer getLevel() {

		if( null == this.property || !(this.property instanceof Building) )
			return null;		
		
		Building building = (Building)this.property;
		
		return (null != building.getLevel()) ? new Integer(building.getLevel().getValue()) : null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setLevel(java.lang.Integer)
	 */
	@Override
	public void setLevel(Integer level) throws PropertyTypeNotMatchException {

		if(null == level)
			return;
		
		this.propertyChecking(Building.class);
		
		if( (null == this.property)){
			Level lv = new Level.Builder(null).setValue(level.toString()).build();
			this.property = new Building.Builder(null, null).setLevel(lv).build();
		}else{
			Building building = (Building)this.property;
			
			Level lv = building.getLevel();
			if( null == lv ){
				lv = new Level.Builder(null).build();
				building.setLevel(lv);
			}
			
			building.getLevel().setValue(level.toString());
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getLevelType()
	 */
	@Override
	public LevelType getLevelType() {

		if(!(this.property instanceof Building))
			return null;
		
		Building building = (Building)this.property;
		
		if(null == building.getLevel())
			return null;
		
		return building.getLevel().getType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)
	 */
	@Override
	public void setLevelType(LevelType type) throws PropertyTypeNotMatchException {

		if(null == type)
			return;
		
		this.propertyChecking(Building.class);
		
		if( (null == this.property)){
			Level lv = new Level.Builder(null).setType(type).build();
			this.property = new Building.Builder(null, null).setLevel(lv).build();
		}else{
			Building building = (Building)this.property;
			
			Level lv = building.getLevel();
			if( null == lv){
				lv = new Level.Builder(null).build();
				building.setLevel(lv);
			}
			
			building.getLevel().setType(type);
		}
	}

	/**
	 * This method is not supported by PhysicalDeliveryLocation
	 * 
	 * (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#getPostalDeliveryType()
	 */
	@Override
	@Deprecated
	public PostalDeliveryType getPostalDeliveryType() {

		throw new UnsupportedOperationException("Physical delivery location doesn't support getPostalDeliveryType()");
	}

	/**
	 * This method is not supported by PhysicalDeliveryLocation
	 *  
	 * (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)
	 */
	@Override
	@Deprecated
	public void setPostalDeliveryType(PostalDeliveryType type) {

		throw new UnsupportedOperationException("Physical delivery location doesn't support setPostalDeliveryType()");
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((property == null) ? 0 : property.hashCode());
		result = prime * result + ((street == null) ? 0 : street.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof PhysicalDeliveryLocation)) {
			return false;
		}
		PhysicalDeliveryLocation other = (PhysicalDeliveryLocation) obj;
		if (property == null) {
			if (other.property != null) {
				return false;
			}
		} else if (!property.equals(other.property)) {
			return false;
		}
		if (street == null) {
			if (other.street != null) {
				return false;
			}
		} else if (!street.equals(other.street)) {
			return false;
		}
		return true;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PhysicalDeliveryLocation clone() throws CloneNotSupportedException {

		return new PhysicalDeliveryLocation(this);
	}

	/**
	 * Create the property object referred to its type
	 * 
	 * @param type The type of property
	 * 
	 * @return The entity of property
	 */
	@SuppressWarnings("unchecked")
	protected <T extends Property> T createProperty(AbstractPropertyType type) {
		Property property  = null;
		
		if(type instanceof BuildingType){
			property = new Building.Builder(this.getLocationNumber(), null).setType(type).build();
		}else if(type instanceof ParcelOfLandType){
			property = new ParcelOfLand.Builder(this.getLocationNumber()).setType(type).build();
		}
		
		return (T) property;
	}
	
	/**
	 * Check the class type of property
	 * 
	 * @param <T> The class type of target property expected
	 * @throws PropertyTypeNotMatchException When the expected type is not match to the stored type.
	 * 
	 */
	protected <T> void propertyChecking(Class<T> expectedType) throws PropertyTypeNotMatchException {
		if(null == this.property)
			return;
		
		if(expectedType != this.property.getClass()){
			throw new PropertyTypeNotMatchException("The expected property is " + expectedType.getSimpleName() 
												  + ", but the stored property is " + this.getProperyName());	
		}
	}
	
	/**
	 * Returns the name of property
	 * 
	 * @return The name of property; null if the property has not been set
	 */
	protected String getProperyName() {
		if(null == this.property)
			return null;
		
		return this.property.getClass().getSimpleName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#copy()
	 */
	@Override
	public IDeliveryLocation copy() {

		return new PhysicalDeliveryLocation(this);
	}
}
