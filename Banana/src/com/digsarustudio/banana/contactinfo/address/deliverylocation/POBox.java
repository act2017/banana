/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import java.io.Serializable;

import com.digsarustudio.banana.contactinfo.address.IDeliveryLocation;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the P.O.Box system in Australia
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class POBox extends AbstractPOBox implements Serializable, Cloneable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 3580575352307338162L;
	
	/**
	 * To build the Australian P.O.Box object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<POBox>{
		private PostalDeliveryType type;
		private String locationNumber;
		/**
		 * @param type
		 * @param locationNumber
		 */
		public Builder(String locationNumber, PostalDeliveryType type) {
			this.type = type;
			this.locationNumber = locationNumber;
		}
		/**
		 * Returns the type of P.O.Box
		 * 
		 * @return the type of P.O.Box
		 */
		public PostalDeliveryType getType() {
			return type;
		}
		/**
		 * Assign a type to the P.O.Box
		 * 
		 * @param type the type for P.O.Box to set
		 * @return Builder
		 */
		public Builder setType(PostalDeliveryType type) {
			this.type = type;
			return this;
		}
		/**
		 * Returns the number of the P.O.Box
		 * @return the number of P.O.Box
		 */
		public String getLocationNumber() {
			return locationNumber;
		}
		/**
		 * Assign a number to the P.O.Box
		 * 
		 * @param locationNumber the number of P.O.Box to set
		 * @return Builder
		 */
		public Builder setLocationNumber(String locationNumber) {
			this.locationNumber = locationNumber;
			return this;
		}
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public POBox build() {
	
			return new POBox(this.locationNumber, this.type);
		}						
	}

	/**
	 * Constructs an Australian P.O.Box
	 * @param locationNumber the number of location
	 * @param type The postal delivery type
	 */
	public POBox(String locationNumber, PostalDeliveryType type) {
		super(locationNumber, type);

	}
	
	/**
	 * Copy an Australian P.O.Box object
	 */
	public POBox(POBox source) {
		super(source.getLocationNumber(), source.getType());

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public POBox clone() throws CloneNotSupportedException {

		return new POBox(this);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IDeliveryLocation#copy()
	 */
	@Override
	public IDeliveryLocation copy() {

		return new POBox(this);
	}
	
}
