/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.deliverylocation;

import com.digsarustudio.banana.contactinfo.address.IDeliveryLocation;

/**
 * The definition of delivery location
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractDeliveryLocation implements IDeliveryLocation {
	/**
	 * The number of location
	 */
	private String locationNumber;

	/**
	 * Constructs a delivery location object
	 */
	public AbstractDeliveryLocation(String number) {
		this.setLocationNumber(number);
	}

	/**
	 * Returns the number of location
	 * 
	 * @return the number of location
	 */
	@Override
	public String getLocationNumber() {
		return locationNumber;
	}

	/**
	 * Assign a number to the location
	 * 
	 * @param number the number for the location to set
	 */
	@Override
	public void setLocationNumber(String number) {
		this.locationNumber = number;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((locationNumber == null) ? 0 : locationNumber.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractDeliveryLocation)) {
			return false;
		}
		AbstractDeliveryLocation other = (AbstractDeliveryLocation) obj;
		if (locationNumber == null) {
			if (other.locationNumber != null) {
				return false;
			}
		} else if (!locationNumber.equals(other.locationNumber)) {
			return false;
		}
		return true;
	}
}
