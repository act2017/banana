/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import java.util.HashSet;

/**
 * The type of parcel of land
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class ParcelOfLandType extends AbstractPropertyType {

	public static ParcelOfLandType Lot = new ParcelOfLandType("Lot", "LOT");
	public static ParcelOfLandType Section = new ParcelOfLandType("Section", "SECTION");
		
	/**
	 * The list of lot types
	 */
	private static HashSet<ParcelOfLandType> all = new HashSet<>();
	
	static{
		all.add(Lot);
		all.add(Section);
	}
	
	/**
	 * Constructs a lot type
	 * 
	 * @param name the name of lot type
	 * @param abbreviation the abbreviation of lot type
	 */
	protected ParcelOfLandType(String name, String abbreviation) {
		super(name, abbreviation);

	}
	
	
	/**
	 * Returns the type according to the name of lot type
	 * 
	 * @param name The name of lot type
	 * 
	 * @return The type relative to the name of lot type
	 */
	public static ParcelOfLandType getTypeFromName(String name){
		ParcelOfLandType rtn = null;
		for (ParcelOfLandType target : all) {
			rtn = target.fromValue(name);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns the type according to the abbreviation of lot type
	 * 
	 * @param name The name of abbreviation
	 * 
	 * @return The type relative to the name of abbreviation
	 */
	public static ParcelOfLandType getTypeFromAbbreviation(String abbrreviation){
		ParcelOfLandType rtn = null;
		for (ParcelOfLandType target : all) {
			rtn = target.fromAbbreviation(abbrreviation);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static ParcelOfLandType[] values(){
		ParcelOfLandType[] rtn = new ParcelOfLandType[all.size()];
		
		rtn = all.toArray(rtn);
		
		return rtn;
	}
	

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected HashSet<ParcelOfLandType> getValues() {

		return all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected ParcelOfLandType self() {

		return this;
	}
}
