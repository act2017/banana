/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import java.util.HashSet;

import com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum;

/**
 * The enumeration of postal delivery type
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PostalDeliveryType extends AbstractAbbreviatableEnum {

	public final static PostalDeliveryType CareOfPostOffice			= new PostalDeliveryType("Care of Post Office", "CARE PO"); 
	public final static PostalDeliveryType CommunityMailAgent		= new PostalDeliveryType("Community Mail Agent.", "CMA");
	public final static PostalDeliveryType CommunityMailBag			= new PostalDeliveryType("Community Mail Bag", "CMB");
	public final static PostalDeliveryType CommunityPostalAgent		= new PostalDeliveryType("Community Postal Agent", "CPA");
	public final static PostalDeliveryType GeneralPostOfficeBox		= new PostalDeliveryType("General Post Office Box", "GPO BOX"); 	
	public final static PostalDeliveryType LockedBag				= new PostalDeliveryType("Locked Bag", "LOCKED BAG");
	public final static PostalDeliveryType MailService				= new PostalDeliveryType("Mail Service", "MS");
	public final static PostalDeliveryType PostOfficeBox			= new PostalDeliveryType("Post Office Box", "PO BOX");
	public final static PostalDeliveryType RoadsideDelivery			= new PostalDeliveryType("Roadside Delivery", "RSD");
	public final static PostalDeliveryType RoadsideMailBoxOrBag		= new PostalDeliveryType("Roadside Mail Box/Bag", "RMB");
	public final static PostalDeliveryType RoadsideMailService		= new PostalDeliveryType("Roadside Mail Service", "RMS"); 
	public final static PostalDeliveryType PrivateBag				= new PostalDeliveryType("Private Bag", "PRIVATE BAG");
	
	private final static HashSet<PostalDeliveryType> all = new HashSet<>();
	
	static{
		all.add(CareOfPostOffice);
		all.add(CommunityMailAgent);
		all.add(CommunityMailBag);
		all.add(CommunityPostalAgent);
		all.add(GeneralPostOfficeBox);
		all.add(LockedBag);
		all.add(MailService);
		all.add(PostOfficeBox);
		all.add(RoadsideDelivery);
		all.add(RoadsideMailBoxOrBag);
		all.add(RoadsideMailService);
		all.add(PrivateBag);
	}
	
	/**
	 * Constructs a P.O.Box type object
	 * 
	 * @param name The name of P.O.Box type
	 * @param abbreviation The abbreviation of P.O.Box type
	 */
	private PostalDeliveryType(String name, String abbreviation) {
		super(name, abbreviation);

	}
	
	/**
	 * Returns P.O.Box type according to the name of P.O.Box type
	 * 
	 * @param name The name of P.O.Box type
	 * 
	 * @return The type of P.O.Box if found, otherwise null.
	 */
	public static PostalDeliveryType getTypeFromName(String name){
		PostalDeliveryType rtn = null;
		for (PostalDeliveryType streetType : all) {
			if(null != (rtn = streetType.fromValue(name)) )
				break;
		}
		
		return rtn;		
	}
	
	/**
	 * Returns the P.O.Box type according to the abbreviation of P.O.Box type
	 * 
	 * @param abbreviation The abbreviation of P.O.Box type
	 * 
	 * @return The type of P.O.Box if found, otherwise null.
	 */
	public static PostalDeliveryType getTypeFromAbbreviation(String abbreviation){
		PostalDeliveryType rtn = null;
		for (PostalDeliveryType streetType : all) {
			if(null != (rtn = streetType.fromAbbreviation(abbreviation)) )
				break;
		}
		
		return rtn;		
	}
	
	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static PostalDeliveryType[] values(){
		PostalDeliveryType[] rtn = new PostalDeliveryType[all.size()];
		
		rtn = all.toArray(rtn);
		
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected HashSet<PostalDeliveryType> getValues() {

		return all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected PostalDeliveryType self() {

		return this;
	}
}
