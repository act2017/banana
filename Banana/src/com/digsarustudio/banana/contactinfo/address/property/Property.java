/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import java.io.Serializable;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents a property
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class Property implements Serializable, Cloneable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -7693316420356234704L;

	/**
	 * The type of property. It could be the building type or lot type.
	 * It is optional when the type of property is BUILDING, otherwise it is compulsory when
	 * the type of property is LOT 
	 */
	private AbstractPropertyType type;
	
	/*
	 * The number of the property. It could be "22" or "1-3"
	 */
	private String number;
	
	public static class Builder implements ObjectBuilder<Property>{
		private AbstractPropertyType type;
		private String number;		
				
		/**
		 * @param number
		 */
		public Builder(String number) {
			super();
			this.number = number;
		}

		/**
		 * Returns the type of property
		 * 
		 * @return the type of property
		 */
		@SuppressWarnings("unchecked")
		public <T extends AbstractPropertyType> T getType() {
			return (T) type;
		}

		/**
		 * Assign a type to the property
		 * 
		 * @param type the type to set
		 * 
		 * @return the builder
		 */
		public Builder setType(AbstractPropertyType type) {
			this.type = type;
			return this;
		}

		/**
		 * Returns the number of property
		 * 
		 * @return the number of property
		 */
		public String getNumber() {
			return number;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public Property build() {
	
			return new Property(this);
		}		
	}
	
	/**
	 * Constructs a property object by the builder
	 */
	private Property(Builder builder) {

		this.setPropertyNumber(builder.getNumber());
		this.setType(builder.getType());
	}
	
	/**
	 * For sub-class to use
	 * 
	 * @param number The number of property
	 * @param type The type of property
	 */
	protected <T extends AbstractPropertyType> Property(String number, T type){
		this.setPropertyNumber(number);
		this.setType(type);
	}
	
	/**
	 * Copy constructor
	 * 
	 * @param source The source to copy
	 */
	public Property(Property source){
		this.setPropertyNumber(source.getPropertyNumber());
		this.setType(source.getType());
	}

	/**
	 * Returns the type of property
	 * 
	 * @return the type of property
	 */
	@SuppressWarnings("unchecked")
	public <T extends AbstractPropertyType> T getType() {
		return (T) type;
	}

	/**
	 * Assign a type to the property
	 * 
	 * @param type the type of property to set
	 */
	public <T extends AbstractPropertyType> void setType(T type) {
		this.type = type;
	}

	/**
	 * Returns the number of property
	 * 
	 * @return the number
	 */
	public String getPropertyNumber() {
		return number;
	}

	/**
	 * Assign a number to property
	 * 
	 * @param number the number of property to set
	 */
	public void setPropertyNumber(String number) {
		this.number = number;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Property)) {
			return false;
		}
		
		Property other = (Property) obj;
		if (number == null) {
			if (other.number != null) {
				return false;
			}
		} else if (!number.equals(other.number)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Property clone() throws CloneNotSupportedException {

		return new Property(this);
	}

}
