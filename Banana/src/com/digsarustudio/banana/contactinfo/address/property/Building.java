/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

/**
 * Represents the property as a building
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class Building extends Property implements Cloneable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 3366176216840868996L;

	/**
	 * The level of building.(Optional)
	 * See {@link com.digsarustudio.banana.contractinfo.address.property.BuildingType}
	 */
	private Level level;
	
	/**
	 * The number of this building.
	 */
	private String buildingNumber;
	
	/**
	 * A builder to build Building object.
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder extends Property.Builder{
		private Level level;
		private String buildingNumber;
				
		/**
		 * Constructs a builder  object
		 * 
		 * @param propertyNumber The number of property
		 * @param buidlingNumber The number of building 
		 */
		public Builder(String propertyNumber, String buildingNumber) {
			super(propertyNumber);
			this.buildingNumber = buildingNumber;
		}

		/**
		 * Assign the type of the building
		 * 
		 * @param type of the building
		 * @return builder
		 */
		public Builder setType(BuildingType type){
			super.setType(type);
			return this;
		}
				
		/**
		 * Returns the number of property
		 * 
		 * @return the number of property
		 */
		public String getPropertyNumber() {
			return super.getNumber();
		}
		
		/**
		 * Returns the number of building
		 * 
		 * @return The number of building
		 */
		public String getBuildingNumber(){
			return this.buildingNumber;
		}

		/**
		 * Returns the type of building
		 * 
		 * @return the type of building
		 */
		@SuppressWarnings("unchecked")
		public BuildingType getType() {
			return super.getType();
		}		

		/**
		 * Returns the level of building
		 * 
		 * @return the level of building
		 */
		public Level getLevel() {
			return level;
		}

		/**
		 * Assign the level to this building
		 * @param level the level of building to set
		 */
		public Builder setLevel(Level level) {
			this.level = level;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public Building build() {
	
			return new Building(this);
		}		
	}
	
	/**
	 * Constructs a building object
	 */
	private Building(Builder builder) {
		super(builder.getPropertyNumber(), builder.getType());
		this.setLevel(builder.getLevel());
		this.setBuildingNumber(builder.getBuildingNumber());
	}
	
	/**
	 * Copy a building object
	 * 
	 * @param source The source building
	 */
	public Building(Building source){
		super(source.getPropertyNumber(), source.getType());
		if( null != source.getLevel()){
			this.setLevel(new Level.Builder(source.getLevel().getType())
								   .setValue(source.getLevel().getValue())
								   .build());
		}
		this.setBuildingNumber(source.getBuildingNumber());
	}	

	/**
	 * Returns the level of building
	 * 
	 * @return the level of building
	 */
	public Level getLevel() {
		return level;
	}

	/**
	 * Assign a level to the building
	 * @param level the level of building to set
	 */
	public void setLevel(Level level) {
		this.level = level;
	}

	/**
	 * Returns the number of building
	 * 
	 * @return the number of building 
	 */
	public String getBuildingNumber() {
		return buildingNumber;
	}

	/**
	 * Assign a number to the building
	 * 
	 * @param buildingNumber the number of building to set
	 */
	public void setBuildingNumber(String buildingNumber) {
		this.buildingNumber = buildingNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((buildingNumber == null) ? 0 : buildingNumber.hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof Building)) {
			return false;
		}
		Building other = (Building) obj;
		if (buildingNumber == null) {
			if (other.buildingNumber != null) {
				return false;
			}
		} else if (!buildingNumber.equals(other.buildingNumber)) {
			return false;
		}
		if (level == null) {
			if (other.level != null) {
				return false;
			}
		} else if (!level.equals(other.level)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Building clone() throws CloneNotSupportedException{
		
		return new Building(this);
	}

}
