/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import java.io.Serializable;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents a level in a building
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class Level implements Cloneable, Serializable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -4384807654550568666L;

	/**
	 * The number of level
	 */
	private String value;
	
	/**
	 * The type of level
	 */
	private LevelType type;
	
	/**
	 * Build the level object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		0.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<Level>{
		private String value;
		private LevelType type;
		
		public Builder(LevelType type){
			this.type = type;
		}

		/**
		 * Returns the number of level
		 * @return the number of level level
		 */
		public String getValue() {
			return value;
		}

		/**
		 * Assign the number of level
		 * @param level the number of level to set
		 */
		public Builder setValue(String value) {
			this.value = value;
			return this;
		}

		/**
		 * Returns the type of level
		 * @return the type of level
		 */
		public LevelType getType() {
			return type;
		}

		/**
		 * Assign the type of level
		 * @param type the type of level to set
		 */
		public Builder setType(LevelType type) {
			this.type = type;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public Level build() {
	
			return new Level(this);
		}				
	}
	
	/**
	 * Constructs a level object
	 */
	private Level(Builder builder) {

		this.setType(builder.getType());
		this.setValue(builder.getValue());
	}
	
	/**
	 * Copy constructor
	 * 
	 * @param source The source object to copy
	 */
	public Level(Level source){
		this.setType(source.getType());
		this.setValue(source.getValue());
	}

	/**
	 * Returns the number of level
	 * @return the level
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Assign the number of level
	 * @param level the level to set
	 */
	public void setValue(String level) {
		this.value = level;
	}

	/**
	 * Returns the type of level
	 * @return the type
	 */
	public LevelType getType() {
		return type;
	}

	/**
	 * Assign the type of level
	 * @param type the type to set
	 */
	public void setType(LevelType type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Level)) {
			return false;
		}
		Level other = (Level) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Level clone() throws CloneNotSupportedException {

		return new Level.Builder(this.getType()).setValue(this.getValue()).build();
	}
}
