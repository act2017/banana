/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum;

/**
 * Defining the boilerplate of property type
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractPropertyType extends AbstractAbbreviatableEnum {

	/**
	 * Constructs a property type
	 * 
	 * @param value The value of type
	 * @param abbreviation The abbreviation of value of type
	 */
	public AbstractPropertyType(String value, String abbreviation) {
		super(value, abbreviation);
	}
}
