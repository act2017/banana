/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

/**
 * Thrown by a address to indicate the input attribute is not match to the property set.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class PropertyTypeNotMatchException extends Exception {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 1692435787923739722L;

	/**
	 * 
	 */
	public PropertyTypeNotMatchException() {

	}

	/**
	 * @param message
	 */
	public PropertyTypeNotMatchException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public PropertyTypeNotMatchException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public PropertyTypeNotMatchException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public PropertyTypeNotMatchException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
