/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

/**
 * Represents the property as a parcel of land, it could be lot or section.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class ParcelOfLand extends Property {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -6491017860934791137L;

	/**
	 * 
	 * The builder to build parcel of land object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder extends Property.Builder{

		/**
		 * Constructs a builder for parcel of land
		 * @param number The number of parcel of land
		 */
		public Builder(String number) {
			super(number);
	
		}			

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.contactinfo.address.property.Property.Builder#getType()
		 */
		@SuppressWarnings("unchecked")
		@Override
		public ParcelOfLandType getType() {
	
			return super.getType();
		}

		/**
		 * Assign the type to this parcel of land
		 * @param type The type for this parcel of land
		 * @return The builder
		 */
		public Builder setType(ParcelOfLandType type) {
	
			super.setType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.contactinfo.address.property.Property.Builder#getNumber()
		 */
		@Override
		public String getNumber() {
	
			return super.getNumber();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.contactinfo.address.property.Property.Builder#build()
		 */
		@Override
		public ParcelOfLand build() {
	
			return new ParcelOfLand(this);
		}									
	}
	
	/**
	 * Constructs a parcel of land object
	 * 
	 * @param builder The builder
	 */
	private ParcelOfLand(Builder builder){
		super(builder.getNumber(), builder.getType());
	}
	
	/**
	 * Copy a parcel of land
	 * 
	 * @param source The source data
	 */
	public ParcelOfLand(Property source) {
		super(source);
		

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public ParcelOfLand clone() throws CloneNotSupportedException {

		return new ParcelOfLand(this);
	}

}
