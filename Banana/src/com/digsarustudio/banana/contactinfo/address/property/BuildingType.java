/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import java.util.HashSet;

/**
 * The type of building
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class BuildingType extends AbstractPropertyType {

	public final static BuildingType Apartment = new BuildingType("Apartment", "APT");
	public final static BuildingType Factory = new BuildingType("Factory", "FY");
	public final static BuildingType Flat = new BuildingType("Flat", "F");
	public final static BuildingType MarineBerth = new BuildingType("Marine berth", "MB");
	public final static BuildingType Office = new BuildingType("Office", "OFF");
	public final static BuildingType Room = new BuildingType("Room", "RM");
	public final static BuildingType Shed = new BuildingType("Shed", "SHED");
	public final static BuildingType Shop = new BuildingType("Shop", "SHOP");	
	public final static BuildingType Site = new BuildingType("Site", "SITE");
	public final static BuildingType Stall = new BuildingType("Stall", "SL");
	public final static BuildingType Suite = new BuildingType("Suite", "SE");
	public final static BuildingType Villa = new BuildingType("Villa", "VLLA");
	public final static BuildingType Warehouse = new BuildingType("Warehouse", "WE");
	public final static BuildingType Unit = ((BuildingType)(new BuildingType("Unit", "U").addOtherAbbreviation("Unit")))
																		 				 .addOtherAbbreviation("UNIT");
	
	/**
	 * The set of enumerators
	 */
	private final static HashSet<BuildingType> all = new HashSet<BuildingType>();
	
	/**
	 * Used to put the enumerators into the static list, otherwise there is nothing in the [all].
	 */
	static{
		all.add(Apartment);
		all.add(Factory);
		all.add(Flat);
		all.add(MarineBerth);
		all.add(Office);
		all.add(Room);
		all.add(Shed);
		all.add(Shop);
		all.add(Site);
		all.add(Stall);
		all.add(Suite);
		all.add(Villa);
		all.add(Warehouse);
		all.add(Unit);
	}
	
	/**
	 * Constructs an object
	 * 
	 * @param name The name of unit type
	 * @param abbreviation The abbreviation of unit type
	 */
	protected BuildingType(String name, String abbreviation) {
		super(name, abbreviation);
	}

	
	/**
	 * Returns the type according to the name of unit type
	 * 
	 * @param name The name of unit type
	 * 
	 * @return The type relative to the name of unit type
	 */
	public static BuildingType getTypeFromName(String name){
		BuildingType rtn = null;
		for (BuildingType country : all) {
			rtn = country.fromValue(name);
		}
		
		return rtn;
	}
	
	/**
	 * Returns the type according to the abbreviation of unit type
	 * 
	 * @param name The name of abbreviation
	 * 
	 * @return The type relative to the name of abbreviation
	 */
	public static BuildingType getTypeFromAbbreviation(String abbrreviation){
		BuildingType rtn = null;
		for (BuildingType country : all) {
			rtn = country.fromAbbreviation(abbrreviation);
		}
		
		return rtn;
	}
	
	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static BuildingType[] values(){
		BuildingType[] rtn = new BuildingType[all.size()];
		
		rtn = all.toArray(rtn);
		
		return rtn;
	}


	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected HashSet<BuildingType> getValues() {

		return all;
	}


	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected BuildingType self() {

		return this;
	}
}
