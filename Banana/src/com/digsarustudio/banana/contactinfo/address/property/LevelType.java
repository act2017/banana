/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.property;

import java.util.HashSet;

/**
 * The type of level.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class LevelType extends AbstractPropertyType {

	public static LevelType Basement			= new LevelType("Basement", "B");
	public static LevelType Floor				= new LevelType("Floor", "FL");
	public static LevelType GroundFloor			= new LevelType("Ground Floor", "G");
	public static LevelType Level				= new LevelType("Level", "L");
	public static LevelType LowerGroundFloor	= new LevelType("Lower Ground Floor", "LG");
	public static LevelType Mezzanine			= new LevelType("Mezzanine", "M");
	public static LevelType UpperGroundFloor	= new LevelType("Upper Ground Floor", "UG");
	
	private static HashSet<LevelType> all = new HashSet<>();
	
	static{
		all.add(Basement);
		all.add(Floor);
		all.add(GroundFloor);
		all.add(Level);
		all.add(LowerGroundFloor);
		all.add(Mezzanine);
		all.add(UpperGroundFloor);
	}
	
	/**
	 * Constructs a level type
	 * 
	 * @param name the name of level type
	 * @param abbreviation the abbreviation of level type
	 */
	private LevelType(String name, String abbreviation){
		super(name, abbreviation);
	}
	
	/**
	 * Returns the type according to the name of type
	 * 
	 * @param name The name of type
	 * 
	 * @return The type relative to the name of type
	 */
	public static LevelType getTypeFromName(String name){
		LevelType rtn = null;
		for (LevelType country : all) {
			rtn = country.fromValue(name);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns the type according to the abbreviation of type
	 * 
	 * @param name The name of abbreviation
	 * 
	 * @return The type relative to the name of abbreviation
	 */
	public static LevelType getTypeFromAbbreviation(String abbrreviation){
		LevelType rtn = null;
		for (LevelType country : all) {
			rtn = country.fromAbbreviation(abbrreviation);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static LevelType[] values(){
		LevelType[] rtn = new LevelType[all.size()];
		
		rtn = all.toArray(rtn);
		
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected HashSet<LevelType> getValues() {

		return all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected LevelType self() {

		return this;
	}
}
