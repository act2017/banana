/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * Defining the operation of delivery location type
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface IDeliveryLocation{

	/**
	 * Returns the name of street
	 * 
	 * @return The name of street; null if the name of street has not been set.
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	String getStreetName();
	
	/**
	 * Assign a name to the street
	 * 
	 * @param name The name for the street
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setStreetName(String name);
	
	/**
	 * Assign a name and suffix to the street
	 * 
	 * @param name The name for the street
	 * @param suffix The suffix for the street
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setStreetName(String name, String suffix);
	
	/**
	 * Returns the type of street
	 * 
	 * @return The type of street; null if the type of street has not been set.
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	StreetType getStreetType();
	
	/**
	 * Assign the type to the street
	 * 
	 * @param type The type for the street
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setStreetType(StreetType type);
	
	/**
	 * Returns the suffix of the street
	 * 
	 * @return The suffix of the street
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	String getStreetSuffix();
	
	/**
	 * Assign a suffix to the street
	 * 
	 * @param suffix The suffix for the street
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setStreetSuffix(String suffix);
			
	/**
	 * Returns the number of location
	 * 
	 * @return The number for the location
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	String getLocationNumber();
	
	/**
	 * Assign a number to the location
	 * 
	 * @param number The number for the location
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setLocationNumber(String number);
	
	/**
	 * Returns the type of property
	 * 
	 * @return The type of property
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	<T extends AbstractPropertyType> T getPropertyType();
	
	/**
	 * Assign a type to the property
	 * 
	 * @param type The type for the property
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	<T extends AbstractPropertyType> void setPropertyType(T type);
	
	/**
	 * Returns the number of building
	 * 
	 * @return The number of building
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	String getBuildingNumber();
	
	/**
	 * Set the number to the building
	 * 
	 * @param number The number for the building
	 * @throws PropertyTypeNotMatchException When the stored property is not a building
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setBuildingNumber(String number) throws PropertyTypeNotMatchException;
	
	/**
	 * Returns the level of the building
	 * 
	 * @return The level of the building
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	Integer getLevel();
	
	/**
	 * Assign the level to the building
	 * 
	 * @param level The level to the building
	 * @throws PropertyTypeNotMatchException When the stored property is not a building
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setLevel(Integer level) throws PropertyTypeNotMatchException;
	
	/**
	 * Returns the type of level
	 * 
	 * @return The type of level
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	LevelType getLevelType();
	
	/**
	 * Assign the type to the level
	 * 
	 * @param type The type for the level
	 * @throws PropertyTypeNotMatchException When the stored property is not a building
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of postal delivery type location
	 */
	void setLevelType(LevelType type) throws PropertyTypeNotMatchException;
	
	/**
	 * Returns the postal delivery type
	 * 
	 * @return The postal delivery type
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of physical delivery location
	 */
	PostalDeliveryType getPostalDeliveryType();
	
	/**
	 * Assign a postal delivery type
	 * 
	 * @param type The postal delivery type
	 * 
	 * @exception UnsupportedOperationException when the sub-class is a sort of physical delivery location
	 */
	void setPostalDeliveryType(PostalDeliveryType type);

	/**
	 * Returns a copy of IDeliveryLocation
	 * 
	 * @return a copy of IDeliveryLocation
	 */
	IDeliveryLocation copy();
	
	
}
