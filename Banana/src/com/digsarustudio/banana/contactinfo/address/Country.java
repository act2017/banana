/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types represents a particular country in the world.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	The new implementation is in the Musaceae module.
 */
public interface Country {
	/**
	 * 
	 * The builder for {@link Country}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<Country> {
		Builder setName(String name);
		Builder setDisplayString(String display);
	}
	
	void setName(String name);
	void setDisplayString(String display);
	
	String getName();
	String getDisplayString();
}
