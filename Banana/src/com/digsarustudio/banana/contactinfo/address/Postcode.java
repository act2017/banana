/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types represents the postcode for a particular address.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	The new implementation is in the Musaceae module.
 */
public interface Postcode {
	/**
	 * 
	 * The builder for {@link Postcode}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<Postcode> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	
	String getValue();
}
