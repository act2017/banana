/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents the state of a country.<br>
 * In practice, the details of state should be from the database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface State {
	/**
	 * 
	 * The builder for {@link State}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<State> {
		Builder setName(String name);
		Builder setDisplayString(String display);
	}
	
	void setName(String name);
	void setDisplayString(String display);
	
	String getName();
	String getDisplayString();
}
