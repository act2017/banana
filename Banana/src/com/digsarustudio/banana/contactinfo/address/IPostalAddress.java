/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;

/**
 * Defining the operation of postal address type
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface IPostalAddress extends IResidentialAddress {
	/**
	 * Returns the number of P.O.Box
	 * 
	 * @return The number of P.O.Box
	 */
	String getPOBoxNumber();
	
	/**
	 * Assign a number to the P.O.Box
	 * 
	 * @param number The number for the P.O.Box
	 */
	void setPOBoxNumber(String number);
	
	
	/**
	 * Returns the postal delivery type
	 * 
	 * @return The postal delivery type
	 */
	PostalDeliveryType getPostalDeliveryType();
	
	/**
	 * Assign the postal delivery type
	 * 
	 * @param type The postal delivery type
	 */
	void setPostalDeliveryType(PostalDeliveryType type);
}
