/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import java.io.Serializable;

import com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.CityNotSetException;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.StateNotSetException;
import com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType;
import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the address of resident, it is a physical address in a post system.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public class ResidentialAddress implements IResidentialAddress,Serializable, Cloneable {	
	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 7463281195918269174L;

	/**
	 * To build the residential address object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<ResidentialAddress> {
		private Countries					country;
		
		private IEnumAdministrativeDivision state;
		private IEnumAdministrativeDivision city;
		private IEnumAdministrativeDivision suburb;
		
		private String						streetName;
		private StreetType					streetType;
		private String						streetSuffix;
		
		private String						propertyNumber;
		private AbstractPropertyType		propertyType;
		
		private String						buildingNumber;
		private Integer						level;
		private LevelType					levelType;
		
		/**
		 * Constructs a builder  object 
		 * 
		 * @param country The country of the target address
		 */
		public Builder(Countries country) {
	
			this.country = country;
		}
		
		/**
		 * Returns the country of the target address
		 * 
		 * @return the country of the target address
		 */
		public Countries getCountry() {
			return country;
		}

		/**
		 * Returns the enumerator indicates the state for the target address
		 * 
		 * @return the enumerator indicates the state for the target address
		 */
		public IEnumAdministrativeDivision getState() {
			return state;
		}

		/**
		 * Assign a state to the target address
		 * 
		 * @param state the state for the target address
		 */
		public Builder setState(IEnumAdministrativeDivision state) {
			this.state = state;
			
			return this;
		}

		/**
		 * Returns the enumerator indicates city for the target address
		 * 
		 * @return the enumerator indicates city for the target address
		 */
		public IEnumAdministrativeDivision getCity() {
			return city;
		}

		/**
		 * Assign a city to the target address
		 * 
		 * @param city the city for the target address
		 */
		public Builder setCity(IEnumAdministrativeDivision city) {
			this.city = city;
			
			return this;
		}

		/**
		 * Returns the enumerator indicates the suburb for the target address
		 * 
		 * @return the enumerator indicates the suburb for the target address
		 */
		public IEnumAdministrativeDivision getSuburb() {
			return suburb;
		}
		
		/**
		 * Assign a suburb to the target address
		 * 
		 * @param suburb the suburb for the target address
		 */
		public Builder setSuburb(IEnumAdministrativeDivision suburb) {
			this.suburb = suburb;
			
			return this;
		}

		/**
		 * Returns the name of street
		 * 
		 * @return the name of street
		 */
		public String getStreetName() {
			return streetName;
		}

		/**
		 * Assign a name to the street
		 * 
		 * @param name the name for the street
		 */
		public Builder setStreetName(String name) {
			this.streetName = name;
			
			return this;
		}

		/**
		 * Returns the type of street
		 * 
		 * @return the type of street
		 */
		public StreetType getStreetType() {
			return streetType;
		}

		/**
		 * Assign a type to the street
		 * 
		 * @param streetType the type for the street
		 */
		public Builder setStreetType(StreetType type) {
			this.streetType = type;
			
			return this;
		}
		
		/**
		 * Returns the suffix of street
		 * 
		 * @return the suffix of street
		 */
		public String getStreetSuffix() {
			return streetSuffix;
		}
		
		/**
		 * Assign a suffix to the street
		 * 
		 * @param suffix the suffix to the street
		 */
		public Builder setStreetSuffix(String suffix) {
			this.streetSuffix = suffix;
			
			return this;
		}

		/**
		 * Returns the number of property
		 * 
		 * @return the number of property
		 */
		public String getPropertyNumber() {
			return propertyNumber;
		}

		/**
		 * Assign a number to the property
		 * 
		 * @param number The number for the property
		 */
		public Builder setPropertyNumber(String number) {
			this.propertyNumber = number;
			
			return this;
		}

		/**
		 * Returns the type of property
		 * 
		 * @return the type of property
		 */
		public AbstractPropertyType getPropertyType() {
			return propertyType;
		}

		/**
		 * Assign a type to the property
		 * 
		 * @param type The type for the property
		 */
		public Builder setPropertyType(AbstractPropertyType type) {
			this.propertyType = type;
			
			return this;
		}

		/**
		 * Returns the number of building
		 * 
		 * @return the number of building
		 */
		public String getBuildingNumber() {
			return buildingNumber;
		}

		/**
		 * Assign a number to the building
		 * 
		 * @param number The number for the building
		 * @throws PropertyTypeNotMatchException When the type of property is not a building
		 */
		public Builder setBuildingNumber(String number) throws PropertyTypeNotMatchException {
			if(null != this.propertyType && !(this.propertyType instanceof BuildingType))
				throw new PropertyTypeNotMatchException("Please set the property type as building type before call this function");
			
			this.buildingNumber = number;
			
			return this;
		}

		/**
		 * Returns the level of building
		 * 
		 * @return the level of building
		 */
		public Integer getLevel() {
			return level;
		}

		/**
		 * Assign a level to the building
		 * 
		 * @param level the level for the building to set
		 * @throws PropertyTypeNotMatchException When the type of property is not a building
		 */
		public Builder setLevel(Integer level) throws PropertyTypeNotMatchException {
			if(null != this.propertyType && !(this.propertyType instanceof BuildingType))
				throw new PropertyTypeNotMatchException("Please set the property type as building type before call this function");
			
			this.level = level;
			
			return this;
		}
		
		/**
		 * Returns the type of level of building
		 * 
		 * @return the type of level of the building
		 */
		public LevelType getLevelType() {
			return levelType;
		}

		/**
		 * Assign a type of level to the building
		 * 
		 * @param type the type for the building to set
		 * @throws PropertyTypeNotMatchException When the type of property is not a building
		 */
		public Builder setLevelType(LevelType type) throws PropertyTypeNotMatchException {
			if(null != this.propertyType && !(this.propertyType instanceof BuildingType))
				throw new PropertyTypeNotMatchException("Please set the property type as building type before call this function");
			
			this.levelType = type;
			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public ResidentialAddress build() {
			return new ResidentialAddress(this);
		}
	}
	
	
	/**
	 * The political boundaries of this address
	 */
	private PoliticalBoundaries boundaries;
	
	/**
	 * The physical location for delivery
	 */
	private PhysicalDeliveryLocation location;

	/**
	 * Constructs a residential address 
	 * 
	 * @param builder the builder builds residential address object
	 */
	private ResidentialAddress(Builder builder){
		this.boundaries = new PoliticalBoundaries.Builder(builder.getCountry())
												 .setState(builder.getState())
												 .setCity(builder.getCity())
												 .setSuburb(builder.getSuburb())
												 .build();

		this.location = new PhysicalDeliveryLocation.Builder(builder.getPropertyNumber())
													.setStreetName(builder.getStreetName())
													.setStreetType(builder.getStreetType())
													.setStreetSuffix(builder.getStreetSuffix())
													.setPropertyType(builder.getPropertyType())
													.build();
		
		if( null == builder.getPropertyType() || (builder.getPropertyType() instanceof BuildingType)){
			try {			
				this.location.setBuildingNumber(builder.getBuildingNumber());
				this.location.setLevel(builder.getLevel());
				this.location.setLevelType(builder.getLevelType());
			} catch (PropertyTypeNotMatchException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Constructs a residential address by copy
	 * 
	 * @param source The source object to copy
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	public ResidentialAddress(ResidentialAddress source) throws StateNotSetException, CityNotSetException {

		this.boundaries = new PoliticalBoundaries.Builder(source.getCountry())
												 .setState(source.getState())
												 .setCity(source.getCity())
												 .setSuburb(source.getSuburb())
												 .build();

		this.location = new PhysicalDeliveryLocation.Builder(source.getPropertyNumber())
													.setStreetName(source.getStreetName())
													.setStreetType(source.getStreetType())
													.setStreetSuffix(source.getStreetSuffix())
													.setPropertyType(source.getPropertyType())
													.build();

		if( null == source.getPropertyType() || (source.getPropertyType() instanceof BuildingType)){
			try {			
				this.location.setBuildingNumber(source.getBuildingNumber());
				this.location.setLevel(source.getLevel());
				this.location.setLevelType(source.getLevelType());
			} catch (PropertyTypeNotMatchException e) {
				e.printStackTrace();
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getCountry()
	 */
	@Override
	public Countries getCountry() {
		
		return this.boundaries.getCountry();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)
	 */
	@Override
	public void setCountry(Countries country) {
		
		this.boundaries.setCountry(country);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getState()
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> T getState() {
		
		return this.boundaries.getState();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setState(T state) {
		
		this.boundaries.setState(state);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getCity()
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> T getCity() {
		
		return this.boundaries.getCity();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setCity(T city) throws StateNotSetException {
		
		this.boundaries.setCity(city);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getSuburb()
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> T getSuburb() {
		
		return this.boundaries.getSuburb();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setSuburb(T suburb) throws CityNotSetException {
		
		this.boundaries.setSuburb(suburb);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getPostcode()
	 */
	@Override
	public String getPostcode() {
		
		return this.boundaries.getPostcode();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getStreetName()
	 */
	@Override
	public String getStreetName() {
		
		return this.location.getStreetName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setStreetName(java.lang.String)
	 */
	@Override
	public void setStreetName(String name) {
		
		this.location.setStreetName(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getStreetType()
	 */
	@Override
	public StreetType getStreetType() {
		
		return this.location.getStreetType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)
	 */
	@Override
	public void setStreetType(StreetType type) {
		
		this.location.setStreetType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getStreetSuffix()
	 */
	@Override
	public String getStreetSuffix() {
		
		return this.location.getStreetSuffix();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setStreetSuffix(java.lang.String)
	 */
	@Override
	public void setStreetSuffix(String suffix) {
		
		this.location.setStreetSuffix(suffix);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getPropertyNumber()
	 */
	@Override
	public String getPropertyNumber() {
		
		return this.location.getLocationNumber();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setPropertyNumber(java.lang.String)
	 */
	@Override
	public void setPropertyNumber(String number) {
		
		this.location.setLocationNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getPropertyType()
	 */
	@Override
	public <T extends AbstractPropertyType> T getPropertyType() {
		
		return this.location.getPropertyType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)
	 */
	@Override
	public <T extends AbstractPropertyType> void setPropertyType(T type) {
		
		this.location.setPropertyType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getBuildingNumber()
	 */
	@Override
	public String getBuildingNumber() {
		
		return this.location.getBuildingNumber();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setBuildingNumber(java.lang.String)
	 */
	@Override
	public void setBuildingNumber(String number) throws PropertyTypeNotMatchException {
		
		this.location.setBuildingNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getLevel()
	 */
	@Override
	public Integer getLevel() {
		
		return this.location.getLevel();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setLevel(java.lang.Integer)
	 */
	@Override
	public void setLevel(Integer level) throws PropertyTypeNotMatchException {
		
		this.location.setLevel(level);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getLevelType()
	 */
	@Override
	public LevelType getLevelType() {
		
		return this.location.getLevelType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)
	 */
	@Override
	public void setLevelType(LevelType type) throws PropertyTypeNotMatchException {
		
		this.location.setLevelType(type);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((boundaries == null) ? 0 : boundaries.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ResidentialAddress)) {
			return false;
		}
		ResidentialAddress other = (ResidentialAddress) obj;
		if (boundaries == null) {
			if (other.boundaries != null) {
				return false;
			}
		} else if (!boundaries.equals(other.boundaries)) {
			return false;
		}
		if (location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!location.equals(other.location)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public ResidentialAddress clone() throws CloneNotSupportedException {
		
		ResidentialAddress rtn = null;
		try {
			rtn = new ResidentialAddress(this);
		} catch (StateNotSetException | CityNotSetException e) {
			e.printStackTrace();
		}
		
		return rtn;
	}
}
