/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.street;

import java.util.HashSet;

import com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum;

/**
 * The type of street
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class StreetType extends AbstractAbbreviatableEnum {
	
	public final static StreetType Alley = new StreetType("Alley", "Ally");
 	public final static StreetType Arcade = new StreetType("Arcade","Arc");
 	public final static StreetType Avenue = new StreetType("Avenue", "Ave");
 	public final static StreetType Boulevard = new StreetType("Boulevard",	"Bvd").addOtherAbbreviation("BLVD");
 	public final static StreetType Bypass = new StreetType("Bypass", "Bypa");
 	public final static StreetType Circuit = new StreetType("Circuit", "Cct");
 	public final static StreetType Close = new StreetType("Close", "Cl");
 	public final static StreetType Corner = new StreetType("Corner", "Crn");
 	public final static StreetType Court = new StreetType("Court", "Ct").addOtherAbbreviation("Crt");
 	public final static StreetType Crescent = new StreetType("Crescent", "Cres");
 	public final static StreetType CulDeSac = new StreetType("Cul-de-sac",	"Cds");
 	public final static StreetType Drive = new StreetType("Drive", "Dr");
 	public final static StreetType Esplanade = new StreetType("Esplanade", "Esp");
 	public final static StreetType Green = new StreetType("Green", "Grn");
 	public final static StreetType Grove = new StreetType("Grove", "Gr");
 	public final static StreetType Highway = new StreetType("Highway", "Hwy");
 	public final static StreetType Junction = new StreetType("Junction", "Jnc");
 	public final static StreetType Lane = new StreetType("Lane", "Lane");
 	public final static StreetType Link = new StreetType("Link", "Link");
 	public final static StreetType Mews = new StreetType("Mews", "Mews");
 	public final static StreetType Parade = new StreetType("Parade", "Pde");
 	public final static StreetType Place = new StreetType("Place",	"Pl");
 	public final static StreetType Ridge = new StreetType("Ridge",	"Rdge");
 	public final static StreetType Road = new StreetType("Road", "Rd");
 	public final static StreetType Square = new StreetType("Square" ,"Sq");
 	public final static StreetType Street = new StreetType("Street", "St");
 	public final static StreetType Terrace = new StreetType("Terrace", "Tce");

	private final static HashSet<StreetType> all = new HashSet<StreetType>();
	
	/**
	 * Used to put the street type into the static list, otherwise there is nothing in the [all].
	 */
	static{
		all.add(Alley);
		all.add(Arcade);
		all.add(Avenue);
		all.add(Boulevard);
		all.add(Bypass);
		all.add(Circuit);
		all.add(Close);
		all.add(Corner);
		all.add(Court);
		all.add(Crescent);
		all.add(CulDeSac);
		all.add(Drive);
		all.add(Esplanade);
		all.add(Green);
		all.add(Grove);
		all.add(Highway);
		all.add(Junction);
		all.add(Lane);
		all.add(Link);
		all.add(Mews);
		all.add(Parade);
		all.add(Place);
		all.add(Ridge);
		all.add(Road);
		all.add(Square);
		all.add(Street);
		all.add(Terrace);
	}
			
	/**
	 * Constructs an object
	 * 
	 * @param name The name of street type
	 * @param abbreviation The abbreviation of street type
	 */
	private StreetType(String name, String abbreviation) {
		super(name, abbreviation);
	}
	
	/**
	 * Returns street type according to the name of street type
	 * 
	 * @param name The name of street type
	 * 
	 * @return The type of street if found, otherwise null.
	 */
	public static StreetType getTypeFromName(String name){
		StreetType rtn = null;
		for (StreetType streetType : all) {
			rtn = streetType.fromValue(name);
			break;
		}
		
		return rtn;		
	}
	
	/**
	 * Returns the street type according to the abbreviation of street type
	 * 
	 * @param abbreviation The abbreviation of street type
	 * 
	 * @return The type of street if found, otherwise null.
	 */
	public static StreetType getTypeFromAbbreviation(String abbreviation){
		StreetType rtn = null;
		for (StreetType streetType : all) {
			rtn = streetType.fromAbbreviation(abbreviation);
			break;
		}
		
		return rtn;		
	}
		
	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static StreetType[] values(){
		StreetType[] rtn = new StreetType[all.size()];
		
		rtn = all.toArray(rtn);
		
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected HashSet<StreetType> getValues() {

		return all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.AbstractAbbrevitableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected StreetType self() {

		return this;
	}

}
