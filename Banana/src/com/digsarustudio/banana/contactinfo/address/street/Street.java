/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.street;

import java.io.Serializable;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents a street object, it's including name, type and suffix of street
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class Street implements Serializable, Cloneable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 4892994968352459323L;

	/**
	 * The name of street
	 */
	private String name;
	
	/**
	 * The type of street
	 */
	private StreetType type;
	
	/**
	 * The suffix of street
	 */
	private String suffix;
	
	/**
	 * 
	 * A builder to build the street object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<Street>{
		private String name;		
		private StreetType type;
		private String suffix;
		
		/**
		 * Build a builder for Street
		 * 
		 * @param name the name of street
		 * @param type the type of street
		 */
		public Builder(String name, StreetType type){
			this.name = name;
			this.type = type;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public Street build() {
			
			return new Street(this);
		}

		/**
		 * Returns the name of street
		 * 
		 * @return the name of street
		 */
		public String getName() {
			return name;
		}

		/**
		 * Returns the type of street
		 * 
		 * @return the type of street
		 */
		public StreetType getType() {
			return type;
		}

		/**
		 * Returns the suffix
		 * 
		 * @return the suffix
		 */
		public String getSuffix() {
			return suffix;
		}

		/**
		 * Assign the suffix
		 * 
		 * @param suffix the suffix to set
		 */
		public Builder setSuffix(String suffix) {
			this.suffix = suffix;
			return this;
		}		
	}
	
	
	/**
	 * Constructs a street object
	 */
	private Street(Builder builder) {

		this.setName(builder.getName());
		this.setType(builder.getType());
		this.setSuffix(builder.getSuffix());
	}
	
	/**
	 * Copy the street object
	 * 
	 * @param source The source object for a copy
	 */
	public Street(Street source){
		this.setName(source.getName());
		this.setSuffix(source.getSuffix());
		this.setType(source.getType());
	}

	/**
	 * Returns the name of street
	 * 
	 * @return the name of street
	 */
	public String getName() {
		return name;
	}

	/**
	 * Assign the name to the street
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Returns the type of street
	 * 
	 * @return the type of street
	 */
	public StreetType getType() {
		return type;
	}

	/**
	 * Assign the type to the street
	 * 
	 * @param type the type to set
	 */
	public void setType(StreetType type) {
		this.type = type;
	}

	/**
	 * Returns the suffix of street
	 * 
	 * @return the suffix of street
	 */
	public String getSuffix() {
		return suffix;
	}

	/**
	 * Assign the suffix of street
	 * @param suffix the suffix to set
	 */
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((suffix == null) ? 0 : suffix.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof Street)) {
			return false;
		}
		Street other = (Street) obj;
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		if (suffix == null) {
			if (other.suffix != null) {
				return false;
			}
		} else if (!suffix.equals(other.suffix)) {
			return false;
		}
		if (type == null) {
			if (other.type != null) {
				return false;
			}
		} else if (!type.equals(other.type)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public Street clone() throws CloneNotSupportedException {
		
		return new Street(this);
	}
}
