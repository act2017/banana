/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries;
import com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;

/**
 * Defining the operation of residential address type
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface IResidentialAddress extends IPoliticalBoundaries {

	/**
	 * Returns the name of street
	 * 
	 * @return then name of street
	 */
	String getStreetName();
	
	/**
	 * Assign a name to the street
	 * 
	 * @param name The name for the street
	 */
	void setStreetName(String name);
	
	/**
	 * Returns the type of street
	 * 
	 * @return The type of street
	 */
	StreetType getStreetType();
	
	/**
	 * Assign a type to the street
	 * 
	 * @param type The type for the street
	 */
	void setStreetType(StreetType type);
	
	/**
	 * Returns the suffix of the street
	 * 
	 * @return The suffix of the street
	 */
	String getStreetSuffix();
	
	/**
	 * Assign a suffix to the street
	 * 
	 * @param suffix The suffix for the street
	 */
	void setStreetSuffix(String suffix);
	
	/**
	 * Returns the number of the property
	 * 
	 * @return The number of the property
	 */
	String getPropertyNumber();
	
	/**
	 * Assign a number to the property
	 * 
	 * @param number The number for the property
	 */
	void setPropertyNumber(String number);
	
	/**
	 * Returns the type of the property
	 * 
	 * @return The type of the property
	 */
	<T extends AbstractPropertyType> T getPropertyType();
	
	/**
	 * Assign a type to the property
	 * 
	 * @param type The type for the property
	 */
	<T extends AbstractPropertyType> void setPropertyType(T type);

	/**
	 * Returns the number of the building
	 * 
	 * @return The number of the building
	 */
	String getBuildingNumber();
	
	/**
	 * Assign a number to the building
	 * 
	 * @param number The number for the building
	 * @throws PropertyTypeNotMatchException The the property is not a building
	 */
	void setBuildingNumber(String number) throws PropertyTypeNotMatchException;
	
	/**
	 * Returns the level of building
	 * 
	 * @return The level of building
	 */
	Integer getLevel();
	
	/**
	 * Assign a level to the building
	 * 
	 * @param level The level for the building
	 * @throws PropertyTypeNotMatchException The the property is not a building
	 */
	void setLevel(Integer level) throws PropertyTypeNotMatchException;
	
	/**
	 * Returns the type of building
	 * 
	 * @return The type of building
	 */
	LevelType getLevelType();
	
	/**
	 * Assign a type to the building
	 * 
	 * @param type The type for the building
	 * @throws PropertyTypeNotMatchException The the property is not a building
	 */
	void setLevelType(LevelType type) throws PropertyTypeNotMatchException;
}
