/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents a suburb in a state or in a city.<br>
 * In the practice, the details of a suburb should be from the database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface Suburb {
	/**
	 * 
	 * The builder for {@link Suburb}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<Suburb> {
		Builder setName(String name);
		Builder setDisplayString(String display);
	}
	
	void setName(String name);
	void setDisplayString(String display);
	
	String getName();
	String getDisplayString();
}
