/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the administrative division as a City
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public final class City extends AdministrativeDivision{
	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -5037847171076950101L;

	/**
	 * To build city object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<City>{
		AdministrativeDivision.Builder builder;
		
		public <T extends IEnumAdministrativeDivision> Builder(T division){
			this.builder = new AdministrativeDivision.Builder(division);
		}
		
		/**
		 * Returns the division of city
		 * 
		 * @return the division of city
		 */
		public <T extends IEnumAdministrativeDivision> T getDivision() {
			return this.builder.getDivision();
		}

		/**
		 * Returns the subdivision of city
		 * 
		 * @return the subdivision of city
		 */
		public AdministrativeDivision getSubdivision() {
			return this.builder.getSubdivision();
		}

		/**
		 * Assign the subdivision of city
		 * 
		 * @param subdivision the constituent of city to set
		 * 
		 * @return the builder itself
		 */
		public Builder setSubDivision(AdministrativeDivision subdivision) {
			this.builder.setSubdivision(subdivision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public City build() {
			
			return new City(this);
		}				
	}
	
	/**
	 * Constructs an object
	 * 
	 * @param builder The builder creates village
	 */
	private City(Builder builder){
		this.setDivision(builder.getDivision());
		this.setSubdivision(builder.getSubdivision());
	}
	
	/**
	 * Constructs a city by copying
	 * 
	 * @param source The source of city object
	 * @throws CloneNotSupportedException When the subdivision cannot be cloned
	 */
	public City(City source) throws CloneNotSupportedException{
		this.setDivision(source.getDivision());
		this.setSubdivision(source.getSubdivision().clone());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#clone()
	 */
	@Override
	public City clone() throws CloneNotSupportedException {
		
		return new City(this);
	}
}
