/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

/**
 * Represent the exception of state not to set
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class StateNotSetException extends Exception {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -1207185839490893493L;

	/**
	 * 
	 */
	public StateNotSetException() {

	}

	/**
	 * @param message
	 */
	public StateNotSetException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public StateNotSetException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public StateNotSetException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public StateNotSetException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
