/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

/**
 * The definition of the enumeration states
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractEnumStates extends AbstractEnumAdministrativeDivisions {

	/**
	 * Constructs an enumerator represents the State
	 * 
	 * @param value The name of state
	 * @param abbreviation The abbreviation of the name of state
	 */
	public AbstractEnumStates(String value, String abbreviation) {
		super(value, abbreviation);
	}
}
