/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import java.util.HashSet;

import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * The definition of enumeration suburbs
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractEnumSuburbs extends AbstractEnumLocalGovernments {
	
	/**
	 * The post code of suburb
	 */
	private String postcode;
	
	/**
	 * Constructs an enumerator of suburb
	 * 
	 * @param value The name of suburb
	 * @param abbreviation The abbreviation of the name of suburb
	 */
	public AbstractEnumSuburbs(String value, String abbreviation) {
		super(value, abbreviation);
	}

	/**
	 * Returns the postcode of the suburb
	 * 
	 * @return the postcode of the suburb
	 */
	@Override
	public String getPostcode(){
		return this.postcode;
	}
	
	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	public <T extends IEnumeratable> T fromPostcode(String value) {

		if( null == value ){
			throw new NullPointerException("The value must be set.");
		}else if(value.length() <= 0){
			throw new IllegalArgumentException("The value of value must be set.");
		}
		

		T rtn = null;
		
		HashSet<T> values = this.getValues();
		for (T target : values) {
			AbstractEnumSuburbs enumerator = (AbstractEnumSuburbs)target;
			
			String postcode = enumerator.getPostcode();
			
			if(null == postcode)
				continue;
			
			if(!postcode.equalsIgnoreCase(value))
				continue;
			
			rtn = (T)enumerator;
			break;
		}
		
		return rtn;
	}	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((postcode == null) ? 0 : postcode.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof AbstractEnumSuburbs)) {
			return false;
		}
		AbstractEnumSuburbs other = (AbstractEnumSuburbs) obj;
		if (postcode == null) {
			if (other.postcode != null) {
				return false;
			}
		} else if (!postcode.equals(other.postcode)) {
			return false;
		}
		return true;
	}

	/**
	 * Assign a post code
	 * 
	 * @param postcode the postcode of suburb
	 * 
	 * @return the postcode of suburb
	 */
	@SuppressWarnings("unchecked")
	protected <T extends AbstractEnumSuburbs> T setPostcode(String postcode){
		this.postcode = postcode;
		return (T)this;
	}
}
