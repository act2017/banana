/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia;

import java.io.Serializable;
import java.util.HashSet;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.AbstractEnumSuburbs;
import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * The suburbs in the City of Brisbane
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class BrisbaneSuburbs extends AbstractEnumSuburbs implements Serializable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -2890001037702996244L;
	
	//North side
	public final static BrisbaneSuburbs Albion			= new BrisbaneSuburbs("Albion", "ALBION").setRegion("North side");
	public final static BrisbaneSuburbs Alderley		= new BrisbaneSuburbs("Alderley", "ALDERLEY").setRegion("North side");
	public final static BrisbaneSuburbs Ascot			= new BrisbaneSuburbs("Ascot", "ASCOT").setRegion("North side");
	public final static BrisbaneSuburbs Aspley			= new BrisbaneSuburbs("Aspley", "ASPLEY").setRegion("North side");
	public final static BrisbaneSuburbs BaldHills		= new BrisbaneSuburbs("Bald Hills", "BALD HILLS").setRegion("North side");
	public final static BrisbaneSuburbs Banyo			= new BrisbaneSuburbs("Banyo", "BANYO").setRegion("North side");
	public final static BrisbaneSuburbs Boondall		= new BrisbaneSuburbs("Boondall", "BOONDALL").setRegion("North side");
	public final static BrisbaneSuburbs BowenHills		= new BrisbaneSuburbs("Bowen Hills", "BOWEN HILLS").setRegion("North side");
	public final static BrisbaneSuburbs BrackenRidge	= new BrisbaneSuburbs("Bracken Ridge", "BRACKEN RIDGE").setRegion("North side");
	public final static BrisbaneSuburbs BridgemanDowns	= new BrisbaneSuburbs("Bridgeman Downs", "BRIDGEMAN DOWNS").setRegion("North side");
	public final static BrisbaneSuburbs Brighton		= new BrisbaneSuburbs("Brighton", "BRIGHTON").setRegion("North side");
	public final static BrisbaneSuburbs BrisbaneAirport = new BrisbaneSuburbs("Brisbane Airport", "BRISBANE AIRPORT").setRegion("North side");
	public final static BrisbaneSuburbs BrisbaneCBD		= ((BrisbaneSuburbs)(new BrisbaneSuburbs("Brisbane CBD", "BRISBANE CBD").setRegion("North side"))).setPostcode("4000");
	public final static BrisbaneSuburbs Carseldine		= new BrisbaneSuburbs("Carseldine", "CARSELDINE").setRegion("North side");
	public final static BrisbaneSuburbs Chermside		= new BrisbaneSuburbs("Chermside", "CHERMSIDE").setRegion("North side");
	public final static BrisbaneSuburbs ChermsideWest	= new BrisbaneSuburbs("Chermside West", "CHERMSIDE WEST").setRegion("North side");
	public final static BrisbaneSuburbs Clayfield		= new BrisbaneSuburbs("Clayfield", "CLAYFIELD").setRegion("North side");
	public final static BrisbaneSuburbs Deagon			= new BrisbaneSuburbs("Deagon", "DEAGON").setRegion("North side");
	public final static BrisbaneSuburbs EagleFarm		= new BrisbaneSuburbs("Eagle Farm", "EAGLE FARM").setRegion("North side");
	public final static BrisbaneSuburbs EnglandCreek	= new BrisbaneSuburbs("England Creek", "ENGLAND CREEK").setRegion("North side");
	public final static BrisbaneSuburbs Enoggera		= new BrisbaneSuburbs("Enoggera", "ENOGGERA").setRegion("North side");
	public final static BrisbaneSuburbs EvertonPark		= new BrisbaneSuburbs("Everton Park", "EVERTON PARK").setRegion("North side");
	public final static BrisbaneSuburbs FernyGrove		= new BrisbaneSuburbs("Ferny Grove", "FERNY GROVE").setRegion("North side");
	public final static BrisbaneSuburbs Fitzgibbon		= new BrisbaneSuburbs("Fitzgibbon", "FITZGIBBON").setRegion("North side");
	public final static BrisbaneSuburbs FortitudeValley = new BrisbaneSuburbs("Fortitude Valley", "FORTITUDE VALLEY").setRegion("North side");
	public final static BrisbaneSuburbs Gaythorne		= new BrisbaneSuburbs("Gaythorne", "GAYTHORNE").setRegion("North side");
	public final static BrisbaneSuburbs Geebung			= new BrisbaneSuburbs("Geebung", "GEEBUNG").setRegion("North side");
	public final static BrisbaneSuburbs GordonPark		= new BrisbaneSuburbs("Gordon Park", "GORDON PARK").setRegion("North side");
	public final static BrisbaneSuburbs Grange			= new BrisbaneSuburbs("Grange", "GRANGE").setRegion("North side");
	public final static BrisbaneSuburbs Hamilton		= new BrisbaneSuburbs("Hamilton", "HAMILTON").setRegion("North side");
	public final static BrisbaneSuburbs Hendra			= new BrisbaneSuburbs("Hendra", "HENDRA").setRegion("North side");
	public final static BrisbaneSuburbs Herston			= new BrisbaneSuburbs("Herston", "HERSTON").setRegion("North side");
	public final static BrisbaneSuburbs Kedron			= new BrisbaneSuburbs("Kedron", "KEDRON").setRegion("North side");
	public final static BrisbaneSuburbs KelvinGrove		= new BrisbaneSuburbs("Kelvin Grove", "KELVIN GROVE").setRegion("North side");
	public final static BrisbaneSuburbs Keperra			= new BrisbaneSuburbs("Keperra", "KEPERRA").setRegion("North side");
	public final static BrisbaneSuburbs Lutwyche		= new BrisbaneSuburbs("Lutwyche", "LUTWYCHE").setRegion("North side");
	public final static BrisbaneSuburbs McDowall		= new BrisbaneSuburbs("McDowall", "MCDOWALL").setRegion("North side");
	public final static BrisbaneSuburbs Mitchelton		= new BrisbaneSuburbs("Mitchelton", "MITCHELTON").setRegion("North side");
	public final static BrisbaneSuburbs NewFarm			= new BrisbaneSuburbs("New Farm", "NEW FARM").setRegion("North side");
	public final static BrisbaneSuburbs Newmarket		= new BrisbaneSuburbs("Newmarket", "NEWMARKET").setRegion("North side");
	public final static BrisbaneSuburbs Newstead		= new BrisbaneSuburbs("Newstead", "NEWSTEAD").setRegion("North side");
	public final static BrisbaneSuburbs Northgate		= new BrisbaneSuburbs("Northgate", "NORTHGATE").setRegion("North side");
	public final static BrisbaneSuburbs Nudgee			= new BrisbaneSuburbs("Nudgee", "NUDGEE").setRegion("North side");
	public final static BrisbaneSuburbs NudgeeBeach		= new BrisbaneSuburbs("Nudgee Beach", "NUDGEE BEACH").setRegion("North side");
	public final static BrisbaneSuburbs Nundah			= new BrisbaneSuburbs("Nundah", "NUNDAH").setRegion("North side");
	public final static BrisbaneSuburbs Pinkenba		= new BrisbaneSuburbs("Pinkenba", "PINKENBA").setRegion("North side");
	public final static BrisbaneSuburbs Sandgate		= new BrisbaneSuburbs("Sandgate", "SANDGATE").setRegion("North side");
	public final static BrisbaneSuburbs Shorncliffe		= new BrisbaneSuburbs("Shorncliffe", "SHORNCLIFFE").setRegion("North side");
	public final static BrisbaneSuburbs SpringHill		= new BrisbaneSuburbs("Spring Hill", "SPRING HILL").setRegion("North side");
	public final static BrisbaneSuburbs Stafford		= new BrisbaneSuburbs("Stafford", "STAFFORD").setRegion("North side");
	public final static BrisbaneSuburbs StaffordHeights = new BrisbaneSuburbs("Stafford Heights", "STAFFORD HEIGHTS").setRegion("North side");
	public final static BrisbaneSuburbs Taigum			= new BrisbaneSuburbs("Taigum", "TAIGUM").setRegion("North side");
	public final static BrisbaneSuburbs Teneriffe		= new BrisbaneSuburbs("Teneriffe", "TENERIFFE").setRegion("North side");
	public final static BrisbaneSuburbs UpperKedron		= new BrisbaneSuburbs("Upper Kedron", "UPPER KEDRON").setRegion("North side");
	public final static BrisbaneSuburbs Virginia		= new BrisbaneSuburbs("Virginia", "VIRGINIA").setRegion("North side");
	public final static BrisbaneSuburbs WavellHeights	= new BrisbaneSuburbs("Wavell Heights", "WAVELL HEIGHTS").setRegion("North side");
	public final static BrisbaneSuburbs Wilston			= new BrisbaneSuburbs("Wilston", "WILSTON").setRegion("North side");
	public final static BrisbaneSuburbs Windsor			= new BrisbaneSuburbs("Windsor", "WINDSOR").setRegion("North side");
	public final static BrisbaneSuburbs Wooloowin		= new BrisbaneSuburbs("Wooloowin", "WOOLOOWIN").setRegion("North side");
	public final static BrisbaneSuburbs Zillmere		= new BrisbaneSuburbs("Zillmere", "ZILLMERE").setRegion("North side");
	
	//West side
	public final static BrisbaneSuburbs Anstead			= new BrisbaneSuburbs("Anstead", "ANSTEAD").setRegion("West side");
	public final static BrisbaneSuburbs Ashgrove		= new BrisbaneSuburbs("Ashgrove", "ASHGROVE").setRegion("West side");
	public final static BrisbaneSuburbs Auchenflower	= new BrisbaneSuburbs("Auchenflower", "AUCHENFLOWER").setRegion("West side");
	public final static BrisbaneSuburbs BanksCreek		= new BrisbaneSuburbs("Banks Creek", "BANKS CREEK").setRegion("West side");
	public final static BrisbaneSuburbs Bardon			= new BrisbaneSuburbs("Bardon", "BARDON").setRegion("West side");
	public final static BrisbaneSuburbs Bellbowrie		= new BrisbaneSuburbs("Bellbowrie", "BELLBOWRIE").setRegion("West side");
	public final static BrisbaneSuburbs Brookfield		= new BrisbaneSuburbs("Brookfield", "BROOKFIELD").setRegion("West side");
	public final static BrisbaneSuburbs CarolePark 		= new BrisbaneSuburbs("Carole Park", "CAROLE PARK").setRegion("West side");
	public final static BrisbaneSuburbs ChapelHill 		= new BrisbaneSuburbs("Chapel Hill", "CHAPEL HILL").setRegion("West side");
	public final static BrisbaneSuburbs Chelmer 		= new BrisbaneSuburbs("Chelmer", "CHELMER").setRegion("West side");
	public final static BrisbaneSuburbs Chuwar 			= new BrisbaneSuburbs("Chuwar", "CHUWAR").setRegion("West side");
	public final static BrisbaneSuburbs Corinda 		= new BrisbaneSuburbs("Corinda", "CORINDA").setRegion("West side");
	public final static BrisbaneSuburbs Darra 			= new BrisbaneSuburbs("Darra", "DARRA").setRegion("West side");
	public final static BrisbaneSuburbs Doolandella 	= new BrisbaneSuburbs("Doolandella", "DOOLANDELLA").setRegion("West side");
	public final static BrisbaneSuburbs Durack 			= new BrisbaneSuburbs("Durack", "DURACK").setRegion("West side");
	public final static BrisbaneSuburbs EllenGrove 		= new BrisbaneSuburbs("Ellen Grove", "ELLEN GROVE").setRegion("West side");
	public final static BrisbaneSuburbs EnoggeraReservoir = new BrisbaneSuburbs("Enoggera Reservoir", "ENOGGERA RESERVOIR").setRegion("West side");
	public final static BrisbaneSuburbs FigTreePocket	 = new BrisbaneSuburbs("Fig Tree Pocket", "FIG TREE POCKET").setRegion("West side");
	public final static BrisbaneSuburbs ForestLake 		= new BrisbaneSuburbs("Forest Lake", "FOREST LAKE").setRegion("West side");
	public final static BrisbaneSuburbs Graceville 		= new BrisbaneSuburbs("Graceville", "GRACEVILLE").setRegion("West side");
	public final static BrisbaneSuburbs Heathwood 		= new BrisbaneSuburbs("Heathwood", "HEATHWOOD").setRegion("West side");
	public final static BrisbaneSuburbs Inala 			= new BrisbaneSuburbs("Inala", "INALA").setRegion("West side");
	public final static BrisbaneSuburbs Indooroopilly 	= new BrisbaneSuburbs("Indooroopilly", "INDOOROOPILLY").setRegion("West side");
	public final static BrisbaneSuburbs JamboreeHeights = new BrisbaneSuburbs("Jamboree Heights", "JAMBOREE HEIGHTS").setRegion("West side");
	public final static BrisbaneSuburbs Jindalee 		= new BrisbaneSuburbs("Jindalee", "JINDALEE").setRegion("West side");
	public final static BrisbaneSuburbs KaranaDowns 	= new BrisbaneSuburbs("Karana Downs", "KARANA DOWNS").setRegion("West side");
	public final static BrisbaneSuburbs Kenmore 		= new BrisbaneSuburbs("Kenmore", "KENMORE").setRegion("West side");
	public final static BrisbaneSuburbs KenmoreHills 	= new BrisbaneSuburbs("Kenmore Hills", "KENMORE HILLS").setRegion("West side");
	public final static BrisbaneSuburbs Kholo 			= new BrisbaneSuburbs("Kholo", "KHOLO").setRegion("West side");
	public final static BrisbaneSuburbs LakeManchester 	= new BrisbaneSuburbs("Lake Manchester", "LAKE MANCHESTER").setRegion("West side");
	public final static BrisbaneSuburbs MiddlePark 		= new BrisbaneSuburbs("Middle Park", "MIDDLE PARK").setRegion("West side");
	public final static BrisbaneSuburbs Milton 			= new BrisbaneSuburbs("Milton", "MILTON").setRegion("West side");
	public final static BrisbaneSuburbs Moggill 		= new BrisbaneSuburbs("Moggill", "MOGGILL").setRegion("West side");
	public final static BrisbaneSuburbs MountCoottha 	= new BrisbaneSuburbs("Mount Coot-tha", "MOUNT COOT-THA").setRegion("West side");
	public final static BrisbaneSuburbs MountCrosby 	= new BrisbaneSuburbs("Mount Crosby", "MOUNT CROSBY").setRegion("West side");
	public final static BrisbaneSuburbs MountOmmaney 	= new BrisbaneSuburbs("Mount Ommaney", "MOUNT OMMANEY").setRegion("West side");
	public final static BrisbaneSuburbs Oxley 			= new BrisbaneSuburbs("Oxley", "OXLEY").setRegion("West side");
	public final static BrisbaneSuburbs Pallara 		= new BrisbaneSuburbs("Pallara", "PALLARA").setRegion("West side");
	public final static BrisbaneSuburbs Paddington 		= new BrisbaneSuburbs("Paddington", "PADDINGTON").setRegion("West side");
	public final static BrisbaneSuburbs PetrieTerrace 	= new BrisbaneSuburbs("Petrie Terrace", "PETRIE TERRACE").setRegion("West side");
	public final static BrisbaneSuburbs PinjarraHills 	= new BrisbaneSuburbs("Pinjarra Hills", "PINJARRA HILLS").setRegion("West side");
	public final static BrisbaneSuburbs Pullenvale 		= new BrisbaneSuburbs("Pullenvale", "PULLENVALE").setRegion("West side");
	public final static BrisbaneSuburbs RedHill 		= new BrisbaneSuburbs("Red Hill", "RED HILL").setRegion("West side");
	public final static BrisbaneSuburbs Richlands 		= new BrisbaneSuburbs("Richlands", "RICHLANDS").setRegion("West side");
	public final static BrisbaneSuburbs Riverhills 		= new BrisbaneSuburbs("Riverhills", "RIVERHILLS").setRegion("West side");
	public final static BrisbaneSuburbs StLucia 		= new BrisbaneSuburbs("St Lucia", "ST LUCIA").setRegion("West side");
	public final static BrisbaneSuburbs SeventeenMileRocks = new BrisbaneSuburbs("Seventeen Mile Rocks", "SEVENTEEN MILE ROCKS").setRegion("West side");
	public final static BrisbaneSuburbs Sherwood 		= new BrisbaneSuburbs("Sherwood", "SHERWOOD").setRegion("West side");
	public final static BrisbaneSuburbs SinnamonPark 	= new BrisbaneSuburbs("Sinnamon Park", "SINNAMON PARK").setRegion("West side");
	public final static BrisbaneSuburbs Sumner 			= new BrisbaneSuburbs("Sumner", "SUMNER").setRegion("West side");
	public final static BrisbaneSuburbs Taringa 		= new BrisbaneSuburbs("Taringa", "TARINGA").setRegion("West side");
	public final static BrisbaneSuburbs TheGap			= new BrisbaneSuburbs("The Gap", "THE GAP").setRegion("West side");
	public final static BrisbaneSuburbs Toowong 		= new BrisbaneSuburbs("Toowong", "TOOWONG").setRegion("West side");
	public final static BrisbaneSuburbs UpperBrookfield = new BrisbaneSuburbs("Upper Brookfield", "UPPER BROOKFIELD").setRegion("West side");
	public final static BrisbaneSuburbs Wacol 			= new BrisbaneSuburbs("Wacol", "WACOL").setRegion("West side");
	public final static BrisbaneSuburbs Westlake 		= new BrisbaneSuburbs("Westlake", "WESTLAKE").setRegion("West side");
	public final static BrisbaneSuburbs Willawong 		= new BrisbaneSuburbs("Willawong", "WILLAWONG").setRegion("West side");
	
	//South side
	public final static BrisbaneSuburbs AcaciaRidge 	= new BrisbaneSuburbs("Acacia Ridge", "ACACIA RIDGE").setRegion("South side");
	public final static BrisbaneSuburbs Algester 		= new BrisbaneSuburbs("Algester", "ALGESTER").setRegion("South side");
	public final static BrisbaneSuburbs Annerley 		= new BrisbaneSuburbs("Annerley", "ANNERLEY").setRegion("South side");
	public final static BrisbaneSuburbs Archerfield 	= new BrisbaneSuburbs("Archerfield", "ARCHERFIELD").setRegion("South side");
	public final static BrisbaneSuburbs Berrinba 		= new BrisbaneSuburbs("Berrinba", "BERRINBA").setRegion("South side");
	public final static BrisbaneSuburbs Burbank 		= new BrisbaneSuburbs("Burbank", "BURBANK").setRegion("South side");
	public final static BrisbaneSuburbs Calamvale 		= new BrisbaneSuburbs("Calamvale", "CALAMVALE").setRegion("South side");
	public final static BrisbaneSuburbs CoopersPlains 	= new BrisbaneSuburbs("Coopers Plains", "COOPERS PLAINS").setRegion("South side");
	public final static BrisbaneSuburbs Drewvale 		= new BrisbaneSuburbs("Drewvale", "DREWVALE").setRegion("South side");
	public final static BrisbaneSuburbs DuttonPark 		= new BrisbaneSuburbs("Dutton Park", "DUTTON PARK").setRegion("South side");
	public final static BrisbaneSuburbs EightMilePlains = new BrisbaneSuburbs("Eight Mile Plains", "EIGHT MILE PLAINS").setRegion("South side");
	public final static BrisbaneSuburbs Fairfield 		= new BrisbaneSuburbs("Fairfield", "FAIRFIELD").setRegion("South side");
	public final static BrisbaneSuburbs Greenslopes 	= new BrisbaneSuburbs("Greenslopes", "GREENSLOPES").setRegion("South side");
	public final static BrisbaneSuburbs HighgateHill 	= new BrisbaneSuburbs("Highgate Hill", "HIGHGATE HILL").setRegion("South side");
	public final static BrisbaneSuburbs HollandPark 	= new BrisbaneSuburbs("Holland Park", "HOLLAND PARK").setRegion("South side");
	public final static BrisbaneSuburbs HollandParkWest = new BrisbaneSuburbs("Holland Park West", "HOLLAND PARK WEST").setRegion("South side");
	public final static BrisbaneSuburbs Karawatha 		= new BrisbaneSuburbs("Karawatha", "KARAWATHA").setRegion("South side");
	public final static BrisbaneSuburbs Kuraby 			= new BrisbaneSuburbs("Kuraby", "KURABY").setRegion("South side");
	public final static BrisbaneSuburbs Larapinta 		= new BrisbaneSuburbs("Larapinta", "LARAPINTA").setRegion("South side");
	public final static BrisbaneSuburbs Macgregor	 	= new BrisbaneSuburbs("Macgregor", "MACGREGOR").setRegion("South side");
	public final static BrisbaneSuburbs Mackenzie 		= new BrisbaneSuburbs("Mackenzie", "MACKENZIE").setRegion("South side");
	public final static BrisbaneSuburbs Mansfield 		= new BrisbaneSuburbs("Mansfield", "MANSFIELD").setRegion("South side");
	public final static BrisbaneSuburbs Moorooka 		= new BrisbaneSuburbs("Moorooka", "MOOROOKA").setRegion("South side");
	public final static BrisbaneSuburbs MountGravatt 	= new BrisbaneSuburbs("Mount Gravatt", "MOUNT GRAVATT").setRegion("South side");
	public final static BrisbaneSuburbs MountGravattEast = new BrisbaneSuburbs("Mount Gravatt East", "MOUNT GRAVATT EAST").setRegion("South side");
	public final static BrisbaneSuburbs Nathan 			= new BrisbaneSuburbs("Nathan", "NATHAN").setRegion("South side");
	public final static BrisbaneSuburbs Parkinson 		= new BrisbaneSuburbs("Parkinson", "PARKINSON").setRegion("South side");
	public final static BrisbaneSuburbs Robertson 		= new BrisbaneSuburbs("Robertson", "ROBERTSON").setRegion("South side");
	public final static BrisbaneSuburbs Rochedale 		= new BrisbaneSuburbs("Rochedale", "ROCHEDALE").setRegion("South side");
	public final static BrisbaneSuburbs Rocklea 		= new BrisbaneSuburbs("Rocklea", "ROCKLEA").setRegion("South side");
	public final static BrisbaneSuburbs Runcorn 		= new BrisbaneSuburbs("Runcorn", "RUNCORN").setRegion("South side");
	public final static BrisbaneSuburbs Salisbury 		= new BrisbaneSuburbs("Salisbury", "SALISBURY").setRegion("South side");
	public final static BrisbaneSuburbs SouthBrisbane 	= new BrisbaneSuburbs("South Brisbane", "SOUTH BRISBANE").setRegion("South side");
	public final static BrisbaneSuburbs Stretton 		= new BrisbaneSuburbs("Stretton", "STRETTON").setRegion("South side");
	public final static BrisbaneSuburbs Sunnybank 		= new BrisbaneSuburbs("Sunnybank", "SUNNYBANK").setRegion("South side");
	public final static BrisbaneSuburbs SunnybankHills 	= new BrisbaneSuburbs("Sunnybank Hills", "SUNNYBANK HILLS").setRegion("South side");
	public final static BrisbaneSuburbs Tarragindi 		= new BrisbaneSuburbs("Tarragindi", "TARRAGINDI").setRegion("South side");
	public final static BrisbaneSuburbs Tennyson 		= new BrisbaneSuburbs("Tennyson", "TENNYSON").setRegion("South side");
	public final static BrisbaneSuburbs UpperMountGravatt = new BrisbaneSuburbs("Upper Mount Gravatt", "UPPER MOUNT GRAVATT").setRegion("South side");
	public final static BrisbaneSuburbs WestEnd 		= new BrisbaneSuburbs("West End", "WEST END").setRegion("South side");
	public final static BrisbaneSuburbs Wishart 		= new BrisbaneSuburbs("Wishart", "WISHART").setRegion("South side");
	public final static BrisbaneSuburbs Woolloongabba 	= new BrisbaneSuburbs("Woolloongabba", "WOOLLOONGABBA").setRegion("South side");
	public final static BrisbaneSuburbs Yeerongpilly 	= new BrisbaneSuburbs("Yeerongpilly", "YEERONGPILLY").setRegion("South side");
	public final static BrisbaneSuburbs Yeronga 		= new BrisbaneSuburbs("Yeronga", "YERONGA").setRegion("South side");
	
	//East side
	public final static BrisbaneSuburbs Balmoral		= new BrisbaneSuburbs("Balmoral", "BALMORAL").setRegion("East side");
	public final static BrisbaneSuburbs Belmont			= new BrisbaneSuburbs("Belmont", "BELMONT").setRegion("East side");
	public final static BrisbaneSuburbs Bulimba			= new BrisbaneSuburbs("Bulimba", "BULIMBA").setRegion("East side");
	public final static BrisbaneSuburbs CampHill		= new BrisbaneSuburbs("Camp Hill", "CAMP HILL").setRegion("East side");
	public final static BrisbaneSuburbs CannonHill		= new BrisbaneSuburbs("Cannon Hill", "CANNON HILL").setRegion("East side");
	public final static BrisbaneSuburbs Carina			= new BrisbaneSuburbs("Carina", "CARINA").setRegion("East side");
	public final static BrisbaneSuburbs CarinaHeights	= new BrisbaneSuburbs("Carina Heights", "CARINA HEIGHTS").setRegion("East side");
	public final static BrisbaneSuburbs Carindale		= new BrisbaneSuburbs("Carindale", "CARINDALE").setRegion("East side");
	public final static BrisbaneSuburbs Chandler		= new BrisbaneSuburbs("Chandler", "CHANDLER").setRegion("East side");
	public final static BrisbaneSuburbs Coorparoo		= new BrisbaneSuburbs("Coorparoo", "COORPAROO").setRegion("East side");
	public final static BrisbaneSuburbs EastBrisbane	= new BrisbaneSuburbs("East Brisbane", "EAST BRISBANE").setRegion("East side");
	public final static BrisbaneSuburbs Gumdale			= new BrisbaneSuburbs("Gumdale", "GUMDALE").setRegion("East side");
	public final static BrisbaneSuburbs Hawthorne 		= new BrisbaneSuburbs("Hawthorne", "HAWTHORNE").setRegion("East side");
	public final static BrisbaneSuburbs Hemmant 		= new BrisbaneSuburbs("Hemmant", "HEMMANT").setRegion("East side");
	public final static BrisbaneSuburbs KangarooPoint 	= new BrisbaneSuburbs("Kangaroo Point", "KANGAROO POINT").setRegion("East side");
	public final static BrisbaneSuburbs Lota 			= new BrisbaneSuburbs("Lota", "LOTA").setRegion("East side");
	public final static BrisbaneSuburbs Lytton 			= new BrisbaneSuburbs("Lytton", "LYTTON").setRegion("East side");
	public final static BrisbaneSuburbs Manly 			= new BrisbaneSuburbs("Manly", "MANLY").setRegion("East side");
	public final static BrisbaneSuburbs ManlyWest 		= new BrisbaneSuburbs("Manly West", "MANLY WEST").setRegion("East side");
	public final static BrisbaneSuburbs Morningside 	= new BrisbaneSuburbs("Morningside", "MORNINGSIDE").setRegion("East side");
	public final static BrisbaneSuburbs Murarrie 		= new BrisbaneSuburbs("Murarrie", "MURARRIE").setRegion("East side");
	public final static BrisbaneSuburbs NormanPark 		= new BrisbaneSuburbs("Norman Park", "NORMAN PARK").setRegion("East side");
	public final static BrisbaneSuburbs PortofBrisbane 	= new BrisbaneSuburbs("Port of Brisbane", "PORT OF BRISBANE").setRegion("East side");
	public final static BrisbaneSuburbs Ransome 		= new BrisbaneSuburbs("Ransome", "RANSOME").setRegion("East side");
	public final static BrisbaneSuburbs SevenHills 		= new BrisbaneSuburbs("Seven Hills", "SEVEN HILLS").setRegion("East side");
	public final static BrisbaneSuburbs Tingalpa 		= new BrisbaneSuburbs("Tingalpa", "TINGALPA").setRegion("East side");
	public final static BrisbaneSuburbs Wakerley 		= new BrisbaneSuburbs("Wakerley", "WAKERLEY").setRegion("East side");
	public final static BrisbaneSuburbs Wynnum 			= new BrisbaneSuburbs("Wynnum", "WYNNUM").setRegion("East side");
	public final static BrisbaneSuburbs WynnumWest 		= new BrisbaneSuburbs("Wynnum West", "WYNNUM WEST").setRegion("East side");
	
//Moreton Bay
	public final static BrisbaneSuburbs Bulwer			= new BrisbaneSuburbs("Bulwer", "BULWER").setRegion("Moreton Bay");
	public final static BrisbaneSuburbs CowanCowan		= new BrisbaneSuburbs("Cowan Cowan", "COWAN COWAN").setRegion("Moreton Bay");
	public final static BrisbaneSuburbs Kooringal		= new BrisbaneSuburbs("Kooringal", "KOORINGAL").setRegion("Moreton Bay");
	public final static BrisbaneSuburbs MoretonIsland	= new BrisbaneSuburbs("Moreton Island", "MORETON ISLAND").setRegion("Moreton Bay");
	
	private static HashSet<BrisbaneSuburbs> all = new HashSet<>();

	/**
	 * Which city this suburb belongs to.
	 */
	public final static AustralianStates belongs2 = AustralianStates.Queensland;
	
	static {
		//North side
		all.add(Albion);
		all.add(Alderley);
		all.add(Ascot);
		all.add(Aspley);
		all.add(BaldHills);
		all.add(Banyo);
		all.add(Boondall);
		all.add(BowenHills);
		all.add(BrackenRidge);
		all.add(BridgemanDowns);
		all.add(Brighton);
		all.add(BrisbaneAirport);
		all.add(BrisbaneCBD);
		all.add(Carseldine);
		all.add(Chermside);
		all.add(ChermsideWest);
		all.add(Clayfield);
		all.add(Deagon);
		all.add(EagleFarm);
		all.add(EnglandCreek);
		all.add(Enoggera);
		all.add(EvertonPark);
		all.add(FernyGrove);
		all.add(Fitzgibbon);
		all.add(FortitudeValley);
		all.add(Gaythorne);
		all.add(Geebung);
		all.add(GordonPark);
		all.add(Grange);
		all.add(Hamilton);
		all.add(Hendra);
		all.add(Herston);
		all.add(Kedron);
		all.add(KelvinGrove);
		all.add(Keperra);
		all.add(Lutwyche);
		all.add(McDowall);
		all.add(Mitchelton);
		all.add(NewFarm);
		all.add(Newmarket);
		all.add(Newstead);
		all.add(Northgate);
		all.add(Nudgee);
		all.add(NudgeeBeach);
		all.add(Nundah);
		all.add(Pinkenba);
		all.add(Sandgate);
		all.add(Shorncliffe);
		all.add(SpringHill);
		all.add(Stafford);
		all.add(StaffordHeights);
		all.add(Taigum);
		all.add(Teneriffe);
		all.add(UpperKedron);
		all.add(Virginia);
		all.add(WavellHeights);
		all.add(Wilston);
		all.add(Windsor);
		all.add(Wooloowin);
		all.add(Zillmere);
		
	//West side
		all.add(Anstead);
		all.add(Ashgrove);
		all.add(Auchenflower);
		all.add(BanksCreek);
		all.add(Bardon);
		all.add(Bellbowrie);
		all.add(Brookfield);
		all.add(CarolePark);
		all.add(ChapelHill);
		all.add(Chelmer);
		all.add(Chuwar);
		all.add(Corinda);
		all.add(Darra);
		all.add(Doolandella);
		all.add(Durack);
		all.add(EllenGrove);
		all.add(EnoggeraReservoir);
		all.add(FigTreePocket);
		all.add(ForestLake);
		all.add(Graceville);
		all.add(Heathwood);
		all.add(Inala);
		all.add(Indooroopilly);
		all.add(JamboreeHeights);
		all.add(Jindalee);
		all.add(KaranaDowns);
		all.add(Kenmore);
		all.add(KenmoreHills);
		all.add(Kholo);
		all.add(LakeManchester);
		all.add(MiddlePark);
		all.add(Milton);
		all.add(Moggill);
		all.add(MountCoottha);
		all.add(MountCrosby);
		all.add(MountOmmaney);
		all.add(Oxley);
		all.add(Pallara);
		all.add(Paddington);
		all.add(PetrieTerrace);
		all.add(PinjarraHills);
		all.add(Pullenvale);
		all.add(RedHill);
		all.add(Richlands);
		all.add(Riverhills);
		all.add(StLucia);
		all.add(SeventeenMileRocks);
		all.add(Sherwood);
		all.add(SinnamonPark);
		all.add(Sumner);
		all.add(Taringa);
		all.add(TheGap);
		all.add(Toowong);
		all.add(UpperBrookfield);
		all.add(Wacol);
		all.add(Westlake);
		all.add(Willawong);
		
		//South side
		all.add(AcaciaRidge);
		all.add(Algester);
		all.add(Annerley);
		all.add(Archerfield);
		all.add(Berrinba);
		all.add(Burbank);
		all.add(Calamvale);
		all.add(CoopersPlains);
		all.add(Drewvale);
		all.add(DuttonPark);
		all.add(EightMilePlains);
		all.add(Fairfield);
		all.add(Greenslopes);
		all.add(HighgateHill);
		all.add(HollandPark);
		all.add(HollandParkWest);
		all.add(Karawatha);
		all.add(Kuraby);
		all.add(Larapinta);
		all.add(Macgregor);
		all.add(Mackenzie);
		all.add(Mansfield);
		all.add(Moorooka);
		all.add(MountGravatt);
		all.add(MountGravattEast);
		all.add(Nathan);
		all.add(Parkinson);
		all.add(Robertson);
		all.add(Rochedale);
		all.add(Rocklea);
		all.add(Runcorn);
		all.add(Salisbury);
		all.add(SouthBrisbane);
		all.add(Stretton);
		all.add(Sunnybank);
		all.add(SunnybankHills);
		all.add(Tarragindi);
		all.add(Tennyson);
		all.add(UpperMountGravatt);
		all.add(WestEnd);
		all.add(Wishart);
		all.add(Woolloongabba);
		all.add(Yeerongpilly);
		all.add(Yeronga);
		
		
	//East side
		all.add(Balmoral);
		all.add(Belmont);
		all.add(Bulimba);
		all.add(CampHill);
		all.add(CannonHill);
		all.add(Carina);
		all.add(CarinaHeights);
		all.add(Carindale);
		all.add(Chandler);
		all.add(Coorparoo);
		all.add(EastBrisbane);
		all.add(Gumdale);
		all.add(Hawthorne);
		all.add(Hemmant);
		all.add(KangarooPoint);
		all.add(Lota);
		all.add(Lytton);
		all.add(Manly);
		all.add(ManlyWest);
		all.add(Morningside);
		all.add(Murarrie);
		all.add(NormanPark);
		all.add(PortofBrisbane);
		all.add(Ransome);
		all.add(SevenHills);
		all.add(Tingalpa);
		all.add(Wakerley);
		all.add(Wynnum);
		all.add(WynnumWest);
		
	//Moreton Bay
		all.add(Bulwer);
		all.add(CowanCowan);
		all.add(Kooringal);
		all.add(MoretonIsland);
	}

	/**
	 * Constructs a type
	 * 
	 * @param value the value of type
	 * @param abbreviation the abbreviation of type
	 */
	private BrisbaneSuburbs(String value, String abbreviation) {
		super(value, abbreviation);
	}

	/**
	 * Returns the type according to the value of type
	 * 
	 * @param value The value of type
	 * 
	 * @return The type relative to the value of type
	 */
	public static BrisbaneSuburbs getTypeFromName(String value) {
		BrisbaneSuburbs rtn = null;
		for (BrisbaneSuburbs type : all) {
			if (null != (rtn = type.fromValue(value)))
				break;
		}

		return rtn;
	}

	/**
	 * Returns the type according to the abbreviation of type
	 * 
	 * @param abbrreviation The value of abbreviation
	 * 
	 * @return The type relative to the value of abbreviation
	 */
	public static BrisbaneSuburbs getTypeFromAbbreviation(String abbrreviation) {
		BrisbaneSuburbs rtn = null;
		for (BrisbaneSuburbs type : all) {
			if (null != (rtn = type.fromAbbreviation(abbrreviation)))
				break;
		}

		return rtn;
	}
	
	/**
	 * Returns the type according to the postcode of suburb
	 * 
	 * @param postcode The postcode of suburb
	 * 
	 * @return The type relative to the value of abbreviation
	 */
	public static BrisbaneSuburbs getTypeFromPostcode(String postcode) {
		BrisbaneSuburbs rtn = null;
		for (BrisbaneSuburbs type : all) {
			if (null != (rtn = type.fromPostcode(postcode)))
				break;
		}

		return rtn;
	}

	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static BrisbaneSuburbs[] values() {
		BrisbaneSuburbs[] rtn = new BrisbaneSuburbs[all.size()];

		rtn = all.toArray(rtn);

		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> HashSet<T> getValues() {

		return (HashSet<T>) all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> T self() {

		return (T)this;
	}
}
