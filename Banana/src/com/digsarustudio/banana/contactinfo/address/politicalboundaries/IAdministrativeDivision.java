/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

/**
 * Defining the operation of administrative division
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
interface IAdministrativeDivision {
	/**
	 * Returns the subdivision in this division
	 * 
	 * @return The subdivision in this division
	 */
	<T extends IAdministrativeDivision> T getSubdivision();

	/**
	 * Assign a subdivision to this division
	 * 
	 * @param subdivision The subdivision for this division
	 */
	<T extends IAdministrativeDivision> void setSubdivision(T subdivision);

	/**
	 * Returns the division represents itself
	 * 
	 * @return the division represents itself
	 */
	<T extends IEnumAdministrativeDivision> T getDivision();

	/**
	 * Assign a enumerator of this divisions
	 * 
	 * @param division The enumerator of this division 
	 */
	<T extends IEnumAdministrativeDivision> void setDivision(T division);
}
