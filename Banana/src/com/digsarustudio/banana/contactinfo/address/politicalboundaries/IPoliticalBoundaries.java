/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

/**
 * Defining the operation of political boundaries
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IPoliticalBoundaries {
	/**
	 * Returns the country for this political boundary
	 * 
	 * @return The country for this political boundary
	 */
	Countries getCountry();
	
	/**
	 * Assign a country to this political boundary
	 * 
	 * @param country The country for this political boundary
	 */
	void setCountry(Countries country);
	
	//============ State - City - Suburb hierarchy ==================
	/**
	 * Returns the state in this country
	 * 
	 * @return The state in this country
	 */
	<T extends IEnumAdministrativeDivision> T getState();
	
	/**
	 * Assign a state to this country
	 * 
	 * @param state The state for this country
	 */
	<T extends IEnumAdministrativeDivision> void setState(T state);
	
	/**
	 * Returns the city in this country
	 * 
	 * @return The city in this country
	 */
	<T extends IEnumAdministrativeDivision> T getCity();
	
	/**
	 * Assign a city to this country
	 * 
	 * @param city The city for this country
	 * @throws StateNotSetException When the state has not been set
	 */
	<T extends IEnumAdministrativeDivision> void setCity(T city) throws StateNotSetException;
	
	/**
	 * Returns the suburb in this country
	 * 
	 * @return The suburb in this country
	 */
	<T extends IEnumAdministrativeDivision> T getSuburb();
	
	/**
	 * Assign a suburb to this country
	 * 
	 * @param suburb The suburb for this country
	 * @throws CityNotSetException When the city has not been set
	 */
	<T extends IEnumAdministrativeDivision> void setSuburb(T suburb) throws CityNotSetException;
	
	/**
	 * Returns the postcode indicates this boundary
	 * 
	 * @return the postcode indicates the boundary
	 */
	String getPostcode();
	
	//============ Other hierarchy ==================================
	//Province
	
	//District
	
	//Country
	
	//Town
	
	//Village
}
