/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import com.digsarustudio.banana.contactinfo.address.IPostcodeble;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the administrative division as a suburb
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public final class Suburb extends AdministrativeDivision implements IPostcodeble {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -5405427871288978360L;

	/**
	 * To build a suburb object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<Suburb>{
		AdministrativeDivision.Builder builder;
		
		public <T extends IEnumAdministrativeDivision> Builder(T name){
			this.builder = new AdministrativeDivision.Builder(name);
		}
		
		/**
		 * Returns the name of suburb
		 * 
		 * @return the name of suburb
		 */
		public <T extends IEnumAdministrativeDivision> T getDivision() {
			return this.builder.getDivision();
		}

		/**
		 * Returns the subdivision of suburb
		 * 
		 * @return the subdivision of suburb
		 */
		public AdministrativeDivision getSubdivision() {
			return this.builder.getSubdivision();
		}

		/**
		 * Assign the subdivision of suburb
		 * 
		 * @param subdivision the subdivision of suburb to set
		 * 
		 * @return the builder itself
		 */
		public Builder setSubdivision(AdministrativeDivision subdivision) {
			this.builder.setSubdivision(subdivision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public Suburb build() {
	
			return new Suburb(this);
		}				
	}	
	
	/**
	 * Constructs an object
	 * 
	 * @param territory The name of village
	 */
	private Suburb(Builder builder){
		this.setDivision(builder.getDivision());
		this.setSubdivision(builder.getSubdivision());
	}
	
	protected Suburb(){
		
	}
	
	/**
	 * Constructs a suburb object by copy
	 * @param source
	 * @throws CloneNotSupportedException When the subdivision cannot be cloned
	 */
	public Suburb(Suburb source) throws CloneNotSupportedException{
		this.setDivision(source.getDivision());
		
		if(null != source.getSubdivision())
			this.setSubdivision(source.getSubdivision().clone());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#clone()
	 */
	@Override
	public Suburb clone() throws CloneNotSupportedException {

		return new Suburb(this);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IPostcodeble#getPostcode()
	 */
	@Override
	public String getPostcode() {

		return this.getDivision().getPostcode();
	}
}
