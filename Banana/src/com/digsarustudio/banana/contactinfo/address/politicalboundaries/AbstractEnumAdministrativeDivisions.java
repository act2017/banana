/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import java.util.HashSet;

import com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum;
import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * The definition of enumeratable administrative division.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractEnumAdministrativeDivisions extends AbstractAbbreviatableEnum implements IEnumAdministrativeDivision {

	/**
	 * Constructs an enumeratable administrative division
	 * 
	 * @param value The value represents the administrative division. Normally, it is the name of administrative division
	 * @param abbreviation The abbreviation of the name of the administrative division
	 */
	public AbstractEnumAdministrativeDivisions(String value, String abbreviation) {
		super(value, abbreviation);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision#getPostcode()
	 */
	@Override
	public String getPostcode() {

		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#getValues()
	 */
	@Override
	protected <T extends IEnumeratable> HashSet<T> getValues() {

		throw new UnsupportedOperationException("This method has not been implemented yet.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#self()
	 */
	@Override
	protected <T extends IEnumeratable> T self() {

		return null;
	}
	
	
}
