/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import com.digsarustudio.banana.enumeration.IAbbreviatableEnum;
import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * Defining the operation of administrative division.
 * 
 * The purpose is to let administrative division can be passed by interface
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IEnumAdministrativeDivision {
	/**
	 * Returns the abbreviation
	 * 
	 * @return The abbreviation
	 */
	String getAbbreviation();		
	
	/**
	 * Returns the enumeration of abbreviation
	 * 
	 * @param abrreviation The abbreviation to get enumeration
	 * 
	 * @return the enumerator relative to the input abbreviation; null if nothing found 
	 */
	<T extends IAbbreviatableEnum> T fromAbbreviation(String abrreviation);
	
	/**
	 * Returns the value of enumerator
	 * 
	 * @return The value of enumerator
	 */
	String getValue();
	
	/**
	 * Returns the enumerator by referring to value
	 * 
	 * @param value The value of enumerator
	 * 
	 * @return The enumerator represents this value; null if the value is not found.
	 */
	<T extends IEnumeratable> T fromValue(String value);
	
	/**
	 * Returns the postcode of this administrative division if this enumeration administrative division contains it.
	 * 
	 * @return The postcode of this administrative division; null if nothing found.
	 * 
	 * @exception UnsupportedOperationException When this method has not been implemented yet.
	 */
	String getPostcode();
}
