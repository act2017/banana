/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import java.io.Serializable;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represent an administrative division in a country. It could contain a sub division in it.
 * Such as State has City or City has Suburb.
 * 
 * This is the entity operated in the political boundary class for residential address and postal address
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class AdministrativeDivision implements Serializable, Cloneable {
	
	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 5598984021572883774L;

	/**
	 * Builder
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<AdministrativeDivision>{		
		private IEnumAdministrativeDivision division;
		private AdministrativeDivision subdivision;

		/**
		 * Constructs a builder 
		 * @param name the enumerator of this administrative division
		 */
		public <T extends IEnumAdministrativeDivision> Builder(T division) {
			this.division = division;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		
		public AdministrativeDivision build() {
			
			return new AdministrativeDivision(this);
		}

		/**
		 * Returns the enumerator of this administrative division
		 * 
		 * @return the enumerator of this administrative division
		 */
		@SuppressWarnings("unchecked")
		public <T extends IEnumAdministrativeDivision> T getDivision() {
			return (T) division;
		}

		/**
		 * Returns the subdivision of this administrative division
		 * 
		 * @return the subdivision of this administrative division
		 */
		@SuppressWarnings("unchecked")
		public <S extends AdministrativeDivision> S getSubdivision() {
			return (S) this.subdivision;
		}

		/**
		 * Assign a subdivision
		 * @param subdivision the subdivision to set
		 */
		public Builder setSubdivision(AdministrativeDivision subdivision) {
			this.subdivision = subdivision;
			return this;
		}				
	}
	
	/**
	 * The subdivision of this administrative division.
	 */
	private AdministrativeDivision subdivision;
	
	/**
	 * The enumeration indicates the value of territory
	 */
	private IEnumAdministrativeDivision division;
	
	/**
	 * hide the constructor for the usage of builder.
	 * @param builder Territory builder
	 */
	private AdministrativeDivision(Builder builder){
		this.setSubdivision(builder.getSubdivision());
		this.setDivision(builder.getDivision());
	}
	
	protected AdministrativeDivision(){
		
	}
	
//	/**
//	 * For sub-class to use.
//	 */
//	protected <T extends IEnumAdministrativeDivision> AdministrativeDivision(T division) {
//		this.setDivision(division);
//	}
	
	/**
	 * Copy constructor
	 * 
	 * @param source The source object to copy
	 * @throws CloneNotSupportedException 
	 */
	public AdministrativeDivision(AdministrativeDivision source) throws CloneNotSupportedException {
		if(source.getSubdivision() != null){
//			this.setSubdivision(new AdministrativeDivision(source.getSubdivision()));
		}else{
			this.setDivision(source.getDivision());
		}
	}

	/**
	 * Returns the subdivision in this division
	 * 
	 * @return The subdivision in this division
	 */
	
	@SuppressWarnings("unchecked")
	public <T extends AdministrativeDivision> T getSubdivision() {
		return (T) this.subdivision;
	}

	/**
	 * Assign a subdivision to this division
	 * 
	 * @param subdivision The subdivision for this division
	 */
	
	public<T extends AdministrativeDivision> void setSubdivision(T subdivision) {
		this.subdivision = (AdministrativeDivision) subdivision;
	}

	/**
	 * Returns the division represents itself
	 * 
	 * @return the division represents itself
	 */
	
	@SuppressWarnings("unchecked")
	public <T extends IEnumAdministrativeDivision> T getDivision() {
		return (T) this.division;
	}

	/**
	 * Assign a enumerator of this divisions
	 * 
	 * @param division The enumerator of this division 
	 */
	
	public <T extends IEnumAdministrativeDivision> void setDivision(T division) {
		this.division = division;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this.subdivision == null) ? 0 : this.subdivision.hashCode());
		result = prime * result + ((this.division == null) ? 0 : this.division.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AdministrativeDivision)) {
			return false;
		}
	
		AdministrativeDivision other = (AdministrativeDivision) obj;
		if (this.subdivision == null) {
			if (other.getSubdivision() != null) {
				return false;
			}
		} else if (!this.subdivision.equals(other.getSubdivision())) {
			return false;
		}
		if (this.division == null) {
			if (other.division != null) {
				return false;
			}
		} else if (!division.equals(other.division)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	
	public AdministrativeDivision clone() throws CloneNotSupportedException {
		
		return new AdministrativeDivision(this);
	}
}
