/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import java.io.Serializable;

import com.digsarustudio.banana.contactinfo.address.IPostcodeble;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the political boundaries in the residential address or postal address
 * 
 * Note: This class has to be refactored.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 *
 */
public class PoliticalBoundaries implements IPoliticalBoundaries, Serializable, Cloneable {
	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 5922204439403321913L;
	
	/**
	 * To build political boundaries object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<PoliticalBoundaries>{
		private Countries country;
		private IEnumAdministrativeDivision state;
		@SuppressWarnings("unused")
		private IEnumAdministrativeDivision city;
		@SuppressWarnings("unused")
		private IEnumAdministrativeDivision suburb;
		
		/**
		 * Constructs a builder for political boundary
		 * 
		 * @param country The country of this political boundary
		 */
		public Builder(Countries country) {
			this.country = country;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public PoliticalBoundaries build(){
			
			PoliticalBoundaries rtn =  new PoliticalBoundaries(this.country);
			rtn.setState(this.state);

//			rtn.setCity(this.city);
//			rtn.setSuburb(this.suburb);
				
			return rtn;
		}

		/**
		 * Assign a state to the political boundary
		 * 
		 * @param state The state for the political boundary
		 * @return builder
		 */
		public <T extends IEnumAdministrativeDivision> Builder setState(T state) {
			
			this.state = state;
			return this;
		}

		/**
		 * Assign a city to the political boundary
		 * 
		 * @param city The city for the political boundary
		 * @return builder
		 */
		public <T extends IEnumAdministrativeDivision> Builder setCity(T city){
			
			this.city = city;
			return this;
		}

		/**
		 * Assign a suburb to the political boundary
		 * 
		 * @param suburb The suburb for the political boundary
		 * @return builder
		 */
		public <T extends IEnumAdministrativeDivision> Builder setSuburb(T suburb){
			
			this.suburb = suburb;
			return this;
		}	
	}
	
	/**
	 * The country
	 */
	private Countries country;
	
	/**
	 * The hierarchically administrative division.
	 * It could be State->City->Suburb or
	 * 			   Province->City->Town->Village
	 */
	private AdministrativeDivision division;

	/**
	 * Constructs a political boundaries
	 */
	private PoliticalBoundaries(Countries country) {

		this.setCountry(country);
	}
	
	/**
	 * Copy a political boundaries
	 * 
	 * @param source The target object to copy
	 */
	public PoliticalBoundaries(PoliticalBoundaries source) {

		this.setCountry(source.getCountry());
		this.setState(source.getState());
		try {
			this.setCity(source.getCity());
			this.setSuburb(source.getSuburb());
		} catch (StateNotSetException | CityNotSetException e) {
			e.printStackTrace();
		}		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getCountry()
	 */
	@Override
	public Countries getCountry() {
		
		return this.country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)
	 */
	@Override
	public void setCountry(Countries country) {
		
		this.country = country;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getState()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends IEnumAdministrativeDivision> T getState() {
		
		State state = this.getDivisionEntity(State.class);
		
		return (null != state) ? (T)state.getDivision() : null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setState(T state) {
		
		if(null == state)
			return;
		
		this.division = new State.Builder(state).build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getCity()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends IEnumAdministrativeDivision> T getCity() {
		
		City city = this.getDivisionEntity(City.class);
		
		return (null != city) ? (T)city.getDivision() : null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setCity(T city) throws StateNotSetException {
		
		State state = this.getDivisionEntity(State.class);
//		if( null == state ){
//			throw new StateNotSetException("The state must be set before the city to be set");
//		}else if(null == city){
//			return;
//		}
		
		state.setSubdivision(new City.Builder(city).build());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getSuburb()
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends IEnumAdministrativeDivision> T getSuburb() {
		
		Suburb suburb = this.getDivisionEntity(Suburb.class);
		
		return (null != suburb) ? (T)suburb.getDivision() : null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setSuburb(T suburb) throws CityNotSetException {
		
		City city = this.getDivisionEntity(City.class);
//		if( null == city ){
//			throw new CityNotSetException("The city must be set before the suburb to be set");
//		}else if(null == suburb){
//			return;
//		}
		
		city.setSubdivision(new Suburb.Builder(suburb).build());
	}	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((division == null) ? 0 : division.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PoliticalBoundaries)) {
			return false;
		}
		PoliticalBoundaries other = (PoliticalBoundaries) obj;
		if (country == null) {
			if (other.country != null) {
				return false;
			}
		} else if (!country.equals(other.country)) {
			return false;
		}
		if (division == null) {
			if (other.division != null) {
				return false;
			}
		} else if (!division.equals(other.division)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PoliticalBoundaries clone() throws CloneNotSupportedException {
		
		PoliticalBoundaries rtn = null;
//		try {
//			rtn = new PoliticalBoundaries.Builder(this.getCountry())
//										 .setState(this.getState())
//										 .setCity(this.getCity())
//										 .setSuburb(this.getSuburb())
//										 .build();
//			
//		} catch (StateNotSetException | CityNotSetException e) {
//			e.printStackTrace();
//		}
				
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getPostcode()
	 */
	@Override
	public String getPostcode() {
		
		AdministrativeDivision division = this.getDivisionEntityContainsPostCode();
		
		if(null == division)
			return null;
						
		if(null == division.getDivision())
			return null;
		
		return division.getDivision().getPostcode();
	}
	
	/**
	 * Get the entity of division
	 * 
	 * @param type The class type of division
	 * 
	 * @return The entity of division
	 */
	@SuppressWarnings("unchecked")
	protected <T extends AdministrativeDivision, S extends AdministrativeDivision> T getDivisionEntity(Class<S> type){
		AdministrativeDivision division = this.division;
		
		T rtn = null;
		
		while(null != division){
			if( !(division.getClass() == type) ){
				division = division.getSubdivision();
				continue;
			}
			
			rtn = (T)division;
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Get the entity of division
	 * 
	 * @param type The class type of division
	 * 
	 * @return The entity of division
	 */
	@SuppressWarnings("unchecked")
	protected <T extends AdministrativeDivision, S extends IPostcodeble> T getDivisionEntityContainsPostCode(){
		AdministrativeDivision division = this.division;
		
		T rtn = null;
		
		while(null != division){
			if( !(division instanceof IPostcodeble) ){
				division = division.getSubdivision();
				continue;
			}
			
			rtn = (T)division;
			break;
		}
		
		return rtn;
	}
}
