/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

/**
 * Represents the exception of city not to set when the subdivision set
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class CityNotSetException extends Exception {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 6476058534474743093L;

	/**
	 * 
	 */
	public CityNotSetException() {
	}

	/**
	 * @param message
	 */
	public CityNotSetException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public CityNotSetException(Throwable cause) {
		super(cause);
	}

	/**
	 * @param message
	 * @param cause
	 */
	public CityNotSetException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public CityNotSetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
