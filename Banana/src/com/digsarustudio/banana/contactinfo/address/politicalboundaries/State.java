/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents the administrative division as a State
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.0
 *
 */
public final class State extends AdministrativeDivision{
	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 8913935902691344094L;

	/**
	 * To build state object
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<State>{
		AdministrativeDivision.Builder builder;
		
		public <T extends IEnumAdministrativeDivision> Builder(T division){
			this.builder = new AdministrativeDivision.Builder(division);
		}
		
		/**
		 * Returns the name of town
		 * 
		 * @return the name of town
		 */
		public <T extends IEnumAdministrativeDivision> T getDivision() {
			return this.builder.getDivision();
		}

		/**
		 * Returns the subdivision of town
		 * 
		 * @return the subdivision of town
		 */
		public AdministrativeDivision getSubdivision() {
			return this.builder.getSubdivision();
		}

		/**
		 * Assign the subdivision of town
		 * 
		 * @param subdivision the subdivision of town to set
		 * 
		 * @return the builder itself
		 */
		public Builder setSubdivision(AdministrativeDivision subdivision) {
			this.builder.setSubdivision(subdivision);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public State build() {
	
			return new State(this);
		}				
	}
	
	/**
	 * Constructs an object
	 * 
	 * @param territory The name of village
	 */
	private State(Builder builder){
		this.setDivision(builder.getDivision());
		this.setSubdivision(builder.getSubdivision());
	}
	
	/**
	 * Constructs a state by copy
	 * 
	 * @param source The source state to copy
	 * @throws CloneNotSupportedException When the subdivision cannot be cloned
	 */
	public State(State source) throws CloneNotSupportedException{
		this.setDivision(source.getDivision());
		
		if(null != source.getSubdivision())
			this.setSubdivision(source.getSubdivision().clone());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.administrativedivision.AdministrativeDivision#clone()
	 */
	@Override
	public State clone() throws CloneNotSupportedException {

		return new State(this);
	}
}
