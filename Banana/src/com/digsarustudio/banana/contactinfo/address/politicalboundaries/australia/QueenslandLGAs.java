/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia;

import java.io.Serializable;
import java.util.HashSet;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.AbstractEnumLocalGovernments;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries;
import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * The local governments in Queensland, Australia
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class QueenslandLGAs extends AbstractEnumLocalGovernments implements Serializable {

	/**
	 *Do not remove. for datastore to use. 
	 */
	private static final long serialVersionUID = 3499723483241295813L;
	
	//South East
	public final static QueenslandLGAs Brisbane			= new QueenslandLGAs("Brisbane", "BNE").setRegion("South East");
	public final static QueenslandLGAs GoldCoast		= new QueenslandLGAs("Gold Coast", "GOLD COAST").setRegion("South East"); 
	public final static QueenslandLGAs Ipswich			= new QueenslandLGAs("Ipswich", "IPSWICH").setRegion("South East");
	public final static QueenslandLGAs LockyerValley	= new QueenslandLGAs("Lockyer Valley", "LOCKYER VALLEY").setRegion("South East"); 
	public final static QueenslandLGAs Logan			= new QueenslandLGAs("Logan", "LOGAN").setRegion("South East");
	public final static QueenslandLGAs MoretonBay		= new QueenslandLGAs("Moreton Bay", "MORETON BAY").setRegion("South East");
	public final static QueenslandLGAs Noosa			= new QueenslandLGAs("Noosa", "NOOSA").setRegion("South East");
	public final static QueenslandLGAs Redland			= new QueenslandLGAs("Redland", "REDLAND").setRegion("South East");
	public final static QueenslandLGAs ScenicRim		= new QueenslandLGAs("Scenic Rim", "SCENIC RIM").setRegion("South East");
	public final static QueenslandLGAs Somerset			= new QueenslandLGAs("Somerset", "SOMERSET").setRegion("South East");
	public final static QueenslandLGAs SunshineCoast	= new QueenslandLGAs("Sunshine Coast", "SUNSHINE COAST").setRegion("South East");
	
	//Wide Bay-Burnett
	public final static QueenslandLGAs Bundaberg		= new QueenslandLGAs("Bundaberg", "BUNDABERG").setRegion("Wide Bay-Burnett");
	public final static QueenslandLGAs Cherbourg		= new QueenslandLGAs("Cherbourg", "CHERBOURG").setRegion("Wide Bay-Burnett");
	public final static QueenslandLGAs FraserCoast		= new QueenslandLGAs("Fraser Coast", "FRASER COAST").setRegion("Wide Bay-Burnett");
	public final static QueenslandLGAs Gympie			= new QueenslandLGAs("Gympie", "GYMPIE").setRegion("Wide Bay-Burnett");
	public final static QueenslandLGAs NorthBurnett		= new QueenslandLGAs("North Burnett", "NORTH BURNETT").setRegion("Wide Bay-Burnett");
	public final static QueenslandLGAs SouthBurnett		= new QueenslandLGAs("South Burnett", "SOUTH BURNETT").setRegion("Wide Bay-Burnett");
	
	//Darling Downs
	public final static QueenslandLGAs Goondiwindi 		= new QueenslandLGAs("Goondiwindi", "GOONDIWINDI").setRegion("Darling Downs");
	public final static QueenslandLGAs SouthernDowns	= new QueenslandLGAs("Southern Downs", "SOUTHERN DOWNS").setRegion("Darling Downs"); 
	public final static QueenslandLGAs Toowoomba		= new QueenslandLGAs("Toowoomba", "TOOWOOMBA").setRegion("Darling Downs");
	public final static QueenslandLGAs WesternDowns		= new QueenslandLGAs("Western Downs", "WESTERN DOWNS").setRegion("Darling Downs");

	//Central
	public final static QueenslandLGAs Banana			= new QueenslandLGAs("Banana", "BANANA").setRegion("Central");
	public final static QueenslandLGAs CentralHighlands	= new QueenslandLGAs("Central Highlands", "CENTRAL HIGHLANDS").setRegion("Central");
	public final static QueenslandLGAs Gladstone		= new QueenslandLGAs("Gladstone", "GLADSTONE").setRegion("Central");
	public final static QueenslandLGAs Isaac			= new QueenslandLGAs("Isaac", "ISAAC").setRegion("Central");
	public final static QueenslandLGAs Livingstone		= new QueenslandLGAs("Livingstone", "LIVINGSTONE").setRegion("Central");
	public final static QueenslandLGAs Rockhampton		= new QueenslandLGAs("Rockhampton", "ROCKHAMPTON").setRegion("Central");
	public final static QueenslandLGAs Whitsunday		= new QueenslandLGAs("Whitsunday", "WHITSUNDAY").setRegion("Central");
	public final static QueenslandLGAs Woorabinda		= new QueenslandLGAs("Woorabinda", "WOORABINDA").setRegion("Central");
	
	//North
	public final static QueenslandLGAs Burdekin			= new QueenslandLGAs("Burdekin", "BURDEKIN").setRegion("North");
	public final static QueenslandLGAs ChartersTowers	= new QueenslandLGAs("Charters Towers", "CHARTERS TOWERS").setRegion("North");
	public final static QueenslandLGAs Hinchinbrook		= new QueenslandLGAs("Hinchinbrook", "HINCHINBROOK").setRegion("North");
	public final static QueenslandLGAs Mackay			= new QueenslandLGAs("Mackay", "MACKAY").setRegion("North");
	public final static QueenslandLGAs Mareeba			= new QueenslandLGAs("Mareeba", "MAREEBA").setRegion("North");
	public final static QueenslandLGAs PalmIsland		= new QueenslandLGAs("Palm Island", "PALM ISLAND").setRegion("North");
	public final static QueenslandLGAs Townsville		= new QueenslandLGAs("Townsville", "TOWNSVILLE").setRegion("North");
	
	//Far North
	public final static QueenslandLGAs Aurukun			= new QueenslandLGAs("Aurukun", "AURUKUN").setRegion("Far North");
	public final static QueenslandLGAs Cairns			= new QueenslandLGAs("Cairns", "CAIRNS").setRegion("Far North");
	public final static QueenslandLGAs CassowaryCoast	= new QueenslandLGAs("Cassowary Coast", "CASSOWARY COAST").setRegion("Far North");
	public final static QueenslandLGAs Cook				= new QueenslandLGAs("Cook", "COOK").setRegion("Far North");
	public final static QueenslandLGAs Douglas			= new QueenslandLGAs("Douglas", "DOUGLAS").setRegion("Far North");
	public final static QueenslandLGAs Hopevale			= new QueenslandLGAs("Hopevale", "HOPEVALE").setRegion("Far North");
	public final static QueenslandLGAs Kowanyama		= new QueenslandLGAs("Kowanyama", "KOWANYAMA").setRegion("Far North");
	public final static QueenslandLGAs LockhartRiver	= new QueenslandLGAs("Lockhart River", "LOCKHART RIVER").setRegion("Far North");
	public final static QueenslandLGAs Mapoon			= new QueenslandLGAs("Mapoon", "MAPOON").setRegion("Far North");
	public final static QueenslandLGAs Napranum			= new QueenslandLGAs("Napranum", "NAPRANUM").setRegion("Far North");
	public final static QueenslandLGAs NorthernPeninsulaArea	= new QueenslandLGAs("Northern Peninsula Area", "NORTHERN PENINSULA AREA").setRegion("Far North");
	public final static QueenslandLGAs Pormpuraaw		= new QueenslandLGAs("Pormpuraaw", "PORMPURAAW").setRegion("Far North");
	public final static QueenslandLGAs Tablelands		= new QueenslandLGAs("Tablelands", "TABLELANDS").setRegion("Far North");
	public final static QueenslandLGAs Torres			= new QueenslandLGAs("Torres", "TORRES").setRegion("Far North");
	public final static QueenslandLGAs TorresStraitIsland	= new QueenslandLGAs("Torres Strait Island", "TORRES STRAIT ISLAND").setRegion("Far North");
	public final static QueenslandLGAs Weipa			= new QueenslandLGAs("Weipa", "WEIPA").setRegion("Far North");
	public final static QueenslandLGAs WujalWujal		= new QueenslandLGAs("Wujal Wujal", "WUJAL WUJAL").setRegion("Far North");
	public final static QueenslandLGAs Yarrabah			= new QueenslandLGAs("Yarrabah", "YARRABAH").setRegion("Far North");
	
	//North West
	public final static QueenslandLGAs Burke			= new QueenslandLGAs("Burke", "BURKE").setRegion("North West");
	public final static QueenslandLGAs Carpentaria		= new QueenslandLGAs("Carpentaria", "CARPENTARIA").setRegion("North West");
	public final static QueenslandLGAs Cloncurry		= new QueenslandLGAs("Cloncurry", "CLONCURRY").setRegion("North West");
	public final static QueenslandLGAs Croydon			= new QueenslandLGAs("Croydon", "CROYDON").setRegion("North West");
	public final static QueenslandLGAs Doomadgee		= new QueenslandLGAs("Doomadgee", "DOOMADGEE").setRegion("North West");
	public final static QueenslandLGAs Etheridge		= new QueenslandLGAs("Etheridge", "ETHERIDGE").setRegion("North West");
	public final static QueenslandLGAs Flinders			= new QueenslandLGAs("Flinders", "FLINDERS").setRegion("North West");
	public final static QueenslandLGAs McKinlay			= new QueenslandLGAs("McKinlay", "MCKINLAY").setRegion("North West");
	public final static QueenslandLGAs Mornington		= new QueenslandLGAs("Mornington", "MORNINGTON").setRegion("North West");
	public final static QueenslandLGAs MountIsa			= new QueenslandLGAs("Mount Isa", "MOUNT ISA").setRegion("North West");
	public final static QueenslandLGAs Richmond			= new QueenslandLGAs("Richmond", "RICHMOND").setRegion("North West");
	
	//Central West
	public final static QueenslandLGAs Barcaldine		= new QueenslandLGAs("Barcaldine", "BARCALDINE").setRegion("Central West");
	public final static QueenslandLGAs Barcoo			= new QueenslandLGAs("Barcoo", "BARCOO").setRegion("Central West");
	public final static QueenslandLGAs BlackallTambo	= new QueenslandLGAs("Blackall-Tambo", "BLACKALL-TAMBO").setRegion("Central West");
	public final static QueenslandLGAs Boulia			= new QueenslandLGAs("Boulia", "BOULIA").setRegion("Central West");
	public final static QueenslandLGAs Diamantina		= new QueenslandLGAs("Diamantina", "DIAMANTINA").setRegion("Central West");
	public final static QueenslandLGAs Longreach		= new QueenslandLGAs("Longreach", "LONGREACH").setRegion("Central West");
	public final static QueenslandLGAs Winton			= new QueenslandLGAs("Winton", "WINTON").setRegion("Central West");
	
	//South West
	public final static QueenslandLGAs Balonne			= new QueenslandLGAs("Balonne", "BALONNE").setRegion("South West");
	public final static QueenslandLGAs Bulloo			= new QueenslandLGAs("Bulloo", "BULLOO").setRegion("South West");
	public final static QueenslandLGAs Maranoa			= new QueenslandLGAs("Maranoa", "MARANOA").setRegion("South West");
	public final static QueenslandLGAs Murweh			= new QueenslandLGAs("Murweh", "MURWEH").setRegion("South West");
	public final static QueenslandLGAs Paroo			= new QueenslandLGAs("Paroo", "PAROO").setRegion("South West");
	public final static QueenslandLGAs Quilpie			= new QueenslandLGAs("Quilpie", "QUILPIE").setRegion("South West");
	
	private static HashSet<QueenslandLGAs> all = new HashSet<>();
	
	/**
	 * Which country this state belongs to.
	 */
	public final static Countries belongs2 = Countries.Australia;

	static {
		//South East
		all.add(Brisbane);
		all.add(GoldCoast);
		all.add(Ipswich);
		all.add(LockyerValley);
		all.add(Logan);
		all.add(MoretonBay);
		all.add(Noosa);
		all.add(Redland);
		all.add(ScenicRim);
		all.add(Somerset);
		all.add(SunshineCoast);
		
		//Wide Bay - Burnett
		all.add(Bundaberg);
		all.add(Cherbourg);
		all.add(FraserCoast);
		all.add(Gympie);
		all.add(NorthBurnett);
		all.add(SouthBurnett);
		
		//Darling Downs
		all.add(Goondiwindi);
		all.add(SouthernDowns); 
		all.add(Toowoomba);
		all.add(WesternDowns);
		
		//Central
		all.add(Banana);
		all.add(CentralHighlands);
		all.add(Gladstone);
		all.add(Isaac);
		all.add(Livingstone);
		all.add(Rockhampton);
		all.add(Whitsunday);
		all.add(Woorabinda);
		
		//North
		all.add(Burdekin);
		all.add(ChartersTowers);
		all.add(Hinchinbrook);
		all.add(Mackay);
		all.add(Mareeba);
		all.add(PalmIsland);
		all.add(Townsville);
		
		//Far North
		all.add(Aurukun);
		all.add(Cairns);
		all.add(CassowaryCoast);
		all.add(Cook);
		all.add(Douglas);
		all.add(Hopevale);
		all.add(Kowanyama);
		all.add(LockhartRiver);
		all.add(Mapoon);
		all.add(Napranum);
		all.add(NorthernPeninsulaArea);
		all.add(Pormpuraaw);
		all.add(Tablelands);
		all.add(Torres);
		all.add(TorresStraitIsland);
		all.add(Weipa);
		all.add(WujalWujal);
		all.add(Yarrabah);
		
		//North West
		all.add(Burke);
		all.add(Carpentaria);
		all.add(Cloncurry);
		all.add(Croydon);
		all.add(Doomadgee);
		all.add(Etheridge);
		all.add(Flinders);
		all.add(McKinlay);
		all.add(Mornington);
		all.add(MountIsa);
		all.add(Richmond);
		
		//Central West
		all.add(Barcaldine);
		all.add(Barcoo);
		all.add(BlackallTambo);
		all.add(Boulia);
		all.add(Diamantina);
		all.add(Longreach);
		all.add(Winton);
		
		//South West
		all.add(Balonne);
		all.add(Bulloo);
		all.add(Maranoa);
		all.add(Murweh);
		all.add(Paroo);
		all.add(Quilpie);
	}

	/**
	 * Constructs a local government in Queensland, Australia
	 * 
	 * @param value the value of type
	 * @param abbreviation the abbreviation of type
	 */
	private QueenslandLGAs(String value, String abbreviation) {
		super(value, abbreviation);
	}

	/**
	 * Returns the type according to the value of type
	 * 
	 * @param value The value of type
	 * 
	 * @return The type relative to the value of type
	 */
	public static QueenslandLGAs getTypeFromValue(String value) {
		QueenslandLGAs rtn = null;
		for (QueenslandLGAs type : all) {
			if (null != (rtn = type.fromValue(value)))
				break;
		}

		return rtn;
	}

	/**
	 * Returns the type according to the abbreviation of type
	 * 
	 * @param value The value of abbreviation
	 * 
	 * @return The type relative to the value of abbreviation
	 */
	public static QueenslandLGAs getTypeFromAbbreviation(String abbrreviation) {
		QueenslandLGAs rtn = null;
		for (QueenslandLGAs type : all) {
			if (null != (rtn = type.fromAbbreviation(abbrreviation)))
				break;
		}

		return rtn;
	}

	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static QueenslandLGAs[] values() {
		QueenslandLGAs[] rtn = new QueenslandLGAs[all.size()];

		rtn = all.toArray(rtn);

		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> HashSet<T> getValues() {

		return (HashSet<T>)all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> T self() {

		return (T)this;
	}	
	
	
}
