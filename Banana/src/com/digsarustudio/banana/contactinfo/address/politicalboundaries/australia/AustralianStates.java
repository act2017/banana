/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries.australia;

import java.io.Serializable;
import java.util.HashSet;

import com.digsarustudio.banana.contactinfo.address.politicalboundaries.AbstractEnumStates;
import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * The states in Australia
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class AustralianStates extends AbstractEnumStates implements Serializable {
		
	/**
	 * Do not remove. For datastore to use. 
	 */
	private static final long serialVersionUID = 2932536945085210131L;
	
	public final static AustralianStates Queensland = new AustralianStates("Queensland", "QLD");
	public final static AustralianStates NewSouthWales = new AustralianStates("New South Wales", "NSW");
	public final static AustralianStates AustralianCapitalTerritory = new AustralianStates("Australian Capital Territory", "ACT");
	public final static AustralianStates Victoria = new AustralianStates("Victoria", "VIC");
	public final static AustralianStates Tasmania = new AustralianStates("Tasmania", "TAS");
	public final static AustralianStates SouthAustralia = new AustralianStates("South Australia", "SA");
	public final static AustralianStates WesternAustralia = new AustralianStates("Western Australia", "WA");
	public final static AustralianStates NorthenTerritory = new AustralianStates("Northen Territory", "NT");

	/**
	 * The set of enumerators
	 */
	private final static HashSet<AustralianStates> all = new HashSet<AustralianStates>();
	
	/**
	 * Used to put the enumerators into the static list, otherwise there is nothing in the [all].
	 */
	static{
		all.add(Queensland);
		all.add(NewSouthWales);
		all.add(AustralianCapitalTerritory);
		all.add(Victoria);
		all.add(Tasmania);
		all.add(SouthAustralia);
		all.add(WesternAustralia);
		all.add(NorthenTerritory);
	}	
	
	/**
	 * Constructs a state for Australia
	 * 
	 * @param value The name of state
	 * @param abbreviation The abbreviation of name of state
	 */
	private AustralianStates(String value, String abbreviation) {
		super(value, abbreviation);
	}
	
	/**
	 * Returns the type according to the value of state
	 * 
	 * @param value The value of state
	 * 
	 * @return The type relative to the value of state
	 */
	public static AustralianStates getTypeFromName(String name){
		AustralianStates rtn = null;
		for (AustralianStates state : all) {
			rtn = state.fromValue(name);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns the type according to the abbreviation of state
	 * 
	 * @param value The value of abbreviation
	 * 
	 * @return The type relative to the value of abbreviation
	 */
	public static AustralianStates getTypeFromAbbreviation(String abbrreviation){
		AustralianStates rtn = null;
		for (AustralianStates state : all) {
			rtn = state.fromAbbreviation(abbrreviation);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns all of the values
	 * 
	 * @return the set of values
	 */
	public static AustralianStates[] values(){
		AustralianStates[] rtn = new AustralianStates[all.size()];
		
		rtn = all.toArray(rtn);
		
		return rtn;
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> HashSet<T> getValues() {

		return (HashSet<T>) all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#self()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> T self() {

		return (T) this;
	}

}
