/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

/**
 * The definition of 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public abstract class AbstractEnumLocalGovernments extends AbstractEnumAdministrativeDivisions {

	/**
	 * the region of this territory
	 */
	private String region;
	
	
	/**
	 * Constructs an enumerator of local governments
	 * 
	 * @param value The name of this local governments
	 * @param abbreviation The abbreviation of name of this local government
	 */
	public AbstractEnumLocalGovernments(String value, String abbreviation) {
		super(value, abbreviation);
	}
	
	/**
	 * Returns the region of the suburb in Queensland
	 * 
	 * @return the region of the suburb in Queensland
	 */
	public String getRegion() {
		return region;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof AbstractEnumLocalGovernments)) {
			return false;
		}
		AbstractEnumLocalGovernments other = (AbstractEnumLocalGovernments) obj;
		if (region == null) {
			if (other.region != null) {
				return false;
			}
		} else if (!region.equals(other.region)) {
			return false;
		}
		return true;
	}

	/**
	 * Assign the region of the suburb in Queensland
	 * @param region the region to set
	 * 
	 * @return the enumerator itself
	 */
	@SuppressWarnings("unchecked")
	protected <T extends AbstractEnumLocalGovernments> T setRegion(String region) {
		this.region = region;
		return (T)this;
	}	
}
