/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address.politicalboundaries;

import java.io.Serializable;
import java.util.HashSet;

import com.digsarustudio.banana.enumeration.AbstractAbbreviatableEnum;
import com.digsarustudio.banana.enumeration.IEnumeratable;

/**
 * The enumeration of countries
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public final class Countries extends AbstractAbbreviatableEnum implements Serializable {

	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = 3785599598973665552L;

	public final static Countries Australia				= new Countries("Australia", "AUS");
	public final static Countries Taiwan				= new Countries("Taiwan", "TW");
	public final static Countries UnitedStatesOfAmerica = new Countries("United States of America", "USA");
	
	/**
	 * The set of enumerators
	 */
	private final static HashSet<Countries> all = new HashSet<Countries>();
	
	/**
	 * Used to put the enumerators into the static list, otherwise there is nothing in the [all].
	 */
	static{
		all.add(Australia);
		all.add(Taiwan);
		all.add(UnitedStatesOfAmerica);
	}
	
	/**
	 * Constructs a country enumerator
	 * @param value The name of country
	 * @param abbreviation The abbreviation of country
	 */
	private Countries(String value, String abbreviation) {
		super(value, abbreviation);
	}
	
	/**
	 * Returns the type according to the name of country
	 * 
	 * @param name The name of country
	 * 
	 * @return The type relative to the name of country
	 */
	public static Countries getTypeFromName(String name){
		Countries rtn = null;
		for (Countries country : all) {
			rtn = country.fromValue(name);
			break;
		}
		
		return rtn;
	}
	
	/**
	 * Returns the type according to the abbreviation of country
	 * 
	 * @param name The name of abbreviation
	 * 
	 * @return The type relative to the name of abbreviation
	 */
	public static Countries getTypeFromAbbreviation(String abbrreviation){
		Countries rtn = null;
		for (Countries country : all) {
			rtn = country.fromAbbreviation(abbrreviation);
			break;
		}
		
		return rtn;
	}
	

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#getValues()
	 */
	@SuppressWarnings("unchecked")
	@Override
	protected <T extends IEnumeratable> HashSet<T> getValues() {

		return (HashSet<T>) all;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractAbbreviatableEnum#self()
	 */
	@Override
	protected <T extends IEnumeratable> T self() {

		return null;
	}

}
