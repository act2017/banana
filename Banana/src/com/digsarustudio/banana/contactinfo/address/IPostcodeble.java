/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

/**
 * To the fine the object contains the postcode as a field
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface IPostcodeble {
	/**
	 * Returns the postcode indicates this boundary
	 * 
	 * @return the postcode indicates the boundary
	 */
	String getPostcode();
}
