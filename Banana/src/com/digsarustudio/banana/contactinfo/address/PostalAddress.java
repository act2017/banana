/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import java.io.Serializable;

import com.digsarustudio.banana.contactinfo.address.deliverylocation.POBox;
import com.digsarustudio.banana.contactinfo.address.deliverylocation.PhysicalDeliveryLocation;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.CityNotSetException;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.PoliticalBoundaries;
import com.digsarustudio.banana.contactinfo.address.politicalboundaries.StateNotSetException;
import com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType;
import com.digsarustudio.banana.contactinfo.address.property.BuildingType;
import com.digsarustudio.banana.contactinfo.address.property.LevelType;
import com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType;
import com.digsarustudio.banana.contactinfo.address.property.PropertyTypeNotMatchException;
import com.digsarustudio.banana.contactinfo.address.street.StreetType;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Represents a P.O.Box address
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		0.0.1
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public class PostalAddress implements IPostalAddress, Serializable{
	
	/**
	 * Do not remove. For datastore to use.
	 */
	private static final long serialVersionUID = -1022590814488097813L;

	/**
	 * To build postal address
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		0.0.1
	 * @since		0.0.1
	 *
	 */
	public static class Builder implements ObjectBuilder<PostalAddress> {
		private Countries					country			= null;
		
		private IEnumAdministrativeDivision state			= null;
		private IEnumAdministrativeDivision city			= null;
		private IEnumAdministrativeDivision suburb			= null;
		
		private String						streetName		= null;
		private StreetType					streetType		= null;
		private String						streetSuffix	= null;
		
		private String						propertyNumber	= null;
		private AbstractPropertyType		propertyType	= null;
		
		private String						buildingNumber	= null;
		private Integer						level			= null;
		private LevelType					levelType		= null;
		
		private String						poBoxNumber		= null;
		private PostalDeliveryType			deliveryType	= null;
		
		/**
		 * Constructs a builder  object
		 * 
		 * @param country The country for the target postal address
		 */
		public Builder(Countries coutry) {
	
			this.country = coutry;
		}

		/**
		 * Returns the country of the target postal address
		 * 
		 * @return the country of the target postal address
		 */
		private Countries getCountry() {
			return country;
		}

		/**
		 * Returns the state the target postal address in
		 * 
		 * @return the state the target postal address in
		 */
		private IEnumAdministrativeDivision getState() {
			return state;
		}

		/**
		 * Assign a state for the target postal address to build
		 * 
		 * @param state the state for the target postal address to set
		 * 
		 * @return Builder the builder
		 */
		public Builder setState(IEnumAdministrativeDivision state) {
			this.state = state;
			return this;
		}

		/**
		 * Returns the city the target postal address in
		 * 
		 * @return the city the target postal address in
		 */
		private IEnumAdministrativeDivision getCity() {
			return city;
		}

		/**
		 * Assign a city for the target postal address to build
		 * 
		 * @param city the city for the target postal address to set
		 * 
		 * @return Builder the builder
		 */
		public Builder setCity(IEnumAdministrativeDivision city) {
			this.city = city;
			
			return this;
		}
		
		/**
		 * Returns the suburb the target postal address in
		 * 
		 * @return the suburb The target postal address in
		 */
		private IEnumAdministrativeDivision getSuburb() {
			return suburb;
		}
		
		/**
		 * Assign a suburb for the target postal address to build
		 * 
		 * @param suburb the suburb for the target postal address to set
		 * 
		 * @return Builder the builder
		 */
		public Builder setSuburb(IEnumAdministrativeDivision suburb) {
			this.suburb = suburb;
			return this;
		}		

		/**
		 * Returns the name of street
		 * 
		 * @return the name of street
		 */
		private String getStreetName() {
			return streetName;
		}

		/**
		 * Assign a name to the street
		 * 
		 * @param streetName the name for the street to set
		 */
		public Builder setStreetName(String name) {			
			this.streetName = name;
			return this;
		}

		/**
		 * Returns the type of street
		 * 
		 * @return the type of street
		 */
		private StreetType getStreetType() {
			return streetType;
		}

		/**
		 * Assign a type to the street
		 * @param streetType the type for the street to set
		 */
		public Builder setStreetType(StreetType type) {
			this.streetType = type;
			return this;
		}

		/**
		 * Returns the suffix of street
		 * 
		 * @return the suffix of street
		 */
		private String getStreetSuffix() {
			return streetSuffix;
		}

		/**
		 * Assign a suffix to the street
		 * 
		 * @param streetSuffix the suffix for the street to set
		 */
		public Builder setStreetSuffix(String suffix) {
			this.streetSuffix = suffix;
			return this;
		}

		/**
		 * Returns the number of property
		 * 
		 * @return the number of property
		 */
		private String getPropertyNumber() {
			return propertyNumber;
		}

		/**
		 * Assign a number to the property
		 * 
		 * @param number the number for the property to set
		 */
		public Builder setPropertyNumber(String number) {
			this.propertyNumber = number;
			return this;
		}

		/**
		 * Returns the type of property
		 * 
		 * @return the type of property
		 */
		private AbstractPropertyType getPropertyType() {
			return propertyType;
		}

		/**
		 * Assign a type to the property
		 * 
		 * @param type the type for the property to set
		 */
		public Builder setPropertyType(AbstractPropertyType type) {
			this.propertyType = type;
			return this;
		}

		/**
		 * Returns the number of building
		 * 
		 * @return the number of building
		 */
		private String getBuildingNumber() {
			return buildingNumber;
		}

		/**
		 * Assign a number to the building
		 * 
		 * @param buildingNumber the number for the building to set
		 */
		public Builder setBuildingNumber(String number) {
			this.buildingNumber = number;
			return this;
		}

		/**
		 * Returns the level of this address
		 * 
		 * @return the level of this address
		 */
		private Integer getLevel() {
			return level;
		}

		/**
		 * Assign a level to this address
		 * 
		 * @param level the level for this address to set
		 */
		public Builder setLevel(Integer level) {
			this.level = level;
			return this;
		}

		/**
		 * Returns the type of level
		 * 
		 * @return the type of level
		 */
		private LevelType getLevelType() {
			return levelType;
		}

		/**
		 * Assign a type to the level
		 * 
		 * @param type the type for the level to set
		 */
		public Builder setLevelType(LevelType type) {
			this.levelType = type;
			return this;
		}

		/**
		 * Returns the number of P.O.Box
		 * 
		 * @return the number of P.O.Box
		 */
		private String getPoBoxNumber() {
			return poBoxNumber;
		}

		/**
		 * Assign a number to the P.O.Box
		 * 
		 * @param number the number for the P.O.Box to set
		 */
		public Builder setPoBoxNumber(String number) {
			this.poBoxNumber = number;
			return this;
		}

		/**
		 * Returns the postal delivery type
		 * 
		 * @return the postal delivery type
		 */
		private PostalDeliveryType getDeliveryType() {
			return deliveryType;
		}

		/**
		 * Assign a postal delivery type
		 * 
		 * @param type the postal delivery type to set
		 */
		public Builder setDeliveryType(PostalDeliveryType type) {
			this.deliveryType = type;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.IBuilder#build()
		 */
		@Override
		public PostalAddress build() {
			
			return new PostalAddress(this);
		}
	}
	
	/**
	 * Political boundaries
	 */
	private PoliticalBoundaries boundaries;
	
	/**
	 * The delivery location should be assigned by client because the type of postal address is depending on the system used in different countries. 
	 */
	private IDeliveryLocation location; 
	

	/**
	 * Constructs a postal address
	 * 
	 * @param builder The builder to construct the postal address object
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	private PostalAddress(Builder builder) {

		this.boundaries = new PoliticalBoundaries.Builder(builder.getCountry())
												 .setState(builder.getState())
												 .setCity(builder.getCity())
												 .setSuburb(builder.getSuburb())
												 .build();

		if(null != builder.getDeliveryType()){
			this.location = new POBox(builder.getPoBoxNumber(), builder.getDeliveryType());
		}else{
			this.location = new PhysicalDeliveryLocation.Builder(builder.getPropertyNumber())
														.setStreetName(builder.getStreetName())
														.setStreetType(builder.getStreetType())
														.setStreetSuffix(builder.getStreetSuffix())
														.setPropertyType(builder.getPropertyType())
														.build();
			
			if(builder.getPropertyType() instanceof BuildingType){
				try {
					this.location.setBuildingNumber(builder.getBuildingNumber());
					this.location.setLevelType(builder.getLevelType());
					this.location.setLevel(builder.getLevel());
				} catch (PropertyTypeNotMatchException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	
	/**
	 * Constructs a postal address by copy 
	 * 
	 * @param source The target postal address to copy
	 * @throws CityNotSetException 
	 * @throws StateNotSetException 
	 */
	public PostalAddress(PostalAddress source) throws StateNotSetException, CityNotSetException {

		this.boundaries = new PoliticalBoundaries.Builder(source.getCountry())
												 .setState(source.getState())
												 .setCity(source.getCity())
												 .setSuburb(source.getSuburb())
												 .build();

		try{
			PostalDeliveryType deliveryType = source.getPostalDeliveryType();			
			this.location = new POBox(source.getPOBoxNumber(), deliveryType);
		}catch (UnsupportedOperationException e) {
			// TODO: handle exception
			this.location = new PhysicalDeliveryLocation((PhysicalDeliveryLocation)source.getLocation());
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getCountry()
	 */
	@Override
	public Countries getCountry() {
		
		return this.boundaries.getCountry();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setCountry(com.digsarustudio.banana.contactinfo.address.politicalboundaries.Countries)
	 */
	@Override
	public void setCountry(Countries country) {
		
		this.boundaries.setCountry(country);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getState()
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> T getState() {
		
		return this.boundaries.getState();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setState(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setState(T state) {
		
		this.boundaries.setState(state);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getCity()
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> T getCity() {
		
		return this.boundaries.getCity();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setCity(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setCity(T city) throws StateNotSetException {
		
		this.boundaries.setCity(city);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getSuburb()
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> T getSuburb() {
		
		return this.boundaries.getSuburb();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#setSuburb(com.digsarustudio.banana.contactinfo.address.politicalboundaries.IEnumAdministrativeDivision)
	 */
	@Override
	public <T extends IEnumAdministrativeDivision> void setSuburb(T suburb) throws CityNotSetException {
		
		this.boundaries.setSuburb(suburb);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.politicalboundaries.IPoliticalBoundaries#getPostcode()
	 */
	@Override
	public String getPostcode() {
		
		return this.boundaries.getPostcode();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IPostalAddress#getPOBoxNumber()
	 */
	@Override
	public String getPOBoxNumber() {
		
		return this.location.getLocationNumber();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IPostalAddress#setPOBoxNumber(java.lang.String)
	 */
	@Override
	public void setPOBoxNumber(String number) {
		
		this.location.setLocationNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IPostalAddress#getPostalDeliveryType()
	 */
	@Override
	public PostalDeliveryType getPostalDeliveryType() {
		
		return this.location.getPostalDeliveryType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getStreetName()
	 */
	@Override
	public String getStreetName() {
		
		return this.location.getStreetName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setStreetName(java.lang.String)
	 */
	@Override
	public void setStreetName(String name) {
		
		this.location.setStreetName(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getStreetType()
	 */
	@Override
	public StreetType getStreetType() {
		
		return this.location.getStreetType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setStreetType(com.digsarustudio.banana.contactinfo.address.street.StreetType)
	 */
	@Override
	public void setStreetType(StreetType type) {
		
		this.location.setStreetType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getStreetSuffix()
	 */
	@Override
	public String getStreetSuffix() {
		
		return this.location.getStreetSuffix();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setStreetSuffix(java.lang.String)
	 */
	@Override
	public void setStreetSuffix(String suffix) {
		
		this.location.setStreetSuffix(suffix);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getPropertyNumber()
	 */
	@Override
	public String getPropertyNumber() {
		
		return this.location.getLocationNumber();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setPropertyNumber(java.lang.String)
	 */
	@Override
	public void setPropertyNumber(String number) {
		
		this.location.setLocationNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getPropertyType()
	 */
	@Override
	public <T extends AbstractPropertyType> T getPropertyType() {
		
		return this.location.getPropertyType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setPropertyType(com.digsarustudio.banana.contactinfo.address.property.AbstractPropertyType)
	 */
	@Override
	public <T extends AbstractPropertyType> void setPropertyType(T type) {
		
		this.location.setPropertyType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getBuildingNumber()
	 */
	@Override
	public String getBuildingNumber() {
		
		return this.location.getBuildingNumber();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setBuildingNumber(java.lang.String)
	 */
	@Override
	public void setBuildingNumber(String number) throws PropertyTypeNotMatchException {
		
		this.location.setBuildingNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getLevel()
	 */
	@Override
	public Integer getLevel() {
		
		return this.location.getLevel();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setLevel(java.lang.Integer)
	 */
	@Override
	public void setLevel(Integer level) throws PropertyTypeNotMatchException {
		
		this.location.setLevel(level);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#getLevelType()
	 */
	@Override
	public LevelType getLevelType() {
		
		return this.location.getLevelType();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IResidentialAddress#setLevelType(com.digsarustudio.banana.contactinfo.address.property.LevelType)
	 */
	@Override
	public void setLevelType(LevelType type) throws PropertyTypeNotMatchException {
		
		this.location.setLevelType(type);
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.contactinfo.address.IPostalAddress#setPostalDeliveryType(com.digsarustudio.banana.contactinfo.address.property.PostalDeliveryType)
	 */
	@Override
	public void setPostalDeliveryType(PostalDeliveryType type) {
		
		this.location.setPostalDeliveryType(type);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((boundaries == null) ? 0 : boundaries.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof PostalAddress)) {
			return false;
		}
		PostalAddress other = (PostalAddress) obj;
		if (boundaries == null) {
			if (other.boundaries != null) {
				return false;
			}
		} else if (!boundaries.equals(other.boundaries)) {
			return false;
		}
		if (location == null) {
			if (other.location != null) {
				return false;
			}
		} else if (!location.equals(other.location)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	public PostalAddress clone() throws CloneNotSupportedException {
		
		PostalAddress rtn = null;
		
		try {
			rtn = new PostalAddress(this);
		} catch (StateNotSetException|CityNotSetException e) {
			e.printStackTrace();
		}
		
		return rtn;
	}
	
	/**
	 * Returns the delivery location
	 * 
	 * @return The delivery location
	 */
	private IDeliveryLocation getLocation(){
		return this.location;
	}
}
