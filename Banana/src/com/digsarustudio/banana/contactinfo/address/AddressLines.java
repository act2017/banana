/**
 * 
 */
package com.digsarustudio.banana.contactinfo.address;

import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The address lines of a particular address.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	No longer available. This interface moved into Musaceae module.<br>
 */
public interface AddressLines {
	/**
	 * 
	 * The builder for {@link AddressLines}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<AddressLines> {
		Builder addAddress(String address);
		Builder setAddresses(List<String> addresses);
	}
	
	void addAddress(String address);
	void setAddresses(List<String> addresses);
	
	String getAddress(Integer index);
	List<String> getAddresses();
}
