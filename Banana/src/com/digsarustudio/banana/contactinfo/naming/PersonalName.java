/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents the name of a person including given name, middle name, and surname.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface PersonalName extends Identity {
	/**
	 * 
	 * The builder for {@link PersonalName}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<PersonalName> {
		Builder setSurname(String name);
		Builder setMiddleName(String name);
		Builder setGivenName(String name);
	}
	
	void setSurname(String name);
	void setMiddleName(String name);
	void setGivenName(String name);
	
	String getSurname();
	String getMiddleName();
	String getGivenName();
	
	PersonalName copy();
}
