/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;

/**
 * The sub-type represents an identity of an entity.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface Identity {
}
