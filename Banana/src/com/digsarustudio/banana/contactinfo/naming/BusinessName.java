/**
 * 
 */
package com.digsarustudio.banana.contactinfo.naming;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type represents a name for the business. For example, Google Pty. Ltd.<br>
 * This also provides the abbreviation name of it. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.3
 * @deprecated	Please use the interface in the Musaceae module.<br>
 */
public interface BusinessName extends Identity {
	/**
	 * 
	 * The builder for {@link Business}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.3
	 *
	 */
	public static interface Builder extends ObjectBuilder<BusinessName> {
		Builder setName(String name);
		Builder setAbbreviation(String name);
	}
	
	
	void setName(String name);
	void setAbbreviation(String name);
	
	String getName();
	String getAbbreviation();
	
	BusinessName copy();
}
