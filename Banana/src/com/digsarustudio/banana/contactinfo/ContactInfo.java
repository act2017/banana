/**
 * 
 */
package com.digsarustudio.banana.contactinfo;

import java.io.Serializable;

import com.digsarustudio.banana.contactinfo.address.PostalAddress;
import com.digsarustudio.banana.contactinfo.address.ResidentialAddress;
import com.digsarustudio.banana.contactinfo.phone.PhoneNumber;

/**
 * The collection of information to contact. 
 * Normally, it is used as a container or collection to transit data from one process to another one.
 * 
 * Including residential address, postal address, telephone number, mobile phone number, fax number, email, website(optional)
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class ContactInfo implements Serializable {
	
	/**
	 * Do not remove. For datastore to use. 
	 */
	private static final long serialVersionUID = 62068333789072718L;

	//residential address
	private ResidentialAddress	residentialAddress;
	
	//postal address - physical address / alternative postal location
	private PostalAddress		postalAddress;
	
	//telephone number
	private PhoneNumber			telephoneNumber;
	
	//mobile phone number
	private PhoneNumber			mobileNumber;
	
	//fax number
	private PhoneNumber			faxNumber;
	
	//email
	private EmailAddress		eMailAddress;
	
	//website(optional, for business)
	
	/**
	 * Constructs a contact information object
	 */
	public ContactInfo(){
		
	}

	/**
	 * Returns the residential address in this contact info
	 * 
	 * @return the residentialAddress in this contact info
	 */
	public ResidentialAddress getResidentialAddress() {
		return residentialAddress;
	}

	/**
	 * Assign a residential address to this contact info
	 * 
	 * @param residentialAddress the residentialAddress for this contact info to set
	 */
	public void setResidentialAddress(ResidentialAddress residentialAddress) {
		this.residentialAddress = residentialAddress;
	}

	/**
	 * Returns the postal address in this contact info
	 * 
	 * @return the postalAddress in this contact info
	 */
	public PostalAddress getPostalAddress() {
		return postalAddress;
	}

	/**
	 * Assign a postal address to this contact info
	 * 
	 * @param postalAddress the postalAddress for this contact info to set
	 */
	public void setPostalAddress(PostalAddress postalAddress) {
		this.postalAddress = postalAddress;
	}

	/**
	 * Returns the telephone number in this contact info
	 * 
	 * @return the telephone number in this contact info
	 */
	public PhoneNumber getTelephoneNumber() {
		return telephoneNumber;
	}

	/**
	 * Assign a telephone number to this contact info
	 * 
	 * @param telephoneNumber the telephone number for this contact info to set
	 */
	public void setTelephoneNumber(PhoneNumber telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}

	/**
	 * Returns the mobile number in this contact info
	 * 
	 * @return the mobileNumber in this contact info
	 */
	public PhoneNumber getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * Assign a mobile number to this contact info
	 * 
	 * @param mobileNumber the mobile number for this contact info to set
	 */
	public void setMobileNumber(PhoneNumber mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * Returns the fax number in this contact info
	 * 
	 * @return the fax number in this contact info
	 */
	public PhoneNumber getFaxNumber() {
		return faxNumber;
	}

	/**
	 * Assign a fax number to this contact info
	 * 
	 * @param faxNumber the fax number for this contact info to set
	 */
	public void setFaxNumber(PhoneNumber faxNumber) {
		this.faxNumber = faxNumber;
	}

	/**
	 * Returns the e-mail address in this contact info
	 * 
	 * @return the eMailAddress in this contact info
	 */
	public EmailAddress geteMailAddress() {
		return eMailAddress;
	}

	/**
	 * Assign a email address to this contact info
	 * 
	 * @param eMailAddress the eMailAddress for this contact info to set
	 */
	public void seteMailAddress(EmailAddress eMailAddress) {
		this.eMailAddress = eMailAddress;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((eMailAddress == null) ? 0 : eMailAddress.hashCode());
		result = prime * result + ((faxNumber == null) ? 0 : faxNumber.hashCode());
		result = prime * result + ((mobileNumber == null) ? 0 : mobileNumber.hashCode());
		result = prime * result + ((postalAddress == null) ? 0 : postalAddress.hashCode());
		result = prime * result + ((residentialAddress == null) ? 0 : residentialAddress.hashCode());
		result = prime * result + ((telephoneNumber == null) ? 0 : telephoneNumber.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof ContactInfo)) {
			return false;
		}
		ContactInfo other = (ContactInfo) obj;
		if (eMailAddress == null) {
			if (other.eMailAddress != null) {
				return false;
			}
		} else if (!eMailAddress.equals(other.eMailAddress)) {
			return false;
		}
		if (faxNumber == null) {
			if (other.faxNumber != null) {
				return false;
			}
		} else if (!faxNumber.equals(other.faxNumber)) {
			return false;
		}
		if (mobileNumber == null) {
			if (other.mobileNumber != null) {
				return false;
			}
		} else if (!mobileNumber.equals(other.mobileNumber)) {
			return false;
		}
		if (postalAddress == null) {
			if (other.postalAddress != null) {
				return false;
			}
		} else if (!postalAddress.equals(other.postalAddress)) {
			return false;
		}
		if (residentialAddress == null) {
			if (other.residentialAddress != null) {
				return false;
			}
		} else if (!residentialAddress.equals(other.residentialAddress)) {
			return false;
		}
		if (telephoneNumber == null) {
			if (other.telephoneNumber != null) {
				return false;
			}
		} else if (!telephoneNumber.equals(other.telephoneNumber)) {
			return false;
		}
		return true;
	}
	
	
}
