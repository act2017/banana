/**
 * 
 */
package com.digsarustudio.banana.converter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Represents the visitor for a string to convert to a Date
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class SimpleDateFormatVisitor implements IStringCoverterVisitor {

	/**
	 * Constructs a date converter visitor
	 */
	public SimpleDateFormatVisitor() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.converter.IStringCoverterVisitor#visitDateConversion(com.digsarustudio.banana.converter.DateConverter)
	 */
	@Override
	public void visit(DateConverter date) {
		DateFormat formatter = new SimpleDateFormat(date.getFormat());
		
		try {
			date.setDate(formatter.parse(date.getDate()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

}
