/**
 * 
 */
package com.digsarustudio.banana.converter;

/**
 * Defining the operations of string converter to convert input string to a target type.
 * This is used for builder pattern
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.2 No longer to use
 */
public interface IStringConverter{
	/**
	 * Convert the input string
	 * 
	 * @param string The string to convert
	 * @param visitor The visitor used to convert the target string
	 */
	<T> void convertString(String string, T visitor);
}
