/**
 * 
 */
package com.digsarustudio.banana.converter;

/**
 * Defining the operations of visitor to convert string into any form.
 * This is used for visitor pattern.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.2 No longer to use
 */
public interface IStringCoverterVisitor {

	/**
	 * Visits the date converter
	 * 
	 * @param converter The date converter
	 */
	void visit(DateConverter converter);
}
