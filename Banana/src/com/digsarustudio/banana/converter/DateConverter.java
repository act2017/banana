/**
 * 
 */
package com.digsarustudio.banana.converter;

import java.util.Date;

import com.digsarustudio.banana.designpattern.builder.IBuilder;
import com.digsarustudio.banana.designpattern.visitor.IGenericVisitable;
import com.digsarustudio.banana.utils.DataConverter;

/**
 * Represents a converter which can convert a string into a date object
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated	Please use {@link DataConverter} instead
 */
public class DateConverter implements IStringConverter, IBuilder, IGenericVisitable {
	/**
	 * The return value for director
	 */
	private Date	rtn		= null;
	
	/**
	 * The format to convert
	 */
	private String	format	= null;
	
	/**
	 * The source date in string
	 */
	private String	date	= null;

	/**
	 * Constructs a date converter object
	 * 
	 * @param format The format of the string to convert which depends on the locale.
	 */
	public DateConverter(String format) {
		this.format = format;
	}

	/**
	 * Returns the date in text
	 * 
	 * @return the date in text
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * Assign a string for the date to convert
	 * 
	 * @param date the string for the date to set
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Assign a date object for the result
	 * 
	 * @param date the date for the result to set
	 */
	public void setDate(Date date) {
		this.rtn = date;
	}

	/**
	 * Returns the format of the date representing in string
	 * 
	 * @return the format of the date representing in string
	 */
	public String getFormat() {
		return format;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.designpattern.builder.IBuilder#getResult()
	 */
	@Override
	public Date getResult() {

		return this.rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.converter.IStringConverter#convertString(java.lang.String)
	 */
	@Override
	public <T> void convertString(String string, T visitor) {

		this.setDate(string);
		this.accept(visitor);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.designpattern.visitor.IVisitable#accept(java.lang.Object)
	 */
	@Override
	public <T> void accept(T visitor) {

		IStringCoverterVisitor converter = (IStringCoverterVisitor)visitor;
		
		converter.visit(this);
	}

}
