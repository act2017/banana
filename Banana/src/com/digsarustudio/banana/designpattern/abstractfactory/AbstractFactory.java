/**
 * 
 */
package com.digsarustudio.banana.designpattern.abstractfactory;

/**
 * The generic abstractor factory to create an object.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @param		T The type that the abstract factory to create.
 */
public interface AbstractFactory<T> {
	T create();
}
