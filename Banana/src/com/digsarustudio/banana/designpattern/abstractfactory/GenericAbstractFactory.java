/**
 * 
 */
package com.digsarustudio.banana.designpattern.abstractfactory;

/**
 * The sub-type works as an abstract factory.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface GenericAbstractFactory {

}
