/**
 * 
 */
package com.digsarustudio.banana.designpattern.visitor;

/**
 * Defining the operation of the data format validation used in visitor pattern
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IDataFormatValidationVisitor {

	/**
	 * Visit the validation of string format
	 * 
	 * @param target The target object to validate
	 */
	<T extends IDataFormatValidationVisitable> void visitStringFormatValidation(T target);
}
