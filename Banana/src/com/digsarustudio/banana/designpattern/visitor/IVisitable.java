/**
 * 
 */
package com.digsarustudio.banana.designpattern.visitor;

/**
 * Defining the operation for a class wants to be operated with visitor pattern.
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IVisitable {
	/**
	 * Accepts a visitor
	 * 
	 * @param visitor The visitor to deal with this visitable object
	 */
	void accept(IVisitor visitor);
}
