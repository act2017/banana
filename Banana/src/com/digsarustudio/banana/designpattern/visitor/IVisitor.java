/**
 * 
 */
package com.digsarustudio.banana.designpattern.visitor;

/**
 * Defining the interface for a visitor to visit a visitable object
 * If the sub-type needs its own operation, just declare a new visit function for it.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IVisitor {
	/**
	 * To visit the element
	 * 
	 * @param element The object type element
	 */
	void visit(IVisitable element);
	
	
	/**
	 * To visit the element manipulated by a wrapper without accept method.
	 */
	void visit(IVisitableElement element);
}
