/**
 * 
 */
package com.digsarustudio.banana.designpattern.visitor;

/**
 * Defining the operation for a class wants to be operated with visitor pattern  in generic form
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IGenericVisitable {
	<T> void accept(T visitor);
}
