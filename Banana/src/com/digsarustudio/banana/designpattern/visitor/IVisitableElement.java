/**
 * 
 */
package com.digsarustudio.banana.designpattern.visitor;

/**
 * Defining the interface of operation for the element which can be visited by a visitor.
 * This is a fundamental interface so that there is no methods declared here.
 * 
 * If the sub-type needs some specific interface to manipulate, just declare the method in the sub-type.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public interface IVisitableElement {

}
