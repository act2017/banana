/**
 * 
 */
package com.digsarustudio.banana.designpattern.visitor;

/**
 * Defining the operations of the visitability for data format validation
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public interface IDataFormatValidationVisitable {
	/**
	 * Returns the format for the visitor to validate the data
	 * 
	 * @return The for mat for the visitor to validate the data
	 */
	<T> T getFormat();
	
	/**
	 * Returns the data for the visitor to validate
	 * 
	 * @return The data for the visitor to validate
	 */
	<T> T getData();
}
