/**
 * 
 */
package com.digsarustudio.banana.designpattern.builder;

/**
 * Defining the operation of director for builder pattern
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.2 Please use {@link BuildDirector} instead 
 */
public interface IDirector {

	/**
	 * Constructs the product by builder
	 * 
	 * @return The product produced by target builder from the concrete director
	 */
	<T> T construct();
}
