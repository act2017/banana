/**
 * 
 */
package com.digsarustudio.banana.designpattern.builder;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The director for the {@link ObjectBuilder}
 * 
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @param T The target data type to be converted
 * @param E The {@link ObjectBuilder} of the target data type
 */
public interface BuildDirector<T, E extends ObjectBuilder<?>> {
	T construct(E builder);
}
