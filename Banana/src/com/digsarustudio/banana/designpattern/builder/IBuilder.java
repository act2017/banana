/**
 * 
 */
package com.digsarustudio.banana.designpattern.builder;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * Defining the operation for the builder pattern
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.2 Please use {@link ObjectBuilder} instead.
 */
public interface IBuilder {
	/**
	 * Returns the result after built
	 * 
	 * @return The result after built; null if it is failed to build the object.
	 */
	Object getResult();
}
