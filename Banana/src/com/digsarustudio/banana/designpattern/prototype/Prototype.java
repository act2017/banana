/**
 * 
 */
package com.digsarustudio.banana.designpattern.prototype;

/**
 * The sub-type of {@link Prototype} can copy itself as a new object.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface Prototype extends Cloneable{
	Prototype clone() throws CloneNotSupportedException;
}
