/**
 * 
 */
package com.digsarustudio.banana.graphic;

/**
 * Represents a size object indicating the width and height of rectangle
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class Size {
	private Integer width;
	private Integer height;
	private String	unit = "px";
	

	/**
	 * Constructs a size object
	 * 
	 * @param width The width of target graphic
	 * @param height The height of target graphic
	 */
	public Size(Integer width, Integer height) {
		this.width = width;
		this.height = height;
	}

	/**
	 * Copy constructor
	 * 
	 * @param source the source object to copy
	 */
	public Size(Size source) {
		this.setWidth(source.getWidth());
		this.setHeight(source.getHeight());
	}

	/**
	 * Returns the width of graphic
	 * 
	 * @return the width of graphic
	 */
	public Integer getWidth() {
		return width;
	}


	/**
	 * Assign a width to the graphic
	 * 
	 * @param width the width to set
	 */
	public void setWidth(Integer width) {
		this.width = width;
	}


	/**
	 * Returns the height of graphic
	 * 
	 * @return the height of graphic
	 */
	public Integer getHeight() {
		return height;
	}


	/**
	 * Assign a height to the graphic
	 * 
	 * @param height the height to set
	 */
	public void setHeight(Integer height) {
		this.height = height;
	}


	/**
	 * Returns the unit of width and height
	 * 
	 * @return the unit of width and height
	 */
	public String getUnit() {
		return unit;
	}


	/**
	 * Assign a unit of width and height to the graphic
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}	
	
}
