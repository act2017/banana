/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * The sub-type can be used to convert JSON string to javascript object or other serializable object, vice versa.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Dson {
	/**
	 * Converts an object to be a json string
	 * 
	 * @param source The source object
	 * 
	 * @return The JSON string represents the source object
	 */
	<T> String toJSON(T source);	
	
	/**
	 * Converts a JSON string to an object
	 * 
	 * @param jsonString The JSON string
	 * 
	 * @return An object converted from the input JSON string
	 */
	<T> T fromJSON(String jsonString);
	
	/**
	 * Converts a JSON string to an object according to the particular type
	 * 
	 * @param jsonString The JSON string
	 * 
	 * @return An object converted into the particular type from the input JSON string
	 */
	<T> T fromJSON(String jsonString, Class<T> type);
	
	/**
	 * Returns true if the input text is a JSON string, otherwise false returned.
	 * 
	 * @param text A string to text if it is a JSON string
	 * 
	 * @return true if the input text is a JSON string, otherwise false returned.
	 */
	Boolean isJSONString(String text);
}
