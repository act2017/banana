/**
 * 
 */
package com.digsarustudio.banana.utils.logging;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


/**
 * The base of the log manager
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class AbstractLogManager implements LogManager {
	/**
	 * The container for loggers
	 */
	private Map<String, Logger> loggers = new HashMap<>();

	/**
	 * Constructs a base of log manager
	 */
	public AbstractLogManager() {
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Enableable#setEnable()
	 */
	@Override
	public void setEnable() {
		Iterator<Logger> iterator = this.loggers.values().iterator();
		
		while (iterator.hasNext()) {
			iterator.next().setEnable();
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Enableable#setDisable()
	 */
	@Override
	public void setDisable() {
		Iterator<Logger> iterator = this.loggers.values().iterator();
		
		while (iterator.hasNext()) {
			iterator.next().setDisable();
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Enableable#isEnabled()
	 */
	@Override
	public Boolean isEnabled() {

		return null;
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#getLogger(java.lang.String)
	 */
	@Override
	public Logger getLogger(String className) {

		return null;
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#enable(java.lang.String)
	 */
	@Override
	public void enable(String className) {

		
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#enable(com.digsarustudio.banana.utils.logging.LogLevel)
	 */
	@Override
	public void enable(LogLevel level) {

		
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#enable(java.lang.String, com.digsarustudio.banana.utils.logging.LogLevel)
	 */
	@Override
	public void enable(String className, LogLevel level) {

		
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#disable(java.lang.String)
	 */
	@Override
	public void disable(String className) {

		
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#disable(com.digsarustudio.banana.utils.logging.LogLevel)
	 */
	@Override
	public void disable(LogLevel level) {

		
	}



	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.LogManager#disable(java.lang.String, com.digsarustudio.banana.utils.logging.LogLevel)
	 */
	@Override
	public void disable(String className, LogLevel level) {

		
	}



}
