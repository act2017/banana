/**
 * 
 */
package com.digsarustudio.banana.utils.logging;

/**
 * The base of logger
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class AbstractLogger implements Logger {
	/**
	 * The flag indicates this logger is workable to write a log if it is true.
	 * This log is not workable for writing a log if it is false.
	 */
	private Boolean isEnabled = true;
	
	/**
	 * Construct a base of logger
	 */
	public AbstractLogger() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Enableable#setEnable()
	 */
	@Override
	public void setEnable() {
		this.isEnabled = true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Enableable#setDisable()
	 */
	@Override
	public void setDisable() {
		this.isEnabled = false;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.Enableable#isEnabled()
	 */
	@Override
	public Boolean isEnabled() {
		return this.isEnabled;
	}

}
