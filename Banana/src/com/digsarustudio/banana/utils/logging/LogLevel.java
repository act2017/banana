/**
 * 
 */
package com.digsarustudio.banana.utils.logging;

import java.util.logging.Level;

/**
 * A wrapper of java.util.logging.Level, but an error occured said Lin 28 is undefined.
 * 
 * To copy the fundamental functions from Level
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class LogLevel{
	public static final LogLevel INFO		= new LogLevel(Level.INFO);	
	public static final LogLevel WARNING	= new LogLevel(Level.WARNING); 
	public static final LogLevel SEVERE		= new LogLevel(Level.SEVERE);	

	private Level value;
	
	public LogLevel(Level value){			
		this.value = value;
	}
	
	public Level getValue(){
		return this.value;
	}
}
