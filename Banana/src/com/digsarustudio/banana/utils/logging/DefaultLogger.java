/**
 * 
 */
package com.digsarustudio.banana.utils.logging;

/**
 * The implementation of the com.digsarustudio.banana.utils.logging.Logger
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DefaultLogger extends AbstractLogger implements Logger {
	private java.util.logging.Logger	javaLogger = null;
	/**
	 * Constructs a default logger which manipulates a java.util.logging.Logger
	 */
	private DefaultLogger(java.util.logging.Logger logger) {
		this.javaLogger = logger;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.Logger#info(java.lang.String)
	 */
	@Override
	public void info(String message) {
		if( !this.isEnabled() ){
			return;
		}
		
		this.javaLogger.info(message);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.Logger#warning(java.lang.String)
	 */
	@Override
	public void warning(String message) {
		if( !this.isEnabled() ){
			return;
		}
		
		this.javaLogger.warning(message);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.utils.logging.Logger#severe(java.lang.String)
	 */
	@Override
	public void severe(String message) {
		if( !this.isEnabled() ){
			return;
		}
		
		this.javaLogger.severe(message);		
	}

}
