/**
 * 
 */
package com.digsarustudio.banana.utils.logging;

import com.digsarustudio.banana.utils.Enableable;

/**
 * Why I have to implement a logger
 * 	1. The approach of output is different
 * 	2. To make the interface consistently.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Logger extends Enableable{
	void info(String message);
	void warning(String message);
	void severe(String message);
	
	
}
