/**
 * 
 */
package com.digsarustudio.banana.utils.logging;

import com.digsarustudio.banana.utils.Enableable;

/**
 * By using this logger, user can disable and enable.
 * 
 * Why I have to implement my own logger and log manager:
 * 	1.The developer cannot disable/enable a logger at the outside of the target object
 * 	2.The developer cannot disable/enable loggers at the same time.
 * 	3.The developer cannot disable/enable the levels of logger at the outside of the target object
 * 	4.The developer cannot disable/enable the level of loggers at the same time.
 * 	 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface LogManager extends Enableable{
	Logger getLogger(String name);		
	
	void enable(String name);
	void enable(LogLevel level);
	void enable(String name, LogLevel level);
	
	void disable(String name);
	void disable(LogLevel level);
	void disable(String name, LogLevel level);
	
}
