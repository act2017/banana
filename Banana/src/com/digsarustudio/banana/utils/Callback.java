/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * The callback object for the delegate class to callback an event to the client code.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @param		T The data object which the delegate class to return back.
 */
public interface Callback<T> {
	void onCall(T param);
}
