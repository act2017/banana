/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * To indicate an operation of method accessing is not exact what the client wants.
 * This exception will be thrown when the program tries to access a method which the client doesn't expect for.
 * 
 * Normally, this exception will be coded in the unit test to make sure the flow work of program is on the right track.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.1
 *
 */
public class UnexpectedMethodAccessedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5610101367573694589L;

	/**
	 * 
	 */
	public UnexpectedMethodAccessedException() {
	}

	/**
	 * @param arg0
	 */
	public UnexpectedMethodAccessedException(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public UnexpectedMethodAccessedException(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public UnexpectedMethodAccessedException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnexpectedMethodAccessedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
