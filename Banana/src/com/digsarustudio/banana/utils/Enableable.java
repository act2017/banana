/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * To define the sub-type has the ability to be enabled or disabled.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Enableable {
	void setEnable();
	void setDisable();
	
	Boolean isEnabled();
}
