/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * The sub-type can be used to build an object from a specific class.
 * This is the fundamental type of the Builder class, please extends this interface for
 * the sub-type.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ObjectBuilder<T> {
	T build();
}
