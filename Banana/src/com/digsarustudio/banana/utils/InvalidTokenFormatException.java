/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * [desc]
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class InvalidTokenFormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9031314715331272156L;

	/**
	 * 
	 */
	public InvalidTokenFormatException() {

	}

	/**
	 * @param message
	 */
	public InvalidTokenFormatException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public InvalidTokenFormatException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidTokenFormatException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidTokenFormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
