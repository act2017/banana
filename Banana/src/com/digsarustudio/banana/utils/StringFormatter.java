/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * The wrapper of {@link StringBuilder} to accept format and arguments as {@link String#format(String, Object...)}
 * for some libraries or SKDs which can not use String.format().
 *
 * The supported type of arguments shown as following:
 * 	%d	for Integer
 *  %s	for String
 *  
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class StringFormatter {
	private static final String ARG_STRING	= "%s";
	private static final String ARG_INTEGER	= "%d";
	@SuppressWarnings("unused")
	private static final String ARG_HEX		= "%x";
	@SuppressWarnings("unused")
	private static final String ARG_HEX_UP	= "%X";
	
	private static final char[]	HEX_DIGIT	= { '0', '1', '2', '3', '4', '5'
											  , '6', '7', '8', '9', 'A', 'B'
											  , 'C', 'D', 'E', 'F'
											  };
	
	/**
	 * 
	 */
	private StringFormatter() {

	}
	
	public static String formatExceptionCauseMessage(String message, Throwable cause) {
		StringBuffer buffer = new StringBuffer();
		
		buffer.append(message);
		
		Throwable child = cause;
		while(null != child){
			buffer.append("\n\nCause:\n");
			buffer.append(child.getMessage());
			
			child = child.getCause();
		}		
		
		return buffer.toString();
	}
	
	public static String formatExceptionCauseMessage(Throwable cause) {
		StringBuffer buffer = new StringBuffer();
		
		Throwable child = cause;
		while(null != child){
			buffer.append("\n\nCause:\n");
			buffer.append(child.getMessage());
			
			child = child.getCause();
		}		
		
		return buffer.toString();
	}
	
	public static String bytes2Hex(byte[] bytes) {
		StringBuffer buffer = new StringBuffer(bytes.length*2);

		for(int i = 0 ; i < bytes.length ; i++) {
			buffer.append(HEX_DIGIT[((bytes[i] >> 4) & 0xF)]);
			buffer.append(HEX_DIGIT[(bytes[i] & 0xF)]);
		}
		
		return buffer.toString();
	}
	
	public static byte[] hex2Bytes(String hex) {
		byte[] bytes = new byte[hex.length()/2];
		
		for(int i = 0 ; i < hex.length() ; i+=2) {
			bytes[i/2] = (byte)( (Character.digit(hex.charAt(i), 16) << 4) + Character.digit(hex.charAt(i+1), 16) );
		}
		
		return bytes;
	}
	
	public static String capitalize(String text) {
		String tmp = text.toUpperCase();
		
		return tmp.charAt(0) + tmp.substring(1).toLowerCase();
	}

	public static String format(String format, Object... args){
		StringBuilder builder = new StringBuilder();
		String source = new String(format);				
		Integer beginIndex = 0, endIndex = 0, argIndex = 0;
		
		while(!source.isEmpty()){
			if( argIndex >= args.length ){
				builder.append(source);
				break;
			}
			
			if( 0 <= (endIndex = seekStringArg(source)) ){
				builder.append(source.substring(beginIndex, endIndex));
				builder.append(args[argIndex]);
				
				endIndex += ARG_STRING.length();
				argIndex++;
			}else if( 0 <= (endIndex = seekIntegerArg(source)) ){
				builder.append(source.substring(beginIndex, endIndex));
				builder.append(args[argIndex]);
				endIndex += ARG_INTEGER.length();
				argIndex++;
			}else{
				
			}
			
			if( 0 > endIndex ){
				builder.append(source.substring(0, source.length()));
				break;
			}
			
			source = source.substring(endIndex, source.length());			
		}
		
		return builder.toString();
	}
	
	private static Integer seekStringArg(String source){
		return source.indexOf(ARG_STRING);
	}
	
	private static Integer seekIntegerArg(String source){
		return source.indexOf(ARG_INTEGER);
	}
}
