/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * The data converter to convert the source {@link S} into {@link T} by a {@link ObjectBuilder} 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 * 
 * @param		T The type going to convert
 * @param		E The builder which constructs T
 * @param		S The source type to be converted into T by E.
 */
public interface DataConverter<T, E extends ObjectBuilder<?>, S> {
	void setReference(S reference);
	T construct(E builder);
}
