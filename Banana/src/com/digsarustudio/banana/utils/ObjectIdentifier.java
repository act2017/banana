/**
 * 
 */
package com.digsarustudio.banana.utils;

/**
 * The sub-type represents an identifier for the data object.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface ObjectIdentifier {
	/**
	 * 
	 * The builder for {@link ObjectIdentifier}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<ObjectIdentifier> {
		Builder setValue(String value);
	}
	
	void setValue(String value);
	String getValue();
	
	Boolean validate();
}
