/**
 * 
 */
package com.digsarustudio.banana.encryption;

/**
 * This exception indicates that fail to decrypt a string
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DecryptionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6719842188046592384L;

	/**
	 * 
	 */
	public DecryptionException() {

	}

	/**
	 * @param message
	 */
	public DecryptionException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public DecryptionException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public DecryptionException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DecryptionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
