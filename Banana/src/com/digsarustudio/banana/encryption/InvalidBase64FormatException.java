/**
 * 
 */
package com.digsarustudio.banana.encryption;

/**
 * This exception indicates the incoming string which doesn't carry a base64 string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class InvalidBase64FormatException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3371778396865849195L;

	/**
	 * 
	 */
	public InvalidBase64FormatException() {

	}

	/**
	 * @param message
	 */
	public InvalidBase64FormatException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public InvalidBase64FormatException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidBase64FormatException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidBase64FormatException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
