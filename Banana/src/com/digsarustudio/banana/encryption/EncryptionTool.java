/**
 * 
 */
package com.digsarustudio.banana.encryption;

import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.util.Base64;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import com.digsarustudio.banana.utils.StringFormatter;

/**
 * This class used for encrypt a plain text into a has string and decrypt it from a hash string.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class EncryptionTool {
	public class EncryptionAlgorithm{
		public static final String AES	= "AES";
	}
	
	private static final String	DEFAULT_SECURE_RAMDOM	= "SHA1PRNG";
	private static final String	DEFAULT_KEY_FACTORY		= "PBKDF2WithHmacSHA512";
	private static final String	DEFAULT_TRANSFORMATION	= "AES/CBC/PKCS5Padding";
	private static final String	DEFAULT_ALGORITHM		= "AES";
	private static final String	DEFAULT_SOURCE_SALT		= "dig.saru.studio";
	private static final String	DEFAULT_KEY				= "dig.saru.studio8";
	private static final Integer	DEFAULT_ITERATION_COUNT	= 4000;
	private static final Integer	DEFAULT_KEY_LENGTH		= 128;
	private static final String	DEFAULT_CHAR_SET			= "UTF-8";
	private static final String	BASE64_IV_DELIMITER		= ":";
	@SuppressWarnings("unused")
	private static final String	ADVAHNCE_HASH_PASSWORD_DELIMITER		= ":";

	private String	key				= DEFAULT_KEY;
	private String	algorithm		= DEFAULT_ALGORITHM;
	private String	transformation	= DEFAULT_TRANSFORMATION;
	private String	keyFactory		= DEFAULT_KEY_FACTORY;
	private Integer	iterationCount	= DEFAULT_ITERATION_COUNT;
	private Integer	keyLength		= DEFAULT_KEY_LENGTH;
	private String	charSet			= DEFAULT_CHAR_SET;
	
	/**
	 * 
	 */
	public EncryptionTool() {

	}
	
	/**
	 * To encrypt a password with the default salt and encoded into Base64
	 * 
	 * @param password The password to encrypt
	 * 
	 * @return The password encoded in Base64
	 * 
	 * @throws EncryptionException When failed to encrypt the password.
	 */
	public String encryptBase64Password(String password) throws EncryptionException {
		return this.encryptBase64Password(password, DEFAULT_SOURCE_SALT);
	}
	
	/**
	 * To encrypt a password with a salt and encoded into Base64
	 * 
	 * @param password The password to encrypt
	 * @param salt The salt for the password to encrypt
	 * 
	 * @return The password encoded in Base64
	 * @throws EncryptionException When failed to encrypt the password.
	 */
	public String encryptBase64Password(String password, String salt) throws EncryptionException {
		byte[] crypToText	= null;
		byte[] iv 			= null;
		
		try {
			SecretKeySpec secretKey = this.createSecretKey(this.key, salt, this.getIterationCount(), this.getKeyLength());
			
			Cipher pbeCipher = Cipher.getInstance(this.transformation);
			pbeCipher.init(Cipher.ENCRYPT_MODE, secretKey);
			
			AlgorithmParameters params = pbeCipher.getParameters();
			IvParameterSpec ivParamSpec = params.getParameterSpec(IvParameterSpec.class);
			
			crypToText = pbeCipher.doFinal(password.getBytes(this.charSet));
			iv = ivParamSpec.getIV();
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException 
				| InvalidKeyException | InvalidParameterSpecException | IllegalBlockSizeException 
				| BadPaddingException | UnsupportedEncodingException e) {
			throw new EncryptionException("Fail to encrypt base64 password", e);
		}		
		
		return toBase64String(iv) + BASE64_IV_DELIMITER + toBase64String(crypToText);
	}

	/**
	 * To decrypt a base64 encoded password with default salt to plain text
	 * 
	 * @param base64EncryptedPassword The password encrypted and encoded by base64
	 * @param salt The salt of password to decrypt.
	 * 
	 * @return The plain text password
	 * 
	 * @throws EncryptionException When failed to encrypt the password.
	 */
	public String decryptBase64Password(String base64EncryptedPassowrd) throws EncryptionException {
		return this.decryptBase64Password(base64EncryptedPassowrd, DEFAULT_SOURCE_SALT);
	}
	
	/**
	 * To decrypt a base64 encoded password to plain text
	 * 
	 * @param base64EncryptedPassword The password encrypted and encoded by base64
	 * 
	 * @return The plain text password
	 * 
	 * @throws EncryptionException When failed to encrypt the password.
	 */
	public String decryptBase64Password(String base64EncryptedPassword, String salt) throws EncryptionException {
		String rtn = null;		
		
		try {
			String[] tmp = base64EncryptedPassword.split(BASE64_IV_DELIMITER);
			if( null == tmp || tmp.length != 2 ) {
				throw new InvalidBase64FormatException(base64EncryptedPassword + " is not a base64 string.");
			}
			
			SecretKeySpec secretKey = this.createSecretKey(this.key, salt, this.getIterationCount(), this.getKeyLength());
			Cipher pbeCipher = Cipher.getInstance(this.transformation);
			pbeCipher.init(Cipher.DECRYPT_MODE, secretKey, new IvParameterSpec(fromBase64StringToByte(tmp[0])));
			rtn = new String(pbeCipher.doFinal(fromBase64StringToByte(tmp[1])), this.charSet);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | InvalidBase64FormatException 
			   | NoSuchPaddingException | InvalidKeyException | InvalidAlgorithmParameterException 
			   | UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
			
			throw new EncryptionException("Fail to decrypt base64 password", e);
		}
		
		return rtn;
	}
	
	/**
	 * To encrypt a plain text password into the hexadecimal format.<BR>
	 * <br>
	 * TODO Bug: The encrypted password cannot be decrypted by {@link #decryptHexadecimalPassword(String, String)}
	 * 
	 * @param password The password to encrypt
	 * @param salt The hexadecimal salt for the password to encrypt
	 * 
	 * @return The encrypted password
	 * 
	 * @throws EncryptionException When failed to encrypt the password. 
	 * 
	 */
	public String encryptHexadecimalPassword(String password, String salt) throws EncryptionException  {
		String rtn = null;
		byte[] byteSalt = StringFormatter.hex2Bytes(salt);
		try {
			SecretKeySpec secretKey = new SecretKeySpec(byteSalt, EncryptionAlgorithm.AES);
			
			Cipher cipher = Cipher.getInstance(EncryptionAlgorithm.AES);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey, cipher.getParameters());
			
			byte[] crypToText = cipher.doFinal(password.getBytes());
			
			rtn = StringFormatter.bytes2Hex(crypToText);
		} catch (NoSuchAlgorithmException | NoSuchPaddingException
			   | InvalidKeyException | IllegalBlockSizeException | BadPaddingException 
			   | InvalidAlgorithmParameterException e) {
			throw new EncryptionException("Fail to encrypt password", e);
		}
				
		return rtn;
	}
	
	/**
	 * To decrypt an encrypted password in the hexadecimal format.<br>
	 * <br>
	 * TODO Bug: Cannot decrypt encrypted password from {@link #encryptHexadecimalPassword(String, String)}.
	 * 
	 * @param encryptedPassword The encrypted password to decrypt
	 * @param salt The salt for encrypted password to decrypt
	 * 
	 * @return The plain text password
	 * 
	 * @throws EncryptionException When failed to decrypt the password.
	 */
	public String decryptHexadecimalPassword(String encryptedPassword, String salt) throws EncryptionException {
		String rtn = null;
		byte[] byteSalt = StringFormatter.hex2Bytes(salt);
		try {
			SecretKeySpec secretKey = new SecretKeySpec(byteSalt, EncryptionAlgorithm.AES);
			
			Cipher cipher = Cipher.getInstance(EncryptionAlgorithm.AES);
			
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			
			byte[] bytes = StringFormatter.hex2Bytes(encryptedPassword);
			
			rtn = new String(cipher.doFinal(bytes));
		} catch (NoSuchAlgorithmException | NoSuchPaddingException 
			   | InvalidKeyException | IllegalBlockSizeException 
			   | BadPaddingException e) {
			throw new EncryptionException("Fail to decrypt password", e);
		}
		
		return rtn;
	}
	
	/**
	 * To generate a byte salt by a string source
	 * 
	 * @param sourceSalt The source of the salt
	 * 
	 * @return The bytes salt
	 */
	public byte[] generateSalt(String sourceSalt) {
		return sourceSalt.getBytes();
	}	

	/**
	 * Returns a hashed password with its own salt
	 * 
	 * @param salt The salt to hash the password
	 * @param password The password to hash
	 * 
	 * @return A hashed password with its own salt
	 * 
	 * @throws EncryptionException If the hash password cannot be generated.
	 */
	public String hashPassword(String salt, String password) throws EncryptionException {
		byte[] byteSalt = StringFormatter.hex2Bytes(salt);
		PBEKeySpec spec = new PBEKeySpec(password.toCharArray(), byteSalt, this.iterationCount, this.keyLength);
		
		String rtn = null;
		try {
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(this.keyFactory);
			byte[] hashed = keyFactory.generateSecret(spec).getEncoded();
			
//			rtn = this.iterationCount.toString() + ADVAHNCE_HASH_PASSWORD_DELIMITER
//				+ salt + ADVAHNCE_HASH_PASSWORD_DELIMITER + StringFormatter.bytes2Hex(hashed);
			rtn = StringFormatter.bytes2Hex(hashed);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
			throw new EncryptionException("Fail to get secret key factory", e);
		}
		
		return rtn;
	}
	
	/**
	 * Returns a random hash salt string
	 * 
	 * @return A random hash salt string
	 * 
	 * @throws EncryptionException If failed to get secure random.
	 */
	public String getRandomHashSalt() throws EncryptionException {
		SecureRandom sr = null;
		try {
			sr = SecureRandom.getInstance(DEFAULT_SECURE_RAMDOM);
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("Fail to get random hash salt", e);
		}
		
		byte[] salt = new byte[16];
		sr.nextBytes(salt);
		
		return StringFormatter.bytes2Hex(salt);
	}
	
	/**
	 * Returns a Random AES Key
	 * 
	 * @return a Random AES Key
	 * 
	 * @throws EncryptionException When {@link NoSuchAlgorithmException} occurred.
	 */
	public static String generateAESKey() throws EncryptionException{
		KeyGenerator keyGen = null;
		try {
			keyGen = KeyGenerator.getInstance(EncryptionAlgorithm.AES);
		} catch (NoSuchAlgorithmException e) {
			throw new EncryptionException("Failed to generate AES key", e);
		}
		
		keyGen.init(128);
		SecretKey secretKey = keyGen.generateKey();
		
		return StringFormatter.bytes2Hex(secretKey.getEncoded());
	}
	
	/**
	 * Decode a encoded base64 string into an array of byte
	 * 
	 * @param text The encoded base 64 string to decode
	 * 
	 * @return An array of byte decoded from a base64 string
	 */
	public static byte[] fromBase64StringToByte(String text) {
		return Base64.getDecoder().decode(text);
	}
	
	/**
	 * Decode a encoded base64 string into a plain text string
	 * 
	 * @param text The encoded base 64 string to decode
	 * 
	 * @return A plain text string decoded from a base64 string
	 */
	public static String fromBase64StringToString(String text) {
		byte[] bytes = fromBase64StringToByte(text);
		return new String(bytes);
	}
	
	/**
	 * Encodes an array of bytes to a string encoded by Base64
	 * 
	 * @param bytes The array of bytes to encode
	 * 
	 * @return a string encoded by Base64
	 */
	public static String toBase64String(byte[] bytes) {
		return Base64.getEncoder().encodeToString(bytes);
	}
	
	/**
	 * Encodes a plain text to a string encoded by Base64
	 * 
	 * @param source The string to encode
	 * 
	 * @return a string encoded by Base64
	 */
	public static String toBase64String(String source) {
		return toBase64String(source.getBytes());
	}
	
	public static String fromBase64StringToHexadecimalString(String source) {
		byte[] decoded = EncryptionTool.fromBase64StringToByte(source);
		return StringFormatter.bytes2Hex(decoded);
	}
	
	public static String fromHexadecimalStringToBase64String(String source) {
		byte[] dehexed = StringFormatter.hex2Bytes(source);
		return EncryptionTool.toBase64String(dehexed);
	}
	
	/**
	 * To generate a default salt which is {@link #DEFAULT_SOURCE_SALT}
	 * 
	 * @return The bytes of {@link #DEFAULT_SOURCE_SALT}
	 */
	@SuppressWarnings("unused")
	private byte[] generateSalt() {
		return this.generateSalt(DEFAULT_SOURCE_SALT);
	}
	
	/**
	 * To create a secret key for the {@link #encrypt(String)} to encrypt a source
	 * 
	 * @param source The source to create a secret key
	 * @param salt The salt of the key
	 * @param iterationCount The count of iteration
	 * @param keyLen The length of the key
	 * 
	 * @return a secret key for the {@link #encrypt(String)} to encrypt a source
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeySpecException
	 */
	private SecretKeySpec createSecretKey(String source, String salt, Integer iterationCount, Integer keyLen) throws NoSuchAlgorithmException, InvalidKeySpecException {
		SecretKeyFactory factory = SecretKeyFactory.getInstance(this.keyFactory);
		
		PBEKeySpec keySpec = new PBEKeySpec(source.toCharArray(), salt.getBytes(), iterationCount, keyLen);
		SecretKey key = factory.generateSecret(keySpec);
		
		return new SecretKeySpec(key.getEncoded(), this.algorithm);		
	}

	/**
	 * @return the algorithm
	 */
	public String getAlgorithm() {
		return algorithm;
	}

	/**
	 * @param algorithm the algorithm to set
	 */
	public void setAlgorithm(String algorithm) {
		this.algorithm = algorithm;
	}

	/**
	 * @return the keyFactory
	 */
	public String getKeyFactory() {
		return keyFactory;
	}

	/**
	 * @param keyFactory the keyFactory to set
	 */
	public void setKeyFactory(String keyFactory) {
		this.keyFactory = keyFactory;
	}

	/**
	 * @return the iterationCount
	 */
	public Integer getIterationCount() {
		return iterationCount;
	}

	/**
	 * @param iterationCount the iterationCount to set
	 */
	public void setIterationCount(Integer iterationCount) {
		this.iterationCount = iterationCount;
	}

	/**
	 * @return the keyLength
	 */
	public Integer getKeyLength() {
		return keyLength;
	}

	/**
	 * @param keyLength the keyLength to set
	 */
	public void setKeyLength(Integer keyLength) {
		this.keyLength = keyLength;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the transformation
	 */
	public String getTransformation() {
		return transformation;
	}

	/**
	 * @param transformation the transformation to set
	 */
	public void setTransformation(String transformation) {
		this.transformation = transformation;
	}

	/**
	 * @return the charSet
	 */
	public String getCharSet() {
		return charSet;
	}

	/**
	 * @param charSet the charSet to set
	 */
	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}	
	
}
