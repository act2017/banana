/**
 * 
 */
package com.digsarustudio.banana.encryption;

/**
 * This exception indicates that failed to encrypt a string.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class EncryptionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5810742121587740033L;

	/**
	 * 
	 */
	public EncryptionException() {

	}

	/**
	 * @param message
	 */
	public EncryptionException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EncryptionException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EncryptionException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EncryptionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
