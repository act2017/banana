/**
 * 
 */
package com.digsarustudio.banana.cakemaker;

/**
 * Defining the operations for an object to make it has ability to be duplicated to a new object.
 * This works as same as clone(), but no exception thrown.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.0	Intend to be removed over 1.2.0
 */
public interface IDuplicatable {
	Object duplicate();
}
