/**
 * 
 */
package com.digsarustudio.banana.cakemaker;

/**
 * Defining the activations for a builder
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * @deprecated	1.0.0	Intend to be removed over 1.2.0
 */
public interface IBuilder<T> {
	
	/**
	 * Returns a built object
	 * 
	 * @return built object
	 * 
	 * @exception Thrown when it is failed to build.
	 */
	T build() throws Exception;
}
