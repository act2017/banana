/**
 * 
 */
package com.digsarustudio.banana.enumeration;

/**
 * Define the object can be operated with its abbreviation
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 * 
 * @deprecated
 */
public interface IAbbreviatable {
	/**
	 * Returns the abbreviation
	 * 
	 * @return The abbreviation
	 */
	public String getAbbreviation();	
}
