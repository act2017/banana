/**
 * 
 */
package com.digsarustudio.banana.enumeration;

import java.util.HashSet;

/**
 * The definition of user-defined enumerator class
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated
 */
public abstract class AbstractEnum implements IEnumeratable {
	/**
	 * The value of enumeration
	 */
	private String value;
	
	/**
	 * Constructs the enumeration
	 */
	public AbstractEnum(String value) {
		this.setValue(value);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.IEnumeratable#getValue()
	 */
	@Override
	public String getValue() {

		return this.value;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.IEnumeratable#fromValue(java.lang.String)
	 */
	@Override
	public <T extends IEnumeratable> T fromValue(String value) {

		if( null == value ){
			throw new NullPointerException("The value must be set.");
		}else if(value.length() <= 0){
			throw new IllegalArgumentException("The value of value must be set.");
		}
		

		T rtn = null;
		
		HashSet<T> values = this.getValues();
		for (T target : values) {
			T enumerator = target;
			
			if(!enumerator.getValue().equalsIgnoreCase(value))
				continue;
			
			rtn = enumerator;
			break;
		}
		
		return rtn;
	}	

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AbstractEnum)) {
			return false;
		}
		AbstractEnum other = (AbstractEnum) obj;
		if (value == null) {
			if (other.value != null) {
				return false;
			}
		} else if (!value.equals(other.value)) {
			return false;
		}
		return true;
	}

	/**
	 * Assign the value of an enumerator
	 * 
	 * @param value the value of an enumerator to set
	 */
	protected void setValue(String value) {
		if( null == value ){
			throw new NullPointerException("The value must be set.");
		}else if(value.length() <= 0){
			throw new IllegalArgumentException("The value of value must be set.");
		}
		
		this.value = value;
	}
	
	/**
	 * Returns the values of this enumerator
	 * 
	 * @return the values of this enumerator
	 */
	protected abstract <T extends IEnumeratable> HashSet<T> getValues();
	
	/**
	 * Returns the entity of the enumeration
	 * 
	 * @return The entity of the enumeration
	 */
	protected abstract <T extends IEnumeratable> T self();
}
