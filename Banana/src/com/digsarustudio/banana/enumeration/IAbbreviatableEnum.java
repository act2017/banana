/**
 * 
 */
package com.digsarustudio.banana.enumeration;

/**
 * Defining the target type can be an abbreviatable enumerator
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated
 */
public interface IAbbreviatableEnum extends IEnumeratable, IAbbreviatable {
	/**
	 * Returns the enumeration of abbreviation
	 * 
	 * @param abrreviation The abbreviation to get enumeration
	 * 
	 * @return the enumerator relative to the input abbreviation; null if nothing found 
	 */
	public <T extends IAbbreviatableEnum> T fromAbbreviation(String abbreviation);
}
