/**
 * 
 */
package com.digsarustudio.banana.enumeration;

/**
 * Defining the target type can be enumerator
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated
 */
public interface IEnumeratable {
	/**
	 * Returns the value of enumerator
	 * 
	 * @return The value of enumerator
	 */
	public String getValue();
	
	/**
	 * Returns the enumerator by referring to value
	 * 
	 * @param value The value of enumerator
	 * 
	 * @return The enumerator represents this value; null if the value is not found.
	 */
	public <T extends IEnumeratable> T fromValue(String value);
}
