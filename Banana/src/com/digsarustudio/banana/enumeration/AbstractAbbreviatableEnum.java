/**
 * 
 */
package com.digsarustudio.banana.enumeration;

import java.util.HashSet;

/**
 * The definition of user-defined enumeration which contains abbreviation attribute
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 * @deprecated
 */
public abstract class AbstractAbbreviatableEnum extends AbstractEnum implements IAbbreviatableEnum {

	/**
	 * The abbreviation of enumeration
	 */
	private String abbreviation;	

	/**
	 * The set of other abbreviations
	 */
	private final HashSet<String> otherAbbreviations = new HashSet<String>(); 
	
	/**
	 * Constructs the enumeration
	 */
	public AbstractAbbreviatableEnum(String value, String abbreviation) {
		super(value);
		this.setAbbreviation(abbreviation);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.IAbbreviatableEnum#fromAbbreviation(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public <T extends IAbbreviatableEnum> T fromAbbreviation(String abbreviation) {

		if( null == abbreviation ){
			throw new NullPointerException("The abbreviation must be set.");
		}else if(abbreviation.length() <= 0){
			throw new IllegalArgumentException("The value of abbreviation must be set.");
		}
				

		AbstractAbbreviatableEnum rtn = null;
		
		HashSet<AbstractAbbreviatableEnum> values = this.getValues();
		for (AbstractAbbreviatableEnum target : values) {
			AbstractAbbreviatableEnum enumerator = target;
			
			if( enumerator.getAbbreviation().equalsIgnoreCase(abbreviation) ){
				rtn = enumerator;
			}else{
				HashSet<String> otherAbbrs = enumerator.getOtherAbbreviations();
				for (String otherAbbr : otherAbbrs) {
					if( !otherAbbr.equalsIgnoreCase(abbreviation) )
						continue;
					
					rtn = enumerator;
					break;
				}
			}
			
			if( null == rtn ){
				continue;
			}
			
			break;
		}
		
		return (T) rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.IAbbreviatable#getAbbreviation()
	 */
	@Override
	public String getAbbreviation() {

		return this.abbreviation;
	}
	
	/**
	 * Returns the set of other abbreviations
	 * 
	 * @return the set of otherAbbreviations
	 */
	public HashSet<String> getOtherAbbreviations() {
		return this.otherAbbreviations;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((abbreviation == null) ? 0 : abbreviation.hashCode());
		result = prime * result + ((otherAbbreviations == null) ? 0 : otherAbbreviations.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!super.equals(obj)) {
			return false;
		}
		if (!(obj instanceof AbstractAbbreviatableEnum)) {
			return false;
		}
		AbstractAbbreviatableEnum other = (AbstractAbbreviatableEnum) obj;
		if (abbreviation == null) {
			if (other.abbreviation != null) {
				return false;
			}
		} else if (!abbreviation.equals(other.abbreviation)) {
			return false;
		}
		if (otherAbbreviations == null) {
			if (other.otherAbbreviations != null) {
				return false;
			}
		} else if (!otherAbbreviations.equals(other.otherAbbreviations)) {
			return false;
		}
		return true;
	}

	/**
	 * Assign the abbreviation of an enumerator
	 * 
	 * @param abbreviation the abbreviation of an enumerator to set
	 */
	protected void setAbbreviation(String abbreviation) {
		if( null == abbreviation ){
			throw new NullPointerException("The abbreviation must be set.");
		}else if(abbreviation.length() <= 0){
			throw new IllegalArgumentException("The value of abbreviation must be set.");
		}
		
		this.abbreviation = abbreviation;
	}
	
	/**
	 * Add other abbreviations if applicable
	 * 
	 * @param abbreviation The other abbreviations
	 * 
	 * @return The entity of the enumeration
	 */
	protected <T extends IAbbreviatableEnum> T addOtherAbbreviation(String abbreviation){
		if( null == abbreviation ){
			throw new NullPointerException("The abbreviation must be set.");
		}else if(abbreviation.length() <= 0){
			throw new IllegalArgumentException("The value of abbreviation must be set.");
		}
		
		this.otherAbbreviations.add(abbreviation);
		return self();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractEnum#getValues()
	 */
	@Override
	protected abstract <T extends IEnumeratable> HashSet<T> getValues();

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.AbstractEnum#self()
	 */
	@Override
	protected abstract <T extends IEnumeratable> T self();

}
