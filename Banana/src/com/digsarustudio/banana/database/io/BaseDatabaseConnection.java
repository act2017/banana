/**
 * 
 */
package com.digsarustudio.banana.database.io;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.digsarustudio.banana.database.DriverNotLoadedException;

/**
 * The base class of {@link DatabaseConnection}.<br>
 * All of the subclass of {@link DatabaseConnection} must inherit this class.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class BaseDatabaseConnection implements DatabaseConnection {	
	public final Integer DEFAULT_PORT		= 3306;
	
	private final String JDBC_URL_PREFIX 	= "jdbc:";
	private final String JDBC_URL_SUFFIX	= "://";
	
	
	protected String 	hostName				= null;
	protected Integer 	port					= null;
	protected String 	userName				= null;
	protected String 	password 				= null;
	protected String	databaseName			= null;
	protected String	instanceConnectionName	= null;
	
	private String		url			= null;
	private Connection	connection	= null;
	private Statement	statement	= null;
	
	private Logger logger	= Logger.getLogger(this.getClass().getName());

	/**
	 * @throws DriverNotLoadedException 
	 * 
	 */
	public BaseDatabaseConnection(String name, String drivePackageName) throws DriverNotLoadedException {
		this.url = String.format("%s%s%s", this.JDBC_URL_PREFIX, name, this.JDBC_URL_SUFFIX);

		try {
			this.loadDrive(drivePackageName);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			String msg = String.format("Fail to initiate database connection for %s. Error:%s"
												, drivePackageName, e.getMessage());
			
			logger.severe(msg);
			throw new DriverNotLoadedException(msg, e);			
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#setDatabaseName(java.lang.String)
	 */
	@Override
	public void setDatabaseName(String name) {
		this.databaseName = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#setInstanceConnectionName(java.lang.String)
	 */
	@Override
	public void setInstanceConnectionName(String name) {
		this.instanceConnectionName = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#open()
	 */
	@Override
	public void open() throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append(this.url);
		
		if(null != this.hostName) {
			builder.append(this.hostName);
		}
		
		if( null != this.port && null == this.instanceConnectionName){
			builder.append(":");
			builder.append(this.port.toString());
		}
		
		if(null != this.databaseName) {
			builder.append(this.databaseName);
		}
		
		if(null == this.instanceConnectionName) {
			logger.warning("No instance connection Name for " + this.getClass().getName());
		}else {
			builder.append("?cloudSqlInstance=");
			builder.append(this.instanceConnectionName);
			builder.append("&socketFactory=com.google.cloud.sql.mysql.SocketFactory");
			builder.append("&user=");
			builder.append(this.userName);
			builder.append("&password=");
			builder.append(this.password);
			builder.append("&useSSL=false");
		}
				
		try {
			if(null != this.userName && null != this.password && null == this.instanceConnectionName) {
				this.connection = DriverManager.getConnection(builder.toString(), this.userName, this.password);
			}else {
				this.connection = DriverManager.getConnection(builder.toString());
			}
			
			if(null != this.instanceConnectionName) {
				logger.info(this.instanceConnectionName + " has been connected");
			}else {
				logger.info(this.databaseName + " has been connected");
			}
			
			this.statement = this.connection.createStatement();
		} catch (SQLException e) {
			String msg = String.format("Fail to connect to %s. Please check the account name and password. Error = %s"
										, builder.toString(), e.getMessage());
			if(null != e.getCause()) {
				msg += "\nunderlying exception: " + e.getCause().getMessage();
				logger.logp(Level.SEVERE, this.getClass().getName(), "open", msg, e);
			}
			logger.severe(msg);
			throw new SQLManipulationException(msg, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#isConnected()
	 */
	@Override
	public Boolean isConnected() {
		return (null != this.connection && null != this.statement);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#close()
	 */
	@Override
	public void close() throws SQLManipulationException {
		try {
			if(null != this.statement) {			
				this.statement.close();
			}
			
			if(null != this.connection) {
				this.connection.close();
			}
			
			this.statement = null;
			this.connection = null;
		} catch (SQLException e) {
			String msg = String.format("Fail to close connection from %s.", this.url);
			logger.severe(msg);
			throw new SQLManipulationException(msg, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#executeUpdate(java.lang.String)
	 */
	@Override
	public void executeUpdate(String sqlString) throws SQLManipulationException {
		try {
			this.statement.executeUpdate(sqlString);
		} catch (SQLException e) {
			String msg = String.format("Fail to execute SQL string (%s).", sqlString);
			logger.severe(msg);
			throw new SQLManipulationException(msg, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#executeUpdateWithAutoIncrementColumn(java.lang.String)
	 */
	@Override
	public void executeUpdateWithAutoIncrementColumn(String sqlString) throws SQLManipulationException {
		try {
			this.statement.executeUpdate(sqlString, Statement.RETURN_GENERATED_KEYS);
		} catch (SQLException e) {
			String msg = String.format("Fail to execute SQL string (%s).", sqlString);
			logger.severe(msg);
			throw new SQLManipulationException(msg, e);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#executeQuery(java.lang.String)
	 */
	@Override
	public ResultBundle executeQuery(String sqlString) throws SQLManipulationException {
		ResultSet result = null;
		
		try {
			result = this.statement.executeQuery(sqlString);
		} catch (SQLException e) {
			String msg = String.format("Fail to execute SQL string (%s).", sqlString);
			logger.severe(msg);
			throw new SQLManipulationException(msg, e);
		}
		
		return ResultBundle.builder().setResultSet(result).build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseConnection#getLastAutoIncrement()
	 */
	@Override
	public Long getLastAutoIncrement() {
		Long rtn = null;
		ResultSet result = null;
		try {
			result = this.statement.getGeneratedKeys();
			
			if(result.next()) {
				rtn = result.getLong(1);
			}
		} catch (SQLException e) {
			String msg = String.format("Fail to get last generated auto-increment value. Err = %s.", e.getMessage());
			System.err.println(msg);
			logger.severe(msg);
		}finally {
			if(null != result) {
				try {
					result.close();
				} catch (SQLException e) {
					//Ignore
					logger.severe(e.getMessage());
				}
			}
		}
		
		return rtn;
	}

	/**
	 * Call this method under {@link #executeQuery(String)} of the subclass.
	 * 
	 * @param sqlString The SQL string to send
	 * 
	 * @return The query result
	 * 
	 * @throws SQLException
	 */
	protected ResultSet executeStatementQuery(String sqlString) throws SQLException{
		return this.statement.executeQuery(sqlString);		
	}
	
	protected void setHostName(String name){
		this.hostName = name;
	}
	
	/**
	 * @param port the port to set
	 */
	protected void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * @param userName the userName to set
	 */
	protected void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @param password the password to set
	 */
	protected void setPassword(String password) {
		this.password = password;
	}
	
	/**
	 * @return the hostName
	 */
	public String getHostName() {
		return hostName;
	}

	/**
	 * @return the port
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * To load the Driver Manager of MySQL
	 * 
	 * @param drivePackageName The name of the package to initiate.
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private void loadDrive(String drivePackageName) throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		Class.forName(drivePackageName).newInstance();
	}
}
