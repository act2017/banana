/**
 * 
 */
package com.digsarustudio.banana.database.io;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The manipulator of database
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface DatabaseManipulator {
	/**
	 * 
	 * The builder for {@link DatabaseManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static interface Builder extends ObjectBuilder<DatabaseManipulator> {
		/**
		 * Assigns a name of database for the creating, selecting, and deleting operation
		 * 
		 * @param name The name of database for the creating, selecting, and deleting operation
		 * 
		 * @return The instance of this builder
		 */
		Builder setName(String name);
		
		/**
		 * Assigns a character sets when going to create a database.
		 * 
		 * @param charset The character sets for the database to create.
		 * 
		 * @return The instance of this builder
		 */
		Builder setCharacterSets(CharacterSets charset);
		
		/**
		 * Assigns a collation when going to create a database.
		 * 
		 * @param collations The collation for the database to create.
		 * 
		 * @return The instance of this builder
		 */
		Builder setCollations(Collations collations);
		
		/**
		 * Assigns a connection of a particular database.<br>
		 * 
		 * @param connection The connection of a particular database.
		 * 
		 * @return The instance of this builder
		 */
		Builder setConnection(DatabaseConnection connection);
	}
	
	/**
	 * Assigns a connection of a particular database.<br>
	 * 
	 * @param connection The connection of a particular database.
	 */
	public void setConnection(DatabaseConnection connection);
	
	/**
	 * To create a particular database
	 * 
	 * @param name The name of database to create
	 * @param charset The character set of the database to create
	 * @param collations The collations of the database to create
	 * 
	 * @throws SQLManipulationException
	 */
	void createDatabase(String name, CharacterSets charset, Collations collations) throws SQLManipulationException;
	
	/**
	 * To select a particular database
	 * 
	 * @param name The name of the particular database to select
	 * 
	 * @throws SQLManipulationException
	 */
	void selectDatabase(String name) throws SQLManipulationException;
	
	/**
	 * To delete a particular database
	 * 
	 * @param name The name of database to delete
	 * 
	 * @throws SQLManipulationException
	 */
	void deleteDatabase(String name) throws SQLManipulationException;
	
	/**
	 * To create a database on the database server.
	 * Please set the name of database, character set, and collations before calling this function.
	 * 
	 * @throws SQLManipulationException
	 */
	void createDatabase() throws SQLManipulationException;
	
	/**
	 * To select an existing database on the database server.
	 * 
	 * Please set the name of database before calling this function.
	 * 
	 * @throws SQLManipulationException
	 */
	void selectDatabase() throws SQLManipulationException;
	
	/**
	 * To delete the assigned database
	 * 
	 * @throws SQLManipulationException
	 */
	void deleteDatabase() throws SQLManipulationException;
	
	/**
	 * Assigns a name of database for the creating, selecting, and deleting operation
	 * 
	 * @param name The name of database for the creating, selecting, and deleting operation
	 */
	void setName(String name);
	
	/**
	 * Assigns a character sets when going to create a database.
	 * 
	 * @param charset The character sets for the database to create.
	 */
	void setCharacterSets(CharacterSets charset);
	
	/**
	 * Assigns a collation when going to create a database.
	 * 
	 * @param collations The collation for the database to create.
	 */
	void setCollations(Collations collations);
	
	/**
	 * Returns the name of database in use.
	 * 
	 * @return the name of database in use.
	 */
	String getName();
	
	/**
	 * Returns the character sets
	 * 
	 * @return the character sets
	 */
	CharacterSets getCharacterSets();
	
	/**
	 * Returns the collations used for this database
	 * 
	 * @return the collations used for this database
	 */
	Collations getCollations();
	
	/**
	 * Returns true if the database is existing, otherwise false returned
	 * 
	 * @return true if the database is existing, otherwise false returned
	 * @throws SQLManipulationException 
	 */
	Boolean isDatabaseExisting() throws SQLManipulationException;

	/**
	 * Returns true if the database is existing, otherwise false returned
	 * 
	 * @param name The name of the database to check
	 * 
	 * @return true if the database is existing, otherwise false returned
	 * @throws SQLManipulationException 
	 */
	Boolean isDatabaseExisting(String name) throws SQLManipulationException;
	
	/**
	 * To open a connection
	 * 
	 * @throws SQLManipulationException When the connection cannot be established 
	 * 			due to invalid host name, user name, or password. 
	 */
	void open() throws SQLManipulationException;
	
	/**
	 * Returns true if the connection is opened, otherwise false returned.
	 * 
	 * @return true if the connection is opened, otherwise false returned.
	 */
	Boolean isConnected();
	
	/**
	 * To close the connection
	 * 
	 * @throws SQLManipulationException If the connection cannot be closed. 
	 */
	void close() throws SQLManipulationException;
	
	/**
	 * To send a query for a specific execution.
	 * 
	 * @param sqlString A query for a specific execution.
	 * 
	 * @throws SQLManipulationException If an error occurred. 
	 */
	void executeUpdate(String sqlString) throws SQLManipulationException;
	
	/**
	 * To send a insert query for a specific execution
	 * 
	 * @param sqlString A query for a specific execution
	 * 
	 * @throws SQLManipulationException if an error occurred.
	 */
	void executeUpdateWithAutoIncrementColumn(String sqlString) throws SQLManipulationException;
	
	/**
	 * Returns the results of a particular query.<br>
	 * 
	 * @param sqlString The query string to execute.
	 * 
	 * @return the results of a particular query.
	 * 
	 * @throws SQLManipulationException If an error occurred. 
	 */
	ResultBundle executeQuery(String sqlString) throws SQLManipulationException;
	
	/**
	 * Returns the last auto increment value.<br>
	 * Before use this function please use {@link #executeInsert(String)} to insert the data.<br>
	 * 
	 * @return The last auto increment value
	 */
	Long getLastAutoIncrement();
	
	String getSQLQueryString();
}
