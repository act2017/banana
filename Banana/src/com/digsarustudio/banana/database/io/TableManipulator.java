/**
 * 
 */
package com.digsarustudio.banana.database.io;

import java.sql.SQLException;

import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This manipulator is used to create, update, or delete a particular table.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface TableManipulator {
	/**
	 * 
	 * The builder for {@link TableManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static interface Builder extends ObjectBuilder<TableManipulator> {
		
		/**
		 * Assigns the database manipulator to operate a table operation.<br>
		 * 
		 * @param manipulator The database manipulator to operate a table operation.
		 * 
		 * @return The instance of this builder
		 */
		Builder setDatabaseManipulator(DatabaseManipulator manipulator);
		
		/**
		 * Assigns a table for manipulation
		 * 
		 * @param table The table to manipulate
		 * 
		 * @return The instance of this builder
		 */
		Builder setTable(Table table);
		
		/**
		 * Assigns a database engine type for the table creation.
		 * 
		 * @param type The type of database engine
		 * 
		 * @return The instance of this builder
		 * 
		 */
		Builder setDatabaseEngineType(DatabaseEngineType type);
		
		/**
		 * To prevent an error thrown from the database when inserting an existing table.<br>
		 * 
		 * @return The instance of this builder
		 */
		Builder setPreventErrorIfTableExists();
	}	
	
	/**
	 * Assigns a table for this manipulator to operate.
	 * 
	 * @param table The table for this manipulator to operate.
	 */
	void setTable(Table table);
	
	/**
	 * Returns the table of this manipulator
	 * 
	 * @return the table of this manipulator
	 */
	Table getTable();
	
	/**
	 * Assigns a database engine type for the table creation.
	 * 
	 * @param type The type of database engine
	 * 
	 * 
	 */
	void setDatabaseEngineType(DatabaseEngineType type);
	
	/**
	 * Returns the engine type this database used
	 * 
	 * @return the engine type this database used
	 */
	DatabaseEngineType getDatabaseEngineType();
	
	/**
	 * To prevent an error thrown from the database when inserting an existing table.<br>
	 * 
	 */
	void setPreventErrorIfTableExists();
	
	/**
	 * To show an error thrown from the database when inserting an existing table.<br>
	 * 
	 */
	void setNotPreventErrorIfTableExists();
	
	
	/**
	 * Returns true if this table has been set to prevent error if the target exists, otherwise false returned.
	 * 
	 * @return true if this table has been set to prevent error if the target exists, otherwise false returned.
	 */
	Boolean isPreventErrorIfTableExists();
	
	/**
	 * Create a table for database
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	void createTable() throws SQLManipulationException;
	
	/**
	 * To rename this table
	 * 
	 * @param name The original name for the table to rename
	 * @throws SQLManipulationException 
	 * 
	 */
	void renameTable(String originalName) throws SQLManipulationException;
	
	/**
	 * To rename this table
	 * 
	 * @param name The new details for original table
	 * @throws SQLManipulationException 
	 * 
	 */
	void renameTable(Table originalTable) throws SQLManipulationException;
	
	/**
	 * To add a column
	 * 
	 * @param column The details of the new column
	 * @throws SQLManipulationException 
	 * 
	 */
	void addColumn(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To modify the attributes of a column, excluding the name of column.<br>
	 * <br>
	 * Before calling this method, please ensure that key constraints are applied properly and the name of column is existed in the table, otherwise
	 * a {@link SQLException} will be thrown.<br>
	 * 
	 * @param column The new details of a specific column to setup.
	 * @throws SQLManipulationException 
	 * 
	 */
	void modifyColumn(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To change the name of target column.<br>
	 * Normally, {@link Table} contains the latest details of columns so that just indicating 
	 * which is the target to be modified as a parameter for this method.<br>
	 * <br>
	 * 
	 * @param originalColumnName The name of the original column. The name of this column should be specified.
	 * @param newColumn The details of the new column to set.
	 * @throws SQLManipulationException 
	 * 
	 */
	void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException;
	
	/**
	 * To change the name of target column.<br>
	 * 
	 * <br>
	 * 
	 * @param originalColumn The details of the original column. The name of this column should be specified.
	 * @param newColumn The details of the new column to set.
	 * @throws SQLManipulationException 
	 */
	void changeColumnName(TableColumn originalColumn, TableColumn newColumn) throws SQLManipulationException;
	
	/**
	 * Returns the details of a particular column from the database.<br>
	 * Only data type attribute works properly. Do not use this method 
	 * to retrieve the details of column from a particular table.
	 * <br>
	 * Note:<br>
	 * The key constraint is not really available now. Only primary keys can be parsed well, and
	 * non-null unique keys as well. The foreign keys cannot be recognized so far. It should be fixed if
	 * this method is used to retrieve the details of column from database.<br>
	 * <br>
	 * 
	 * @param name The name of the particular column to retrieve the details of it.
	 * 
	 * @return the details of a particular column from the database.
	 * @throws SQLManipulationException 
	 * 
	 */
	TableColumn getColumn(String name) throws SQLManipulationException;
	
	/**
	 * Returns true if the specific column exists, otherwise false returned.
	 * 
	 * @param name The name of the column to check
	 * 
	 * @return true if the specific column exists, otherwise false returned.
	 * @throws SQLManipulationException 
	 * 
	 */
	Boolean isColumnExisting(String name) throws SQLManipulationException;
	
	/**
	 * Returns true if the specific column exists, otherwise false returned.
	 * 
	 * @param column The column to check
	 * 
	 * @return true if the specific column exists, otherwise false returned.
	 * @throws SQLManipulationException 
	 * 
	 */
	Boolean isColumnExisting(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To register a column as a primary key
	 * 
	 * @param column The details of column
	 * @throws SQLManipulationException 
	 * 
	 */
	void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To remove all of the primary keys
	 * @throws SQLManipulationException 
	 * 
	 */
	void unregisterPrimaryKeys() throws SQLManipulationException;
	
	/**
	 * To register a column as a foreign key with the constraints.<br>
	 * 
	 * @param column The details of column
	 * @throws SQLManipulationException 
	 * 
	 */
	void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To remove foreign key constraint from a column.<br>
	 * 
	 * @param column The details of column
	 * @throws SQLManipulationException 
	 * 
	 */
	void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To register a column as a unique key
	 * 
	 * @param column The details of column
	 * @throws SQLManipulationException 
	 * 
	 */
	void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To remove unique key  from a column.<br>
	 * 
	 * @param column The details of column
	 * @throws SQLManipulationException 
	 * 
	 */
	void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To remove a column
	 * 
	 * @param column The details of column
	 * @throws SQLManipulationException 
	 * 
	 */
	void removeColumn(TableColumn column) throws SQLManipulationException;
	
	/**
	 * To remove a column
	 * 
	 * @param name The name of the column to be removed.
	 * @throws SQLManipulationException 
	 * 
	 */
	void removeColumn(String name) throws SQLManipulationException;
	
	/**
	 * Delete a table from database
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	void deleteTable() throws SQLManipulationException;

	/**
	 * Delete a table from database
	 * 
	 * @param name The name of table to delete.
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	void deleteTable(String name) throws SQLManipulationException;	
	
	/**
	 * Removing all of the data from the table
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	void clearTable() throws SQLManipulationException;

	/**
	 * Removing all of the data from the table
	 * 
	 * @param name The name of the table to clear
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	void clearTable(String name) throws SQLManipulationException;
	
	/**
	 * To remove the data and reset the auto increment setting of the table. The data removed by this method cannot 
	 * be rolled back.<br>
	 * @throws SQLManipulationException 
	 * 
	 */
	void resetTable() throws SQLManipulationException;
	
	/**
	 * To remove the data and reset the auto increment setting of the table. The data removed by this method cannot 
	 * be rolled back.<br>
	 * 
	 * @param name The name of the table to reset
	 * @throws SQLManipulationException 
	 * 
	 */
	void resetTable(String name) throws SQLManipulationException;
	
	/**
	 * Returns true if the target table exists, otherwise false returned
	 * 
	 * @return true if the target table exists, otherwise false returned
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	Boolean isTableExisting() throws SQLManipulationException;
	
	/**
	 * Returns true if the target table exists, otherwise false returned
	 * 
	 * @param name The name of table to check
	 * 
	 * @return true if the target table exists, otherwise false returned
	 * @throws SQLManipulationException 
	 * 
	 * @throws SQLException
	 */
	Boolean isTableExisting(String name) throws SQLManipulationException;
	
	/**
	 * Returns the total count of the rows in this table.
	 * 
	 * @return the total count of the rows in this table.
	 * @throws SQLManipulationException 
	 */
	Integer getTotalRowCount() throws SQLManipulationException;
	
	/**
	 * Returns the total count of the rows in this table.
	 * 
	 * @param name The name of table to get the total count of rows
	 * 
	 * @return the total count of the rows in this table.
	 * @throws SQLManipulationException 
	 */
	Integer getTotalRowCount(String name) throws SQLManipulationException;
	
	/**
	 * Returns true if the target table contains no data, otherwise false.
	 * 
	 * @return true if the target table contains no data, otherwise false.
	 * 
	 * @throws SQLManipulationException
	 */
	Boolean isEmpty() throws SQLManipulationException;
	
	/**
	 * Returns true if the target table contains no data, otherwise false.
	 * 
	 * @param name The name of table to get the total count of rows
	 * 
	 * @return true if the target table contains no data, otherwise false.
	 * 
	 * @throws SQLManipulationException
	 */
	Boolean isEmpty(String name) throws SQLManipulationException;
	
	String getSQLQueryString();
}
