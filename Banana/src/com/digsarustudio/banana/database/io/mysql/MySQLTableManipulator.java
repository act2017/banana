/**
 * 
 */
package com.digsarustudio.banana.database.io.mysql;

import java.util.List;

import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.io.BaseTableManipulator;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.ResultBundle;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.io.TableManipulator;
import com.digsarustudio.banana.database.table.ColumnDataType;
import com.digsarustudio.banana.database.table.DefaultValues;
import com.digsarustudio.banana.database.table.ForeignKeyConstraint;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnAttributes;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;
import com.digsarustudio.banana.database.table.UnSupportedColumnDataType;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The table manipulator for MySQL database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLTableManipulator extends BaseTableManipulator implements TableManipulator {
	/**
	 * 
	 * The object builder for {@link MySQLTableManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements TableManipulator.Builder {
		private MySQLTableManipulator result = null;

		public Builder() {
			this.result = new MySQLTableManipulator();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.TableManipulator.Builder#setDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)
		 */
		@Override
		public Builder setDatabaseManipulator(DatabaseManipulator manipulator) {
			this.result.setDatabaseManipulator(manipulator);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.TableManipulator.Builder#setTable(com.digsarustudio.banana.database.table.Table)
		 */
		@Override
		public Builder setTable(Table table) {
			this.result.setTable(table);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.TableManipulator.Builder#setDatabaseEngineType(com.digsarustudio.banana.database.DatabaseEngineType)
		 */
		@Override
		public Builder setDatabaseEngineType(DatabaseEngineType type) {
			this.result.setDatabaseEngineType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.TableManipulator.Builder#setPreventErrorIfTableExists()
		 */
		@Override
		public Builder setPreventErrorIfTableExists() {
			this.result.setPreventErrorIfTableExists();
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public TableManipulator build() {
			return this.result;
		}
	}
	
	/**
	 * 
	 */
	private MySQLTableManipulator() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#createTable()
	 */
	@Override
	public void createTable() throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("CREATE TABLE");
		
		if(this.isPreventingErrorIfTableExists){
			builder.append(" IF NOT EXISTS");
		}
		
		builder.append(StringFormatter.format(" `%s` (", this.table.getName()));
		builder.append(StringFormatter.format("%s", this.getColumnNamesString(this.table)));
		
		if( this.table.hasPrimaryKey() ){
			builder.append(StringFormatter.format(", PRIMARY KEY(%s)", this.getPrimaryKeysString(this.table)));
		}
		
		if( this.table.hasForeignKey() ){
			builder.append(StringFormatter.format(", %s", this.getForeignKeysString(this.table)));
		}
		
		if( this.table.hasUniqueKey() ){
			builder.append(StringFormatter.format(", %s", this.getUniqueKeyString(this.table)));
		}
		
		if( this.table.isSupportFullTextSearch() ){
			builder.append(StringFormatter.format(", FULLTEXT (%s)", this.getFullTextColumnString(this.table)));
		}
		
		builder.append(")");
		
		if(null != this.engineType){
			builder.append(" ENGINE=");
			builder.append(this.engineType.getValue());
		}
		
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());	
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#renameTable(java.lang.String)
	 */
	@Override
	public void renameTable(String originalName) throws SQLManipulationException {
		//ALTER TABLE t1 RENAME t2;
		StringBuilder builder = new StringBuilder();
		
		builder.append("ALTER TABLE");
		builder.append(StringFormatter.format(" `%s` RENAME `%s`", originalName, this.table.getName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#addColumn(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void addColumn(TableColumn column) throws SQLManipulationException {
		// ALTER TABLE t2 ADD d TIMESTAMP;
		// ALTER TABLE t2 ADD c INT UNSIGNED NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (c);
		StringBuilder builder = new StringBuilder();
		
		builder.append("ALTER TABLE ");
		builder.append(this.table.getName());
		builder.append(" ADD ");
		builder.append(this.getColumnDefinition(column));
		
		if( column.isPrimaryKey() ) {
			builder.append( StringFormatter.format(", ADD PRIMARY KEY (`%s`)", column.getName()) );
		}
		
		if( column.isForeignKey() ) {
			builder.append( StringFormatter.format(", ADD %s", this.getForeignKeyDefinition(column)) );
		}
		
		if( column.isUniqueKey() ) {
			builder.append( StringFormatter.format(", ADD UNIQUE KEY (`%s`)", column.getName()) );
		}
		
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#modifyColumn(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void modifyColumn(TableColumn column) throws SQLManipulationException {
		 // ALTER TABLE t2 MODIFY a TINYINT NOT NULL;
		// ALTER TABLE t2 CHANGE a a  TINYINT NOT NULL;		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(" MODIFY");
		builder.append(" ");
		builder.append(this.getColumnDefinition(column));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#changeColumnName(java.lang.String, com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLManipulationException {
		// ALTER TABLE t2 CHANGE b c COLUMN_DEFINITION;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" CHANGE `%s` `%s`", originalColumnName, newColumn.getName()));
		builder.append(" ");
		builder.append(this.getColumnDefinition(newColumn));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#getColumn(java.lang.String)
	 */
	@Override
	public TableColumn getColumn(String name) throws SQLManipulationException {
	//	SHOW [FULL] {COLUMNS | FIELDS}
	//    {FROM | IN} tbl_name
	//    [{FROM | IN} db_name]
	//    [LIKE 'pattern' | WHERE expr]
	    		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("SHOW COLUMNS FROM `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" LIKE '%s'", name));
		builder.append(";");
		
		ResultBundle result = this.dbManipulator.executeQuery(builder.toString());
		
		//It should be one result returned.
		if( !result.next() ) {
			result.release();
			return null;
		}				
		
		/*		
		SHOW COLUMNS displays the following values for each table column:
		Field
			The column name.
	
		Type
			The column data type.
	
		Collation
			The collation for nonbinary string columns, or NULL for other columns. This value is displayed only if you use the FULL keyword.
	
		Null
			Column nullability. The value is YES if NULL values can be stored in the column, NO if not.
	
		Key
			Whether the column is indexed:
			If Key is empty, the column either is not indexed or is indexed only as a secondary column in a multiple-column, nonunique index.
			If Key is PRI, the column is a PRIMARY KEY or is one of the columns in a multiple-column PRIMARY KEY.
			If Key is UNI, the column is the first column of a UNIQUE index. (A UNIQUE index permits multiple NULL values, but you can tell whether the column permits NULL by checking the Null field.)
			If Key is MUL, the column is the first column of a nonunique index in which multiple occurrences of a given value are permitted within the column.
			If more than one of the Key values applies to a given column of a table, Key displays the one with the highest priority, in the order PRI, UNI, MUL.
	
			A UNIQUE index may be displayed as PRI if it cannot contain NULL values and there is no PRIMARY KEY in the table. 
			A UNIQUE index may display as MUL if several columns form a composite UNIQUE index; although the combination of 
			the columns is unique, each column can still hold multiple occurrences of a given value.
	
		Default
			The default value for the column. This is NULL if the column has an explicit default of NULL, or if the column definition includes no DEFAULT clause.
	
		Extra
			Any additional information that is available about a given column. The value is nonempty in these cases: 
			auto_increment for columns that have the AUTO_INCREMENT attribute; 
			on update CURRENT_TIMESTAMP for TIMESTAMP or 
			DATETIME columns that have the ON UPDATE CURRENT_TIMESTAMP attribute.
	
		Privileges
			The privileges you have for the column. This value is displayed only if you use the FULL keyword.
	
		Comment
			Any comment included in the column definition. This value is displayed only if you use the FULL keyword.		
	*/		
		TableColumn rtn = TableColumn.builder().setName(result.getString("Field")).build();
		
		String dataType = result.getString("Type");
		try {
			ColumnDataType columnDataType = new ColumnDataType(dataType);
			
			rtn.setDataType(columnDataType.getDataType());
			rtn.setLength(columnDataType.getLength());
			rtn.setPrecisionLength(columnDataType.getSupplementaryLength());
			
			List<String> enums = columnDataType.getEnumerations();
			if(null != enums){
				for (String en : enums) {
					rtn.addEnum(en);
				}
			}
		} catch (UnSupportedColumnDataType e) {
			e.printStackTrace();
		}
		
		if( result.getString("Null").equals("YES") ){
			rtn.setNullable();
		}else{
			rtn.setNotNullable();
		}
		
		/*
		 * Note:<br>
		 * The key constraint is not really available now. Only primary keys can be parsed well, and
		 * non-null unique keys as well. The foreign keys cannot be recognized so far. It should be fixed if
		 * this method is used to retrieve the details of column from database.<br>
		 * <br>
		 * TODO Trying to recognize key constraints.
		 */
		switch(result.getString("Key")){
			case "PRI":
				rtn.setAsPrimaryKey();
				break;
				
			case "UNI":
				rtn.setAsUniqueKey();
				break;
			
			//The foreign key will go through here
			case "MUL":
				break;
		}
		
		String defaultValue = result.getString("Default");
		if( null != defaultValue ){
			if( defaultValue.equals("NULL") && !rtn.isNullable() ){
				rtn.setNotNullable();
			}else if( defaultValue.equals("NULL") && rtn.isNullable() ){
				rtn.setNullable();
			}else{
				rtn.setDefaultValue(defaultValue);			
			}
		}
	
		result.release();
		return rtn;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#registerColumnAsPrimaryKey(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void registerColumnAsPrimaryKey(TableColumn column) throws SQLManipulationException {
		//ALTER TABLE t2 ADD PRIMARY KEY (c);
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" ADD PRIMARY KEY (`%s`)", column.getName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#unregisterPrimaryKeys()
	 */
	@Override
	public void unregisterPrimaryKeys() throws SQLManipulationException {
		//ALTER TABLE mytable DROP PRIMARY KEY
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s` DROP PRIMARY KEY", this.table.getName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#registerColumnAsForeignKey(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void registerColumnAsForeignKey(TableColumn column) throws SQLManipulationException {
//		ALTER TABLE tbl_name
//    ADD [CONSTRAINT [symbol]] FOREIGN KEY
//    [index_name] (index_col_name, ...)
//    REFERENCES tbl_name (index_col_name,...)
//    [ON DELETE reference_option]
//    [ON UPDATE reference_option]		
	StringBuilder builder = new StringBuilder();
	
	builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
	builder.append(StringFormatter.format(" ADD %s", this.getForeignKeyDefinition(column)));
	builder.append(";");
	
	this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#unregisterColumnFromForeignKey(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void unregisterColumnFromForeignKey(TableColumn column) throws SQLManipulationException {
		// ALTER TABLE tbl_name DROP FOREIGN KEY fk_symbol;		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP FOREIGN KEY `%s`", column.getForeignKeyConstraint().getName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#registerColumnAsUniqueKey(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void registerColumnAsUniqueKey(TableColumn column) throws SQLManipulationException {
		// ALTER TABLE sales.order ADD UNIQUE(order_)
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(" ADD UNIQUE");
		if( null != column.getIndexName() ){
			builder.append(StringFormatter.format(" `%s`", column.getIndexName()));
		}
		builder.append(StringFormatter.format(" (`%s`)", column.getName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#unregisterColumnFromUniquekKey(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLManipulationException {
		// alter table TABLE drop INDEX email;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP INDEX `%s`", column.getIndexName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#removeColumn(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void removeColumn(TableColumn column) throws SQLManipulationException {
		// ALTER TABLE t2 DROP COLUMN c, DROP COLUMN d;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP COLUMN `%s`", column.getName()));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#removeColumn(java.lang.String)
	 */
	@Override
	public void removeColumn(String name) throws SQLManipulationException {
		// ALTER TABLE t2 DROP COLUMN c, DROP COLUMN d;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP COLUMN `%s`", name));
		builder.append(";");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#deleteTable(java.lang.String)
	 */
	@Override
	public void deleteTable(String name) throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append(StringFormatter.format("DROP TABLE `%s`;", name));
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#clearTable(java.lang.String)
	 */
	@Override
	public void clearTable(String name) throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("DELETE FROM ");
		builder.append(" `");
		builder.append(name);
		builder.append("`;");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#resetTable(java.lang.String)
	 */
	@Override
	public void resetTable(String name) throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("TRUNCATE TABLE");
		builder.append(" `");
		builder.append(name);
		builder.append("`;");
		
		this.dbManipulator.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isTableExisting(java.lang.String)
	 */
	@Override
	public Boolean isTableExisting(String name) throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		Boolean isExisting = false;
		
//Bug: Exception thrown if the table doesn't exist.		
//		builder.append(StringFormatter.format("SELECT * FROM `%s` LIMIT 1;", name));
		builder.append(StringFormatter.format("SHOW TABLES LIKE '%s';", name));
		
		ResultBundle results = null;
		try{		
			results = this.dbManipulator.executeQuery(builder.toString());
			isExisting = results.next();			
		}finally{
			if( null != results ){
				results.release();;
			}
		}		
		
		return isExisting;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#getTotalRowCount()
	 */
	@Override
	public Integer getTotalRowCount() throws SQLManipulationException {
		return this.getTotalRowCount(this.table.getName());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#getTotalRowCount(java.lang.String)
	 */
	@Override
	public Integer getTotalRowCount(String name) throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append(StringFormatter.format("SELECT COUNT(*) AS `total` FROM `%s`;", name));
		
		Integer total = 0;
		ResultBundle results = null;
		try{		
			results = this.dbManipulator.executeQuery(builder.toString());
			
			if(results.next()){
				total = Integer.parseInt(results.getString("total"));
			}
		}finally{
			if( null != results ){
				results.release();;
			}
		}
		
		return total;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isEmpty()
	 */
	@Override
	public Boolean isEmpty() throws SQLManipulationException {
		return this.isEmpty(this.table.getName());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isEmpty(java.lang.String)
	 */
	@Override
	public Boolean isEmpty(String name) throws SQLManipulationException {
		return (0 == this.getTotalRowCount(name));
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getColumnNamesString(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	protected String getColumnNamesString(Table table) {
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count)) ){
			if(0 != count){
				builder.append(", ");
			}
			
			builder.append(this.getColumnDefinition(column));
			count++;
		}
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getDefinitions(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	protected String getDefinitions(TableColumn column) {
		StringBuilder builder = new StringBuilder();
		
		TableColumnDataTypes dataType = column.getDataType();			
		String dataTypeString = null;
		if( TableColumnDataTypes.Enum == dataType ){
			dataTypeString = getEnumString(column.getEnums());
		}else{
			if( null != column.getPrecisionLength() ){
				dataTypeString = StringFormatter.format(dataType.toString(), column.getLength(), column.getPrecisionLength());
			}else if(null != column.getLength()){
				dataTypeString = StringFormatter.format(dataType.toString(), column.getLength());
			}else{
				dataTypeString = dataType.getName();
			}
		}			
		builder.append(StringFormatter.format(" %s", dataTypeString));
		
		//sign/unsign
		if( column.isUnsignedColumn() ) {
			builder.append(" UNSIGNED");
		}
		
		builder.append(" ");
		if( column.isNullable() ){				
			builder.append(TableColumnAttributes.CanBeNull.getValue());
		}else{
			builder.append(TableColumnAttributes.CannotBeNull.getValue());
		}
		
		if( column.hasDefaultValue() ){
			builder.append(" DEFAULT ");
			
			String defaultValue = column.getDefaultValue().trim();
			if(defaultValue.equals(DefaultValues.Null.getValue()) || defaultValue.equals(DefaultValues.False.getValue()) 
			|| defaultValue.equals(DefaultValues.True.getValue()) || defaultValue.equals(DefaultValues.CurrentTimestamp.getValue())) {
				builder.append(column.getDefaultValue());
			}else {
				builder.append(StringFormatter.format("'%s'", column.getDefaultValue()));
			}
			
			
		}
		
		//On update current timestamp
		if( column.isOnUpdateCurrentTimestamp() ){
			builder.append(" ON UPDATE ");
			builder.append(DefaultValues.CurrentTimestamp.getValue());
		}
		
		if( column.isAutoIncrement() ){
			builder.append(StringFormatter.format(" %s", TableColumnAttributes.AutoIncrement.getValue()));
		}
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getColumnDefinition(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	protected String getColumnDefinition(TableColumn column) {
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("`%s`", column.getName()));
		builder.append(this.getDefinitions(column));		
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getPrimaryKeysString(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	protected String getPrimaryKeysString(Table table) {
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, pkCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isPrimaryKey()){
				continue;
			}
			
			if(0 != pkCount++){
				builder.append(", ");
			}
			
			builder.append(column.getName());
		}		
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getForeignKeysString(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	protected String getForeignKeysString(Table table) {
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, keyCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isForeignKey()){
				continue;
			}
			
			if(0 != keyCount++){
				builder.append(", ");
			}
			
			builder.append(this.getForeignKeyDefinition(column));
		}		
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getForeignKeyDefinition(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	protected String getForeignKeyDefinition(TableColumn column) {
		StringBuilder builder = new StringBuilder();
		
		ForeignKeyConstraint foreignKeyConstraint = column.getForeignKeyConstraint();		
		if( foreignKeyConstraint.getName() != null && !foreignKeyConstraint.getName().isEmpty() ) {
			builder.append(StringFormatter.format("CONSTRAINT `%s` ", foreignKeyConstraint.getName()));
		}
		
		builder.append(StringFormatter.format("FOREIGN KEY (`%s`) REFERENCES `%s`(`%s`)", column.getName()
																				   , foreignKeyConstraint.getReferentialTable()
																				   , foreignKeyConstraint.getReferentialPrimaryKey()));
		if( null != foreignKeyConstraint.getDeleteReferentialActions() ){
			builder.append(StringFormatter.format(" ON DELETE %s", foreignKeyConstraint.getDeleteReferentialActions().getValue()));
		}
		
		if( null != foreignKeyConstraint.getUpdateReferentialActions()){
			builder.append(StringFormatter.format(" ON UPDATE %s", foreignKeyConstraint.getUpdateReferentialActions().getValue()));
		}		

		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getUniqueKeyString(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	protected String getUniqueKeyString(Table table) {
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, keyCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isUniqueKey()){
				continue;
			}
			
			if(0 != keyCount++){
				builder.append(", ");
			}
			
			builder.append("UNIQUE KEY");
			
			if( null != column.getIndexName() ){
				builder.append(StringFormatter.format(" `%s`", column.getIndexName()));
			}
			
			builder.append(StringFormatter.format(" (`%s`)",  column.getName()));
		}		
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getFullTextColumnString(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	protected String getFullTextColumnString(Table table) {
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, keyCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isSupportFullTextSearch()){
				continue;
			}
			
			if(0 != keyCount++){
				builder.append(",");
			}
			
			builder.append(column.getName());
		}		
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseTableManipulator#getEnumString(java.util.List)
	 */
	@Override
	protected String getEnumString(List<String> enums) {
		StringBuilder builder = new StringBuilder();		
		Integer count = 0;
		
		for (String value : enums) {
			if( 0 != count++ ){
				builder.append(",");
			}
			
			builder.append(StringFormatter.format("'%s'", value));
		}
		
		return StringFormatter.format("%s(%s)", TableColumnDataTypes.Enum.getName(), builder.toString());
	}

}
