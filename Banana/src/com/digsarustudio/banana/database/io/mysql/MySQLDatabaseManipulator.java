/**
 * 
 */
package com.digsarustudio.banana.database.io.mysql;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.io.BaseDatabaseManipulator;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.ResultBundle;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The database manipulator for MySQL server.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLDatabaseManipulator extends BaseDatabaseManipulator implements DatabaseManipulator {
	/**
	 * 
	 * The object builder for {@link MySQLDatabaseManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements DatabaseManipulator.Builder {
		private MySQLDatabaseManipulator result = null;

		public Builder() {
			this.result = new MySQLDatabaseManipulator();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseManipulator.Builder#setConnection(com.digsarustudio.banana.database.io.DatabaseConnection)
		 */
		@Override
		public Builder setConnection(DatabaseConnection connection) {
			this.result.setConnection(connection);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseManipulator.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseManipulator.Builder#setCharacterSets(com.digsarustudio.banana.database.CharacterSets)
		 */
		@Override
		public Builder setCharacterSets(CharacterSets charset) {
			this.result.setCharacterSets(charset);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseManipulator.Builder#setCollations(com.digsarustudio.banana.database.Collations)
		 */
		@Override
		public Builder setCollations(Collations collations) {
			this.result.setCollations(collations);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DatabaseManipulator build() {
			return this.result;
		}
	}
	
	/**
	 * 
	 */
	private MySQLDatabaseManipulator() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#createDatabase(java.lang.String, com.digsarustudio.banana.database.CharacterSets, com.digsarustudio.banana.database.Collations)
	 */
	@Override
	public void createDatabase(String name, CharacterSets charset, Collations collations) throws SQLManipulationException {
		this.setName(name);
		this.setCharacterSets(charset);
		this.setCollations(collations);
		
		this.createDatabase();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#selectDatabase(java.lang.String)
	 */
	@Override
	public void selectDatabase(String name) throws SQLManipulationException {
		this.setName(name);
		this.selectDatabase();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#deleteDatabase(java.lang.String)
	 */
	@Override
	public void deleteDatabase(String name) throws SQLManipulationException {
		this.setName(name);
		
		this.deleteDatabase();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#createDatabase()
	 */
	@Override
	public void createDatabase() throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("CREATE DATABASE");
		
		if(this.isToCreateIfNotExist){
			builder.append(" IF NOT EXISTS");
		}
		
		builder.append(StringFormatter.format(" %s ", this.databaseName));
		
		if(this.isDefaultCharSet){
			builder.append(" DEFAULT");
		}		
		builder.append(StringFormatter.format(" CHARACTER SET = %s", this.charSet.getValue()));
		
		if(this.isDefaultCollation){
			builder.append(" DEFAULT");
		}
		builder.append(StringFormatter.format(" COLLATE = %s", this.collation.getValue()));
		builder.append(";");
		
		this.connection.executeUpdate(builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#selectDatabase()
	 */
	@Override
	public void selectDatabase() throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("USE");
		builder.append(StringFormatter.format(" %s", this.databaseName));
		builder.append(";");
		
		this.connection.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#deleteDatabase()
	 */
	@Override
	public void deleteDatabase() throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("DROP DATABASE");
		
		if(this.isToDropIfExists){
			builder.append(" IF EXISTS");
		}
		
		builder.append(StringFormatter.format(" %s", this.databaseName));
		builder.append(";");
		
		this.connection.executeUpdate(builder.toString());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#isDatabaseExisting()
	 */
	@Override
	public Boolean isDatabaseExisting() throws SQLManipulationException {
		return this.isDatabaseExisting(this.databaseName);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#isDatabaseExisting(java.lang.String)
	 */
	@Override
	public Boolean isDatabaseExisting(String name) throws SQLManipulationException {
		StringBuilder builder = new StringBuilder();
		builder.append("SHOW DATABASES LIKE '");
		builder.append(name);
		builder.append("';");
		
		ResultBundle result = this.connection.executeQuery(builder.toString());

		return result.next();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
