/**
 * 
 */
package com.digsarustudio.banana.database.io.mysql;

import java.util.logging.Logger;

import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.BaseDatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseConnection;

/**
 * The implementation of {@link DatabaseConnection} for MySQL database server.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLConnection extends BaseDatabaseConnection implements DatabaseConnection {
	private Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 * 
	 * The object builder for {@link DatabaseConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements DatabaseConnection.Builder {
		private MySQLConnection result = null;

		public Builder() throws DriverNotLoadedException {
			this.result = new MySQLConnection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setHostName(java.lang.String)
		 */
		@Override
		public Builder setHostName(String name) {
			this.result.setHostName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setPort(java.lang.Integer)
		 */
		@Override
		public Builder setPort(Integer port) {
			this.result.setPort(port);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setUserName(java.lang.String)
		 */
		@Override
		public Builder setUserName(String name) {
			this.result.setUserName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setPassword(java.lang.String)
		 */
		@Override
		public Builder setPassword(String password) {
			this.result.setPassword(password);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setDatabaseName(java.lang.String)
		 */
		@Override
		public Builder setDatabaseName(String name) {
			this.result.setDatabaseName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setInstanceConnectionName(java.lang.String)
		 */
		@Override
		public Builder setInstanceConnectionName(String name) {
			this.result.setInstanceConnectionName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MySQLConnection build() {
			return this.result;
		}

	}
	
	private static final String DRIVER_PACKAGE_NAME= "com.mysql.jdbc.Driver";
	private static final String DATABASE_NAME		= "mysql";

	/**
	 * @throws DriverNotLoadedException If this connection cannot be established.<br> 
	 * 
	 */
	private MySQLConnection() throws DriverNotLoadedException {
		super(DATABASE_NAME, DRIVER_PACKAGE_NAME);
		
		logger.info("To initial MySQL connection...");
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() throws DriverNotLoadedException {
		return new Builder();
	}
}
