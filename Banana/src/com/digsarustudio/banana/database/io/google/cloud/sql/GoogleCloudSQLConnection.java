/**
 * 
 */
package com.digsarustudio.banana.database.io.google.cloud.sql;

import java.util.logging.Logger;

import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.BaseDatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseConnection;

/**
 * The implementation of {@link DatabaseConnection} for Cloud SQL database server.<br>
 * <br>
 * For Java 8:<br>
 * jdbc:mysql://google/<DATABASE_NAME>?cloudSqlInstance=<INSTANCE_CONNECTION_NAME>
 * 									  &socketFactory=com.google.cloud.sql.mysql.SocketFactory
 * 									  &user=<MYSQL_USER_NAME>
 * 									  &password=<MYSQL_USER_PASSWORD>
 * 									  &useSSL=false<br>
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class GoogleCloudSQLConnection extends BaseDatabaseConnection implements DatabaseConnection {
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 * 
	 * The object builder for {@link DatabaseConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements DatabaseConnection.Builder {
		private GoogleCloudSQLConnection result = null;

		public Builder() throws DriverNotLoadedException {
			this.result = new GoogleCloudSQLConnection();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setHostName(java.lang.String)
		 */
		@Override
		public Builder setHostName(String name) {
			this.result.setHostName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setPort(java.lang.Integer)
		 */
		@Override
		public Builder setPort(Integer port) {
			this.result.setPort(port);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setUserName(java.lang.String)
		 */
		@Override
		public Builder setUserName(String name) {
			this.result.setUserName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setPassword(java.lang.String)
		 */
		@Override
		public Builder setPassword(String password) {
			this.result.setPassword(password);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public GoogleCloudSQLConnection build() {
			return this.result;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setDatabaseName(java.lang.String)
		 */
		@Override
		public Builder setDatabaseName(String name) {
			this.result.setDatabaseName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.io.DatabaseConnection.Builder#setInstanceConnectionName(java.lang.String)
		 */
		@Override
		public Builder setInstanceConnectionName(String name) {
			this.result.setInstanceConnectionName(name);
			return this;
		}

	}
	
	private static final String DRIVER_PACKAGE_NAME= "com.mysql.jdbc.Driver";//"com.mysql.jdbc.GoogleDriver";
	private static final String DATABASE_TYPE		= "mysql";

	/**
	 * @throws DriverNotLoadedException If this connection cannot be established.<br> 
	 * 
	 */
	private GoogleCloudSQLConnection() throws DriverNotLoadedException {
		super(DATABASE_TYPE, DRIVER_PACKAGE_NAME);
		
		logger.info("To initial Google Cloud SQL connection...");
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.BaseDatabaseConnection#setDatabaseName(java.lang.String)
	 */
	@Override
	public void setDatabaseName(String name) {
		name = "google/" + name;
		super.setDatabaseName(name);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() throws DriverNotLoadedException {
		return new Builder();
	}
}
