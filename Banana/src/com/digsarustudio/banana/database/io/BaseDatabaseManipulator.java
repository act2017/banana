/**
 * 
 */
package com.digsarustudio.banana.database.io;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;

/**
 * The base class of {@link DatabaseManipulator}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class BaseDatabaseManipulator implements DatabaseManipulator {
	protected String				databaseName		= null;
	protected CharacterSets			charSet				= null;
	protected Collations			collation			= null;
	protected Boolean				isDefaultCharSet	= false;
	protected Boolean				isDefaultCollation	= false;
	protected Boolean				isToCreateIfNotExist= true;
	protected Boolean				isToDropIfExists	= true;
	
	protected DatabaseConnection	connection			= null;
	
	protected String					sqlQueryString	= null;
	
	/**
	 * 
	 */
	public BaseDatabaseManipulator() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#setConnection(com.digsarustudio.banana.database.io.DatabaseConnection)
	 */
	@Override
	public void setConnection(DatabaseConnection connection) {
		this.connection = connection;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.databaseName = name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#setCharacterSets(com.digsarustudio.banana.database.CharacterSets)
	 */
	@Override
	public void setCharacterSets(CharacterSets charset) {
		this.charSet = charset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#setCollations(com.digsarustudio.banana.database.Collations)
	 */
	@Override
	public void setCollations(Collations collations) {
		this.collation = collations;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#getName()
	 */
	@Override
	public String getName() {		
		return this.databaseName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#getCharacterSets()
	 */
	@Override
	public CharacterSets getCharacterSets() {
		return this.charSet;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#getCollations()
	 */
	@Override
	public Collations getCollations() {		
		return this.collation;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#open()
	 */
	@Override
	public void open() throws SQLManipulationException {
		this.connection.open();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#isConnected()
	 */
	@Override
	public Boolean isConnected() {
		return this.connection.isConnected();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#close()
	 */
	@Override
	public void close() throws SQLManipulationException {
		this.connection.close();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#executeUpdate(java.lang.String)
	 */
	@Override
	public void executeUpdate(String sqlString) throws SQLManipulationException {
		this.sqlQueryString = sqlString;
		this.connection.executeUpdate(sqlString);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#executeQuery(java.lang.String)
	 */
	@Override
	public ResultBundle executeQuery(String sqlString) throws SQLManipulationException {
		this.sqlQueryString = sqlString;
		return this.connection.executeQuery(sqlString);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#executeUpdateWithAutoIncrementColumn(java.lang.String)
	 */
	@Override
	public void executeUpdateWithAutoIncrementColumn(String sqlString) throws SQLManipulationException {
		this.sqlQueryString = sqlString;
		this.connection.executeUpdateWithAutoIncrementColumn(sqlString);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#getLastAutoIncrement()
	 */
	@Override
	public Long getLastAutoIncrement() {
		return this.connection.getLastAutoIncrement();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.DatabaseManipulator#getSQLQueryString()
	 */
	@Override
	public String getSQLQueryString() {
		return this.sqlQueryString;
	}

}
