/**
 * 
 */
package com.digsarustudio.banana.database.io;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type of this interface represents as a connection object which contains the information of the connection 
 * between a specific database. It can build and release the connection, and the data can be transferred via sending 
 * and receiving functions.<br>  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface DatabaseConnection {
	/**
	 * 
	 * The builder for {@link DatabaseConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static interface Builder extends ObjectBuilder<DatabaseConnection> {
		/**
		 * Assigns a host name to connect
		 * 
		 * @param name The host name to connect
		 * 
		 * @return The instance of this builder
		 */
		Builder setHostName(String name);
		
		/**
		 * Assigns a port to connect
		 * 
		 * @param port A port to connect
		 * 
		 * @return The instance of this builder
		 */
		Builder setPort(Integer port);
		
		/**
		 * Assigns a user name for this connection
		 * @param name The user name for this connection
		 */
		Builder setUserName(String name);
			
		/**
		 * Assigns a password for this connection
		 * 
		 * @param password The password for this connection
		 * 
		 * @return The instance of this builder
		 */
		Builder setPassword(String password);
		
		/**
		 * The name of database for cloud SQL to connect
		 * 
		 * @param name
		 * @return
		 */
		Builder setDatabaseName(String name);
		
		/**
		 * The name of instance for cloud SQL to connect the database
		 * 
		 * @param name
		 * @return
		 */
		Builder setInstanceConnectionName(String name);
	}
	
	/**
	 * To open a connection
	 * 
	 * @throws SQLManipulationException When the connection cannot be established 
	 * 			due to invalid host name, user name, or password. 
	 */
	void open() throws SQLManipulationException;
	
	/**
	 * To close the connection
	 * 
	 * @throws SQLManipulationException If the connection cannot be closed. 
	 */
	void close() throws SQLManipulationException;
	
	/**
	 * Returns true if the connection is opened, otherwise false returned.
	 * 
	 * @return true if the connection is opened, otherwise false returned.
	 */
	Boolean isConnected();
	
	/**
	 * To send a query for a specific execution.
	 * 
	 * @param sqlString A query for a specific execution.
	 * 
	 * @throws SQLManipulationException If an error occurred. 
	 */
	void executeUpdate(String sqlString) throws SQLManipulationException;
	
	/**
	 * To send a insert query for a specific execution with stores data with auto increment column
	 * 
	 * @param sqlString A query for a specific execution
	 * 
	 * @throws SQLManipulationException if an error occurred.
	 */
	void executeUpdateWithAutoIncrementColumn(String sqlString) throws SQLManipulationException;
	
	/**
	 * Returns the results of a particular query.<br>
	 * 
	 * @param sqlString The query string to execute.
	 * 
	 * @return the results of a particular query.
	 * 
	 * @throws SQLManipulationException If an error occurred. 
	 */
	ResultBundle executeQuery(String sqlString) throws SQLManipulationException;
	
	/**
	 * Returns the last auto increment value.<br>
	 * Before use this function please use {@link #executeInsert(String)} to insert the data.<br>
	 * 
	 * @return The last auto increment value
	 */
	Long getLastAutoIncrement();
	

	
	/**
	 * The name of database for cloud SQL to connect
	 * 
	 * @param name
	 */
	void setDatabaseName(String name);
	
	/**
	 * The name of instance for cloud SQL to connect the database
	 * 
	 * @param name
	 */
	void setInstanceConnectionName(String name);
}
