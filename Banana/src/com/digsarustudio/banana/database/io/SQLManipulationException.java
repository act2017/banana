/**
 * 
 */
package com.digsarustudio.banana.database.io;

import com.digsarustudio.banana.database.DatabaseException;

/**
 * This exception indicates that an error occurred because of the SQL execution.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.2
 * @version		1.0.2
 * <br>
 * Note:<br>
 * 				1.0.2	-> Move this class under {@link DatabaseException} to consolidate the exceptions used by client code.<br>
 *
 */
@SuppressWarnings("serial")
public class SQLManipulationException extends DatabaseException {
	/**
	 * 
	 */
	public SQLManipulationException() {

	}

	/**
	 * @param message
	 */
	public SQLManipulationException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public SQLManipulationException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public SQLManipulationException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public SQLManipulationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
