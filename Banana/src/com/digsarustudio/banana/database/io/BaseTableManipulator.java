/**
 * 
 */
package com.digsarustudio.banana.database.io;

import java.util.List;

import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;

/**
 * The subclass of {@link TableManipulator} used for creating, renaming, structure modifying, data clearing, 
 * and deleting to a specific table.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class BaseTableManipulator implements TableManipulator {
	protected DatabaseManipulator	dbManipulator					= null;	
	protected Table					table							= null;			
	protected DatabaseEngineType	engineType						= null;	
	protected Boolean				isPreventingErrorIfTableExists	= false;	
	
	/**
	 * 
	 */
	public BaseTableManipulator() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#setTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setTable(Table table) {
		this.table = table;	
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#getTable()
	 */
	@Override
	public Table getTable() {
		return this.table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#setDatabaseEngineType(com.digsarustudio.banana.database.DatabaseEngineType)
	 */
	@Override
	public void setDatabaseEngineType(DatabaseEngineType type) {
		this.engineType = type;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#getDatabaseEngineType()
	 */
	@Override
	public DatabaseEngineType getDatabaseEngineType() {
		return this.engineType;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#setPreventErrorIfTableExists()
	 */
	@Override
	public void setPreventErrorIfTableExists() {
		this.isPreventingErrorIfTableExists = true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#setNotPreventErrorIfTableExists()
	 */
	@Override
	public void setNotPreventErrorIfTableExists() {
		this.isPreventingErrorIfTableExists = false;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isPreventErrorIfTableExists()
	 */
	@Override
	public Boolean isPreventErrorIfTableExists() {
		return this.isPreventingErrorIfTableExists;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#renameTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void renameTable(Table originalTable) throws SQLManipulationException {
		this.renameTable(originalTable.getName());
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#changeColumnName(com.digsarustudio.banana.database.table.TableColumn, com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void changeColumnName(TableColumn originalColumn, TableColumn newColumn) throws SQLManipulationException {
		// ALTER TABLE t2 CHANGE b c;
		this.changeColumnName(originalColumn.getName(), newColumn);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isColumnExisting(java.lang.String)
	 */
	@Override
	public Boolean isColumnExisting(String name) throws SQLManipulationException {
		return (null != this.getColumn(name));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isColumnExisting(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public Boolean isColumnExisting(TableColumn column) throws SQLManipulationException {
		return this.isColumnExisting(column.getName());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#deleteTable()
	 */
	@Override
	public void deleteTable() throws SQLManipulationException {
		this.deleteTable(this.table.getName());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#clearTable()
	 */
	@Override
	public void clearTable() throws SQLManipulationException {
		this.clearTable(this.table.getName());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#resetTable()
	 */
	@Override
	public void resetTable() throws SQLManipulationException {
		this.resetTable(this.table.getName());

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#isTableExisting()
	 */
	@Override
	public Boolean isTableExisting() throws SQLManipulationException {
		return this.isTableExisting(this.table.getName());
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.io.TableManipulator#getSQLQueryString()
	 */
	@Override
	public String getSQLQueryString() {
		return this.dbManipulator.getSQLQueryString();
	}

	/**
	 * @param dbManipulator the dbManipulator to set
	 */
	protected void setDatabaseManipulator(DatabaseManipulator dbManipulator) {
		this.dbManipulator = dbManipulator;
	}
	
	/**
	 * Returns a SQL string which is a formatted column name for a particular SQL syntax/<br>
	 * 
	 * @param table The table which contains the target column.
	 * 
	 * @return A SQL string which is a formatted column name for a particular SQL syntax
	 */
	abstract protected String getColumnNamesString(Table table);
	
	/**
	 * Returns the definitions of a particular {@link TableColumn}
	 * 
	 * @param column The column to be retrieved definitions
	 * 
	 * @return the definitions of a particular {@link TableColumn}
	 */
	abstract protected String getDefinitions(TableColumn column);
	
	/**
	 * Returns the name and definitions of a particular {@link TableColumn}
	 * 
	 * @param column The column to be retrieved definitions
	 * 
	 * @return the name and definitions of a particular {@link TableColumn}
	 */
	abstract protected String getColumnDefinition(TableColumn column);
	
	/**
	 * Returns a SQL string which contains the primary keys.<br>
	 * 
	 * @param table The table to retrieve primary keys
	 * 
	 * @return A SQL string which contains the primary keys.
	 */
	abstract protected String getPrimaryKeysString(Table table);
	
	/**
	 * Returns a SQL string which contains the foreign keys.<br>
	 * 
	 * @param table The table to retrieve foreign keys
	 * 
	 * @return A SQL string which contains the foreign keys.
	 */
	abstract protected String getForeignKeysString(Table table);
	
	/**
	 * Returns a SQL string which contains the foreign definition.<br>
	 * 
	 * @param table The table to retrieve foreign definition
	 * 
	 * @return A SQL string which contains the foreign definition.
	 */
	abstract protected String getForeignKeyDefinition(TableColumn column);

	/**
	 * Returns a SQL string which contains the unique keys.<br>
	 * 
	 * @param table The table to retrieve unique keys
	 * 
	 * @return A SQL string which contains the unique keys.
	 */
	abstract protected String getUniqueKeyString(Table table);
	
	/**
	 * Returns a SQL string which contains the columns supporting full text search
	 * 
	 * @param table The table to retrieve columns
	 * 
	 * @return A SQL string which contains the columns supporting full text search
	 */
	abstract protected String getFullTextColumnString(Table table);
	
	/**
	 * Returns a SQL string which contains all of the enums
	 * 
	 * @param enums The list of enums to format
	 * 
	 * @return A SQL string which contains all of the enums
	 */
	abstract protected String getEnumString(List<String> enums);
}
