/**
 * 
 */
package com.digsarustudio.banana.database;

import java.sql.SQLException;

/**
 * The manipulator of database in a database server
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.io.DatabaseManipulator} instead.
 */
public interface DatabaseManipulator {
	/**
	 * To create a particular database
	 * 
	 * @param name The name of database to create
	 * @param charset The character set of the database to create
	 * @param collations The collations of the database to create
	 * 
	 * @throws SQLException
	 */
	void createDatabase(String name, CharacterSets charset, Collations collations) throws SQLException;
	
	/**
	 * To select a particular database
	 * 
	 * @param name The name of the particular database to select
	 * 
	 * @throws SQLException
	 */
	void selectDatabase(String name) throws SQLException;
	
	/**
	 * To delete a particular database
	 * 
	 * @param name The name of database to delete
	 * 
	 * @throws SQLException
	 */
	void deleteDatabase(String name) throws SQLException;
	
	/**
	 * To create a database on the database server.
	 * Please set the name of database, character set, and collations before calling this function.
	 * 
	 * @throws SQLException
	 */
	void createDatabase() throws SQLException;
	
	/**
	 * To select an existing database on the database server.
	 * 
	 * Please set the name of database before calling this function.
	 * 
	 * @throws SQLException
	 */
	void selectDatabase() throws SQLException;
	
	/**
	 * To delete the assigned database
	 * 
	 * @throws SQLException
	 */
	void deleteDatabase() throws SQLException;
	
	/**
	 * Assigns a name of database for the creating, selecting, and deleting operation
	 * 
	 * @param name The name of database for the creating, selecting, and deleting operation
	 */
	void setName(String name);
	
	/**
	 * Assigns a character sets when going to create a database.
	 * 
	 * @param charset The character sets for the database to create.
	 */
	void setCharacterSets(CharacterSets charset);
	
	/**
	 * Assigns a collation when going to create a database.
	 * 
	 * @param collations The collation for the database to create.
	 */
	void setCollations(Collations collations);
	
	/**
	 * Returns the name of database in use.
	 * 
	 * @return the name of database in use.
	 */
	String getName();
	
	/**
	 * Returns the character sets
	 * 
	 * @return the character sets
	 */
	CharacterSets getCharacterSets();
	
	/**
	 * Returns the collations used for this database
	 * 
	 * @return the collations used for this database
	 */
	Collations getCollations();
	
	/**
	 * Returns true if the database is existing, otherwise false returned
	 * 
	 * @return true if the database is existing, otherwise false returned
	 * @throws SQLException 
	 */
	Boolean isDatabaseExisting() throws SQLException;

	/**
	 * Returns true if the database is existing, otherwise false returned
	 * 
	 * @param name The name of the database to check
	 * 
	 * @return true if the database is existing, otherwise false returned
	 * @throws SQLException 
	 */
	Boolean isDatabaseExisting(String name) throws SQLException;
}
