/**
 * 
 */
package com.digsarustudio.banana.database;

/**
 * Thrown when an application tries to create a {@link DatabaseConnector} but
 * the driver for the target database cannot be initiated. It could be caused
 * by {@link InstantiationException} or {@link IllegalAccessException}, to see
 * the error message by {@link #getMessage()} for the details.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.0
 * @version		1.0.2
 * <br>
 * Note:<br>
 * 				1.0.2	-> Move this class under {@link DatabaseException} to consolidate the exceptions used by client code.<br>
 */
@SuppressWarnings("serial")
public class DriverNotLoadedException extends DatabaseException {
	/**
	 * 
	 */
	public DriverNotLoadedException() {

	}

	/**
	 * @param message
	 */
	public DriverNotLoadedException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public DriverNotLoadedException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public DriverNotLoadedException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DriverNotLoadedException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
