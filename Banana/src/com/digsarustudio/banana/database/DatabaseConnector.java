/**
 * 
 */
package com.digsarustudio.banana.database;

import java.sql.SQLException;

/**
 * The connector used to connect database.
 * 
 * Note: How to hide Statement
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2 Please use {@link com.digsarustudio.banana.database.io.DatabaseConnection} instead.
 */
public interface DatabaseConnector {
	/**
	 * Assigns a URL for this connector
	 * @param url The URL for this connector
	 */
	void setURL(String url);
	
	/**
	 * Returns the URL set for this connector
	 * 
	 * @return the URL set for this connector
	 */
	String getURL();
	
	/**
	 * Assigns a user name for this connector
	 * @param userName The user name for this connector
	 */
	void setUserName(String userName);
	
	/**
	 * Returns the user name set for this connector
	 * 
	 * @return the user name set for this connector
	 */
	String getUserName();
	
	/**
	 * Assigns a password for this connector
	 * @param password The password for this connector
	 */
	void setPassword(String password);
	
	/**
	 * Returns the password set for this connector
	 * 
	 * @return the password set for this connector
	 */
	String getPassword();
	
	/**
	 * To connect the target database
	 * 
	 * Please set URL by {@link DatabaseConnector#setURL(String)},
	 * 			  User Name by {@link DatabaseConnector#setUserName(String)},
	 * 		  and Password by {@link DatabaseConnector#setPassword(String)}
	 * before calling this function.	
	 * 
	 * @throws SQLException When the database cannot be connected
	 */
	void connect() throws SQLException;
	
	/**
	 * To disconnect from database server
	 */
	void disconnect();
	
	/**
	 * Returns the connection object of the database server
	 * 
	 * @return the connection object of the database server
	 */
	DatabaseConnection getConnection();
}
