/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import java.sql.SQLException;
import java.util.List;

import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.table.ColumnDataType;
import com.digsarustudio.banana.database.table.DefaultValues;
import com.digsarustudio.banana.database.table.ForeignKeyConstraint;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.database.table.TableColumnAttributes;
import com.digsarustudio.banana.database.table.TableColumnDataTypes;
import com.digsarustudio.banana.database.table.TableManipulator;
import com.digsarustudio.banana.database.table.UnSupportedColumnDataType;
import com.digsarustudio.banana.utils.StringFormatter;
//import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

/**
 * The manipulator for the table of MySQL
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator} instead.<br>
 */
public class MySQLTableManipulator implements TableManipulator {
	/**
	 * 
	 * The object builder for {@link MySQLTableManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements TableManipulator.Builder {
		private Query				query		= null;	
		private Table				table		= null;			
		private DatabaseEngineType	engineType	= null;	
		private Boolean				isPreventingErrorIfTableExists	= false;
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.TableManipulator.Builder#setQuery(com.digsarustudio.banana.database.Query)
		 */
		@Override
		public Builder setQuery(Query query) {
			this.query = query;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.TableManipulator.Builder#setTable(com.digsarustudio.banana.database.Table)
		 */
		@Override
		public Builder setTable(Table table) {
			this.table = table;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.TableManipulator.Builder#setDatabaseEngineType(com.digsarustudio.banana.database.DatabaseEngineType)
		 */
		@Override
		public Builder setDatabaseEngineType(DatabaseEngineType type) {
			this.engineType = type;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.TableManipulator.Builder#setPreventErrorIfTableExists()
		 */
		@Override
		public Builder setPreventErrorIfTableExists() {
			this.isPreventingErrorIfTableExists = true;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MySQLTableManipulator build() {
			return new MySQLTableManipulator(this);
		}

		/**
		 * @return the query
		 */
		private Query getQuery() {
			return query;
		}

		/**
		 * @return the table
		 */
		private Table getTable() {
			return table;
		}

		/**
		 * @return the engineType
		 */
		private DatabaseEngineType getEngineType() {
			return engineType;
		}

		/**
		 * @return the isPreventingErrorIfTableExists
		 */
		private Boolean isPreventingErrorIfTableExists() {
			return isPreventingErrorIfTableExists;
		}
		
	}
	
	
	private Query				query		= null;	
	private Table				table		= null;			
	private DatabaseEngineType	engineType	= null;	
	private Boolean				isPreventingErrorIfTableExists	= false;
	
	/**
	 * Construct a table manipulator
	 * 
	 * @param connector The connector of target database
	 */
	private MySQLTableManipulator(Builder builder) {
		this.query = builder.getQuery();
		this.table = builder.getTable();
		this.engineType = builder.getEngineType();
		this.isPreventingErrorIfTableExists = builder.isPreventingErrorIfTableExists();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.TableManipulator#setTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setTable(Table table) {
		this.table = table;		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#createTable()
	 */
	@Override
	public void createTable() throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("CREATE TABLE");
		
		if(this.isPreventingErrorIfTableExists){
			builder.append(" IF NOT EXISTS");
		}
		
		builder.append(StringFormatter.format(" `%s` (", this.table.getName()));
		builder.append(StringFormatter.format("%s", this.getColumnNamesString(this.table)));
		
		if( this.table.hasPrimaryKey() ){
			builder.append(StringFormatter.format(", PRIMARY KEY(%s)", this.getPrimaryKeysString(this.table)));
		}
		
		if( this.table.hasForeignKey() ){
			builder.append(StringFormatter.format(", %s", this.getForeignKeysString(this.table)));
		}
		
		if( this.table.hasUniqueKey() ){
			builder.append(StringFormatter.format(", %s", this.getUniqueKeyString(this.table)));
		}
		
		if( this.table.isSupportFullTextSearch() ){
			builder.append(StringFormatter.format(", FULLTEXT (%s)", this.getFullTextColumnString(this.table)));
		}
		
		builder.append(")");
		
		if(null != this.engineType){
			builder.append(" ENGINE=");
			builder.append(this.engineType.getValue());
		}
		
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#alterTable()
	 * 
	 * @deprecated 1.0.0
	 */
	@Override
	@Deprecated
	public void alterTable() {
	}

	@Override
	public void renameTable(String originalName) throws SQLException {
		//ALTER TABLE t1 RENAME t2;
		StringBuilder builder = new StringBuilder();
		
		builder.append("ALTER TABLE");
		builder.append(StringFormatter.format(" `%s` RENAME `%s`", originalName, this.table.getName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void renameTable(Table originalTable) throws SQLException {
		this.renameTable(originalTable.getName());
	}

	@Override
	public void addColumn(TableColumn column) throws SQLException {
		// ALTER TABLE t2 ADD d TIMESTAMP;
		// ALTER TABLE t2 ADD c INT UNSIGNED NOT NULL AUTO_INCREMENT, ADD PRIMARY KEY (c);
		StringBuilder builder = new StringBuilder();
		
		builder.append("ALTER TABLE ");
		builder.append(this.table.getName());
		builder.append(" ADD ");
		builder.append(this.getColumnDefinition(column));
		
		if( column.isPrimaryKey() ) {
			builder.append( StringFormatter.format(", ADD PRIMARY KEY (`%s`)", column.getName()) );
		}
		
		if( column.isForeignKey() ) {
			builder.append( StringFormatter.format(", ADD ", this.getForeignKeyDefinition(column)) );
		}
		
		if( column.isUniqueKey() ) {
			builder.append( StringFormatter.format(", ADD UNIQUE KEY (`%s`)", column.getName()) );
		}
		
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void modifyColumn(TableColumn column) throws SQLException {
		 // ALTER TABLE t2 MODIFY a TINYINT NOT NULL;
		// ALTER TABLE t2 CHANGE a a  TINYINT NOT NULL;		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(" ");
		builder.append(this.getDefinitions(column));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLException {
		// ALTER TABLE t2 CHANGE b c COLUMN_DEFINITION;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" CHANGE `%s` `%s`", originalColumnName, newColumn.getName()));
		builder.append(" ");
		builder.append(this.getDefinitions(newColumn));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void changeColumnName(TableColumn originalColumn, TableColumn newColumn) throws SQLException {
		// ALTER TABLE t2 CHANGE b c;
		this.changeColumnName(originalColumn.getName(), newColumn);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.TableManipulator#getColumn(java.lang.String)
	 */
	@Override
	public TableColumn getColumn(String name) throws SQLException {
//		SHOW [FULL] {COLUMNS | FIELDS}
//	    {FROM | IN} tbl_name
//	    [{FROM | IN} db_name]
//	    [LIKE 'pattern' | WHERE expr]
	    		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("SHOW COLUMNS FROM `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" LIKE '%s'", name));
		builder.append(";");
		
		QueryResult result = this.query.executeQuery(builder.toString());
		
		//It should be one result returned.
		if( !result.next() ) {
			return null;
		}				
		
		/*		
		SHOW COLUMNS displays the following values for each table column:
		Field
			The column name.

		Type
			The column data type.

		Collation
			The collation for nonbinary string columns, or NULL for other columns. This value is displayed only if you use the FULL keyword.

		Null
			Column nullability. The value is YES if NULL values can be stored in the column, NO if not.

		Key
			Whether the column is indexed:
			If Key is empty, the column either is not indexed or is indexed only as a secondary column in a multiple-column, nonunique index.
			If Key is PRI, the column is a PRIMARY KEY or is one of the columns in a multiple-column PRIMARY KEY.
			If Key is UNI, the column is the first column of a UNIQUE index. (A UNIQUE index permits multiple NULL values, but you can tell whether the column permits NULL by checking the Null field.)
			If Key is MUL, the column is the first column of a nonunique index in which multiple occurrences of a given value are permitted within the column.
			If more than one of the Key values applies to a given column of a table, Key displays the one with the highest priority, in the order PRI, UNI, MUL.

			A UNIQUE index may be displayed as PRI if it cannot contain NULL values and there is no PRIMARY KEY in the table. 
			A UNIQUE index may display as MUL if several columns form a composite UNIQUE index; although the combination of 
			the columns is unique, each column can still hold multiple occurrences of a given value.

		Default
			The default value for the column. This is NULL if the column has an explicit default of NULL, or if the column definition includes no DEFAULT clause.

		Extra
			Any additional information that is available about a given column. The value is nonempty in these cases: 
			auto_increment for columns that have the AUTO_INCREMENT attribute; 
			on update CURRENT_TIMESTAMP for TIMESTAMP or 
			DATETIME columns that have the ON UPDATE CURRENT_TIMESTAMP attribute.

		Privileges
			The privileges you have for the column. This value is displayed only if you use the FULL keyword.

		Comment
			Any comment included in the column definition. This value is displayed only if you use the FULL keyword.		
*/		
		TableColumn rtn = TableColumn.builder().setName(result.getString("Field")).build();
		
		String dataType = result.getString("Type");
		try {
			ColumnDataType columnDataType = new ColumnDataType(dataType);
			
			rtn.setDataType(columnDataType.getDataType());
			rtn.setLength(columnDataType.getLength());
			rtn.setPrecisionLength(columnDataType.getSupplementaryLength());
			
			List<String> enums = columnDataType.getEnumerations();
			if(null != enums){
				for (String en : enums) {
					rtn.addEnum(en);
				}
			}
		} catch (UnSupportedColumnDataType e) {
			e.printStackTrace();
		}
		
		if( result.getString("Null").equals("YES") ){
			rtn.setNullable();
		}else{
			rtn.setUnNullable();			
		}
		
		/*
		 * Note:<br>
		 * The key constraint is not really available now. Only primary keys can be parsed well, and
		 * non-null unique keys as well. The foreign keys cannot be recognized so far. It should be fixed if
		 * this method is used to retrieve the details of column from database.<br>
		 * <br>
		 * TODO Trying to recognize key constraints.
		 */
		switch(result.getString("Key")){
			case "PRI":
				rtn.setAsPrimaryKey();
				break;
				
			case "UNI":
				rtn.setAsUniqueKey();
				break;
			
			//The foreign key will go through here
			case "MUL":
				break;
		}
		
		String defaultValue = result.getString("Default");
		if( null != defaultValue ){
			if( defaultValue.equals("NULL") && !rtn.isNullable() ){
				rtn.setUnNullable();
			}else if( defaultValue.equals("NULL") && rtn.isNullable() ){
				rtn.setNullable();
			}else{
				rtn.setDefaultValue(defaultValue);			
			}
		}

		return rtn;
	}

	@Override
	public void registerColumnAsPrimaryKey(TableColumn column) throws SQLException {
		//ALTER TABLE t2 ADD PRIMARY KEY (c);
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" ADD PRIMARY KEY (`%s`)", column.getName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void unregisterPrimaryKeys() throws SQLException {
		//ALTER TABLE mytable DROP PRIMARY KEY
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s` DROP PRIMARY KEY", this.table.getName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void registerColumnAsForeignKey(TableColumn column) throws SQLException {
// 		ALTER TABLE tbl_name
//	    ADD [CONSTRAINT [symbol]] FOREIGN KEY
//	    [index_name] (index_col_name, ...)
//	    REFERENCES tbl_name (index_col_name,...)
//	    [ON DELETE reference_option]
//	    [ON UPDATE reference_option]		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" ADD %s", this.getForeignKeyDefinition(column)));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void unregisterColumnFromForeignKey(TableColumn column) throws SQLException {
		// ALTER TABLE tbl_name DROP FOREIGN KEY fk_symbol;		
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP FOREIGN KEY `%s`", column.getForeignKeyConstraint().getName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void registerColumnAsUniqueKey(TableColumn column) throws SQLException {
		// ALTER TABLE sales.order ADD UNIQUE(order_)
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" ADD UNIQUE(`%s`)", column.getName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void unregisterColumnFromUniquekKey(TableColumn column) throws SQLException {
		// alter table TABLE drop INDEX email;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP INDEX `%s`", column.getIndexName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void removeColumn(TableColumn column) throws SQLException {
		// ALTER TABLE t2 DROP COLUMN c, DROP COLUMN d;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP COLUMN `%s`", column.getName()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	@Override
	public void removeColumn(String name) throws SQLException {
		// ALTER TABLE t2 DROP COLUMN c, DROP COLUMN d;
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("ALTER TABLE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" DROP COLUMN `%s`", name));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#deleteTable()
	 */
	@Override
	public void deleteTable() throws SQLException {
		this.deleteTable(this.table.getName());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#deleteTable(java.lang.String)
	 */
	@Override
	public void deleteTable(String name) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append(StringFormatter.format("DROP TABLE `%s`;", name));
		
		this.query.executeUpdate(builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#clearTable()
	 */
	@Override
	public void clearTable() throws SQLException {
		this.clearTable(this.table.getName());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#clearTable(java.lang.String)
	 */
	@Override
	public void clearTable(String name) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("DELETE FROM ");
		builder.append(" `");
		builder.append(name);
		builder.append("`;");
		
		this.query.executeUpdate(builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.TableManipulator#resetTable()
	 */
	@Override
	public void resetTable() throws SQLException {
		this.resetTable(this.table.getName());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.TableManipulator#resetTable(java.lang.String)
	 */
	@Override
	public void resetTable(String name) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("TRUNCATE TABLE");
		builder.append(" `");
		builder.append(name);
		builder.append("`;");
		
		this.query.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#isTableExisting()
	 */
	@Override
	public Boolean isTableExisting() throws SQLException {
		return this.isTableExisting(this.table.getName());
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.TableManipulator#isTableExisting(java.lang.String)
	 */
	@Override
	public Boolean isTableExisting(String name) throws SQLException {
		StringBuilder builder = new StringBuilder();
		Boolean isExisting = false;
		
		builder.append(StringFormatter.format("SELECT * FROM `%s` LIMIT 1;", name));
		
		QueryResult results = null;
		try{		
			results = this.query.executeQuery(builder.toString());
			isExisting = true;
//		}catch( MySQLSyntaxErrorException syntax){
//			isExisting = false;
		}catch (SQLException e) {			
			throw e;
		}finally{
			if( null != results ){
				results.close();
			}
		}		
		
		return isExisting;
	}	
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.TableManipulator#isColumnExisting(java.lang.String)
	 */
	@Override
	public Boolean isColumnExisting(String name) throws SQLException {					
		return (null != this.getColumn(name));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.TableManipulator#isColumnExisting(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public Boolean isColumnExisting(TableColumn column) throws SQLException {
		return this.isColumnExisting(column.getName());
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private String getColumnNamesString(Table table){
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count)) ){
			if(0 != count){
				builder.append(", ");
			}
			
			builder.append(this.getColumnDefinition(column));
			count++;
		}
		
		return builder.toString();
	}
	
	/**
	 * Returns the definitions of a particular {@link TableColumn}
	 * 
	 * @param column The column to be retrieved definitions
	 * 
	 * @return the definitions of a particular {@link TableColumn}
	 */
	private String getDefinitions(TableColumn column){
		StringBuilder builder = new StringBuilder();
		
		TableColumnDataTypes dataType = column.getDataType();			
		String dataTypeString = null;
		if( TableColumnDataTypes.Enum == dataType ){
			dataTypeString = getEnumString(column.getEnums());
		}else{
			if( null != column.getPrecisionLength() ){
				dataTypeString = StringFormatter.format(dataType.toString(), column.getLength(), column.getPrecisionLength());
			}else if(null != column.getLength()){
				dataTypeString = StringFormatter.format(dataType.toString(), column.getLength());
			}else{
				dataTypeString = dataType.getName();
			}
		}			
		builder.append(StringFormatter.format(" %s", dataTypeString));
		
		//sign/unsign
		if( column.isUnsignedColumn() ) {
			builder.append(" UNSIGNED");
		}
		
		builder.append(" ");
		if( column.isNullable() ){				
			builder.append(TableColumnAttributes.CanBeNull.getValue());
		}else{
			builder.append(TableColumnAttributes.CannotBeNull.getValue());
		}
		
		if( column.hasDefaultValue() ){
			builder.append(" DEFAULT ");
			builder.append(column.getDefaultValue());
		}
		
		//On update current timestamp
		if( column.isOnUpdateCurrentTimestamp() ){
			builder.append(" ON UPDATE ");
			builder.append(DefaultValues.CurrentTimestamp.getValue());
		}
		
		if( column.isAutoIncrement() ){
			builder.append(StringFormatter.format(" %s", TableColumnAttributes.AutoIncrement.getValue()));
		}
		
		return builder.toString();
	}
	
	/**
	 * Returns the name and definitions of a particular {@link TableColumn}
	 * 
	 * @param column The column to be retrieved definitions
	 * 
	 * @return the name and definitions of a particular {@link TableColumn}
	 */
	private String getColumnDefinition(TableColumn column) {
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("`%s`", column.getName()));
		builder.append(this.getDefinitions(column));		
		
		return builder.toString();
	}
	
	private String getPrimaryKeysString(Table table){
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, pkCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isPrimaryKey()){
				continue;
			}
			
			if(0 != pkCount++){
				builder.append(", ");
			}
			
			builder.append(column.getName());
		}		
		
		return builder.toString();
	}
	
	private String getForeignKeysString(Table table){
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, keyCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isForeignKey()){
				continue;
			}
			
			if(0 != keyCount++){
				builder.append(", ");
			}
			
			builder.append(this.getForeignKeyDefinition(column));
		}		
		
		return builder.toString();
	}
	
	private String getForeignKeyDefinition(TableColumn column) {
		StringBuilder builder = new StringBuilder();
		
		ForeignKeyConstraint foreignKeyConstraint = column.getForeignKeyConstraint();		
		if( foreignKeyConstraint.getName() != null && !foreignKeyConstraint.getName().isEmpty() ) {
			builder.append(StringFormatter.format("CONSTRAINT `%s` ", foreignKeyConstraint.getName()));
		}
		
		builder.append(StringFormatter.format("FOREIGN KEY (`%s`) REFERENCES `%s`(`%s`)", column.getName()
																				   , foreignKeyConstraint.getReferentialTable()
																				   , foreignKeyConstraint.getReferentialPrimaryKey()));
		if( null != foreignKeyConstraint.getDeleteReferentialActions() ){
			builder.append(StringFormatter.format(" ON DELETE %s", foreignKeyConstraint.getDeleteReferentialActions().getValue()));
		}
		
		if( null != foreignKeyConstraint.getUpdateReferentialActions()){
			builder.append(StringFormatter.format(" ON UPDATE %s", foreignKeyConstraint.getUpdateReferentialActions().getValue()));
		}		
		
		return builder.toString();
	}
	
	private String getUniqueKeyString(Table table){
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, keyCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isUniqueKey()){
				continue;
			}
			
			if(0 != keyCount++){
				builder.append(", ");
			}
			
			builder.append("UNIQUE KEY");
			
			if( null != column.getIndexName() ){
				builder.append(StringFormatter.format(" `%s`", column.getIndexName()));
			}
			
			builder.append(StringFormatter.format(" (`%s`)",  column.getName()));
		}		
		
		return builder.toString();
	}
	
	private String getFullTextColumnString(Table table){
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0, keyCount = 0;
		TableColumn column = null;
		while(null != (column = table.getColumn(count++))){
			if(!column.isSupportFullTextSearch()){
				continue;
			}
			
			if(0 != keyCount++){
				builder.append(",");
			}
			
			builder.append(column.getName());
		}		
		
		return builder.toString();
	}
	
	private String getEnumString(List<String> enums){
		StringBuilder builder = new StringBuilder();		
		Integer count = 0;
		
		for (String value : enums) {
			if( 0 != count++ ){
				builder.append(",");
			}
			
			builder.append(StringFormatter.format("'%s'", value));
		}
		
		return StringFormatter.format("%s(%s)", TableColumnDataTypes.Enum.getName(), builder.toString());
	}
}
