/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import java.sql.SQLException;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The manipulator for the database of MySQL
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @reference	https://dev.mysql.com/doc/refman/5.7/en/create-database.html
 * 				https://dev.mysql.com/doc/refman/5.7/en/use.html
 * 				https://dev.mysql.com/doc/refman/5.7/en/drop-database.html
 * 
 * @deprecated	1.0.2 Please use {@link com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator} instead.
 */
public class MySQLDatabaseManipulator implements com.digsarustudio.banana.database.DatabaseManipulator{
	private String				databaseName		= null;
	private CharacterSets		charSet				= null;
	private Collations			collation			= null;
	private Boolean				isDefaultCharSet	= false;
	private Boolean				isDefaultCollation	= false;
	private Boolean				isToCreateIfNotExist= true;
	private Boolean				isToDropIfExists	= true;
	
	private Query				query				= null;
		
	/**
	 * 
	 */
	public MySQLDatabaseManipulator(Query query) {
		this.query = query;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#createDatabase()
	 */
	@Override
	public void createDatabase() throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("CREATE DATABASE");
		
		if(this.isToCreateIfNotExist){
			builder.append(" IF NOT EXISTS");
		}
		
		builder.append(StringFormatter.format(" %s ", this.databaseName));
		
		if(this.isDefaultCharSet){
			builder.append(" DEFAULT");
		}		
		builder.append(StringFormatter.format(" CHARACTER SET = %s", this.charSet.getValue()));
		
		if(this.isDefaultCollation){
			builder.append(" DEFAULT");
		}
		builder.append(StringFormatter.format(" COLLATE = %s", this.collation.getValue()));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());

	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#createDatabase(java.lang.String, com.digsarustudio.banana.database.CharacterSets, com.digsarustudio.banana.database.Collations)
	 */
	@Override
	public void createDatabase(String name, CharacterSets charset, Collations collations) throws SQLException {
		this.setName(name);
		this.setCharacterSets(charset);
		this.setCollations(collations);
		
		this.createDatabase();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#selectDatabase()
	 */
	@Override
	public void selectDatabase() throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("USE");
		builder.append(StringFormatter.format(" %s", this.databaseName));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#selectDatabase(java.lang.String)
	 */
	@Override
	public void selectDatabase(String name) throws SQLException {
		this.setName(name);
		this.selectDatabase();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#deleteDatabase()
	 */
	@Override
	public void deleteDatabase() throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("DROP DATABASE");
		
		if(this.isToDropIfExists){
			builder.append(" IF EXISTS");
		}
		
		builder.append(StringFormatter.format(" %s", this.databaseName));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#deleteDatabase(java.lang.String)
	 */
	@Override
	public void deleteDatabase(String name) throws SQLException {
		this.setName(name);
		
		this.deleteDatabase();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.databaseName = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#setCharacterSets(com.digsarustudio.banana.database.CharacterSets)
	 */
	@Override
	public void setCharacterSets(CharacterSets charset) {
		this.charSet = charset;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#setCollations(com.digsarustudio.banana.database.Collations)
	 */
	@Override
	public void setCollations(Collations collations) {
		this.collation = collations;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#getName()
	 */
	@Override
	public String getName() {
		return this.databaseName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#getCharacterSets()
	 */
	@Override
	public CharacterSets getCharacterSets() {

		return this.charSet;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#getCollations()
	 */
	@Override
	public Collations getCollations() {

		return this.collation;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#isDatabaseExisting()
	 */
	@Override
	public Boolean isDatabaseExisting() throws SQLException {
		return this.isDatabaseExisting(this.databaseName);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseManipulator#isDatabaseExisting(java.lang.String)
	 */
	@Override
	public Boolean isDatabaseExisting(String name) throws SQLException {
		StringBuilder builder = new StringBuilder();
		builder.append("SHOW DATABASES LIKE '");
		builder.append(name);
		builder.append("';");
		
		QueryResult result = this.query.executeQuery(builder.toString());

		return result.next();
	}

}
