/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseConnection;

/**
 * The connection object of {@link DatabaseConnector}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.io.mysql.MySQLConnection} instead.
 */
public class MySQLConnection implements DatabaseConnection {
	/**
	 * 
	 * The object builder for {@link MySQLConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements DatabaseConnection.Builder {
		private java.sql.Connection connection = null;
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.DatabaseConnection.Builder#setConnection(java.sql.Connection)
		 */
		@Override
		public Builder setConnection(java.sql.Connection connection) {
			this.connection = connection;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MySQLConnection build() {
			return new MySQLConnection(this);
		}

		private java.sql.Connection getConnection(){
			return this.connection;
		}
	}
	
	private java.sql.Connection connection = null;
	
	/**
	 * 
	 */
	private MySQLConnection(Builder builder) {
		this.connection = builder.getConnection();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseConnection#getStatement()
	 */
	@Override
	public Statement getStatement() throws SQLException {
		
		return this.connection.createStatement();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DatabaseConnection#getPreparedStatement(java.lang.String)
	 */
	@Override
	public PreparedStatement getPreparedStatement(String query) throws SQLException {
		return this.connection.prepareStatement(query);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
