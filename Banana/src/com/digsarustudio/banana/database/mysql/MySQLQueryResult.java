/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.digsarustudio.banana.database.io.ResultBundle;

/**
 * This an adaptor which deals with {@link ResultSet} after {@link Statement#executeUpdate(String)} or
 * {@link Statement#executeQuery(String)} executed.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2 Please use {@link ResultBundle} instead
 * 
 */
public class MySQLQueryResult implements com.digsarustudio.banana.database.query.QueryResult {
	/**
	 * 
	 * The object builder for {@link MySQLQueryResult}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.banana.database.query.QueryResult.Builder {
		private ResultSet results = null;
		
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.QueryResult.Builder#setResults(java.sql.ResultSet)
		 */
		@Override
		public Builder setResults(ResultSet rawData) {
			this.results = rawData;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MySQLQueryResult build() {
			return new MySQLQueryResult(this);
		}

		/**
		 * @return the results
		 */
		public ResultSet getResults() {
			return results;
		}
		
	}
	
	private ResultSet results = null;
	
	/**
	 * 
	 */
	private MySQLQueryResult(Builder builder) {
		this.results = builder.getResults();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.QueryResult#next()
	 */
	@Override
	public Boolean next() throws SQLException {
		
		return this.results.next();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.QueryResult#getString(java.lang.Integer)
	 */
	@Override
	public String getString(Integer columnIndex) throws SQLException {
		return this.results.getString(columnIndex);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.QueryResult#getString(java.lang.String)
	 */
	@Override
	public String getString(String columnLabel) throws SQLException {
		return this.results.getString(columnLabel);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.QueryResult#close()
	 */
	@Override
	public void close() {
		try {
			this.results.close();
		} catch (SQLException e) {
			// Do nothing, just for log
			e.printStackTrace();
		}
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
