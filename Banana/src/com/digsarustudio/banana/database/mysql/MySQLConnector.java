/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import com.digsarustudio.banana.database.DatabaseConnector;
import com.digsarustudio.banana.database.DatabaseConnection;
import com.digsarustudio.banana.database.DriverNotLoadedException;

/**
 * The connector used to connect MySQL database server
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.io.mysql.MySQLConnection} instead.
 */
public class MySQLConnector implements DatabaseConnector {
	private final String JDBC_URL_PREFIX 	= "jdbc:mysql://";
	
	/**
	 * Used with {@link Properties} to get {@link Connection} by {@link DriverManager#getConnection(String)}
	 */
	@SuppressWarnings("unused")
	private final String PROPERTY_USER_NAME	= "user";
	
	/**
	 * Used with {@link Properties} to get {@link Connection} by {@link DriverManager#getConnection(String)}
	 */
	@SuppressWarnings("unused")
	private final String PROPERTY_PASSWORD	= "password";
	
	private final String DRIVER_PACKAGE_NAME= "com.mysql.jdbc.Driver";

	private String url = null;
	private String userName = null;
	private String password = null;
	
	private Connection connection = null;
	
	/**
	 * Constructs a MySQL connector
	 * 
	 * @throws DriverNotLoadedException when the applications tries to load a driver but
	 * 									it cannot be done properly.
	 */
	public MySQLConnector() throws DriverNotLoadedException {
		try {
			this.loadDrive();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			throw new DriverNotLoadedException(e.getMessage(), e);			
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.connector.database.DataBaseConnector#setURL(java.lang.String)
	 */
	@Override
	public void setURL(String url) {
		this.url = url;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.connector.database.DataBaseConnector#setUserName(java.lang.String)
	 */
	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.connector.database.DataBaseConnector#setPassword(java.lang.String)
	 */
	@Override
	public void setPassword(String password) {
		this.password = password;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.connector.database.DataBaseConnector#connect()
	 */
	@Override
	public void connect() throws SQLException {
		String jdbcURL = this.JDBC_URL_PREFIX + this.url;
		this.connection = DriverManager.getConnection(jdbcURL, this.userName, this.password);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.connector.database.DataBaseConnector#disconnect()
	 */
	@Override
	public void disconnect() {
		if(null == this.connection){
			return;
		}
		
		try {
			this.connection.close();
		} catch (SQLException e) {
			//Do nothing. print for debugging
			e.printStackTrace();
		}finally {
			this.connection = null;
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DataBaseConnector#getURL()
	 */
	@Override
	public String getURL() {

		return this.url;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DataBaseConnector#getUserName()
	 */
	@Override
	public String getUserName() {

		return this.userName;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DataBaseConnector#getPassword()
	 */
	@Override
	public String getPassword() {

		return this.password;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.DataBaseConnector#getConnection()
	 */
	@Override
	public DatabaseConnection getConnection() {
		return com.digsarustudio.banana.database.mysql.MySQLConnection.builder().setConnection(this.connection).build();
	}

	/**
	 * To load the Driver Manager of MySQL
	 * 
	 * @throws ClassNotFoundException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 */
	private void loadDrive() throws InstantiationException, IllegalAccessException, ClassNotFoundException{
		Class.forName(this.DRIVER_PACKAGE_NAME).newInstance();
	}
}
