/**
 * 
 */
package com.digsarustudio.banana.database.mysql;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.dao.DataAccessObjectConfig;
import com.digsarustudio.banana.database.dao.EmptyConditionException;
import com.digsarustudio.banana.database.dao.EmptyDatasetException;
import com.digsarustudio.banana.database.dao.EmptyGroupsException;
import com.digsarustudio.banana.database.dao.EmptySortingCriteriaException;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.SortingCriteria;
import com.digsarustudio.banana.database.query.StatementKeywords;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The base class of other sub-type of {@link MySQLDataAccessObject}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class MySQLDataAccessObject implements com.digsarustudio.banana.database.dao.DataAccessObject, DataAccessObjectConfig {
	/**
	 * 
	 * The object builder for {@link MySQLDataAccessObject}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements com.digsarustudio.banana.database.dao.DataAccessObject.Builder{
		private List<SelectColumn>		columns					= null;
		private List<Condition>			conditions	 			= null;
		private List<SortingCriteria>	sortingCriteria			= null;
		private Boolean					isIgnoreDuplicatedData	= false;
		private List<Dataset>			datasets				= null;
		private Table					table					= null;
		private List<JoinTable>			joinTables				= null;
		private List<TableColumn>		groupBys				= null;
		
		
		private Query					query					= null;

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setQuery(com.digsarustudio.banana.database.query.Query)
		 */
		@Override
		public Builder setQuery(Query query) {
			this.query = query;			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setTable(com.digsarustudio.banana.database.table.Table)
		 */
		@Override
		public Builder setTable(Table table) {
			this.table = table;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setIgnoreDuplicateData()
		 */
		@Override
		public Builder setIgnoreDuplicateData() {
			this.isIgnoreDuplicatedData = true;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setIncludeDuplicateData()
		 */
		@Override
		public Builder setIncludeDuplicateData() {
			this.isIgnoreDuplicatedData = false;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#addColumn(com.digsarustudio.banana.database.query.SelectColumn)
		 */
		@Override
		public Builder addColumn(SelectColumn column) {
			if(null == this.columns){
				this.columns = new ArrayList<>();
			}
			
			this.columns.add(column);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setColumns(java.util.List)
		 */
		@Override
		public Builder setColumns(List<SelectColumn> columns) {
			this.columns = columns;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#addDataset(com.digsarustudio.banana.database.query.Dataset)
		 */
		@Override
		public Builder addDataset(Dataset dataset) {
			if(null == this.datasets){
				this.datasets = new ArrayList<>();
			}
			
			this.datasets.add(dataset);			
			return this;			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setDataset(java.util.List)
		 */
		@Override
		public Builder setDataset(List<Dataset> dataset) {
			this.datasets = dataset;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#addCondition(com.digsarustudio.banana.database.query.Condition)
		 */
		@Override
		public Builder addCondition(Condition condition) {
			if(null == this.conditions){
				this.conditions = new ArrayList<>();
			}
			
			this.conditions.add(condition);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setConditions(java.util.List)
		 */
		@Override
		public Builder setConditions(List<Condition> conditions) {
			this.conditions = conditions;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#addSortingCriteria(com.digsarustudio.banana.database.query.SortingCriteria)
		 */
		@Override
		public Builder addSortingCriteria(SortingCriteria criteria) {
			if(null == this.sortingCriteria){
				this.sortingCriteria = new ArrayList<>();
			}
			
			this.sortingCriteria.add(criteria);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setSortingCriteria(java.util.List)
		 */
		@Override
		public Builder setSortingCriteria(List<SortingCriteria> criteria) {
			this.sortingCriteria = criteria;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
		 */
		@Override
		public Builder addJoinTable(JoinTable joinTable) {
			if(null == this.joinTables){
				this.joinTables = new ArrayList<>();
			}
			
			this.joinTables.add(joinTable);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setJoinTables(java.util.List)
		 */
		@Override
		public Builder setJoinTables(List<JoinTable> joinTables) {
			this.joinTables = joinTables;			
			return this;
			
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#addGroupBy(com.digsarustudio.banana.database.table.TableColumn)
		 */
		@Override
		public Builder addGroupBy(TableColumn column) {
			if(null == this.groupBys){
				this.groupBys = new ArrayList<>();
			}
			
			this.groupBys.add(column);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#setGroupBy(java.util.List)
		 */
		@Override
		public Builder setGroupBy(List<TableColumn> columns) {
			this.groupBys = columns;			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public MySQLDataAccessObject build() {
			return new MySQLDataAccessObject(this);
		}

		/**
		 * @return the columns
		 */
		private List<SelectColumn> getColumns() {
			return columns;
		}

		/**
		 * @return the conditions
		 */
		private List<Condition> getConditions() {
			return conditions;
		}

		/**
		 * @return the sortingCriteria
		 */
		private List<SortingCriteria> getSortingCriteria() {
			return sortingCriteria;
		}

		/**
		 * @return the isIgnoreDuplicatedData
		 */
		private Boolean isIgnoreDuplicatedData() {
			return isIgnoreDuplicatedData;
		}

		/**
		 * @return the datasets
		 */
		private List<Dataset> getDatasets() {
			return datasets;
		}

		/**
		 * @return the table
		 */
		private Table getTable() {
			return table;
		}

		/**
		 * @return the joinTables
		 */
		private List<JoinTable> getJoinTables() {
			return joinTables;
		}

		/**
		 * @return the groupBys
		 */
		private List<TableColumn> getGroupBys() {
			return groupBys;
		}

		/**
		 * @return the query
		 */
		private Query getQuery() {
			return query;
		}
	}
	
	private List<SelectColumn>		columns					= null;
	private List<Condition>			conditions	 			= null;
	private List<SortingCriteria>	sortingCriteria			= null;
	private Boolean					isIgnoreDuplicatedData	= false;
	private List<Dataset>			datasets				= null;
	private Table					table					= null;
	private List<JoinTable>			joinTables				= null;
	private List<TableColumn>		groupBys				= null;
	
	
	private Query					query					= null;
	

	/**
	 * 
	 */
	private MySQLDataAccessObject(Builder builder) {
		this.setColumns(builder.getColumns());
		this.setConditions(builder.getConditions());
		this.setSortingCriteria(builder.getSortingCriteria());
		
		if(builder.isIgnoreDuplicatedData()){
			this.setIgnoreDuplicateData();
		}else{
			this.setIncludeDuplicateData();
		}
		
		this.setDataset(builder.getDatasets());
		this.setTable(builder.getTable());
		this.setJoinTables(builder.getJoinTables());
		this.setGroupBy(builder.getGroupBys());
		this.setQuery(builder.getQuery());
		
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#insert()
	 */
	@Override
	public void insert() throws SQLException {
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("INSERT INTO `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" (%s)", this.getInsertColumnQueryString(this.datasets)));
		builder.append(StringFormatter.format(" VALUES (%s)", this.getInsertValueQueryString(this.datasets)));
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#update()
	 */
	@Override
	public void update() throws SQLException {
		this.update(null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#update(java.lang.Integer)
	 */
	@Override
	public void update(Integer limit) throws SQLException {
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("UPDATE `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" SET %s", this.getUpdateDatasetQueryString(this.datasets)));
		builder.append(StringFormatter.format(" WHERE %s", this.getWhereConditionsQueryString(this.conditions)));
		
		if(null != this.sortingCriteria && !this.sortingCriteria.isEmpty()){
			builder.append(StringFormatter.format(" ORDER BY %s", this.getSortingQueryString(this.sortingCriteria)));			
		}
		
		if(null != limit){
			builder.append(StringFormatter.format(" LIMIT %d", limit));
		}
			
		this.query.executeUpdate(builder.toString());
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#fetch(java.lang.Integer, java.lang.Integer)
	 */
	@Override
	public QueryResult fetch(Integer cursor, Integer limit) throws SQLException {
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT");
		
		if( this.isIgnoreDuplicatedData ){
			builder.append(StringFormatter.format(" %s", StatementKeywords.Distinct.getValue()));
		}
		
		builder.append(StringFormatter.format(" %s", this.getSelectColumnQueryString(this.columns)));
		
		builder.append(StringFormatter.format(" FROM `%s`", this.table.getName()));
		
		if(null != this.joinTables && !this.joinTables.isEmpty()){
			builder.append(StringFormatter.format(" %s", this.getJoinTableQueryString(this.joinTables)));
			builder.append(StringFormatter.format(" ON (%s)", this.getJoinTableCriteriaQueryString(this.joinTables)));
		}
		
		if( null != this.conditions && !this.conditions.isEmpty() 
			&& !this.hasAggregateFunctionalCondition(this.conditions) ){
			builder.append(StringFormatter.format(" WHERE %s", this.getWhereConditionsQueryString(this.conditions)));
		}
		
		if(null != this.groupBys && !this.groupBys.isEmpty()){
			builder.append(StringFormatter.format(" GROUP BY %s", this.getGroupByQueryString(this.groupBys)));
		}
		
		if( null != this.conditions && !this.conditions.isEmpty() 
				&& this.hasAggregateFunctionalCondition(this.conditions) ){
				builder.append(StringFormatter.format(" HAVING %s", this.getWhereConditionsQueryString(this.conditions)));
		}
		
		if(null != this.sortingCriteria && !this.sortingCriteria.isEmpty()){
			builder.append(StringFormatter.format(" ORDER BY %s", this.getSortingQueryString(this.sortingCriteria)));
		}
		
		if(null != cursor && null != limit){
			builder.append(StringFormatter.format(" LIMIT %d, %d", cursor, limit));
		}else if( null != limit ){
			builder.append(StringFormatter.format(" LIMIT %d", limit));
		}
		builder.append(";");
		
		return this.query.executeQuery(builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#fetch()
	 */
	@Override
	public QueryResult fetch() throws SQLException {
		
		return this.fetch(null, null);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#delete()
	 */
	@Override
	public void delete() throws SQLException {
		this.delete(null);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#delete(java.lang.Integer)
	 */
	@Override
	public void delete(Integer limit) throws SQLException {
		StringBuilder builder = new StringBuilder();
		
		builder.append(StringFormatter.format("DELETE FROM `%s`", this.table.getName()));
		builder.append(StringFormatter.format(" WHERE %s", this.getWhereConditionsQueryString(this.conditions)));
		
		if(null != this.sortingCriteria && !this.sortingCriteria.isEmpty()){
			builder.append(StringFormatter.format(" ORDER BY %s", this.getSortingQueryString(this.sortingCriteria)));			
		}
		
		if(null != limit){
			builder.append(StringFormatter.format(" LIMIT %d", limit));
		}
		builder.append(";");
		
		this.query.executeUpdate(builder.toString());		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setQuery(com.digsarustudio.banana.database.query.Query)
	 */
	@Override
	public void setQuery(Query query) {
		this.query = query;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setTable(Table table) {
		this.table = table;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setIgnoreDuplicateData()
	 */
	@Override
	public void setIgnoreDuplicateData() {
		this.isIgnoreDuplicatedData = true;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setIncludeDuplicateData()
	 */
	@Override
	public void setIncludeDuplicateData() {
		this.isIgnoreDuplicatedData = false;

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#addColumn(com.digsarustudio.banana.database.query.SelectColumn)
	 */
	@Override
	public void addColumn(SelectColumn column) {
		if(null == this.columns){
			this.columns = new ArrayList<>();
		}

		this.columns.add(column);
	}	
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setColumns(java.util.List)
	 */
	@Override
	public void setColumns(List<SelectColumn> columns) {
		this.columns = columns;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#addDataSet(com.digsarustudio.banana.database.query.DataSet)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		if(null == this.datasets){
			this.datasets = new ArrayList<>();
		}

		this.datasets.add(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setDataset(java.util.List)
	 */
	@Override
	public void setDataset(List<Dataset> dataset) {
		this.datasets = dataset;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		if(null == this.conditions){
			this.conditions = new ArrayList<>();
		}
		
		this.conditions.add(condition);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#addSortingCriteria(com.digsarustudio.banana.database.query.SortingCriteria)
	 */
	@Override
	public void addSortingCriteria(SortingCriteria criteria) {
		if(null == this.sortingCriteria){
			this.sortingCriteria = new ArrayList<>();
		}

		this.sortingCriteria.add(criteria);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setSortingCriteria(java.util.List)
	 */
	@Override
	public void setSortingCriteria(List<SortingCriteria> criteria) {
		this.sortingCriteria = criteria;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTable(JoinTable joinTable) {
		if(null == this.joinTables){
			this.joinTables = new ArrayList<>();
		}
		
		this.joinTables.add(joinTable);

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setJoinTables(java.util.List)
	 */
	@Override
	public void setJoinTables(List<JoinTable> joinTables) {
		this.joinTables = joinTables;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#addGroupBy(com.digsarustudio.banana.database.table.TableColumn)
	 */
	@Override
	public void addGroupBy(TableColumn column) {
		if(null == this.groupBys){
			this.groupBys = new ArrayList<>();
		}

		this.groupBys.add(column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setGroupBy(java.util.List)
	 */
	@Override
	public void setGroupBy(List<TableColumn> columns) {
		this.groupBys = columns;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getQuery()
	 */
	@Override
	public Query getQuery() {
		return this.query;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getTable()
	 */
	@Override
	public Table getTable() {
		return this.table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#isIgnoreDuplicateData()
	 */
	@Override
	public Boolean isIgnoreDuplicateData() {
		return this.isIgnoreDuplicatedData;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getColumns()
	 */
	@Override
	public List<SelectColumn> getColumns() {

		return this.columns;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getDataset()
	 */
	@Override
	public List<Dataset> getDataset() {

		return this.datasets;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getConditions()
	 */
	@Override
	public List<Condition> getConditions() {

		return this.conditions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getSortingCriteria()
	 */
	@Override
	public List<SortingCriteria> getSortingCriteria() {

		return this.sortingCriteria;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getJoinTables()
	 */
	@Override
	public List<JoinTable> getJoinTables() {

		return this.joinTables;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#getGroupBys()
	 */
	@Override
	public List<TableColumn> getGroupBys() {
		return this.groupBys;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulatorConfig#clear()
	 */
	@Override
	public void clear() {
		this.columns = null;
		this.conditions = null;
		this.datasets = null;
		this.groupBys = null;
		this.isIgnoreDuplicatedData = false;
		this.joinTables = null;
		this.sortingCriteria = null;		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataAccessObject#getLastQuery()
	 */
	@Override
	public String getLastQuery() {
		return this.query.getLastQuery();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private String getInsertColumnQueryString(List<Dataset> datasets){
		if( (null == datasets) || datasets.isEmpty() ){
			throw new EmptyDatasetException();
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (Dataset data : datasets) {
			if( 0 != count++ ){
				builder.append(", ");
			}
			
			builder.append(StringFormatter.format("`%s`", data.getTableColumn().getColumn()));
		}
		
		return builder.toString();
	}
	
	private String getInsertValueQueryString(List<Dataset> datasets){
		if( (null == datasets) || datasets.isEmpty() ){
			throw new EmptyDatasetException();
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (Dataset data : datasets) {
			if( 0 != count++ ){
				builder.append(", ");
			}

			builder.append(data.getQueryValue());
		}
		
		return builder.toString();
	}	
	
	private String getUpdateDatasetQueryString(List<Dataset> datasets){
		if( (null == datasets) || datasets.isEmpty() ){
			throw new EmptyDatasetException("The dataset must be set for the insert or update");
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (Dataset data : datasets) {
			if( 0 != count++ ){
				builder.append(", ");
			}

			builder.append(data.getQueryDataSet());
		}		
		
		return builder.toString();
	}
	
	private String getWhereConditionsQueryString(List<Condition> conditions){
		if( (null == conditions) || conditions.isEmpty() ){
			throw new EmptyConditionException("The conditions must be set for the fetch or update");
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (Condition condition : conditions) {
			if( 0 != count++ ){
				builder.append(" ");
			}

			builder.append(condition.getQueryCondition());
		}		
		
		return builder.toString();
	}
	
	private String getSortingQueryString(List<SortingCriteria> sortingCriteria){
		if( (null == sortingCriteria) || sortingCriteria.isEmpty() ){
			throw new EmptySortingCriteriaException("The sorting criteria must be set for the sorting");
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (SortingCriteria criteria : sortingCriteria) {
			if( 0 != count++ ){
				builder.append(", ");
			}

			builder.append(criteria.getQuerySortingCriteria());
		}		
		
		return builder.toString();
	}

	private String getGroupByQueryString(List<TableColumn> columns){
		if( (null == columns) || columns.isEmpty() ){
			throw new EmptyGroupsException("The group must be set for the fetch or update");
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (TableColumn column : columns) {
			if( 0 != count++ ){
				builder.append(", ");
			}

			builder.append(StringFormatter.format("`%s`", column.getName()));
		}		
		
		return builder.toString();
	}	
	
	private String getSelectColumnQueryString(List<SelectColumn> columns){
		if( (null == columns) || columns.isEmpty() ){
			return "*";
		}
		
		StringBuilder builder = new StringBuilder();
		Integer count = 0;
		for (SelectColumn column : columns) {
			if( 0 != count++ ){
				builder.append(", ");
			}

			builder.append(column.getQuerySelectColumn());
		}		
		
		return builder.toString();
	}
	
	private Boolean hasAggregateFunctionalCondition(List<Condition> conditions){
		for (Condition condition : conditions) {
			if( !condition.hasAggregateFunction() ){
				continue;
			}
			
			return true;
		}
		
		return false;
	}
	
	private String getJoinTableQueryString(List<JoinTable> tables){
		if( (null == tables) || tables.isEmpty() ){
			throw new IllegalArgumentException("The join tables must be set before merging");
		}
		
		StringBuilder builder = new StringBuilder();
		builder.append(tables.get(0).getType().getValue());
		builder.append("(");
		
		Integer count = 0;
		for (JoinTable table : tables) {
			if( 0 != count++ ){
				builder.append(", ");
			}

			builder.append(table.getTable().getName());
		}	
		
		builder.append(")");
		return builder.toString();
	}
	
	private String getJoinTableCriteriaQueryString(List<JoinTable> tables){
		if( (null == tables) || tables.isEmpty() ){
			throw new IllegalArgumentException("The join tables must be set before merging");
		}
		
		StringBuilder builder = new StringBuilder();		
		
		for (JoinTable table : tables) {
			for(JoinCondition condition : table.getConditions()){
				builder.append(condition.getQueryJoinCondition());	
			}
		}	
		
		return builder.toString();
	}
}
