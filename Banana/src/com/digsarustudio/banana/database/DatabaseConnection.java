/**
 * 
 */
package com.digsarustudio.banana.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type contains the details of the connection from {@link DatabaseConnector} after
 * connecting to the database server.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2 Please use {@link com.digsarustudio.banana.database.io.DatabaseConnection} instead.
 */
public interface DatabaseConnection {
	/**
	 * 
	 * The builder for {@link DatabaseConnection}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DatabaseConnection> {
		/**
		 * Assigns the connection object
		 * 
		 * @param connection the connection object
		 * 
		 * @return The instance of this builder
		 */
		Builder setConnection(Connection connection);
	}
	
	/**
	 * Returns the statement object
	 * 
	 * @return the statement object
	 * 
	 * @exception SQLException
	 */
	Statement getStatement() throws SQLException;
	
	
	/**
	 * Returns the prepared statement object
	 * 
	 * @param query The query string
	 * 
	 * @return the prepared statement object
	 * 
	 * @throws SQLException
	 */
	PreparedStatement getPreparedStatement(String query) throws SQLException;
}
