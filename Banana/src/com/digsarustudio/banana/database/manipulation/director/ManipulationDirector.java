/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.director;

import java.util.List;

import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.manipulation.manipulator.InvalidDataException;
import com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy;

/**
 * The base class of {@link DataManipulationDirector}.<br>
 * The subclass only has to defined the data type of referential data and result data.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class ManipulationDirector<T, S> implements DataManipulationDirector<T, S> {
	private DataManipulationStrategy<T, S>	strategy	= null;
	private T								data		= null;
	
	/**
	 * 
	 */
	public ManipulationDirector() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataManipulationDirector#setDataManipulationStrategy(com.digsarustudio.banana.database.manipulator.DataManipulationStrategy)
	 */
	@Override
	public void setDataManipulationStrategy(DataManipulationStrategy<T, S> strategy) {
		this.strategy = strategy;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataManipulationDirector#setDataModel(java.lang.Object)
	 */
	@Override
	public void setDataModel(T data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataManipulationDirector#execute()
	 */
	@Override
	public List<S> execute() throws SQLManipulationException, DriverNotLoadedException, InvalidDataException {
		return this.strategy.execute(this.data);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.director.DataManipulationDirector#getSQLQuery()
	 */
	@Override
	public String getSQLQuery() {
		return this.strategy.getSQLQuery();
	}
	
}
