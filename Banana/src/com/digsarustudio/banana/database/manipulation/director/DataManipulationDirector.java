/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.director;

import java.util.List;

import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject;
import com.digsarustudio.banana.database.manipulation.manipulator.InvalidDataException;
import com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy;

/**
 * The sub-type can do a database manipulation by {@link DataManipulationStrategy} according to a data model.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface DataManipulationDirector<T, S> {
	/**
	 * Assigns a strategy which manipulates the referential data with a {@link DataAccessObject} to interact with 
	 * the database.<br>
	 * 
	 * @param strategy A strategy which manipulates the referential data with a {@link DataAccessObject} to interact with  
	 * 					the database.
	 */
	void setDataManipulationStrategy(DataManipulationStrategy<T, S> strategy);
	
	/**
	 * Assigns a referential data for this manipulation.
	 * 
	 * @param data A referential data for this manipulation.
	 */
	void setDataModel(T data);
	
	/**
	 * To execute this manipulation and returns the result of this manipulation
	 * 
	 * @return the results of this manipulation
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 * @throws InvalidDataException 
	 */
	List<S> execute() throws SQLManipulationException, DriverNotLoadedException, InvalidDataException;
	
	/**
	 * Returns the SQL query string which has been executed.<br>
	 * 
	 * @return the SQL query string which has been executed.
	 */
	String getSQLQuery();
}
