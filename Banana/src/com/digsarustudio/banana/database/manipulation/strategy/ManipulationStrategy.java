/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.strategy;

import java.util.List;

import com.digsarustudio.banana.database.DatabaseConfig;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory;
import com.digsarustudio.banana.database.manipulation.manipulator.InvalidDataException;
import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;

/**
 * The base class of {@link DataManipulationStrategy}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class ManipulationStrategy<T, S> implements DataManipulationStrategy<T, S> {
	/**
	 * For extension to use
	 */
	private DatabaseConnection 	connection		= null;
	private DatabaseManipulator	dbManipulator	= null;
	
	private AbstractDataAccessObjectFactory	manipulatorFactory = null;
	
	private Integer 			offset			= null;
	private Integer 			maxRowCount		= null;
	
	private Boolean				isUsingExternalConneciton = false;
	
	private String				sqlQuery		= null;
	
	/**
	 * 
	 */
	public ManipulationStrategy() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataManipulationStrategy#setDataAccessObjectFactory(com.digsarustudio.banana.database.manipulator.AbstractDataAccessObjectFactory)
	 */
	@Override
	public void setDataAccessObjectFactory(AbstractDataAccessObjectFactory factory) {
		this.manipulatorFactory = factory;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy#setDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)
	 */
	@Override
	public void setDatabaseManipulator(DatabaseManipulator manipulator) {
		//Close the internal database manipulator while an external one assigned.
		if(null != this.dbManipulator && !this.isUsingExternalConneciton){
			try {
				this.dbManipulator.close();
			} catch (SQLManipulationException e) {
				e.printStackTrace();
			}
		}
		
		this.dbManipulator = manipulator;
		this.isUsingExternalConneciton = true;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy#setOffset(java.lang.Integer)
	 */
	@Override
	public void setOffset(Integer offset) {
		this.offset = offset;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer count) {
		this.maxRowCount = count;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy#getSQLQuery()
	 */
	@Override
	public String getSQLQuery() {
		return this.sqlQuery;
	}
	
	/**
	 * Assigns a SQL query string to this strategy
	 * 
	 * @param sqlQuery a SQL query string for this strategy
	 */
	protected void setSQLQuery(String sqlQuery){
		this.sqlQuery = sqlQuery;
	}

	/**
	 * Returns the {@link DatabaseConnection} for this strategy.
	 * 
	 * @return the {@link DatabaseConnection} for this strategy.
	 */
	protected DatabaseConnection getConnection() throws DriverNotLoadedException{
		if(null == this.connection){
			this.connection = this.manipulatorFactory.createDatabaseConnection();
		}
		
		return this.connection;
	}
	
	/**
	 * Returns the {@link DatabaseManipulator} for this strategy.
	 * 
	 * @return the {@link DatabaseManipulator} for this strategy.
	 * @throws DriverNotLoadedException 
	 */
	protected DatabaseManipulator getDatabaseManipulator() throws DriverNotLoadedException{
		if(null == this.dbManipulator){
			this.dbManipulator = this.manipulatorFactory.createDatabaseManipulator(this.getConnection());	
		}
				
		return this.dbManipulator;
	}
	
	/**
	 * To connect to a particular database set by {@link DatabaseConfig} already.
	 * 
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	protected void connect() throws SQLManipulationException, DriverNotLoadedException{
		if( this.isUsingExternalConnection() ) {
			return;
		}
		
		this.getDatabaseManipulator().open();
	}
	
	/**
	 * To select a preset database in the database server
	 * 
	 * @throws SQLManipulationException
	 * @throws DriverNotLoadedException
	 */
	protected void selectDatabase() throws SQLManipulationException, DriverNotLoadedException {
		this.getDatabaseManipulator().selectDatabase();
	}
	
	/**
	 * Returns true if the connection between database is opened, otherwise false returned
	 * 
	 * @return true if the connection between database is opened, otherwise false returned
	 * 
	 * @throws DriverNotLoadedException
	 */
	protected Boolean isConnected() throws DriverNotLoadedException{
		return this.getDatabaseManipulator().isConnected();
	}
	
	/**
	 * To disconnect from a particular database set by {@link DatabaseConfig} already.
	 * 
	 * @throws SQLManipulationException 
	 * @throws DriverNotLoadedException 
	 */
	protected void disconnect() throws SQLManipulationException, DriverNotLoadedException{
		if( this.isUsingExternalConnection() ) {
			return;
		}
		
		this.getDatabaseManipulator().close();
	}

	/**
	 * Returns the offset of search
	 * 
	 * @return the offset of search
	 */
	protected Integer getOffset() {
		return offset;
	}

	/**
	 * Returns the maximum of row returned after a search, updated by a updating, or deleted by a deleting operation.
	 * 
	 * @return the maximum of row returned after a search, updated by a updating, or deleted by a deleting operation.
	 */
	protected Integer getMaxRowCount() {
		return maxRowCount;
	}

	/**
	 * Returns the statement compiler
	 * 
	 * @return the statement compiler
	 */
	protected StatementCompiler getStatementCompiler(){
		return this.manipulatorFactory.createStatementCompiler();
	}

	/**
	 * Returns the clause compiler
	 * 
	 * @return the clause compiler
	 */
	protected ClauseCompiler getClauseCompiler(){
		return this.manipulatorFactory.createClauseCompiler();
	}
	
	/**
	 * Returns true if the connection has been established from outside code, false if the connection has not been 
	 * established or it has been created internally.<br>
	 * 
	 * @return true if the connection has been established from outside code, false if the connection has not been 
	 * 		   established or it has been created internally
	 */
	protected Boolean isUsingExternalConnection(){
		return this.isUsingExternalConneciton;
	}
	
	/**
	 * Returns the {@link AbstractDataAccessObjectFactory} used in this class
	 * 
	 * @return the {@link AbstractDataAccessObjectFactory} used in this class
	 */
	protected AbstractDataAccessObjectFactory getFacotry(){
		return this.manipulatorFactory;
	}
		
	/**
	 * To validate incoming data, please call this function first to reduce the resources comsumption.<br>
	 * 
	 * @param data The incoming data to validate
	 * 
	 * @throws IllegalArgumentException If the data are not valid for this strategy.<br>
	 * 
	 * @deprecated	1.0.6	Please use {@link #validate(Object)} instead.<br>
	 */
	abstract protected void validateData(T data) throws IllegalArgumentException;
	
	/**
	 * To validate the incoming data by a specific validator.<br>
	 * This method should be abstract in the future.<br>
	 * 
	 * @param data The data object to validate
	 * @throws InvalidDataException	Thrown if the incoming data is not appropriate for the strategy.<br>
	 */
	protected void validate(T data) throws InvalidDataException{
		//TODO Be abstract for the new version.
	}
	
	/**
	 * Returns a particular table object
	 * 
	 * @return a particular table object
	 */
	abstract protected Table generateTable();

	/**
	 * To setup the tables for this strategy to query
	 */
	abstract protected void setupTables();
	
	/**
	 * To setup the columns for the query
	 */
	abstract protected void setupColumns();	
	
	/**
	 * Returns a list of dataset, null returned if there is no dataset provided.
	 * 
	 * @param source The source data object which provides the data for the query.
	 * 
	 * @return a list of dataset, null returned if there is no dataset provided.
	 */
	abstract protected List<Dataset> getDatasets(T source);
	
	/**
	 * Returns a list of SelectColumn, null returned if there is no SelectColumn provided.
	 * 
	 * @return a list of SelectColumn, null returned if there is no SelectColumn provided.
	 */
	abstract protected List<SelectColumn> getSelectColumns();
	
	/**
	 * Returns a list of join table, null returned if there is no join table provided.
	 * 
	 * @return a list of condition, null returned if there is no join table provided.
	 */
	abstract protected List<JoinTable> getJoinTables();
	
	/**
	 * Returns a list of condition, null returned if there is no condition provided.
	 * 
	 * @param source The source data object which provides the condition for the query.
	 * 
	 * @return a list of condition, null returned if there is no condition provided.
	 */
	abstract protected List<Condition> getConditions(T source);
	
	/**
	 * Returns a list of Aggregation, null returned if there is no Aggregation provided.
	 * 
	 * @return a list of Aggregation, null returned if there is no Aggregation provided.
	 */
	abstract protected List<Aggregation> getAggregations();
	
	/**
	 * Returns a list of Ordering, null returned if there is no Ordering provided.
	 * 
	 * @return a list of Ordering, null returned if there is no Ordering provided.
	 */
	abstract protected List<Ordering> getOrderings();

}
