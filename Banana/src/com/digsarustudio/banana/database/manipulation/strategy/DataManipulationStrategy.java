/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.strategy;

import java.util.List;

import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory;
import com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject;
import com.digsarustudio.banana.database.manipulation.manipulator.InvalidDataException;

/**
 * The sub-type of {@link DataManipulationStrategy} contains the algorithm that is used to setup a specific manipulation 
 * to the {@link DataAccessObject} and execute it to insert data into database, update data, delete data from database, 
 * and retrieve data for the client code.<br>
 *
 * @param T	The data type of the referential data.
 * @param S The data type to return converted from the raw data of the manipulation.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface DataManipulationStrategy<T, S> {
	/**
	 * Assigns a abstract data access object factory for this strategy to use.<br>
	 * 
	 * @param factory A abstract data access object factory for this strategy to use.
	 */
	void setDataAccessObjectFactory(AbstractDataAccessObjectFactory factory);
	
	/**
	 * Assigns an existing and connection opened {@link DatabaseManipulator} for this strategy.<br>
	 * When the external {@link DatabaseManipulator} has been set, the internal one will be closed immediately.<br>
	 * If a null has been given, it means this {@link DataManipulationStrategy} has no {@link DatabaseManipulator} for it 
	 * to use.<br>
	 * 
	 * @param manipulator An existing and connection opened {@link DatabaseManipulator} for this strategy.
	 */
	void setDatabaseManipulator(DatabaseManipulator manipulator);
	
	/**
	 * Assigns an offset for the search.
	 * 
	 * @param offset The offset for the search
	 */
	void setOffset(Integer offset);
	
	/**
	 * Assigns the maximum of row returned after a search, updated by a updating, or deleted by a deleting operation.
	 * 
	 * @param count the maximum of row returned after a search, updated by a updating, or deleted by a deleting operation.
	 */
	void setMaxRowCount(Integer count);	
	
	/**
	 * Returns the results of manipulation based on the incoming data.<br>
	 * null returned if the target data cannot be found in the search scenario or the target table 
	 * doesn't contain any data.<br>
	 * null will be returned in the inserting, deleting, and updating scenario as well.<br>
	 * 
	 * @param data The referential data for manipulation.
	 * 
	 * @return the results of manipulation based on the incoming data.<br>
	 * 			null returned if the target data cannot be found in the search scenario or the target 
	 * 			table doesn't contain any data.<br>
	 * 			null will be returned in the inserting, deleting, and updating scenario as well.<br>
	 * 
	 * @throws SQLManipulationException if it is failed to insert, update, delete, or fetch data from table.<br>
	 *  
	 * @throws DriverNotLoadedException if the target database cannot be connected.<br> 
	 * @throws InvalidDataException 
	 */
	List<S> execute(T data) throws SQLManipulationException, DriverNotLoadedException, InvalidDataException;
	
	/**
	 * Returns the SQL query string which has been executed.<br>
	 * 
	 * @return the SQL query string which has been executed.
	 */
	String getSQLQuery();
}
