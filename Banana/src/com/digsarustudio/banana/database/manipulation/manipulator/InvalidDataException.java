/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

/**
 * This exception indicates the code executor found the incoming data are invalid.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
@SuppressWarnings("serial")
public class InvalidDataException extends Exception {

	/**
	 * 
	 */
	public InvalidDataException() {

	}

	/**
	 * @param message
	 */
	public InvalidDataException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public InvalidDataException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public InvalidDataException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public InvalidDataException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
