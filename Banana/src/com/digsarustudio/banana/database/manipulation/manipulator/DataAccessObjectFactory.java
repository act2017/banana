/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

import com.digsarustudio.banana.database.DatabaseConfig;

/**
 * This is the base class implementing {@link AbstractDataAccessObjectFactory}.<br>
 * The subclass should extend this class for the implementation.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class DataAccessObjectFactory implements AbstractDataAccessObjectFactory {
	protected DatabaseConfig	config		= null;
	
	/**
	 * 
	 */
	public DataAccessObjectFactory() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.AbstractDataAccessObjectFactory#setDatabaseConfig(com.digsarustudio.banana.database.DatabaseConfig)
	 */
	@Override
	public void setDatabaseConfig(DatabaseConfig config) {
		this.config = config;
	}
}
