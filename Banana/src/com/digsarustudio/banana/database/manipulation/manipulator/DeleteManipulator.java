/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

import java.util.List;

import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.SQLManipulationException;
import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.DeleteStatement;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * This manipulator is used for deleting data from a particular table according to some criteria.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
@SuppressWarnings("deprecation")
public class DeleteManipulator extends DataManipulator implements DataAccessObject {
	/**
	 * 
	 * The object builder for {@link InsertManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements DataAccessObject.Builder {
		private DeleteManipulator result = null;

		public Builder() {
			this.result = new DeleteManipulator();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setPriority(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
		 */
		@Override
		public Builder setPriority(SQLSyntaxModifiers priority) {
			this.result.setPriority(priority);			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setDistinctType(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
		 */
		@Override
		public Builder setDistinctType(SQLSyntaxModifiers type) {
			this.result.setDistinctType(type);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setInsertInto(com.digsarustudio.banana.database.table.Table)
		 */
		@Override
		public Builder setInsertInto(Table table) {
			this.result.setInsertInto(table);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setSelectFrom(com.digsarustudio.banana.database.table.Table)
		 */
		@Override
		public Builder setSelectFrom(Table table) {
			this.result.setSelectFrom(table);
			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setSelectFrom(com.digsarustudio.banana.database.table.VirtualTable)
		 */
		@Override
		public Builder setSelectFrom(VirtualTable table) {
			this.result.setSelectFrom(table);
			
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setDeleteFrom(com.digsarustudio.banana.database.table.Table)
		 */
		@Override
		public Builder setDeleteFrom(Table table) {
			this.result.setDeleteFrom(table);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setUpdateTable(com.digsarustudio.banana.database.table.Table)
		 */
		@Override
		public Builder setUpdateTable(Table table) {
			this.result.setUpdateTable(table);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setDatasets(java.util.List)
		 */
		@Override
		public Builder setDatasets(List<Dataset> dataset) {
			this.result.setDatasets(dataset);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addDataset(com.digsarustudio.banana.database.query.Dataset)
		 */
		@Override
		public Builder addDataset(Dataset dataset) {
			this.result.addDataset(dataset);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setOnDuplicateKeyUpdateAssignments(java.util.List)
		 */
		@Override
		public Builder setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments) {
			this.result.setOnDuplicateKeyUpdateAssignments(assignments);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)
		 */
		@Override
		public Builder addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment) {
			this.result.addOnDuplicateKeyUpdateAssignment(assignment);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setSelectColumns(java.util.List)
		 */
		@Override
		public Builder setSelectColumns(List<SelectColumn> columns) {
			this.result.setSelectColumns(columns);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)
		 */
		@Override
		public Builder addSelectColumn(SelectColumn column) {
			this.result.addSelectColumn(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setJoinTable(java.util.List)
		 */
		@Override
		public Builder setJoinTable(List<JoinTable> tables) {
			this.result.setJoinTable(tables);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
		 */
		@Override
		public Builder addJoinTable(JoinTable table) {
			this.result.addJoinTable(table);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setJoinTableReference(java.util.List)
		 */
		@Deprecated
		@Override
		public Builder setJoinTableReference(List<JoinTable> tables) {
			this.result.setJoinTableReference(tables);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addJoinTableReferences(com.digsarustudio.banana.database.query.JoinTable)
		 */
		@Deprecated
		@Override
		public Builder addJoinTableReferences(JoinTable table) {
			this.result.addJoinTableReferences(table);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setJoinConditions(java.util.List)
		 */
		@Deprecated
		@Override
		public Builder setJoinConditions(List<JoinCondition> conditions) {
			this.result.setJoinConditions(conditions);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
		 */
		@Deprecated
		@Override
		public Builder addJoinCondition(JoinCondition condition) {
			this.result.addJoinCondition(condition);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setConditions(java.util.List)
		 */
		@Override
		public Builder setConditions(List<Condition> conditions) {
			this.result.setConditions(conditions);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addCondition(com.digsarustudio.banana.database.query.Condition)
		 */
		@Override
		public Builder addCondition(Condition condition) {
			this.result.addCondition(condition);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setAggregations(java.util.List)
		 */
		@Override
		public Builder setAggregations(List<Aggregation> aggregations) {
			this.result.setAggregations(aggregations);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addAggregation(com.digsarustudio.banana.database.query.Aggregation)
		 */
		@Override
		public Builder addAggregation(Aggregation aggregation) {
			this.result.addAggregation(aggregation);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setOrderings(java.util.List)
		 */
		@Override
		public Builder setOrderings(List<Ordering> orderings) {
			this.result.setOrderings(orderings);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#addOrdering(com.digsarustudio.banana.database.query.Ordering)
		 */
		@Override
		public Builder addOrdering(Ordering ordering) {
			this.result.addOrdering(ordering);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setOffset(java.lang.Integer)
		 */
		@Override
		public Builder setOffset(Integer offset) {
			this.result.setOffset(offset);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setMaxRowCount(java.lang.Integer)
		 */
		@Override
		public Builder setMaxRowCount(Integer rowCount) {
			this.result.setMaxRowCount(rowCount);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setCompiler(com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler)
		 */
		@Override
		public Builder setCompiler(ClauseCompiler compiler) {
			this.result.setCompiler(compiler);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setCompiler(com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler)
		 */
		@Override
		public Builder setCompiler(StatementCompiler compiler) {
			this.result.setCompiler(compiler);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject.Builder#setDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)
		 */
		@Override
		public Builder setDatabaseManipulator(DatabaseManipulator manipulator) {
			this.result.setDatabaseManipulator(manipulator);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DataAccessObject build() {
			return this.result;
		}
	}
	
	/**
	 * 
	 */
	private DeleteManipulator() {
		super();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setPriority(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setPriority(SQLSyntaxModifiers priority) {
		this.statement.setModifier(priority);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setDeleteFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setDeleteFrom(Table table) {
		this.statement.setDeleteFrom(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		this.statement.setConditions(conditions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		this.statement.addCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		this.statement.setOrderings(orderings);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		this.statement.addOrdering(ordering);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		this.statement.setMaxRowCount(rowCount);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.DataAccessObject#execute()
	 */
	@Override
	public void execute() throws SQLManipulationException {
		String sqlQuery = this.statement.construct();
		this.setSQLQuery(sqlQuery);
		this.dbManipulator.executeUpdate(sqlQuery);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.DataManipulator#init()
	 */
	@Override
	protected void init() {
		this.statement = new DeleteStatement();
	}

}
