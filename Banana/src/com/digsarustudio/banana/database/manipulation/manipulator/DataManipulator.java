/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

import java.util.List;

import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.ResultBundle;
import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * This is the base class of {@link DataAccessObject}.<br>
 * The sub-type of this class can execute a {@link SQLStatement} and get the {@link QueryResult} from {@link Query}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class DataManipulator implements DataAccessObject {
	protected DatabaseManipulator	dbManipulator	= null;
	protected SQLStatement			statement		= null;
	protected ResultBundle			result			= null;
	protected String				sqlQuery		= null;
	
	/**
	 * 
	 */
	public DataManipulator() {
		this.init();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject#setDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)
	 */
	@Override
	public void setDatabaseManipulator(DatabaseManipulator manipulator) {
		this.dbManipulator = manipulator;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setPriority(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setPriority(SQLSyntaxModifiers priority) {
		throw new UnsupportedOperationException("This data manipulator doesn't support Priority attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setDistinctType(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setDistinctType(SQLSyntaxModifiers type) {
		throw new UnsupportedOperationException("This data manipulator doesn't support distinct type attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setInsertInto(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setInsertInto(Table table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support table reference.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setSelectFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setSelectFrom(Table table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support table reference.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject#setSelectFrom(com.digsarustudio.banana.database.table.VirtualTable)
	 */
	@Override
	public void setSelectFrom(VirtualTable table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support virtual table reference.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setDeleteFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setDeleteFrom(Table table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support table reference attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setUpdateTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setUpdateTable(Table table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support table reference attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		throw new UnsupportedOperationException("This data manipulator doesn't support dataset attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		throw new UnsupportedOperationException("This data manipulator doesn't support dataset attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setOnDuplicateKeyUpdateAssignments(java.util.List)
	 */
	@Override
	public void setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments) {
		throw new UnsupportedOperationException("This data manipulator doesn't support duplicate key update attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)
	 */
	@Override
	public void addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment) {
		throw new UnsupportedOperationException("This data manipulator doesn't support duplicate key update attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setSelectColumns(java.util.List)
	 */
	@Override
	public void setSelectColumns(List<SelectColumn> columns) {
		throw new UnsupportedOperationException("This data manipulator doesn't support select column attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)
	 */
	@Override
	public void addSelectColumn(SelectColumn column) {
		throw new UnsupportedOperationException("This data manipulator doesn't support select column attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject#setJoinTable(java.util.List)
	 */
	@Override
	public void setJoinTable(List<JoinTable> tables) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join type attribute.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTable(JoinTable table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join type attribute.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)
	 */
	@Deprecated
	@Override
	public void setJoinType(TableJoinType type) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join type attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setJoinTableRefereces(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableRefereces(List<Table> tables) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join table reference attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addJoinTableReference(com.digsarustudio.banana.database.table.Table)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(Table table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join table reference attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.DataAccessObject#setJoinTableReference(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableReference(List<JoinTable> tables) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join table reference attribute.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.DataAccessObject#addJoinTableReferences(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Deprecated
	@Override
	public void addJoinTableReferences(JoinTable table) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join table reference.");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setJoinConditions(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinConditions(List<JoinCondition> conditions) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join condition attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
	 */
	@Deprecated
	@Override
	public void addJoinCondition(JoinCondition condition) {
		throw new UnsupportedOperationException("This data manipulator doesn't support join condition attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		throw new UnsupportedOperationException("This data manipulator doesn't support condition attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		throw new UnsupportedOperationException("This data manipulator doesn't support condition attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setAggregations(java.util.List)
	 */
	@Override
	public void setAggregations(List<Aggregation> aggregations) {
		throw new UnsupportedOperationException("This data manipulator doesn't support aggregation attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addAggregation(com.digsarustudio.banana.database.query.Aggregation)
	 */
	@Override
	public void addAggregation(Aggregation aggregation) {
		throw new UnsupportedOperationException("This data manipulator doesn't support aggregation attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		throw new UnsupportedOperationException("This data manipulator doesn't support ordering attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		throw new UnsupportedOperationException("This data manipulator doesn't support ordering attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setOffset(java.lang.Integer)
	 */
	@Override
	public void setOffset(Integer offset) {
		throw new UnsupportedOperationException("This data manipulator doesn't support offset attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		throw new UnsupportedOperationException("This data manipulator doesn't support max row count attribute.");

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)
	 */
	@Override
	public void setCompiler(ClauseCompiler compiler) {
		this.statement.setCompiler(compiler);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#setCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)
	 */
	@Override
	public void setCompiler(StatementCompiler compiler) {
		this.statement.setCompiler(compiler);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulator.DataAccessObject#getResult()
	 */
	@Override
	public ResultBundle getResult() {
		return this.result;
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject#getSQLQuery()
	 */
	@Override
	public String getSQLQuery() {
		return this.sqlQuery;
	}
	
	protected void setSQLQuery(String query){
		this.sqlQuery = query;
	}

	/**
	 * To initialize this manipulator
	 */
	abstract protected void init();
}
