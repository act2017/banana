/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator.google.cloud.sql;

import java.util.logging.Logger;

import com.digsarustudio.banana.database.CharacterSets;
import com.digsarustudio.banana.database.Collations;
import com.digsarustudio.banana.database.DatabaseConfig;
import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.TableManipulator;
import com.digsarustudio.banana.database.io.google.cloud.sql.GoogleCloudSQLConnection;
import com.digsarustudio.banana.database.io.mysql.MySQLDatabaseManipulator;
import com.digsarustudio.banana.database.io.mysql.MySQLTableManipulator;
import com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory;
import com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObjectFactory;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.claue.compiler.mysql.MySQLClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.mysql.MySQLStatementCompiler;

/**
 * This class is used for creating {@link DatabaseConnection}, {@link DatabaseManipulator}, {@link StatementCompiler}, and 
 * {@link ClauseCompiler} which supports MySQL database on Google Cloud SQL.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class GoogleCloudSQLDataAccessObjectFactory extends DataAccessObjectFactory implements AbstractDataAccessObjectFactory {
	Logger logger = Logger.getLogger(this.getClass().getName());
	
	/**
	 * 
	 */
	public GoogleCloudSQLDataAccessObjectFactory() {

	}
	
	public GoogleCloudSQLDataAccessObjectFactory(DatabaseConfig config) {
		this.setDatabaseConfig(config);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createDatabaseConnection()
	 */
	@Override
	public DatabaseConnection createDatabaseConnection() throws DriverNotLoadedException {
		if(null == this.config) {
			logger.warning("No config for Google Cloud SQL Data Access Object Factory");
		}else {
			logger.info("Creating Google Cloud SQL Data Access Object Factory by DB: " + this.config.getCloudDatabaseName()
						+ ", Instance: " + this.config.getCloudInstanceConnectionName()
						+ ", user: " + this.config.getUserName()
						+ ", password: " + this.config.getPassword());
		}
		
		return GoogleCloudSQLConnection.builder().setDatabaseName(this.config.getCloudDatabaseName())
												 .setInstanceConnectionName(this.config.getCloudInstanceConnectionName())
												 .setUserName(this.config.getUserName())
												 .setPassword(this.config.getPassword())
												 .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createDatabaseManipulator()
	 */
	@Override
	public DatabaseManipulator createDatabaseManipulator() throws DriverNotLoadedException {
		return this.createDatabaseManipulator(this.createDatabaseConnection());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createDatabaseManipulator(com.digsarustudio.banana.database.io.DatabaseConnection)
	 */
	@Override
	public DatabaseManipulator createDatabaseManipulator(DatabaseConnection connection) {
		return MySQLDatabaseManipulator.builder().setName(this.config.getDefaultDB())
												 .setConnection(connection)
												 .setCharacterSets(CharacterSets.UTF8Unicode)
												 .setCollations(Collations.UTF8Unicode)
												 .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createTableManipulator()
	 */
	@Override
	public TableManipulator createTableManipulator() throws DriverNotLoadedException {
		return this.createTableManipulator(this.createDatabaseManipulator());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createTableManipulator(com.digsarustudio.banana.database.io.DatabaseManipulator)
	 */
	@Override
	public TableManipulator createTableManipulator(DatabaseManipulator databaseManipulator) {
		return MySQLTableManipulator.builder().setDatabaseManipulator(databaseManipulator)
											  .setDatabaseEngineType(DatabaseEngineType.InnoDB)
											  .setPreventErrorIfTableExists()
											  .build();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createStatementCompiler()
	 */
	@Override
	public StatementCompiler createStatementCompiler() {
		return new MySQLStatementCompiler();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.manipulation.manipulator.AbstractDataAccessObjectFactory#createClauseCompiler()
	 */
	@Override
	public ClauseCompiler createClauseCompiler() {
		return new MySQLClauseCompiler();
	}

}
