/**
 * 
 */
package com.digsarustudio.banana.database.manipulation.manipulator;

import com.digsarustudio.banana.database.DatabaseConfig;
import com.digsarustudio.banana.database.DriverNotLoadedException;
import com.digsarustudio.banana.database.io.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.database.io.TableManipulator;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;

/**
 * The sub-type generates a particular {@link DataAccessObject} for the specific database, such as 
 * MySQL, MS-SQL, and Oracle.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface AbstractDataAccessObjectFactory {
	/**
	 * Assigns a configuration of database for the connection and manipulation
	 * 
	 * @param config A configuration of database for the connection and manipulation
	 */
	void setDatabaseConfig(DatabaseConfig config);
	
	/**
	 * To create a {@link DatabaseConnection} for a particular database.<br>
	 * 
	 * @return A {@link DatabaseConnection} for a particular database.
	 * @throws DriverNotLoadedException 
	 */
	DatabaseConnection 	createDatabaseConnection() throws DriverNotLoadedException;
	
	/**
	 * To create a {@link DatabaseManipulator} for a particular database.<br>
	 * 
	 * @return A {@link DatabaseManipulator} for a particular database.
	 * @throws DriverNotLoadedException 
	 */
	DatabaseManipulator	createDatabaseManipulator() throws DriverNotLoadedException;
	
	/**
	 * To create a {@link DatabaseManipulator} for a particular database with an external connection.<br>
	 * 
	 * @param coonection The connection of a particular database server.
	 * 
	 * @return A {@link DatabaseManipulator} for a particular database.
	 */
	DatabaseManipulator	createDatabaseManipulator(DatabaseConnection connection);
	
	/**
	 * To create a {@link TableManipulator} for a particular database.<br>
	 * 
	 * @return A {@link TableManipulator} for a particular database.
	 * @throws DriverNotLoadedException 
	 */
	TableManipulator	createTableManipulator() throws DriverNotLoadedException;
	
	/**
	 * To create a {@link TableManipulator} for a particular database with an external database manipulator .<br>
	 * 
	 * @return A {@link TableManipulator} for a particular database.
	 */
	TableManipulator	createTableManipulator(DatabaseManipulator databaseManipulator);
	
	/**
	 * To create a {@link StatementCompiler} for a particular database.<br>
	 * 
	 * @return A {@link StatementCompiler} for a particular database.
	 */
	StatementCompiler	createStatementCompiler();
	
	/**
	 * To create a {@link ClauseCompiler} for a particular database.<br>
	 * 
	 * @return A {@link ClauseCompiler} for a particular database.
	 */
	ClauseCompiler		createClauseCompiler();
}
