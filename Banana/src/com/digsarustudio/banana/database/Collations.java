/**
 * 
 */
package com.digsarustudio.banana.database;

/**
 * The collations set used in MySQL
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum Collations {
	UTF8Unicode("utf8_general_ci")
	,UTF16Unicode("utf16_general_ci")
	;
	
	private String value = null;
	
	private Collations(String charset){
		this.value = charset;
	}
	
	public String getValue(){
		return this.value;
	}
}
