/**
 * 
 */
package com.digsarustudio.banana.database.table;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * This class contains the name of table and column used by database.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class SimpleTableColumn {
	/**
	 * 
	 * The object builder for {@link SimpleTableColumn}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<SimpleTableColumn> {
		private String table	= null;
		private String column	= null;
		
		/**
		 * @return the table
		 */
		private String getTable() {
			return table;
		}

		/**
		 * The name of table
		 * 
		 * @param table the table to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setTable(String name) {
			this.table = name;
			return this;
		}

		/**
		 * Returns the name of the column for the SELECT Statement
		 * 
		 * @return the name of the column for the SELECT Statement
		 */
		private String getColumn() {
			return column;
		}

		/**
		 * Assigns a name of the column for the SELECT Statement
		 * 
		 * @param name the column to set
		 */
		public Builder setColumn(String name) {
			this.column = name;
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SimpleTableColumn build() {
			return new SimpleTableColumn(this);
		}

	}
	
	private String table	= null;
	private String column	= null;
	
	private SimpleTableColumn(Builder builder){
		this.setTable(builder.getTable());
		this.setColumn(builder.getColumn());
	}
	
	/**
	 * Returns the name of the table this column belongs to
	 * 
	 * @return the name of the table this column belongs to
	 */
	public String getTable() {
		return table;
	}

	/**
	 * Assigns a name of table this column belongs to
	 * 
	 * @param table the table to set
	 */
	public void setTable(String table) {
		this.table = table;
	}

	/**
	 * Returns the name of the column for the SELECT Statement
	 * 
	 * @return the name of the column for the SELECT Statement
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * Assigns a name of the column for the SELECT Statement
	 * 
	 * @param name the column to set
	 */
	public void setColumn(String name) {
		this.column = name;
	}

	/**
	 * Returns the string contains table name and column name<br>
	 * <br>
	 * For example:<br>
	 * column = identifier<br>
	 * The returning is <b>`identifier`</b><br>
	 * <br>
	 * Or<br>
	 * table = inventory<br>
	 * column = id<br>
	 * The returning is <b>inventory.id</b><br>
	 * 
	 * @return the string contains table name and column name
	 * 
	 */
	public String getQuerySimpleTableColumn(){
		StringBuilder builder = new StringBuilder();
		
		if( null == this.table || table.isEmpty() ){
			builder.append(StringFormatter.format("`%s`", this.column));
		}else{
			builder.append(this.getTableColumnName());
		}
		
		return builder.toString();
	}
	
	public String getTableColumnName(){
		StringBuilder builder = new StringBuilder();
		if(null != this.table){
			builder.append(this.table);
			builder.append(".");
		}
		builder.append(this.column);
		
		return builder.toString();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
