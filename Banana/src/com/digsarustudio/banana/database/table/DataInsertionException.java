/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * To indicate a data cannot be written into a table.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DataInsertionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3792220246478562663L;

	/**
	 * 
	 */
	public DataInsertionException() {

	}

	/**
	 * @param message
	 */
	public DataInsertionException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public DataInsertionException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public DataInsertionException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DataInsertionException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
