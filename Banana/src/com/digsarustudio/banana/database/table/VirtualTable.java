/**
 * 
 */
package com.digsarustudio.banana.database.table;

import com.digsarustudio.banana.database.query.TemporaryTable;
import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.database.query.statement.SelectStatement;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type of the {@link VirtualTable} contains its name and a {@link SelectStatement} which represents a table 
 * for the selection manipulation from the client code temporarily.<br>
 * This table can be used for a table join manipulation when the left table should be limited in a count. This might 
 * provide more efficient than using a subquery in the where clause.(To be tested)<br>
 * If the data in a virtual table are fixed, please use {@link ViewTable}.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * 
 * @deprecated	1.0.2 Please use {@link TemporaryTable} instead
 */
public interface VirtualTable {
	/**
	 * 
	 * The builder for {@link VirtualTable}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static interface Builder extends ObjectBuilder<VirtualTable> {
		/**
		 * Assigns a name for this virtual table.
		 * 
		 * @param name The name of this virtual table
		 * 
		 * @return The instance of this builder
		 */
		Builder setName(String name);
		
		/**
		 * Assigns a statement used to generate a virtual table.
		 * 
		 * @param statement A statement used to generate a virtual table.
		 * 
		 * @return The instance of this builder
		 */
		Builder setStatement(SQLStatement statement);
	}
	
	/**
	 * Returns the name of this virtual table
	 * 
	 * @return the name of this virtual table
	 */
	String getName();
	
	/**
	 * Returns the statement which creates the data as virtual table
	 *  
	 * @return the statement which creates the data as virtual table
	 */
	SQLStatement getStatement();
}
