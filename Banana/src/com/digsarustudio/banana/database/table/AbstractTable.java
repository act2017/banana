/**
 * 
 */
package com.digsarustudio.banana.database.table;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * The base class of {@link Table}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class AbstractTable implements Table{
	private String						name	= null;
	private Map<String, TableColumn>	columns = new HashMap<>();

	/**
	 * Constructs an abstract table object
	 * 
	 * @param name The name of this table
	 */
	public AbstractTable(String name) {
		this.setName(name);
		
		this.initColumns();
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		this.name = name;
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getName()
	 */
	@Override
	public String getName() {
		
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getColumn(java.lang.Integer)
	 */
	@Override
	public TableColumn getColumn(Integer index) {
		if(null == this.columns){
			return null;
		}
		
		if(index >= this.columns.size()){
			return null;
		}

		Integer count = 0;
		Iterator<TableColumn> iterator = this.columns.values().iterator();
		while(iterator.hasNext()){
			TableColumn column = iterator.next();
			
			if( index != count++ ){
				continue;
			}
			
			return column;
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getColumn(java.lang.String)
	 */
	@Override
	public TableColumn getColumn(String name) {
		if(!this.columns.containsKey(name)){
			return null;
		}
		
		return this.columns.get(name);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getPrimaryKey(java.lang.Integer)
	 */
	@Override
	public TableColumn getPrimaryKey(Integer index) {
		if(null == this.columns){
			return null;
		}
		
		Integer count = 0;
		Iterator<TableColumn> iterator = this.columns.values().iterator();
		while(iterator.hasNext()){
			TableColumn column = iterator.next();
			if( !column.isPrimaryKey() ){
				continue;
			}
			
			if( index != count++ ){
				continue;
			}
			
			return column;
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getPrimaryKey(java.lang.String)
	 */
	@Override
	public TableColumn getPrimaryKey(String name) {
		if(!this.columns.containsKey(name)){
			return null;
		}
		
		TableColumn column = this.columns.get(name);
		
		return (!column.isPrimaryKey() ? null : column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getForeignKey(java.lang.Integer)
	 */
	@Override
	public TableColumn getForeignKey(Integer index) {
		if(null == this.columns){
			return null;
		}
		
		Integer count = 0;
		Iterator<TableColumn> iterator = this.columns.values().iterator();
		while(iterator.hasNext()){
			TableColumn column = iterator.next();
			if( !column.isForeignKey() ){
				continue;
			}
			
			if( index != count++ ){
				continue;
			}
			
			return column;
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getForeignKey(java.lang.String)
	 */
	@Override
	public TableColumn getForeignKey(String name) {
		if(!this.columns.containsKey(name)){
			return null;
		}
		
		TableColumn column = this.columns.get(name);
		
		return (!column.isForeignKey() ? null : column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getUniqueKey(java.lang.Integer)
	 */
	@Override
	public TableColumn getUniqueKey(Integer index) {
		if(null == this.columns){
			return null;
		}
		
		Integer count = 0;
		Iterator<TableColumn> iterator = this.columns.values().iterator();
		while(iterator.hasNext()){
			TableColumn column = iterator.next();
			if( !column.isUniqueKey() ){
				continue;
			}
			
			if( index != count++ ){
				continue;
			}
			
			return column;
		}
		
		return null;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#getUniqueKey(java.lang.String)
	 */
	@Override
	public TableColumn getUniqueKey(String name) {
		if(!this.columns.containsKey(name)){
			return null;
		}
		
		TableColumn column = this.columns.get(name);
		
		return (!column.isUniqueKey() ? null : column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#addColumn(com.digsarustudio.banana.database.TableColumn)
	 */
	@Override
	public void addColumn(TableColumn column) {
		if(this.columns.containsKey(column.getName())){
			//Throw exception
			return;
		}
		
		this.columns.put(column.getName(), column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#isSupportFullTextSearch()
	 */
	@Override
	public Boolean isSupportFullTextSearch() {
		if(null == this.columns){
			return null;
		}
		
		Iterator<TableColumn> iterator = this.columns.values().iterator();
		while(iterator.hasNext()){
			TableColumn column = iterator.next();
			if( !column.isSupportFullTextSearch() ){
				continue;
			}
			
			return true;
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#hasPrimaryKey()
	 */
	@Override
	public Boolean hasPrimaryKey() {
		return (null != this.getPrimaryKey(0));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#hasForeignKey()
	 */
	@Override
	public Boolean hasForeignKey() {
		return (null != this.getForeignKey(0));
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.Table#hasUniqueKey()
	 */
	@Override
	public Boolean hasUniqueKey() {
		return (null != this.getUniqueKey(0));
	}

	/**
	 * To setup the column of the table
	 */
	protected abstract void initColumns();
}
