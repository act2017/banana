/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * The data types of column
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum TableColumnDataTypes {
	Boolean("BOOLEAN", "BOOLEAN")
	
	/**
	 * The BIT data type is used to store bit values. A type of BIT(M) enables storage of M-bit values. 
	 * M can range from 1 to 64.
	 */
	, Bit("BIT", "%d")
	
	/**
	 * <table>
	 * 	<tr>
	 * 		<td>Storage(BYTES)</td>
	 * 		<td>: 2</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Signed</td>
	 * 		<td>: -32768 ~ 32767</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Unsigned</td>
	 * 		<td>: 0 ~ 65535</td>
	 * 	</tr>
	 * </table>
	 * 
	 */
	, SmallInteger("SMALLINT", "%d")
	 
	/**
	 * <table>
	 * 	<tr>
	 * 		<td>Storage(BYTES)</td>
	 * 		<td>: 3</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Signed</td>
	 * 		<td>: -8388608 ~ 8388607</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Unsigned</td>
	 * 		<td>: 0 ~ 1677215</td>
	 * 	</tr>
	 * </table>
	 * 
	 */
	,MediumInteger("MEDIUMINT", "%d")
	 
	/**
	 * <table>
	 * 	<tr>
	 * 		<td>Storage(BYTES)</td>
	 * 		<td>: 4</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Signed</td>
	 * 		<td>: -2147483648 ~ 2147483647</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Unsigned</td>
	 * 		<td>: 0 ~ 4294967295</td>
	 * 	</tr>
	 * </table>
	 * 
	 */
	,Integer("INT", "%d")
	 
	/**
	 * <table>
	 * 	<tr>
	 * 		<td>Storage(BYTES)</td>
	 * 		<td>: 8</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Signed</td>
	 * 		<td>: -9223372036854775808 ~ 9223372036854775807</td>
	 * 	</tr>
	 * 	<tr>
	 * 		<td>Unsigned</td>
	 * 		<td>: 0 ~ 18446744073709551615</td>
	 * 	</tr>
	 * </table>
	 * 
	 */
	,BigInteger("BIGINT", "%d")
	
	,Decimal("DECIMAL", "%d, %d")
	,Float("FLOAT", "%d")
	
	,Binary("BINARY", "%d")	
	
	
	,VarChar("VARCHAR","%d")
	,Text("TEXT", "%d")
	
	/**
	 * A BLOB column with a maximum length of 255 (28 − 1) bytes. 
	 * Each TINYBLOB value is stored using a 1-byte length prefix that indicates the number of bytes in the value.
	 */
	,TinyBlob("TINYBLOB")	

	/**
	 * A BLOB column with a maximum length of 65,535 (216 − 1) bytes.
	 * Each BLOB value is stored using a 2-byte length prefix that indicates the number of bytes in the value.
	 * <br>
	 * An optional length M can be given for this type. 
	 * If this is done, MySQL creates the column as the smallest BLOB type large enough to hold values M bytes long.
	 */
	,Blob("BLOB")		
	
	/**
	 * A BLOB column with a maximum length of 16,777,215 (224 − 1) bytes. 
	 * 
	 * Each MEDIUMBLOB value is stored using a 3-byte length prefix that indicates the number of bytes 
	 * in the value.
	 */
	,MediumBlob("MEDIUMBLOB")

	/**
	 * A BLOB column with a maximum length of 4,294,967,295 or 4GB (232 − 1) bytes. 
	 * The effective maximum length of LONGBLOB columns depends on the configured maximum packet 
	 * size in the client/server protocol and available memory. 
	 * Each LONGBLOB value is stored using a 4-byte length prefix that indicates the number of bytes 
	 * in the value.
	 */
	,LongBlob("LONGBLOB")

	,Enum("ENUM")
	
	,Date("DATE", "%d")
	,DateTime("DATE_TIME", "%d")
	,Timestamp("TIMESTAMP", "%d")
	
	;
	
	private String name = null;
	private String format = null;
	
	private TableColumnDataTypes(String value){
		this.name = value;
	}
	
	private TableColumnDataTypes(String name, String format){
		this.name = name;
		this.format = format;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getFormat(){
		return this.format;
	}
	
	/**
	 * Returns the name and additional information of this data type
	 * 
	 * @return the name and additional information of this data type
	 */
	public String toString(){
		if(null == this.format){
			return this.name;
		}
		
		return this.name + "(" + this.format + ")";
	}
	
	/**
	 * To get the enumeration of {@link TableColumnDataTypes} by the name of data type.
	 * 
	 * @param name The name of data type.
	 * 
	 * @return the enumeration of {@link TableColumnDataTypes} the given name mapped
	 * 
	 * @throws UnSupportedColumnDataType When the incoming name does not exist in this enumeration. 
	 */
	public static final TableColumnDataTypes fromName(String name) throws UnSupportedColumnDataType{
		TableColumnDataTypes[] types = TableColumnDataTypes.values();
		
		TableColumnDataTypes rtn = null;
		for (TableColumnDataTypes type : types) {
			if( !name.toUpperCase().equals(type.getName()) ){
				continue;
			}
			
			rtn = type;
			break;
		}
		
		if( null == rtn ){
			throw new UnSupportedColumnDataType("The name of data type[" + name + "] is not supported.");
		}
		
		return rtn;
	}
}
