/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * To indicate the deletion operation to a table is failure.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class DataDeletionException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4711340463268863308L;

	/**
	 * 
	 */
	public DataDeletionException() {

	}

	/**
	 * @param arg0
	 */
	public DataDeletionException(String arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 */
	public DataDeletionException(Throwable arg0) {
		super(arg0);

	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public DataDeletionException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	/**
	 * @param arg0
	 * @param arg1
	 * @param arg2
	 * @param arg3
	 */
	public DataDeletionException(String arg0, Throwable arg1, boolean arg2, boolean arg3) {
		super(arg0, arg1, arg2, arg3);

	}

}
