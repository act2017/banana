/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * The system default values for the table column
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum DefaultValues {
	  unknown("UNKNOWN")
	, Null("NULL")
	, CurrentTimestamp("CURRENT_TIMESTAMP")
	, False("FALSE")
	, True("TRUE")
	, isNull("IS NULL")
	, isNotNull("IS NOT NULL")
	;
	
	private String value = null;
	
	private static DefaultValues[] VALUES = values();
	
	private DefaultValues(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public static DefaultValues fromValue(String value){
		DefaultValues rtn = unknown;
		
		for (DefaultValues dv : VALUES) {
			if( !dv.getValue().equals(value.toUpperCase()) ){
				continue;
			}
			
			rtn = dv;
			break;
		}
		
		return rtn;
	}
}
