/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * This exception indicates the data type of column has not been supported so far.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class UnSupportedColumnDataType extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4820899193435264224L;

	/**
	 * 
	 */
	public UnSupportedColumnDataType() {

	}

	/**
	 * @param message
	 */
	public UnSupportedColumnDataType(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public UnSupportedColumnDataType(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public UnSupportedColumnDataType(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public UnSupportedColumnDataType(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
