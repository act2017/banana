/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * To represent the referential actions of foreign key constraint
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum ReferentialActions {
	  Cascade("CASCADE")
	, Restrict("RESTRICT")
	, NoAction("NO ACTION")
	, SetNull("SET NULL")
	, SetDefault("SET DEFAULT")
	;
	
	private String value = null;	
	
	private ReferentialActions(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
