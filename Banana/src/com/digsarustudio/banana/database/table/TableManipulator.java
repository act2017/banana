/**
 * 
 */
package com.digsarustudio.banana.database.table;

import java.sql.SQLException;

import com.digsarustudio.banana.database.DatabaseEngineType;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The manipulator of the table in the database
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.io.TableManipulator} instead.
 */
public interface TableManipulator {
	/**
	 * 
	 * The builder for {@link TableManipulator}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<TableManipulator> {
		Builder setQuery(Query query);
		Builder setTable(Table table);
		Builder setDatabaseEngineType(DatabaseEngineType type);
		Builder setPreventErrorIfTableExists();
	}
	
	/**
	 * Assigns a table for this manipulator to operate.
	 * 
	 * @param table The table for this manipulator to operate.
	 */
	void setTable(Table table);
	
	/**
	 * Create a table for database
	 * 
	 * @throws SQLException
	 */
	void createTable() throws SQLException;
	
	/**
	 * @deprecated 1.0.0 alter is syntax, not a specific operation working on the table.
	 */
	void alterTable();
	
	/**
	 * To rename this table
	 * 
	 * @param name The original name for the table to rename
	 * @throws SQLException 
	 */
	void renameTable(String originalName) throws SQLException;
	
	/**
	 * To rename this table
	 * 
	 * @param name The new details for original table
	 * @throws SQLException 
	 */
	void renameTable(Table originalTable) throws SQLException;
	
	/**
	 * To add a column
	 * 
	 * @param column The details of the new column
	 * @throws SQLException 
	 */
	void addColumn(TableColumn column) throws SQLException;
	
	/**
	 * To modify the attributes of a column, excluding the name of column.<br>
	 * <br>
	 * Before calling this method, please ensure that key constraints are applied properly and the name of column is existed in the table, otherwise
	 * a {@link SQLException} will be thrown.<br>
	 * 
	 * @param column The new details of a specific column to setup.
	 * @throws SQLException 
	 */
	void modifyColumn(TableColumn column) throws SQLException;
	
	/**
	 * To change the name of target column.<br>
	 * Normally, {@link Table} contains the latest details of columns so that just indicating 
	 * which is the target to be modified as a parameter for this method.<br>
	 * <br>
	 * 
	 * @param originalColumnName The name of the original column. The name of this column should be specified.
	 * @param newColumn The details of the new column to set.
	 * @throws SQLException 
	 */
	void changeColumnName(String originalColumnName, TableColumn newColumn) throws SQLException;
	
	/**
	 * To change the name of target column.<br>
	 * 
	 * <br>
	 * 
	 * @param originalColumn The details of the original column. The name of this column should be specified.
	 * @param newColumn The details of the new column to set.
	 */
	void changeColumnName(TableColumn originalColumn, TableColumn newColumn) throws SQLException;
	
	/**
	 * Returns the details of a particular column from the database.<br>
	 * Only data type attribute works properly. Do not use this method 
	 * to retrieve the details of column from a particular table.
	 * <br>
	 * Note:<br>
	 * The key constraint is not really available now. Only primary keys can be parsed well, and
	 * non-null unique keys as well. The foreign keys cannot be recognized so far. It should be fixed if
	 * this method is used to retrieve the details of column from database.<br>
	 * <br>
	 * 
	 * @param name The name of the particular column to retrieve the details of it.
	 * 
	 * @return the details of a particular column from the database.
	 * @throws SQLException 
	 */
	TableColumn getColumn(String name) throws SQLException;
	
	/**
	 * Returns true if the specific column exists, otherwise false returned.
	 * 
	 * @param name The name of the column to check
	 * 
	 * @return true if the specific column exists, otherwise false returned.
	 * @throws SQLException 
	 */
	Boolean isColumnExisting(String name) throws SQLException;
	
	/**
	 * Returns true if the specific column exists, otherwise false returned.
	 * 
	 * @param column The column to check
	 * 
	 * @return true if the specific column exists, otherwise false returned.
	 * @throws SQLException 
	 */
	Boolean isColumnExisting(TableColumn column) throws SQLException;
	
	/**
	 * To register a column as a primary key
	 * 
	 * @param column The details of column
	 * @throws SQLException 
	 */
	void registerColumnAsPrimaryKey(TableColumn column) throws SQLException;
	
	/**
	 * To remove all of the primary keys
	 * @throws SQLException 
	 */
	void unregisterPrimaryKeys() throws SQLException;
	
	/**
	 * To register a column as a foreign key with the constraints.<br>
	 * 
	 * @param column The details of column
	 * @throws SQLException 
	 */
	void registerColumnAsForeignKey(TableColumn column) throws SQLException;
	
	/**
	 * To remove foreign key constraint from a column.<br>
	 * 
	 * @param column The details of column
	 * @throws SQLException 
	 */
	void unregisterColumnFromForeignKey(TableColumn column) throws SQLException;
	
	/**
	 * To register a column as a unique key
	 * 
	 * @param column The details of column
	 * @throws SQLException 
	 */
	void registerColumnAsUniqueKey(TableColumn column) throws SQLException;
	
	/**
	 * To remove unique key  from a column.<br>
	 * 
	 * @param column The details of column
	 * @throws SQLException 
	 */
	void unregisterColumnFromUniquekKey(TableColumn column) throws SQLException;
	
	/**
	 * To remove a column
	 * 
	 * @param column The details of column
	 * @throws SQLException 
	 */
	void removeColumn(TableColumn column) throws SQLException;
	
	/**
	 * To remove a column
	 * 
	 * @param name The name of the column to be removed.
	 * @throws SQLException 
	 */
	void removeColumn(String name) throws SQLException;
	
	/**
	 * Delete a table from database
	 * 
	 * @throws SQLException
	 */
	void deleteTable() throws SQLException;

	/**
	 * Delete a table from database
	 * 
	 * @param name The name of table to delete.
	 * 
	 * @throws SQLException
	 */
	void deleteTable(String name) throws SQLException;	
	
	/**
	 * Removing all of the data from the table
	 * 
	 * @throws SQLException
	 */
	void clearTable() throws SQLException;

	/**
	 * Removing all of the data from the table
	 * 
	 * @param name The name of the table to clear
	 * 
	 * @throws SQLException
	 */
	void clearTable(String name) throws SQLException;
	
	/**
	 * To remove the data and reset the auto increment setting of the table. The data removed by this method cannot 
	 * be rolled bakc.<br>
	 * @throws SQLException 
	 */
	void resetTable() throws SQLException;
	
	/**
	 * To remove the data and reset the auto increment setting of the table. The data removed by this method cannot 
	 * be rolled bakc.<br>
	 * 
	 * @param name The name of the table to reset
	 * @throws SQLException 
	 */
	void resetTable(String name) throws SQLException;
	
	/**
	 * Returns true if the target table exists, otherwise false returned
	 * 
	 * @return true if the target table exists, otherwise false returned
	 * 
	 * @throws SQLException
	 */
	Boolean isTableExisting() throws SQLException;
	
	/**
	 * Returns true if the target table exists, otherwise false returned
	 * 
	 * @param name The name of table to check
	 * 
	 * @return true if the target table exists, otherwise false returned
	 * 
	 * @throws SQLException
	 */
	Boolean isTableExisting(String name) throws SQLException;
}
