/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * The sub-type of this table is a data structure of table which is used for table manipulations, such as table creation, table altering, and table deleting and so on.<br>
 * If the table which is used for the SQL Query, please use {@link com.digsarustudio.banana.database.query.Table}.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface Table {
	/**
	 * Assigns a name to this table
	 * 
	 * @param name The name for this table
	 */
	void setName(String name);
	
	/**
	 * Returns the name of this table
	 * 
	 * @return the name of this table
	 */
	String getName();
	
	/**
	 * Returns a specific column by the index, null returned if the index is out of range.
	 * 
	 * @param index The specific index to get a column
	 * 
	 * @return a specific column by the index, null returned if the index is out of range.
	 * 
	 */
	TableColumn getColumn(Integer index);
	
	/**
	 * Returns a specific column by its name, null returned if the name is not found.
	 *  
	 * @param name The name of the specific column
	 * 
	 * @return a specific column by the name, null returned if the name is not found.
	 */
	TableColumn getColumn(String name);
		
	/**
	 * Returns the column working as a primary key, null returned if there is no primary key set.
	 * 
	 * @param index The index of primary key
	 * 
	 * @return the column working as a primary key, null returned if there is no primary key set.
	 */
	TableColumn getPrimaryKey(Integer index);
	
	/**
	 * Returns the column working as a primary key, null returned if there is no primary key set.
	 * 
	 * @param name The name of the specific column
	 * 
	 * @return the column working as a primary key, null returned if there is no primary key set.
	 */
	TableColumn getPrimaryKey(String name);
	
	/**
	 * Returns the column working as a foreign key, null returned if there is no foreign key set.
	 * 
	 * @param index The index of foreign key
	 * 
	 * @return the column working as a foreign key, null returned if there is no foreign key set.
	 */
	TableColumn getForeignKey(Integer index);
	
	/**
	 * Returns the column working as a foreign key, null returned if there is no foreign key set.
	 * 
	 * @param name The name of the specific column
	 * 
	 * @return the column working as a foreign key, null returned if there is no foreign key set.
	 */
	TableColumn getForeignKey(String name);
	
	/**
	 * Returns the column working as a unique key, null returned if there is no unique key set.
	 * 
	 * @param index The index of foreign key
	 * 
	 * @return the column working as a unique key, null returned if there is no unique key set.
	 */
	TableColumn getUniqueKey(Integer index);
	
	/**
	 * Returns the column working as a unique key, null returned if there is no unique key set.
	 * 
	 * @param name The name of the specific column
	 * 
	 * @return the column working as a unique key, null returned if there is no unique key set.
	 */
	TableColumn getUniqueKey(String name);
	
	/**
	 * Append a column to this table
	 * 
	 * @param column The column for this table
	 */
	void addColumn(TableColumn column);
	
	/**
	 * Returns true if this table supports full text search, otherwise false returned
	 * 
	 * @return true if this table supports full text search, otherwise false returned
	 */
	Boolean isSupportFullTextSearch();
	
	/**
	 * Returns true if this table has primary key, otherwise false returned
	 * 
	 * @return true if this table has primary key, otherwise false returned
	 */
	Boolean hasPrimaryKey();
	
	/**
	 * Returns true if this table has foreign key, otherwise false returned
	 * 
	 * @return true if this table has foreign key, otherwise false returned
	 */
	Boolean hasForeignKey();
	
	/**
	 * Returns true if this table has unique key, otherwise false returned
	 * 
	 * @return true if this table has unique key, otherwise false returned
	 */
	Boolean hasUniqueKey();
}
