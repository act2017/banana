/**
 * 
 */
package com.digsarustudio.banana.database.table;

import com.digsarustudio.banana.database.query.TemporaryTable;
import com.digsarustudio.banana.database.query.statement.SQLStatement;

/**
 * The implementation of {@link VirtualTable}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2 Please use {@link TemporaryTable} instead
 */
public class SimpleVirtualTable implements VirtualTable {
	/**
	 * 
	 * The object builder for {@link VirtualTable}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements VirtualTable.Builder {
		private SimpleVirtualTable result = null;

		public Builder() {
			this.result = new SimpleVirtualTable();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.table.VirtualTable.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.table.VirtualTable.Builder#setStatement(com.digsarustudio.banana.database.query.statement.SQLStatement)
		 */
		@Override
		public Builder setStatement(SQLStatement statement) {
			this.result.setStatement(statement);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public VirtualTable build() {
			return this.result;
		}

	}
	
	private String 			name		= null;
	private SQLStatement	statement	= null;
	
	/**
	 * 
	 */
	private SimpleVirtualTable() {
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.VirtualTable#getName()
	 */
	@Override
	public String getName() {

		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.table.VirtualTable#getStatement()
	 */
	@Override
	public SQLStatement getStatement() {

		return this.statement ;
	}

	/**
	 * @param name the name to set
	 */
	protected void setName(String name) {
		this.name = name;
	}

	/**
	 * @param statement the statement to set
	 */
	protected void setStatement(SQLStatement statement) {
		this.statement = statement;
	}

}
