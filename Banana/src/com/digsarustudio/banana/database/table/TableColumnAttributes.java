/**
 * 
 */
package com.digsarustudio.banana.database.table;

/**
 * This enumeration represents the attributes used for the table column
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum TableColumnAttributes {
	 CannotBeNull("NOT NULL")
	,CanBeNull("NULL")
	,AutoIncrement("AUTO_INCREMENT")
	,OnUpdateCurrentTimestamp("ON UPDATE CURRENT_TIMESTAMP")
	;

	private String value = null;
	
	private TableColumnAttributes(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
