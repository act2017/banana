/**
 * 
 */
package com.digsarustudio.banana.database.table;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The data type of a table column which contains the data type, length, supplementary length, and 
 * a list of strings used to describe what sort of data could be stored in the particular column.
 * When the data type is {@link TableColumnDataTypes#Enum}, this class will contain a list of enumerations 
 * in the string format. 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ColumnDataType {
	/**
	 * 
	 * The object builder for {@link ColumnDataType}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<ColumnDataType> {
		private TableColumnDataTypes dataType = null;
		private Integer length = null;
		private Integer supplementaryLength = null;
		private List<String> enumerations	= null;		
		
		/**
		 * Returns the type of data the column could be accepted.
		 * 
		 * @return the type of data the column could be accepted.
		 */
		private TableColumnDataTypes getDataType() {
			return dataType;
		}

		/**
		 * Assigns the type of data the column could be accepted.
		 * 
		 * @param dataType the type of data the column could be accepted.
		 * 
		 * @return The instance of this builder
		 */
		public Builder setDataType(TableColumnDataTypes dataType) {
			this.dataType = dataType;
			return this;
		}

		/**
		 * Returns the length of data the column could be accepted. 
		 * Null returned if the {@link ColumnDataType#dataType} is {@link TableColumnDataTypes#Boolean}
		 * 
		 * @return the length
		 */
		private Integer getLength() {
			return length;
		}

		/**
		 * Assigns the length of data the column could be accepted
		 * 
		 * @param length the length to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setLength(Integer length) {
			this.length = length;
			return this;
		}

		/**
		 * Returns the value of supplementary length. Null returned if 
		 * the {@link ColumnDataType#dataType} is not {@link TableColumnDataTypes#Decimal}.
		 * 
		 * @return the value of supplementary length. Null returned if 
		 * 			the {@link ColumnDataType#dataType} is not {@link TableColumnDataTypes#Decimal}.
		 */
		private Integer getSupplementaryLength() {
			return supplementaryLength;
		}

		/**
		 * Assigns the supplementary length for this data type.<br>
		 * 
		 * Normally, this method called when the {@link ColumnDataType#dataType} is {@link TableColumnDataTypes#Decimal}.
		 * 
		 * @param supplementaryLength the length to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setSupplementaryLength(Integer length) {
			this.supplementaryLength = length;
			return this;
		}

		/**
		 * Returns a list of enumerations declared in this type.
		 * 
		 * @return the enumerations if the {@link ColumnDataType#dataType} is {@link TableColumnDataTypes#Enum}. 
		 * 			Otherwise null returned.
		 */
		private List<String> getEnumerations() {
			return enumerations;
		}

		/**
		 * Assigns a list of enumerations
		 * 
		 * @param enumerations the enumerations to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setEnumerations(List<String> enumerations) {
			this.enumerations = enumerations;
			return this;
		}

		/**
		 * Appends an enumeration
		 * 
		 * @param enumeration The name of the enumeration to append
		 * 
		 * @return The instance of this builder
		 */
		public Builder addEnumerations(String enumeration){
			if(null == this.enumerations){
				this.enumerations = new ArrayList<>();
			}
			
			this.enumerations.add(enumeration);
			return this;
		}		

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ColumnDataType build() {
			return new ColumnDataType(this);
		}

	}
	
	/**
	 * The data type of the particular column.
	 */
	private TableColumnDataTypes dataType = null;
	
	/**
	 * The length of data.<br>
	 * 
	 * It represents as the total length of digits for {@link TableColumnDataTypes#Decimal}.
	 */
	private Integer length = null;
	
	/**
	 * The supplementary length for {@link ColumnDataType#dataType}.<br>
	 * This field will be used when {@link ColumnDataType#dataType} equals to 
	 * {@link TableColumnDataTypes#Decimal} only.<br>
	 * It represents as the length of right hand side digits for {@link TableColumnDataTypes#Decimal}. 
	 */
	private Integer supplementaryLength = null;

	/**
	 * The list of enumerations for {@link TableColumnDataTypes#Enum}.<br>
	 * It would be null if the {@link ColumnDataType#dataType} is not {@link TableColumnDataTypes#Enum}. 
	 */
	private List<String> enumerations	= null;
	
	/**
	 * Constructs a column data type object from a data type string fetched from database.<br>
	 * 
	 * @param dataType The string of data type
	 * 
	 * @throws UnSupportedColumnDataType When the incoming data type is not supported yet.
	 */
	public ColumnDataType(String dataType) throws UnSupportedColumnDataType{
		this.parse(dataType);
	}
	
	/**
	 * 
	 */
	private ColumnDataType(Builder builder) {
		this.setDataType(builder.getDataType());
		this.setLength(builder.getLength());
		this.setSupplementaryLength(builder.getSupplementaryLength());
		this.setEnumerations(builder.getEnumerations());
	}

	/**
	 * Returns the type of data the column could be accepted.
	 * 
	 * @return the type of data the column could be accepted.
	 */
	public TableColumnDataTypes getDataType() {
		return dataType;
	}

	/**
	 * Assigns the type of data the column could be accepted.
	 * 
	 * @param dataType the type of data the column could be accepted.
	 */
	public void setDataType(TableColumnDataTypes dataType) {
		this.dataType = dataType;
	}

	/**
	 * Returns the length of data the column could be accepted. 
	 * Null returned if the {@link ColumnDataType#dataType} is {@link TableColumnDataTypes#Boolean}
	 * 
	 * @return the length
	 */
	public Integer getLength() {
		return length;
	}

	/**
	 * Assigns the length of data the column could be accepted
	 * 
	 * @param length the length to set
	 */
	public void setLength(Integer length) {
		this.length = length;
	}

	/**
	 * Returns the value of supplementary length. Null returned if 
	 * the {@link ColumnDataType#dataType} is not {@link TableColumnDataTypes#Decimal}.
	 * 
	 * @return the value of supplementary length. Null returned if 
	 * 			the {@link ColumnDataType#dataType} is not {@link TableColumnDataTypes#Decimal}.
	 */
	public Integer getSupplementaryLength() {
		return supplementaryLength;
	}

	/**
	 * Assigns the supplementary length for this data type.<br>
	 * 
	 * Normally, this method called when the {@link ColumnDataType#dataType} is {@link TableColumnDataTypes#Decimal}.
	 * 
	 * @param supplementaryLength the length to set
	 */
	public void setSupplementaryLength(Integer length) {
		this.supplementaryLength = length;
	}

	/**
	 * Returns a list of enumerations declared in this type.
	 * 
	 * @return the enumerations if the {@link ColumnDataType#dataType} is {@link TableColumnDataTypes#Enum}. 
	 * 			Otherwise null returned.
	 */
	public List<String> getEnumerations() {
		return enumerations;
	}

	/**
	 * Assigns a list of enumerations
	 * 
	 * @param enumerations the enumerations to set
	 */
	public void setEnumerations(List<String> enumerations) {
		this.enumerations = enumerations;
	}

	/**
	 * Appends an enumeration
	 * 
	 * @param enumeration The name of the enumeration to append
	 */
	public void addEnumerations(String enumeration){
		if(null == this.enumerations){
			this.enumerations = new ArrayList<>();
		}
		
		this.enumerations.add(enumeration);
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private void parse(String dataType) throws UnSupportedColumnDataType{
		//S1: TYPE
		//S2: TYPE (LENGTH)
		//S3: TYPE (LENGTH, LENGTH)
		//S4: TYPE (ENUM1, ENUM2, ...)
		
		Integer pointer = dataType.indexOf("(");
		
		//S1
		if( 0 > pointer ){
			this.dataType = this.retrieveDataType(dataType);
			this.length = null;
			this.supplementaryLength = null;
			this.enumerations = null;
			
		//S2
		}else{
			this.dataType = this.retrieveDataType(dataType.substring(0, pointer).trim());
			
			Integer endIndex = dataType.indexOf(")");
			if(0 > endIndex){
				throw new IllegalArgumentException("Invalid data type string. (" + dataType + ")"); 
			}
			
			String values = dataType.substring(pointer+1, endIndex).trim();
			
			//S4
			if( TableColumnDataTypes.Enum == this.dataType ){
				this.enumerations = retrieveEnums(values);
				
			//S3
			}else{
				List<Integer> lengths = this.retrieveLengths(values);				
				
				this.length = lengths.get(0);
				if( lengths.size() > 1){
					this.supplementaryLength = lengths.get(1);
				}
				
				this.enumerations = null;
			}
		}
	}
	
	private TableColumnDataTypes retrieveDataType(String source) throws UnSupportedColumnDataType{
		return TableColumnDataTypes.fromName(source);
	}
	
	private List<Integer> retrieveLengths(String source){				
		String[] tmp = source.split(",");
		
		List<Integer> rtns = new ArrayList<>();
		for (String len : tmp) {
			rtns.add(new Integer(len.trim()));
		}
		
		return rtns;
	}
	
	private List<String> retrieveEnums(String source){
		String[] tmp = source.split(",");
		
		List<String> rtns = new ArrayList<>();
		for (String e : tmp) {
			rtns.add(e.trim());
		}
		
		return rtns;
	}
}
