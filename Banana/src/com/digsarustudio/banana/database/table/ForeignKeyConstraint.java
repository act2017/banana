/**
 * 
 */
package com.digsarustudio.banana.database.table;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class provides the name of the target table, and the primary key of the target table.<br>
 * The name of this constraint must be provided if the foreign key might be removed from the table
 * in the future, otherwise call SHOW_CREATE_TABLE statement to get the name of this constraint generated
 * by database.<br>
 * <br>
 * For example:<br>
 * SHOW_CREATE_TABLE shows:<br>
 *  CONSTRAINT `0_38776` FOREIGN KEY (`B`, `C`) REFERENCES `ibtest11a` (`B`, `C`) ON DELETE CASCADE ON UPDATE CASCADE<br>
 *  0_38776 is the database auto generated constraint name.<br> 
 * 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class ForeignKeyConstraint {
	/**
	 * 
	 * The object builder for {@link ForeignKeyConstraint}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<ForeignKeyConstraint> {
		private String name				= null;
		private String refTable 		= null;
		private String refPK			= null;
		
		private ReferentialActions deleteReferentialAction = null;
		private ReferentialActions updateReferentialAction = null;
		
		/**
		 * Assigns the name of the referenced table
		 * 
		 * @param name the name of the referenced table to set
		 * 
		 * @return The instance of the builder
		 * 
		 * @deprecated 1.0.0 Please use {@link ForeignKeyConstraint.Builder#setReferencedTable(String)} instead.
		 */
		public Builder setReferencedTable(String name) {
			this.refTable = name;
			return this;
		}

		/**
		 * Assigns the name of the referenced primary key in the referenced table
		 * 
		 * @param name the name of the referenced primary key in the referenced table to set
		 * 
		 * @return The instance of the builder
		 * 
		 * @deprecated 1.0.0 Please use {@link ForeignKeyConstraint.Builder#setReferentialPrimaryKey(String)} instead.
		 */
		public Builder setReferencedPrimaryKey(String name) {
			this.refPK = name;
			return this;
		}
		
		/**
		 * Assigns a name to this constraint
		 * 
		 * @param name a name for this constraint
		 * 
		 * @return The instance of this builder
		 */
		public Builder setName(String name) {
			this.name = name;
			return this;
		}
		
		/**
		 * Assigns the name of the referential table
		 * 
		 * @param name the name of the referential table to set
		 * 
		 * @return The instance of the builder
		 */
		public Builder setReferentialTable(String name) {
			this.refTable = name;
			return this;
		}

		/**
		 * Assigns the name of the referential primary key in the referenced table
		 * 
		 * @param name the name of the referential primary key in the referential table to set
		 * 
		 * @return The instance of the builder
		 */
		public Builder setReferentialPrimaryKey(String name) {
			this.refPK = name;
			return this;
		}

		/**		 
		 * Delete the row from the parent table, and automatically delete or update the matching rows 
		 * in the child table.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnDeleteCascade(){
			this.deleteReferentialAction = ReferentialActions.Cascade;
			return this;
		}

		/**
		 * Rejects the delete operation for the parent table. 
		 * Specifying RESTRICT (or NO ACTION) is the same as omitting the ON DELETE clause.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnDeleteRestrict(){
			this.deleteReferentialAction = ReferentialActions.Restrict;
			return this;
		}

		/**
		 * Same as {@link ForeignKeyConstraint.Builder#setOnDeleteRestrict()}
		 * 
		 * A keyword from standard SQL. In MySQL, equivalent to RESTRICT. 
		 * The MySQL Server rejects the delete operation for the parent table if there is a 
		 * related foreign key value in the referenced table. 
		 * 
		 * Some database systems have deferred checks, and NO ACTION is a deferred check. 
		 * In MySQL, foreign key constraints are checked immediately, so NO ACTION is the same as RESTRICT.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnDeleteNoAction(){
			this.deleteReferentialAction = ReferentialActions.NoAction;
			return this;
		}

		/**
		 * Delete the row from the parent table, and set the foreign key column or columns in the 
		 * child table to NULL.
		 * 
		 * If you specify a SET NULL action, make sure that you have not declared the columns in the child 
		 * table as NOT NULL.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnDeleteSetNull(){
			this.deleteReferentialAction = ReferentialActions.SetNull;
			return this;
		}

		/**
		 * This action is recognized by the MySQL parser, but both InnoDB and NDB reject table definitions 
		 * containing ON DELETE SET DEFAULT clauses.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnDeleteSetDefault(){
			this.deleteReferentialAction = ReferentialActions.SetDefault;
			return this;
		}
		
		/**
		 * Update the row from the parent table, and automatically update the matching rows 
		 * in the child table. Between two tables, 
		 * do not define several ON UPDATE CASCADE clauses that act on the same column in the parent table or 
		 * in the child table.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnUpdateCascade(){
			this.updateReferentialAction = ReferentialActions.Cascade;
			return this;
		}

		/**
		 * Rejects the update operation for the parent table. 
		 * Specifying RESTRICT (or NO ACTION) is the same as omitting the ON UPDATE clause.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnUpdateRestrict(){
			this.updateReferentialAction = ReferentialActions.Restrict;
			return this;
		}

		/**
		 * Same as {@link ForeignKeyConstraint.Builder#setOnUpdateRestrict()}
		 * 
		 * A keyword from standard SQL. In MySQL, equivalent to RESTRICT. 
		 * The MySQL Server rejects the update operation for the parent table if there is a 
		 * related foreign key value in the referenced table. 
		 * Some database systems have deferred checks, and NO ACTION is a deferred check. 
		 * In MySQL, foreign key constraints are checked immediately, so NO ACTION is the same as RESTRICT.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnUpdateNoAction(){
			this.updateReferentialAction = ReferentialActions.NoAction;
			return this;
		}

		/**
		 * Update the row from the parent table, and set the foreign key column or columns in the 
		 * child table to NULL. 
		 * 
		 * If you specify a SET NULL action, make sure that you have not declared the columns in the child 
		 * table as NOT NULL.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnUpdateSetNull(){
			this.updateReferentialAction = ReferentialActions.SetNull;
			return this;
		}

		/**
		 * This action is recognized by the MySQL parser, but both InnoDB and NDB reject table definitions 
		 * containing ON UPDATE SET DEFAULT clauses.
		 * 
		 * @return The instance of the builder
		 */
		public Builder setOnUpdateSetDefault(){
			this.updateReferentialAction = ReferentialActions.SetDefault;
			return this;
		}

		private String getName() {
			return this.name;
		}
		
		/**
		 * @return the refTable
		 */
		private String getRefTable() {
			return refTable;
		}

		/**
		 * @return the refPK
		 */
		private String getRefPK() {
			return refPK;
		}

		private ReferentialActions getDeleteReferentialActions(){
			return this.deleteReferentialAction;
		}

		private ReferentialActions getUpdateReferentialActions(){
			return this.updateReferentialAction;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public ForeignKeyConstraint build() {
			return new ForeignKeyConstraint(this);
		}

	}
	
	private String name				= null;
	private String refTable 		= null;
	private String refPK			= null;
	private ReferentialActions deleteReferentialAction = null;
	private ReferentialActions updateReferentialAction = null;
	
	private ForeignKeyConstraint(Builder builder){
		this.name			= builder.getName();
		this.refTable		= builder.getRefTable();
		this.refPK			= builder.getRefPK();
		this.deleteReferentialAction = builder.getDeleteReferentialActions();
		this.updateReferentialAction = builder.getUpdateReferentialActions();
	}
	
	/**
	 * Assigns a name to this constraint
	 * 
	 * @param name a name for this constraint
	 * 
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Returns the name of this constraint
	 * 
	 * @return the name of this constraint
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Assigns the name of the source table for the target foreign key
	 * 
	 * @param name the name of the source table for the target foreign key
	 * 
	 * @deprecated 1.0.0 Please use {@link ForeignKeyConstraint#setReferentialTabe(String)} instead.
	 */
	public void setReferencedTabe(String name){
		this.refTable = name;
	}
	
	/**
	 * Assigns a name of the primary key  used as the foreign key
	 * 
	 * @param name The name of the primary key  used as the foreign key
	 * 
	 * @deprecated 1.0.0 Please use {@link ForeignKeyConstraint#setReferentialPrimaryKey(String)} instead.
	 */
	public void setReferencedPrimaryKey(String name){
		this.refPK = name;
	}
	
	/**
	 * Assigns the name of the source table for the target foreign key
	 * 
	 * @param name the name of the source table for the target foreign key
	 */
	public void setReferentialTabe(String name){
		this.refTable = name;
	}
	
	/**
	 * Assigns a name of the primary key  used as the foreign key
	 * 
	 * @param name The name of the primary key  used as the foreign key
	 */
	public void setReferentialPrimaryKey(String name){
		this.refPK = name;
	}

	/**		 
	 * Delete the row from the parent table, and automatically delete or update the matching rows 
	 * in the child table.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnDeleteCascade(){
		this.deleteReferentialAction = ReferentialActions.Cascade;
		
	}

	/**
	 * Rejects the delete operation for the parent table. 
	 * Specifying RESTRICT (or NO ACTION) is the same as omitting the ON DELETE clause.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnDeleteRestrict(){
		this.deleteReferentialAction = ReferentialActions.Restrict;
		
	}

	/**
	 * Same as {@link ForeignKeyDetails.void#setOnDeleteRestrict()}
	 * 
	 * A keyword from standard SQL. In MySQL, equivalent to RESTRICT. 
	 * The MySQL Server rejects the delete operation for the parent table if there is a 
	 * related foreign key value in the referenced table. 
	 * 
	 * Some database systems have deferred checks, and NO ACTION is a deferred check. 
	 * In MySQL, foreign key constraints are checked immediately, so NO ACTION is the same as RESTRICT.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnDeleteNoAction(){
		this.deleteReferentialAction = ReferentialActions.NoAction;
		
	}

	/**
	 * Delete the row from the parent table, and set the foreign key column or columns in the 
	 * child table to NULL.
	 * 
	 * If you specify a SET NULL action, make sure that you have not declared the columns in the child 
	 * table as NOT NULL.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnDeleteSetNull(){
		this.deleteReferentialAction = ReferentialActions.SetNull;
		
	}

	/**
	 * This action is recognized by the MySQL parser, but both InnoDB and NDB reject table definitions 
	 * containing ON DELETE SET DEFAULT clauses.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnDeleteSetDefault(){
		this.deleteReferentialAction = ReferentialActions.SetDefault;
		
	}
	
	/**
	 * Update the row from the parent table, and automatically update the matching rows 
	 * in the child table. Between two tables, 
	 * do not define several ON UPDATE CASCADE clauses that act on the same column in the parent table or 
	 * in the child table.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnUpdateCascade(){
		this.updateReferentialAction = ReferentialActions.Cascade;
		
	}

	/**
	 * Rejects the update operation for the parent table. 
	 * Specifying RESTRICT (or NO ACTION) is the same as omitting the ON UPDATE clause.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnUpdateRestrict(){
		this.updateReferentialAction = ReferentialActions.Restrict;
		
	}

	/**
	 * Same as {@link ForeignKeyDetails.void#setOnUpdateRestrict()}
	 * 
	 * A keyword from standard SQL. In MySQL, equivalent to RESTRICT. 
	 * The MySQL Server rejects the update operation for the parent table if there is a 
	 * related foreign key value in the referenced table. 
	 * Some database systems have deferred checks, and NO ACTION is a deferred check. 
	 * In MySQL, foreign key constraints are checked immediately, so NO ACTION is the same as RESTRICT.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnUpdateNoAction(){
		this.updateReferentialAction = ReferentialActions.NoAction;
		
	}

	/**
	 * Update the row from the parent table, and set the foreign key column or columns in the 
	 * child table to NULL. 
	 * 
	 * If you specify a SET NULL action, make sure that you have not declared the columns in the child 
	 * table as NOT NULL.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnUpdateSetNull(){
		this.updateReferentialAction = ReferentialActions.SetNull;
		
	}

	/**
	 * This action is recognized by the MySQL parser, but both InnoDB and NDB reject table definitions 
	 * containing ON UPDATE SET DEFAULT clauses.
	 * 
	 * @return The instance of the builder
	 */
	public void setOnUpdateSetDefault(){
		this.updateReferentialAction = ReferentialActions.SetDefault;
		
	}
	
	/**
	 * Returns the name of the source table of the target foreign key
	 * 
	 * @return the name of the source table of the target foreign key
	 * 
	 * @deprecated 1.0.0 Please use {@link ForeignKeyConstraint#getReferentialTable()} instead
	 */
	public String getReferencedTable(){
		return this.refTable;
	}
	
	/**
	 * Returns the name of the source table of the target foreign key
	 * 
	 * @return the name of the source table of the target foreign key
	 * 
	 */
	public String getReferentialTable() {
		return this.refTable;
	}
	
	/**
	 * Returns the name of primary key used as the foreign key
	 * 
	 * @return the name of primary key used as the foreign key
	 */
	public String getReferentialPrimaryKey(){
		return this.refPK;
	}
	
	/**
	 * Returns the name of primary key used as the foreign key
	 * 
	 * @return the name of primary key used as the foreign key
	 * 
	 * @deprecated 1.0.0 Please use {@link ForeignKeyConstraint#getReferentialPrimaryKey()} instead
	 */
	public String getReferencedPrimaryKey(){
		return this.refPK;
	}
	
	/**
	 * Returns the referential action on deleting
	 * 
	 * @return the referential action on deleting
	 */
	public ReferentialActions getDeleteReferentialActions(){
		return this.deleteReferentialAction;
	}

	/**
	 * Returns the referential action on updating
	 * 
	 * @return the referential action on updating
	 */
	public ReferentialActions getUpdateReferentialActions(){
		return this.updateReferentialAction;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
