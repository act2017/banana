/**
 * 
 */
package com.digsarustudio.banana.database;

/**
 * The character set used in MySQL
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum CharacterSets {
	UTF8Unicode("utf8")
	,UTF16Unicode("utf16")
	;
	
	private String value = null;
	
	private CharacterSets(String charset){
		this.value = charset;
	}
	
	public String getValue(){
		return this.value;
	}
}
