/**
 * 
 */
package com.digsarustudio.banana.database;

import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The configuration of the database
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DatabaseConfig {
	public static final Integer DEFAULT_PORT = 3306;
	/**
	 * 
	 * The object builder for {@link DatabaseConfig}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<DatabaseConfig> {
//		private String 				host						= "localhost";
//		private Integer				port						= DEFAULT_PORT;
//		private String 				userName					= "db_boss";
//		private String		 		password 					= "1234";		
//		private String 				defaultDB 					= "b_box";
//		private String				cloudDatabaseName			= "";
//		private String				cloudInstanceConnectionName	= null;
//		private DatabaseEngineType	engineType	= DatabaseEngineType.InnoDB;
		private DatabaseConfig	result = null;

		private Builder() {
			this.result = new DatabaseConfig();
		}
		
//		/**
//		 * Returns the host of the database
//		 * 
//		 * @return the host of the database
//		 * 
//		 */
//		private String getHost() {
//			return host;
//		}

		/**
		 * Assigns a new host for this config
		 * 
		 * @param host the host to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setHost(String host) {
//			this.host = host;
			this.result.setHost(host);
			return this;
		}

//		/**
//		 * Returns the user name to login the host database.
//		 * 
//		 * @return the userName to login the host database.
//		 */
//		private String getUserName() {
//			return userName;
//		}
		
		/**
		 * Assigns a user name to login the host database.
		 * 
		 * @param userName the userName to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setUserName(String userName) {
//			this.userName = userName;
			this.result.setUserName(userName);
			return this;
		}

//		/**
//		 * Returns the password to login the host database
//		 * 
//		 * @return the password to login the host database
//		 */
//		private String getPassword() {
//			return password;
//		}

		/**
		 * Assigns a password to login the host database
		 * 
		 * @param password the password to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setPassword(String password) {
//			this.password = password;
			this.result.setPassword(password);
			return this;
		}

//		/**
//		 * Returns the default database to select
//		 * 
//		 * @return the default database to select
//		 */
//		private String getDefaultDB() {
//			return defaultDB;
//		}

		/**
		 * Assigns a database to select as default
		 * 
		 * @param defaultDB the defaultDB to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setDefaultDB(String defaultDB) {
//			this.defaultDB = defaultDB;
			this.result.setDefaultDB(defaultDB);
			return this;
		}

//		/**
//		 * Returns the engine type used for this database
//		 * 
//		 * @return the engine type used for this database
//		 */
//		private DatabaseEngineType getEngineType() {
//			return engineType;
//		}

		/**
		 * Assigns an engine type to this database
		 * 
		 * @param engineType the engineType to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setEngineType(DatabaseEngineType engineType) {
//			this.engineType = engineType;
			this.result.setEngineType(engineType);
			return this;
		}

//		/**
//		 * @return the port
//		 */
//		private Integer getPort() {
//			return port;
//		}

		/**
		 * Assigns a port
		 * 
		 * @param port the port to set
		 */
		public Builder setPort(Integer port) {
//			this.port = port;
			this.result.setPort(port);
			return this;
		}

		/**
		 * @param cloudDatabaseName
		 * @see com.digsarustudio.banana.database.DatabaseConfig#setCloudDatabaseName(java.lang.String)
		 */
		public Builder setCloudDatabaseName(String cloudDatabaseName) {
			result.setCloudDatabaseName(cloudDatabaseName);
			return this;
		}

		/**
		 * @param cloudInstanceConnectionName
		 * @see com.digsarustudio.banana.database.DatabaseConfig#setCloudInstanceConnectionName(java.lang.String)
		 */
		public Builder setCloudInstanceConnectionName(String cloudInstanceConnectionName) {
			result.setCloudInstanceConnectionName(cloudInstanceConnectionName);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DatabaseConfig build() {
			return this.result;
		}

	}
	
	private String 				host						= null;
	private Integer				port						= DEFAULT_PORT;
	private String 				userName					= null;
	private String 				password 					= null;
	private String 				defaultDB 					= null;
	private String				cloudDatabaseName			= "";
	private String				cloudInstanceConnectionName	= null;
	private DatabaseEngineType	engineType					= DatabaseEngineType.InnoDB;
	
	/**
	 * 
	 */
	private DatabaseConfig() {
		
	}

	/**
	 * Returns the host of the database
	 * @return the host of the database
	 * 
	 */
	public String getHost() {
		return host;
	}

	/**
	 * Assigns a new host for this config
	 * 
	 * @param host the host to set
	 */
	public void setHost(String host) {
		this.host = host;
	}

	/**
	 * Returns the user name to login the host database.
	 * 
	 * @return the userName to login the host database.
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * Assigns a user name to login the host database.
	 * 
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * Returns the password to login the host database
	 * 
	 * @return the password to login the host database
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Assigns a password to login the host database
	 * 
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Returns the default database to select
	 * 
	 * @return the default database to select
	 */
	public String getDefaultDB() {
		return defaultDB;
	}

	/**
	 * Assigns a database to select as default
	 * 
	 * @param defaultDB the defaultDB to set
	 */
	public void setDefaultDB(String defaultDB) {
		this.defaultDB = defaultDB;
	}

	/**
	 * Returns the engine type used for this database
	 * 
	 * @return the engine type used for this database
	 */
	public DatabaseEngineType getEngineType() {
		return engineType;
	}

	/**
	 * Assigns an engine type to this database
	 * @param engineType the engineType to set
	 */
	public void setEngineType(DatabaseEngineType engineType) {
		this.engineType = engineType;
	}

	/**
	 * Returns the port used to connect database server
	 * 
	 * @return the port used to connect database server
	 */
	public Integer getPort() {
		return port;
	}

	/**
	 * Assigns a port for the connection with database
	 * 
	 * @param port the port to set
	 */
	public void setPort(Integer port) {
		this.port = port;
	}

	/**
	 * @return the cloudDatabaseName
	 */
	public String getCloudDatabaseName() {
		return cloudDatabaseName;
	}

	/**
	 * @param cloudDatabaseName the cloudDatabaseName to set
	 */
	public void setCloudDatabaseName(String cloudDatabaseName) {
		this.cloudDatabaseName = cloudDatabaseName;
		
		this.setDefaultDB(this.cloudDatabaseName);
	}

	/**
	 * @return the cloudInstanceConnectionName
	 */
	public String getCloudInstanceConnectionName() {
		return cloudInstanceConnectionName;
	}

	/**
	 * @param cloudInstanceConnectionName the cloudInstanceConnectionName to set
	 */
	public void setCloudInstanceConnectionName(String cloudInstanceConnectionName) {
		this.cloudInstanceConnectionName = cloudInstanceConnectionName;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
