/**
 * 
 */
package com.digsarustudio.banana.database;

/**
 * The exception indicates there is an error occurred when the client code tried to use a database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com> 
 * @since		1.0.2
 * @version		1.0.0
 * <br>
 * Note:<br>
 * 				1.0.0	-> Created
 */
@SuppressWarnings("serial")
public class DatabaseException extends Exception {

	/**
	 * 
	 */
	public DatabaseException() {
		
	}

	/**
	 * @param message
	 */
	public DatabaseException(String message) {
		super(message);
		
	}

	/**
	 * @param cause
	 */
	public DatabaseException(Throwable cause) {
		super(cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 */
	public DatabaseException(String message, Throwable cause) {
		super(message, cause);
		
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DatabaseException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		
	}

}
