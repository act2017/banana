/**
 * 
 */
package com.digsarustudio.banana.database;

/**
 * The engine type of database
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum DatabaseEngineType {
	  InnoDB("InnoDB")
	, MyISAM("MyISAM")
	;
	
	private String value = null;
	
	private DatabaseEngineType(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
