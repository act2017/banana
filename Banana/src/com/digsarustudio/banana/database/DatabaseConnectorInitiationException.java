/**
 * 
 */
package com.digsarustudio.banana.database;

/**
 * To indicate a {@link DatabaseConnector} cannot be initiated.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.0
 * @version		1.0.2
 * <br>
 * Note:<br>
 * 				1.0.2	-> Move this class under {@link DatabaseException} to consolidate the exceptions used by client code.<br>
 *
 */
@SuppressWarnings("serial")
public class DatabaseConnectorInitiationException extends DatabaseException {
	/**
	 * 
	 */
	public DatabaseConnectorInitiationException() {

	}

	/**
	 * @param message
	 */
	public DatabaseConnectorInitiationException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public DatabaseConnectorInitiationException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public DatabaseConnectorInitiationException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public DatabaseConnectorInitiationException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
