/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The aggregate function for the <b>GROUP BY</b> clause.<br>
 * 
 * Please ensure the <b>GROUP BY</b> has been set before use {@link SQLAggregateFunctions}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum SQLAggregateFunctions {
	/**
	 * Return the average value of the argument
	 */
	  Average("AVG", "AVG(%s)")
	, AverageDistinct("AVG", "AVG(DISTINCT %s)")
	
//BIT_AND()	Return bitwise AND
//BIT_OR()	Return bitwise OR
//BIT_XOR()	Return bitwise XOR
	
	/**
	 * Return a count of the number of rows returned
	 */
	, Count("COUNT", "COUNT(%s)")
	
	/**
	 * Return the count of a number of different values
	 */
	, CountDistinct("COUNT", "COUNT(DISTINCT %s)")
	
//GROUP_CONCAT()	Return a concatenated string
	
	/**
	 * Return the maximum value
	 */
	, Maximum("MAX", "MAX(%s)")
	
	/**
	 * Return the minimum value
	 */
	, Minimum("MIN", "MIN(%s)")
	
//STD()	Return the population standard deviation
//STDDEV()	Return the population standard deviation
//STDDEV_POP()	Return the population standard deviation
//STDDEV_SAMP()	Return the sample standard deviation
	
	/**
	 * Return the sum
	 */
	, Summary("SUM", "SUM(%s)")
	
//VAR_POP()	Return the population standard variance
//VAR_SAMP()	Return the sample variance
//VARIANCE()	Return the population standard variance
	
	;
	
	
	private String value = null;
	private String format = null;
	private static SQLAggregateFunctions[] values = values();
	
	private SQLAggregateFunctions(String value, String format){
		this.value = value;
		this.format = format;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public String getFormat(){
		return this.format;
	}
	
	public static Boolean isAggregateFunction(SQLAggregateFunctions target){
		for (SQLAggregateFunctions source : values) {
			if( !source.equals(target) ){
				continue;
			}
			
			return true;
		}
		
		return false;
	}
}
