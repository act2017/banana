/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class defines which column has to be sorted within ascending or descending.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class Ordering{
	/**
	 * 
	 * The object builder to generate {@link Ordering}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements ObjectBuilder<Ordering> {
		private Ordering ordering = null;

		public Builder(){
			this.ordering = new Ordering();
		}

		/**
		 * Assigns a column to be sorted
		 * 
		 * @param column the column to set
		 */
		public Builder setColumn(SimpleTableColumn column) {
			this.ordering.setColumn(column);
			return this;
		}

		/**
		 * Assigns how to sort the target column
		 * 
		 * @param type the type to set
		 */
		public Builder setType(SortingType type) {
			this.ordering.setType(type);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Ordering build() {
			return this.ordering;
		}

	}
	
	private SimpleTableColumn	column	= null;
	private SortingType			type	= SortingType.getDefault();
	
	private Ordering(){
	}
	
	/**
	 * Returns the column to be sorted
	 * 
	 * @return  the column to be sorted
	 */
	public SimpleTableColumn getColumn() {
		return column;
	}

	/**
	 * Assigns a column to be sorted
	 * 
	 * @param column the column to set
	 */
	public void setColumn(SimpleTableColumn column) {
		this.column = column;
	}

	/**
	 * Returns the sorting type
	 * 
	 * @return the type of sorting
	 */
	public SortingType getType() {
		return type;
	}

	/**
	 * Assigns how to sort the target column
	 * 
	 * @param type the type to set
	 */
	public void setType(SortingType type) {
		this.type = type;
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
