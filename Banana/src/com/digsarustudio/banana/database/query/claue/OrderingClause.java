/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Ordering;

/**
 * This clause contains the ordering of result data fetched via {@link SelectStatement}, 
 * the ordering of application {@link UpdateStatement} and {@link DeleteStatement} did.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class OrderingClause extends SyntaxClause implements SQLClause {
	private List<Ordering> orderings = null;
	
	/**
	 * 
	 */
	public OrderingClause() {

	}

	/**
	 * Returns the list of orderings
	 * 
	 * @return the list of orderings
	 */
	public List<Ordering> getOrderings() {
		return orderings;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		this.orderings = orderings;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		if( null == this.orderings ){
			this.orderings = new ArrayList<>();
		}
		
		this.orderings.add(ordering);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
