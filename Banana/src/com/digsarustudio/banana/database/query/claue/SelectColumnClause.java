/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.SelectColumn;

/**
 * This clause contains the details of column for {@link SelectStatement}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SelectColumnClause extends SyntaxClause implements SQLClause {
	private List<SelectColumn> columns = null;
		
	/**
	 * 
	 */
	public SelectColumnClause() {
	}

	/**
	 * @return the columns
	 */
	public List<SelectColumn> getColumns() {
		return columns;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setSelectColumns(java.util.List)
	 */
	@Override
	public void setSelectColumns(List<SelectColumn> columns) {
		this.columns = columns;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)
	 */
	@Override
	public void addSelectColumn(SelectColumn column) {
		if(null == this.columns){
			this.columns = new ArrayList<>();
		}
		
		this.columns.add(column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
