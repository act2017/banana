/**
 * 
 */
package com.digsarustudio.banana.database.query.claue.compiler;

import com.digsarustudio.banana.database.query.claue.AggregationClause;
import com.digsarustudio.banana.database.query.claue.ConditionClause;
import com.digsarustudio.banana.database.query.claue.DuplicateKeyUpdateClause;
import com.digsarustudio.banana.database.query.claue.InsertColumnClause;
import com.digsarustudio.banana.database.query.claue.InsertValueClause;
import com.digsarustudio.banana.database.query.claue.JoinConditionClause;
import com.digsarustudio.banana.database.query.claue.JoinTableClause;
import com.digsarustudio.banana.database.query.claue.JoinTableReferenceClause;
import com.digsarustudio.banana.database.query.claue.LimitClause;
import com.digsarustudio.banana.database.query.claue.ModifierClause;
import com.digsarustudio.banana.database.query.claue.OrderingClause;
import com.digsarustudio.banana.database.query.claue.SQLClause;
import com.digsarustudio.banana.database.query.claue.SelectColumnClause;
import com.digsarustudio.banana.database.query.claue.TableReferenceClause;
import com.digsarustudio.banana.database.query.claue.UpdateSetClause;
import com.digsarustudio.banana.database.query.claue.VirtualTableReferenceClause;

/**
 * This clause compiler handles the algorithms used to generate a SQL string according to the settings 
 * of {@link SQLClause}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
@SuppressWarnings("deprecation")
public interface ClauseCompiler {
	/**
	 * To generate SQL string according to {@link ModifierClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 */
	String compile(ModifierClause clause);
	
	/**
	 * To generate SQL string according to {@link TableReferenceClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 */	
	String compile(TableReferenceClause clause);
	
	/**
	 * To generate SQL string according to {@link VirtualTableReferenceClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @deprecated 1.0.2 Please use {@link #compile(TableReferenceClause)} instead
	 */	
	String compile(VirtualTableReferenceClause clause);
	
	/**
	 * To generate SQL string according to {@link InsertColumnClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 */	
	String compile(InsertColumnClause clause);
	
	/**
	 * To generate SQL string according to {@link InsertValueClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 */	
	String compile(InsertValueClause clause);
	
	/**
	 * To generate SQL string according to {@link DuplicateKeyUpdateClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(DuplicateKeyUpdateClause clause);
	
	/**
	 * To generate SQL string according to {@link SelectColumnClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(SelectColumnClause clause);
	
	/**
	 * To generate SQL string according to {@link JoinTableClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 * 
	 */	
	String compile(JoinTableClause clause);
	
	/**
	 * To generate SQL string according to {@link JoinTableReferenceClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 * 
	 * @deprecated 1.0.2 please use {@link #compile(JoinTableClause)} instead.
	 */	
	String compile(JoinTableReferenceClause clause);
	
	/**
	 * To generate SQL string according to {@link JoinConditionClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 * 
	 * @deprecated 1.0.2 please use {@link #compile(JoinTableClause)} instead.
	 */	
	String compile(JoinConditionClause clause);
	
	/**
	 * To generate SQL string according to {@link ConditionClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(ConditionClause clause);
	
	/**
	 * To generate SQL string according to {@link AggregationClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(AggregationClause clause);
	
	/**
	 * To generate SQL string according to {@link UpdateSetClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(UpdateSetClause clause);
	
	/**
	 * To generate SQL string according to {@link LimitClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(LimitClause clause);
	
	/**
	 * To generate SQL string according to {@link OrderingClause}
	 * 
	 * @param clause The clause to be converted to SQL string
	 * 
	 * @return The compiled SQL string for a particular SQL database
	 */	
	String compile(OrderingClause clause);
}
