/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;

/**
 * This class contains the details of modifier for {@link SQLStatement}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class ModifierClause extends SyntaxClause implements SQLClause {

	/**
	 * The modifier for {@link SQLStatement}
	 */
	private SQLSyntaxModifiers	modifier	= null;
	
	/**
	 * 
	 */
	public ModifierClause() {
	}
	
	public SQLSyntaxModifiers getSyntaxModifier(){
		return this.modifier;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setModifier(SQLSyntaxModifiers modifier) {
		this.modifier = modifier;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
