/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.statement.SQLStatement;

/**
 * This clause contains the value of dataset for {@link SQLStatement} to use.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class InsertValueClause extends SyntaxClause implements SQLClause {
	private List<Dataset> dataset = null;
	
	/**
	 * 
	 */
	public InsertValueClause() {

	}

	/**
	 * Returns the value of data to insert.
	 * 
	 * @return the value of data to insert.
	 */
	public List<Dataset> getValues() {
		return dataset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		this.dataset = dataset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		if(null == this.dataset){
			this.dataset = new ArrayList<>();
		}
		
		this.dataset.add(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
