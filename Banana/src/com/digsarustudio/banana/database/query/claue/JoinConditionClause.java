/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.JoinCondition;

/**
 * This clause indicates the relationship of referee column and referential column and 
 * the criteria to join.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * @deprecated	1.0.2 Please use {@link JoinTableClause} instead
 */
public class JoinConditionClause extends SyntaxClause implements SQLClause {
	private List<JoinCondition> conditions = null;
	
	/**
	 * 
	 */
	public JoinConditionClause() {
	
	}

	/**
	 * Returns the conditions which indicate the what this join bases on.
	 * 
	 * @return the conditions which indicate the what this join bases on.
	 */
	public List<JoinCondition> getConditions() {
		return conditions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinConditions(java.util.List)
	 */
	@Override
	public void setJoinConditions(List<JoinCondition> conditions) {
		this.conditions = conditions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
	 */
	@Override
	public void addJoinCondition(JoinCondition condition) {
		if(null == this.conditions){
			this.conditions = new ArrayList<>();
		}
		
		this.conditions.add(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.compiler.compile(this);
	}

}
