/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;

/**
 * This clause contains the assignments for the duplicate key.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DuplicateKeyUpdateClause extends SyntaxClause implements SQLClause {
	private List<DuplicateKeyUpdateAssignment> assignments = null;

	/**
	 * 
	 */
	public DuplicateKeyUpdateClause() {

	}

	/**
	 * Returns the assignments for duplicate key updating
	 * 
	 * @return the assignments for duplicate key updating
	 */
	public List<DuplicateKeyUpdateAssignment> getAssignments() {
		return assignments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setOnDuplicateKeyUpdateAssignments(java.util.List)
	 */
	@Override
	public void setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments) {
		this.assignments = assignments;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)
	 */
	@Override
	public void addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment) {
		if(null == this.assignments){
			this.assignments = new ArrayList<>();
		}
		
		this.assignments.add(assignment);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
