/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.Table;

/**
 * This clause indicates which tables are going to be joined and how to join the tables together.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * @deprecated	1.0.2 Please use {@link JoinTableClause} instead
 */
public class JoinTableReferenceClause extends SyntaxClause implements SQLClause {
	/**
	 * @deprecated 1.0.2 Please use {@link #joinTables} instead
	 */
	private TableJoinType	jointType	= null;
	
	/**
	 * @deprecated 1.0.2 Please use {@link #joinTables} instead
	 */
	private List<Table>		tables		= null;
	
	private List<JoinTable> joinTables	 = null;
	
	/**
	 * 
	 */
	public JoinTableReferenceClause() {

	}

	/**
	 * Returns how the tables going to join
	 * 
	 * @return how the tables going to join
	 * 
	 * @deprecated 1.0.2 Please use {@link #getJoinTables()} instead
	 */
	public TableJoinType getJointType() {
		return jointType;
	}

	/**
	 * Returns the tables to be joined.
	 * 
	 * @return the tables to be joined.
	 * 
	 * @deprecated 1.0.2 Please use {@link #getJoinTables()} instead
	 */
	public List<Table> getTables() {
		return tables;
	}

	/**
	 * Returns the join tables
	 * 
	 * @return the join tables
	 */
	public List<JoinTable> getJoinTables() {
		return joinTables;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)
	 */
	@Deprecated
	@Override
	public void setJoinType(TableJoinType type) {
		this.jointType = type;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinTableRefereces(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableRefereces(List<Table> tables) {
		this.tables = tables;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addJoinTableReference(com.digsarustudio.banana.database.table.Table)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(Table table) {
		if(null == this.tables){
			this.tables = new ArrayList<>();
		}
		
		this.tables.add(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinTableReferences(java.util.List)
	 */
	@Override
	public void setJoinTableReferences(List<JoinTable> tables) {
		this.joinTables = tables;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addJoinTableReference(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTableReference(JoinTable table) {
		if(null == this.joinTables){
			this.joinTables = new ArrayList<>();
		}
		
		this.joinTables.add(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.compiler.compile(this);
	}

}
