/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Condition;

/**
 * This clause contains conditions for {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class ConditionClause extends SyntaxClause implements SQLClause {
	private List<Condition> conditions = null;
	
	/**
	 * 
	 */
	public ConditionClause() {
	}	

	/**
	 * Returns the list of condition.
	 * 
	 * @return the list of conditions
	 */
	public List<Condition> getConditions() {
		return conditions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		this.conditions = conditions;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		if(null == this.conditions){
			this.conditions = new ArrayList<>();
		}
		
		this.conditions.add(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
