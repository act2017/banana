/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Dataset;

/**
 * This clause contains the name of tables and columns which are going to be inserted data.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class InsertColumnClause extends SyntaxClause implements SQLClause {
	private List<Dataset>	dataset = null;
	
	/**
	 * 
	 */
	public InsertColumnClause() {

	}

	/**
	 * @return the dataset
	 */
	public List<Dataset> getColumns() {
		return this.dataset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		this.dataset = dataset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		if(null == this.dataset){
			this.dataset = new ArrayList<>();
		}
		
		this.dataset.add(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
		
	}

}
