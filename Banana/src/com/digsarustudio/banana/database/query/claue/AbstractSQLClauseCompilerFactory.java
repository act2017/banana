/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;

/**
 * The abstract factory for {@link SQLClause} and its family.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface AbstractSQLClauseCompilerFactory {
	/**
	 * Returns the clause compiler which generates MySQL clause string.<br>
	 * 
	 * @return the clause compiler which generates MySQL clause string
	 */
	ClauseCompiler createMySQLClauseCompiler();
	
	/**
	 * Returns the clause compiler which generates MS SQL clause string.<br>
	 * 
	 * @return the clause compiler which generates MS SQL clause string
	 */
	ClauseCompiler createMSSQLClauseCompiler();
	
	/**
	 * Returns the clause compiler which generates Google Cloud SQL clause string.<br>
	 * <br>
	 * Note:<br>
	 * Not supported now.<br>
	 * 
	 * @return the clause compiler which generates Google Cloud SQL clause string
	 */
	ClauseCompiler createGoogleCloudSQLClauseCompiler();
}
