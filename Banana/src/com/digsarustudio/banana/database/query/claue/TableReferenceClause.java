/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.database.query.TableReferenceActions;
import com.digsarustudio.banana.database.query.Table;

/**
 * This class contains table reference action and the reference table for {@link SQLStatement}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class TableReferenceClause extends SyntaxClause implements SQLClause {
	
	private TableReferenceActions	referenceAction = null;
	private Table					table			= null;

	/**
	 * 
	 */
	public TableReferenceClause() {
	}

	/**
	 * Returns the reference action of a particular table
	 * 
	 * @return the reference action of a particular table
	 */
	public TableReferenceActions getReferenceAction() {
		return referenceAction;
	}

	/**
	 * Returns the reference table
	 * 
	 * @return the reference table
	 */
	public Table getTable() {
		return table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setInsertInto(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setInsertInto(Table table) {
		this.referenceAction = TableReferenceActions.Into;
		this.table = table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setSelectFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setSelectFrom(Table table) {
		this.referenceAction = TableReferenceActions.From;
		this.table = table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setDeleteFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setDeleteFrom(Table table) {
		this.referenceAction = TableReferenceActions.From;
		this.table = table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setTable(Table table) {
		this.referenceAction = null;
		this.table = table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
