/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Dataset;

/**
 * This clause contains the name of columns and the values for those.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class UpdateSetClause extends SyntaxClause implements SQLClause {
	private List<Dataset> dataset = null;

	/**
	 * 
	 */
	public UpdateSetClause() {

	}

	/**
	 * Returns the dataset for the {@link UpdateStatement} to use
	 * 
	 * @return the dataset for the {@link UpdateStatement} to use
	 */
	public List<Dataset> getDataset() {
		return dataset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		this.dataset = dataset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		if(null == this.dataset){
			this.dataset = new ArrayList<>();
		}
		this.dataset.add(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
