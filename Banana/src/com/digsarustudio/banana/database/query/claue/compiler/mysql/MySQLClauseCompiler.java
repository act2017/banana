/**
 * 
 */
package com.digsarustudio.banana.database.query.claue.compiler.mysql;

import java.util.List;

import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLKeywords;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.claue.AggregationClause;
import com.digsarustudio.banana.database.query.claue.ConditionClause;
import com.digsarustudio.banana.database.query.claue.DuplicateKeyUpdateClause;
import com.digsarustudio.banana.database.query.claue.InsertColumnClause;
import com.digsarustudio.banana.database.query.claue.InsertValueClause;
import com.digsarustudio.banana.database.query.claue.JoinConditionClause;
import com.digsarustudio.banana.database.query.claue.JoinTableClause;
import com.digsarustudio.banana.database.query.claue.JoinTableReferenceClause;
import com.digsarustudio.banana.database.query.claue.LimitClause;
import com.digsarustudio.banana.database.query.claue.ModifierClause;
import com.digsarustudio.banana.database.query.claue.OrderingClause;
import com.digsarustudio.banana.database.query.claue.SelectColumnClause;
import com.digsarustudio.banana.database.query.claue.TableReferenceClause;
import com.digsarustudio.banana.database.query.claue.UpdateSetClause;
import com.digsarustudio.banana.database.query.claue.VirtualTableReferenceClause;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.mathfunc.MathematicalFunction;
import com.digsarustudio.banana.database.query.mathfunc.Round;
import com.digsarustudio.banana.database.query.time.DateTime;
import com.digsarustudio.banana.database.query.time.FromUnixTime;
import com.digsarustudio.banana.database.query.time.Now;
import com.digsarustudio.banana.database.query.time.Str2Date;
import com.digsarustudio.banana.database.query.time.Timestamp;
import com.digsarustudio.banana.database.query.time.TimestampAdd;
import com.digsarustudio.banana.database.query.time.TimestampDiff;
import com.digsarustudio.banana.database.table.DefaultValues;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * A {@link ClauseCompiler} for MySQL database.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.2
 * @version		1.0.4
 * <br>
 * Note:<br>
 * 				1.0.4	->	To support enclosure condition for query.<br>
 *
 */
public class MySQLClauseCompiler implements ClauseCompiler {

	/**
	 * 
	 */
	public MySQLClauseCompiler() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.ModifierClause)
	 */
	@Override
	public String compile(ModifierClause clause) {
		if( null == clause || null == clause.getSyntaxModifier() ){
			return null;			
		}
		
		return clause.getSyntaxModifier().getValue();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.TableReferenceClause)
	 */
	@Override
	public String compile(TableReferenceClause clause) {
		if( null == clause || null == clause.getTable() ){
			return null;			
		}
		
		StringBuilder builder = new StringBuilder();
		if( null != clause.getReferenceAction() ){
			builder.append(clause.getReferenceAction().getValue());
			builder.append(" ");
		}
		
		//The escape symbol must be done in Table#getName()
		builder.append(StringFormatter.format("%s", clause.getTable().toSQLString()));
		
		if( null != clause.getTable().getAlias() ){
			builder.append(StringFormatter.format(" `%s`", clause.getTable().getAlias()));
		}
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.VirtualTableReferenceClause)
	 */
	@Deprecated
	@Override
	public String compile(VirtualTableReferenceClause clause) {
		throw new UnsupportedOperationException("Please use #compile(TableReferenceClause) instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.InsertColumnClause)
	 */
	@Override
	public String compile(InsertColumnClause clause) {
		if( null == clause || null == clause.getColumns() ){
			return null;			
		}
		
		StringBuilder builder = new StringBuilder();
		
		List<Dataset> columns = clause.getColumns();
		Integer count = 0;
		for (Dataset dataset : columns) {
			if(null == dataset.getTableColumn()) {
				continue;
			}
			
			if(0 != count++){
				builder.append(", ");
			}
			
			builder.append(StringFormatter.format("`%s`", dataset.getTableColumn().getColumn()));
		}
		
		return StringFormatter.format("(%s)", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.InsertValueClause)
	 */
	@Override
	public String compile(InsertValueClause clause) {
		if( null == clause || null == clause.getValues() ){
			return null;			
		}
		
		StringBuilder builder = new StringBuilder();
		
		List<Dataset> values = clause.getValues();
		Integer count = 0;
		for (Dataset dataset : values) {
			if( null == dataset ) {
				continue;
			}
			
			if(0 != count++){
				builder.append(", ");
			}
						
			//Default
			if( null == dataset.getValue() && !dataset.hasFunctions()){
				builder.append(SQLKeywords._Default.getValue());
				continue;
			}

			//Values
			builder.append(this.getValueSQLString(dataset));

		}
		
		return StringFormatter.format("VALUES(%s)", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.DuplicateKeyUpdateClause)
	 */
	@Override
	public String compile(DuplicateKeyUpdateClause clause) {
		if(null == clause || null == clause.getAssignments() ){
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		List<DuplicateKeyUpdateAssignment> assignments = clause.getAssignments();
		
		Integer count = 0;
		for (DuplicateKeyUpdateAssignment assignment : assignments) {
			if( 0 != count++ ){
				builder.append(", ");
			}
			
			builder.append(StringFormatter.format("`%s`=%s", assignment.getColumn(), assignment.getRightOperand()));
		}
		
		return StringFormatter.format("ON DUPLICATE KEY UPDATE %s", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.SelectColumnClause)
	 */
	@Override
	public String compile(SelectColumnClause clause) {
		if( null == clause || null == clause.getColumns() ){
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		
		List<SelectColumn> columns = clause.getColumns();
		
		if( !columns.isEmpty() ){
			Integer count = 0;
			for (SelectColumn column : columns) {
				if( 0 != count++ ){
					builder.append(", ");
				}
				
				builder.append(this.getColumnSQLString(column));
			}
		}
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.JoinTableClause)
	 */
	@Override
	public String compile(JoinTableClause clause) {
		//TODO [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition
		//	   [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition
		if( null == clause || null == clause.getJoinTable() ){
			return null;
		}
		
		StringBuilder builder = new StringBuilder(); 
		JoinTable table = clause.getJoinTable();

		builder.append(table.getType().getValue());
		builder.append(" ");
		//The Table deals with the escape symbol by itself.
//		builder.append(StringFormatter.format("`%s`", table.getTable().getName()));
		builder.append(StringFormatter.format("%s", table.getTable().toSQLString()));
		if( null != table.getAlias() ) {
			builder.append(" AS `");
			builder.append(table.getAlias());
			builder.append("`");
		}
		
		List<JoinCondition> conditions = table.getConditions();
		StringBuffer buffer = new StringBuffer();
		for (JoinCondition condition : conditions) {
			if(null == condition){
				continue;
			}
			
			buffer.append(this.getConditionSQLString(condition));
		}
		
		builder.append(StringFormatter.format(" ON (%s)", buffer.toString()));
			
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.JoinTableReferenceClause)
	 */
	@Deprecated
	@Override
	public String compile(JoinTableReferenceClause clause) {
		//TODO [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition
		//	   [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition
		//JoinTableRererenceClause should be refined.
		
		if( null == clause || null == clause.getJoinTables() ){
			return null;
		}
		
		StringBuilder builder = new StringBuilder(); 
		List<JoinTable> tables = clause.getJoinTables();
		Integer count = 0;
		for (JoinTable table : tables) {
			//The first one goes to outside of the bracket.
			if( 0 != count++){
				builder.append(" ");
			}
			
			builder.append(table.getType().getValue());
			builder.append(" ");			
			builder.append(table.getTable().getName());
		}
		
		return StringFormatter.format("%s (%s)", clause.getJoinTables().get(0).getType().getValue()
											   , builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.JoinConditionClause)
	 */
	@Deprecated
	@Override
	public String compile(JoinConditionClause clause) {
		//TODO [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition
		//To be combined with JoinTableRererenceClause
		
		if(null == clause || null == clause.getConditions() || clause.getConditions().isEmpty()){
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		List<JoinCondition> conditions = clause.getConditions();
		
		for (JoinCondition condition : conditions) {
			if(null == condition){
				continue;
			}
			
			builder.append(this.getConditionSQLString(condition));
		}
		
		
		return StringFormatter.format("ON (%s)", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.ConditionClause)
	 */
	@Override
	public String compile(ConditionClause clause) {
		if(null == clause || null == clause.getConditions() || clause.getConditions().isEmpty()){
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		List<Condition> conditions = clause.getConditions();
		
		Boolean isAggregationCondition = false;
		for (Condition condition : conditions) {
			if(null == condition){
				continue;
			}
			
			if( condition.hasAggregateFunction() ){
				isAggregationCondition = true;
			}
			
			builder.append(this.getConditionSQLString(condition));
		}
		
		String format = isAggregationCondition ? "HAVING %s" : "WHERE %s";
		
		return StringFormatter.format(format, builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.AggregationClause)
	 */
	@Override
	public String compile(AggregationClause clause) {
		if(null == clause || null == clause.getAggregations() || clause.getAggregations().isEmpty()) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		List<Aggregation> aggregations = clause.getAggregations();
		
		Integer count = 0;
		for (Aggregation aggregation : aggregations) {
			if(null == aggregation) {
				continue;
			}
			
			if(0 != count++) {
				builder.append(", ");
			}
			
			builder.append(StringFormatter.format("%s %s", this.getColumnSQLString(aggregation.getColumn()), aggregation.getSortingType().getValue()));
		}
		
		
		return StringFormatter.format("GROUP BY %s", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.UpdateSetClause)
	 */
	@Override
	public String compile(UpdateSetClause clause) {
		if(null == clause || null == clause.getDataset()) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		List<Dataset> dataset = clause.getDataset();
		Integer count = 0;
		for (Dataset data : dataset) {
			if( null == data || !data.validate()) {
				continue;
			}
			
			if(0 != count++) {
				builder.append(", ");
			}
			
			builder.append(this.getDatasetSQLString(data));
		}
		
		return StringFormatter.format("SET %s", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.LimitClause)
	 */
	@Override
	public String compile(LimitClause clause) {
		if(null == clause || null == clause.getLimit()) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		
		if(null != clause.getOffset()) {
			builder.append(clause.getOffset().toString());
			builder.append(", ");
		}
		
		builder.append(clause.getLimit().toString());
		
		return StringFormatter.format("LIMIT %s", builder.toString());
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.ClauseCompiler#compile(com.digsarustudio.banana.database.query.claue.OrderingClause)
	 */
	@Override
	public String compile(OrderingClause clause) {
		if(null == clause || null == clause.getOrderings()) {
			return null;
		}
		
		StringBuilder builder = new StringBuilder();
		List<Ordering> orderings = clause.getOrderings();
		
		Integer count = 0;
		for (Ordering ordering : orderings) {
			if(null == ordering) {
				continue;
			}
			
			if(0 != count++) {
				builder.append(", ");
			}
			
			builder.append(StringFormatter.format("%s %s", this.getColumnSQLString(ordering.getColumn()), ordering.getType().getValue()));
		}
		
		
		return StringFormatter.format("ORDER BY %s", builder.toString());
	}

	private String getColumnSQLString(SimpleTableColumn column) {
		if(null == column) {
			return "";
		}
		
		StringBuilder builder = new StringBuilder();
		String format = "`%s`";
		if( null != column.getTable() ) {
			builder.append(column.getTable());
			builder.append(".");
			
			format = "%s";
		}
		
		if(null != column.getColumn()) {
			builder.append(StringFormatter.format(format, column.getColumn()));
		}
		
		return builder.toString();
	}
	
	private String getColumnSQLString(Condition condition){
		String format = "%s";
		
		if( condition.hasAggregateFunction() ){
			format = StringFormatter.format(condition.getAggregateFunction().getFormat(), format);
		}
		
		return StringFormatter.format(format, this.getColumnSQLString(condition.getTableColumn()));
	}
	
	private String getColumnSQLString(SelectColumn column){
		String format = "%s", value = this.getColumnSQLString(column.getTableColumn());
		StringBuffer buffer = new StringBuffer();
		
		
		if( column.hasSQLStringFunction() ){
			format = column.getStringFunction().getFormat();
		}
		
		if( column.hasSQLAggregationFunction() ){
			format = column.getAggregateFunction().getFormat();
		}
		
		if( column.hasMiscellaneousFunction() ) {
			format = column.getMiscellaneousFunction().getFormat();
		}
		
		if( column.hasDateTime() ) {
			value = this.getDateTimeSQLString(column.getDateTime());
		}
		
		if( column.hasFormula() ) {
			format = column.getFormula().toSQLString();
		}
		
		if( column.hasMathematicalFunction() ) {
			format = this.getMathematicalFunctionSQLString(column.getMathematicalFunction());
		}
		
		buffer.append(StringFormatter.format(format, value));
		
		if(null != column.getAlias() && !column.getAlias().isEmpty()) {
			buffer.append(StringFormatter.format(" AS '%s'", column.getAlias()));
		}
		
		return buffer.toString();
	}
	
	/**
	 * This method should handle nested functions, but it is not implemented yet.<br> 
	 * 
	 * @param dataset
	 * @return
	 */
	private String getValueSQLString(Dataset dataset) {
		String  format = "%s";
		
		String value = (null != dataset.getValue() && dataset.getValue().isEmpty()) ? "NULL" : dataset.getValue();
		
		if(null != dataset.getEncryptionFunction()) {
			format = dataset.getEncryptionFunction().getFormat();
		}
		
		if(null != dataset.getStringFunction()) {
			format = dataset.getStringFunction().getFormat();
		}
		
		if( dataset.hasMiscellaneousFunction() ) {
			format = dataset.getMiscellaneousFunction().getFormat();
		}
		
		if( dataset.hasDateTime() ) {
			value = this.getDateTimeSQLString(dataset.getDateTime());
		}
		
		if( dataset.hasFormula() ) {
			format = dataset.getFormula().toSQLString();
		}				
		
		//The customized one has highest priority.<br>
		if( dataset.hasCustomizedFunction() ) {
			format = dataset.getCustomizedFunction();
		}
		
		return StringFormatter.format(format, value);
	}
	
	private String getValueSQLString(Condition condition) {
		String  format = "%s";
		String value = condition.getValue();
		
		if( null != condition.isValueNullable() ) {
			format = StringFormatter.format(" %s", condition.isValueNullable() ? DefaultValues.isNull.getValue() 
																			  : DefaultValues.isNotNull.getValue());
		}else {
			if(null != condition.getEncryptionFunction()) {
				format = condition.getEncryptionFunction().getFormat();
			}
			
			if(null != condition.getStringFunction()) {
				format = condition.getStringFunction().getFormat();
			}
			
			if( condition.hasMiscellaneousFunction() ) {
				format = condition.getMiscellaneousFunction().getFormat();
			}
			
			if( condition.hasDateTime() ) {
				value = this.getDateTimeSQLString(condition.getDateTime());
			}
		}
		
		return StringFormatter.format(format, value);
	}
	
	private String getDatasetSQLString(Dataset dataset) {
		return StringFormatter.format("%s = %s", this.getColumnSQLString(dataset.getTableColumn()), this.getValueSQLString(dataset));
	}
	
	/**
	 * If the subquery has to be used in the condition clause, please modify this function to support it.
	 *  
	 * @param condition The condition for a clause
	 * 
	 * @return A SQL string converted from {@link Condition}
	 */
	private String getConditionSQLString(Condition condition) {
		StringBuilder builder = new StringBuilder();
						
		if(null != condition.getOperation()) {
			builder.append(" ");
			builder.append(condition.getOperation().getValue());
			builder.append(" ");
		}

		if( condition.hasEnclosedCondition() ) {
			List<Condition> enclosedCons = condition.getEnclosedConditions();
			builder.append("(");
			for (Condition enclosedCon : enclosedCons) {
				builder.append(" ");
				builder.append(this.getConditionSQLString(enclosedCon));				
			}
			builder.append(")");
		}else {		
			builder.append(this.getColumnSQLString(condition));
			
			if(null != condition.getComparator()) {
				builder.append(" ");
				builder.append(condition.getComparator().getValue());	
			}
			
			builder.append(" ");
			builder.append(this.getValueSQLString(condition));
		}
		
		return builder.toString();
	}
	
	private String getConditionSQLString(JoinCondition condition){
		StringBuilder builder = new StringBuilder();

		if(null != condition.getOperation()) {
			builder.append(" ");
			builder.append(condition.getOperation().getValue());			
		}

		builder.append(StringFormatter.format("%s = %s", this.getColumnSQLString(condition.getRefereeColumn())
													  , this.getColumnSQLString(condition.getReferentialColumn())));		
		
		return builder.toString();
	}
	
	private String getMathematicalFunctionSQLString(MathematicalFunction function) {
		String rtn = null;
		switch(function.getFunction()) {
			case Round:
				rtn = formatRoundFunction(function);
				break;
			default:
				throw new UnsupportedOperationException(function.getFunction() + " is not supported yet.");
		}
		
		return rtn;
	}
	
	private String formatRoundFunction(MathematicalFunction function) {
		Round roundFunc = (Round) function;
		String rtn = "";
		
		//Check the source
		if( roundFunc.hasColumnTarget() ) {
			rtn = StringFormatter.format(roundFunc.getFunction().getFormat()
									   , roundFunc.getColumnTarget2Round().getQuerySimpleTableColumn()
									   , roundFunc.getDigit2Round());
		}else if( roundFunc.hasFormulaTarget() ) {
			rtn = StringFormatter.format(roundFunc.getFunction().getFormat()
									//Due to the formula is the same in different database system,
									//so that just print it out directly.
									   , roundFunc.getFormulaTarget2Round().toSQLString()
									   , roundFunc.getDigit2Round());
		}else if( roundFunc.hasStringTarget() ) {
			rtn = StringFormatter.format(roundFunc.getFunction().getFormat()
									  , roundFunc.getStringTarget2Round(), roundFunc.getDigit2Round());
		}
		
		return rtn;
	}
	
	private String getDateTimeSQLString(DateTime dateTime) {
		String rtn = null;
		
		switch(dateTime.getFunction()) {
			case Now:
				rtn = formatDateTimeNow(dateTime);
				break;
			case FromUNIXTime:
				rtn = formatDateTimeFromUnixTime(dateTime);
				break;
			case Timestamp:
				rtn = formatDateTimeTimestamp(dateTime);
				break;
			case TimestampAdd:
				rtn = formatDateTimeTimestampAdd(dateTime);
				break;
			case TimestampDiff:
				rtn = formatDateTimeTimestampDiff(dateTime);
				break;
			case Str2Date:
				rtn = formatStr2Date(dateTime);
				break;
			default:
				throw new UnsupportedOperationException(dateTime.getFunction() + " is not supported yet.");
		}
		
		return rtn;
	}
	
	private String formatDateTimeNow(DateTime dateTime) {
		Now time = (Now) dateTime;
		
		return time.getFunction().getFormat();
	}
	
	private String formatDateTimeTimestamp(DateTime dateTime) {
		Timestamp time = (Timestamp) dateTime;
		
		StringBuffer buffer = new StringBuffer();
		buffer.append(time.getFunction().getName());
		buffer.append("(");
		buffer.append(time.getBaseTime());
		
		String addOnTime = time.getAddOnTime();
		if(null != addOnTime && !addOnTime.isEmpty()) {
			buffer.append(", ");
			buffer.append(addOnTime);
		}
		buffer.append(")");
		
		return buffer.toString();
	}
	
	private String formatDateTimeTimestampDiff(DateTime dateTime) {
		TimestampDiff time = (TimestampDiff) dateTime;
		return String.format(time.getFunction().getFormat(), time.getUnit().getValue()
							, time.getBaseTime(), time.getReferentialTime());
	}
	
	private String formatDateTimeTimestampAdd(DateTime dateTime) {
		TimestampAdd time = (TimestampAdd) dateTime;
		String inteval = String.format("'%s'", time.getInterval().toString());
		return String.format(time.getFunction().getFormat(), time.getUnit().getValue()
							, inteval, time.getBaseTime());
	}
	
	private String formatDateTimeFromUnixTime(DateTime dateTime) {
		FromUnixTime time = (FromUnixTime) dateTime;
		
		StringBuffer buffer = new StringBuffer();
		buffer.append(time.getFunction().getName());
		buffer.append("(");
		buffer.append(time.getBaseTime());
		
		String format = time.getFormat();
		if(null != format && !format.isEmpty()) {
			buffer.append(", ");
			buffer.append(String.format("'%s'", format));
		}
		buffer.append(")");
		
		return buffer.toString();
		
	}
	
	private String formatStr2Date(DateTime dateTime) {
		Str2Date time = (Str2Date)dateTime;
		
		StringBuffer buffer = new StringBuffer();
		buffer.append(time.getFunction().getName());
		buffer.append("('");
		buffer.append(time.getDateString());
		buffer.append("', '");
		buffer.append(time.getDateFormat());
		buffer.append("')");
		
		return buffer.toString();
		
	}
}
