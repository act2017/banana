/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.List;

import com.digsarustudio.banana.database.query.JoinTable;

/**
 * This clause indicates which tables are going to be joined and how to join the tables together.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class JoinTableClause extends SyntaxClause implements SQLClause {
	private	JoinTable joinTable	 = null;
	
	/**
	 * 
	 */
	public JoinTableClause() {

	}

	/**
	 * Returns the join tables
	 * 
	 * @return the join tables
	 */
	public JoinTable getJoinTable() {
		return this.joinTable;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setJoinTable(java.util.List)
	 */
	@Override
	public void setJoinTable(List<JoinTable> tables) {
		throw new UnsupportedOperationException("Please use #addJoinTable() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTable(JoinTable table) {
		this.joinTable = table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
