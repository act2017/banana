/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

/**
 * This clause contains the offset of the search and maximum row count for the retrieving.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class LimitClause extends SyntaxClause implements SQLClause {
	private Integer offset = null;		
	private Integer limit = null;
	
	/**
	 * 
	 */
	public LimitClause() {

	}

	/**
	 * Returns the row number of the 1st retrieved data.
	 * 
	 * @return the row number of the 1st retrieved data.
	 */
	public Integer getOffset() {
		return offset;
	}

	/**
	 * Returns the maximum data retrieved from database.
	 * 
	 * @return the maximum data retrieved from database.
	 */
	public Integer getLimit() {
		return limit;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setOffset(java.lang.Integer)
	 */
	@Override
	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		this.limit = rowCount;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
