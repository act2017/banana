/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import com.digsarustudio.banana.database.query.statement.SelectStatement;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * This clause contains a name and a {@link SelectStatement} which generates a table of data for the client code to 
 * select.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated 1.0.2 Please use {@link TableReferenceClause} for the dynamic table created by {@link SQLStatement}.
 */
public class VirtualTableReferenceClause extends SyntaxClause implements SQLClause {
	private VirtualTable	table 		= null;
	
	/**
	 * 
	 */
	public VirtualTableReferenceClause() {
	}

	/**
	 * Returns the virtual table
	 * 
	 * @return the virtual table
	 */
	public VirtualTable getVirtualTable() {
		return table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setSelectFrom(com.digsarustudio.banana.database.table.VirtualTable)
	 */
	@Override
	public void setSelectFrom(VirtualTable table) {
		this.table = table;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.compiler.compile(this);
	}
}
