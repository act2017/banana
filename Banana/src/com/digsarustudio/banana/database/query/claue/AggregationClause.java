/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Aggregation;

/**
 * This clause contains the name of column to aggregate as a group and its ordering.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class AggregationClause extends SyntaxClause implements SQLClause {
	private List<Aggregation> aggregations = null;
	
	/**
	 * 
	 */
	public AggregationClause() {
	
	}

	/**
	 * Returns the list of aggregations
	 * 
	 * @return the list of aggregations
	 */
	public List<Aggregation> getAggregations() {
		return aggregations;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#setAggregations(java.util.List)
	 */
	@Override
	public void setAggregations(List<Aggregation> aggregations) {
		this.aggregations = aggregations;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SyntaxClause#addAggregation(com.digsarustudio.banana.database.query.Aggregation)
	 */
	@Override
	public void addAggregation(Aggregation aggregation) {
		if(null == this.aggregations){
			this.aggregations = new ArrayList<>();
		}
		
		this.aggregations.add(aggregation);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.claue.SQLClause#build()
	 */
	@Override
	public void build() {
		this.clause = this.compiler.compile(this);
	}

}
