/**
 * 
 */
package com.digsarustudio.banana.database.query.claue;

import java.util.List;

import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLAggregateFunctions;
import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * The sub-type represents the clause used by {@link SQLStatement} for the {@link DataAccessObject}.<br>
 * In addition, the sub-type will implement what method it needs only and ignore the mutator which the sub-type 
 * doesn't have an attribute for. As the result, please only implementing what the sub-type needs.<br>
 * <br>
 * Note:<br> 
 * The sub-type {@link SQLClause} is going to be implemented by Builder pattern. If there is any new clause which 
 * has to be added into this system, the mutator must be set in this interface before the sub-type has been 
 * implemented.<br>
 * 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface SQLClause {
	/**
	 * Assigns the modifier for {@link SQLStatement} to use
	 * 
	 * @param modifier The modifier for {@link SQLStatement} to use
	 */
	void setModifier(SQLSyntaxModifiers modifier);
	
	/**
	 * To indicate the data will be inserted into a particular table.<br>
	 * 
	 * @param table The data to be inserted to
	 */
	void setInsertInto(Table table);
	
	/**
	 * To indicate the data will be fetched from a particular table.<br>
	 * 
	 * @param table The data to be retrieved from
	 */
	void setSelectFrom(Table table);
	
	/**
	 * To indicate the data will be fetched from a particular temporarily virtual table.<br>
	 * 
	 * @param table The data to be retrieved from
	 * 
	 * @deprecated 1.0.2 Please use {@link #setSelectFrom(Table)}
	 */
	void setSelectFrom(VirtualTable table);
	
	/**
	 * To indicate the data will be deleted from a particular table.<br>
	 * 
	 * @param table The data to be deleted from
	 */
	void setDeleteFrom(Table table);
	
	/**
	 * To indicate what table the data will be manipulated with.<br>
	 * 
	 * @param table what table the data will be manipulated with
	 */
	void setTable(Table table);
	
	/**
	 * Assigns a list of dataset for the {@link SQLStatement} to manipulate.<br>
	 * 
	 * This method is set by {@link InsertStatement} and {@link UpdateStatement} only.<br>
	 * 
	 * @param dataset The list of dataset for the {@link SQLStatement} to manipulate.
	 */
	void setDatasets(List<Dataset> dataset);
	
	/**
	 * Assigns a dataset for the {@link SQLStatement} to manipulate.<br>
	 * 
	 * This method is set by {@link InsertStatement} and {@link UpdateStatement} only.<br>
	 * 
	 * @param dataset The dataset for the {@link SQLStatement} to manipulate.
	 */
	void addDataset(Dataset dataset);

	/**
	 * Assigns a list of assignments used for updating the duplicate key.<br>
	 * 
	 * This method is set by {@link InsertStatement} only.
	 * 
	 * @param assignments The list of assignments used for updating the duplicate key
	 */
	void setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments);	
	
	/**
	 * Assigns an assignment used for updating the duplicate key.<br>
	 * 
	 * This method is set by {@link InsertStatement} only.
	 * 
	 * @param assignment The assignment used for updating the duplicate key
	 */
	void addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment);
	
	/**
	 * Assigns a set of columns to be selected in a search.<br>
	 * 
	 * This method is set by {@link SelectStatement} only.<br>
	 * 
	 * @param columns The set of columns to be selected in a search.
	 */
	void setSelectColumns(List<SelectColumn> columns);
	
	/**
	 * Assigns a list of tables for the join.<br>
	 * 
	 * @param tables The list of tables for the join.
	 */
	void setJoinTable(List<JoinTable> tables);
	
	/**
	 * Assigns a table for the join.<br>
	 * 
	 * @param table The table for the join.
	 */
	void addJoinTable(JoinTable table);
	
	/**
	 * Assigns a column to be selected in a search.<br>
	 * 
	 * This method is set by {@link SelectStatement} only.<br>
	 * 
	 * @param column The column to be selected in a search.
	 * 
	 */
	void addSelectColumn(SelectColumn column);
	
	/**
	 * Assigns a list of tables for the join.<br>
	 * 
	 * @param tables The list of tables for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTable(List)} instead
	 */
	void setJoinTableReferences(List<JoinTable> tables);
	
	/**
	 * Assigns a table for the join.<br>
	 * 
	 * @param table The table for the join.
	 * @deprecated 1.0.2 Please use {@link #setJoinTable(List)} instead
	 */
	void addJoinTableReference(JoinTable table);
	
	
	/**
	 * Assigns a type which indicates how to join the tables.<br>
	 * 
	 * @param type The type which indicates how to join the tables.
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTableReferences(List)} instead
	 * 
	 */
	void setJoinType(TableJoinType type);
	
	/**
	 * Assigns a list of tables for the join.<br>
	 * 
	 * @param tables The list of tables for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTableReferences(List)} instead
	 */
	void setJoinTableRefereces(List<Table> tables);
	
	/**
	 * Assigns a table for the join.<br>
	 * 
	 * @param table The table for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #addJoinCondition(JoinCondition)} instead
	 */
	void addJoinTableReference(Table table);
	
	/**
	 * Assigns a list of condition for the set of particular data to join.<br>
	 * 
	 * @param conditionsa The list of condition for the set of particular data to join
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTable(List)} instead
	 */
	void setJoinConditions(List<JoinCondition> conditions);
	
	/**
	 * Assigns a condition for the particular data to join
	 * 
	 * @param condition The condition for the particular data to join
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTable(List)} instead
	 */
	void addJoinCondition(JoinCondition condition);
	
	/**
	 * Assigns a set of conditions used for finding data.<br>
	 * 
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * 
	 * @param conditions The set of conditions used for finding data.<br>
	 */
	void setConditions(List<Condition> conditions);
	
	/**
	 * Assigns a condition used for finding data.<br>
	 * 
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * 
	 * @param condition The condition used for finding data.<br>
	 */
	void addCondition(Condition condition);
	
	/**
	 * Assign a set of aggregation which indicate how to distinguish, summarize, and average the data and so on.<br>
	 * <br>
	 * This method is set by {@link SelectStatement} only and the {@link SQLAggregateFunctions} could be set 
	 * for some columns which has to be manipulated in aggregation.<br>
	 * If the search is set as distinct search, this method should be called for the column which has to be 
	 * grouped together to hide the reduplicated data.<br>
	 * 
	 * @param aggregations The set of aggregations which indicate how to summarize and average the data and so on.
	 */
	void setAggregations(List<Aggregation> aggregations);
	
	/**
	 * Assign an aggregation which indicates how to distinguish, summarize, and average the data and so on.<br>
	 * <br>
	 * This method is set by {@link SelectStatement} only and the {@link SQLAggregateFunctions} could be set 
	 * for some columns which has to be manipulated in aggregation.<br>
	 * If the search is set as distinct search, this method should be called for the column which has to be 
	 * grouped together to hide the reduplicated data.<br>
	 * 
	 * @param aggregation The aggregation which indicates how to summarize and average the data and so on.
	 */
	void addAggregation(Aggregation aggregation);
	
	/**
	 * Assigns a set of ordering which indicates how the data to be going to be manipulated.<br>
	 * In {@link UpdateStatement} and {@link DeleteStatement}, the data are updated or deleted in the order that 
	 * is specified. In {@link SelectStatement}, the data are fetched and listed in the order that is specified.<br>
	 * <br>
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * 
	 * @param orderings The set of ordering which indicates how the data to be going to be manipulated.
	 */
	void setOrderings(List<Ordering> orderings);
	
	/**
	 * Assigns an ordering of the data to be manipulated.<br>
	 * In {@link UpdateStatement} and {@link DeleteStatement}, the data are updated or deleted in the order that 
	 * is specified. In {@link SelectStatement}, the data are fetched and listed in the order that is specified.<br>
	 * <br>
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * 
	 * @param ordering The ordering of the data to be manipulated.
	 */
	void addOrdering(Ordering ordering);
	
	/**
	 * Assigns the starting row of a search to retrieve data.<br>
	 * <br>
	 * This method is only set by {@link SelectStatement}.<br>
	 * 
	 * @param offset The index of the row to be the 1st data to retrieve. This value is 1-based index.
	 */
	void setOffset(Integer offset);
	
	/**
	 * Assigns the maximum count of row to be retrieved from a particular table.<br>
	 * <br>
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * 
	 * @param rowCount THe maximum count of row to be retrieved from a particular table.
	 */
	void setMaxRowCount(Integer rowCount);
	
	/**
	 * Assigns a compiler for the clause to build SQL string.
	 * 
	 * @param compiler The compile for the clause to build SQL string.
	 */
	void setCompiler(ClauseCompiler compiler);
	
	/**
	 * To build the specified clause
	 */
	void build();
	
	/**
	 * Returns the result of {@link #build()} in SQL form
	 * 
	 * @return the result of {@link #build()} in SQL form
	 */
	String getResult();
}
