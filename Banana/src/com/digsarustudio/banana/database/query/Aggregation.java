/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class contains the name of column to aggregate and the ordering for {@link SelectStatement}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class Aggregation {
	/**
	 * 
	 * The object builder for {@link Aggregation}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<Aggregation> {
		private Aggregation aggregation = null;
		
		public Builder(){
			this.aggregation = new Aggregation();
		}

		/**
		 * Assign a column to be aggregated
		 * 
		 * @param column the column to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setColumn(SimpleTableColumn column) {
			this.aggregation.setColumn(column);
			return this;
		}

		/**
		 * Assigns a sorting type to the aggregated column
		 * 
		 * @param sortingType the sortingType to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setSortingType(SortingType sortingType) {
			this.aggregation.setSortingType(sortingType);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Aggregation build() {
			return this.aggregation;
		}

	}

	/**
	 * The column to be aggregated together for the {@link SelectStatement}<br>
	 */
	private SimpleTableColumn	column		= null;
	
	/**
	 * The sorting type when the specific column to be aggregated.<br>
	 */
	private	SortingType			sortingType	= SortingType.getDefault();
	
	/**
	 * 
	 */
	private Aggregation() {
		
	}

	/**
	 * Returns the column to be aggregated
	 * 
	 * @return the column to be aggregated
	 */
	public SimpleTableColumn getColumn() {
		return column;
	}

	/**
	 * Assign a column to be aggregated
	 * 
	 * @param column the column to set
	 */
	public void setColumn(SimpleTableColumn column) {
		this.column = column;
	}

	/**
	 * Returns the sorting type of the aggregated column
	 * 
	 * @return the sorting type of the aggregated column
	 */
	public SortingType getSortingType() {
		return sortingType;
	}

	/**
	 * Assigns a sorting type to the aggregated column
	 * 
	 * @param sortingType the sortingType to set
	 */
	public void setSortingType(SortingType sortingType) {
		this.sortingType = sortingType;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
