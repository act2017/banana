/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.query.statement.DeleteStatement;
import com.digsarustudio.banana.database.query.statement.InsertStatement;
import com.digsarustudio.banana.database.query.statement.JoinStatement;
import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.database.query.statement.SelectStatement;
import com.digsarustudio.banana.database.query.statement.UpdateStatement;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-types of this table are used by {@link SQLStatement} to operate a particular table.<br>
 * In the {@link InsertStatement}, {@link UpdateStatement}, and {@link DeleteStatement}, this table provides the name and alias for the SQL query.<br>
 * It is the same as in the {@link SelectStatement} in some circumstance, but also it can provide a temporary(virtual) table constructed by another query results 
 * of {@link SelectStatement} for the query, which is as same as used in the {@link JoinStatement}.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface Table {
	/**
	 * 
	 * The builder for {@link Table}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static interface Builder extends ObjectBuilder<Table> {
		/**
		 * Assigns a name to this table.
		 * 
		 * @param name The name for this table
		 * 
		 * @return The instance of this table
		 */
		Builder setName(String name);
		
		/**
		 * Assigns a select statement for this table
		 * 
		 * @param statment A select statement for this table
		 * 
		 * @return The instance of this table
		 */
		Builder setSelectStatement(SQLStatement statement);
		
		/**
		 * Assigns an alias to this table
		 * 
		 * @param alias An alias for this table
		 * 
		 * @return The instance of this builder
		 */
		Builder setAlias(String alias);
	}
	
	/**
	 * Returns the name of this table or a SQL query string returned if the table is constructed by a select query.<br>
	 * 
	 * @return the name of this table or a SQL query string returned if the table is constructed by a select query.
	 */
	String getName();
	
	/**
	 * Returns the name of this table or a SQL query string returned if the table is constructed 
	 * by a select query in the SQL syntax form.<br>
	 * 
	 * @return the name of this table or a SQL query string returned if the table is constructed 
	 * 			by a select query in the SQL syntax form
	 */
	String toSQLString();
	
	/**
	 * Returns the alias of this table
	 * 
	 * @return The alias of this table
	 */
	String getAlias();
}
