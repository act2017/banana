/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * This class represents an assignment used when a data is going to be inserted into a table 
 * to deal with the duplication of key.<br>
 * 
 * Reference:<br>
 * 1.https://dev.mysql.com/doc/refman/5.7/en/insert-on-duplicate.html<br>
 * 
 * Example:<br>
 * 1.INSERT INTO t1 (a,b,c) VALUES (1,2,3) ON DUPLICATE KEY UPDATE c=c+1;<br>
 * 		equals to<br>
 * 	 UPDATE t1 SET c=c+1 WHERE a=1;<br>
 * <br>
 * 2.INSERT INTO t1 (a,b,c) VALUES (1,2,3),(4,5,6) ON DUPLICATE KEY UPDATE c=VALUES(a)+VALUES(b);<br>
 * 		equals to<br>
 * 	 INSERT INTO t1 (a,b,c) VALUES (1,2,3) ON DUPLICATE KEY UPDATE c=3;<br>
 * 		and<br>
 * 	 INSERT INTO t1 (a,b,c) VALUES (4,5,6) ON DUPLICATE KEY UPDATE c=9;<br>
 * <br>
 * 3.INSERT INTO t1 (a, b) SELECT * FROM<br>
 * 									(SELECT c, d FROM t2 UNION SELECT e, f FROM t3) AS dt<br>
 * 									ON DUPLICATE KEY UPDATE b = b + c;<br>
 * <br>
 * <br>
 * TODO To be implement when this clause is going to be implemented.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class DuplicateKeyUpdateAssignment {
	/**
	 * 
	 * The object builder for {@link DuplicateKeyUpdateAssignment}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<DuplicateKeyUpdateAssignment> {
		private DuplicateKeyUpdateAssignment result = null;

		public Builder() {
			this.result = new DuplicateKeyUpdateAssignment();
		}

		/**
		 * Assign a column to be updated when a collision occurs on the super keys.<br>
		 * 
		 * @param column the column to set
		 * 
		 * @return The instance of this builder
		 */
		public Builder setColumn(SimpleTableColumn column) {
			this.result.setColumn(column);
			return this;
		}

		/**
		 * To assign a value to the right operand.<br>
		 * 
		 * @param value The value for the right operand.
		 * 
		 * @return The instance of this builder
		 */
		public Builder setValue(String value){
			this.result.setValue(value);
			return this;
		}
		
		/**
		 * To assign the column will be summarized to be the value of right operand
		 * 
		 * @param column the column will be summarized to be the value of right operand
		 * 
		 * @return The instance of this builder
		 */
		public Builder setValueFrom(SimpleTableColumn column){
			this.result.setValueFrom(column);
			return this;
		}
		
		/**
		 * To assign the columns which will be summarized to be the value of right operand
		 * 
		 * @param columns The columns which will be summarized to be the value of right operand
		 * 
		 * @return The instance of this builder
		 */
		public Builder setValueSummaryFrom(SimpleTableColumn... columns){
			this.result.setValueSummaryFrom(columns);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DuplicateKeyUpdateAssignment build() {
			return this.result;
		}

	}
	
	/**
	 * The column to be updated if a collision occurs on the super keys.<br>
	 */
	private SimpleTableColumn	column			= null;
	
	/**
	 * A value or a function which provides the new value for the data with the collision super keys.<br>
	 * <br>
	 * Example:<br>
	 * 	1.1<br>
	 * 	2.column+1<br>
	 *  3.VALUES(column1)+VALUES(column2)<br>
	 */
	private String 				rightOperand	= null;
	
	/**
	 * 
	 */
	private DuplicateKeyUpdateAssignment() {
		
	}

	/**
	 * Returns the column to be updated when a collision occurs on the super keys.<br>
	 * 
	 * @return the column to be updated when a collision occurs on the super keys.
	 */
	public SimpleTableColumn getColumn() {
		return column;
	}
	
	/**
	 * Return the value or the function which provides the new value for the data with the collision super keys.<br>
	 * 
	 * @return the value or the function which provides the new value for the data with the collision super keys.
	 */
	public String getRightOperand(){
		return this.rightOperand;
	}

	/**
	 * Assign a column to be updated when a collision occurs on the super keys.<br>
	 * @param column the column to set
	 */
	public void setColumn(SimpleTableColumn column) {
		this.column = column;
	}

	/**
	 * To assign a value to the right operand.<br>
	 * 
	 * @param value The value for the right operand.
	 */
	public void setValue(String value){
		this.rightOperand = value;
	}
	
	/**
	 * To assign the column will be summarized to be the value of right operand
	 * 
	 * @param column the column will be summarized to be the value of right operand
	 */
	public void setValueFrom(SimpleTableColumn column){
		this.rightOperand = this.formatColumn(MiscellaneousFunctions.Values, this.column);
	}
	
	/**
	 * To assign the columns which will be summarized to be the value of right operand
	 * 
	 * @param columns The columns which will be summarized to be the value of right operand
	 */
	public void setValueSummaryFrom(SimpleTableColumn... columns){
		StringBuilder builder = new StringBuilder();
		
		Integer count = 0;
		for (SimpleTableColumn column : columns) {
			if( count++ != 0 ){
				builder.append("+");
			}
				
			builder.append(this.formatColumn(MiscellaneousFunctions.Values, column));
		}
		
		this.rightOperand = builder.toString();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	private String formatColumn(MiscellaneousFunctions function, SimpleTableColumn column){
		String col = StringFormatter.format("`%s`", column.getColumn());
		return StringFormatter.format(function.getValue(), col);
	}
}
