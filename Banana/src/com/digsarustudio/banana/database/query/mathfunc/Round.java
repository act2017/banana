/**
 * 
 */
package com.digsarustudio.banana.database.query.mathfunc;

import com.digsarustudio.banana.database.query.Formula;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The implementation of {@link MathematicalFunctions#Round}.<br> 
 * This class provides 3 interfaces for the client code to set the "target" up which are in String, 
 * {@link SimpleTableColumn}, and {@link Formula}. Please set up one from those three, the SQL query string 
 * compiler will choose the sequence by itself.<br> 
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public class Round extends BaseMathematicalFunction implements MathematicalFunction {
	/**
	 * 
	 * The object builder for {@link Round}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<Round> {
		private Round result = null;

		public Builder() {
			this.result = new Round();
		}

		/**
		 * @param digit
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setDigit2Round(java.lang.Integer)
		 */
		public Builder setDigit2Round(Integer digit) {
			result.setDigit2Round(digit);
			return this;
		}

		/**
		 * @param target
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setTarget2Round(java.lang.String)
		 */
		public Builder setTarget2Round(String target) {
			result.setTarget2Round(target);
			return this;
		}

		/**
		 * @param column
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setTarget2Round(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		public Builder setTarget2Round(SimpleTableColumn column) {
			result.setTarget2Round(column);
			return this;
		}

		/**
		 * @param formula
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setTarget2Round(com.digsarustudio.banana.database.query.Formula)
		 */
		public Builder setTarget2Round(Formula formula) {
			result.setTarget2Round(formula);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Round build() {
			return this.result;
		}

	}
	
	public static final MathematicalFunctions	FUNC		= MathematicalFunctions.Round;
	
	private Integer				digit2Round		= 0;
	private String				targetString		= null;
	private SimpleTableColumn	targetColumn		= null;
	private Formula				targetFormula	= null;
	
	/**
	 * @param function
	 */
	public Round() {
		super(FUNC);
	}

	public void setDigit2Round(Integer digit) {
		this.digit2Round = digit;
	}
	
	public void setTarget2Round(String target) {
		this.targetString = target;
	}
	
	public void setTarget2Round(SimpleTableColumn column) {
		this.targetColumn = column;
	}
	
	public void setTarget2Round(Formula formula) {
		this.targetFormula = formula;
	}

	public Integer getDigit2Round() {
		return this.digit2Round;
	}
	
	public String getStringTarget2Round() {
		return this.targetString;
	}
	
	public SimpleTableColumn getColumnTarget2Round() {
		return this.targetColumn;
	}
	
	public Formula getFormulaTarget2Round() {
		return this.targetFormula;
	}
	
	public Boolean hasStringTarget() {
		return (null != this.targetString);
	}
	
	public Boolean hasColumnTarget() {
		return (null != this.targetColumn);
	}
	
	public Boolean hasFormulaTarget() {
		return (null != this.targetFormula);
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
