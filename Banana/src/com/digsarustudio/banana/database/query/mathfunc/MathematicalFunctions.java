/**
 * 
 */
package com.digsarustudio.banana.database.query.mathfunc;

/**
 * The mathematical function for database.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public enum MathematicalFunctions {
	//	ASIN()	Return the arc sine
	//	ATAN()	Return the arc tangent
	//	ATAN2(), ATAN()	Return the arc tangent of the two arguments
	//	CEIL()	Return the smallest integer value not less than the argument
	//	CEILING()	Return the smallest integer value not less than the argument
	//	CONV()	Convert numbers between different number bases
	//	COS()	Return the cosine
	//	COT()	Return the cotangent
	//	CRC32()	Compute a cyclic redundancy check value
	//	DEGREES()	Convert radians to degrees
	//	EXP()	Raise to the power of
	//	FLOOR()	Return the largest integer value not greater than the argument
	//	LN()	Return the natural logarithm of the argument
	//	LOG()	Return the natural logarithm of the first argument
	//	LOG10()	Return the base-10 logarithm of the argument
	//	LOG2()	Return the base-2 logarithm of the argument
	//	MOD()	Return the remainder
	//	PI()	Return the value of pi
	//	POW()	Return the argument raised to the specified power
	//	POWER()	Return the argument raised to the specified power
	//	RADIANS()	Return argument converted to radians
	//	RAND()	Return a random floating-point value
	
	/**
	 * ROUND()	Round the argument<br>
	 * <br>
	 * ROUND(X), ROUND(X,D)<br>
	 * Rounds the argument X to D decimal places. The rounding algorithm depends on the data type of X. D defaults to
	 *  0 if not specified. D can be negative to cause D digits left of the decimal point of the value X to become 
	 *  zero.<br>
	 * mysql> SELECT ROUND(-1.23);<br>
	 * 			-> -1<br>
	 * mysql> SELECT ROUND(-1.58);<br>
	 * 			-> -2<br>
	 * mysql> SELECT ROUND(1.58);<br>
	 * 			-> 2<br>
	 * mysql> SELECT ROUND(1.298, 1);<br>
	 * 			-> 1.3<br>
	 * mysql> SELECT ROUND(1.298, 0);<br>
	 * 			-> 1<br>
	 * mysql> SELECT ROUND(23.298, -1);<br>
	 * 			-> 20<br>
	 * The return value has the same type as the first argument (assuming that it is integer, double, or decimal). 
	 * This means that for an integer argument, the result is an integer (no decimal places):<br>
	 * <br>
	 * mysql> SELECT ROUND(150.000,2), ROUND(150,2);<br>
	 * 			+------------------+--------------+<br>
	 * 			| ROUND(150.000,2) | ROUND(150,2) |<br>
	 * 			+------------------+--------------+<br>
	 * 			|           150.00 |          150 |<br>
	 * 			+------------------+--------------+<br>
	 * <br>
	 * ROUND() uses the following rules depending on the type of the first argument:<br>
	 * <br>
	 * For exact-value numbers, ROUND() uses the “round half away from zero” or “round toward nearest” rule: 
	 * A value with a fractional part of .5 or greater is rounded up to the next integer if positive or down to 
	 * the next integer if negative. (In other words, it is rounded away from zero.) A value with a fractional 
	 * part less than .5 is rounded down to the next integer if positive or up to the next integer if negative.<br>
	 * <br>
	 * For approximate-value numbers, the result depends on the C library. 
	 * On many systems, this means that ROUND() uses the "round to nearest even" rule: A value with any fractional 
	 * part is rounded to the nearest even integer.<br>
	 * <br>
	 * The following example shows how rounding differs for exact and approximate values:<br>
	 * <br>
	 * <br>
	 * mysql> SELECT ROUND(2.5), ROUND(25E-1);<br>
	 * 			+------------+--------------+<br>
	 * 			| ROUND(2.5) | ROUND(25E-1) |<br>
	 * 			+------------+--------------+<br>
	 * 			| 3          |            2 |<br>
	 * 			+------------+--------------+<br>
	 * <br>
	 * For more information, see Section 12.21, “Precision Math”.<br>	
	 */
	Round("ROUND", "ROUND(%s, %s)")
	//	SIGN()	Return the sign of the argument
	//	SIN()	Return the sine of the argument
	//	SQRT()	Return the square root of the argument
	//	TAN()	Return the tangent of the argument
	//	TRUNCATE()	Truncate to specified number of decimal places
	;
		
	private String value = null;
	private String format = null;
	
	private static MathematicalFunctions[] VALUES = values();
	
	private MathematicalFunctions(String value, String format) {
		this.value = value;
		this.format = format;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public String getFormat() {
		return this.format;
	}
	
	public static MathematicalFunctions fromValue(String value) {
		if (null == value) {
			return null;
		}
	
		MathematicalFunctions rtn = null;
	
		for (MathematicalFunctions target : VALUES) {
			if (!value.equals(target.getValue())) {
				continue;
			}
	
			rtn = target;
		}
	
		return rtn;
	}
}
