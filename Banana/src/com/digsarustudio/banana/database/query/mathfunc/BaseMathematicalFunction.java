/**
 * 
 */
package com.digsarustudio.banana.database.query.mathfunc;

/**
 * The basement class of {@link MathematicalFunction} to handle the fundamental operations.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public abstract class BaseMathematicalFunction implements MathematicalFunction {

	protected MathematicalFunctions function	= null;
	
	/**
	 * 
	 */
	public BaseMathematicalFunction(MathematicalFunctions function) {
		this.function = function;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.mathfunc.MathematicalFunction#getFunction()
	 */
	@Override
	public MathematicalFunctions getFunction() {
		
		return this.function;
	}

}
