/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * This is the fundamental table object for the {@link SQLStatement}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SimpleQueryTable extends QueryTable implements Table {

	/**
	 * 
	 * The object builder for {@link SimpleQueryTable}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Table.Builder {
		private SimpleQueryTable result = null;

		public Builder() {
			this.result = new SimpleQueryTable();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.Table.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			this.result.setName(name);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.Table.Builder#setSelectStatement(com.digsarustudio.banana.database.query.statement.SQLStatement)
		 */
		@Override
		public Builder setSelectStatement(SQLStatement statement) {
			throw new UnsupportedOperationException("Please use #setName() instead");
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.Table.Builder#setAlias(java.lang.String)
		 */
		@Override
		public Builder setAlias(String alias) {
			this.result.setAlias(alias);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Table build() {
			return this.result;
		}

	}
	
	private String name = null;
	
	/**
	 * 
	 */
	private SimpleQueryTable() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.Table#getName()
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.Table#toSQLString()
	 */
	@Override
	public String toSQLString() {
		return StringFormatter.format("`%s`", this.name);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	protected void setName(String name){
		this.name = name;
	}
}
