/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The comparators, '=', '<', '>', '<=', '>=', '!=', for the {@link Condition}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum Comparators {
	  Equals("=")
	, LessThan("<")
	, GreaterThan(">")
	, LessEquals("<=")
	, GreaterEquals(">=")
	, NotEqual("!=")
	, IsNot("IS NOT")
	, Is("IS")
	;
	
	private String value = null;
	
	private Comparators(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public static Comparators getDefault(){
		return Comparators.Equals;
	}
}
