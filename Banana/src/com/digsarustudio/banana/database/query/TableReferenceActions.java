/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The actions to refer a table.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public enum TableReferenceActions {
	/**
	 * Retrieves or delete data from a table. 
	 */
	 From("FROM")
	 
	 /**
	  * Assigns data into a table.
	  */
	,Into("INTO")
	;
	private String value = null;

	private static TableReferenceActions[] VALUES = values();

	private TableReferenceActions(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static TableReferenceActions fromValue(String value) {
		TableReferenceActions rtn = null;

		for (TableReferenceActions target : VALUES) {
			if (!value.equals(target.getValue())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}
}
