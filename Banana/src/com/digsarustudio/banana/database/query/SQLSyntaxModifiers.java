/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * This enumerations enumerate the modifiers used for {@link ModifierClause}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public enum SQLSyntaxModifiers {
	 LowPriority("LOW_PRIORITY")
	,Delayed("DELAYED")
	,HighPriority("HIGH_PRIORITY")
	,Ignore("IGNORE")
	,Quick("QUICK")
	,All("ALL")
	,Distinguish("DISTINCT")
	,DistinguishRow("DISTINCTROW")
	
	//TODO To add more if it needs
	;
	
	private String value = null;
	private static SQLSyntaxModifiers[] VALUES = values();

	private SQLSyntaxModifiers(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
	
	public static SQLSyntaxModifiers fromValue(String value){
		SQLSyntaxModifiers rtn = null;
		
		for (SQLSyntaxModifiers modifier : VALUES) {
			if( !value.equals(modifier.getValue()) ){
				continue;
			}
			
			rtn = modifier;
		}
		
		return rtn;
	}
}
