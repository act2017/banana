/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The type of join clause for the table
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum TableJoinType {
	 Default("JOIN")
	,InnerJoin("INNER JOIN")
	,CrossJoin("CROSS JOIN")
	,LeftJoin("LEFT JOIN")
	,LeftOuterJoin("LEFT OUTER JOIN")
	,RightJoin("RIGHT JOIN")
	,RightOuterJoin("RIGHT OUTER JOIN")
	,FullOuterJoin("OUTER JOIN")
	;
	
	private String value = null;
	
	private TableJoinType(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
