/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * To indicate the target parameters are not a subquery operator
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class NotSubqueryOperatorException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4225702319306846276L;

	/**
	 * 
	 */
	public NotSubqueryOperatorException() {

	}

	/**
	 * @param message
	 */
	public NotSubqueryOperatorException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public NotSubqueryOperatorException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public NotSubqueryOperatorException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public NotSubqueryOperatorException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
