/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The keywords will be used in the statement
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * 
 * @deprecated	1.0.2 Please use {@link SQLSyntaxModifiers#Distinguish} and {@link SQLKeywords#_Default} instead.
 */
public enum StatementKeywords {
	/**
	 * @deprecated 1.0.2 Please use {@link SQLKeywords#_Default}
	 */
	_Default("DEFAULT")
	
	/**
	 * @deprecated 1.0.2 Please use {@link SQLSyntaxModifiers#Distinguish}
	 */
	,Distinct("DISTINCT")
	;
	
	private String value = null;
	
	private StatementKeywords(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
}
