/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The encryption and compression functions
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @see <a href="https://dev.mysql.com/doc/refman/5.5/en/encryption-functions.html#function_encode">Encryption and Compression Functions</a>
 */
public enum SQLEncryptionFunctions {
//	AES_DECRYPT()	Decrypt using AES
//	AES_ENCRYPT()	Encrypt using AES
//	COMPRESS()	Return result as a binary string
//	DECODE()	Decodes a string encrypted using ENCODE()
//	DES_DECRYPT()	Decrypt a string
//	DES_ENCRYPT()	Encrypt a string
//	ENCODE()	Encode a string
//	ENCRYPT()	Encrypt a string
	
	/**
	 * Calculates an MD5 128-bit checksum for the string. 
	 * The value is returned as a string of 32 hexadecimal digits, or NULL if the argument was NULL. 
	 * The return value can, for example, be used as a hash key. 
	 * See the notes at the beginning of this section about storing hash values efficiently.
	 * As of MySQL 5.5.3, the return value is a nonbinary string in the connection character set. 
	 * Before 5.5.3, the return value is a binary string; 
	 * see the notes at the beginning of this section about using the value as a nonbinary string.
	 * <br>
	 * For example:<br>
	 * 	MD5('testing')
	 * <br>
	 * This is the “RSA Data Security, Inc. MD5 Message-Digest Algorithm.<br>
	 * See the note regarding the MD5 algorithm at the beginning this section.
	 * <br>
	 * Note: Please do not use this function for the encryption of password because of the exploits.
	 * 		 According to the comment on the MySQL website, using SHA2() instead or something else. 
	 */
	MD5("MD5", "MD5(%s)")
	
//	OLD_PASSWORD()	Return the value of the pre-4.1 implementation of PASSWORD
//	PASSWORD()	Calculate and return a password string
//	SHA1(), SHA()	Calculate an SHA-1 160-bit checksum
//	SHA2()	Calculate an SHA-2 checksum
//	UNCOMPRESS()	Uncompress a string compressed
//	UNCOMPRESSED_LENGTH()	Return the length of a string before compression
	;
	
	private String value	= null;
	private String format	= null;
	
	private SQLEncryptionFunctions(String value, String format){
		this.value = value;
		this.format = format;
	}	
	
	public String getValue(){
		return this.value;
	}
	
	public String getFormat(){
		return this.format;
	}
}
