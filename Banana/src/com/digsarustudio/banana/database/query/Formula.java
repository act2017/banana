/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.query.aggregation.AggregationFunction;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * This class used to add, subtract, multiple, or divide a value from a base column
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class Formula {
	/**
	 * 
	 * The object builder for {@link Formula}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<Formula> {
		private Formula result = null;

		public Builder() {
			this.result = new Formula();
		}

		/**
		 * @param target
		 * @see com.digsarustudio.banana.database.query.Formula#setTarget(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		public Builder setTarget(SimpleTableColumn target) {
			result.setTarget(target);
			return this;
		}

		/**
		 * @param fomula
		 * @see com.digsarustudio.banana.database.query.Formula#setSubFormula(com.digsarustudio.banana.database.query.Formula)
		 */
		public Builder setSubFormula(Formula fomula) {
			result.setSubFormula(fomula);
			return this;
		}

		/**
		 * @param function
		 * @see com.digsarustudio.banana.database.query.Formula#setAggregationFunction(com.digsarustudio.banana.database.query.aggregation.AggregationFunction)
		 */
		public Builder setAggregationFunction(AggregationFunction function) {
			result.setAggregationFunction(function);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#add(java.lang.String)
		 */
		public Builder add(String value) {
			result.add(value);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#add(java.lang.Integer)
		 */
		public Builder add(Integer value) {
			result.add(value);
			return this;
		}
		
		public Builder add(SimpleTableColumn column) {
			this.result.add(column);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#subtract(java.lang.String)
		 */
		public Builder subtract(String value) {
			result.subtract(value);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#subtract(java.lang.Integer)
		 */
		public Builder subtract(Integer value) {
			result.subtract(value);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#multiple(java.lang.String)
		 */
		public Builder multiple(String value) {
			result.multiple(value);
			return this;
		}
		
		public Builder multiple(SimpleTableColumn column) {
			this.result.multiple(column);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#multiple(java.lang.Integer)
		 */
		public Builder multiple(Integer value) {
			result.multiple(value);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#divide(java.lang.String)
		 */
		public Builder divide(String value) {
			result.divide(value);
			return this;
		}

		/**
		 * @param value
		 * @see com.digsarustudio.banana.database.query.Formula#divide(java.lang.Integer)
		 */
		public Builder divide(Integer value) {
			result.divide(value);
			return this;
		}

		/**
		 * @param baseNumber
		 * @see com.digsarustudio.banana.database.query.Formula#setBaseNumber(java.lang.String)
		 */
		public Builder setBaseNumber(String baseNumber) {
			result.setBaseNumber(baseNumber);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Formula build() {
			return this.result;
		}

	}
	
	private final String ADD		= "+";
	private final String SUBTRACT	= "-";
	private final String MULTIPLE	= "*";
	private final String DIVIDE		= "/";

	
	private String 				formula 	= "";
	private SimpleTableColumn	target	= null;
	private String				baseNumber	= null;
	
	private Formula				subFormula	= null;
	
	private AggregationFunction	aggregationFunction	= null;
	
	/**
	 * 
	 */
	private Formula() {
		
	}

	/**
	 * @return the target
	 */
	public SimpleTableColumn getTarget() {
		return target;
	}

	/**
	 * @return the baseNumber
	 */
	public String getBaseNumber() {
		return baseNumber;
	}

	/**
	 * @param baseNumber the baseNumber to set
	 */
	public void setBaseNumber(String baseNumber) {
		this.baseNumber = baseNumber;
	}

	/**
	 * @param target the target to set
	 */
	public void setTarget(SimpleTableColumn target) {
		this.target = target;
	}
	
	public void setSubFormula(Formula fomula) {
		this.subFormula = fomula;
	}
	
	public void setAggregationFunction(AggregationFunction function) {
		this.aggregationFunction = function;
	}
	
	public void add(String value) {
		this.formula += ADD + value;
	}
	
	public void add(Integer value) {
		this.add(value.toString());
	}
	
	public void add(SimpleTableColumn column) {
		this.add(column.getTableColumnName());
	}
	
	public void subtract(String value) {
		this.formula += SUBTRACT + value;
	}
	
	public void subtract(Integer value) {
		this.subtract(value.toString());
	}
		
	public void multiple(Integer value) {
		this.multiple(value.toString());
	}
	
	public void multiple(String value) {	
		this.formula += MULTIPLE + value;
	}
	
	public void multiple(SimpleTableColumn column) {
		this.multiple(column.getTableColumnName());
	}
	
	public void divide(String value) {
		this.formula += DIVIDE + value;
	}
	
	public void divide(Integer value) {
		this.divide(value.toString());
	}

	public String toSQLString() {
		StringBuffer buffer = new StringBuffer();
		
		if(null != this.aggregationFunction) {
			buffer.append("(");
			buffer.append(this.aggregationFunction.toSQLString());
			buffer.append(")");
		}else if(null != this.subFormula) {
			buffer.append("(");
			buffer.append(this.subFormula.toSQLString());
			buffer.append(")");
		}else if(null != this.target) {
			buffer.append(this.target.getQuerySimpleTableColumn());
		}else if(null != this.baseNumber) {
			buffer.append(this.baseNumber);
		}else {
			//Let compiler design which value to be considered with this formula
			return "%s";
		}
		
		if(null != this.formula && !this.formula.isEmpty()) {
			buffer.append(this.formula);
		}
				
		return buffer.toString();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
