/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The base class of {@link Table}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class QueryTable implements Table {
	private String	alias	 = null;
	
	/**
	 * 
	 */
	public QueryTable() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.Table#getAlias()
	 */
	@Override
	public String getAlias() {
		return this.alias;
	}

	/**
	 * Assigns an alias for this table
	 * 
	 * @param alias An alias for this table
	 */
	protected void setAlias(String alias){
		this.alias = alias;
	}
}
