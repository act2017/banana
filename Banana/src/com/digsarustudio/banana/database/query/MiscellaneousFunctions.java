/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The miscellaneous functions of database query string.<br>
 * <br>
 * Reference:<br>
 * 	1.https://dev.mysql.com/doc/refman/5.7/en/miscellaneous-functions.html#function_values<br>
 *  
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.12
 *
 */
public enum MiscellaneousFunctions {
	/**
	 * To generate a 128-bits number represented as a utf8 string of five hexadecimal numbers 
	 * in aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee format. If this UUID is going to be stored in the database, 
	 * please remove the hyphen(-) from the UUID string.<br>
	 * <br>
	 * For example:<br>
	 * SELECT UUID(); -> 6ccd780c-baba-1026-9564-5b8c656024db<br>
	 * 
	 */
	 UUID("UUID", "UUID()")
	 
	 /**
	  * To generate a 64-bit unsigned integer.<br>
	  * <br>
	  * For example:<br>
	  * SELECT UUID_SHORT(); -> 92395783831158784<br>
	  */
	,ShortUUID("UUID_SHORT", "UUID_SHORT()")
	
	/**
	 * To get the value of a column would be inserted in an INSERT...ON DUPLICATE KEY UPDATE statement. 
	 * It will return NULL if the column is not in an INSER...ON DUPLICATE KEY UPDATE statement. 
	 * The parameter is the name of the column would be inserted.<br>
	 * <br>
	 * For example:<br>
	 * 	INSERT INTO table (a,b,c) VALUES (1,2,3),(4,5,6) ON DUPLICATE KEY UPDATE c=VALUES(a)+VALUES(b);
	 */
	,Values("VALUES", "VALUES(%s)")
	;
	
	
	private String value	= null;
	private String format	= null;

	static MiscellaneousFunctions[] VALUES = values();

	private MiscellaneousFunctions(String value, String format) {
		this.value = value;
		this.format = format;
	}

	public String getValue() {
		return this.value;
	}
	
	public String getFormat(){
		return this.format;
	}
	
	public static MiscellaneousFunctions fromValue(String value){
		MiscellaneousFunctions rtn = null;
		
		for (MiscellaneousFunctions target : VALUES) {
			if( !value.equals(target.getValue()) ){
				continue;
			}
			
			rtn = target;
		}
		
		return rtn;
	}
}
