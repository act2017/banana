/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;

/**
 * The date and time functions.<br>
 * Please refactor the instance of {@link ClauseCompiler} to support the new date and time function for a particular database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum SQLDateAndTimeFunctions {
	//These 2 should be implemented as an object to assing unit, interval, and the base.
	TimestampAdd("TIMESTAMPADD", "TIMESTAMPADD(%s, %s, %s)")
	,TimestampDiff("TIMESTAMPDIFF", "TIMESTAMPDIFF(%s, %s, %s)")
	
	/**
	 * Convert the date to unix timestamp in digits. e.g. UNIX_TIMESTAMP('2008/08/22-12:02:33') => 1219406553
	 */
	,UNIXTimestamp("UNIX_TIMESTAMP", "UNIX_TIMESTAMP(%s)")
	
	/**
	 * Converts the UNIX timestamp to date. e.g. FROM_UNIXTIME('1219406553') => 2008/08/22-12:02:33
	 */
	,FromUNIXTime("FROM_UNIXTIME", "FROM_UNIXTIME(%s)")
	
	,Now("NOW", "NOW()")
	
	/**
	 * Converts the UNIX timestamp or date to database timestamp. e.g. TIMESTAMP('1219406553') => 2008/08/22-12:02:33
	 * 																,  TIMESTAMP('2008/08/22') => 2008/08/22-00:00:00
	 */
	,Timestamp("TIMESTAMP", "TIMESTAMP(%s)")
	
	/**
	 * STR_TO_DATE(str,format)<br>
	 * <br>
	 * This is the inverse of the DATE_FORMAT() function. It takes a string str and a format string format. STR_TO_DATE() returns a DATETIME value 
	 * if the format string contains both date and time parts, or a DATE or TIME value if the string contains only date or time parts. If the date, time, 
	 * or datetime value extracted from str is illegal, STR_TO_DATE() returns NULL and produces a warning.<br>
	 * <br>
	 * The server scans str attempting to match format to it. The format string can contain literal characters and format specifiers beginning with %. 
	 * Literal characters in format must match literally in str. Format specifiers in format must match a date or time part in str. 
	 * For the specifiers that can be used in format, see the DATE_FORMAT() function description.<br>
	 * <br>
	 * 	mysql> SELECT STR_TO_DATE('01,5,2013','%d,%m,%Y');<br>
	 *     	-> '2013-05-01'<br>
	 *  mysql> SELECT STR_TO_DATE('May 1, 2013','%M %d,%Y');<br>
	 *  	-> '2013-05-01'<br>
	 *  <br>
	 *  Scanning starts at the beginning of str and fails if format is found not to match. Extra characters at the end of str are ignored.<br>
	 *  <br>
	 *  mysql> SELECT STR_TO_DATE('a09:30:17','a%h:%i:%s');<br>
	 *  	-> '09:30:17'<br>
	 *  mysql> SELECT STR_TO_DATE('a09:30:17','%h:%i:%s');<br>
	 *  	-> NULL<br>
	 *  mysql> SELECT STR_TO_DATE('09:30:17a','%h:%i:%s');<br>
	 *  	-> '09:30:17'<br>
	 *  <br>
	 *  Unspecified date or time parts have a value of 0, so incompletely specified values in str produce a result with some or all parts set to 0:<br>
	 *  <br>
	 *  mysql> SELECT STR_TO_DATE('abc','abc');<br>
	 *  	-> '0000-00-00'<br>
	 *  mysql> SELECT STR_TO_DATE('9','%m');<br>
	 *  	-> '0000-09-00'<br>
	 *  mysql> SELECT STR_TO_DATE('9','%s');<br>
	 *  -> '00:00:09'<br>
	 *  <br>
	 *  Range checking on the parts of date values is as described in Section 11.3.1, �The DATE, DATETIME, and TIMESTAMP Types�. This means, for example, 
	 *  that �zero� dates or dates with part values of 0 are permitted unless the SQL mode is set to disallow such values.<br>
	 *  <br>
	 *  mysql> SELECT STR_TO_DATE('00/00/0000', '%m/%d/%Y');<br>
	 *  	-> '0000-00-00'<br>
	 *  mysql> SELECT STR_TO_DATE('04/31/2004', '%m/%d/%Y');<br>
	 *  	-> '2004-04-31'<br>
	 *  If the NO_ZERO_DATE or NO_ZERO_IN_DATE SQL mode is enabled, zero dates or part of dates are disallowed. In that case, STR_TO_DATE() returns NULL and 
	 *  generates a warning:<br>
	 *  <br>
	 *  mysql> SET sql_mode = '';<br>
	 *  mysql> SELECT STR_TO_DATE('15:35:00', '%H:%i:%s');<br>
	 *  <br>
	 *  +-------------------------------------+<br>
	 *  | STR_TO_DATE('15:35:00', '%H:%i:%s') |<br>
	 *  +-------------------------------------+<br>
	 *  | 15:35:00                            |<br>
	 *  +-------------------------------------+<br>
	 *  mysql> SET sql_mode = 'NO_ZERO_IN_DATE';<br>
	 *  mysql> SELECT STR_TO_DATE('15:35:00', '%h:%i:%s');<br>
	 *  <br>
	 *  +-------------------------------------+<br>
	 *  | STR_TO_DATE('15:35:00', '%h:%i:%s') |<br>
	 *  +-------------------------------------+<br>
	 *  | NULL                                |<br>
	 *  +-------------------------------------+<br>
	 *  <br>
	 *  mysql> SHOW WARNINGS\G<br>
	 *  ************************* 1. row ***************************<br>
	 *  Level: Warning<br>
	 *  Code: 1411<br>
	 *  Message: Incorrect datetime value: '15:35:00' for function str_to_date<br>
	 *  Note<br>
	 *  	You cannot use format "%X%V" to convert a year-week string to a date because the combination of a year and week does not uniquely identify a year 
	 *  and month if the week crosses a month boundary. To convert a year-week to a date, you should also specify the weekday:<br>
	 *  mysql> SELECT STR_TO_DATE('200442 Monday', '%X%V %W');<br>
	 *  	-> '2004-10-18'<br>
	 */
	,Str2Date("STR_TO_DATE", "STR_TO_DATE(%s, %s)")
	;
	
	private String name		= null;
	private String format	= null;
	
	private SQLDateAndTimeFunctions(String name, String format) {
		this.name = name;
		this.format = format;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getFormat() {
		return this.format;
	}
}
