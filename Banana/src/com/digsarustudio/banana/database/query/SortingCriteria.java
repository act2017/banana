/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * This class defines which column has to be sorted within ascending or descending.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * @deprecated	1.0.2 Please use {@link Ordering} instead.
 */
public class SortingCriteria {
	/**
	 * 
	 * The object builder for {@link SortingCriteria}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<SortingCriteria> {
		private String		column	= null;
		private SortingType	type	= SortingType.getDefault();

		/**
		 * Returns the name of the column to be sorted
		 * 
		 * @return  the name of the column to be sorted
		 */
		private String getColumn() {
			return column;
		}

		/**
		 * Assigns a name of the column to be sorted
		 * 
		 * @param column the column to set
		 */
		public Builder setColumn(String column) {
			this.column = column;
			return this;
		}

		/**
		 * Returns the sorting type
		 * 
		 * @return the type of sorting
		 */
		private SortingType getType() {
			return type;
		}

		/**
		 * Assigns how to sort the target column
		 * 
		 * @param type the type to set
		 */
		public Builder setType(SortingType type) {
			this.type = type;
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SortingCriteria build() {
			return new SortingCriteria(this);
		}

	}
	private String		column	= null;
	private SortingType	type	= SortingType.getDefault();
	
	/**
	 * 
	 */
	private SortingCriteria(Builder builder) {
		this.setColumn(builder.getColumn());
		this.setType(builder.getType());
	}

	/**
	 * Returns the name of the column to be sorted
	 * 
	 * @return  the name of the column to be sorted
	 */
	public String getColumn() {
		return column;
	}

	/**
	 * Assigns a name of the column to be sorted
	 * 
	 * @param column the column to set
	 */
	public void setColumn(String column) {
		this.column = column;
	}

	/**
	 * Returns the sorting type
	 * 
	 * @return the type of sorting
	 */
	public SortingType getType() {
		return type;
	}

	/**
	 * Assigns how to sort the target column
	 * 
	 * @param type the type to set
	 */
	public void setType(SortingType type) {
		this.type = type;
	}
	
	/**
	 * Returns the query string of sorting criteria
	 * 
	 * @return the query string of sorting criteria
	 */
	public String getQuerySortingCriteria(){
		StringBuilder builder = new StringBuilder();
		builder.append(StringFormatter.format("`%s`", this.column));
		builder.append(" ");
		builder.append(this.type.getValue());
		
		return builder.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
