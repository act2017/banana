/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The keywords used in SQL string
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public enum SQLKeywords {
	_Default("DEFAULT")
	;
	private String value = null;

	static private SQLKeywords[] VALUES = values();

	private SQLKeywords(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static SQLKeywords fromValue(String value) {
		SQLKeywords rtn = null;

		for (SQLKeywords target : VALUES) {
			if (!value.equals(target.getValue())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}
}
