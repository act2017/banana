/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy;
import com.digsarustudio.banana.database.query.time.DateTime;
import com.digsarustudio.banana.database.table.DefaultValues;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * To describe a data with its name used in the database
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class Dataset {
	/**
	 * 
	 * The object builder for {@link Dataset}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements ObjectBuilder<Dataset> {
		
		private Dataset	result = new Dataset();
		
		/**
		 * Assigns a column in a particular table this data applied for.<br>
		 * The table attribute can be omitted if the table name is as same as the one which insert or update syntax used.<br>
		 * 
		 * @param column The column this data applied for
		 */
		public Builder setTableColumn(SimpleTableColumn column) {
			this.result.setTableColumn(column);
			return this;
		}

		/**
		 * Assigns the data
		 * 
		 * @param value the value to set
		 */
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}
		
		public Builder setValue(Integer value) {
			this.result.setValue(value.toString());
			return this;
		}

		/**
		 * Assign a query function for the value
		 * 
		 * @return The instance of the query function
		 * 
		 * @deprecated 1.0.2 Please use {@link #setStringFunction(SQLStringFunctions)} instead.
		 */
		public Builder setQueryFunction(SQLStringFunctions function) {
			this.result.setQueryFunction(function);
			return this;
		}
		
		/**
		 * Assigns a string function for the value
		 * 
		 * @param function The string function applies on the value
		 * 
		 * @return The instance of this builder
		 */
		public Builder setStringFunction(SQLStringFunctions function){
			this.result.setStringFunction(function);
			return this;
		}
		
		/**
		 * Assigns an encryption function for the value of this dataset
		 * 
		 * @param function The encryption function
		 * 
		 * @return The instance of this builder
		 */
		public Builder setEncryptionFunction(SQLEncryptionFunctions function){
			this.result.setEncryptionFunction(function);
			return this;
		}

		/**
		 * @param miscellaneousFunction
		 * @see com.digsarustudio.banana.database.query.Dataset#setMiscellaneousFunction(com.digsarustudio.banana.database.query.MiscellaneousFunctions)
		 */
		public Builder setMiscellaneousFunction(MiscellaneousFunctions miscellaneousFunction) {
			result.setMiscellaneousFunction(miscellaneousFunction);
			return this;
		}

		/**
		 * @param dateTime
		 * @see com.digsarustudio.banana.database.query.Dataset#setDateTime(com.digsarustudio.banana.database.query.time.DateTime)
		 */
		public Builder setDateTime(DateTime dateTime) {
			result.setDateTime(dateTime);
			return this;
		}

		/**
		 * @param formula
		 * @see com.digsarustudio.banana.database.query.Dataset#setFormula(com.digsarustudio.banana.database.query.Formula)
		 */
		public Builder setFormula(Formula formula) {
			result.setFormula(formula);
			return this;
		}

		/**
		 * @param customizedFunction
		 * @see com.digsarustudio.banana.database.query.Dataset#setCustomizedFunction(java.lang.String)
		 */
		public Builder setCustomizedFunction(String customizedFunction) {
			result.setCustomizedFunction(customizedFunction);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Dataset build() {
			return this.result;
		}
	}
	
	private SimpleTableColumn	tableColumn			= null;
	private String				value				= null;
	/**
	 * @deprecated 1.0.2 Please use {@link #stringFunction} instead
	 */
	private SQLStringFunctions		queryFunction 			= null;
	private SQLStringFunctions		stringFunction			= null;
	private SQLEncryptionFunctions	encryptionFunction		= null;
	private MiscellaneousFunctions	miscellaneousFunction	= null;
	private DateTime				dateTime				= null;
	private Formula					formula					= null;
	
	private String					customizedFunction				= null;
	
	/**
	 * 
	 */
	private Dataset() {		
	}

	/**
	 * Returns the table column this data applied to
	 * 
	 * @return the table column this data applied to
	 */
	public SimpleTableColumn getTableColumn() {
		return this.tableColumn;
	}

	/**
	 * Assigns a table column this data applied for.<br>
	 * The table attribute can be omitted if the table name is as same as the one which insert or update syntax used.<br>
	 * 
	 * @param column The table column this data applied for
	 */
	public void setTableColumn(SimpleTableColumn column) {
		this.tableColumn = column;
	}

	/**
	 * Returns the data
	 * 
	 * @return the data
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Assigns a data
	 * 
	 * @param value the data
	 */
	public void setValue(String value) {
		if( null == value ) {		
			this.value = null;
		}else if( value.isEmpty() ) {
			this.value = DefaultValues.Null.getValue();
		}else{
			value = value.replaceAll("'", "\\\\'");
			switch (DefaultValues.fromValue(value)) {
				case False:
				case True:
				case CurrentTimestamp:
				case isNotNull:
				case isNull:
					this.value = value;
					break;
				case Null:
					this.value = null;
					break;
				default:
					this.value = String.format("'%s'", value);
					break;
			}			
		}
	}
	
	/**
	 * Returns the string function
	 * 
	 * @return the string function
	 */
	public SQLStringFunctions getStringFunction(){
		return this.stringFunction;
	}
	
	/**
	 * Assigns a string function for the value
	 * 
	 * @param function The string function applies on the value
	 * 
	 */
	public void setStringFunction(SQLStringFunctions function){
		this.stringFunction = function;
	}	
	
	/**
	 * @return the miscellaneousFunction
	 */
	public MiscellaneousFunctions getMiscellaneousFunction() {
		return miscellaneousFunction;
	}

	/**
	 * @param miscellaneousFunction the miscellaneousFunction to set
	 */
	public void setMiscellaneousFunction(MiscellaneousFunctions miscellaneousFunction) {
		this.miscellaneousFunction = miscellaneousFunction;
	}
	
	public Boolean hasMiscellaneousFunction() {
		return (null != this.miscellaneousFunction);
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * @return the dateTime
	 */
	public DateTime getDateTime() {
		return dateTime;
	}
	
	public Boolean hasDateTime() {
		return (null != this.dateTime);
	}

	/**
	 * @return the formula
	 */
	public Formula getFormula() {
		return formula;
	}

	/**
	 * @param formula the formula to set
	 */
	public void setFormula(Formula formula) {
		this.formula = formula;
	}

	public Boolean hasFormula() {
		return (null != this.formula);
	}
	
	/**
	 * Returns the query function for the value
	 * 
	 * @return the query function for the value
	 * 
	 * @deprecated 1.0.2 Please use {@link #getStringFunction()} instead.
	 */
	public SQLStringFunctions getQueryFunction() {
		return queryFunction;
	}	

	/**
	 * Assigns a query function for the value
	 * 
	 * @param queryFunction the queryFunction to set
	 * 
	 * @deprecated 1.0.2 Please use {@link #setStringFunction(SQLStringFunctions)} instead
	 */
	public void setQueryFunction(SQLStringFunctions queryFunction) {
		this.queryFunction = queryFunction;
	}
	
	/**
	 * Returns a string combined by query function and value.
	 * Normally, this function called by INSERT statement
	 * 
	 * For example:
	 * 	If the query function is not set:
	 * 		'123'
	 * 
	 * 	If the query function is set:
	 * 		UNHEX('123adc213')
	 * 		NOW()
	 * 
	 * @return a string combined by query function and value
	 * 
	 * @deprecated	1.0.2 {@link DataManipulationStrategy} doesn't need this function.
	 */
	public String getQueryValue(){
		if( null == this.value ){
			return this.queryFunction.getFormat();
		}
		
		String sqlValue = StringFormatter.format("'%s'", this.value);
		if( null != this.encryptionFunction ){
			sqlValue = StringFormatter.format(this.encryptionFunction.getFormat(), sqlValue);
		}
		
		if( null != this.queryFunction ){
			sqlValue = StringFormatter.format(this.queryFunction.getFormat(), sqlValue);
		}
		
		return sqlValue;
	}
	
	/**
	 * Returns a string combined by name, function, and value
	 * Normally, this called by UPDATE statement
	 * 
	 * For example:
	 * 	`id`=UNHEX('1234df332abc') or
	 * 	`id`='1'
	 * 	`date`=NOW()
	 * 
	 * @return a string combined by name, function, and value
	 * @deprecated	1.0.2 {@link DataManipulationStrategy} doesn't need this function.
	 */
	public String getQueryDataSet(){
		//Because the query value might be a functioned value or encrypted value, leave the single quote for 
		//the getQueryValue() to deal with.
		return StringFormatter.format("%s=%s", this.tableColumn.getQuerySimpleTableColumn(), this.getQueryValue());
	}
	
	/**
	 * Returns true if this value is operated by a function, otherwise false returned
	 * 
	 * @return true if this value is operated by a function, otherwise false returned
	 * 
	 * @deprecated Please use {@link #hasFunctions()} instead
	 */
	public Boolean isFunctionOperated(){
		return (null != this.queryFunction || null != this.stringFunction || null != this.encryptionFunction || null != this.miscellaneousFunction || null != this.dateTime);
	}
	
	/**
	 * Assigns an encryption function for the value
	 * 
	 * @param function an encryption function for the value
	 */
	public void setEncryptionFunction(SQLEncryptionFunctions function){
		this.encryptionFunction = function;
	}
	
	/**
	 * Returns the encryption function used on value
	 * 
	 * @return the encryption function used on value
	 */
	public SQLEncryptionFunctions getEncryptionFunction(){
		return this.encryptionFunction;
	}
	
	public Boolean hasFunctions() {
		return (null != this.stringFunction || null != this.encryptionFunction
			 || null != this.miscellaneousFunction || null != this.dateTime 
			 || null != this.formula || null != this.customizedFunction);
	}
	
	/**
	 * Returns true if this dataset contains valid data or format, false returned if the contents are invalid.<br>
	 * 
	 * @return true if this dataset contains valid data or format, false returned if the contents are invalid.
	 */
	public Boolean validate() {
		Boolean hasValue = (null != this.getValue());
		Boolean hasFuncs	= this.hasFunctions();
		
		return (hasValue || hasFuncs);
	}
	
	/**
	 * @return the customizedFunction
	 */
	public String getCustomizedFunction() {
		return customizedFunction;
	}

	/**
	 * @param customizedFunction the customizedFunction to set
	 */
	public void setCustomizedFunction(String customizedFunction) {
		this.customizedFunction = customizedFunction;
	}

	public Boolean hasCustomizedFunction() {
		return (null != this.customizedFunction);
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
