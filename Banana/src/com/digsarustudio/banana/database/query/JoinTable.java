/**
 * 
 */
package com.digsarustudio.banana.database.query;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The details of table to the join
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class JoinTable {
	/**
	 * 
	 * The object builder for {@link JoinTable}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements ObjectBuilder<JoinTable> {		
		private JoinTable result = null;
		
		public Builder() {
			this.result = new JoinTable();
		}

		/**
		 * Assigns a table to join
		 * 
		 * @param table the table to set
		 */
		public Builder setTable(Table table) {
			this.result.setTable(table);
			return this;
		}

		/**
		 * Assigns a type of join for the target table
		 * 
		 * @param type the type to set
		 */
		public Builder setType(TableJoinType type) {
			this.result.setType(type);
			return this;
		}

		/**
		 * Assigns an alias for this table to join
		 * 
		 * @param alias the alias to set
		 */
		public Builder setAlias(String alias) {
			this.result.setAlias(alias);
			return this;
		}

		/**
		 * Assigns a condition for the join
		 * 
		 * @param condition the conditions to set
		 * 
		 */
		public Builder addCondition(JoinCondition condition) {
			this.result.addCondition(condition);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public JoinTable build() {
			return this.result;
		}

	}
	
	private Table table = null;
	private String alias = null;
	private TableJoinType type = TableJoinType.Default;
	
	/**
	 * To be used in [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition
	 * 				 [LEFT|RIGHT] [OUTER] JOIN table_refenrece ON join_condition form.
	 */
	private List<JoinCondition> conditions = null;
	
	/**
	 * 
	 */
	private JoinTable() {
	}
	
	/**
	 * Returns the table to join
	 * 
	 * @return the table to join
	 */
	public Table getTable() {
		return table;
	}

	/**
	 * Assigns a table to join
	 * 
	 * @param table the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}

	/**
	 * Returns the type of join for the target table
	 * 
	 * @return the type of join for the target table
	 */
	public TableJoinType getType() {
		return type;
	}

	/**
	 * Assigns a type of join for the target table
	 * 
	 * @param type the type to set
	 */
	public void setType(TableJoinType type) {
		this.type = type;
	}

	/**
	 * Returns the conditions for the join
	 * 
	 * @return the conditions for the join
	 * 
	 */
	public List<JoinCondition> getConditions() {
		return conditions;
	}

	/**
	 * Assigns a list of conditions for the join
	 * 
	 * @param conditions the conditions to set
	 * 
	 */
	public void setConditions(List<JoinCondition> conditions) {
		this.conditions = conditions;
	}

	/**
	 * Assigns a condition for the join
	 * 
	 * @param condition the conditions to set
	 * 
	 */
	public void addCondition(JoinCondition condition) {
		if( null == this.conditions ){
			this.conditions = new ArrayList<>();
		}
		
		this.conditions.add(condition);
	}
	
	/**
	 * Returns the alias of this table for the join
	 * 
	 * @return the alias of this table for the join
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Assigns an alias for this table to join.
	 * 
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}

}
