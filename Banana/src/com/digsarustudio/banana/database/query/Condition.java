/**
 * 
 */
package com.digsarustudio.banana.database.query;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.time.DateTime;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The condition for the WHERE Clause in SELECT, UPDATE, and DELETE
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @since		1.0.2
 * @version		1.0.3
 * <br>
 * Note:<br>
 * 				1.0.3	-> Bug Fixing: Crash when set null value to Condition.<br>
 */
@SuppressWarnings("deprecation")
public class Condition {
	/**
	 * 
	 * The object builder for {@link Condition}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements ObjectBuilder<Condition> {
		private Condition result = new Condition();
		
		/**
		 * Assigns a table column for this condition to be validated.<br>
		 * 
		 * @param column The table column for this condition to be validated.<br>
		 */
		public Builder setTableColumn(SimpleTableColumn column) {
			this.result.setTableColumn(column);			
			return this;
		}

		/**
		 * Assigns the value to compare with target
		 * 
		 * @param value the value to set
		 */
		public Builder setValue(String value) {
			this.result.setValue(value);
			return this;
		}

		/**
		 * Assigns the comparator to compare with target
		 * 
		 * @param comparator the comparator to set
		 */
		public Builder setComparator(Comparators comparator) {
			this.result.setComparator(comparator);
			return this;
		}

		/**
		 * Assigns the operation applied on this condition
		 * @param operation the operation to set
		 */
		public Builder setOperation(Operations operation) {
			this.result.setOperation(operation);
			return this;
		}

		/**
		 * Set a sub query as the condition.
		 * Please make sure the name of columns in the sub query are consistent with the where condition
		 * 
		 * @param statement The sub query
		 * 
		 * @return The instance of this builder
		 */
		public Builder setSubQuery(SQLStatement statement){
			this.result.setSubQuery(statement);
			return this;
		}
		
		/**
		 * Assigns an operator for the sub query.
		 * 
		 * This must be the one of {@link Operations#All}, {@link Operations#Some}, {@link Operations#Any},
		 * {@link Operations#Including}, {@link Operations#Excluding}, {@link Operations#Exists}, or
		 * {@link Operations#NotExists}
		 *  
		 * @param operator The operator for the sub query
		 * 
		 * @return The instance of this builder
		 */
		public Builder setSubQueryOperator(Operations operator){
			if( !Operations.isSubqueryOperator(operator) ){
				throw new NotSubqueryOperatorException();
			}
			
			this.result.setSubqueryOperator(operator);
			return this;
		}
		
		/**
		 * Assign a query function for the value
		 * 
		 * @param function the query function for the value
		 * 
		 * @return The instance of the query function
		 * 
		 * @deprecated 1.0.2 Please use {@link #setStringFunction(SQLStringFunctions)} instead.
		 */
		public Builder setQueryFunction(SQLStringFunctions function) {
			this.result.setQueryFunction(function);
			return this;
		}
		
		/**
		 * Assign a string function for the value
		 * 
		 * @param function the string function for the value
		 * 
		 * @return The instance of the query function
		 */
		public Builder setStringFunction(SQLStringFunctions function) {
			this.result.setStringFunction(function);
			return this;
		}		
		
		/**
		 * Assign an aggregate function
		 * 
		 * @param function  an aggregate function
		 * 
		 * @return The instance of this builder
		 */
		public Builder setAggregateFunction(SQLAggregateFunctions function){
			this.result.setAggregateFunction(function);
			return this;
		}
		
		/**
		 * Set the target value should be a null to match this condition
		 * 
		 * @return The instance of this builder
		 */
		public Builder setIsNullValue(){
			this.result.setIsNullValue();
			return this;
		}
		
		/**
		 * Set the target value should not be a null to match this condition
		 * 
		 * @return The instance of this builder
		 */
		public Builder setIsNotNullValue(){
			this.result.setIsNotNullValue();
			return this;
		}
		
		/**
		 * Assigns an encryption function for the value of this dataset
		 * 
		 * @param function The encryption function
		 * 
		 * @return The instance of this builder
		 */
		public Builder setEncryptFunction(SQLEncryptionFunctions function){
			this.result.setEncryptionFunction(function);
			return this;
		}

		/**
		 * @param miscellaneousFunction
		 * @see com.digsarustudio.banana.database.query.Condition#setMiscellaneousFunction(com.digsarustudio.banana.database.query.MiscellaneousFunctions)
		 */
		public Builder setMiscellaneousFunction(MiscellaneousFunctions miscellaneousFunction) {
			result.setMiscellaneousFunction(miscellaneousFunction);
			return this;
		}

		/**
		 * @param dateTime
		 * @see com.digsarustudio.banana.database.query.Condition#setDateTime(com.digsarustudio.banana.database.query.time.DateTime)
		 */
		public Builder setDateTime(DateTime dateTime) {
			result.setDateTime(dateTime);
			return this;
		}

		/**
		 * Append a list of conditions to represent in an enclosed block.<br>
		 * If this method has been called, the {@link #setValue(String)} will be ignore.<br>
		 * 
		 * @param conditions
		 * @see com.digsarustudio.banana.database.query.Condition#setEnclosedConditions(java.util.List)
		 */
		public Builder setEnclosedConditions(List<Condition> conditions) {
			result.setEnclosedConditions(conditions);
			return this;
		}

		/**
		 * Append a condition to represent in an enclosed block.<br>
		 * If this method has been called, the {@link #setValue(String)} will be ignore.<br>
		 * 
		 * @param condition
		 * @see com.digsarustudio.banana.database.query.Condition#addEnclosedCondition(com.digsarustudio.banana.database.query.Condition)
		 */
		public Builder addEnclosedCondition(Condition condition) {
			result.addEnclosedCondition(condition);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Condition build() {
			return this.result;
		}
	}

	private SimpleTableColumn	tableColumn			= null;
	private String				value				= null;
	private Comparators			comparator			= null;
	private Operations			operation			= null;
	private SQLStringFunctions		stringFunction 		= null;
	private SQLAggregateFunctions 	aggregateFunction 	= null;
	private SQLEncryptionFunctions	encryptionFunction 	= null;
	private Boolean				isNullValue			= null;
	
	/**
	 * The operation {@link Operations#Some} and {@link Operations#Any} can cooperate with comparator for the condition.
	 * But {@link Operations#In} only can be used alone.
	 */
	private Operations	subqueryOperator	= null;
	private SQLStatement	subQuery	= null;
	
	private DateTime					dateTime					= null;
	private MiscellaneousFunctions	miscellaneousFunction	= null;
	
	private List<Condition>				enclosedConditions	= null;
	
	/**
	 * 
	 */
	private Condition() {

	}

	/**
	 * Returns the table column this condition used to be validated for. 
	 * 
	 * @return the table column this condition used to be validated for.
	 */
	public SimpleTableColumn getTableColumn() {
		return this.tableColumn;
	}

	/**
	 * Assigns a table column this condition used to be validated for.
	 * 
	 * @param column the table column this condition used to be validated for.
	 */
	public void setTableColumn(SimpleTableColumn column) {
		this.tableColumn = column;
	}

	/**
	 * Returns the value to compare with this condition
	 * 
	 * @return the value to compare with this condition
	 */
	public String getValue() {
		return value;
	}

	/**
	 * Assigns a value to compare with this condition
	 * 
	 * @param value the value to compare with this condition
	 */
	public void setValue(String value) {
		if(null == value) {
			return;			
		}
		
		value = value.replaceAll("'", "\\\\'");
		this.value = String.format("'%s'", value);
	}

	/**
	 * Returns the comparator in this condition
	 * 
	 * @return the comparator in this condition
	 */
	public Comparators getComparator() {
		return comparator;
	}

	/**
	 * Assign a comparator for this condition
	 * 
	 * @param comparator the comparator to set
	 */
	public void setComparator(Comparators comparator) {
		this.comparator = comparator;
	}

	/**
	 * Returns the operation of this condition.
	 * 
	 * It could be {@link Operations#And}, {@link Operations#Or}, {@link Operations#Not} and so on.
	 * 
	 * @return the operation of this condition.
	 */
	public Operations getOperation() {
		return operation;
	}

	/**
	 * Assigns an operation to this condition
	 * 
	 * @param operation the operation to set
	 */
	public void setOperation(Operations operation) {
		this.operation = operation;
	}
	
	/**
	 * Set a sub query as the condition.
	 * Please make sure the name of columns in the sub query are consistent with the where condition
	 * 
	 * @param statement The sub query
	 * 
	 */
	public void setSubQuery(SQLStatement statement){
		this.subQuery = statement;
	}
	
	/**
	 * Returns the sub query
	 *  
	 * @return the sub query
	 */
	public SQLStatement getSubQuery(){
		return this.subQuery;
	}

	/**
	 * Returns the query function for the value
	 * 
	 * @return the query function for the value
	 * 
	 * @deprecated 1.0.2 Please use {@link #getStringFunction()} instead
	 */
	public SQLStringFunctions getQueryFunction() {
		return stringFunction;
	}
	
	/**
	 * Assign a string function for the value
	 * 
	 * @param function the string function for the value
	 * 
	 */
	public void setStringFunction(SQLStringFunctions function) {
		this.stringFunction = function;
	}		


	/**
	 * Returns the string function for the value
	 * 
	 * @return The string function for the value
	 */
	public SQLStringFunctions getStringFunction() {
		return this.stringFunction;
	}		
		
	

	/**
	 * Assigns a query function for the value
	 * 
	 * @param stringFunction the stringFunction to set
	 * 
	 * @deprecated 1.0.2 Please use {@link #setStringFunction(SQLStringFunctions)} instead
	 */
	public void setQueryFunction(SQLStringFunctions stringFunction) {
		this.stringFunction = stringFunction;
	}
	
	/**
	 * @return the aggregateFunction
	 */
	public SQLAggregateFunctions getAggregateFunction() {
		return aggregateFunction;
	}

	/**
	 * @param aggregateFunction the aggregateFunction to set
	 */
	public void setAggregateFunction(SQLAggregateFunctions aggregateFunction) {
		this.aggregateFunction = aggregateFunction;
	}

	/**
	 * @return the miscellaneousFunction
	 */
	public MiscellaneousFunctions getMiscellaneousFunction() {
		return miscellaneousFunction;
	}

	/**
	 * @param miscellaneousFunction the miscellaneousFunction to set
	 */
	public void setMiscellaneousFunction(MiscellaneousFunctions miscellaneousFunction) {
		this.miscellaneousFunction = miscellaneousFunction;
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * @return the dateTime
	 */
	public DateTime getDateTime() {
		return dateTime;
	}
	
	public Boolean hasDateTime() {
		return (null != this.dateTime);
	}
	
	public Boolean hasMiscellaneousFunction() {
		return (null != this.miscellaneousFunction);
	}

	/**
	 * Set the target value should be a null to match this condition
	 * 
	 */
	public void setIsNullValue(){
		this.isNullValue = true;
	}
	
	/**
	 * Set the target value should not be a null to match this condition
	 * 
	 */
	public void setIsNotNullValue(){
		this.isNullValue = false;
	}
	
	/**
	 * Returns true if the target value should be a null to match this condition, 
	 * false if the target value should not be a null to match this condition.<br>
	 * null returned if this condition works with other criteria.
	 *  
	 * @return Returns true if the target value should be a null to match this condition, 
	 * 			false if the target value should not be a null to match this condition.<br>
	 * 			null returned if this condition works with other criteria.
	 */
	public Boolean isValueNullable(){
		return this.isNullValue;
	}

	/**
	 * Returns a string combined by name, function, and value<br>
	 * <br>
	 * For example:<br>
	 * 	OR `id`=UNHEX('1234df332abc')<br>
	 * 	AND `id`='1'<br>
	 * 	AND `date`=NOW()<br>
	 *  `qty`>'15'<br>
	 *  `id` IN (SELECT ...)<br>
	 *  `qty` > ANY (SELECT COUNT(*) ...)<br>
	 *  `id` = SOME (SELECT ...)<br>
	 *  `id` > ALL (SELECT ...)<br>
	 *  `id` = (SELECT `id` FROM ...)<br>
	 *  sum(*) > 10<br>
	 * 
	 * @return a string combined by name, function, and value
	 * 
	 * @deprecated please use {@link ClauseCompiler} instead
	 */
	public String getQueryCondition(){
		StringBuilder builder = new StringBuilder();
					
		//Operation
		if(null != this.operation){
			builder.append(StringFormatter.format("%s " , this.operation.getValue()));
		}		
		
		//name
		if( null != this.aggregateFunction ){
			builder.append(StringFormatter.format(this.aggregateFunction.getFormat(), this.tableColumn.getColumn()));
		}else{
			builder.append(StringFormatter.format("`%s`", this.tableColumn.getColumn()));
		}
				
		if( null != this.isNullValue ){
			builder.append(this.isNullValue ? " IS NULL" : " IS NOT NULL");
		}else{		
			//comparison operator
			if(null != this.comparator){
				builder.append(StringFormatter.format("%s", this.comparator.getValue()));
			}
			
			//subquery or value/function
			if(null == this.subQuery){
				String sqlValue = StringFormatter.format("'%s'", this.value);
				
				if( null != this.encryptionFunction){
					sqlValue = StringFormatter.format(this.encryptionFunction.getFormat(), sqlValue);
				}
				
				if( null != this.stringFunction ){
					sqlValue = this.getQueryValue(this.stringFunction, sqlValue);
				}
				
				builder.append(sqlValue);
			}else{			
				if(null != this.subqueryOperator){
					builder.append(StringFormatter.format(" %s", this.subqueryOperator.getValue()));
				}
				
				builder.append(StringFormatter.format(" (%s)", this.subQuery.toString()));			
			}
		}
		
		return builder.toString();
	}	
	
	/**
	 * Returns true if this value is operated by a function, otherwise false returned
	 * 
	 * @return true if this value is operated by a function, otherwise false returned
	 */
	public Boolean isFunctionOperated(){
		return (null != this.stringFunction);
	}
	
	/**
	 * Returns true if this condition uses aggregate function, otherwise false returned
	 * 
	 * @return true if this condition uses aggregate function, otherwise false returned
	 */
	public Boolean hasAggregateFunction(){
		return SQLAggregateFunctions.isAggregateFunction(this.aggregateFunction);
	}
	
	/**
	 * Assigns an operator for the sub query.
	 * 
	 * This must be the one of {@link Operations#All}, {@link Operations#Some}, {@link Operations#Any},
	 * {@link Operations#Including}, {@link Operations#Excluding}, {@link Operations#Exists}, or
	 * {@link Operations#NotExists}
	 *  
	 * @param operator The operator for the sub query
	 */
	public void setSubqueryOperator(Operations operator){
		if( null != operator && !Operations.isSubqueryOperator(operator) ){
			throw new NotSubqueryOperatorException();
		}
		
		this.subqueryOperator = operator;
	}	
	
	/**
	 * Returns the operator for subquery
	 * 
	 * @return the operator for subquery
	 */
	public Operations getSubqueryOperator(){
		return this.subqueryOperator;
	}
	
	/**
	 * Assigns an encryption function for the value
	 * 
	 * @param function an encryption function for the value
	 */
	public void setEncryptionFunction(SQLEncryptionFunctions function){
		this.encryptionFunction = function;
	}
	
	/**
	 * Returns the encryption function used on value
	 * 
	 * @return the encryption function used on value
	 */
	public SQLEncryptionFunctions getEncryptionFunction(){
		return this.encryptionFunction;
	}
	
	public void setEnclosedConditions(List<Condition> conditions) {
		this.enclosedConditions = conditions;
	}
	
	public void addEnclosedCondition(Condition condition){
		if(null == this.enclosedConditions) {
			this.enclosedConditions = new ArrayList<>();
		}
		
		this.enclosedConditions.add(condition);
	}
	
	public List<Condition> getEnclosedConditions(){
		return this.enclosedConditions;
	}

	public Boolean hasEnclosedCondition() {
		return (null != this.enclosedConditions && !this.enclosedConditions.isEmpty());
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	/**
	 * Returns a string combined by query function and value.
	 * 
	 * For example:
	 * 	If the query function is not set:
	 * 		'123'
	 * 
	 * 	If the query function is set:
	 * 		UNHEX('123adc213')
	 * 		NOW()
	 * 
	 * @param function The function operates the value attribute
	 * @param value the value for the query
	 * 
	 * @deprecated
	 * 
	 * @return a string combined by query function and value
	 */
	private String getQueryValue(SQLStringFunctions function, String value){
		if(null == value && null == function){
			throw new IllegalArgumentException("The query function or value has to be set.");
		}
		
		if( null == value ){
			return function.getFormat();
		}else if(null == function){
			return StringFormatter.format("'%s'", value);
		}
		
		return StringFormatter.format(function.getFormat(), value);
	}
}
