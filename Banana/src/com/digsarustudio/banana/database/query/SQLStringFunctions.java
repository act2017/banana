/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The functions used in query string. 
 * It is string function in mysql manual.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum SQLStringFunctions {
	 Hex("HEX", "HEX(%s)")
	,UnHex("UNHEX", "UNHEX(%s)")
	
	/**
	 * @deprecated Please use {@link SQLDateAndTimeFunctions} instead
	 */
	,Now("NOW", "NOW()")
	;
	
	
	private String name		= null;
	private String format	= null;
	
	private SQLStringFunctions(String name, String format){
		this.name = name;
		this.format = format;
	}
	
	public String getName(){
		return this.name;
	}
	
	public String getFormat(){
		return this.format;
	}
}
