/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The condition for the WHERE Clause in SELECT, UPDATE, and DELETE
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class JoinCondition {
	/**
	 * 
	 * The object builder for {@link JoinCondition}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements ObjectBuilder<JoinCondition> {
		private SimpleTableColumn		referentialColumn	= null;
		private SimpleTableColumn		refereeColumn		= null;
		private Operations	operation	= null;
		
		/**
		 * Assigns the column which is from the join table
		 * 
		 * @param column The column which is from the join table
		 */
		public Builder setReferentialColumn(SimpleTableColumn column) {
			this.referentialColumn = column;
			return this;
		}

		/**
		 * Assigns the column which is from the base table
		 * 
		 * @param column The column which is from the base table
		 */
		public Builder setRefereeColumn(SimpleTableColumn column) {
			this.refereeColumn = column;
			return this;
		}

		/**
		 * Assigns the operation applied on this condition
		 * 
		 * @param operation the operation to set
		 * @deprecated	1.0.2 No longer available
		 */
		public Builder setOperation(Operations operation) {
			this.operation = operation;
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public JoinCondition build() {
			return new JoinCondition(this);
		}

		private SimpleTableColumn getReferentialColumn() {
			return this.referentialColumn;
		}

		private SimpleTableColumn getRefereeColumn() {
			return this.refereeColumn;
		}

		/**
		 * @return the operation
		 */
		private Operations getOperation() {
			return operation;
		}
	}

	private SimpleTableColumn		referentialColumn		= null;
	private SimpleTableColumn		refereeColumn		= null;
	
	/**
	 * The operation {@link Operations#Some} and {@link Operations#Any} can cooperate with comparator for the condition.
	 * But {@link Operations#In} only can be used alone.
	 */
	private Operations	operation	= null;	
	
	/**
	 * 
	 */
	private JoinCondition(Builder builder) {
		this.setReferentialColumn(builder.getReferentialColumn());
		this.setRefereeColumn(builder.getRefereeColumn());
		this.setOperation(builder.getOperation());
	}

	/**
	 * Returns the column which is from join table
	 * 
	 * @return the column which is from join table
	 */
	public SimpleTableColumn getReferentialColumn() {
		return this.referentialColumn;
	}

	/**
	 * Assigns a column which is from join table
	 * 
	 * @param column The column which is from join table
	 */
	public void setReferentialColumn(SimpleTableColumn column) {
		this.referentialColumn = column;
	}

	/**
	 * Returns the column which is from base table
	 * 
	 * @return the column which is from base table
	 */
	public SimpleTableColumn getRefereeColumn() {
		return this.refereeColumn;
	}

	/**
	 * Assigns a column which is from base table
	 * 
	 * @param column The column which is from base table
	 */
	public void setRefereeColumn(SimpleTableColumn column) {
		this.refereeColumn = column;
	}

	/**
	 * Returns the operation of this condition.
	 * 
	 * It could be {@link Operations#And}, {@link Operations#Or}, {@link Operations#Not} and so on.
	 * 
	 * @return the operation of this condition.
	 */
	public Operations getOperation() {
		return operation;
	}

	/**
	 * Assigns an operation to this condition
	 * 
	 * @param operation the operation to set
	 */
	public void setOperation(Operations operation) {
		this.operation = operation;
	}
	
	/**
	 * Returns a string combined by query function and value
	 * 
	 * @return a string combined by query function and value
	 * 
	 */
	public String getQueryJoinCondition(){
		StringBuilder builder = new StringBuilder();
		
		if(null != this.operation){
			builder.append(StringFormatter.format(" %s ",  this.operation.getValue()));
		}
		
		builder.append(StringFormatter.format("%s=%s", this.refereeColumn, this.referentialColumn));
		
		return builder.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
