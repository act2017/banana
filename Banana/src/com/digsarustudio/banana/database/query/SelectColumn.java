/**
 * 
 */
package com.digsarustudio.banana.database.query;

import com.digsarustudio.banana.database.manipulation.strategy.DataManipulationStrategy;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.mathfunc.MathematicalFunction;
import com.digsarustudio.banana.database.query.time.DateTime;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The class stores the name of the column for the SELECT Statement and the
 * alias for the {@link QueryResult}.<br>
 * Only {@link SQLStringFunctions} or {@link SQLAggregateFunctions} can be used at the same time, the {@link SQLStringFunctions} will appear 
 * if both of them are set at the same time.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SelectColumn {
	/**
	 * 
	 * The object builder for {@link SelectColumn}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.2
	 *
	 */
	public static class Builder implements ObjectBuilder<SelectColumn> {
		private SelectColumn			column		= null;
		
		private Builder() {
			this.column = new SelectColumn();
		}

		/**
		 * Assigns a table column for the SELECT Statement
		 * 
		 * @param column the column to set
		 */
		public Builder setTableColumn(SimpleTableColumn column) {
			this.column.setTableColumn(column);
			return this;
		}
		
		/**
		 * Assign a query function for the value
		 * 
		 * @param function the query function for the value
		 * 
		 * @return The instance of the query function
		 * 
		 * @deprecated 1.0.2 Please use {@link #setStringFunction(SQLStringFunctions)} instead
		 */
		public Builder setQueryFunction(SQLStringFunctions function) {
			this.column.setQueryFunction(function);
			return this;
		}
		
		/**
		 * Assign a string function for the value
		 * 
		 * @param function the string function for the value
		 * 
		 * @return The instance of the query function
		 */
		public Builder setStringFunction(SQLStringFunctions function) {
			this.column.setStringFunction(function);
			return this;
		}
		
		/**
		 * Assign an aggregate function
		 * 
		 * @param function  an aggregate function
		 * 
		 * @return The instance of this builder
		 */
		public Builder setAggregateFunction(SQLAggregateFunctions function){
			this.column.setAggregateFunction(function);
			return this;
		}
		
		/**
		 * Assign an alias for this select column
		 * 
		 * @param alias the alias to set
		 */
		public Builder setAlias(String alias) {
			this.column.setAlias(alias);
			return this;
		}

		/**
		 * @param function
		 * @see com.digsarustudio.banana.database.query.SelectColumn#setMiscellaneousFunction(com.digsarustudio.banana.database.query.MiscellaneousFunctions)
		 */
		public Builder setMiscellaneousFunction(MiscellaneousFunctions function) {
			this.column.setMiscellaneousFunction(function);
			return this;
		}

		/**
		 * @param dateTime
		 * @see com.digsarustudio.banana.database.query.SelectColumn#setDateTime(com.digsarustudio.banana.database.query.time.DateTime)
		 */
		public Builder setDateTime(DateTime dateTime) {
			column.setDateTime(dateTime);
			return this;
		}

		/**
		 * @param formula
		 * @see com.digsarustudio.banana.database.query.Dataset#setFormula(com.digsarustudio.banana.database.query.Formula)
		 */
		public Builder setFormula(Formula formula) {
			column.setFormula(formula);
			return this;
		}		

		/**
		 * @param function
		 * @see com.digsarustudio.banana.database.query.SelectColumn#setMathematicalFunction(com.digsarustudio.banana.database.query.mathfunc.MathematicalFunction)
		 */
		public Builder setMathematicalFunction(MathematicalFunction function) {
			column.setMathematicalFunction(function);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public SelectColumn build() {
			return this.column;
		}
	}
	
	private SimpleTableColumn	tableColumn			= null;
	
	private String 				alias				= null;
	/**
	 * @deprecated 1.02 Please use {@link #stringFunction} instead
	 */
	private SQLStringFunctions		queryFunction 			= null;
	private SQLStringFunctions		stringFunction 			= null;
	
	private SQLAggregateFunctions	aggregateFunction		= null;
	private MiscellaneousFunctions	miscellaneousFunction	= null;
	private DateTime					dateTime					= null;
	private Formula					formula					= null;
	private MathematicalFunction		mathematicalFuncion		= null;
	
	private SelectColumn(){
	}

	/**
	 * Returns the column for the SELECT Statement
	 * 
	 * @return the column for the SELECT Statement
	 */
	public SimpleTableColumn getTableColumn() {
		return tableColumn;
	}

	/**
	 * Assigns a name of the column for the SELECT Statement
	 * 
	 * @param name the column to set
	 */
	public void setTableColumn(SimpleTableColumn column) {
		this.tableColumn = column;
	}

	/**
	 * Returns the alias this column used in the SELECT Statement
	 * 
	 * @return the alias this column used in the SELECT Statement
	 */
	public String getAlias() {
		return alias;
	}

	/**
	 * Assign an alias for this select column
	 * 
	 * @param alias the alias to set
	 */
	public void setAlias(String alias) {
		this.alias = alias;
	}

	/**
	 * Returns the query function for the value
	 * 
	 * @return the query function for the value
	 * 
	 * @deprecated 1.0.2 Please use {@link #getStringFunction()} instead
	 */
	public SQLStringFunctions getQueryFunction() {
		return queryFunction;
	}

	/**
	 * Assigns a query function for the value
	 * 
	 * @param queryFunction the queryFunction to set
	 * 
	 * @deprecated 1.0.2 Please use {@link #setStringFunction(SQLStringFunctions)} instead
	 */
	public void setQueryFunction(SQLStringFunctions queryFunction) {
		this.queryFunction = queryFunction;
	}

	/**
	 * Assign a string function for the value
	 * 
	 * @param function the string function for the value
	 * 	
	 */
	public void setStringFunction(SQLStringFunctions function) {
		this.stringFunction = function;
	}
	
	public SQLStringFunctions getStringFunction(){
		return this.stringFunction;
	}
	
	/**
	 * Returns the aggregate function of this column
	 * @return  the aggregate function of this column
	 */
	public SQLAggregateFunctions getAggregateFunction() {
		return aggregateFunction;
	}

	/**
	 * Assigns an aggregate function for this column
	 * 
	 * @param function the {@link SQLAggregateFunctions} to set
	 */
	public void setAggregateFunction(SQLAggregateFunctions function) {
		this.aggregateFunction = function;
	}
	
	/**
	 * @return the miscellaneousFunction
	 */
	public MiscellaneousFunctions getMiscellaneousFunction() {
		return miscellaneousFunction;
	}

	/**
	 * @param miscellaneousFunction the miscellaneousFunction to set
	 */
	public void setMiscellaneousFunction(MiscellaneousFunctions function) {
		this.miscellaneousFunction = function;
	}

	/**
	 * @return the dateTime
	 */
	public DateTime getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(DateTime dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * Returns true if this column applied with {@link SQLStringFunctions}, otherwise false.
	 * 
	 * @return true if this column applied with {@link SQLStringFunctions}, otherwise false.
	 */
	public Boolean hasSQLStringFunction(){
		return (null != this.stringFunction);
	}
	
	/**
	 * Returns true if this column applied with {@link SQLAggregateFunctions}, otherwise false.
	 * 
	 * @return true if this column applied with {@link SQLAggregateFunctions}, otherwise false.
	 */
	public Boolean hasSQLAggregationFunction(){
		return (null != this.aggregateFunction);
	}
	
	public Boolean hasMiscellaneousFunction() {
		return (null != this.miscellaneousFunction);
	}
	
	public Boolean hasDateTime() {
		return (null != this.dateTime);
	}

	/**
	 * @return the formula
	 */
	public Formula getFormula() {
		return formula;
	}

	/**
	 * @param formula the formula to set
	 */
	public void setFormula(Formula formula) {
		this.formula = formula;
	}

	public Boolean hasFormula() {
		return (null != this.formula);
	}
	
	public void setMathematicalFunction(MathematicalFunction function) {
		this.mathematicalFuncion = function;
	}
	
	public MathematicalFunction getMathematicalFunction() {
		return this.mathematicalFuncion;
	}
	
	public Boolean hasMathematicalFunction() {
		return (null != this.mathematicalFuncion);
	}

	/**
	 * Returns the string contains column name and alias<br>
	 * <br>
	 * For example:<br>
	 * column = identifier<br>
	 * alias = id<br>
	 * The returning is `identifier` AS 'id'<br>
	 * <br>
	 * Or<br>
	 * table = inventory<br>
	 * column = id<br>
	 * alias = invId<br>
	 * The returning is inventory.id AS 'invId'<br>
	 * Or<br>
	 * column = identifier<br>
	 * alias = id<br>
	 * aggregate function = UNHEX()<br>
	 * The returning is UNHEX(`id`) AS 'id'<br>
	 * 
	 * @return the string contains column name and alias
	 * 
	 * @deprecated 1.0.2 Please use {@link ClauseCompiler} instead for {@link DataManipulationStrategy}
	 */
	public String getQuerySelectColumn(){
		StringBuilder builder = new StringBuilder();

		//The string will be formatted-well in the SQL syntax.
		String sqlColumn = this.tableColumn.getQuerySimpleTableColumn();
		
		if(null != this.queryFunction){
			sqlColumn = StringFormatter.format(this.queryFunction.getFormat(), sqlColumn);
		}else if(null != this.aggregateFunction){
			sqlColumn = StringFormatter.format(this.aggregateFunction.getFormat(), sqlColumn);
		}
		
		builder.append(sqlColumn);		
		
		if(null != this.alias && !this.alias.isEmpty()){
			builder.append(StringFormatter.format(" AS '%s'", this.alias));
		}
		
		return builder.toString();
	}
	
	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
