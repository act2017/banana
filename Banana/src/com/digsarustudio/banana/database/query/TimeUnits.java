/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The unit of time
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum TimeUnits {
//	unit Value	Expected expr Format
//	MICROSECOND	MICROSECONDS
//	SECOND	=>	SECONDS
	Second("SECOND")
	
//	MINUTE	=>	MINUTES
	, Minute("MINUTE")
	
//	HOUR		=>	HOURS
	, Hour("HOUR")
	
//	DAY		=>	DAYS
	, Day("DAY")
	
//	WEEK	WEEKS
//	MONTH	MONTHS
//	QUARTER	QUARTERS
//	YEAR	YEARS
//	SECOND_MICROSECOND	'SECONDS.MICROSECONDS'
//	MINUTE_MICROSECOND	'MINUTES:SECONDS.MICROSECONDS'
//	MINUTE_SECOND	'MINUTES:SECONDS'
//	HOUR_MICROSECOND	'HOURS:MINUTES:SECONDS.MICROSECONDS'
//	HOUR_SECOND	'HOURS:MINUTES:SECONDS'
//	HOUR_MINUTE	'HOURS:MINUTES'
//	DAY_MICROSECOND	'DAYS HOURS:MINUTES:SECONDS.MICROSECONDS'
//	DAY_SECOND	'DAYS HOURS:MINUTES:SECONDS'
//	DAY_MINUTE	'DAYS HOURS:MINUTES'
//	DAY_HOUR	'DAYS HOURS'
//	YEAR_MONTH	'YEARS-MONTHS'
	;
	
	private String value = null;

	private static TimeUnits[] VALUES = values();

	private TimeUnits(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public static TimeUnits fromValue(String value) {
		TimeUnits rtn = null;

		for (TimeUnits target : VALUES) {
			if (!value.equals(target.getValue())) {
				continue;
			}

			rtn = target;
		}

		return rtn;
	}
}
