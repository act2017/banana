/**
 * 
 */
package com.digsarustudio.banana.database.query;

import java.sql.SQLException;

import com.digsarustudio.banana.database.DatabaseConnection;
import com.digsarustudio.banana.database.io.DatabaseManipulator;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type of this interface can be used to send a query string to database 
 * This type can be reused in any circumstance, but please release the resource
 * by {@link Query#close()} while this object is no longer to use.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * @deprecated	1.0.2 Please use {@link DatabaseManipulator} instead.
 */
public interface Query {
	/**
	 * 
	 * The builder for {@link Query}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<Query> {
		/**
		 * Assigns a connection for this query
		 * 
		 * @param connection The connection for this query
		 * 
		 * @return The instance of this builder
		 */
		Builder setConnection(DatabaseConnection connection);
		
		/**
		 * The name of the target database this query object operates for.
		 * 
		 * @param name The name of the target database this query object operates for.
		 * 
		 * @return The instance of this builder
		 */
		Builder setDatabase(String name);
	}
	
	/**
	 * To execute the query string which is INSERT, UPDATE, or DELETE
	 * 
	 * @param queryString The query string to use
	 * 
	 * @return The row count of the effected row
	 * @throws SQLException 
	 */
	Integer executeUpdate(String queryString) throws SQLException;
	
	/**
	 * To execute the query string which is SELECT 
	 * 
	 * @param queryString The query string to use
	 * 
	 * @return the query string which gets the result as {@link QueryResult}
	 * @throws SQLException 
	 */
	QueryResult executeQuery(String queryString) throws SQLException;
		
	/**
	 * To release the resource.
	 * This function can be called at any time while this query object is no longer to use.
	 */
	void close();
	
	/**
	 * Returns the last query string.
	 * 
	 * @return the last query string
	 */
	String getLastQuery();
	
	/**
	 * To select a particular database
	 * 
	 * @param name The name of the particular database
	 * @throws SQLException 
	 */
	void selectDatabase(String name) throws SQLException;
}
