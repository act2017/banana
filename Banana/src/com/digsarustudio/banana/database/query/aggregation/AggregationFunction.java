/**
 * 
 */
package com.digsarustudio.banana.database.query.aggregation;

import com.digsarustudio.banana.database.query.SQLAggregateFunctions;

/**
 * The sub-type represents a aggregation function for the compiler to compile a SQL string depending on 
 * the database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public interface AggregationFunction {
	
	
	SQLAggregateFunctions getFunction();
	
	String toSQLString();
}
