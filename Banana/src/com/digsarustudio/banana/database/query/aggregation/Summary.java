/**
 * 
 */
package com.digsarustudio.banana.database.query.aggregation;

import com.digsarustudio.banana.database.query.Formula;
import com.digsarustudio.banana.database.query.SQLAggregateFunctions;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * The implementation of {@link SQLAggregateFunctions#Summary}.<br>
 *  
 * This class provides 3 interfaces for the client code to set the "target" up which are in String, 
 * {@link SimpleTableColumn}, and {@link Formula}. Please set up one from those three, the SQL query string 
 * compiler will choose the sequence by itself.<br>
 *  
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public class Summary extends BaseAggregationFunction implements AggregationFunction {
	/**
	 * 
	 * The object builder for {@link Summary}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.6
	 *
	 */
	public static class Builder implements ObjectBuilder<Summary> {
		private Summary result = null;

		public Builder() {
			this.result = new Summary();
		}

		/**
		 * @param target
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setTarget(java.lang.String)
		 */
		public Builder setTarget(String target) {
			result.setTarget(target);
			return this;
		}

		/**
		 * @param column
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setTarget(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		public Builder setTarget(SimpleTableColumn column) {
			result.setTarget(column);
			return this;
		}

		/**
		 * @param formula
		 * @see com.digsarustudio.banana.database.query.mathfunc.Round#setTarget(com.digsarustudio.banana.database.query.Formula)
		 */
		public Builder setTarget(Formula formula) {
			result.setTarget(formula);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Summary build() {
			return this.result;
		}

	}
	
	public static final SQLAggregateFunctions	FUNC		= SQLAggregateFunctions.Summary;
	
	private String				targetString		= null;
	private SimpleTableColumn	targetColumn		= null;
	private Formula				targetFormula	= null;
	
	/**
	 * @param function
	 */
	public Summary() {
		super(FUNC);
	}

	public void setTarget(String target) {
		this.targetString = target;
	}
	
	public void setTarget(SimpleTableColumn column) {
		this.targetColumn = column;
	}
	
	public void setTarget(Formula formula) {
		this.targetFormula = formula;
	}

	public String getStringTarget() {
		return this.targetString;
	}
	
	public SimpleTableColumn getColumnTarget() {
		return this.targetColumn;
	}
	
	public Formula getFormulaTarget() {
		return this.targetFormula;
	}
	
	public Boolean hasStringTarget() {
		return (null != this.targetString);
	}
	
	public Boolean hasColumnTarget() {
		return (null != this.targetColumn);
	}
	
	public Boolean hasFormulaTarget() {
		return (null != this.targetFormula);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.aggregation.AggregationFunction#toSQLString()
	 */
	@Override
	public String toSQLString() {
		StringBuffer buffer = new StringBuffer();
		
		if( this.hasStringTarget() ) {
			buffer.append(StringFormatter.format(this.getFunction().getFormat(), this.getStringTarget()));
		}else if( this.hasColumnTarget() ) {
			buffer.append(StringFormatter.format(this.getFunction().getFormat(), this.getColumnTarget().getQuerySimpleTableColumn()));
		}else if( this.hasFormulaTarget() ) {
			buffer.append(StringFormatter.format(this.getFunction().getFormat(), this.getFormulaTarget().toSQLString()));
		}		
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
