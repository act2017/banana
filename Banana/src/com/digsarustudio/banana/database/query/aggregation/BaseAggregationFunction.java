/**
 * 
 */
package com.digsarustudio.banana.database.query.aggregation;

import com.digsarustudio.banana.database.query.SQLAggregateFunctions;

/**
 * The basement class of {@link AggregationFunction} to handle the fundamental operations.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.6
 *
 */
public abstract class BaseAggregationFunction implements AggregationFunction {

	protected SQLAggregateFunctions function	= null;
	
	/**
	 * 
	 */
	public BaseAggregationFunction(SQLAggregateFunctions function) {
		this.function = function;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.mathfunc.AggregationFunction#getFunction()
	 */
	@Override
	public SQLAggregateFunctions getFunction() {
		
		return this.function;
	}

}
