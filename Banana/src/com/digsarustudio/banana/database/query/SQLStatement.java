/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * The sub-type of this statement has the ability to construct {@link SQLClause} and build a SQL string 
 * for the {@link DataAccessObject} to interact with database with writing, reading, or deleting operation.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2 Please use {@link com.digsarustudio.banana.database.query.statement.SQLStatement} instead.
 */
public interface SQLStatement {
	
	/**
	 * To construct a statement with clauses.<br>
	 */
	String construct();
}
