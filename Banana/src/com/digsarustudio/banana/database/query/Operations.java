/**
 * 
 */
package com.digsarustudio.banana.database.query;

import java.util.ArrayList;
import java.util.List;

/**
 * The operations, {@link Operations#And}, {@link Operations#Or}, {@link Operations#ExclusiveOR}
 * , {@link Operations#Not}, {@link Operations#NotAand}, {@link Operations#NotOr}
 * , for {@link Condition} used in WHERE clause with the columns
 * 
 * {@link Operations#In}, {@link Operations#Any}, and {@link Operations#Some} are used for Sub Query Statement
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum Operations {
		And("AND")
	,	Or("OR")
	,	ExclusiveOr("XOR")
	,	Not("NOT")
	,	NotAnd("NAND")
	,	NotOr("NOR")
	
	/**
	 * Those three as following are used for Sub Query Statement in WHERE condition
	 */
	,	Including("IN")
	,	Excluding("NOT IN")	
	,	Exists("EXISTS")
	,	NotExists("NOT EXISTS")
	
	/**
	 * Those followings must follow a comparison operator.
	 * It means “return TRUE if the comparison is TRUE for ALL/Some/Any of the values in the column 
	 * that the subquery returns. 
	 */
	,	Any("ANY")
	,	Some("SOME")
	,	All("ALL")	
	;
	
	private String value = null;
	
	private Operations(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public static List<Operations> getSubqueryOperators(){
		List<Operations> rtns = new ArrayList<>();
		
		rtns.add(Operations.Including);
		rtns.add(Operations.Excluding);
		rtns.add(Operations.Exists);
		rtns.add(Operations.NotExists);
		rtns.add(Operations.All);
		rtns.add(Operations.Any);
		rtns.add(Operations.Some);
		
		return rtns;
	}
	
	public static Boolean isSubqueryOperator(Operations operator){
		if( null == operator){
			return false;
		}
		
		List<Operations> operators = Operations.getSubqueryOperators();
		
		for (Operations operation : operators) {
			if( !operator.equals(operation) ){
				continue;
			}
			
			return true;
		}
		
		
		return false;
	}
}
