/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * FROM_UNIXTIME(unix_timestamp), FROM_UNIXTIME(unix_timestamp,format)
 * 
 * Returns a representation of the unix_timestamp argument as a value in 'YYYY-MM-DD HH:MM:SS' or YYYYMMDDHHMMSS format, 
 * depending on whether the function is used in a string or numeric context. The value is expressed in the current time 
 * zone. unix_timestamp is an internal timestamp value such as is produced by the UNIX_TIMESTAMP() function.
 * 
 * If format is given, the result is formatted according to the format string, which is used the same way as listed in 
 * the entry for the DATE_FORMAT() function.
 * 
 * mysql> SELECT FROM_UNIXTIME(1447430881);
 *         -> '2015-11-13 10:08:01'
 *         
 * mysql> SELECT FROM_UNIXTIME(1447430881) + 0;
 *         -> 20151113100801
 *         
 * mysql> SELECT FROM_UNIXTIME(UNIX_TIMESTAMP(), '%Y %D %M %h:%i:%s %x');
 *         -> '2015 13th November 10:08:01 2015'
 *         
 * Note: If you use UNIX_TIMESTAMP() and FROM_UNIXTIME() to convert between TIMESTAMP values and Unix timestamp values, 
 * the conversion is lossy because the mapping is not one-to-one in both directions. 
 * For details, see the description of the UNIX_TIMESTAMP() function.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class FromUnixTime extends BaseDateTime implements DateTime {
	/**
	 * 
	 * The object builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DateTime.Builder {
		private FromUnixTime result = null;

		public Builder() {
			this.result = new FromUnixTime();
		}

		/**
		 * @param format
		 * @see com.digsarustudio.banana.database.query.time.FromUnixTime#setFormat(java.lang.String)
		 */
		public Builder setFormat(String format) {
			result.setFormat(format);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(java.lang.String)
		 */
		@Override
		public Builder setBaseTime(String time) {
			this.result.setBaseTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
		 */
		@Override
		public Builder setBaseTime(SQLDateAndTimeFunctions function) {
			this.result.setBaseTime(function);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		@Override
		public Builder setBaseTime(SimpleTableColumn column) {
			this.result.setBaseTime(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DateTime build() {
			return this.result;
		}

	}
	
	private final SQLDateAndTimeFunctions	FUNC	  = SQLDateAndTimeFunctions.FromUNIXTime;
	
	private String	format	= null;
	
	
	/**
	 * 
	 */
	private FromUnixTime() {
		this.setFunction(FUNC);
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @param format the format to set
	 */
	public void setFormat(String format) {
		this.format = format;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.time.DateTime#toSQLString()
	 * 
	 * 
	 */
	@Deprecated
	@Override
	public String toSQLString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.function.getName());
		buffer.append("(");
		buffer.append(this.baseTime);
		
		if(null != this.format && !this.format.isEmpty()) {
			buffer.append(", ");
			buffer.append(String.format("'%s'", this.format));
		}
		buffer.append(")");
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
