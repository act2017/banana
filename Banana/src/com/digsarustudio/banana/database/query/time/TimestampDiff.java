/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.query.TimeUnits;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * TIMESTAMPDIFF(unit,datetime_expr1,datetime_expr2)<br>
 * 
 * Returns datetime_expr2 − datetime_expr1, where datetime_expr1 and datetime_expr2 are date or datetime 
 * expressions. One expression may be a date and the other a datetime; a date value is treated as a datetime 
 * having the time part '00:00:00' where necessary. The unit for the result (an integer) is given by the unit 
 * argument. The legal values for unit are the same as those listed in the description of the TIMESTAMPADD() 
 * function.<br>
 * 
 * mysql> SELECT TIMESTAMPDIFF(MONTH,'2003-02-01','2003-05-01');<br>
 *         -> 3<br>
 * mysql> SELECT TIMESTAMPDIFF(YEAR,'2002-05-01','2001-01-01');<br>
 *         -> -1<br>
 * mysql> SELECT TIMESTAMPDIFF(MINUTE,'2003-02-01','2003-05-01 12:05:55');<br>
 *         -> 128885<br>
 *         
 * Note<br>
 * The order of the date or datetime arguments for this function is the opposite of that used with the 
 * TIMESTAMP() function when invoked with 2 arguments.<br>
 * <br>
 * Please call {@link #setBaseTime(SimpleTableColumn)} to setup the time to be subtracted as datetime_expr1, and 
 * call 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TimestampDiff extends BaseDateTime implements DateTime {			
	private final SQLDateAndTimeFunctions	FUNC		= SQLDateAndTimeFunctions.TimestampDiff;
	
	/**
	 * 
	 * The object builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DateTime.Builder {
		private TimestampDiff result = null;

		public Builder() {
			this.result = new TimestampDiff();
		}

		public Builder setUnit(TimeUnits unit) {
			this.result.setUnit(unit);
			return this;
		}

		/**
		 * @param referentialTime
		 * @see com.digsarustudio.banana.database.query.time.TimestampDiff#setReferentialTime(java.lang.String)
		 */
		public Builder setReferentialTime(String referentialTime) {
			result.setReferentialTime(referentialTime);
			return this;
		}

		/**
		 * @param function
		 * @see com.digsarustudio.banana.database.query.time.TimestampDiff#setReferentialTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
		 */
		public Builder setReferentialTime(SQLDateAndTimeFunctions function) {
			result.setReferentialTime(function);
			return this;
		}

		/**
		 * @param column
		 * @see com.digsarustudio.banana.database.query.time.TimestampDiff#setReferentialTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		public Builder setReferentialTime(SimpleTableColumn column) {
			result.setReferentialTime(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(java.lang.String)
		 */
		@Override
		public Builder setBaseTime(String time) {
			this.result.setBaseTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
		 */
		@Override
		public Builder setBaseTime(SQLDateAndTimeFunctions function) {
			this.result.setBaseTime(function);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		@Override
		public Builder setBaseTime(SimpleTableColumn column) {
			this.result.setBaseTime(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DateTime build() {
			return this.result;
		}
	}
	
	private String		referentialTime	= null;
	private TimeUnits	unit				= null;
	
	/**
	 * 
	 */
	private TimestampDiff() {
		this.setFunction(FUNC);
	}

	/**
	 * @return the referentialTime
	 */
	public String getReferentialTime() {
		return referentialTime;
	}
	
	/**
	 * @param baseTime the baseTime to set
	 */
	public void setReferentialTime(String referentialTime) {
		this.referentialTime = String.format("'%s'", referentialTime);
	}

	public void setReferentialTime(SQLDateAndTimeFunctions function) {
		this.referentialTime = function.getFormat();
	}

	public void setReferentialTime(SimpleTableColumn column) {
		this.referentialTime = column.getTableColumnName();		
	}
	
	/**
	 * @return the unit
	 */
	public TimeUnits getUnit() {
		return unit;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(TimeUnits unit) {
		this.unit = unit;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.DateTime#toSQLString()
	 */
	@Deprecated
	@Override
	public String toSQLString() {
		if(null == this.referentialTime) {
			throw new IllegalArgumentException("referential time must be set");
		}
		
		if(null == this.baseTime) {
			throw new IllegalArgumentException("base time must be set");
		}		
		
		return String.format(this.function.getFormat(), this.unit.getValue(), this.referentialTime.toString(), this.baseTime);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
