/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.table.SimpleTableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type handles the exact date-time format for the clauses.<br>
 * 
 * TODO Before implementing this interface, please add a {@link DateTime} formatter in {@link ClauseCompiler} 
 *      to ensure the format of {@link DateTime} will match the SQL in a specific database.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public interface DateTime {
	/**
	 * 
	 * The builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DateTime> {
		Builder setBaseTime(String time);
		Builder setBaseTime(SQLDateAndTimeFunctions function);
		Builder setBaseTime(SimpleTableColumn column);
	}
	
	void setBaseTime(String time);
	void setBaseTime(SQLDateAndTimeFunctions function);
	void setBaseTime(SimpleTableColumn column);
	
	String getBaseTime();
	SQLDateAndTimeFunctions getFunction();
	
	/**
	 * @deprecated
	 * @return
	 */
	String toSQLString();
}
