/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.query.TimeUnits;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * TIMESTAMPADD(unit,interval,datetime_expr)
 * 
 * Adds the integer expression interval to the date or datetime expression datetime_expr. 
 * The unit for interval is given by the unit argument, which should be one of the following values: 
 * MICROSECOND (microseconds), SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, QUARTER, or YEAR.
 * 
 * The unit value may be specified using one of keywords as shown, or with a prefix of SQL_TSI_. 
 * For example, DAY and SQL_TSI_DAY both are legal.
 * 
 * mysql> SELECT TIMESTAMPADD(MINUTE,1,'2003-01-02');
 *       -> '2003-01-02 00:01:00'
 *       
 * mysql> SELECT TIMESTAMPADD(WEEK,1,'2003-01-02');
 * 		-> '2003-01-09'
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class TimestampAdd extends BaseDateTime implements DateTime {			
	private final SQLDateAndTimeFunctions	FUNC		= SQLDateAndTimeFunctions.TimestampAdd;
	
	/**
	 * 
	 * The object builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DateTime.Builder {
		private TimestampAdd result = null;

		public Builder() {
			this.result = new TimestampAdd();
		}

		public Builder setUnit(TimeUnits unit) {
			this.result.setUnit(unit);
			return this;
		}

		public Builder setInterval(Integer interval) {
			this.result.setInterval(interval);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(java.lang.String)
		 */
		@Override
		public Builder setBaseTime(String time) {
			this.result.setBaseTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
		 */
		@Override
		public Builder setBaseTime(SQLDateAndTimeFunctions function) {
			this.result.setBaseTime(function);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		@Override
		public Builder setBaseTime(SimpleTableColumn column) {
			this.result.setBaseTime(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DateTime build() {
			return this.result;
		}
	}
	
	private Integer 		interval	= 0;
	private TimeUnits	unit		= null;
	
	/**
	 * 
	 */
	private TimestampAdd() {
		this.setFunction(FUNC);
	}

	/**
	 * @return the interval
	 */
	public Integer getInterval() {
		return interval;
	}

	/**
	 * @param interval the interval to set
	 */
	public void setInterval(Integer interval) {
		this.interval = interval;
	}

	/**
	 * @return the unit
	 */
	public TimeUnits getUnit() {
		return unit;
	}

	/**
	 * @param unit the unit to set
	 */
	public void setUnit(TimeUnits unit) {
		this.unit = unit;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.DateTime#toSQLString()
	 */
	@Deprecated
	@Override
	public String toSQLString() {
		if(null == this.interval) {
			throw new IllegalArgumentException("interval must be set");
		}
		
		if(null == this.baseTime) {
			throw new IllegalArgumentException("base time must be set");
		}		
		
		return String.format(this.function.getFormat(), this.unit.getValue(), this.interval.toString(), this.baseTime);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
