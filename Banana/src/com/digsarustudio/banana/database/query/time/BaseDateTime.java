/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * To add time to a timestamp
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public abstract class BaseDateTime implements DateTime {	
	protected SQLDateAndTimeFunctions	function		= null;
	protected String						baseTime		= null;
	
	/**
	 * 
	 */
	public BaseDateTime() {

	}

	/**
	 * @return the function
	 */
	public SQLDateAndTimeFunctions getFunction() {
		return function;
	}

	/**
	 * @param function the function to set
	 */
	protected void setFunction(SQLDateAndTimeFunctions function) {
		this.function = function;
	}

	/**
	 * @return the baseTime
	 */
	public String getBaseTime() {
		return baseTime;
	}

	/**
	 * @param baseTime the baseTime to set
	 */
	public void setBaseTime(String baseTime) {
		this.baseTime = String.format("'%s'", baseTime);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.time.DateTime#setBaseTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
	 */
	@Override
	public void setBaseTime(SQLDateAndTimeFunctions function) {
		this.baseTime = function.getFormat();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.time.DateTime#setBaseTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
	 */
	@Override
	public void setBaseTime(SimpleTableColumn column) {
		this.baseTime = column.getTableColumnName();
		
	}	
}
