/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * NOW([fsp])
 * Returns the current date and time as a value in 'YYYY-MM-DD HH:MM:SS' or YYYYMMDDHHMMSS format, depending on whether 
 * the function is used in a string or numeric context. The value is expressed in the current time zone.
 * If the fsp argument is given to specify a fractional seconds precision from 0 to 6, the return value includes a 
 * fractional seconds part of that many digits.
 * mysql> SELECT NOW();
 *         -> '2007-12-15 23:50:26'
 *       mysql> SELECT NOW() + 0;
 *         -> 20071215235026.000000
 *         
 * NOW() returns a constant time that indicates the time at which the statement began to execute. 
 * (Within a stored function or trigger, NOW() returns the time at which the function or triggering statement began to 
 * execute.) This differs from the behavior for SYSDATE(), which returns the exact time at which it executes.
 * 
 * mysql> SELECT NOW(), SLEEP(2), NOW();
 * 
 * +---------------------+----------+---------------------+
 * | NOW()               | SLEEP(2) | NOW()               |
 * +---------------------+----------+---------------------+
 * | 2006-04-12 13:47:36 |        0 | 2006-04-12 13:47:36 |
 * +---------------------+----------+---------------------+
 * 
 * mysql> SELECT SYSDATE(), SLEEP(2), SYSDATE();
 * +---------------------+----------+---------------------+
 * | SYSDATE()           | SLEEP(2) | SYSDATE()           |
 * +---------------------+----------+---------------------+
 * | 2006-04-12 13:47:44 |        0 | 2006-04-12 13:47:46 |
 * +---------------------+----------+---------------------+
 * 
 * In addition, the SET TIMESTAMP statement affects the value returned by NOW() but not by SYSDATE(). 
 * This means that timestamp settings in the binary log have no effect on invocations of SYSDATE(). 
 * Setting the timestamp to a nonzero value causes each subsequent invocation of NOW() to return that value. 
 * Setting the timestamp to zero cancels this effect so that NOW() once again returns the current date and time.
 * 
 * See the description for SYSDATE() for additional information about the differences between the two functions.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Now extends BaseDateTime implements DateTime {
	/**
	 * 
	 * The object builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DateTime.Builder {
		private DateTime result = null;

		public Builder() {
			this.result = new Now();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(java.lang.String)
		 */
		@Override
		public Builder setBaseTime(String time) {
			this.result.setBaseTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
		 */
		@Override
		public Builder setBaseTime(SQLDateAndTimeFunctions function) {
			this.result.setBaseTime(function);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		@Override
		public Builder setBaseTime(SimpleTableColumn column) {
			this.result.setBaseTime(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DateTime build() {
			return this.result;
		}

	}
	
	private final SQLDateAndTimeFunctions	FUNC		= SQLDateAndTimeFunctions.Now;
	
	/**
	 * 
	 */
	private Now() {
		this.setFunction(FUNC);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.time.DateTime#toSQLString()
	 */
	@Deprecated
	@Override
	public String toSQLString() {
		return this.getFunction().getFormat();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
