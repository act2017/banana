/**
 * 
 */
package com.digsarustudio.banana.database.query.time;

import com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions;
import com.digsarustudio.banana.database.table.SimpleTableColumn;

/**
 * TIMESTAMP(expr), TIMESTAMP(expr1,expr2)<br>
 * <br>
 * With a single argument, this function returns the date or datetime expression expr as a datetime value. 
 * With two arguments, it adds the time expression expr2 to the date or datetime expression expr1 and returns 
 * the result as a datetime value.<br>
 * <br>
 * <br>
 * mysql> SELECT TIMESTAMP('2003-12-31');<br>
 *         -> '2003-12-31 00:00:00'<br>
 * mysql> SELECT TIMESTAMP('2003-12-31 12:00:00','12:00:00');<br>
 *         -> '2004-01-01 00:00:00'<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class Timestamp extends BaseDateTime implements DateTime {
	/**
	 * 
	 * The object builder for {@link DateTime}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements DateTime.Builder {
		private Timestamp result = null;

		public Builder() {
			this.result = new Timestamp();
		}

		/**
		 * @param addOnTime
		 * @see com.digsarustudio.banana.database.query.time.Timestamp#setAddOnTime(java.lang.String)
		 */
		public Builder setAddOnTime(String addOnTime) {
			result.setAddOnTime(addOnTime);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(java.lang.String)
		 */
		@Override
		public Builder setBaseTime(String time) {
			this.result.setBaseTime(time);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.query.SQLDateAndTimeFunctions)
		 */
		@Override
		public Builder setBaseTime(SQLDateAndTimeFunctions function) {
			this.result.setBaseTime(function);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.time.DateTime.Builder#setBaseTime(com.digsarustudio.banana.database.table.SimpleTableColumn)
		 */
		@Override
		public Builder setBaseTime(SimpleTableColumn column) {
			this.result.setBaseTime(column);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public DateTime build() {
			return this.result;
		}

	}
	
	private final SQLDateAndTimeFunctions	FUNC = SQLDateAndTimeFunctions.Timestamp;
	
	private String addOnTime		= null;

	/**
	 * 
	 */
	private Timestamp() {
		this.setFunction(FUNC);
	}

	/**
	 * @return the addOnTime
	 */
	public String getAddOnTime() {
		return addOnTime;
	}

	/**
	 * @param addOnTime the addOnTime to set
	 */
	public void setAddOnTime(String addOnTime) {
		this.addOnTime = String.format("'%s'", addOnTime);
	}
	
	public void setAddOnTime(SQLDateAndTimeFunctions function) {
		this.addOnTime = function.getFormat();
	}
	
	public void setAddOnTime(SimpleTableColumn column) {
		this.addOnTime = column.getTableColumnName();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.time.DateTime#toSQLString()
	 */
	@Deprecated
	@Override
	public String toSQLString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(this.function.getName());
		buffer.append("(");
		buffer.append(this.baseTime);
		
		if(null != this.addOnTime && !this.addOnTime.isEmpty()) {
			buffer.append(", ");
			buffer.append(this.addOnTime);
		}
		buffer.append(")");
		
		return buffer.toString();
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
}
