/**
 * 
 */
package com.digsarustudio.banana.database.query.statement.compiler;

import com.digsarustudio.banana.database.query.statement.DeleteStatement;
import com.digsarustudio.banana.database.query.statement.InsertStatement;
import com.digsarustudio.banana.database.query.statement.JoinStatement;
import com.digsarustudio.banana.database.query.statement.SelectStatement;
import com.digsarustudio.banana.database.query.statement.UpdateStatement;

/**
 * The sub-type of this compiler knows how to construct a proper statement for it.<br>
 * All of the construction algorithm should be implemented in the sub-type.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface StatementCompiler {
	/**
	 * To compile the {@link InsertStatement} as a SQL string
	 * 
	 * @param statement A {@link InsertStatement} to compile.
	 * 
	 * @return The SQL string based on the incoming statement
	 */
	String compile(InsertStatement statement);
	
	/**
	 * To compile the {@link SelectStatement} as a SQL string
	 * 
	 * @param statement A {@link SelectStatement} to compile.
	 * 
	 * @return The SQL string based on the incoming statement
	 */
	String compile(SelectStatement statement);
	
	/**
	 * To compile the {@link JoinStatement} as a SQL string
	 * 
	 * @param statement A {@link JoinStatement} to compile.
	 * 
	 * @return The SQL string based on the incoming statement
	 */
	String compile(JoinStatement statement);
	
	/**
	 * To compile the {@link UpdateStatement} as a SQL string
	 * 
	 * @param statement A {@link UpdateStatement} to compile.
	 * 
	 * @return The SQL string based on the incoming statement
	 */
	String compile(UpdateStatement statement);
	
	/**
	 * To compile the {@link DeleteStatement} as a SQL string
	 * 
	 * @param statement A {@link DeleteStatement} to compile.
	 * 
	 * @return The SQL string based on the incoming statement
	 */
	String compile(DeleteStatement statement);
}
