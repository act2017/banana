/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import java.util.List;

import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLAggregateFunctions;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.SQLClause;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * The sub-type of this statement has the ability to construct {@link SQLClause} and build a SQL string 
 * for the {@link DataAccessObject} to interact with database with writing, reading, or deleting operation.<br> 
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public interface SQLStatement {
	/**
	 * Assigns a modifier which indicates how the data affected by this statement.<br>
	 * 
	 * @param modifier The modifier which indicates how the data affected by this statement.<br>
	 */
	void setModifier(SQLSyntaxModifiers modifier);
	
	/**
	 * To indicate the data will be inserted into a particular table.<br>
	 * 
	 * @param table The data to be inserted to
	 */
	void setInsertInto(Table table);
	
	/**
	 * To indicate the data will be fetched from a particular table.<br>
	 * 
	 * @param table The data to be retrieved from
	 */
	void setSelectFrom(Table table);
	
	/**
	 * To indicate the data will be fetched from a particular temporarily virtual table.<br>
	 * 
	 * @param table The data to be retrieved from
	 * 
	 * @deprecated 1.0.2 Please use {@link #setSelectFrom(Table)} instead
	 */
	void setSelectFrom(VirtualTable table);
	
	/**
	 * To indicate the data will be deleted from a particular table.<br>
	 * 
	 * @param table The data to be deleted from
	 */
	void setDeleteFrom(Table table);
	
	/**
	 * Set the table for the statement to update
	 * 
	 * @param table for the statement to update
	 */
	void setUpdateTable(Table table);
	
	/**
	 * Assigns a list of dataset for the {@link InsertStatement} and {@link UpdateStatement} to manipulate.<br>
	 * 
	 * @param dataset The list of dataset for the {@link InsertStatement} and {@link UpdateStatement} to manipulate.
	 */
	void setDatasets(List<Dataset> dataset);
	
	/**
	 * Assigns a dataset for the {@link InsertStatement} and {@link UpdateStatement} to manipulate.<br>
	 * 
	 * @param dataset The dataset for the {@link InsertStatement} and {@link UpdateStatement} to manipulate.
	 */
	void addDataset(Dataset dataset);
	
	/**
	 * Assigns a set of assignments used for updating the duplicate key.<br>
	 * 
	 * This method is set by {@link InsertStatement} only.
	 * 
	 * @param assignments The set of assignments used for updating the duplicate key
	 */
	void setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments);
	
	/**
	 * Assigns an assignment used for updating the duplicate key.<br>
	 * 
	 * This method is set by {@link InsertStatement} only.
	 * 
	 * @param assignment The assignment used for updating the duplicate key
	 */
	void addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment);
	
	/**
	 * Assigns a set of columns to be selected in a search.<br>
	 * 
	 * This method is set by {@link SelectStatement} and {@link InsertStatement} which retrieves data 
	 * from a select search only.<br>
	 * 
	 * @param columns The set of columns to be selected in a search.
	 */
	void setSelectColumns(List<SelectColumn> columns);
	
	/**
	 * Assigns a column to be selected in a search.<br>
	 * 
	 * This method is set by {@link SelectStatement} and {@link InsertStatement} which retrieves data 
	 * from a select search only.<br>
	 * 
	 * @param column The column to be selected in a search.
	 */
	void addSelectColumn(SelectColumn column);
	
	/**
	 * Assigns a list of tables for the join.<br>
	 * 
	 * @param tables The list of tables for the join.
	 */
	void setJoinTable(List<JoinTable> tables);
	
	/**
	 * Assigns a table for the join.<br>
	 * 
	 * @param table The table for the join.
	 */
	void addJoinTable(JoinTable table);
	
	/**
	 * Assigns a list of tables for the join.<br>
	 * 
	 * @param tables The list of tables for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTable(List)} instead
	 */
	void setJoinTableReferences(List<JoinTable> tables);
	
	/**
	 * Assigns a table for the join.<br>
	 * 
	 * @param table The table for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #addJoinTable(JoinTable)} instead
	 */
	void addJoinTableReference(JoinTable table);	
	
	/**
	 * Assigns a type which indicates how to join the tables.<br>
	 * 
	 * @param type The type which indicates how to join the tables.
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTableReferences(List)} instead
	 */
	void setJoinType(TableJoinType type);
	
	/**
	 * Assigns a list of tables for the join.<br>
	 * 
	 * @param tables The list of tables for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #setJoinTableReferences(List)} instead
	 */
	void setJoinTableRefereces(List<Table> tables);
	
	/**
	 * Assigns a table for the join.<br>
	 * 
	 * @param table The table for the join.
	 * 
	 * @deprecated 1.0.2 Please use {@link #addJoinCondition(JoinCondition)} instead
	 */
	void addJoinTableReference(Table table);
	
	/**
	 * Assigns a list of condition for the set of particular data to join.<br>
	 * 
	 * @param conditionsa The list of condition for the set of particular data to join
	 * 
	 * @deprecated 1.0.2 please use {@link #setJoinTable(List)} instead
	 */
	void setJoinConditions(List<JoinCondition> conditions);
	
	/**
	 * Assigns a condition for the particular data to join
	 * 
	 * @param condition The condition for the particular data to join
	 * 
	 * @deprecated 1.0.2 Please use {@link #addJoinTable(JoinTable)} instead
	 */
	void addJoinCondition(JoinCondition condition);
	
	/**
	 * Assigns a set of conditions used for finding data.<br>
	 * 
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, {@link DeleteStatement}, 
	 * and {@link InsertStatement} which retrieves data from a select search only.<br>
	 * 
	 * @param conditions The set of conditions used for finding data.<br>
	 */
	void setConditions(List<Condition> conditions);
	
	/**
	 * Assigns a condition used for finding data.<br>
	 * 
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, {@link DeleteStatement}, and 
	 * and {@link InsertStatement} which retrieves data from a select search only.<br>
	 * 
	 * @param condition The condition used for finding data.<br>
	 */
	void addCondition(Condition condition);
	
	/**
	 * Assign a set of aggregation which indicate how to distinguish, summarize, and average the data and so on.<br>
	 * <br>
	 * This method is set by {@link SelectStatement} only and the {@link SQLAggregateFunctions} could be set 
	 * for some columns which has to be manipulated in aggregation.<br>
	 * This method is also available for {@link InsertStatement} which retrieves data 
	 * from a select search.<br>
	 * If the search is set as distinct search, this method should be called for the column which has to be 
	 * grouped together to hide the reduplicated data.<br>
	 * 
	 * @param aggregations The set of aggregations which indicate how to summarize and average the data and so on.
	 */
	void setAggregations(List<Aggregation> aggregations);
	
	/**
	 * Assign an aggregation which indicates how to distinguish, summarize, and average the data and so on.<br>
	 * <br>
	 * This method is set by {@link SelectStatement} only and the {@link SQLAggregateFunctions} could be set 
	 * for some columns which has to be manipulated in aggregation.<br>
	 * This method is also available for {@link InsertStatement} which retrieves data 
	 * from a select search.<br>
	 * If the search is set as distinct search, this method should be called for the column which has to be 
	 * grouped together to hide the reduplicated data.<br>
	 * 
	 * @param aggregation The aggregation which indicates how to summarize and average the data and so on.
	 */
	void addAggregation(Aggregation aggregation);
	
	/**
	 * Assigns a set of ordering which indicates how the data to be going to be manipulated.<br>
	 * In {@link UpdateStatement} and {@link DeleteStatement}, the data are updated or deleted in the order that 
	 * is specified. In {@link SelectStatement}, the data are fetched and listed in the order that is specified.<br>
	 * <br>
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * This method is also available for {@link InsertStatement} which retrieves data 
	 * from a select search.<br>
	 * 
	 * @param orderings The set of ordering which indicates how the data to be going to be manipulated.
	 */
	void setOrderings(List<Ordering> orderings);
	
	/**
	 * Assigns an ordering of the data to be manipulated.<br>
	 * In {@link UpdateStatement} and {@link DeleteStatement}, the data are updated or deleted in the order that 
	 * is specified. In {@link SelectStatement}, the data are fetched and listed in the order that is specified.<br>
	 * <br>
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * This method is also available for {@link InsertStatement} which retrieves data 
	 * from a select search.<br>
	 * 
	 * @param ordering The ordering of the data to be manipulated.
	 */
	void addOrdering(Ordering ordering);
	
	/**
	 * Assigns the starting row of a search to retrieve data.<br>
	 * <br>
	 * This method is only set by {@link SelectStatement}.<br>
	 * This method is also available for {@link InsertStatement} which retrieves data 
	 * from a select search.<br>
	 * @param offset The index of the row to be the 1st data to retrieve. This value is 1-based index.
	 */
	void setOffset(Integer offset);
	
	/**
	 * Assigns the maximum count of row to be retrieved from a particular table.<br>
	 * <br>
	 * This method is set by {@link SelectStatement}, {@link UpdateStatement}, and {@link DeleteStatement} only.<br>
	 * This method is also available for {@link InsertStatement} which retrieves data 
	 * from a select search.<br>
	 * @param rowCount THe maximum count of row to be retrieved from a particular table.
	 */
	void setMaxRowCount(Integer rowCount);
	
	/**
	 * Assigns a compiler for this statement to build SQL string.
	 * 
	 * @param compiler The compile for this statement to build SQL string.
	 */
	void setCompiler(StatementCompiler compiler);
	
	/**
	 * Assigns a compiler for the clauses used in this statement to build SQL string.
	 * 
	 * @param compiler The compile for the clauses used in this statement to build SQL string.
	 */
	void setCompiler(ClauseCompiler compiler);
	
	/**
	 * To construct a statement with clauses.<br>
	 */
	String construct();
}
