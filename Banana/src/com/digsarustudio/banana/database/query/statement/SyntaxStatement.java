/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import java.util.List;

import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.AbstractSQLClauseCompilerFactory;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * The base class of {@link SQLStatement} which contains {@link AbstractSQLClauseCompilerFactory} and {@link StatementCompiler}.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public abstract class SyntaxStatement implements SQLStatement {
	protected StatementCompiler	compiler		= null;
	protected ClauseCompiler	clauseCompiler	= null;
	
	/**
	 * 
	 */
	public SyntaxStatement() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setModifier(SQLSyntaxModifiers modifier) {
		throw new UnsupportedOperationException("This statement doesn't support modifier");
		
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setInsertInto(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setInsertInto(Table table) {
		throw new UnsupportedOperationException("This statement doesn't support table reference");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setSelectFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setSelectFrom(Table table) {
		throw new UnsupportedOperationException("This statement doesn't support table reference");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setSelectFrom(com.digsarustudio.banana.database.table.VirtualTable)
	 */
	@Deprecated
	@Override
	public void setSelectFrom(VirtualTable table) {
		throw new UnsupportedOperationException("This statement doesn't support virtual table reference");		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setDeleteFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setDeleteFrom(Table table) {
		throw new UnsupportedOperationException("This statement doesn't support table reference");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setUpdateTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setUpdateTable(Table table) {
		throw new UnsupportedOperationException("This statement doesn't support table reference");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		throw new UnsupportedOperationException("This statement doesn't support data inserting and updating");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		throw new UnsupportedOperationException("This statement doesn't support data inserting and updating");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setOnDuplicateKeyUpdateAssignments(java.util.List)
	 */
	@Override
	public void setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments) {
		throw new UnsupportedOperationException("This statement doesn't support updating of dulicate key]");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)
	 */
	@Override
	public void addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment) {
		throw new UnsupportedOperationException("This statement doesn't support updating of dulicate key");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setSelectColumns(java.util.List)
	 */
	@Override
	public void setSelectColumns(List<SelectColumn> columns) {
		throw new UnsupportedOperationException("This statement doesn't support data search");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)
	 */
	@Override
	public void addSelectColumn(SelectColumn column) {
		throw new UnsupportedOperationException("This statement doesn't support data search");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)
	 */
	@Deprecated
	@Override
	public void setJoinType(TableJoinType type) {
		throw new UnsupportedOperationException("This statement doesn't support join table");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setJoinTableRefereces(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableRefereces(List<Table> tables) {
		throw new UnsupportedOperationException("This statement doesn't support join table");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addJoinTableReference(com.digsarustudio.banana.database.table.Table)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(Table table) {
		throw new UnsupportedOperationException("This statement doesn't support join table");
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setJoinTable(java.util.List)
	 */
	@Override
	public void setJoinTable(List<JoinTable> tables) {
		throw new UnsupportedOperationException("This statement doesn't support join table");		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTable(JoinTable table) {
		throw new UnsupportedOperationException("This statement doesn't support join table");		
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setJoinTableReferences(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableReferences(List<JoinTable> tables) {
		throw new UnsupportedOperationException("This statement doesn't support join table");		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addJoinTableReference(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(JoinTable table) {
		throw new UnsupportedOperationException("This statement doesn't support join table");		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setJoinConditions(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinConditions(List<JoinCondition> conditions) {
		throw new UnsupportedOperationException("This statement doesn't support join table");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
	 */
	@Deprecated
	@Override
	public void addJoinCondition(JoinCondition condition) {
		throw new UnsupportedOperationException("This statement doesn't support join table");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		throw new UnsupportedOperationException("This statement doesn't support conditional operation");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		throw new UnsupportedOperationException("This statement doesn't support conditional operation");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setAggregations(java.util.List)
	 */
	@Override
	public void setAggregations(List<Aggregation> aggregations) {
		throw new UnsupportedOperationException("This statement doesn't support grouping");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addAggregation(com.digsarustudio.banana.database.query.Aggregation)
	 */
	@Override
	public void addAggregation(Aggregation aggregation) {
		throw new UnsupportedOperationException("This statement doesn't support grouping");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		throw new UnsupportedOperationException("This statement doesn't support ordering");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		throw new UnsupportedOperationException("This statement doesn't support ordering");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setOffset(java.lang.Integer)
	 */
	@Override
	public void setOffset(Integer offset) {
		throw new UnsupportedOperationException("This statement doesn't support that to assign the 1st row to manipulate.");
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		throw new UnsupportedOperationException("This statement doesn't support maximum affecting row count");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)
	 */
	@Override
	public void setCompiler(StatementCompiler compiler) {
		this.compiler = compiler;
		this.updateCompiler(compiler);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#setCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)
	 */
	@Override
	public void setCompiler(ClauseCompiler compiler) {
		this.clauseCompiler = compiler;		
		this.updateCompiler(compiler);
	}
	
	/**
	 * To update the compiler for the sub statements.<br>
	 * 
	 * @param compiler The compiler for the sub statements.<br>
	 */
	abstract protected void updateCompiler(StatementCompiler compiler);
	
	/**
	 * To update the compiler for the clauses.<br>
	 * @param compiler The compiler for the clauses.<br>
	 */
	abstract protected void updateCompiler(ClauseCompiler compiler);
}
