/**
 * 
 */
package com.digsarustudio.banana.database.query.statement.compiler.mysql;

import com.digsarustudio.banana.database.query.statement.DeleteStatement;
import com.digsarustudio.banana.database.query.statement.InsertStatement;
import com.digsarustudio.banana.database.query.statement.JoinStatement;
import com.digsarustudio.banana.database.query.statement.SelectStatement;
import com.digsarustudio.banana.database.query.statement.UpdateStatement;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;

/**
 * This compiler compiles incoming data into MySQL SQL string form.<br>
 * It supports MySQL 5.7 and it works well with MySQL 5.6 as well.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class MySQLStatementCompiler implements StatementCompiler {

	/**
	 * 
	 */
	public MySQLStatementCompiler() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler#compile(com.digsarustudio.banana.database.query.statement.InsertStatement)
	 */
	@Override
	public String compile(InsertStatement statement) {
//		INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
//			    [INTO] tbl_name
//			    [PARTITION (partition_name [, partition_name] ...)]
//			    [(col_name [, col_name] ...)]
//			    {VALUES | VALUE} (value_list) [, (value_list)] ...
//			    [ON DUPLICATE KEY UPDATE assignment_list]
//
//			INSERT [LOW_PRIORITY | DELAYED | HIGH_PRIORITY] [IGNORE]
//			    [INTO] tbl_name
//			    [PARTITION (partition_name [, partition_name] ...)]
//			    SET assignment_list
//			    [ON DUPLICATE KEY UPDATE assignment_list]
//
//			INSERT [LOW_PRIORITY | HIGH_PRIORITY] [IGNORE]
//			    [INTO] tbl_name
//			    [PARTITION (partition_name [, partition_name] ...)]
//			    [(col_name [, col_name] ...)]
//			    SELECT ...
//			    [ON DUPLICATE KEY UPDATE assignment_list]
//
//			value:
//			    {expr | DEFAULT}
//
//			value_list:
//			    value [, value] ...
//
//			assignment:
//			    col_name = value
//
//			assignment_list:
//			    assignment [, assignment] ...
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("INSERT");
		
		String modifier = statement.getSQLModifier();
		if(null != modifier){
			builder.append(" ");
			builder.append(modifier);
		}
		
		String tableReference = statement.getSQLTableReference();
		if( null == tableReference ){
			throw new IllegalArgumentException("Table reference must be set");
		}
		builder.append(" ");
		builder.append(tableReference);
		
		String insertColumn = statement.getSQLInsertColumn();
		if( null == insertColumn ){
			throw new IllegalArgumentException("Columns must be set");
		}
		builder.append(" ");
		builder.append(insertColumn);
				
		String insertValue = statement.getSQLInsertValue();
		if( null == insertValue ){
			throw new IllegalArgumentException("Values must be set");
		}
		builder.append(" ");
		builder.append(insertValue);
		
		String selectStatement = statement.getSQLSelectStatement();
		if(null != selectStatement){
			builder.append(" ");
			builder.append(selectStatement);
		}
		
		String duplicateKeyUpdate = statement.getSQLDuplicateKeyUpdate();
		if(null != duplicateKeyUpdate){
			builder.append(" ");
			builder.append(duplicateKeyUpdate);
		}
		
		builder.append(";");
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler#compile(com.digsarustudio.banana.database.query.statement.SelectStatement)
	 */
	@Override
	public String compile(SelectStatement statement) {
//		SELECT
//	    [ALL | DISTINCT | DISTINCTROW ]
//	      [HIGH_PRIORITY]
//	      [STRAIGHT_JOIN]
//	      [SQL_SMALL_RESULT] [SQL_BIG_RESULT] [SQL_BUFFER_RESULT]
//	      [SQL_CACHE | SQL_NO_CACHE] [SQL_CALC_FOUND_ROWS]
//	    select_expr [, select_expr ...]
//	    [FROM table_references
//	      [PARTITION partition_list]
//	    [WHERE where_condition]
//	    [GROUP BY {col_name | expr | position}
//	      [ASC | DESC], ... [WITH ROLLUP]]
//	    [HAVING where_condition]
//	    [ORDER BY {col_name | expr | position}
//	      [ASC | DESC], ...]
//	    [LIMIT {[offset,] row_count | row_count OFFSET offset}]
//	    [PROCEDURE procedure_name(argument_list)]
//	    [INTO OUTFILE 'file_name'
//	        [CHARACTER SET charset_name]
//	        export_options
//	      | INTO DUMPFILE 'file_name'
//	      | INTO var_name [, var_name]]
//	    [FOR UPDATE | LOCK IN SHARE MODE]]
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("SELECT");
		
		String modifier = statement.getSQLModifier();
		if(null != modifier){
			builder.append(" ");
			builder.append(modifier);
		}
		
		String selectExpress = statement.getSQLSelectColumn();
		if( null != selectExpress ) {
			builder.append(" ");
			if(selectExpress.isEmpty()){
				builder.append("*");
			}else{
				builder.append(selectExpress);
			}
		}
		
		String tableReference = statement.getSQLTableReference();
		if(null != tableReference) {
			builder.append(" ");
			builder.append(tableReference);
		}
		
		String joinStatement = statement.getSQLJoinTable();
		if(null != joinStatement){
			builder.append(" ");
			builder.append(joinStatement);
		}
		
		String whereCondition = statement.getSQLWhereCondition();
		if(null != whereCondition){
			builder.append(" ");
			builder.append(whereCondition);			
		}
		
		String groupBys = statement.getSQLAggregation();
		if(null != groupBys){
			builder.append(" ");
			builder.append(groupBys);			
		}
		
		String havingCondition = statement.getSQLHavingCondtion();
		if(null != havingCondition){
			builder.append(" ");
			builder.append(havingCondition);			
		}
		
		String ordering = statement.getSQLOrdering();
		if(null != ordering){
			builder.append(" ");
			builder.append(ordering);			
		}
		
		String offsetAndLimit = statement.getSQLOffsetAndLImit();
		if(null != offsetAndLimit){
			builder.append(" ");
			builder.append(offsetAndLimit);			
		}
		
		builder.append(";");
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler#compile(com.digsarustudio.banana.database.query.statement.JoinStatement)
	 */
	@Override
	public String compile(JoinStatement statement) {
//		table_references:
//		    escaped_table_reference [, escaped_table_reference] ...
//
//		escaped_table_reference:
//		    table_reference
//		  | { OJ table_reference }
//
//		table_reference:
//		    table_factor
//		  | join_table
//
//		table_factor:
//		    tbl_name [PARTITION (partition_names)]
//		        [[AS] alias] [index_hint_list]
//		  | table_subquery [AS] alias
//		  | ( table_references )
//
//		join_table:
//		    table_reference [INNER | CROSS] JOIN table_factor [join_condition]
//		  | table_reference STRAIGHT_JOIN table_factor
//		  | table_reference STRAIGHT_JOIN table_factor ON conditional_expr
//		  | table_reference {LEFT|RIGHT} [OUTER] JOIN table_reference join_condition
//		  | table_reference NATURAL [{LEFT|RIGHT} [OUTER]] JOIN table_factor
//
//		join_condition:
//		    ON conditional_expr
//		  | USING (column_list)
//
//		index_hint_list:
//		    index_hint [, index_hint] ...
//
//		index_hint:
//		    USE {INDEX|KEY}
//		      [FOR {JOIN|ORDER BY|GROUP BY}] ([index_list])
//		  | IGNORE {INDEX|KEY}
//		      [FOR {JOIN|ORDER BY|GROUP BY}] (index_list)
//		  | FORCE {INDEX|KEY}
//		      [FOR {JOIN|ORDER BY|GROUP BY}] (index_list)
//
//		index_list:
//		   index_name [, index_name] ...		
		
		StringBuilder builder = new StringBuilder();
		
		String joinTable = statement.getSQLJointable();
		if(null == joinTable){
			throw new IllegalArgumentException("Join table reference must be set.");
		}		
		builder.append(" ");
		builder.append(joinTable);
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler#compile(com.digsarustudio.banana.database.query.statement.UpdateStatement)
	 */
	@Override
	public String compile(UpdateStatement statement) {
//For single table		
//		UPDATE [LOW_PRIORITY] [IGNORE] table_reference
//	    SET assignment_list
//	    [WHERE where_condition]
//	    [ORDER BY ...]
//	    [LIMIT row_count]
//		
//For multiple table
//		UPDATE [LOW_PRIORITY] [IGNORE] table_references
//	    SET assignment_list
//	    [WHERE where_condition]
//
//		value:
//		    {expr | DEFAULT}
//		
//		assignment:
//		    col_name = value
//		
//		assignment_list:
//		    assignment [, assignment] ..		
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("UPDATE");
		
		String modifier = statement.getSQLModifier();
		if(null != modifier){
			builder.append(" ");
			builder.append(modifier);
		}
		
		
		String tableReference = statement.getSQLTableReference();
		if(null == tableReference){
			throw new IllegalArgumentException("The table reference must be set");
		}
		builder.append(" ");
		builder.append(tableReference);
		
		String dataset = statement.getSQLDataset();
		if(null == dataset){
			throw new IllegalArgumentException("Dataset reference must be set");
		}
		builder.append(" ");
		builder.append(dataset);
		
		String condition = statement.getSQLCondition();
		if( null != condition ){
			builder.append(" ");
			builder.append(condition);
		}
		
		String ordering = statement.getSQLOrdering();
		if( null != ordering ){
			builder.append(" ");
			builder.append(ordering);
		}
		
		String maxRowCount = statement.getSQLMaxRowCount();
		if( null != maxRowCount ){
			builder.append(" ");
			builder.append(maxRowCount);
		}		
		
		builder.append(";");
		
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler#compile(com.digsarustudio.banana.database.query.statement.DeleteStatement)
	 */
	@Override
	public String compile(DeleteStatement statement) {
//		DELETE [LOW_PRIORITY] [QUICK] [IGNORE] FROM tbl_name
//	    [PARTITION (partition_name [, partition_name] ...)]
//	    [WHERE where_condition]
//	    [ORDER BY ...]
//	    [LIMIT row_count]		
		
		StringBuilder builder = new StringBuilder();
		
		builder.append("DELETE FROM");
		
		String modifier = statement.getSQLModifier();
		if(null != modifier){
			builder.append(" ");
			builder.append(modifier);
		}
		
		
		String tableReference = statement.getSQLTableReference();
		if(null == tableReference){
			throw new IllegalArgumentException("The table reference must be set");
		}
		builder.append(" ");
		builder.append(tableReference);
				
		String condition = statement.getSQLCondition();
		if( null != condition ){
			builder.append(" ");
			builder.append(condition);
		}
		
		String ordering = statement.getSQLOrdering();
		if( null != ordering ){
			builder.append(" ");
			builder.append(ordering);
		}
		
		String maxRowCount = statement.getSQLMaxRowCount();
		if( null != maxRowCount ){
			builder.append(" ");
			builder.append(maxRowCount);
		}		
		
		builder.append(";");
		
		return builder.toString();
	}

}
