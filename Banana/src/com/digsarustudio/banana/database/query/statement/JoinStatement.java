/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import java.util.List;

import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.JoinConditionClause;
import com.digsarustudio.banana.database.query.claue.JoinTableClause;
import com.digsarustudio.banana.database.query.claue.JoinTableReferenceClause;
import com.digsarustudio.banana.database.query.claue.SQLClause;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;

/**
 * This statement is used to generate a SQL string which describes how to make tables join together.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class JoinStatement extends SyntaxStatement implements SQLStatement {
	private JoinTableReferenceClause	joinTableReference	= null;
	private JoinConditionClause			joinCondition		= null;
	private JoinTableClause				joinTable			= null;

	/**
	 * 
	 */
	public JoinStatement() {
	}
	
	/**
	 * Returns the join table clause.<br>
	 * For example:<br>
	 * 	INNER JOIN `make` AS `mk` ON (model.make=mk.id)<br>
	 * 
	 * @return Returns the join table clause
	 */
	public String getSQLJointable(){
		if(null == this.joinTable){
			return null;
		}
		
		this.joinTable.build();
		return this.joinTable.getResult();
	}
	
	/**
	 * Returns the table reference in SQL string, null returned if the table reference is not supported.<br>
	 * 
	 * @return the table reference in SQL string, null returned if the table reference is not supported
	 * 
	 * @deprecated 1.0.2 Please use {@link #getJointable()} instead
	 */
	public String getSQLJoinTableReference(){
		if(null == this.joinTableReference){
			return null;
		}
		
		this.joinTableReference.build();
		return this.joinTableReference.getResult();
	}

	/**
	 * Returns the condition in SQL string, null returned if the condition is not supported.<br>
	 * 
	 * @return the condition in SQL string, null returned if the condition is not supported
	 * 
	 * @deprecated 1.0.2 Please use {@link #getJointable()} instead
	 */
	public String getSQLJoinCondition(){
		if(null == this.joinCondition){
			return null;
		}
		
		this.joinCondition.build();
		return this.joinCondition.getResult();
	}
	
	/**
	 * Returns the join table reference
	 * 
	 * @return the join table reference
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLJoinTableReference()} instead
	 */
	public JoinTableReferenceClause getJoinTableReference() {
		return joinTableReference;
	}

	/**
	 * Returns the join condition
	 * 
	 * @return the join condition
	 * 
	 * @deprecated 1.02. Please use {@link #getSQLJoinCondition()} instead
	 */
	public JoinConditionClause getJoinCondition() {
		return joinCondition;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTable(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTable(List<JoinTable> tables) {
		throw new UnsupportedOperationException("Please use #addJoinTable() instead");
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTable(JoinTable table) {
		if(null == table){
			throw new IllegalArgumentException("Table must be set.");
		}
		
		SQLClause clause = this.getSourceJoinTable();
		clause.addJoinTable(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)
	 */
	@Deprecated
	@Override
	public void setJoinType(TableJoinType type) {
		if(null == type){
			throw new IllegalArgumentException("Join Type must be set.");
		}
		
		JoinTableReferenceClause joinTableReference = this.getSourceJoinTableReference();
		joinTableReference.setJoinType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTableRefereces(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableRefereces(List<Table> tables) {
		if(null == tables){
			throw new IllegalArgumentException("Table must be set.");
		}
		
		JoinTableReferenceClause joinTableReference = this.getSourceJoinTableReference();
		joinTableReference.setJoinTableRefereces(tables);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTableReference(com.digsarustudio.banana.database.table.Table)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(Table table) {
		if(null == table){
			throw new IllegalArgumentException("Table must be set.");
		}
		
		JoinTableReferenceClause joinTableReference = this.getSourceJoinTableReference();
		joinTableReference.addJoinTableReference(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTableReferences(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableReferences(List<JoinTable> tables) {
		if(null == tables){
			throw new IllegalArgumentException("Table must be set.");
		}
		
		JoinTableReferenceClause joinTableReference = this.getSourceJoinTableReference();
		joinTableReference.setJoinTableReferences(tables);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTableReference(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(JoinTable table) {
		if(null == table){
			throw new IllegalArgumentException("Table must be set.");
		}
		
		JoinTableReferenceClause joinTableReference = this.getSourceJoinTableReference();
		joinTableReference.addJoinTableReference(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinConditions(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinConditions(List<JoinCondition> conditions) {
		if(null == conditions){
			throw new IllegalArgumentException("Condition must be set.");
		}
		
		JoinConditionClause clause = this.getSourceJoinCondition();
		clause.setJoinConditions(conditions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
	 */
	@Override
	@Deprecated
	public void addJoinCondition(JoinCondition condition) {
		if(null == condition){
			throw new IllegalArgumentException("Condition must be set.");
		}
		
		JoinConditionClause clause = this.getSourceJoinCondition();
		clause.addJoinCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)
	 */
	@Override
	protected void updateCompiler(StatementCompiler compiler) {
		// No statement contains, do nothing
		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)
	 */
	@Override
	protected void updateCompiler(ClauseCompiler compiler) {
		if(null != this.joinTableReference){
			this.joinTableReference.setCompiler(compiler);
		}
		
		if(null != this.joinCondition){
			this.joinCondition.setCompiler(compiler);
		}
		
		if(null != this.joinTable){
			this.joinTable.setCompiler(compiler);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#construct()
	 */
	@Override
	public String construct() {
		return this.compiler.compile(this);
	}

	@Deprecated
	private JoinTableReferenceClause getSourceJoinTableReference(){
		if(null == this.joinTableReference){
			this.joinTableReference = new JoinTableReferenceClause();
			this.joinTableReference.setCompiler(this.clauseCompiler);
		}
		
		return this.joinTableReference;
	}
	
	@Deprecated
	private JoinConditionClause getSourceJoinCondition(){
		if(null == this.joinCondition){
			this.joinCondition = new JoinConditionClause();
			this.joinCondition.setCompiler(this.clauseCompiler);
		}
		
		return this.joinCondition;
	}
	
	private JoinTableClause getSourceJoinTable(){
		if(null == this.joinTable){
			this.joinTable = new JoinTableClause();
			this.joinTable.setCompiler(this.clauseCompiler);
		}
		
		return this.joinTable;
	}
}
