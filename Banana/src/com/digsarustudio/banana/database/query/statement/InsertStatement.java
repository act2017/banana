/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import java.util.List;

import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.DuplicateKeyUpdateClause;
import com.digsarustudio.banana.database.query.claue.InsertColumnClause;
import com.digsarustudio.banana.database.query.claue.InsertValueClause;
import com.digsarustudio.banana.database.query.claue.ModifierClause;
import com.digsarustudio.banana.database.query.claue.TableReferenceClause;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;

/**
 * This statement is used to generate a SQL string which contains the information of a particular dataset 
 * going to be inserted into a database.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class InsertStatement extends SyntaxStatement implements SQLStatement {
	private ModifierClause				modifier			= null;
	private TableReferenceClause		tableReference		= null;
	private InsertColumnClause			insertColumn		= null;
	private InsertValueClause			insertValue			= null;
	private DuplicateKeyUpdateClause	duplicateKeyUpdate	= null;
	private SelectStatement				selectStatement		= null;
	
	/**
	 * 
	 */
	public InsertStatement() {
	}
	
	/**
	 * Returns the modifier in SQL string, null returned if the modifier is not supported.<br>
	 * 
	 * @return the modifier in SQL string, null returned if the modifier is not supported
	 */
	public String getSQLModifier(){
		if(null == this.modifier){
			return null;
		}
		
		this.modifier.build();
		return this.modifier.getResult();
	}
	
	/**
	 * Returns the table reference in SQL string, null returned if the table reference is not supported.<br>
	 * 
	 * @return the table reference in SQL string, null returned if the table reference is not supported
	 */
	public String getSQLTableReference(){
		if(null == this.tableReference){
			return null;
		}
		
		this.tableReference.build();
		return this.tableReference.getResult();
	}
	
	/**
	 * Returns the insert column in SQL string, null returned if the insert column is not supported.<br>
	 * 
	 * @return the insert column in SQL string, null returned if the insert column is not supported
	 */
	public String getSQLInsertColumn(){
		if(null == this.insertColumn){
			return null;
		}
		
		this.insertColumn.build();
		return this.insertColumn.getResult();
	}
	
	/**
	 * Returns the insert value in SQL string, null returned if the insert value is not supported.<br>
	 * 
	 * @return the insert value in SQL string, null returned if the insert value is not supported
	 */
	public String getSQLInsertValue(){
		if(null == this.insertValue){
			return null;
		}
		
		this.insertValue.build();
		return this.insertValue.getResult();
	}
	
	/**
	 * Returns the duplicate key update in SQL string, null returned if the duplicate key update is not supported.<br>
	 * 
	 * @return the duplicate key update in SQL string, null returned if the duplicate key update is not supported
	 */
	public String getSQLDuplicateKeyUpdate(){
		if(null == this.duplicateKeyUpdate){
			return null;
		}
		
		this.duplicateKeyUpdate.build();
		return this.duplicateKeyUpdate.getResult();
	}
	
	/**
	 * Returns the select statement in SQL string, null returned if the select statement is not supported.<br>
	 * 
	 * @return the select statement in SQL string, null returned if the select statement is not supported
	 */
	public String getSQLSelectStatement(){
		if(null == this.selectStatement){
			return null;
		}
		
		return this.selectStatement.construct();
	}
	
	/**
	 * Returns the modifier
	 * 
	 * @return the modifier
	 * 
	 * @deprecated 1.0.2 Please use {@link #getModifier()} instead.
	 */
	public ModifierClause getModifier() {
		return modifier;
	}

	/**
	 * Returns the table reference
	 * 
	 * @return the table reference
	 * 
	 * @deprecated 1.0.2 Please use {@link #getTableReference()} instead.
	 */
	public TableReferenceClause getTableReference() {
		return tableReference;
	}

	/**
	 * Returns the insert column
	 * 
	 * @return the insert column
	 * 
	 * @deprecated 1.0.2 Please use {@link #getInsertColumn()} instead.
	 */
	public InsertColumnClause getInsertColumn() {
		return insertColumn;
	}

	/**
	 * Returns the insert value
	 * 
	 * @return the insert Value
	 * 
	 * @deprecated 1.0.2 Please use {@link #getInsertValue()} instead.
	 */
	public InsertValueClause getInsertValue() {
		return insertValue;
	}

	/**
	 * Returns the duplicate key update clause
	 * 
	 * @return the duplicate key update clause
	 * 
	 * @deprecated 1.0.2 Please use {@link #getDuplicateKeyUpdate()} instead.
	 */
	public DuplicateKeyUpdateClause getDuplicateKeyUpdate() {
		return duplicateKeyUpdate;
	}

	/**
	 * Returns the select statement
	 * 
	 * @return the select statement
	 */
	public SelectStatement getSelectStatement() {
		return selectStatement;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setModifier(SQLSyntaxModifiers modifier) {
		if( null == modifier){
			return;
		}
		
		ModifierClause clause = this.getSourceModifier();		
		clause.setModifier(modifier);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setInsertInto(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setInsertInto(Table table) {
		if( null == table){
			throw new IllegalArgumentException("Table must be set");
		}
		
		TableReferenceClause clause = this.getSourceTableReference();		
		clause.setInsertInto(table);		
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setSelectFrom(Table table) {
		if( null == table ){
			throw new IllegalArgumentException("Table must be set");
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setSelectFrom(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		if( null == dataset){
			throw new IllegalArgumentException("Data to insert must be set");
		}
		
		InsertColumnClause clause = this.getSourceInsertColumn();
		clause.setDatasets(dataset);

		InsertValueClause valueClause = this.getSourceInsertValue();
		valueClause.setDatasets(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		if( null == dataset){
			throw new IllegalArgumentException("Data to insert must be set");
		}
		
		InsertColumnClause clause = this.getSourceInsertColumn();
		clause.addDataset(dataset);

		InsertValueClause valueClause = this.getSourceInsertValue();
		valueClause.addDataset(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOnDuplicateKeyUpdateAssignments(java.util.List)
	 */
	@Override
	public void setOnDuplicateKeyUpdateAssignments(List<DuplicateKeyUpdateAssignment> assignments) {
		if( null == assignments){
			return;
		}
		
		DuplicateKeyUpdateClause clause = this.getSourceDuplicateKeyUpdate();		
		clause.setOnDuplicateKeyUpdateAssignments(assignments);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addOnDuplicateKeyUpdateAssignment(com.digsarustudio.banana.database.query.DuplicateKeyUpdateAssignment)
	 */
	@Override
	public void addOnDuplicateKeyUpdateAssignment(DuplicateKeyUpdateAssignment assignment) {
		if( null == assignment){
			return;
		}
		
		DuplicateKeyUpdateClause clause = this.getSourceDuplicateKeyUpdate();		
		clause.addOnDuplicateKeyUpdateAssignment(assignment);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectColumns(java.util.List)
	 */
	@Override
	public void setSelectColumns(List<SelectColumn> columns) {
		if( null == columns){
			throw new IllegalArgumentException("Column must be set");
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setSelectColumns(columns);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)
	 */
	@Override
	public void addSelectColumn(SelectColumn column) {
		if( null == column){
			throw new IllegalArgumentException("Column must be set");
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.addSelectColumn(column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)
	 */
	@Deprecated
	@Override
	public void setJoinType(TableJoinType type) {
		if( null == type){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setJoinType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTableRefereces(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableRefereces(List<Table> tables) {
		if( null == tables){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setJoinTableRefereces(tables);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTableReference(com.digsarustudio.banana.database.table.Table)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(Table table) {
		if( null == table){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.addJoinTableReference(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinConditions(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinConditions(List<JoinCondition> conditions) {
		if( null == conditions){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setJoinConditions(conditions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
	 */
	@Deprecated
	@Override
	public void addJoinCondition(JoinCondition condition) {
		if( null == condition){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.addJoinCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		if( null == conditions){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setConditions(conditions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		if( null == condition){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.addCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setAggregations(java.util.List)
	 */
	@Override
	public void setAggregations(List<Aggregation> aggregations) {
		if( null == aggregations){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setAggregations(aggregations);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addAggregation(com.digsarustudio.banana.database.query.Aggregation)
	 */
	@Override
	public void addAggregation(Aggregation aggregation) {
		if( null == aggregation){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.addAggregation(aggregation);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		if( null == orderings){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setOrderings(orderings);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		if( null == ordering){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.addOrdering(ordering);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOffset(java.lang.Integer)
	 */
	@Override
	public void setOffset(Integer offset) {
		if( null == offset){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setOffset(offset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		if( null == rowCount){
			return;
		}
		
		SelectStatement statement = this.getSourceSelectStatement();
		statement.setMaxRowCount(rowCount);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#construct()
	 */
	@Override
	public String construct() {
		return this.compiler.compile(this);
	}
	
	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)
	 */
	@Override
	protected void updateCompiler(StatementCompiler compiler) {
		if(null != this.selectStatement){
			this.selectStatement.setCompiler(compiler);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)
	 */
	@Override
	protected void updateCompiler(ClauseCompiler compiler) {
		if(null != this.modifier){
			this.modifier.setCompiler(compiler);
		}
		
		if(null != this.tableReference){
			this.tableReference.setCompiler(compiler);
		}
	
		if(null != this.insertColumn){
			this.insertColumn.setCompiler(compiler);
		}
		
		if(null != this.insertValue){
			this.insertValue.setCompiler(compiler);
		}
		
		if(null != this.duplicateKeyUpdate){
			this.duplicateKeyUpdate.setCompiler(compiler);
		}
		
		if(null != this.selectStatement){
			this.selectStatement.setCompiler(compiler);
		}		
	}

	private ModifierClause				getSourceModifier(){
		if( null == this.modifier ){
			this.modifier = new ModifierClause();
			this.modifier.setCompiler(this.clauseCompiler);
		}
		
		return this.modifier;
	}
	
	private TableReferenceClause		getSourceTableReference(){
		if( null == this.tableReference ){
			this.tableReference = new TableReferenceClause();
			this.tableReference.setCompiler(this.clauseCompiler);
		}
		
		return this.tableReference;
	}
	
	private InsertColumnClause			getSourceInsertColumn(){
		if( null == this.insertColumn ){
			this.insertColumn = new InsertColumnClause();
			this.insertColumn.setCompiler(this.clauseCompiler);
		}
		
		return this.insertColumn;
	}
	
	private InsertValueClause			getSourceInsertValue(){
		if( null == this.insertValue ){
			this.insertValue = new InsertValueClause();
			this.insertValue.setCompiler(this.clauseCompiler);
		}
		
		return this.insertValue;
	}
	
	private DuplicateKeyUpdateClause	getSourceDuplicateKeyUpdate(){
		if(null == this.duplicateKeyUpdate){
			this.duplicateKeyUpdate = new DuplicateKeyUpdateClause();
			this.duplicateKeyUpdate.setCompiler(this.clauseCompiler);
		}
		
		return this.duplicateKeyUpdate;
	}
	
	private SelectStatement getSourceSelectStatement(){
		if(null == this.selectStatement){
			this.selectStatement = new SelectStatement();
			this.selectStatement.setCompiler(this.compiler);
			this.selectStatement.setCompiler(this.clauseCompiler);			
		}
		
		return this.selectStatement;
	}
}
