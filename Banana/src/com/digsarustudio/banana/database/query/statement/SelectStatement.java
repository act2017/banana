/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.database.query.Aggregation;
import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.JoinCondition;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.TableJoinType;
import com.digsarustudio.banana.database.query.claue.AggregationClause;
import com.digsarustudio.banana.database.query.claue.ConditionClause;
import com.digsarustudio.banana.database.query.claue.LimitClause;
import com.digsarustudio.banana.database.query.claue.ModifierClause;
import com.digsarustudio.banana.database.query.claue.OrderingClause;
import com.digsarustudio.banana.database.query.claue.SQLClause;
import com.digsarustudio.banana.database.query.claue.SelectColumnClause;
import com.digsarustudio.banana.database.query.claue.TableReferenceClause;
import com.digsarustudio.banana.database.query.claue.VirtualTableReferenceClause;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;
import com.digsarustudio.banana.database.table.VirtualTable;

/**
 * This statement is used to fetch data from a table according to some particular conditions and the methods of 
 * aggregation to make the data be meaningful in a descending or ascending ordering.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class SelectStatement extends SyntaxStatement implements SQLStatement {
	private ModifierClause				modifier				= null;
	private SelectColumnClause			selectColumn			= null;
	private TableReferenceClause		tableReference			= null;
	private VirtualTableReferenceClause	virtualTableReference	= null;
	private List<JoinStatement>			joinTables				= null;
	private ConditionClause				whereCondition			= null;
	private AggregationClause			aggregation				= null;
	private ConditionClause				havingCondition			= null;
	private OrderingClause				ordering				= null;
	private LimitClause					offsetAndLimit			= null;
	
	/**
	 * 
	 */
	public SelectStatement() {
	}
	
	/**
	 * Returns the modifier in SQL string, null returned if the modifier is not supported.<br>
	 * 
	 * @return the modifier in SQL string, null returned if the modifier is not supported
	 */
	public String getSQLModifier(){
		if(null == this.modifier){
			return null;
		}
		
		this.modifier.build();
		return this.modifier.getResult();
	}
	
	/**
	 * Returns the select column in SQL string, null returned if the select column is not supported.<br>
	 * 
	 * @return the select column in SQL string, null returned if the select column is not supported
	 */
	public String getSQLSelectColumn(){
		if(null == this.selectColumn){
			return null;
		}
		
		this.selectColumn.build();
		return this.selectColumn.getResult();
	}
	
	/**
	 * Returns the table reference in SQL string, null returned if the table reference is not supported.<br>
	 * 
	 * @return the table reference in SQL string, null returned if the table reference is not supported
	 */
	public String getSQLTableReference(){
		if(null == this.tableReference){
			return null;
		}
		
		this.tableReference.build();
		return this.tableReference.getResult();
	}
	
	/**
	 * Returns the virtual table reference in SQL string, null returned if the virtual table reference is not supported.<br>
	 * 
	 * @return the virtual table reference in SQL string, null returned if the virtual table reference is not supported
	 */
	@Deprecated
	public String getSQLVirtualTableReference(){
		if(null == this.virtualTableReference){
			return null;
		}
		
		this.virtualTableReference.build();
		return this.virtualTableReference.getResult();
	}
	
	/**
	 * Returns the join table in SQL string, null returned if the join table is not supported.<br>
	 * 
	 * @return the join table in SQL string, null returned if the join table is not supported
	 */
	public String getSQLJoinTable(){
		if(null == this.joinTables){
			return null;
		}
		
		StringBuffer buffer = new StringBuffer();
		Integer count = 0 ;
		for (JoinStatement joinStatement : joinTables) {
			if(0 != count++) {
				buffer.append(" ");
			}
			
			buffer.append(joinStatement.construct());
		}
		
		return buffer.toString();
	}
	
	/**
	 * Returns the where condition in SQL string, null returned if the where condition is not supported.<br>
	 * 
	 * @return the where condition in SQL string, null returned if the where condition is not supported
	 */
	public String getSQLWhereCondition(){
		if(null == this.whereCondition){
			return null;
		}
		
		this.whereCondition.build();
		return this.whereCondition.getResult();
	}
	
	/**
	 * Returns the aggregation in SQL string, null returned if the modifier is not supported.<br>
	 * 
	 * @return the aggregation in SQL string, null returned if the modifier is not supported
	 */
	public String getSQLAggregation(){
		if(null == this.aggregation){
			return null;
		}
		
		this.aggregation.build();
		return this.aggregation.getResult();
	}
	
	/**
	 * Returns the having condition in SQL string, null returned if the having condition is not supported.<br>
	 * 
	 * @return the having condition in SQL string, null returned if the having condition is not supported
	 */
	public String getSQLHavingCondtion(){
		if(null == this.havingCondition){
			return null;
		}
		
		this.havingCondition.build();
		return this.havingCondition.getResult();
	}
	
	/**
	 * Returns the ordering in SQL string, null returned if the ordering is not supported.<br>
	 * 
	 * @return the ordering in SQL string, null returned if the ordering is not supported
	 */
	public String getSQLOrdering(){
		if(null == this.ordering){
			return null;
		}
		
		this.ordering.build();
		return this.ordering.getResult();
	}
	
	/**
	 * Returns the offset and limit in SQL string, null returned if the offset and limit is not supported.<br>
	 * 
	 * @return the offset and limit in SQL string, null returned if the offset and limit is not supported
	 */
	public String getSQLOffsetAndLImit(){
		if(null == this.offsetAndLimit){
			return null;
		}
		
		this.offsetAndLimit.build();
		return this.offsetAndLimit.getResult();
	}	

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setModifier(SQLSyntaxModifiers modifier) {
		if(null == modifier){
			return;
		}
		
		ModifierClause clause = this.getSourceModifier();
		clause.setModifier(modifier);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectFrom(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setSelectFrom(Table table) {
		if( null == table ){
//			throw new IllegalArgumentException("Table must be set");
			//Because the client code might use SELECT to get UUID or other value from SQL functions.
			return;			
		}
		
		TableReferenceClause clause = this.getSourceTableReference();
		clause.setSelectFrom(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectFrom(com.digsarustudio.banana.database.table.VirtualTable)
	 */
	@Override
	public void setSelectFrom(VirtualTable table) {
		if( null == table ){
			throw new IllegalArgumentException("Table must be set");
		}
		
		SQLClause clause= this.getSourceVirtualTableReference();
		clause.setSelectFrom(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setSelectColumns(java.util.List)
	 */
	@Override
	public void setSelectColumns(List<SelectColumn> columns) {
		if( null == columns ){
			throw new IllegalArgumentException("The columns to be fetched must be set");
		}
		
		SelectColumnClause clause = this.getSourceSelectColumn();
		clause.setSelectColumns(columns);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addSelectColumn(com.digsarustudio.banana.database.query.SelectColumn)
	 */
	@Override
	public void addSelectColumn(SelectColumn column) {
		if( null == column ){
			throw new IllegalArgumentException("The columns to be fetched must be set");
		}
		
		SelectColumnClause clause = this.getSourceSelectColumn();
		clause.addSelectColumn(column);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTable(java.util.List)
	 */
	@Override
	public void setJoinTable(List<JoinTable> tables) {
		if(null == tables){
			return;
		}
				
		for (JoinTable joinTable : tables) {
			this.addJoinTable(joinTable);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTable(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Override
	public void addJoinTable(JoinTable table) {
		if(null == table){
			return;
		}
		
		if(null == this.joinTables) {
			this.joinTables = new ArrayList<>();
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.addJoinTable(table);
		
		this.joinTables.add(statement);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinType(com.digsarustudio.banana.database.query.TableJoinType)
	 */
	@Deprecated
	@Override
	public void setJoinType(TableJoinType type) {
		if(null == type){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.setJoinType(type);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTableRefereces(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinTableRefereces(List<Table> tables) {
		if(null == tables){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.setJoinTableRefereces(tables);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTableReference(com.digsarustudio.banana.database.table.Table)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(Table table) {
		if(null == table){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.addJoinTableReference(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinTableReferences(java.util.List)
	 */
	@Override
	public void setJoinTableReferences(List<JoinTable> tables) {
		if(null == tables){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.setJoinTableReferences(tables);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinTableReference(com.digsarustudio.banana.database.query.JoinTable)
	 */
	@Deprecated
	@Override
	public void addJoinTableReference(JoinTable table) {
		if(null == table){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.addJoinTableReference(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setJoinConditions(java.util.List)
	 */
	@Deprecated
	@Override
	public void setJoinConditions(List<JoinCondition> conditions) {
		if(null == conditions){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.setJoinConditions(conditions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addJoinCondition(com.digsarustudio.banana.database.query.JoinCondition)
	 */
	@Deprecated
	@Override
	public void addJoinCondition(JoinCondition condition) {
		if(null == condition){
			return;
		}
		
		JoinStatement statement = this.getSourceJoinStatement();
		statement.addJoinCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		if(null == conditions){
			return;
		}
		
		for (Condition condition : conditions) {
			this.addCondition(condition);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		if(null == condition){
			return;
		}
		
		ConditionClause clause = condition.hasAggregateFunction() ? this.getSourceHavingCondition() : this.getSourceWhereCondition();
		clause.addCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setAggregations(java.util.List)
	 */
	@Override
	public void setAggregations(List<Aggregation> aggregations) {
		if(null == aggregations){
			return;
		}
		
		AggregationClause clause = this.getSourceAggregation();
		clause.setAggregations(aggregations);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addAggregation(com.digsarustudio.banana.database.query.Aggregation)
	 */
	@Override
	public void addAggregation(Aggregation aggregation) {
		if(null == aggregation){
			return;
		}
		AggregationClause clause = this.getSourceAggregation();
		clause.addAggregation(aggregation);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		if(null == orderings){
			return;
		}
		
		OrderingClause clause = this.getSourceOrdering();
		clause.setOrderings(orderings);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		if(null == ordering){
			return;
		}
		
		OrderingClause clause = this.getSourceOrdering();
		clause.addOrdering(ordering);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOffset(java.lang.Integer)
	 */
	@Override
	public void setOffset(Integer offset) {
		if(null == offset){
			return;
		}
		
		LimitClause clause = this.getSourceOffsetAndLimit();
		clause.setOffset(offset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		if(null == rowCount){
			return;
		}		
		
		LimitClause clause = this.getSourceOffsetAndLimit();
		clause.setMaxRowCount(rowCount);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)
	 */
	@Override
	protected void updateCompiler(StatementCompiler compiler) {
		if(null == this.joinTables) {
			return;
		}
		
		for (JoinStatement joinStatement : joinTables) {
			if(null == joinStatement) {
				continue;
			}
			
			joinStatement.setCompiler(compiler);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)
	 */
	@Override
	protected void updateCompiler(ClauseCompiler compiler) {
		if(null != this.modifier){
			this.modifier.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.selectColumn){
			this.selectColumn.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.tableReference){
			this.tableReference.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.joinTables){
			for (JoinStatement joinStatement : joinTables) {
				if(null == joinStatement) {
					continue;
				}
				
				joinStatement.setCompiler(compiler);
			}
		}
		
		if(null != this.whereCondition){
			this.whereCondition.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.aggregation){
			this.aggregation.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.havingCondition){
			this.havingCondition.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.ordering){
			this.ordering.setCompiler(this.clauseCompiler);
		}
		
		if(null != this.offsetAndLimit){
			this.offsetAndLimit.setCompiler(this.clauseCompiler);
		}
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#construct()
	 */
	@Override
	public String construct() {
		return this.compiler.compile(this);
	}

	private ModifierClause getSourceModifier(){
		if( null == this.modifier ){
			this.modifier = new ModifierClause();
			this.modifier.setCompiler(this.clauseCompiler);
		}
		
		return this.modifier;
	}
	
	private SelectColumnClause getSourceSelectColumn(){
		if(null == this.selectColumn){
			this.selectColumn = new SelectColumnClause();
			this.selectColumn.setCompiler(this.clauseCompiler);
		}
		
		return this.selectColumn;
	}
	
	private TableReferenceClause getSourceTableReference(){
		if(null == this.tableReference){
			this.tableReference = new TableReferenceClause();
			this.tableReference.setCompiler(this.clauseCompiler);
		}
		
		return this.tableReference;
	}
	
	private VirtualTableReferenceClause getSourceVirtualTableReference(){
		if(null == this.virtualTableReference){
			this.virtualTableReference = new VirtualTableReferenceClause();
			this.virtualTableReference.setCompiler(this.clauseCompiler);
		}
		
		return this.virtualTableReference;
	}
	
	
	private JoinStatement getSourceJoinStatement(){
		JoinStatement joinStatement = new JoinStatement();
		joinStatement.setCompiler(this.clauseCompiler);
		joinStatement.setCompiler(this.compiler);
		
		return joinStatement;
	}
	
	private ConditionClause getSourceWhereCondition(){
		if(null == this.whereCondition){
			this.whereCondition = new ConditionClause();
			this.whereCondition.setCompiler(this.clauseCompiler);
		}
		
		return this.whereCondition;
	}
	
	private AggregationClause getSourceAggregation(){
		if(null == this.aggregation){
			this.aggregation = new AggregationClause();
			this.aggregation.setCompiler(this.clauseCompiler);
		}
		
		return this.aggregation;
	}
	
	private ConditionClause getSourceHavingCondition(){
		if(null == this.havingCondition){
			this.havingCondition = new ConditionClause();
			this.havingCondition.setCompiler(this.clauseCompiler);
		}
		
		return this.havingCondition;
	}
	
	private OrderingClause getSourceOrdering(){
		if(null == this.ordering){
			this.ordering = new OrderingClause();
			this.ordering.setCompiler(this.clauseCompiler);
		}
		
		return this.ordering;
	}
	
	private LimitClause getSourceOffsetAndLimit(){
		if(null == this.offsetAndLimit){
			this.offsetAndLimit = new LimitClause();
			this.offsetAndLimit.setCompiler(this.clauseCompiler);
		}
		
		return this.offsetAndLimit;
	}
}
