/**
 * 
 */
package com.digsarustudio.banana.database.query.statement;

import java.util.List;

import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.Ordering;
import com.digsarustudio.banana.database.query.SQLSyntaxModifiers;
import com.digsarustudio.banana.database.query.claue.ConditionClause;
import com.digsarustudio.banana.database.query.claue.LimitClause;
import com.digsarustudio.banana.database.query.claue.ModifierClause;
import com.digsarustudio.banana.database.query.claue.OrderingClause;
import com.digsarustudio.banana.database.query.claue.TableReferenceClause;
import com.digsarustudio.banana.database.query.claue.UpdateSetClause;
import com.digsarustudio.banana.database.query.claue.compiler.ClauseCompiler;
import com.digsarustudio.banana.database.query.statement.compiler.StatementCompiler;
import com.digsarustudio.banana.database.query.Table;

/**
 * This statement is used to update a data by some particular conditions.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class UpdateStatement extends SyntaxStatement implements SQLStatement {
	private ModifierClause			modifier		= null;
	private TableReferenceClause	tableReference	= null;
	private UpdateSetClause			dataset			= null;
	private ConditionClause			condition		= null;
	private OrderingClause			ordering		= null;
	private LimitClause				maxRowCount		= null;
	
	/**
	 * 
	 */
	public UpdateStatement() {
		
	}
	
	/**
	 * Returns the modifier in SQL string, null returned if the modifier is not supported.<br>
	 * 
	 * @return the modifier in SQL string, null returned if the modifier is not supported
	 */
	public String getSQLModifier(){
		if(null == this.modifier){
			return null;
		}
		
		this.modifier.build();
		return this.modifier.getResult();
	}
	
	/**
	 * Returns the table reference in SQL string, null returned if the table reference is not supported.<br>
	 * 
	 * @return the table reference in SQL string, null returned if the table reference is not supported
	 */
	public String getSQLTableReference(){
		if(null == this.tableReference){
			return null;
		}
		
		this.tableReference.build();
		return this.tableReference.getResult();
	}
	
	/**
	 * Returns the dataset in SQL string, null returned if the dataset is not supported.<br>
	 * 
	 * @return the dataset in SQL string, null returned if the dataset is not supported
	 */
	public String getSQLDataset(){
		if(null == this.dataset){
			return null;
		}
		
		this.dataset.build();
		return this.dataset.getResult();
	}
	
	/**
	 * Returns the condition in SQL string, null returned if the condition is not supported.<br>
	 * 
	 * @return the condition in SQL string, null returned if the condition is not supported
	 */
	public String getSQLCondition(){
		if(null == this.condition){
			return null;
		}
		
		this.condition.build();
		return this.condition.getResult();
	}
	
	/**
	 * Returns the ordering in SQL string, null returned if the ordering is not supported.<br>
	 * 
	 * @return the ordering in SQL string, null returned if the ordering is not supported
	 */
	public String getSQLOrdering(){
		if(null == this.ordering){
			return null;
		}
		
		this.ordering.build();
		return this.ordering.getResult();
	}
	
	/**
	 * Returns the max row count in SQL string, null returned if the max row count is not supported.<br>
	 * 
	 * @return the max row count in SQL string, null returned if the max row count is not supported
	 */
	public String getSQLMaxRowCount(){
		if(null == this.maxRowCount){
			return null;
		}
		
		this.maxRowCount.build();
		return this.maxRowCount.getResult();
	}

	/**
	 * Returns the modifier
	 * 
	 * @return the modifier
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLModifier()} instead
	 */
	public ModifierClause getModifier() {
		return modifier;
	}

	/**
	 * Returns the reference of table
	 * 
	 * @return the reference of table
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLTableReference()} instead
	 */
	public TableReferenceClause getTableReference() {
		return tableReference;
	}

	/**
	 * Returns the dataset
	 * 
	 * @return the dataset
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLDataset()} instead
	 */
	public UpdateSetClause getDataset() {
		return dataset;
	}

	/**
	 * Returns the condition
	 * 
	 * @return the condition
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLCondition()} instead
	 */
	public ConditionClause getCondition() {
		return condition;
	}

	/**
	 * Returns the ordering
	 * 
	 * @return the ordering
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLOrdering()} instead
	 */
	public OrderingClause getOrdering() {
		return ordering;
	}

	/**
	 * Returns the max row count
	 * 
	 * @return the max row count
	 * 
	 * @deprecated 1.0.2 Please use {@link #getSQLMaxRowCount()} instead
	 */
	public LimitClause getMaxRowCount() {
		return maxRowCount;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setModifier(com.digsarustudio.banana.database.query.SQLSyntaxModifiers)
	 */
	@Override
	public void setModifier(SQLSyntaxModifiers modifier) {
		if(null == modifier){
			return;
		}
		
		ModifierClause clause = this.getSourceModifier();
		clause.setModifier(modifier);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setUpdateTable(com.digsarustudio.banana.database.table.Table)
	 */
	@Override
	public void setUpdateTable(Table table) {
		if( null == table){
			throw new IllegalArgumentException("Table must be set");
		}
		
		TableReferenceClause clause = this.getSourceTableReference();
		clause.setTable(table);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setDatasets(java.util.List)
	 */
	@Override
	public void setDatasets(List<Dataset> dataset) {
		if( null == dataset){
			throw new IllegalArgumentException("The data to be updated must be set");
		}
		
		UpdateSetClause clause = this.getSourceDataset();
		clause.setDatasets(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addDataset(com.digsarustudio.banana.database.query.Dataset)
	 */
	@Override
	public void addDataset(Dataset dataset) {
		if( null == dataset){
			throw new IllegalArgumentException("The data to be updated must be set");
		}
		
		UpdateSetClause clause = this.getSourceDataset();
		clause.addDataset(dataset);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setConditions(java.util.List)
	 */
	@Override
	public void setConditions(List<Condition> conditions) {
		if( null == conditions){
			throw new IllegalArgumentException("The data who is going to be updated must be set");
		}
		
		ConditionClause clause = this.getSourceCondition();
		clause.setConditions(conditions);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addCondition(com.digsarustudio.banana.database.query.Condition)
	 */
	@Override
	public void addCondition(Condition condition) {
		if( null == condition){
			throw new IllegalArgumentException("The data who is going to be updated must be set");
		}
		
		ConditionClause clause = this.getSourceCondition();
		clause.addCondition(condition);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setOrderings(java.util.List)
	 */
	@Override
	public void setOrderings(List<Ordering> orderings) {
		if(null == orderings){
			return;
		}
		
		OrderingClause clause = this.getSourceOrdering();
		clause.setOrderings(orderings);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#addOrdering(com.digsarustudio.banana.database.query.Ordering)
	 */
	@Override
	public void addOrdering(Ordering ordering) {
		if(null == ordering){
			return;
		}
		
		OrderingClause clause = this.getSourceOrdering();
		clause.addOrdering(ordering);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#setMaxRowCount(java.lang.Integer)
	 */
	@Override
	public void setMaxRowCount(Integer rowCount) {
		if(null == rowCount){
			return;
		}
		
		LimitClause clause = this.getSourceMaxRowCount();
		clause.setMaxRowCount(rowCount);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SQLStatement#construct()
	 */
	@Override
	public String construct() {
		return this.compiler.compile(this);
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.statement.StatementCompiler)
	 */
	@Override
	protected void updateCompiler(StatementCompiler compiler) {
		// No sub statement representing in this statement, do nothing.

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.statement.SyntaxStatement#updateCompiler(com.digsarustudio.banana.database.query.claue.ClauseCompiler)
	 */
	@Override
	protected void updateCompiler(ClauseCompiler compiler) {
		if(null != this.modifier){
			this.modifier.setCompiler(compiler);
		}
		
		if(null != this.tableReference){
			this.tableReference.setCompiler(compiler);
		}
		
		if(null != this.dataset){
			this.dataset.setCompiler(compiler);
		}
		
		if(null != this.condition){
			this.condition.setCompiler(compiler);
		}
		
		if(null != this.ordering){
			this.ordering.setCompiler(compiler);
		}
		
		if(null != this.maxRowCount){
			this.maxRowCount.setCompiler(compiler);
		}
	}

	private ModifierClause getSourceModifier(){
		if(null == this.modifier){
			this.modifier = new ModifierClause();
			this.modifier.setCompiler(this.clauseCompiler);
		}
		
		return this.modifier;
	}
	
	private TableReferenceClause getSourceTableReference(){
		if(null == this.tableReference){
			this.tableReference = new TableReferenceClause();
			this.tableReference.setCompiler(this.clauseCompiler);
		}
		
		return this.tableReference;
	}
	
	private UpdateSetClause getSourceDataset(){
		if(null == this.dataset){
			this.dataset = new UpdateSetClause();
			this.dataset.setCompiler(this.clauseCompiler);
		}
		
		return this.dataset;
	}
	
	private ConditionClause getSourceCondition(){
		if(null == this.condition){
			this.condition = new ConditionClause();
			this.condition.setCompiler(this.clauseCompiler);
		}
		
		return this.condition;
	}
	
	private OrderingClause getSourceOrdering(){
		if(null == this.ordering){
			this.ordering = new OrderingClause();
			this.ordering.setCompiler(this.clauseCompiler);
		}
		
		return this.ordering;
	}
	
	private LimitClause getSourceMaxRowCount(){
		if(null == this.maxRowCount){
			this.maxRowCount = new LimitClause();
			this.maxRowCount.setCompiler(this.clauseCompiler);
		}
		
		return this.maxRowCount;
	}
}
