package com.digsarustudio.banana.database.query;
import com.digsarustudio.banana.database.query.statement.SQLStatement;
import com.digsarustudio.banana.utils.StringFormatter;

/**
 * 
 */

/**
 * This temporary table is consisted of a select statement returning the query result.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 */
public class TemporaryTable extends QueryTable implements Table {
	/**
	 * 
	 * The object builder for {@link TemporaryTable}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static class Builder implements Table.Builder {
		private TemporaryTable result = null;

		public Builder() {
			this.result = new TemporaryTable();
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.Table.Builder#setName(java.lang.String)
		 */
		@Override
		public Builder setName(String name) {
			throw new UnsupportedOperationException("Please setup this table by #selectStatement()");
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.Table.Builder#setSelectStatement(com.digsarustudio.banana.database.query.statement.SQLStatement)
		 */
		@Override
		public Builder setSelectStatement(SQLStatement statement) {
			this.result.setStatement(statement);
			return this;
		}

		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.database.query.Table.Builder#setAlias(java.lang.String)
		 */
		@Override
		public Builder setAlias(String alias) {
			this.result.setAlias(alias);
			return this;
		}
		
		/* (non-Javadoc)
		 * @see com.digsarustudio.banana.utils.ObjectBuilder#build()
		 */
		@Override
		public Table build() {
			return this.result;
		}

	}
	
	private SQLStatement statement = null;
	
	/**
	 * 
	 */
	private TemporaryTable() {
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.Table#getName()
	 */
	@Override
	public String getName() {
		//The statement which represents as a virtual table cannot have ';' in the end of statement.
		String statement = this.statement.construct();
		statement = statement.replaceAll(";", "");
		return statement;
	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.query.Table#toSQLString()
	 */
	@Override
	public String toSQLString() {
		//The statement which represents as a virtual table cannot have ';' in the end of statement.
		String statement = this.statement.construct();
		statement = statement.replaceAll(";", "");
		return StringFormatter.format("(%s)",  statement);
	}

	/*
	 * To create a new builder
	 */
	public static Builder builder() {
		return new Builder();
	}
	
	protected void setStatement(SQLStatement statement){
		this.statement = statement;
	}
}
