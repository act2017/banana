/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * Ascending or descending
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum SortingType {
	  Ascending("ASC")
	, Descending("DESC")
	;
	
	private String value = null;
	
	private SortingType(String value){
		this.value = value;
	}
	
	public String getValue(){
		return this.value;
	}
	
	public static SortingType getDefault(){
		return SortingType.Ascending;
	}
}
