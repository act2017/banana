/**
 * 
 */
package com.digsarustudio.banana.database.query;

/**
 * Date time unit values
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public enum SQLDateTimeUnitValues {
	Second("SECOND")
	;
	
	private String value = null;
	
	private SQLDateTimeUnitValues(String value) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
}
