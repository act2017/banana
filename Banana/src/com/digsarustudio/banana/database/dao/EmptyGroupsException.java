/**
 * 
 */
package com.digsarustudio.banana.database.dao;

/**
 * This exception indicates the user tries to access a null group or an empty one.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class EmptyGroupsException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6140519650359295640L;

	/**
	 * 
	 */
	public EmptyGroupsException() {

	}

	/**
	 * @param message
	 */
	public EmptyGroupsException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EmptyGroupsException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EmptyGroupsException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EmptyGroupsException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
