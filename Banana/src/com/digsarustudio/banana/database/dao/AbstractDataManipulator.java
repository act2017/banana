/**
 * 
 */
package com.digsarustudio.banana.database.dao;


/**
 * The base class of {@link DataManipulator}.
 * The sub-class has to implement its own {@link #insert(Object)}, {@link #update(Object, Object)}, 
 * {@link #list()}, and {@link #remove(Object)} methods.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
@SuppressWarnings("deprecation")
public abstract class AbstractDataManipulator<T> implements DataManipulator<T> {
	/*
	 * The data access object provided from client code.<br>
	 */
	protected DataAccessObject dao = null;
	
	/**
	 * 
	 */
	public AbstractDataManipulator() {

	}

	/* (non-Javadoc)
	 * @see com.digsarustudio.banana.database.dao.DataManipulator#setDataAccessObject(com.digsarustudio.banana.database.dao.DataAccessObject)
	 */
	@Override
	public void setDataAccessObject(DataAccessObject dao) {
		this.dao = dao;
	}

}
