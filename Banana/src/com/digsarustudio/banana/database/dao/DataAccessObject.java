/**
 * 
 */
package com.digsarustudio.banana.database.dao;

import java.sql.SQLException;
import java.util.List;

import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.QueryResult;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.SortingCriteria;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type of this interface can do the insertion, modification, searching, and deletion
 * on a specific table.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject} instead to 
 * 						support strategy pattern.
 */
public interface DataAccessObject extends DataAccessObjectConfig{
	/**
	 * 
	 * The builder for {@link DataAccessObject}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DataAccessObject> {
		/**
		 * Assigns a query object for the data manipulation
		 * 
		 * @param query The query object for the data manipulation
		 * 
		 */
		Builder setQuery(Query query);
		
		/**
		 * Assign a table for the data manipulation
		 * 
		 * @param table The table for the data manipulation
		 * 
		 */
		Builder setTable(Table table);
		
		/**
		 * Set the query results returned without the duplicated data
		 * The results will contain the distinct data
		 * 
		 * @see DataAccessObjectConfig#setIgnoreDuplicateData()
		 */
		Builder setIgnoreDuplicateData();
		
		/**
		 * Set the query results returned with the duplicated data
		 * The results contains the duplicated data 
		 * 
		 * @see DataAccessObjectConfig#setIncludeDuplicateData()
		 */
		Builder setIncludeDuplicateData();
		
		/**
		 * To add a column for the search
		 * This function must be called before operating {@link DataAccessObject#fetch(String, String)}
		 * 
		 * @param column The table column for the search
		 * 
		 * @see DataAccessObjectConfig#addColumn(SelectColumn)
		 */
		Builder addColumn(SelectColumn column);
		
		/**
		 * Assigns a set of column for the search
		 * 
		 * @param columns The list of columns for the search
		 */
		Builder setColumns(List<SelectColumn> columns);
		
		/**
		 * Appends a dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 * 
		 * @param dataset The dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 * 
		 * @see DataAccessObjectConfig#addDataset(Dataset)
		 */		
		Builder addDataset(Dataset dataset);
		
		/**
		 * Assigns a set of data for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 * 
		 * @param dataset The dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 */
		Builder setDataset(List<Dataset> dataset);
		
		/**
		 * Append a list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 * 
		 * @param condition The condition for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 * 
		 * @see DataAccessObjectConfig#addCondition(Condition)
		 */
		Builder addCondition(Condition condition);
		
		/**
		 * Assigns a list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 * 
		 * @param conditions The list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 */
		Builder setConditions(List<Condition> conditions);
		
		/**
		 * Appends a sorting criteria for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
		 * and {@link DataAccessObject#update()}.<br>
		 * <br>
		 * If a sorting criteria has been added, the rows are going to be updated in the order that is specified 
		 * while {@link DataAccessObject#update()} is called.<br>
		 * <br>
		 * 
		 * @param criteria The sorting criteria for {@link DataAccessObject#fetch()} and {@link DataAccessObject#delete()}
		 * 
		 * @see DataAccessObjectConfig#addSortingCriteria(SortingCriteria)
		 */
		Builder addSortingCriteria(SortingCriteria criteria);
		
		/**
		 * Assigns a bulk of sorting criteria for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
		 * and {@link DataAccessObject#update()}.<br>
		 * <br>
		 * If a sorting criteria has been added, the rows are going to be updated in the order that is specified 
		 * while {@link DataAccessObject#update()} is called.<br>
		 * <br>
		 * 
		 * @param criteria The list of sorting criteria for {@link DataAccessObject#fetch()} and {@link DataAccessObject#delete()}
		 * 
		 * @see DataAccessObjectConfig#addSortingCriteria(SortingCriteria)
		 */
		Builder setSortingCriteria(List<SortingCriteria> criteria);
		
		/**
		 * Appends the details of table for the join used by {@link DataAccessObject#fetch()}
		 * 
		 * @param joinTable The details of the table to join
		 * 
		 * @see DataAccessObjectConfig#addJoinTable(JoinTable)
		 */
		Builder addJoinTable(JoinTable joinTable);
		
		/**
		 * Assigns a list of tables for the join used by {@link DataAccessObject#fetch()}
		 * 
		 * @param joinTable The details of the table to join
		 */
		Builder setJoinTables(List<JoinTable> joinTables);
		
		/**
		 * Appends a column for the grouping
		 * 
		 * @param column The column to make the data as a group
		 * 
		 * @see DataAccessObjectConfig#addGroupBy(TableColumn)
		 */
		Builder addGroupBy(TableColumn column);
		
		/**
		 * Assigns a list of columns for the grouping
		 * 
		 * @param columns The columns to make the data as a group
		 * 
		 */
		Builder setGroupBy(List<TableColumn> columns);		
	}
	
	/**
	 * Assigns a table as the target table.<br>
	 * 
	 * @param table The table to access
	 */
	void setTable(Table table);
	
	/**
	 * To insert data into a specific table.
	 * 
	 * Before calling this function, please set up the data manipulator by
	 * {@link DataAccessObject#addDataSet(Dataset)}
	 * @throws SQLException 
	 */
	void insert() throws SQLException;
	
	/**
	 * To update data into a specific table
	 * 
	 * Before calling this function, please set up the data manipulator by
	 * {@link DataAccessObject#addDataSet(Dataset)} and
	 * {@link DataAccessObject#addCondition(Condition)}
	 *  
	 * {@link DataAccessObject#addGroupBy(TableColumn)} is optional.
	 * @throws SQLException 
	 */
	void update() throws SQLException;
	
	/**
	 * To update data into a specific table on some rows
	 * 
	 * Before calling this function, please set up the data manipulator by
	 * {@link DataAccessObject#addDataSet(Dataset)} and
	 * {@link DataAccessObject#addCondition(Condition)}
	 *  
	 * {@link DataAccessObject#addGroupBy(TableColumn)} is optional.
	 * 
	 * @param limit The number of rows can be updated
	 * 
	 * @throws SQLException 
	 */
	void update(Integer limit) throws SQLException;
	
	/**
	 * To fetch the data from a specific table by a condition
	 * 
	 * @param cursor The start point for the search
	 * 
	 * @param limit The length of the data to return
	 * 
	 * @return The results of query
	 * @throws SQLException 
	 */
	QueryResult fetch(Integer cursor, Integer limit) throws SQLException;
	
	/**
	 * To fetch the data from a specific table by a condition.
	 * The data will be included from 0 to the maximum count
	 * 
	 * Before calling this function, please set up the data manipulator by
	 * {@link DataAccessObject#addColumn(SelectColumn)} and
	 * 
	 * 
	 * The following functions are optional:
	 * {@link DataAccessObject#addGroupBy(TableColumn)}
	 * {@link DataAccessObject#addCondition(Condition)}
	 * {@link DataAccessObject#addSortingCriteria(SortingCriteria)}
	 * 
	 * @return The results of query
	 * @throws SQLException 
	 */
	QueryResult fetch() throws SQLException;	
	
	/**
	 * To delete all of the dataset from a specific table by a condition
	 * 
	 * Before calling this function, please setup the data manipulator by
	 * {@link DataAccessObject#addCondition(Condition)}
	 * @throws SQLException 
	 */
	void delete() throws SQLException;
	
	/**
	 * To delete some dataset from a specific table by a condition in an ascending ordering
	 * or descending ordering.
	 * 
	 * Before calling this function, please setup the data manipulator by
	 * {@link DataAccessObject#addCondition(Condition)} and
	 * {@link DataAccessObject#addSortingCriteria(SortingCriteria)}
	 * 
	 * @param limit The maximum data to delete
	 * @throws SQLException 
	 */
	void delete(Integer limit) throws SQLException;
	
	/**
	 * Returns the last query string
	 * 
	 * @return the last query string
	 */
	String getLastQuery();
}
