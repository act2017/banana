/**
 * 
 */
package com.digsarustudio.banana.database.dao;

import java.util.List;

import com.digsarustudio.banana.database.query.Condition;
import com.digsarustudio.banana.database.query.Dataset;
import com.digsarustudio.banana.database.query.JoinTable;
import com.digsarustudio.banana.database.query.Query;
import com.digsarustudio.banana.database.query.SelectColumn;
import com.digsarustudio.banana.database.query.SortingCriteria;
import com.digsarustudio.banana.database.table.Table;
import com.digsarustudio.banana.database.table.TableColumn;
import com.digsarustudio.banana.utils.ObjectBuilder;

/**
 * The sub-type of this interface can setup the configurations for the {@link DataAccessObject}
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.2
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject} instead to 
 * 						support strategy pattern.
 */
public interface DataAccessObjectConfig {
	/**
	 * 
	 * The builder for {@link DataAccessObjectConfig}
	 *
	 * @author		Otto Hung <digsarustudio@gmail.com>
	 * @version		1.0.0
	 * @since		1.0.0
	 *
	 */
	public static interface Builder extends ObjectBuilder<DataAccessObjectConfig> {
		/**
		 * Assigns a query object for the data manipulation
		 * 
		 * @param query The query object for the data manipulation
		 * 
		 */
		Builder setQuery(Query query);
		
		/**
		 * Assign a table for the data manipulation
		 * 
		 * @param table The table for the data manipulation
		 * 
		 */
		Builder setTable(Table table);
		
		/**
		 * Set the query results returned without the duplicated data
		 * The results will contain the distinct data
		 * 
		 * @see DataAccessObjectConfig#setIgnoreDuplicateData()
		 */
		Builder setIgnoreDuplicateData();
		
		/**
		 * Set the query results returned with the duplicated data
		 * The results contains the duplicated data 
		 * 
		 * @see DataAccessObjectConfig#setIncludeDuplicateData()
		 */
		Builder setIncludeDuplicateData();
		
		/**
		 * To add a column for the search
		 * This function must be called before operating {@link DataAccessObject#fetch(String, String)}
		 * 
		 * @param column The table column for the search
		 * 
		 * @see DataAccessObjectConfig#addColumn(SelectColumn)
		 */
		Builder addColumn(SelectColumn column);
		
		/**
		 * Assigns a set of column for the search
		 * 
		 * @param columns The list of columns for the search
		 */
		Builder setColumns(List<SelectColumn> columns);
		
		/**
		 * Appends a dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 * 
		 * @param dataset The dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 * 
		 * @see DataAccessObjectConfig#addDataset(Dataset)
		 */		
		Builder addDataset(Dataset dataset);
		
		/**
		 * Assigns a set of data for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 * 
		 * @param dataset The dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
		 */
		Builder setDataset(List<Dataset> dataset);
		
		/**
		 * Append a list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 * 
		 * @param condition The condition for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 * 
		 * @see DataAccessObjectConfig#addCondition(Condition)
		 */
		Builder addCondition(Condition condition);
		
		/**
		 * Assigns a list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 * 
		 * @param conditions The list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
		 * and {@link DataAccessObject#delete()}
		 */
		Builder setConditions(List<Condition> conditions);
		
		/**
		 * Appends a sorting criteria for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
		 * and {@link DataAccessObject#update()}.<br>
		 * <br>
		 * If a sorting criteria has been added, the rows are going to be updated in the order that is specified 
		 * while {@link DataAccessObject#update()} is called.<br>
		 * <br>
		 * 
		 * @param criteria The sorting criteria for {@link DataAccessObject#fetch()} and {@link DataAccessObject#delete()}
		 * 
		 * @see DataAccessObjectConfig#addSortingCriteria(SortingCriteria)
		 */
		Builder addSortingCriteria(SortingCriteria criteria);
		
		/**
		 * Assigns a bulk of sorting criteria for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
		 * and {@link DataAccessObject#update()}.<br>
		 * <br>
		 * If a sorting criteria has been added, the rows are going to be updated in the order that is specified 
		 * while {@link DataAccessObject#update()} is called.<br>
		 * <br>
		 * 
		 * @param criteria The list of sorting criteria for {@link DataAccessObject#fetch()} and {@link DataAccessObject#delete()}
		 * 
		 * @see DataAccessObjectConfig#addSortingCriteria(SortingCriteria)
		 */
		Builder setSortingCriteria(List<SortingCriteria> criteria);
		
		/**
		 * Appends the details of table for the join used by {@link DataAccessObject#fetch()}
		 * 
		 * @param joinTable The details of the table to join
		 * 
		 * @see DataAccessObjectConfig#addJoinTable(JoinTable)
		 */
		Builder addJoinTable(JoinTable joinTable);
		
		/**
		 * Assigns a list of tables for the join used by {@link DataAccessObject#fetch()}
		 * 
		 * @param joinTable The details of the table to join
		 */
		Builder setJoinTables(List<JoinTable> joinTables);
		
		/**
		 * Appends a column for the grouping
		 * 
		 * @param column The column to make the data as a group
		 * 
		 * @see DataAccessObjectConfig#addGroupBy(TableColumn)
		 */
		Builder addGroupBy(TableColumn column);
		
		/**
		 * Assigns a list of columns for the grouping
		 * 
		 * @param columns The columns to make the data as a group
		 * 
		 */
		Builder setGroupBy(List<TableColumn> columns);		
	}	
	
	
	/**
	 * Assigns a query object for the data manipulation
	 * 
	 * @param query The query object for the data manipulation
	 * 
	 */
	void setQuery(Query query);
	
	/**
	 * Returns the query object for the data manipulation
	 * 
	 * @return the query object for the data manipulation
	 */
	Query getQuery();
	
	/**
	 * Assign a table for the data manipulation
	 * 
	 * @param table The table for the data manipulation
	 * 
	 */
	void setTable(Table table);
	
	/**
	 * Returns the table for the data manipulation
	 * 
	 * @return the table for the data manipulation
	 */
	Table getTable();
	
	/**
	 * Set the query results returned without the duplicated data
	 * The results will contain the distinct data
	 * 
	 * @see DataAccessObjectConfig#setIgnoreDuplicateData()
	 */
	void setIgnoreDuplicateData();
	
	/**
	 * Returns true if the duplicate data is ignored from the search result, otherwise false returned.
	 * 
	 * @return true if the duplicate data is ignored from the search result, otherwise false returned.
	 */
	Boolean isIgnoreDuplicateData();
	
	/**
	 * Set the query results returned with the duplicated data
	 * The results contains the duplicated data 
	 * 
	 * @see DataAccessObjectConfig#setIncludeDuplicateData()
	 */
	void setIncludeDuplicateData();
	
	/**
	 * To add a column for the search
	 * This function must be called before operating {@link DataAccessObject#fetch(String, String)}
	 * 
	 * @param column The table column for the search
	 * 
	 * @see DataAccessObjectConfig#addColumn(SelectColumn)
	 */
	void addColumn(SelectColumn column);
	
	/**
	 * Assigns a set of columns for the search
	 * 
	 * @param columns The list of columns for the search
	 */
	void setColumns(List<SelectColumn> columns);
	
	/**
	 * Returns a set of columns for {@link DataAccessObject#fetch()}
	 * 
	 * @return a set of columns for {@link DataAccessObject#fetch()}
	 */
	List<SelectColumn> getColumns();
	
	/**
	 * Appends a dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
	 * 
	 * @param dataset The dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
	 * 
	 * @see DataAccessObjectConfig#addDataset(Dataset)
	 */		
	void addDataset(Dataset dataset);
	
	/**
	 * Assigns a set of data for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
	 * 
	 * @param dataset The dataset for the {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
	 */
	void setDataset(List<Dataset> dataset);
	
	/**
	 * Returns a list of dataset for {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
	 * 
	 * @return a list of dataset for {@link DataAccessObject#insert()} and {@link DataAccessObject#update()}
	 */
	List<Dataset> getDataset();
	
	/**
	 * Append a list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
	 * and {@link DataAccessObject#delete()}
	 * 
	 * @param condition The condition for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
	 * and {@link DataAccessObject#delete()}
	 * 
	 * @see DataAccessObjectConfig#addCondition(Condition)
	 */
	void addCondition(Condition condition);
	
	/**
	 * Assigns a list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
	 * and {@link DataAccessObject#delete()}
	 * 
	 * @param conditions The list of conditions for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
	 * and {@link DataAccessObject#delete()}
	 */
	void setConditions(List<Condition> conditions);
	
	/**
	 * Returns a list of conditions used for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
	 * and {@link DataAccessObject#delete()}
	 * 
	 * @return a list of conditions used for {@link DataAccessObject#fetch()}, {@link DataAccessObject#update()},
	 * 			and {@link DataAccessObject#delete()}
	 */
	List<Condition> getConditions();
	
	/**
	 * Appends a sorting criteria for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
	 * and {@link DataAccessObject#update()}.<br>
	 * <br>
	 * If a sorting criteria has been added, the rows are going to be updated in the order that is specified 
	 * while {@link DataAccessObject#update()} is called.<br>
	 * <br>
	 * 
	 * @param criteria The sorting criteria for {@link DataAccessObject#fetch()} and {@link DataAccessObject#delete()}
	 * 
	 * @see DataAccessObjectConfig#addSortingCriteria(SortingCriteria)
	 */
	void addSortingCriteria(SortingCriteria criteria);
	
	/**
	 * Assigns a bulk of sorting criteria for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
	 * and {@link DataAccessObject#update()}.<br>
	 * <br>
	 * If a sorting criteria has been added, the rows are going to be updated in the order that is specified 
	 * while {@link DataAccessObject#update()} is called.<br>
	 * <br>
	 * 
	 * @param criteria The list of sorting criteria for {@link DataAccessObject#fetch()} and {@link DataAccessObject#delete()}
	 * 
	 * @see DataAccessObjectConfig#addSortingCriteria(SortingCriteria)
	 */
	void setSortingCriteria(List<SortingCriteria> criteria);
	
	
	/**
	 * Returns a set of sorting criteria used for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
	 * and {@link DataAccessObject#update()}.<br>
	 * 
	 * @return a set of sorting criteria used for {@link DataAccessObject#fetch()}, {@link DataAccessObject#delete()},
	 * 			and {@link DataAccessObject#update()}.<br>
	 */
	List<SortingCriteria> getSortingCriteria();
	
	/**
	 * Appends the details of table for the join used by {@link DataAccessObject#fetch()}
	 * 
	 * @param joinTable The details of the table to join
	 * 
	 * @see DataAccessObjectConfig#addJoinTable(JoinTable)
	 */
	void addJoinTable(JoinTable joinTable);
	
	/**
	 * Assigns a list of tables for the join used by {@link DataAccessObject#fetch()}
	 * 
	 * @param joinTable The details of the table to join
	 */
	void setJoinTables(List<JoinTable> joinTables);
	
	/**
	 * Returns a set of join table
	 * 
	 * @return a set of join table
	 */
	List<JoinTable> getJoinTables();
	
	/**
	 * Appends a column for the grouping
	 * 
	 * @param column The column to make the data as a group
	 * 
	 * @see DataAccessObjectConfig#addGroupBy(TableColumn)
	 */
	void addGroupBy(TableColumn column);
	
	/**
	 * Assigns a list of columns for the grouping
	 * 
	 * @param columns The columns to make the data as a group
	 * 
	 */
	void setGroupBy(List<TableColumn> columns);
	
	/**
	 * Returns a list of column used for the grouping
	 * 
	 * @return a list of column used for the grouping
	 */
	List<TableColumn> getGroupBys();
	
	/**
	 * Clear all of the settings.
	 */
	void clear();
}
