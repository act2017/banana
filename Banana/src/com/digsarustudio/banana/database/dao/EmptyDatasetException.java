/**
 * 
 */
package com.digsarustudio.banana.database.dao;

/**
 * This exception indicates the user tries to access a null dataset or an empty one.
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 */
public class EmptyDatasetException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6140519650359295640L;

	/**
	 * 
	 */
	public EmptyDatasetException() {

	}

	/**
	 * @param message
	 */
	public EmptyDatasetException(String message) {
		super(message);

	}

	/**
	 * @param cause
	 */
	public EmptyDatasetException(Throwable cause) {
		super(cause);

	}

	/**
	 * @param message
	 * @param cause
	 */
	public EmptyDatasetException(String message, Throwable cause) {
		super(message, cause);

	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public EmptyDatasetException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

	}

}
