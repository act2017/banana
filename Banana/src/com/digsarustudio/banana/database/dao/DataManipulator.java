/**
 * 
 */
package com.digsarustudio.banana.database.dao;

import java.sql.SQLException;
import java.util.List;

/**
 * The sub-class of this interface can inert, update, remove, and list a particular data object in/from
 * a specific table.<br>
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		1.0.0
 * @since		1.0.0
 *
 * @deprecated	1.0.2	Please use {@link com.digsarustudio.banana.database.manipulation.manipulator.DataAccessObject} and 
 * 						{@link com.digsarustudio.banana.database.manipulation.manipulator.DataManipulator} instead.
 */
public interface DataManipulator<T> {
	
	/**
	 * Assign a {@link DataAccessObject} for this manipulator to operate data with database.<br>
	 * 
	 * @param dao The {@link DataAccessObject} to manipulate.
	 */
	void setDataAccessObject(DataAccessObject dao);
	
	/**
	 * To insert a new data
	 * 
	 * @param data The data to insert
	 * @throws SQLException 
	 */
	void insert(T data) throws SQLException;
	
	/**
	 * To update an existing data to the new one.
	 * 
	 * @param original The existing data to update.<br>
	 * 				   Please use PK for the search
	 * @param incoming The new data for the existing data. 
	 * 
	 * @throws SQLException 
	 */
	void update(T original, T incoming) throws SQLException;
	
	/**
	 * To remove a particular data from a specific table
	 * 
	 * @param target The particular data to remove.
	 * @throws SQLException 
	 */
	void remove(T target) throws SQLException;
	
	/**
	 * To fetch all of the data from a particular table.<br>
	 * 
	 * @return a list of all of the data from a particular table. null returned if there is no data found.
	 * 
	 * @throws SQLException 
	 */
	List<T> list() throws SQLException;

	/**
	 * To fetch a specific number of data from a particular row.<br>
	 * 
	 * @param cursor The start point to fetch.
	 * @param limit The total data should be returned.
	 * 
	 * @return a list of all of the data from a particular table. null returned if there is no data found.
	 * 
	 * @throws SQLException 
	 */
	List<T> list(Integer cursor, Integer limit) throws SQLException;
	
	/**
	 * To fetch a list of item which matches the provided value of <b>target</b>
	 * 
	 * @param cursor The start point to fetch.
	 * @param limit The total data should be returned.
	 * @param condition The object which contains the condition to fetch.
	 * 
	 * @return The a list data from a specific row to the limit according to the condition, null returned if there is no data matched by condition.
	 * 
	 * @throws SQLException 
	 */
	List<T> list(Integer cursor, Integer limit, T condition) throws SQLException;
	
	/**
	 * To get a specific item by its own ID
	 * 
	 * @param condition The object which contains the condition to fetch.
	 * 
	 * @return The detail item according to the incoming ID.
	 * @throws SQLException 
	 */
	T get(T condition) throws SQLException;
}
