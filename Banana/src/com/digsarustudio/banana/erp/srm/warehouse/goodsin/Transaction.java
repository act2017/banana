/**
 * 
 */
package com.digsarustudio.banana.erp.srm.warehouse.goodsin;

import java.io.Serializable;

/**
 * @author lachlanwen
 *
 */
public class Transaction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8521042002106557902L;
	private String	supplier;
	private String	invoiceNo;
	private String	refNo;
	private String	partNo;
	private String	date;
	private Integer	quantity;
	private Float	price;
	private Float	freightRate;
	
	private String	desc;
	
	/**
	 * Constructs a transaction
	 */
	public Transaction(){
		
	}

	/**
	 * Returns the supplier
	 * 
	 * @return The supplier
	 */
	public String getSupplier() {
		return supplier;
	}

	/**
	 * Assign a supplier to this transaction
	 * 
	 * @param supplier The supplier for this transaction
	 */
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	/**
	 * Returns the invoice number
	 * 
	 * @return The number of this invoice
	 */
	public String getInvoiceNo() {
		return invoiceNo;
	}

	/**
	 * Assign an invoice number to this transaction
	 * 
	 * @param invoiceNo The number of invoice for this transaction 
	 */
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	/**
	 * Returns the reference number
	 * 
	 * @return The reference number
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * Assign a reference number to this transaction
	 * 
	 * @param refNo The reference number for this transaction
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	/**
	 * Returns the part number in this transaction
	 * 
	 * @return The part number in this transaction
	 */
	public String getPartNo() {
		return partNo;
	}

	/**
	 * Assign a part number to this transaction
	 * 
	 * @param partNo The part number for this transaction
	 */
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	/**
	 * Returns the date of the transaction
	 * 
	 * @return The date of the transaction
	 */
	public String getDate() {
		return date;
	}

	/**
	 * Assign a date to the transaction
	 * 
	 * @param date The date for the transaction
	 */
	public void setDate(String date) {
		this.date = date;
	}

	/**
	 * Returns the quantity in this transaction
	 * 
	 * @return The quantity in this transaction
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * Assign the quantity to this transaction
	 * 
	 * @param quantity The quantity for this transaction
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * Returns the price in this transaction
	 * 
	 * @return The price in this transaction
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * Assign a price to the transaction
	 * 
	 * @param price The price for the transaction
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/**
	 * Returns the freightRate rate in this transaction
	 * 
	 * @return The freightRate rate in this transaction
	 */	
	public Float getFreightRate() {
		return freightRate;
	}

	/**
	 * Assign a freight rate to this transaction
	 * 
	 * @param freightRate The freight rate for this transaction
	 */
	public void setFreightRate(Float freightRate) {
		this.freightRate = freightRate;
	}

	/**
	 * @return the desc
	 */
	public String getDesc() {
		return desc;
	}

	/**
	 * @param desc the desc to set
	 */
	public void setDesc(String desc) {
		this.desc = desc;
	}
		
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((desc == null) ? 0 : desc.hashCode());
		result = prime * result + ((freightRate == null) ? 0 : freightRate.hashCode());
		result = prime * result + ((invoiceNo == null) ? 0 : invoiceNo.hashCode());
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((refNo == null) ? 0 : refNo.hashCode());
		result = prime * result + ((supplier == null) ? 0 : supplier.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (desc == null) {
			if (other.desc != null)
				return false;
		} else if (!desc.equals(other.desc))
			return false;
		if (freightRate == null) {
			if (other.freightRate != null)
				return false;
		} else if (!freightRate.equals(other.freightRate))
			return false;
		if (invoiceNo == null) {
			if (other.invoiceNo != null)
				return false;
		} else if (!invoiceNo.equals(other.invoiceNo))
			return false;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (refNo == null) {
			if (other.refNo != null)
				return false;
		} else if (!refNo.equals(other.refNo))
			return false;
		if (supplier == null) {
			if (other.supplier != null)
				return false;
		} else if (!supplier.equals(other.supplier))
			return false;
		return true;
	}

	@Override
	public Transaction clone(){
		Transaction rtn = new Transaction();
		rtn.setSupplier(this.getSupplier());
		rtn.setInvoiceNo(this.getInvoiceNo());
		rtn.setRefNo(this.getRefNo());
		rtn.setPartNo(this.getPartNo());
		rtn.setDate(this.getDate());
		rtn.setQuantity(this.getQuantity());
		rtn.setPrice(this.getPrice());
		rtn.setFreightRate(this.getFreightRate());
		rtn.setDesc(this.getDesc());
		
		return rtn;
	}
}
