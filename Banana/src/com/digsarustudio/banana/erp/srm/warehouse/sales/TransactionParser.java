/**
 * 
 */
package com.digsarustudio.banana.erp.srm.warehouse.sales;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.converter.DateConverter;
import com.digsarustudio.banana.converter.IStringCoverterVisitor;

/**
 * To parse the transactions of Sales
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.0
 * @since		0.0.0
 *
 */
public class TransactionParser {	
	final int IDX_INVOICE_NO	= 0;
	final int IDX_DATE			= 1;
	final int IDX_HOUR			= 2;
	final int IDX_SALESMAN		= 3;
	final int IDX_GROUP			= 4;
	final int IDX_CUSTOMER_CODE	= 5;
	final int IDX_CUSTOMER_NAME	= 6;
	final int IDX_PART_NO		= 7;
	final int IDX_PART_DESC		= 8;
	final int IDX_QTY			= 9;
	final int IDX_PRICE			= 10;
	
	
	private IStringCoverterVisitor conversionVisitor	= null;
	
	/**
	 * Constructs a transaction parser
	 * 
	 * @param visitor The converter visitor to convert string into date object
	 */
	public TransactionParser(IStringCoverterVisitor visitor){
		this.conversionVisitor = visitor;
	}

	/**
	 * Returns the transactions in the specific file.
	 * 
	 * @param filePath The target file
	 * 
	 * @return The list of transactions
	 */
	public List<Transaction> getTransactions(String filePath){
		List<String> contents = this.loadFile(filePath);		
		
		return this.parse(contents);		
	}
	
	/**
	 * Load a file
	 * 
	 * @param filePath The path of the target file
	 * 
	 * @return True if there is no error occurred when file loaded; otherwise false. 
	 */
	private List<String> loadFile(String filePath){
		BufferedReader reader = null;
		List<String> rtn = null;
		String data = "";
		
		try {
			reader = new BufferedReader(new FileReader(filePath));
			
			rtn = new ArrayList<>();
			
			while( null != (data = reader.readLine()) ){
				rtn.add(data);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		//Catch for reader.readLine()
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(null != reader){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		
		return rtn;
	}
	
	/**
	 * Parse the content
	 * 
	 * @param contents The contents to parse
	 */
	public List<Transaction> parse(List<String> contents){
		List<Transaction> rtn = new ArrayList<>();
		DateConverter converter = new DateConverter("dd-MM-yy");		
		
		for (String content : contents) {
			String[] cav = content.split(",");
			
			if(cav.length <= 0)
				continue;
			
			Transaction transaction = new Transaction();			

			try {
				Integer qty = new Integer(cav[IDX_QTY]);
				
				if( 0 > qty )
					continue;
				
				transaction.setQuantity(qty);
			} catch (NumberFormatException e) {
				try{
					Float qty = Float.parseFloat(cav[IDX_QTY]);
					
					if( 0 > qty )
						continue;

					transaction.setQuantity(qty.intValue());
				}catch(NumberFormatException ex){
					continue;
				}
			}			
			
			try {				
				transaction.setPrice(new Float(cav[IDX_PRICE]));
			} catch (NumberFormatException e) {
				continue;
			}
			
			
			String invoiceNo = cav[IDX_INVOICE_NO];
			String partNo = cav[IDX_PART_NO];
			
			if( invoiceNo.isEmpty() || partNo.isEmpty() )
				continue;
			
			transaction.setInvoiceNo(invoiceNo);
			
			converter.convertString(cav[IDX_DATE], this.conversionVisitor);
			transaction.setDate(converter.getResult());
			
			transaction.setCustomerCode(cav[IDX_CUSTOMER_CODE]);
			transaction.setCustomerName(cav[IDX_CUSTOMER_NAME]);
			
			transaction.setPartNo(partNo);			

		
			rtn.add(transaction);
		}
		
		return rtn;
	}
}
