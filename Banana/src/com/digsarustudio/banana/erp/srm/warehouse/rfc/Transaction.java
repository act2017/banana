/**
 * 
 */
package com.digsarustudio.banana.erp.srm.warehouse.rfc;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * Represents the transaction of RFC
 *
 * @author		Otto Hung <digsarustudio@gmail.com>
 * @version		0.0.1
 * @since		0.0.1
 *
 */
public class Transaction implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 4551464473951623823L;
	
	private String	supplier;
	private String	rfcNo;
	private String	creditNo;
	private Date	date;
	private String	partNo;
	private Boolean suspended;
	private Integer	quantity;
	private Float	price;
	
	/**
	 * Constructs a transaction
	 */
	public Transaction(){
		
	}

	/**
	 * Returns the supplier in this transaction
	 * 
	 * @return the supplier in this transaction
	 */
	public String getSupplier() {
		return supplier;
	}

	/**
	 * Assign a supplier to this transaction
	 * 
	 * @param supplier the supplier for this transaction to set
	 */
	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	/**
	 * Returns the number of RFC in this transaction
	 * 
	 * @return the number of RFC in this transaction
	 */
	public String getRFCNo() {
		return rfcNo;
	}

	/**
	 * Assign a number of RFC to this transaction
	 * 
	 * @param rfcNo the number of RFC to set
	 */
	public void setRFCNo(String rfcNo) {
		this.rfcNo = rfcNo;
	}

	/**
	 * Returns the number of credit note
	 * 
	 * @return the number of credit note
	 */
	public String getCreditNo() {
		return creditNo;
	}

	/**
	 * Assign the number of credit note to this transaction
	 * 
	 * @param creditNo the number of credit note for this transaction to set
	 */
	public void setCreditNo(String creditNo) {
		this.creditNo = creditNo;
	}

	/**
	 * Returns the date of transaction
	 * 
	 * @return the date of transaction
	 */
	public Date getDate() {
		return date;
	}

	/**
	 * Assign the date to this transaction
	 * 
	 * @param date the date for this transaction to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Returns the part number in this transaction
	 * 
	 * @return the partNo in this transaction
	 */
	public String getPartNo() {
		return partNo;
	}

	/**
	 * Assign a part number to this transaction
	 * 
	 * @param partNo the part number for this transaction to set
	 */
	public void setPartNo(String partNo) {
		this.partNo = partNo;
	}

	/**
	 * Returns the transaction is suspended or not.
	 * 
	 * @return True if the credit has been paid for this transaction, otherwise false
	 */
	public Boolean getSuspended() {
		return suspended;
	}

	/**
	 * Set this transaction is suspended or not
	 * @param suspended True if the credit has been paid for this transaction, otherwise false 
	 */
	public void setSuspended(Boolean suspended) {
		this.suspended = suspended;
	}

	/**
	 * Returns the quantity in this transaction
	 * 
	 * @return the quantity in this transaction
	 */
	public Integer getQuantity() {
		return quantity;
	}

	/**
	 * Assign a quantity to this transaction
	 * 
	 * @param quantity the quantity for this transaction to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	/**
	 * Returns the price in this transaction
	 * 
	 * @return the price in this transaction
	 */
	public Float getPrice() {
		return price;
	}

	/**
	 * Assign a price to this transaction
	 * 
	 * @param price the price for this transaction to set
	 */
	public void setPrice(Float price) {
		this.price = price;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((creditNo == null) ? 0 : creditNo.hashCode());
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + ((partNo == null) ? 0 : partNo.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
		result = prime * result + ((rfcNo == null) ? 0 : rfcNo.hashCode());
		result = prime * result + ((supplier == null) ? 0 : supplier.hashCode());
		result = prime * result + ((suspended == null) ? 0 : suspended.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transaction other = (Transaction) obj;
		if (creditNo == null) {
			if (other.creditNo != null)
				return false;
		} else if (!creditNo.equals(other.creditNo))
			return false;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (partNo == null) {
			if (other.partNo != null)
				return false;
		} else if (!partNo.equals(other.partNo))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (quantity == null) {
			if (other.quantity != null)
				return false;
		} else if (!quantity.equals(other.quantity))
			return false;
		if (rfcNo == null) {
			if (other.rfcNo != null)
				return false;
		} else if (!rfcNo.equals(other.rfcNo))
			return false;
		if (supplier == null) {
			if (other.supplier != null)
				return false;
		} else if (!supplier.equals(other.supplier))
			return false;
		if (suspended == null) {
			if (other.suspended != null)
				return false;
		} else if (!suspended.equals(other.suspended))
			return false;
		return true;
	}
	
	@Override
	public Transaction clone(){
		Transaction rtn = new Transaction();
	
		rtn.setSupplier(this.getSupplier());
		rtn.setRFCNo(this.getRFCNo());
		rtn.setCreditNo(this.getCreditNo());
		
		rtn.setDate(this.getDate());
		rtn.setPartNo(this.getPartNo());
		
		rtn.setSuspended(this.getSuspended());
		
		rtn.setQuantity(this.getQuantity());
		rtn.setPrice(this.getPrice());
		
		return rtn;
	}
}
