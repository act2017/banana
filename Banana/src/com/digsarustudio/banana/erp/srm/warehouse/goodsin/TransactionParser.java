/**
 * 
 */
package com.digsarustudio.banana.erp.srm.warehouse.goodsin;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.converter.DateConverter;
import com.digsarustudio.banana.converter.IStringCoverterVisitor;

/**
 * @author lachlanwen
 *
 */
public class TransactionParser {	
	final int IDX_SUPPLIER		= 0;		//This only valid in the first row of each section
	final int IDX_SUPPLIER_NAME	= 1;		//But Peach put invoice date in this filed for the transaction
	final int IDX_INVOICE_NO	= 2;
	final int IDX_ORDER_NO		= 3;
	final int IDX_REF_NO		= 4;		//Use this to recognize what supplier provided the stock
	
	final int IDX_BLANK_1		= 5;
	
	final int IDX_PART_NO		= 6;
	final int IDX_PART_DESC		= 7;
	
	final int IDX_BLANK_2		= 8;
	final int IDX_BLANK_3		= 9;
	
	final int IDX_DATE			= 10;
	final int IDX_QTY			= 11;		//This field can be negative because Peach Software uses negative value to fix the wrong invoice.<br>
	final int IDX_PRICE			= 12;
	
	final int IDX_BLANK_4		= 13;
	
	final int IDX_SUBTOTAL		= 14;
	final int IDX_FREIGHT		= 15;
	
	private IStringCoverterVisitor conversionVisitor	= null;
	
	/**
	 * Constructs a transaction parser
	 * 
	 * @param visitor The converter visitor to convert string into date object
	 */
	public TransactionParser(IStringCoverterVisitor visitor){
		this.conversionVisitor = visitor;
	}
	
	/**
	 * Returns the transactions in the specific file.
	 * 
	 * @param filePath The target file
	 * 
	 * @return The list of transactions
	 */
	public List<Transaction> getTransactions(String filePath){
		List<String> contents = this.loadFile(filePath);		
		
		return this.parse(contents);		
	}
	
	/**
	 * Load a file
	 * 
	 * @param filePath The path of the target file
	 * 
	 * @return True if there is no error occurred when file loaded; otherwise false. 
	 */
	private List<String> loadFile(String filePath){
		BufferedReader reader = null;
		List<String> rtn = null;
		String data = "";
		
		try {
			reader = new BufferedReader(new FileReader(filePath));
			
			rtn = new ArrayList<>();
			
			while( null != (data = reader.readLine()) ){
				rtn.add(data);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		//Catch for reader.readLine()
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(null != reader){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		
		return rtn;
	}
	
	/**
	 * Parse the content
	 * 
	 * @param contents The contents to parse
	 */
	public List<Transaction> parse(List<String> contents){
		List<Transaction> rtn = new ArrayList<>();
		
		for (String content : contents) {
			String[] cav = content.split(",");
			
			if(cav.length <= 0)
				continue;
			
			Transaction transaction = new Transaction();
			
			String supplier = cav[IDX_REF_NO].trim();
			String invoiceNo= cav[IDX_INVOICE_NO].trim();
			String partNo	= cav[IDX_PART_NO].trim();
			String desc		= cav[IDX_PART_DESC].trim();
			String date		= cav[IDX_DATE].trim();
			
			Integer qty		= 0;
			Float	price	= 0.0f;
			Float	freightRate = 0.0f;
			
			if( supplier.isEmpty() || invoiceNo.isEmpty() || partNo.isEmpty() )
				continue;
					
			try {
				qty = new Integer(cav[IDX_QTY].trim());
								
				price = new Float(cav[IDX_PRICE].trim());
				freightRate = new Float(cav[IDX_FREIGHT].trim());				
			} catch (NumberFormatException e) {
				continue;
			}			
			
			
			transaction.setSupplier(supplier);
			transaction.setInvoiceNo(invoiceNo);
			transaction.setPartNo(partNo);
			transaction.setDesc(desc);
			
			transaction.setDate(date);
			
			transaction.setQuantity(qty);
			transaction.setPrice(price);
			transaction.setFreightRate(freightRate);
			rtn.add(transaction);
		}
		
		return rtn;
	}
}
