/**
 * 
 */
package com.digsarustudio.banana.erp.srm.warehouse.rfc;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.digsarustudio.banana.converter.DateConverter;
import com.digsarustudio.banana.converter.IStringCoverterVisitor;

/**
 * @author lachlanwen
 *
 */
public class TransactionParser {	
	final int IDX_SUPPLIER		= 0;
	final int IDX_SUPPLIER_NAME	= 1;
	final int IDX_RFC_NO		= 2;
	final int IDX_CREDIT_NO		= 3;
	final int IDX_DATE			= 4;
	final int IDX_PART_NO		= 5;
	final int IDX_PART_DESC		= 6;
	final int IDX_RFC_DETAIL	= 7;
	final int IDX_SUSPENDED		= 8;
	final int IDX_QTY			= 9;
	final int IDX_PRICE			= 10;
	final int IDX_NETT			= 11;
	
	private IStringCoverterVisitor conversionVisitor	= null;
	
	/**
	 * Constructs a transaction parser
	 * 
	 * @param visitor The converter visitor to convert string into date object
	 */
	public TransactionParser(IStringCoverterVisitor visitor){
		this.conversionVisitor = visitor;
	}
	
	/**
	 * Returns the transactions in the specific file.
	 * 
	 * @param filePath The target file
	 * 
	 * @return The list of transactions
	 */
	public List<Transaction> getTransactions(String filePath){
		List<String> contents = this.loadFile(filePath);		
		
		return this.parse(contents);		
	}
	
	/**
	 * Load a file
	 * 
	 * @param filePath The path of the target file
	 * 
	 * @return True if there is no error occurred when file loaded; otherwise false. 
	 */
	private List<String> loadFile(String filePath){
		BufferedReader reader = null;
		List<String> rtn = null;
		String data = "";
		
		try {
			reader = new BufferedReader(new FileReader(filePath));
			
			rtn = new ArrayList<>();
			
			while( null != (data = reader.readLine()) ){
				rtn.add(data);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			
		//Catch for reader.readLine()
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			if(null != reader){
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}		
		
		return rtn;
	}
	
	/**
	 * Parse the content
	 * 
	 * @param contents The contents to parse
	 */
	public List<Transaction> parse(List<String> contents){
		List<Transaction> rtn = new ArrayList<>();
		DateConverter converter = new DateConverter("dd/MM/yy");		
		
		for (String content : contents) {
			String[] cav = content.split(",");
			
			if(cav.length <= 0)
				continue;
			
			Transaction transaction = new Transaction();
			
			String supplier = cav[IDX_SUPPLIER];
			String rfcNo = cav[IDX_RFC_NO];
			
			if(supplier.isEmpty() || rfcNo.isEmpty())
				continue;
			
			try {
				Integer qty = new Integer(cav[IDX_QTY]);
				
				if(qty < 0)
					continue;
				
				transaction.setQuantity(qty);
				transaction.setPrice(new Float(cav[IDX_PRICE]));
			} catch (NumberFormatException e) {
				continue;
			}
			
			transaction.setSupplier(supplier);
			transaction.setRFCNo(rfcNo);
			
			
			transaction.setCreditNo(cav[IDX_CREDIT_NO]);
			
			converter.convertString(cav[IDX_DATE], this.conversionVisitor);
			transaction.setDate(converter.getResult());
			
			transaction.setPartNo(cav[IDX_PART_NO]);			
			transaction.setSuspended((cav[IDX_SUSPENDED].toLowerCase() == "yes"));
		
			rtn.add(transaction);
		}
		
		return rtn;
	}
}
